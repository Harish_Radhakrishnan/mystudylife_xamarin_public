#!/bin/bash

# Fail if any commands fails
set -e

TEST_RUNNER_PATH="packages/xunit.runner.console.2.1.0/tools/xunit.console.exe"
CONFIGURATION="Release"

# Get all test projects. This assumes the dll has the same naming as the project
TEST_PROJECTS="$(ls tests/*/*.csproj | awk 'BEGIN{FS="/"}{print $2}' | sort | uniq)"

printf "Found test projects:\n${TEST_PROJECTS}\n\n"

# Build the test projects
for proj in $TEST_PROJECTS
do
# xbuild doesn't tell us what it's building so we output it
echo "Building $proj..."
xbuild /p:Configuration="$CONFIGURATION" /verbosity:minimal "tests/$proj/$proj.csproj"
echo ""
done

#Run the tests 
for proj in $TEST_PROJECTS
do
echo ""
mono "$TEST_RUNNER_PATH" "tests/$proj/bin/$CONFIGURATION/$proj.dll"
done