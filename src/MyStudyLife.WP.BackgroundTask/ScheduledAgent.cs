﻿using System.Windows;
using Microsoft.Phone.Scheduler;
using MyStudyLife.WP.Services;

namespace MyStudyLife.WP.BackgroundTask {
	public class BackgroundTask : ScheduledTaskAgent {
		//public readonly RaygunClient RaygunClient = new RaygunClient(WPConfig.RaygunClientApiKey);

        private static volatile bool _classInitialized;

        /// <remarks>
        ///     ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        public BackgroundTask() {
            if (_classInitialized) return;

            _classInitialized = true;

            Deployment.Current.Dispatcher.BeginInvoke(() => {
                Application.Current.UnhandledException += ScheduledAgent_UnhandledException;
            });
        }

        /// Code to execute on Unhandled Exceptions
        private void ScheduledAgent_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e) {
			//RaygunClient.Send(e.ExceptionObject, new List<string> {"BackgroundTask"});
        }

        protected override void OnInvoke(ScheduledTask task) {
            Deployment.Current.Dispatcher.BeginInvoke(async () => {
	            try {
	                if (!WPBackgroundTaskHelper.CheckAndInitializeIoC()) {
	                    return;
	                }

		            var sp = new WPStorageProvider();

		            await new LiveTileService(sp).RunAsync();

		            // Not supported by background tasks.
		            // await new ReminderService(sp, new WPCryptoProvider(), new WPConfig(), new WPReminderScheduler()).RunAsync();
	            }
	            //catch (Exception ex) {
		            //RaygunClient.Send(ex, new List<string> {"BackgroundTask"});
	            //}
	            finally {
		            NotifyComplete();
	            }
            });
        }
    }
}
