﻿using System;
using System.Diagnostics;
using Windows.UI.Xaml;
using Cirrious.CrossCore.Exceptions;
using MyStudyLife.W8.Services;
using Windows.ApplicationModel.Background;

namespace MyStudyLife.W8.BackgroundTasks {
    public sealed class LiveTileBackgroundTask : IBackgroundTask {
        // private volatile bool _cancelRequested = false;
        private BackgroundTaskDeferral _deferral = null;

        public async void Run(IBackgroundTaskInstance taskInstance) {
            _deferral = taskInstance.GetDeferral();

            if (W8BackgroundTaskHelper.CheckAndInitializeIoC()) {
                await UpdateLiveTile();
            }
            else {
                _deferral.Complete();
            }
        }

        private async System.Threading.Tasks.Task UpdateLiveTile() {
#if DEBUG
            var sw = new Stopwatch();
            sw.Start();
#endif

            try {
                await LiveTileService.RunAsync();
            }
#if DEBUG
            catch (Exception ex) {
                Debug.WriteLine("LiveTileBackgroundTask resulted in an exception: " + ex.ToLongString());
            }
#endif
            finally {
#if DEBUG
                Debug.WriteLine("LiveTileBackgroundTask finished at in {0} milliseconds", sw.ElapsedMilliseconds);

                sw.Stop();
#endif

                _deferral.Complete();
            }
        }
    }
}