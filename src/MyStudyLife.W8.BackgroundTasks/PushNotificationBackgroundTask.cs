﻿using System.Diagnostics;
using MyStudyLife.Net;
using MyStudyLife.W8.Services;
using Windows.ApplicationModel.Background;
using Windows.Networking.PushNotifications;
using MyStudyLife.Configuration;

namespace MyStudyLife.W8.BackgroundTasks {
    public sealed class PushNotificationBackgroundTask : IBackgroundTask {
        public void Run(IBackgroundTaskInstance taskInstance) {
            BackgroundTaskDeferral deferral = taskInstance.GetDeferral();

            RawNotification notification = taskInstance.TriggerDetails as RawNotification;

            if (notification == null) {
                Debug.WriteLine("PushNotificationBackgroundTask: Notification received was invalid or not a raw notification, terminating.");

                return;
            }

            var storageProvider = new W8StorageProvider();
            var networkProvider = new W8NetworkProvider(storageProvider);

            new W8PushNotificationService(
                networkProvider,
                new HttpApiClient(MslConfig.Current.ApiClientSettings, new DefaultHttpClientFactory(), storageProvider)
            ).HandleNotification(notification);

            // TODO: WARNING, the app won't sync in the background as we neither have SyncService registered or can wait for sync service to complete a sync.
            deferral.Complete();
        }
    }
}