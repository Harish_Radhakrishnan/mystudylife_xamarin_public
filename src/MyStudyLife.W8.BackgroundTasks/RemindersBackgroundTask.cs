﻿using System.Diagnostics;
using Cirrious.CrossCore;
using MyStudyLife.Data.Store;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using Windows.ApplicationModel.Background;

namespace MyStudyLife.W8.BackgroundTasks {
    public sealed class RemindersBackgroundTask : IBackgroundTask {
        private BackgroundTaskDeferral _deferral;

        public async void Run(IBackgroundTaskInstance taskInstance) {
            _deferral = taskInstance.GetDeferral();

            if (W8BackgroundTaskHelper.CheckAndInitializeIoC()) {
                await UpdateReminders();
            }
            else {
                _deferral.Complete();
            }
        }

        private async System.Threading.Tasks.Task UpdateReminders() {
            // TODO: [FUTURE] Log and be aware of cancellations

#if DEBUG
            Stopwatch sw = new Stopwatch();
            Debug.WriteLine("RemindersBackgroundTask started");
#endif

            try {
                await Mvx.Resolve<IReminderService>().RunAsync();
            }
            finally {
#if DEBUG
                Debug.WriteLine("RemindersBackgroundTask finished in {0}ms", sw.ElapsedMilliseconds);
#endif

                _deferral.Complete();
            }
        }
    }
}