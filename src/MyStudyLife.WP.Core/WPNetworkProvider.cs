﻿using System.Threading.Tasks;
using System.Windows;
using Microsoft.Phone.Net.NetworkInformation;
using MyStudyLife.Data;
using MyStudyLife.Net;

namespace MyStudyLife.WP {
	public class WPNetworkProvider : INetworkProvider {
		#region INetworkProvider

		public event NetworkConnectivityChangedDelegate ConnectivityChanged;

		public bool IsConnectionDisabled { get { return false; } }

		public bool IsOnline { get; private set; }

		#endregion

		// ReSharper disable once NotAccessedField.Local
		private readonly IStorageProvider _storageProvider;

		public WPNetworkProvider(IStorageProvider storageProvider) {
			this._storageProvider = storageProvider;

			Invalidate();
		}

		private async void Invalidate() {
			await GetCanUseConnectionAsync();

			DeviceNetworkInformation.NetworkAvailabilityChanged += async (sender, e) => {
				await GetCanUseConnectionAsync();
			};
		}

		#region INetworkProvider Methods

		public Task<bool> GetCanUseConnectionAsync() {
			// TODO: We could add some stuff like DeviceNetworkInformation.IsCellularDataEnabled

			return System.Threading.Tasks.Task.Run(() => {
			    bool wasOnline = this.IsOnline;

                // IsConnectionDisabled always false
                this.IsOnline = DeviceNetworkInformation.IsNetworkAvailable;
                
				// If the connection has changed.
				if (wasOnline != IsOnline && ConnectivityChanged != null) {
					Deployment.Current.Dispatcher.BeginInvoke(() => ConnectivityChanged.Invoke(
							this,
							new NetworkConnectivityChangedEventArgs(
                                wasOnline, IsConnectionDisabled, DeviceNetworkInformation.IsNetworkAvailable, IsConnectionDisabled
							)
						)
					);
				}

				return IsOnline && !IsConnectionDisabled;
			});
		}

		#endregion
	}
}
