﻿using System.IO;
using System.Security.Cryptography;
using System.Text;
using MyStudyLife.Security.Crypto;

namespace MyStudyLife.WP {
	public class WPCryptoProvider : ICryptoProvider {
		public byte[] ComputeHMACSHA1(string content, string privateKey) {
			HMACSHA1 algorithm = new HMACSHA1(Encoding.UTF8.GetBytes(privateKey));
			byte[] byteArray = Encoding.UTF8.GetBytes(content);

			using (MemoryStream cryptStream = new MemoryStream(byteArray)) {
				return algorithm.ComputeHash(cryptStream);
			}
		}
	}
}
