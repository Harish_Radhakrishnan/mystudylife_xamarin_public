﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Resources;
using MyStudyLife.Data;
using Newtonsoft.Json;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.WP {
    public class WPStorageProvider : IStorageProvider {
        #region Application Settings

        public void AddOrUpdateSetting<T>(string settingName, T settingValue) {
            IsolatedStorageSettings localSettings = IsolatedStorageSettings.ApplicationSettings;

            var type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            localSettings[settingName] = type.In(typeof(string), typeof(int), typeof(bool))
				? settingValue != null ? settingValue.ToString() : null
				: JsonConvert.SerializeObject(settingValue);

            localSettings.Save();
        }

        public T GetSetting<T>(string settingName) {
            IsolatedStorageSettings localSettings = IsolatedStorageSettings.ApplicationSettings;

	        string value;

            if (localSettings.TryGetValue(settingName, out value) && value != null) {
                var type = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

                if (type.In(typeof(string), typeof(int), typeof(bool))) {
                    if (type != typeof(string) && String.IsNullOrEmpty(value)) {
                        return default(T);
                    }

	                return (T) Convert.ChangeType(value, type);
	            }

		        if (value != "null") {
			        return JsonConvert.DeserializeObject<T>(value);
		        }
	        }

	        return default(T);
        }

        public void RemoveSetting(string settingName) {
            IsolatedStorageSettings localSettings = IsolatedStorageSettings.ApplicationSettings;

            localSettings.Remove(settingName);

            localSettings.Save();
        }

        #endregion

        #region Files

        private readonly object _fileLock = new object();
        private readonly Dictionary<string, object> _fileLocks = new Dictionary<string, object>();

        /// <summary>
        /// A lock used to prevent files being written to concurrently.
        /// 
        /// Also prevents the application suspending during write.
        /// </summary>
        public readonly object FileWriteLock = new object();

        private object LockForFile(string fileName) {
            object lockObj;

            lock (_fileLock) {
                if (!_fileLocks.TryGetValue(fileName, out lockObj))
                    _fileLocks.Add(fileName, (lockObj = new object()));
            }

            return lockObj;
        }

        public async Task<string> LoadFileContentsAsync(string fileName) {
            object lockObj = LockForFile(fileName);

            return await AsyncTask.Factory.StartNew(() => {
                lock (lockObj) {
                    IsolatedStorageFile storageFile = IsolatedStorageFile.GetUserStoreForApplication();

                    string contents;

                    using (var stream = storageFile.OpenFile(fileName, FileMode.OpenOrCreate, FileAccess.Read))
                    using (var reader = new StreamReader(stream)) {
                        contents = reader.ReadToEnd();
                    }

                    return contents;
                }
            });
        }

        public Task<Stream> FileOpenReadAsync(string fileName) {
            object lockObj = LockForFile(fileName);

            IsolatedStorageFile storageFile = IsolatedStorageFile.GetUserStoreForApplication();

            lock (lockObj) {
                return AsyncTask.FromResult((Stream)storageFile.OpenFile(fileName, FileMode.OpenOrCreate, FileAccess.Read));
            }
        }

        public async System.Threading.Tasks.Task WriteFileContentsAsync(string fileName, string contents) {
            object lockObj = LockForFile(fileName);

            await AsyncTask.Factory.StartNew(() => {
                    IsolatedStorageFile storageFile = IsolatedStorageFile.GetUserStoreForApplication();

                    // This lock stops the app exiting half way through writing a file.
                    lock (FileWriteLock) {
                        // This lock stops file read exception at the same time as write.
                        lock (lockObj) {
                            using (var stream = storageFile.OpenFile(fileName, FileMode.Create, FileAccess.Write))
                            using (var reader = new StreamWriter(stream)) {
                                reader.Write(contents);
                            }
                        }
                    }
                });
        }

        public async System.Threading.Tasks.Task RemoveFilesAsync() {
            await System.Threading.Tasks.Task.Factory.StartNew(() => {
                    IsolatedStorageFile storageFile = IsolatedStorageFile.GetUserStoreForApplication();

                    // Only delete JSON files because otherwise we're deleting
                    // the database or XML files from prior versions.
                    foreach (var file in storageFile.GetFileNames("*.json")) {
                        try {
                            storageFile.DeleteFile(file);
                        }
                        // ReSharper disable EmptyGeneralCatchClause
                        catch { }
                        // ReSharper restore EmptyGeneralCatchClause
                    }
                });
        }

        #endregion

        #region Resource

        public async Task<string> LoadResourceFileContentsAsync(string resourceUrl) {
            resourceUrl = resourceUrl.TrimStart('/');

            Debug.WriteLine("Retrieving resource file from path '{0}'", resourceUrl);

            Uri resourceUri = new Uri(resourceUrl, UriKind.Relative);

            StreamResourceInfo stream = Application.GetResourceStream(resourceUri);

            using (var reader = new StreamReader(stream.Stream)) {
                return await reader.ReadToEndAsync();
            }
        }

        public DateTimeOffset GetLastModified(string path) => IsolatedStorageFile.GetUserStoreForApplication().GetLastWriteTime(path);

        #endregion
    }
}
