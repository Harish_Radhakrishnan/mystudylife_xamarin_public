﻿using System;
using System.ComponentModel;
using System.Windows;
using MyStudyLife.Scheduling;

namespace MyStudyLife.WP.Tiles {
    public partial class AgendaEntryTile {
        public string Heading {
			get { return this.HeadingText.Text; }
            set {
                if (value != "NOW" && value != "NEXT" && value != "LATER")
                    throw new ArgumentOutOfRangeException("value", "Value must be either NOW, NEXT or LATER.");

                this.HeadingText.Text = value;
            }
        }

        public AgendaEntryTile(AgendaEntry entry, Kind kind, bool isTransparent = false)
            : base(String.Concat("agenda", kind), isTransparent) {

            this.InitializeComponent();

		    if (DesignerProperties.IsInDesignTool) {
			    this.TitleText.Text = "Computing";
			    this.TimeText.Text = "09:00 - 10:30";
			    this.LocationText.Text = "Computing 1, John Guy Building";
		    }
		    else {
			    this.SetEntry(entry, kind);
			}

            this.UpdatedText.Text = GetUpdatedText();
	    }

	    #region Overrides of WPTile

		protected override void Initialize() {
			InitializeComponent();
		}

	    public override void SetSize(TileSize size) {
		    base.SetSize(size);

		    Visibility hideWhenIconVisibility = size == TileSize.Icon ? Visibility.Collapsed : Visibility.Visible;

			this.TaskIndicator.Visibility = hideWhenIconVisibility;
			this.TimeText.Visibility = hideWhenIconVisibility;
			this.LocationText.Visibility = hideWhenIconVisibility;
		    this.UpdatedText.Visibility = hideWhenIconVisibility;
	    }

		#endregion
        
		public void SetEntry(AgendaEntry entry, Kind kind) {
			this.DataContext = entry;

			this.ImageName = String.Concat("agenda", kind);

			this.Heading = kind.ToString().ToUpperInvariant();

			this.TaskCountText.Text = entry.TasksDue.ToString();

			this.TitleText.Text = entry.Title;
			this.TimeText.Text = entry.Time;
			this.LocationText.Text = entry.Location;
		}

	    public enum Kind {
            Now,
            Next,
            Later
        }
    }
}
