﻿namespace MyStudyLife.WP.Tiles {
    public enum TileSize {
        Wide,
        Square,
		Icon,
    }
}