﻿using System;
using System.Windows;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.WP.Services.Tiles {
    public partial class ClassFrontTile {
        public static readonly DependencyProperty ClassProperty =
            DependencyProperty.Register("Class", typeof(AgendaEntry), typeof(ClassFrontTile), new PropertyMetadata(null));

        public AgendaEntry Class {
            get { return (AgendaEntry)GetValue(ClassProperty); }
            set {
                SetValue(ClassProperty, value);

                var _class = ((Class)value.Base);

                this.DataContext = value;

                this.TitleTextBlock.Text = value.Title.ToTitleCase();
                this.TimeTextBlock.Text = value.Time;
                this.LocationTextBlock.Text = value.Location.ToTitleCase();

                this.DateTextBlock.Text = String.Concat(_class.Day,
                                                        _class.Week != TwoWeekTimetableWeek.Both
                                                            ? _class.Week.GetTitle()
                                                            : String.Empty);
                this.NextOccurrenceRun.Text = value.StartTime.ToShortDateString();
            }
        }

        public ClassFrontTile() {
            InitializeComponent();
        }
    }
}
