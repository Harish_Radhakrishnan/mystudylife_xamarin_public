﻿using System;
using System.Windows;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.WP7.Services.Tiles
{
    public partial class ExamFrontTile
    {
        public static readonly DependencyProperty ExamProperty =
            DependencyProperty.Register("Exam", typeof (Exam), typeof (ExamFrontTile), new PropertyMetadata(null));

        public Exam Exam
        {
            get { return (Exam) GetValue(ExamProperty); }
            set
            {
                SetValue(ExamProperty, value);
                
                this.TitleTextBlock.Text    = value.Title.ToTitleCase();
                this.TimeTextBlock.Text     = value.Time;
                this.LocationTextBlock.Text = value.Location.ToTitleCase();

                this.DataContext = value;
            }
        }

        public ExamFrontTile()
        {
            InitializeComponent();
        }
    }
}
