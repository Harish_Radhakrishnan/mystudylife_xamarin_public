﻿using System;
using System.Globalization;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI;

namespace MyStudyLife.WP.Tiles {
    public partial class HolidayTile {
        public HolidayTile(Holiday holiday, DateTime forDate, bool isTransparent = false)
            : base(String.Format("holiday_{0:yyyyMMdd}", holiday.EndDate), isTransparent) {

            string headingText;

            if (forDate.IsToday()) {
                if (forDate == holiday.EndDate) {
                    headingText = R.RelativeTime.Today.ToUpper(CultureInfo.CurrentUICulture);
                }
                else if (holiday.EndDate.IsTomorrow()) {
                    headingText = "UNTIL TOMORROW";
                }
                else {
                    headingText = String.Concat("UNTIL ", GetRelativeDate(forDate, holiday.EndDate));
                }
            }
            else if (forDate.IsTomorrow()) {
                headingText = R.RelativeTime.Tomorrow.ToUpper(CultureInfo.CurrentUICulture);

                if (holiday.EndDate > forDate) {
                    headingText += String.Concat(" UNTIL ", GetRelativeDate(forDate, holiday.EndDate));
                }
            }
            else {
                headingText = forDate.ToString("dddd").ToUpper(CultureInfo.CurrentUICulture);
            }

            this.HeadingText.Text = headingText;
            this.HolidayNameText.Text = holiday.Name;
            this.UpdatedText.Text = GetUpdatedText();
        }

	    #region Overrides of WPTile

	    protected override void Initialize() {
			InitializeComponent();
	    }

	    public override void SetSize(TileSize size) {
	        switch (size) {
	            case TileSize.Wide:
                    this.IconGrid.Style = (System.Windows.Style) this.Resources["IconGridStyle"];
	                break;
                case TileSize.Square:
	                this.IconGrid.Style = (System.Windows.Style) this.Resources["SquareIconGridStyle"];
	                break;
                case TileSize.Icon:
                    throw new NotSupportedException();
	        }

		    base.SetSize(size);
	    }

	    #endregion

        private string GetRelativeDate(DateTime baseDate, DateTime date) {
            var diff = (date - baseDate).TotalDays;

            if (diff < 7) {
                return date.ToString("dddd").ToUpper(CultureInfo.CurrentCulture);
            }

            return date.ToString(DateTimeFormat.AreDayAndMonthTheWrongWayRound ? "MMM d" : "d MMM").ToUpper(CultureInfo.CurrentCulture);
        }
    }
}