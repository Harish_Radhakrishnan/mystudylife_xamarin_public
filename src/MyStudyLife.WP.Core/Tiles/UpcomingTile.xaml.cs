﻿using System;
using System.Globalization;
using System.Windows;
using MyStudyLife.Scheduling.Statistics;
using MyStudyLife.UI;

namespace MyStudyLife.WP.Tiles {
    public partial class UpcomingTile {
        private readonly AgendaStatistics _stats;
        
        public UpcomingTile(AgendaStatistics stats, bool isTransparent = false) : base("upcoming", isTransparent) {
            this._stats = stats;

            string headingText;

            if (stats.ForDate.IsToday()) {
                headingText = R.RelativeTime.Today.ToUpper(CultureInfo.CurrentUICulture);
            }
            else if (stats.ForDate.IsTomorrow()) {
                headingText = R.RelativeTime.Tomorrow.ToUpper(CultureInfo.CurrentUICulture);
            }
            else {
                headingText = stats.ForDate.ToString("dddd").ToUpper(CultureInfo.CurrentUICulture);
            }

            this.HeadingText.Text = headingText;
            this.UpdatedText.Text = GetUpdatedText();
        }

	    #region Overrides of WPTile

	    protected override void Initialize() {
			InitializeComponent();
	    }

	    public override void SetSize(TileSize size) {
	        if (size == TileSize.Icon) {
	            throw new NotSupportedException();
	        }

		    base.SetSize(size);

	        bool isSquare = size == TileSize.Square;
            
            string classNumber = _stats.Classes.Total.ToString(CultureInfo.CurrentUICulture);
            string taskNumber = _stats.Tasks.Total.ToString(CultureInfo.CurrentUICulture);
            string examNumber = _stats.Exams.Total.ToString(CultureInfo.CurrentUICulture);

            if (isSquare) {
                this.IconGrid.Visibility = Visibility.Collapsed;
                this.SquareIconGrid.Visibility = Visibility.Visible;

                this.SquareClassesText.Text = String.Concat(classNumber, _stats.Classes.Total == 1 ? " class" : " classes");
                this.SquareTasksText.Text = String.Concat(taskNumber, _stats.Tasks.Total == 1 ? " task" : " tasks", " due");
                this.SquareExamsText.Text = String.Concat(examNumber, _stats.Exams.Total == 1 ? " exam" : " exams");
		    }
            else {
                this.IconGrid.Visibility = Visibility.Visible;
                this.SquareIconGrid.Visibility = Visibility.Collapsed;
                
                this.ClassesText.Text = classNumber;
                this.TasksText.Text = taskNumber;
                this.ExamsText.Text = examNumber;
		    }
	    }

	    #endregion
    }
}