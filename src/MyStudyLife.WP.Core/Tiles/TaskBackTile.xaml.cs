﻿using System.Windows;
using MyStudyLife.Data;

namespace MyStudyLife.WP7.Services.Tiles
{
    public partial class TaskBackTile
    {
        public static readonly DependencyProperty TaskProperty =
            DependencyProperty.Register("Task", typeof(Task), typeof(TaskBackTile), new PropertyMetadata(null));

        public Task Task
        {
            get { return (Task) GetValue(TaskProperty); }
            set
            {
                SetValue(TaskProperty, value);

                this.ProgressRun.Text = value.Progress.ToString();

                this.DataContext = value;
            }
        }

        public TaskBackTile()
        {
            InitializeComponent();
        }
    }
}
