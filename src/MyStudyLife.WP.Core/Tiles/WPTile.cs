﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using ImageTools;
using ImageTools.IO.Png;
using MyStudyLife.Globalization;

namespace MyStudyLife.WP.Tiles {
    public abstract class WPTile : Grid {
        private const string UriFormat = @"Shared\ShellContent\{0}.png";

        private TileSize _size;
        private string _imageName;

        public string ImageName {
            get { return String.Concat( _imageName, "_", _size); }
            protected set { _imageName = value; }
        }

        protected WPTile(string imageName, bool isTransparent = false) {
// ReSharper disable once DoNotCallOverridableMethodsInConstructor
			this.Initialize();

            this.ImageName = imageName;

            // #1c8d76
            this.Background = new SolidColorBrush(isTransparent
                ? Colors.Transparent
                : new Color {
                    A = 255,
                    R = 28,
                    G = 141,
                    B = 118
                }
            );
        }

		/// <summary>
		///		Override to contain <c>InitializeComponent().</c>
		/// </summary>
	    protected abstract void Initialize();

        protected string GetUpdatedText() {
            return String.Format("updated: {0:" + (DateTimeFormat.AreDayAndMonthTheWrongWayRound ? "MMM d" : "d MMM") + ".} {0:t}", DateTime.Now);
        }

	    public virtual void SetSize(TileSize size) {
		    this._size = size;

		    if (size == TileSize.Icon) {
			    this.Width = 159;
			    this.Height = this.Width;
		    }
		    else {
			    this.Width = size == TileSize.Wide ? 691 : 336;
			    this.Height = 336;
		    }
	    }

        /// <summary>
        ///     Renders the current <see cref="WPTile"/>
        ///     to storage and returns the <see cref="Uri"/>
        ///     for the new file.
        /// </summary>
        /// <returns>The URI of the image.</returns>
        private Uri Render() {
            this.Measure(new Size(this.Width, this.Height));
            this.Arrange(new Rect(0d, 0d, this.Width, this.Height));

            var bitmap = new WriteableBitmap((int) this.Width, (int) this.Height);

            bitmap.Render(this, null);
            bitmap.Invalidate();

            string uri = GetUriForString(this.ImageName);

            var img = bitmap.ToImage();
            var encoder = new PngEncoder();
            
            using (var isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication()) {
                if (!isolatedStorage.DirectoryExists(@"Shared\ShellContent")) {
                    isolatedStorage.CreateDirectory(@"Shared\ShellContent");
                }

                using (var fs = new IsolatedStorageFileStream(uri, FileMode.OpenOrCreate, isolatedStorage)) {
                    encoder.Encode(img, fs);
                }
            }

            return new Uri("isostore:" + uri, UriKind.Absolute);
        }

        public Uri RenderSpecifiedSize(TileSize size) {
            this.SetSize(size);

            return Render();
        }

        public static Uri GetUriFor(string imageName) {
            return new Uri("isostore:" + GetUriForString(imageName), UriKind.Absolute);
        }

        private static string GetUriForString(string imageName) {
            return String.Format(UriFormat, imageName);
        }
    }
}
