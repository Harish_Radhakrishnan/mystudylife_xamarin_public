﻿using System.Windows;
using MyStudyLife.Data;

namespace MyStudyLife.WP.Services.Tiles {
    public partial class TaskFrontTile {
        public static readonly DependencyProperty TaskProperty =
            DependencyProperty.Register("Task", typeof(Task), typeof(TaskFrontTile), new PropertyMetadata(null));

        public Task Task {
            get { return (Task)GetValue(TaskProperty); }
            set {
                SetValue(TaskProperty, value);

                this.DataContext = value;

                this.TypeTextBlock.Text = value.Type.ToString().ToUpper();
                this.TitleTextBlock.Text = value.Title.ToTitleCase();
                this.SubjectTextBlock.Text = value.Subject.Name.ToTitleCase();
                this.DueDateRun.Text = value.DueDate.ToShortDateString();

#warning no colors!

                //if (value.UiState == UiState.None)
                //    this.UiStateBorder.Visibility = Visibility.Collapsed;
                //else if (value.UiState == UiState.Attention)
                //    this.UiStateBorder.Background = Resources["MslAttentionBrush"] as SolidColorBrush;
                //else if (value.UiState == UiState.Warning)
                //    this.UiStateBorder.Background = Resources["MslWarningBrush"] as SolidColorBrush;
                //else if (value.UiState == UiState.Okay)
                //    this.UiStateBorder.Background = Resources["MslOkayBrush"] as SolidColorBrush;
                //else if (value.UiState == UiState.Past)
                //    this.UiStateBorder.Background = Resources["MslPastBrush"] as SolidColorBrush;
            }
        }

        public TaskFrontTile() {
            InitializeComponent();
        }
    }
}
