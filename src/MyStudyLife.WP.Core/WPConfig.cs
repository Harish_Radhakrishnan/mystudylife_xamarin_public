﻿using System;
using System.Xml.Linq;
using MyStudyLife.Configuration;

namespace MyStudyLife.WP {
    public class WPConfig : MslConfig {
        public const string RaygunClientApiKey = "lxgXs5/F0vpMopTrgKedEQ==";

        // ReSharper disable once PossibleNullReferenceException
        private readonly Lazy<Version> _appVersion = new Lazy<Version>(() =>
           new Version(XDocument.Load("WMAppManifest.xml").Root.Element("App").Attribute("Version").Value));

        public override Version AppVersion => _appVersion.Value;

        protected override string ApiClientId => "gqluNBhn0OHa1Bfya0Ci4ZP3tGzPGdUh";
    }
}
