﻿using System;
using System.IO.IsolatedStorage;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Versioning;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.WP {
	public class WPUpgradeService : UpgradeService {
		public WPUpgradeService(IMslConfig config, IStorageProvider provider) : base(config, provider) {}

		#region Overrides of UpgradeService

		/// <summary>
		///     Allows applications to set the previous version
		///     property before an attempted upgrade is made.
		/// 
		///     Only called if <see cref="UpgradeService.PreviousAppVersion"/> is null.
		/// </summary>
		protected override AsyncTask EnsurePreviousVersionAsync() {
			return System.Threading.Tasks.Task.Run(() => {
				if (IsolatedStorageFile.GetUserStoreForApplication().FileExists("User.json")) {
					this.PreviousAppVersion = new Version(3, 0);
				}
			});
		}


		#endregion
	}
}
