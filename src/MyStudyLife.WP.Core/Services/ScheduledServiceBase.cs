﻿using System;
using System.Diagnostics;
using Microsoft.Phone.Info;
using MyStudyLife.Data;
using MyStudyLife.Services;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.WP.Services {
    // Removed stopwatch for now as it conflicts in System and MvvmCross SQLite plugin
    // but extern alias doesn't seem to work
    public abstract class ScheduledServiceBase : RequireStorageProviderBase, IScheduledService {
#if DEBUG
        //private readonly Stopwatch _sw = new Stopwatch();
#endif
        protected ScheduledServiceBase(IStorageProvider provider) : base(provider) {
#if DEBUG
            //this._sw.Start();
#endif
		}

        public abstract Task RunAsync();

        [Conditional("DEBUG")]
        protected void TraceMemoryUsage(string message = null, params object[] args) {
			Trace("Current Memory usage {0}/{1}" + (message != null ? " " + String.Format(message, args) : String.Empty), DeviceStatus.ApplicationCurrentMemoryUsage, DeviceStatus.ApplicationMemoryUsageLimit);
        }

	    protected void TryFreeMemory() {
#if DEBUG
		    var memoryBefore = DeviceStatus.ApplicationCurrentMemoryUsage;
#endif
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();
			
#if DEBUG
			Trace("Freed {0} bytes of memory", memoryBefore - DeviceStatus.ApplicationCurrentMemoryUsage);
#endif
	    }

		[Conditional("DEBUG")]
		protected void Trace(string message, params object[] args) {
#if DEBUG
		    //Debug.WriteLine("msl:ScheduledService:\t{0}\t{1}", _sw.Elapsed.TotalSeconds, String.Format(message, args));
#endif
	    }
    }
}
