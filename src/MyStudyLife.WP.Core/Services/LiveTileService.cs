﻿using System;
using System.Diagnostics;
using System.Linq;
using Cirrious.CrossCore;
using Community.SQLite;
using Microsoft.Phone.Info;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.Scheduling;
using MyStudyLife.WP.Tiles;
using AgendaStatistics = MyStudyLife.Scheduling.Statistics.AgendaStatistics;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.WP.Services {
    // The TryFreeMemory() calls may not look like much, but they free around
    // 1.5mb - 2mb after rendering each image.

    public class LiveTileService : ScheduledServiceBase {
        #region Uri

        private Uri DefaultLogoUri {
            get { return new Uri(@"appdata:background.png"); }
        }
        private Uri DefaultWideLogoUri {
            get { return new Uri(@"appdata:Assets\WideTile.png"); }
        }
        private Uri NonExistentUri {
            get { return new Uri(@"isostore:Shared\ShellContent\NonExistent.png"); }
        }

        #endregion

        public LiveTileService(IStorageProvider provider) : base(provider) { }

        #region IScheduledTask
        
        public override async Task RunAsync() {
            TraceMemoryUsage("RunAsync()");

            await UpdatePrimaryTileAsync();
        }

        #endregion

        public async Task UpdatePrimaryTileAsync() {
            DateTime now = DateTime.Now;

            var primaryTileData = new FlipTileData {
                Count = null,
                Title = String.Empty,
                BackTitle = String.Empty,
                SmallBackgroundImage = DefaultLogoUri,
                BackgroundImage = DefaultLogoUri,
                WideBackgroundImage = DefaultWideLogoUri,
                BackBackgroundImage = NonExistentUri,
                WideBackBackgroundImage = NonExistentUri
            };
            
            try {
                TraceMemoryUsage("Getting entries from agenda scheduler");

                AgendaDay today = await Mvx.Resolve<IAgendaScheduler>().GetDayForDateAsync(DateTime.Today, AgendaSchedulerOptions.SetTaskCounts);
                
                // We're removing all conflicting entries, this could be done better in future!
                var entriesRemainingToday = today.Entries.Where(ae => ae.EndTime > now && !ae.IsConflicting).OrderBy(ae => ae.StartTime).ToList();
                
                const bool isTransparent = true;
                bool generateUpcoming = true;

                if (today.IsHoliday) {
                    var holidayTile = new HolidayTile(today.Holiday, DateTime.Today, isTransparent);

                    primaryTileData.WideBackgroundImage = holidayTile.RenderSpecifiedSize(TileSize.Wide);
                    TraceMemoryUsage("Generated front wide tile image (Holiday)");
                    TryFreeMemory();

                    primaryTileData.BackgroundImage = holidayTile.RenderSpecifiedSize(TileSize.Square);
                    TraceMemoryUsage("Generated front square tile image (Holiday)");
                    TryFreeMemory();

                    if (today.Holiday.EndDate != today.Date) {
                        generateUpcoming = false;
                    }
                }
                else if (entriesRemainingToday.Any()) {
                    AgendaEntry entry = entriesRemainingToday.First();
                    AgendaEntryTile.Kind kind = entry.IsCurrent ? AgendaEntryTile.Kind.Now : AgendaEntryTile.Kind.Next;

                    var aeTile = new AgendaEntryTile(entry, kind, isTransparent);
                    
                    primaryTileData.WideBackgroundImage = aeTile.RenderSpecifiedSize(TileSize.Wide);
                    TraceMemoryUsage("Generated front wide tile image (AgendaEntry)");
                    TryFreeMemory();

                    primaryTileData.BackgroundImage = aeTile.RenderSpecifiedSize(TileSize.Square);
                    TraceMemoryUsage("Generated front square tile image (AgendaEntry)");
                    TryFreeMemory();

                    primaryTileData.SmallBackgroundImage = aeTile.RenderSpecifiedSize(TileSize.Icon);
                    TraceMemoryUsage("Generated front icon tile image (AgendaEntry)");
                    TryFreeMemory();

                    if (entriesRemainingToday.Count >= 2) {
                        generateUpcoming = false;

                        entry = entriesRemainingToday.Skip(1).First();

                        aeTile.SetEntry(entry,
                            kind == AgendaEntryTile.Kind.Now ? AgendaEntryTile.Kind.Next : AgendaEntryTile.Kind.Later);

                        primaryTileData.BackBackgroundImage = aeTile.RenderSpecifiedSize(TileSize.Square);
                        TraceMemoryUsage("Generated back medium tile image (AgendaEntry)");
                        TryFreeMemory();

                        primaryTileData.WideBackBackgroundImage = aeTile.RenderSpecifiedSize(TileSize.Wide);
                        TraceMemoryUsage("Generated back square tile image (AgendaEntry)");
                        TryFreeMemory();
                    }
                }

                if (generateUpcoming) {
                    bool isBack = today.IsHoliday || entriesRemainingToday.Count > 0;

                    var statistics = await AgendaStatistics.ForDateAsync(
                        DateTime.Today.AddDays(1),
                        new WPStorageProvider()
                    );

                    TraceMemoryUsage("Computed statistics");

                    WPTile upTile = statistics.IsHoliday ? new HolidayTile(statistics.Holiday, statistics.ForDate, isTransparent) : (WPTile) new UpcomingTile(statistics, isTransparent);

                    Uri wideBackgroundImage = upTile.RenderSpecifiedSize(TileSize.Wide);
                    TraceMemoryUsage("Generated upcoming wide tile image");
                    TryFreeMemory();

                    Uri squareBackgroundImage = upTile.RenderSpecifiedSize(TileSize.Square);
                    TraceMemoryUsage("Generated upcoming square tile image");
                    TryFreeMemory();

                    if (isBack) {
                        primaryTileData.WideBackBackgroundImage = wideBackgroundImage;
                        primaryTileData.BackBackgroundImage = squareBackgroundImage;
                    }
                    else {
                        primaryTileData.WideBackgroundImage = wideBackgroundImage;
                        primaryTileData.BackgroundImage = squareBackgroundImage;
                    }
                }
            }
            catch (OutOfMemoryException) {
                Debug.WriteLine("An out of memory exception was caught whilst updating the primary tile.");
            }
            catch (SQLiteException) {
                Debug.WriteLine("LiveTileService: Caught SQLite exception, assuming DB not initialized.");
            }
            finally {
                ShellTile.ActiveTiles.First().Update(primaryTileData);
            }

            Debug.WriteLine("LiveTileService: Finished, peak memory usage " + DeviceStatus.ApplicationPeakMemoryUsage);
        }

        #region ClearPrimaryTile(...)

        /// <summary>
        ///		Returns the primary tile to it's default state.
        /// </summary>
        public void ClearPrimaryTile() {
            ShellTile tile = ShellTile.ActiveTiles.First();

            var tileData = new StandardTileData {
                BackgroundImage = new Uri("appdata:Background.png"),
                Title = "My Study Life",
                BackBackgroundImage = new Uri("/NonExistant.png", UriKind.Relative),
                BackTitle = String.Empty
            };

            tile.Update(tileData);
        }

        #endregion
    }
}
