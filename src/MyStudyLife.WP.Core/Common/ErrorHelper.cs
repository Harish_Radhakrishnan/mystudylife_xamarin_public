﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace MyStudyLife.WP.Common {
	public static class ErrorHelper {
		public static readonly DependencyProperty ErrorsProperty =
			DependencyProperty.RegisterAttached("Errors", typeof(IDictionary<string, string>), typeof(ErrorHelper), new PropertyMetadata(null, PropertyChangedCallback));

		public static IDictionary<string, string> GetErrors(DependencyObject d) {
			return (IDictionary<string, string>)d.GetValue(ErrorsProperty);
		}

		public static void SetErrors(DependencyObject d, IDictionary<string, string> errors) {
			d.SetValue(ErrorsProperty, errors);
		}

		public static readonly DependencyProperty FieldNameProperty =
			DependencyProperty.RegisterAttached("FieldName", typeof(string), typeof(ErrorHelper), new PropertyMetadata(null, PropertyChangedCallback));

		public static string GetFieldName(DependencyObject d) {
			return (string)d.GetValue(FieldNameProperty);
		}

		public static void SetFieldName(DependencyObject d, string fieldName) {
			d.SetValue(FieldNameProperty, fieldName);
		}

		private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			TextBlock t = (TextBlock)d;

			var errors = (IDictionary<string, string>)d.GetValue(ErrorsProperty);
			var fieldName = (string)d.GetValue(FieldNameProperty);

			string errorMessage;

			if (errors != null && fieldName != null && errors.TryGetValue(fieldName, out errorMessage)) {
				t.Text = errorMessage;
				t.Visibility = Visibility.Visible;
			}
			else {
				t.Text = String.Empty;
				t.Visibility = Visibility.Collapsed;
			}
		}
	}
}
