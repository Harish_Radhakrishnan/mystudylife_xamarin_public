﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MyStudyLife.WP.Common {
	public static class NumericInput {
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.RegisterAttached("Value", typeof(int), typeof(NumericInput), new PropertyMetadata(0, ValueOnChanged));

		public static int GetValue(DependencyObject d) {
			return (int)d.GetValue(ValueProperty);
		}

		public static void SetValue(DependencyObject d, int value) {
			d.SetValue(ValueProperty, value);
		}

		public static readonly DependencyProperty IntegerOnlyProperty =
			DependencyProperty.RegisterAttached("IntegerOnly", typeof (bool), typeof (NumericInput), new PropertyMetadata(false, IntegerOnlyOnChanged));
			
		public static bool GetIntegerOnly(DependencyObject d) {
			return (bool) d.GetValue(IntegerOnlyProperty);
		}

		public static void SetIntegerOnly(DependencyObject d, bool value) {
			d.SetValue(IntegerOnlyProperty, value);
		}

		public static readonly DependencyProperty MinValueProperty =
			DependencyProperty.RegisterAttached("MinValue", typeof(int?), typeof(NumericInput), new PropertyMetadata(null, ConstraintValueOnChanged));

		public static int? GetMinValue(DependencyObject d) {
			return (int?)d.GetValue(MinValueProperty);
		}

		public static void SetMinValue(DependencyObject d, int? value) {
			d.SetValue(MinValueProperty, value);
		}

		public static readonly DependencyProperty MaxValueProperty =
			DependencyProperty.RegisterAttached("MaxValue", typeof(int?), typeof(NumericInput), new PropertyMetadata(null));

		public static int? GetMaxValue(DependencyObject d) {
			return (int?)d.GetValue(MaxValueProperty);
		}

		public static void SetMaxValue(DependencyObject d, int? value) {
			d.SetValue(MaxValueProperty, value);
		}

		private static void ValueOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var textBox = (TextBox)d;

			textBox.TextChanged -= OnTextChanged;

			textBox.Text = e.NewValue.ToString();

			textBox.TextChanged += OnTextChanged;
		}

		private static void IntegerOnlyOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			if (GetIntegerOnly(d)) {
				((TextBox) d).KeyDown += OnKeyDown;
			}
			else {
				((TextBox)d).KeyDown -= OnKeyDown;
			}
		}

		private static void ConstraintValueOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((TextBox)d).LostFocus += OnLostFocus;
		}

		private static void OnTextChanged(object sender, TextChangedEventArgs e) {
			int value;

			Int32.TryParse(((TextBox) sender).Text, out value);

			SetValue(((TextBox) sender), value);
		}

		private static void OnKeyDown(object sender, KeyEventArgs e) {
			if (!e.Key.In(Key.D0, Key.D1, Key.D2, Key.D3, Key.D4, Key.D5, Key.D6, Key.D7, Key.D8, Key.D9)) {
				e.Handled = true;
			}
		}

		private static void OnLostFocus(object sender, RoutedEventArgs e) {
			var textBox = (TextBox) sender;
			int value;
			
			Int32.TryParse(textBox.Text, out value);

			var minValue = GetMinValue(textBox);
			var maxValue = GetMaxValue(textBox);

			if (minValue.HasValue && value < minValue.Value) {
				textBox.Text = minValue.Value.ToString();
			}
			else if (maxValue.HasValue && value > maxValue.Value) {
				textBox.Text = maxValue.Value.ToString();
			}
		}
	}
}
