﻿using System;
using System.Windows;
using MyStudyLife.Scheduling;

namespace MyStudyLife.WP.Common {
	public sealed class AgendaEntryTemplateSelector : TemplateSelector {
	    public static readonly DependencyProperty MinutesRemainingProperty = DependencyProperty.Register(
	        "MinutesRemaining", typeof (int), typeof (AgendaEntryTemplateSelector), new PropertyMetadata(0, MinutesRemainingOnChanged));

	    private static void MinutesRemainingOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
	        ((AgendaEntryTemplateSelector) d).ApplyTemplate();
	    }

	    public int MinutesRemaining {
	        get { return (int) GetValue(MinutesRemainingProperty); }
	        set { SetValue(MinutesRemainingProperty, value); }
	    }

		public DataTemplate CurrentTemplate { get; set; }

		public DataTemplate NextTemplate { get; set; }

		public DataTemplate PastTemplate { get; set; }

		public override DataTemplate SelectTemplate(object item, DependencyObject container) {
		    if (item != null) {
		        var ae = (AgendaEntry) item;

		        if (ae.IsCurrent) {
		            return CurrentTemplate;
		        }
		        
                if (ae.IsPast) {
		            return PastTemplate;
		        }
		    }

		    return NextTemplate;
		}
	}
}
