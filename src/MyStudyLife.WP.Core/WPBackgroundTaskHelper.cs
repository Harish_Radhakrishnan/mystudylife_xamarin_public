﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Community.Plugins.Sqlite.WindowsPhone;
using MyStudyLife.Reminders;
using MyStudyLife.Versioning;

namespace MyStudyLife.WP {
    public static class WPBackgroundTaskHelper {
        public static bool CheckAndInitializeIoC() {
            return BackgroundTaskHelper.CheckAndInitializeIoC<WPConfig, MvxWindowsPhoneSQLiteConnectionFactory, WPStorageProvider, DummyReminderScheduler>(
                Mvx.LazyConstructAndRegisterSingleton<IUpgradeService, WPUpgradeService>
            );
        }

        // WP doesn't allow scheduled actions to be accessed in a dll referenced by the background task

// ReSharper disable once ClassNeverInstantiated.Local
        class DummyReminderScheduler : IReminderScheduler {
            public bool ScheduleReminder(Reminder reminder) {
                return false;
            }

            public void ClearReminders() { }
        }
    }
}
