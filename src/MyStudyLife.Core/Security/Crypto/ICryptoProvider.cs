﻿// ReSharper disable IdentifierTypo
namespace MyStudyLife.Security.Crypto {
	/// <summary>
	///		Deals with platform specific implementations
	///		of cryptography libraries such as HMCSHA1.
	/// </summary>
	public interface ICryptoProvider {
// ReSharper restore IdentifierTypo

		/// <summary>
		///		Converts the given <paramref name="content"/>
		///		to a HMACSHA1 hash using the given <paramref name="privateKey"/>.
		/// </summary>
		/// <remarks>
		///		There is no HMACSHA1 implementation in the PCL... hence this.
		/// </remarks>
		byte[] ComputeHMACSHA1(string content, string privateKey);
	}
}
