﻿using System.Collections.Generic;
using MyStudyLife.Data.Settings;
using Newtonsoft.Json;

namespace MyStudyLife.Reminders {
    public class ReminderResponseEnvelope {
        [JsonProperty("device_settings")]
        public DeviceSettings DeviceSettings { get; set; }

        [JsonProperty("reminders")]
        public IEnumerable<Reminder> Reminders { get; set; }
    }
}
