﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Diagnostics;
using MyStudyLife.Scheduling;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Reminders
{
    /// <summary>
    ///     Schedules the next week of reminders
    /// </summary>
    public sealed class ReminderService : IReminderService {
        private readonly IStorageProvider _storageProvider;
        private readonly IUserStore _userStore;
        private readonly IReminderScheduler _reminderScheduler;
        private readonly IAgendaScheduler _agendaScheduler;
        private readonly ITaskRepository _taskRepository;

        public ReminderService(
            IStorageProvider storageProvider,
            IUserStore userStore,
            IReminderScheduler scheduler,
            IAgendaScheduler agendaScheduler,
            ITaskRepository taskRepository
        ) {
            this._storageProvider = storageProvider;
            this._userStore = userStore;
            this._reminderScheduler = scheduler;
            this._agendaScheduler = agendaScheduler;
            this._taskRepository = taskRepository;
        }

        public async Task RunAsync() {
            try {
                // In the future we could also check the Expires property, but that
                // could be a bit of trouble if we change the lifespan on the server and the access
                // token's expiration date on the device is never updated.
                if (AccessToken.GetCurrent(_storageProvider) == null || User.GetCurrent(_userStore) == null) {
                    _reminderScheduler.ClearReminders();

                    return;
                }

                var reminders = await GetRemindersAsync(DateTimeEx.Now, DateTimeEx.Today.AddWeeks(1d));

                _reminderScheduler.ClearReminders();

                foreach (var reminder in reminders) {
                    _reminderScheduler.ScheduleReminder(reminder);
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<ReminderService>>().LogError("ReminderService: Caught exception whilst scheduling reminders:\n" + ex.ToLongString());

                if (Mvx.IoCProvider.TryResolve(out IBugReporter bugReporter)) {
                    bugReporter.Send(ex, handled: true, tags: new List<string> { "ReminderService" });
                }
            }
        }

        private async Task<IEnumerable<Reminder>> GetRemindersAsync(DateTime fromDate, DateTime toDate) {
            var reminders = new List<Reminder>();

            DeviceSettings deviceSettings;

            using (var settingsRepo = new SettingsRepository(this._storageProvider)) {
                deviceSettings = await settingsRepo.GetDeviceSettingsAsync();
            }

            if (!deviceSettings.RemindersEnabled) {
                return reminders;
            }

            IEnumerable<AgendaEntry> entries = null;

            if (deviceSettings.ClassRemindersEnabled || deviceSettings.ExamRemindersEnabled) {
                entries = await _agendaScheduler.GetForDateRangeAsync(fromDate, toDate);

                foreach (var entry in entries) {
                    bool isClass = AgendaEntryType.Class == entry.Type;

                    if ((isClass && !deviceSettings.ClassRemindersEnabled) ||
                        (!isClass && !deviceSettings.ExamRemindersEnabled)) {

                        continue;
                    }

                    int minutesBefore = (isClass ? (int)deviceSettings.ClassReminderTime : (int)deviceSettings.ExamReminderTime);

                    DateTime reminderTime = entry.StartTime.AddMinutes(-minutesBefore);

                    if (reminderTime >= fromDate) {
                        var reminder = new Reminder {
                            Type = isClass ? ReminderType.Class : ReminderType.Exam,
                            Guid = entry.BaseGuid,
                            Time = reminderTime,
                            Title = $"{entry.Title} in {minutesBefore} minutes",
                            Message = entry.Location,
                            Color = entry.Color
                        };

                        if (isClass) {
                            var classTime = ((ScheduledClass)entry.Base).ClassTime;

                            if (classTime != null) {
                                reminder.Extras = new Dictionary<string, string> {
                                    { "TimeGuid", classTime.Guid.ToString() }
                                };
                            }
                        }

                        reminders.Add(reminder);
                    }
                };
            }

            // Only gets task reminders for a single day
            // TODO: Get tasks for multiple days
            if (deviceSettings.TaskRemindersEnabled) {
                // If the time of day has passed, then get tomorrows
                DateTime reminderTime = DateTimeHelper.FromDateAndTime(
                    fromDate.TimeOfDay > deviceSettings.TaskReminderTime ? fromDate.AddDays(1) : fromDate,
                    deviceSettings.TaskReminderTime
                );

                // Get incomplete tasks for the day after the reminder date
                DateTime incompleteTaskDate = reminderTime.AddDays(1).Date;

                var tasks = (await _taskRepository.GetIncompleteByDueDateAsync(null, incompleteTaskDate)).ToList();

                if (tasks.Any()) {
                    var incompleteTasks = new List<Data.Task>();
                    var overdueTasks = new List<Data.Task>();

                    // We do filter out incomplete tasks due "today", in the future
                    // it would be good to have a reminder for that too, but that's
                    // not how the reminder is described to the user currently.
                    foreach (var task in tasks) {
                        // Filter tasks into overdue based on the reminder date,
                        // not the current time.
                        if (task.DueDate < reminderTime.Date) {
                            overdueTasks.Add(task);
                        }
                        // This check is required as the repo method uses
                        // the first arg  to determine whether a task should be
                        // considered overdue. Without this check, tasks due
                        // today (being the reminder date) would be in the
                        // overdue reminder.
                        else if (reminderTime.Date < task.DueDate) {
                            incompleteTasks.Add(task);
                        }
                    }

                    if (incompleteTasks.Any()) {
                        var reminder = new Reminder {
                            Type = ReminderType.Task,
                            Time = reminderTime,
                            Title = StringEx.PluralFormat("{0} incomplete {0:task;tasks} due tomorrow", incompleteTasks.Count)
                        };

                        foreach (var t in incompleteTasks) {
                            reminder.AddLine(t.Subject + ": " + t.Title);
                        }

                        reminders.Add(reminder);
                    }

                    if (overdueTasks.Any()) {
                        var reminder = new Reminder {
                            Type = ReminderType.Task,
                            Time = reminderTime,
                            Title = StringEx.PluralFormat(true, "{0:task;tasks} overdue", overdueTasks.Count)
                        };

                        foreach (var t in overdueTasks) {
                            reminder.AddLine(t.Subject + ": " + t.Title);
                        }

                        reminders.Add(reminder);
                    }
                }
            }

            return reminders;
        }
    }
}
