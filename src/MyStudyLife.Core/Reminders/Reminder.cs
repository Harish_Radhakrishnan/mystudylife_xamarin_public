﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace MyStudyLife.Reminders {
    // Json is used because in Android we serialize the reminder into the pending intent.

    [JsonObject]
    public sealed class Reminder {
        [JsonProperty("message")] private string _message;
        [JsonProperty("lines")] private List<string> _lines;

        [JsonProperty("time")]
        public DateTime Time { get; set; }
        
        [JsonProperty("type")]
	    public ReminderType Type { get; set; }
        
        [JsonProperty("guid", NullValueHandling = NullValueHandling.Ignore)]
        public Guid? Guid { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }
        
        [JsonIgnore]
        public string Message {
            get {
                if (this._message == null && this._lines != null) {
                    return this._lines.Aggregate((x, y) => String.Concat(x, Environment.NewLine, y));
                }
                
                return _message;
            }
            set {
                if (this._lines != null && this._lines.Count > 0) {
                    throw new InvalidOperationException("Cannot explicitly set message for reminder with multiple lines.");
                }
                
                _message = value;
            }
        }

        [JsonIgnore]
        public IEnumerable<string> Lines {
            get { return _message != null || _lines == null ? null : _lines.AsEnumerable(); }
        }

        [JsonIgnore]
        public bool IsMultiline {
            get { return _lines != null && _message == null; }
        }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("extras")]
        public IDictionary<string, string> Extras { get; set; }

        public Reminder AddLine(string message) {
            if (this._message != null) {
                throw new InvalidOperationException("Cannot add lines to an existing message. Set Message property to null.");
            }

            if (this._lines == null) {
                this._lines = new List<string>();
            }

            this._lines.Add(message);

            return this;
        }

        [JsonIgnore]
        public int Id {
            get {
                int id = (int) this.Type;

                id += unchecked((int)this.Time.Ticks);

                return id;
            }
        }

		/// <summary>
		///		Returns a name which can be used to uniquely identify
		///		the reminder.
		/// </summary>
	    public string GetUniqueName() {
			string n = String.Format("{0}_{1}", this.Time.ToString("s"), this.Type.ToString().ToLower());

			if (this.Guid.HasValue) {
				return String.Format("{0}_{1}", n, this.Guid.Value);
			}

		    return n;
		}
    }

    public enum ReminderType {
		Unknown,
        Class,
        Exam,
        Task
    }
}
