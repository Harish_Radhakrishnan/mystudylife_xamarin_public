﻿namespace MyStudyLife.Reminders {
    public interface IReminderScheduler {
        /// <summary>
        ///     Schedules a reminder to be displayed to a user at a future point in time.
        /// </summary>
        /// <returns>
        ///     True if successful. Can be false if reminders are disabled by the device.
        /// </returns>
        bool ScheduleReminder(Reminder reminder);
        
        void ClearReminders();
    }
}