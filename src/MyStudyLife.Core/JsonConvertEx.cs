﻿using System.IO;
using Newtonsoft.Json;

namespace MyStudyLife {
    /// <summary>
    ///     The methods or overloads <see cref="JsonConvert"/>
    ///     is missing.
    /// </summary>
    public static class JsonConvertEx {
        public static T DeserializeObject<T>(Stream stream) {
            using (var sr = new StreamReader(stream))
            using (var jsonReader = new JsonTextReader(sr)) {
                var serializer = JsonSerializer.CreateDefault();

                return serializer.Deserialize<T>(jsonReader);
            }
        }
    }
}
