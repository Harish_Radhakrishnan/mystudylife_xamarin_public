﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;
using MyStudyLife.Net;
using System.Text.RegularExpressions;

namespace MyStudyLife.Extensions {
    // TODO: This should be refactored to be within the bug reporter and in a retry policy for http requests
    // Most parts of the application should never need to call this, so it shouldn't be exposed as a global
    // extension. The functionality may also not be clear to the caller - for example, the sync service should
    // not ignore a 429 or 401 response!
    //
    // TODO: Update message handler for Android which translates Java throwables to WebException, like 
    public static class ExceptionExtensions {
        private static readonly HttpStatusCode[] TransientStatusCodes = {
            HttpStatusCode.NotModified, HttpStatusCode.Conflict, HttpStatusCode.Forbidden, HttpStatusCode.Unauthorized, (HttpStatusCode) 429
        };

        private static readonly string[] AndroidTransientExceptionTypes = {
            "Java.Net.UnknownHostException", "Java.Net.ConnectException"
        };

        /// <summary>
        ///     Indicates whether this exception is likely to be caused by a network failure
        ///     or http response that should not be reported to the bug tracker.
        /// </summary>
        /// <param name="ex">The exception to check.</param>
        public static bool IsTransientWebException(this Exception ex) {
            // Client timed out the request or user canceled
            if (ex is TaskCanceledException || ex is OperationCanceledException) {
                return true;
            }

            HttpApiException apiException = ex as HttpApiException;
            if (apiException != null) {
                return apiException.StatusCode?.In(TransientStatusCodes) ?? false;
            }

            // This handles all iOS and ONE Android error (ConnectFailure)
            WebException webException = ex as WebException ?? (ex.InnerException as WebException);
            if (webException != null) {
                if (
                    ((WebExceptionStatusEx)(int)webException.Status).In(
                        WebExceptionStatusEx.ConnectFailure,
                        WebExceptionStatusEx.SendFailure,
                        WebExceptionStatusEx.ReceiveFailure,
                        WebExceptionStatusEx.Timeout,
                        WebExceptionStatusEx.NameResolutionFailure,
                        WebExceptionStatusEx.ConnectionClosed,
                        WebExceptionStatusEx.RequestCanceled
                    ) || HasTransientMessage(webException)
                ) {
                    return true;
                }
            }

            HttpRequestException httpException = ex as HttpRequestException;
            if (httpException != null) {
                Exception currentException = httpException;
                // Handles SocketException "Socket not connected" on mono 4.8 which can be 5 exceptions deep
                while (currentException != null) {
                    if (HasTransientMessage(currentException)) {
                        return true;
                    }

                    currentException = currentException.InnerException;
                }
            }

            // Android handling
            var exceptionType = GetExceptionType(ex);
            if (exceptionType.In(AndroidTransientExceptionTypes)) {
                return true;
            }

            if (String.Equals(exceptionType, "Java.IO.IOException", StringComparison.OrdinalIgnoreCase)) {
                if (ex.StackTrace == null) return false;

                return new[] {
                    "failed to connect",
                    "unexpected end of stream",
                    "connection closed by peer",
                    "connection reset by peer",
                    "connection timed out",
                    "connection refused",
                    "connection abort",
                    "unable to resolve host",
                    "network is unreachable",
                    "no route to host"
                }.Any(phrase => ex.StackTrace.Contains(phrase, StringComparison.OrdinalIgnoreCase));
            }

            return false;
        }

        private static bool HasTransientMessage(Exception ex) {
            var message = ex.Message;
            if (String.IsNullOrEmpty(message)) return false;

            return new[] {
                "timed out",
                "remote name could not be resolved",
                "underlying connection was closed",
                "unable to connect",
                "could not connect",
                "socket is not connected",
                "network connection was lost",
                "hostname could not be found",
                "connection reset by peer",
                "connection appears to be offline",
                "network is unreachable",
                "network subsystem is down"
            }.Any(phrase => message.Contains(phrase, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        ///     True if it's an SSL issue likely caused by a public hotspot
        ///     forcing it's own SSL cert.
        /// </summary>
        public static bool IsNaughtyHotspotException(this Exception ex) {
            WebException webException = ex as WebException ?? (ex.InnerException as WebException);

            if (webException != null) {
                if ((int)webException.Status == (int)WebExceptionStatusEx.TrustFailure) return true;

                var message = webException.Message;
                if (String.IsNullOrEmpty(message)) return false;

                return new [] {
                    "certificate for this server is invalid",
                    // iOS would throw this for non TLS connections and when we are told to use a
                    // TLS version below 1.2. Since we know MSL supports this, we can assume we're
                    // being forced to use a certificate from a WiFi hotspot.
                    // http://stackoverflow.com/questions/32755674/ios9-getting-error-an-ssl-error-has-occurred-and-a-secure-connection-to-the-ser/32854670
                    "secure connection to the server cannot be made",

                }.Any(phrase => message.Contains(phrase, StringComparison.OrdinalIgnoreCase));
            }

            if (GetExceptionType(ex) == "Java.IO.IOException") {
                return ex.StackTrace != null && Enumerable.Any(new[] {
                    "Trust anchor for certification path not found",
                    "SSLPeerUnverifiedException"
                }, phrase => ex.StackTrace.Contains(phrase, StringComparison.OrdinalIgnoreCase));
            }

            if (ex is HttpRequestException && ex.InnerException is AuthenticationException authEx && authEx.InnerException != null) {
                return ex.Message.Contains("CERTIFICATE_VERIFY_FAILED", StringComparison.OrdinalIgnoreCase);
            }

            return false;
        }

        /// <summary>
        ///     False if the exception should be reported to our bug tracker.
        /// </summary>
        public static bool IsTransientWebOrNaughtyHotspotException(this Exception ex) => IsTransientWebException(ex) || IsNaughtyHotspotException(ex);

        private static string GetExceptionType(Exception ex) {
            var exType = ex.GetType();

            if (exType == typeof(Exception)) {
                var regex = new Regex("^Exception of type '([a-zA-Z.]*)' was thrown");

                var match = regex.Match(ex.Message);

                if (match.Success) {
                    return match.Groups[1].Value;
                }
            }

            return exType.FullName;
        }
    }
}
