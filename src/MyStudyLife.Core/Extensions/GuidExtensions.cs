﻿using System;

// ReSharper disable CheckNamespace
namespace MyStudyLife {
// ReSharper restore CheckNamespace
	public static class GuidExtensions {
		/// <summary>
		///		Checks if the current <see cref="Guid"/>
		///		is equal to <see cref="Guid.Empty"/>.
		/// </summary>
		/// <param name="guid">The guid to check.</param>
		/// <returns>True if empty.</returns>
		public static bool IsEmpty(this Guid guid) {
			return guid == Guid.Empty;
		}
	}
}
