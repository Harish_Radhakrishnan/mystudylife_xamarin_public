﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStudyLife.Extensions {
    public static class EnumEx {
        public static IEnumerable<T> GetValues<T>() {
            return Enum.GetValues(typeof (T)).Cast<T>();
        }

        public static int GetSetFlagCount(long i) {
            int iCount = 0;

            while (i != 0) {
                i = i & (i - 1);

                iCount++;
            }

            return iCount;
        }

        public static int GetFirstFlagIndex<T>(int flags) where T : struct {
            int flagCount = Enum.GetValues(typeof(T)).Length;

            for (int i = 0; i < flagCount; i++) {
                var flag = 1 << i;

                if ((flags & flag) == flag) {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        ///     Converts a short to enum, or null if the given value does not exist in the enum.
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="value">Short to convert in to Enum item</param>
        /// <returns>Enum</returns>
        public static T? FromShort<T>(short? value) where T : struct => (value != null && Enum.IsDefined(typeof(T), value)) ? (T?)(object)value : null;

        /// <summary>
        ///     Converts a short to enum, or default value if the given value does not exist in the enum.
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="value">Short to convert in to Enum item</param>
        /// <param name="defaultValue">Value to return if the short is not present in the enum</param>
        /// <returns>Enum</returns>
        public static T FromShort<T>(short? value, T defaultValue) where T : struct => FromShort<T>(value) ?? defaultValue;
    }
}
