﻿using System;

namespace MyStudyLife.Extensions {
    public static class VersionExtensions {
        /// <summary>
        ///     Retruns a string representation of the current
        ///     version in the format Major.Minor.Build if the
        ///     Build is available, otherwise using Major.Minor.
        /// </summary>
        public static string ToHumanString(this Version v) => v.ToString(v.Build > -1 ? 3 : 2);
    }
}
