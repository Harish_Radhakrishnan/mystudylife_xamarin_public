﻿using System;
using System.Linq;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace MyStudyLife {
    public static class ReflectionExtensions {
        public static T Clone<T>(this T source) 
            where T : class {

            if (source == null) return null;

            var clone = Activator.CreateInstance<T>();

            source.CopyPropertyValuesTo(clone);

            return clone;
        }

        public static void CopyPropertyValuesTo<T>(this T source, T destination) {
            var tType = typeof (T);
            var srcProperties = tType.GetRuntimeProperties();
            var destProperties = typeof(T).GetRuntimeProperties().ToList();

            foreach (var sourceProperty in srcProperties) {
                PropertyInfo property = sourceProperty;

                foreach (var destProperty in destProperties.Where(destProperty => destProperty.Name == property.Name &&
                                                                                  destProperty.PropertyType.GetTypeInfo().IsAssignableFrom(property.PropertyType.GetTypeInfo()) &&
                                                                                  destProperty.CanWrite)) {
                    // Try catch as WP7 cannot write to properties with private setters. (Quick hack).

                    try {
                        destProperty.SetValue(destination, sourceProperty.GetValue(
                            source, new object[] { }), new object[] { });
                    }
                    // ReSharper disable EmptyGeneralCatchClause
                    catch
                        // ReSharper restore EmptyGeneralCatchClause
                    {
                    }
                }
            }
        }

        public static void SetProperty(this Type type, object instance, string name, object value) {
            var setMethod = type.GetRuntimeProperty(name).SetMethod;

            setMethod.Invoke(instance, new[] { value });
        }
    }
}