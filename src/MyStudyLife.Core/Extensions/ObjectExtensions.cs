﻿using System;
// ReSharper disable CheckNamespace


namespace MyStudyLife {
// ReSharper restore CheckNamespace
    public static class ObjectExtensions {
        public static bool In<T>(this T obj, params T[] args) {
// ReSharper disable once CompareNonConstrainedGenericWithNull
            if (obj == null) {
                throw new ArgumentNullException(nameof(obj), "Cannot check for null.");
            }

            if (args == null || args.Length == 0)
                return false;

            return args.Any(arg => obj.Equals(arg));
        }
    }
}
