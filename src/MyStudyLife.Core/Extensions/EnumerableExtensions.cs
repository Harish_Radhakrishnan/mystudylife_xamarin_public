﻿using MyStudyLife.Sync;
using System.Collections.Generic;
using System.Linq;

namespace MyStudyLife.Extensions {
    public static class EnumerableExtensions {
        /// <summary>
        ///     Filters a sequence of <see cref="ISyncable"/> to only
        ///     those of which are not marked as deleted.
        /// </summary>
        public static IEnumerable<T> ExcludeDeleted<T>(this IEnumerable<T> enumerable) where T : ISyncable
            => enumerable.Where(x => x.DeletedAt == null);
    }
}
