﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace MyStudyLife.Extensions {
    public static class TaskExtensions {
        /// <summary>
        ///     Configures an awaiter not to attempt to marshal the continuation
        ///     back to the original context captured.
        /// </summary>
        [DebuggerStepThrough]
        public static ConfiguredTaskAwaitable AnyContext(this Task task)
            => task.ConfigureAwait(continueOnCapturedContext: false);

        /// <summary>
        ///     Configures an awaiter not to attempt to marshal the continuation
        ///     back to the original context captured.
        /// </summary>
        [DebuggerStepThrough]
        public static ConfiguredTaskAwaitable<TResult> AnyContext<TResult>(this Task<TResult> task)
            => task.ConfigureAwait(continueOnCapturedContext: false);

        [DebuggerStepThrough]
        public static TResult GetResult<TResult>(this Task<TResult> task) {
            try {
                return task.AnyContext().GetAwaiter().GetResult();
            }
            catch (AggregateException ex) when (ex.InnerExceptions.Count == 1) {
                throw ex.InnerExceptions[0];
            }
        }
    }
}
