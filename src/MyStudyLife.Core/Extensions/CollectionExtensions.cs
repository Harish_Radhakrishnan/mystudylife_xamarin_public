﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MyStudyLife.Data;

// ReSharper disable once CheckNamespace
namespace MyStudyLife {
	public static class CollectionExtensions {
		/// <summary>
		///     Checks if there are any elements in the given
		///     <paramref name="collection" /> by using it's
		///     <code>Count</code> property preventing an 
		///     unnecessary enumeration.
		/// </summary>
		public static bool Any<T>(this ICollection<T> collection, Func<T, bool> predicate = null) {
			if (predicate == null)
				return collection.Count > 0;

			return Enumerable.Any(collection, predicate);
		}

		/// <summary>
		/// Adds the given <see cref="items"/> to the collection.
		/// </summary>
		/// <typeparam name="T">The type stored in the collection.</typeparam>
		/// <param name="collection"></param>
		/// <param name="items">The elements to be added to the collection.</param>
		public static void AddRange<T>(this ICollection<T> collection, IEnumerable<T> items) {
			foreach (T item in items)
				collection.Add(item);
		}

		/// <summary>
		/// Removes the given <see cref="items"/> from the collection.
		/// </summary>
		/// <typeparam name="T">The type stored in the collection.</typeparam>
		/// <param name="collection"></param>
		/// <param name="items">The elements to be removed from the collection.</param>
		public static void RemoveRange<T>(this ICollection<T> collection, IEnumerable<T> items) {
			foreach (T item in items)
				collection.Remove(item);
		}

		/// <summary>
		/// Removes a range of items from the given predicate.
		/// </summary>
		/// <typeparam name="T">The type stored in the collection.</typeparam>
		/// <param name="collection"></param>
		/// <param name="predicate">A function to test each element for a condition.</param>
		public static void RemoveAll<T>(this ICollection<T> collection, Func<T, bool> predicate) {
			List<T> itemsToRemove = collection.Where(predicate).ToList();

			foreach (T item in itemsToRemove)
				collection.Remove(item);
		}

        /// <summary>
        ///     Removes using the item's id rather than
        ///     reference.
        /// </summary>
        public static void RemoveById<T>(this ICollection<T> collection, T item) where T : class, IDbRow {
            var itemToRemove = collection.SingleOrDefault(x => x.Guid == item.Guid);

            if (itemToRemove != null) {
                collection.Remove(itemToRemove);
            }
	    }

		/// <summary>
		/// Adds a collection of items to a collection where the item does not already exist.
		/// </summary>
		/// <typeparam name="T">The type stored in the collection.</typeparam>
		/// <param name="collection"></param>
		/// <param name="items">The collection to synchronize.</param>
		/// <param name="removeItems">True to remove items which do not exist in the <paramref name="items"/> collection from the collection.</param>
		public static void AddWhereNotExists<T>(this ICollection<T> collection, IEnumerable<T> items, bool removeItems = true) {
			List<T> itemsToRemove = removeItems ? collection.Where(t => !items.Any(_t => _t.Equals(t))).ToList() : null;
			List<T> itemsToAdd = items.Where(t => !collection.Any(_t => _t.Equals(t))).ToList();

			if (removeItems)
				collection.RemoveRange(itemsToRemove);

			collection.AddRange(itemsToAdd);
		}

        /// <summary>
        /// Adds an item to a collection where the item does not already exist.
        /// </summary>
        /// <typeparam name="T">The type stored in the collection.</typeparam>
        /// <param name="collection"></param>
        /// <param name="item">The item to synchronize.</param>
        public static void AddWhereNotExists<T>(this ICollection<T> collection, T item) {
            if (!collection.Contains(item)) {
                collection.Add(item);
            }
        }

        /// <summary>
        /// Inserts an item in a collection where it doesn't exist at any position.
        /// </summary>
        /// <typeparam name="T">The type stored in the collection.</typeparam>
        /// <param name="collection"></param>
        /// <param name="item">The item to synchronize.</param>
        public static void InsertWhereNotExists<T>(this IList<T> collection, T item, int position) {
            if (!collection.Contains(item)) {
                collection.Insert(position, item);
            }
        }

        /// <summary>
        ///		Runs the given <paramref name="action"/>
        ///		against every item in the current collection.
        /// </summary>
        /// <param name="collection">The current collection.</param>
        /// <param name="action">
        ///		The action to run. An example would be setting a property
        ///		on every item in the collection.
        /// </param>
        public static void Apply<T>(this IEnumerable<T> collection, Action<T> action) {
			foreach (var item in collection) {
				action(item);
			}
		}

		/// <summary>
		///		Checks whether any of the given <paramref name="items"/>
		///		exist in the current collection.
		/// </summary>
		/// <remarks>
		///		Shorthand for <c> collection.Any(x => x.In(items))</c>
		/// </remarks>
		/// <param name="collection">The current collection.</param>
		/// <param name="items">The items to check whether they exist in the collection.</param>
		public static bool Contains<T>(this IEnumerable<T> collection, params T[] items) {
			return collection.Any(x => x.In(items));
		}

        /// <summary>
        ///     Returns the index of the first element in the current
        ///     collection which matches the given <param name="item" />.
        /// 
        ///     Returns -1 if no match is found.
        /// </summary>
        public static int FirstIndexOf<T>(this IEnumerable<T> collection, T item)
            => FirstIndexOf(collection, x => x.Equals(item));

        /// <summary>
        ///     Returns the index of the first element in the current
        ///     collection which matches the given <paramref name="predicate"/>.
        /// 
        ///     Returns -1 if no match is found.
        /// </summary>
	    public static int FirstIndexOf<T>(this IEnumerable<T> collection, Func<T, bool> predicate) {
            int index = -1;

            foreach (var item in collection) {
                index++;

	            if (predicate(item)) {
	                break;
	            }
            }

            return index;
        }

        /// <summary>
        ///     Returns the item at the given <paramref name="index"/> or null
        ///     if it is out of range.
        /// </summary>
        public static T ElementAtOrDefault<T>(this IEnumerable<T> collection, int index) {
            if (collection == null || index < 0) {
                return default(T);
            }

            var list = collection as IList;
            if (list != null) {
                return index < list.Count ? (T)list[index] : default(T);
            }

            int i = 0;
            foreach (var item in collection) {
                if (i == index) return item;
                i++;
            }

            return default(T);
        }

	    public static void Sort<T>(this ObservableCollection<T> collection) where T : IComparable<T> {
	        if (collection == null) {
	            throw new ArgumentNullException(nameof(collection));
	        }

            // Selection sort algorithm from http://stackoverflow.com/a/14371246/491468
            for (var startIndex = 0; startIndex < collection.Count - 1; startIndex += 1) {
                var indexOfSmallestItem = startIndex;

                for (var i = startIndex + 1; i < collection.Count; i += 1) {
                    if (collection[i].CompareTo(collection[indexOfSmallestItem]) < 0) {
                        indexOfSmallestItem = i;
                    }
                }

                if (indexOfSmallestItem != startIndex) {
                    collection.Move(indexOfSmallestItem, startIndex);
                }
            }
	    }
	}
}
