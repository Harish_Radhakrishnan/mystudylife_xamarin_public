﻿using System;
using System.Globalization;
using MyStudyLife.Data;
using MyStudyLife.Globalization;

// ReSharper disable once CheckNamespace
namespace MyStudyLife {
    public static class DateTimeExtensions {
        #region Day

        public static DateTime StartOfDay(this DateTime d) {
            return d.Date;
        }

        public static DateTime EndOfDay(this DateTime d) {
            return new DateTime(d.Year, d.Month, d.Day, 23, 59, 59);
        }

        #endregion

        #region Week

        public static DateTime AddWeeks(this DateTime date, double weeks) {
            return date.AddDays(weeks * 7);
        }

        private static DateTime SetWeekday(DateTime date, int dow) {
            var weekday = (date.DayOfWeek + 7 - CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek) % 7;
            return date.AddDays(dow - weekday);
        }

        public static DateTime StartOfWeek(this DateTime date) {
            return SetWeekday(date, 0);
        }

        public static DateTime EndOfWeek(this DateTime date) {
            return SetWeekday(date, 6);
        }

        #endregion

        #region Month

        public static DateTime StartOfMonth(this DateTime d) {
            return new DateTime(d.Year, d.Month, 1).Date;
        }

        public static DateTime EndOfMonth(this DateTime d) {
            return new DateTime(d.Year, d.Month, CultureInfo.CurrentCulture.Calendar.GetDaysInMonth(d.Year, d.Month)).Date;
        }

        #endregion

        #region IsSame

        /// <summary>
        ///     Compares the two given DateTimes based on the given
        ///     comparison.
        ///
        ///     The comparison also checks each "higher level" property.
        ///     For example if you are using <code>DateTimeComparison.Month</code>
        ///     the year will also be compared.
        /// </summary>
        public static bool IsSame(this DateTime a, DateTime b, DateTimeComparison comparison = DateTimeComparison.Day) {

            if (a.Year != b.Year) return false;
            if (comparison == DateTimeComparison.Year) return true;

            if (a.Month != b.Month) return false;
            if (comparison == DateTimeComparison.Month) return true;

            if (a.Day != b.Day) return false;
            if (comparison == DateTimeComparison.Day) return true;

            if (a.Hour != b.Hour) return false;
            if (comparison == DateTimeComparison.Hour) return true;

            if (a.Minute != b.Minute) return false;
            if (comparison == DateTimeComparison.Minute) return true;

            return a.Second == b.Second;
        }

        #endregion

        #region IsCurrent

        /// <summary>
        ///     Compares the given <see cref="DateTime"/> to the current date and
        ///		time based on the given comparison.
        ///
        ///     The comparison also checks each "higher level" property.
        ///     For example if you are using <code>DateTimeComparison.Month</code>
        ///     the year will also be compared.
        /// </summary>
        public static bool IsCurrent(this DateTime a, DateTimeComparison comparison) {
            return IsSame(a, DateTimeEx.Now, comparison);
        }

        #endregion

        #region IsTomorrow

        /// <summary>
        ///     Determines whether the given DateTime is the same as
        ///     <c>DateTime.Today</c>.
        /// </summary>
        public static bool IsToday(this DateTime a) {
            return a.Date == DateTimeEx.Today;
        }

        #endregion

        #region IsTomorrow

        /// <summary>
        ///     Determines whether the given DateTime represents
        ///     the day after the current date (using <c>DateTime.Today</c>).
        /// </summary>
        public static bool IsTomorrow(this DateTime a) {
            return IsSame(a, DateTimeEx.Today.AddDays(1.0), DateTimeComparison.Day);
        }

        #endregion

        #region IsYesterday

        /// <summary>
        ///     Determines whether the given DateTime represents
        ///     the day before the current date (using <c>DateTime.Today</c>).
        /// </summary>
        public static bool IsYesterday(this DateTime a) {
            return IsSame(a, DateTimeEx.Today.AddDays(-1.0), DateTimeComparison.Day);
        }

        #endregion

        #region IsCurrentWeek

        /// <summary>
        ///     Determines whether the given DateTime is
        ///     for the current week.
        /// </summary>
        public static bool IsCurrentWeek(this DateTime a) {
            return DateTimeEx.Today.GetWeekOfYear() == a.GetWeekOfYear();
        }

        #endregion

        #region IsPast

        /// <summary>
        ///     Determines whether the given DateTime represents
        ///     a day in the past (using <c>DateTime.Today</c>).
        /// </summary>
        public static bool IsPast(this DateTime a) {
            return a.Date < DateTimeEx.Today;
        }

        #endregion

        #region IsBefore

        /// <summary>
        ///     Returns true if the current <see cref="DateTime"/> represents
        ///     a point in time that occurs BEFORE the point in time represented
        ///     by <paramref name="b"/>.
        ///
        ///     The comparison also checks each "higher level" property.
        ///     For example if you are using <code>DateTimeComparison.Month</code>
        ///     the year will also be compared.
        /// </summary>
        public static bool IsBefore(this DateTime a, DateTime b, DateTimeComparison comparison = DateTimeComparison.Day) {

            if (a.Year < b.Year) return true;
            if (a.Year > b.Year || comparison == DateTimeComparison.Year) return false;

            if (a.Month < b.Month) return true;
            if (a.Month > b.Month || comparison == DateTimeComparison.Month) return false;

            if (a.Day < b.Day) return true;
            if (a.Day > b.Day || comparison == DateTimeComparison.Day) return false;

            if (a.Hour < b.Hour) return true;
            if (a.Hour > b.Hour || comparison == DateTimeComparison.Hour) return false;

            if (a.Minute < b.Minute) return true;
            if (a.Minute > b.Minute || comparison == DateTimeComparison.Minute) return false;

            return a.Second < b.Second;
        }

        #endregion

        #region IsAfter

        /// <summary>
        ///     Returns true if the current <see cref="DateTime"/> represents
        ///     a point in time that occurs AFTER the point in time represented
        ///     by <paramref name="b"/>.
        ///
        ///     The comparison also checks each "higher level" property.
        ///     For example if you are using <code>DateTimeComparison.Month</code>
        ///     the year will also be compared.
        /// </summary>
        public static bool IsAfter(this DateTime a, DateTime b, DateTimeComparison comparison = DateTimeComparison.Day) {

            if (a.Year > b.Year) return true;
            if (a.Year < b.Year || comparison == DateTimeComparison.Year) return false;

            if (a.Month > b.Month) return true;
            if (a.Month < b.Month || comparison == DateTimeComparison.Month) return false;

            if (a.Day > b.Day) return true;
            if (a.Day < b.Day || comparison == DateTimeComparison.Day) return false;

            if (a.Hour > b.Hour) return true;
            if (a.Hour < b.Hour || comparison == DateTimeComparison.Hour) return false;

            if (a.Minute > b.Minute) return true;
            if (a.Minute < b.Minute || comparison == DateTimeComparison.Minute) return false;

            return a.Second > b.Second;
        }

        #endregion

        #region IsBetween

        public static bool IsBetween(this DateTime a, DateTime startDate, DateTime endDate, DateTimeComparison comparison = DateTimeComparison.Day) {

			double startDiff = (a - startDate).TotalSeconds;
	        double endDiff = (endDate - a).TotalSeconds;

	        if (comparison == DateTimeComparison.Second) {
		        return 0 <= startDiff && 0 <= endDiff;
	        }

			// Minute
	        startDiff = Math.Round(startDiff/60);
	        endDiff = Math.Round(endDiff/60);

			if (comparison == DateTimeComparison.Minute) {
				return 0 <= startDiff && 0 <= endDiff;
			}

			// Hour
			startDiff = Math.Round(startDiff / 60);
			endDiff = Math.Round(endDiff / 60);

			if (comparison == DateTimeComparison.Hour) {
				return 0 <= startDiff && 0 <= endDiff;
			}

			// Day
			startDiff = Math.Round(startDiff / 24);
			endDiff = Math.Round(endDiff / 24);

			if (comparison == DateTimeComparison.Day) {
				return 0 <= startDiff && 0 <= endDiff;
			}

			throw new NotSupportedException("Cannot diff DateTimeComparison.Month or DateTimeComparison.Year");
        }

        #endregion

        #region GetClosest

        public static DateTime GetClosest(this DateTime a, DayOfWeek day) {
            int diff = day - a.DayOfWeek;

            if (diff == 0) {
                return a;
            }

            return a.AddDays(diff);
        }

        /// <summary>
        ///     Determines the closest day to the current instance that is a day of week
        ///     in the given <paramref name="days"/>.
        /// </summary>
        /// <param name="a">
        ///     The <see cref="DateTime"/> instance.
        /// </param>
        /// <param name="days">
        ///     The valid days of week.
        /// </param>
        /// <param name="tense">
        ///     Use to restrict the date returned to either past or future.
        /// </param>
        /// <returns>
        ///     The closest date, or the current one if it exists in given <paramref name="days"/>.
        /// </returns>
        public static DateTime GetClosestInDays(this DateTime a, DaysOfWeek days, Tense tense = Tense.Any) {
            if (days.HasFlag(a.DayOfWeek.ToFlag())) {
                return a;
            }

            bool past = tense.HasFlag(Tense.Past),
                 future = tense.HasFlag(Tense.Future);

            int startDoW = (int)a.DayOfWeek;

            for (int i = 1; i <= 6; i++) {
                if (future) {
                    var nextDoW = (DaysOfWeek)(1 << ((startDoW + i) % 7));
                    if (days.HasFlag(nextDoW)) {
                        return a.AddDays(i);
                    }
                }

                if (past) {
                    var prevDoW = (DaysOfWeek)(1 << ((startDoW - i) % 7));
                    if (days.HasFlag(prevDoW)) {
                        return a.AddDays(-i);
                    }
                }
            }

            return a;
        }

        public static DateTime GetClosestWeekday(this DateTime a, Tense tense = Tense.Any) {
            return GetClosestInDays(a, DaysOfWeek.Weekdays, tense);
        }

        #endregion

        #region JSON

        /// <summary>
        ///     Converts the given <paramref name="date"/>
        ///     to a JSON compliant string.
        /// </summary>
        public static string ToJson(this DateTime date) {
            return date.ToString("yyyy-MM-ddTHH:mm:ss");
        }

        /// <summary>
        ///     Converts the given <paramref name="date"/>
        ///     to a JSON compliant string.
        /// </summary>
        public static string ToJson(this DateTime? date) {
            return date.HasValue && date.Value != default(DateTime) ? date.Value.ToJson() : String.Empty;
        }

        #endregion

        #region Range

        public static DateTimeRange Range(this DateTime a, DateTime b) {
            return new DateTimeRange(a, b);
        }

        public static DateTimeRange RangeFromDays(this DateTime a, double days) {
            return new DateTimeRange(a, a.AddDays(days));
        }

        public static DateTimeRange RangeFromWeeks(this DateTime a, double weeks) {
            return new DateTimeRange(a, a.AddWeeks(weeks));
        }

        public static DateTimeRange RangeFromMonths(this DateTime a, int months) {
            return new DateTimeRange(a, a.AddMonths(months));
        }

        public static DateTimeRange RangeFromYears(this DateTime a, int years) {
            return new DateTimeRange(a, a.AddMonths(years));
        }

        #endregion
    }

    public enum DateTimeComparison {
        Year,
        Month,
        Day,
        Hour,
        Minute,
        Second
    }

    public static class TimeSpanExtensions {
        /// <summary>
        ///     Converts the given TimeSpan to a date time
        ///     using the Unix epoch for the year, month and day.
        /// </summary>
        public static DateTime ToDateTime(this TimeSpan ts) {
            return ts.ToDateTime(new DateTime(1970, 1, 1));
        }

        /// <summary>
        ///     Converts the given TimeSpan to a date time
        ///     using the given DateTime for the year, month and day.
        /// </summary>
        public static DateTime ToDateTime(this TimeSpan ts, DateTime d) {
            if (ts.TotalDays >= 1) {
                throw new ArgumentOutOfRangeException(nameof(ts), "The given TimeSpan cannot represent a duration greater than a day.");
            }

            return DateTimeHelper.FromDateAndTime(d, ts);
        }
    }

    public static class DateTimeHelper {
        /// <summary>
        ///     Creates a new date using the given <paramref name="date"/>
        ///     for the date (ie. a day) and the given <paramref name="time"/>
        ///     as the time (eg. 09:00).
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="time">The time.</param>
        /// <returns>A newly formed date time.</returns>
        public static DateTime FromDateAndTime(DateTime date, TimeSpan time) {
            return new DateTime(date.Year, date.Month, date.Day, time.Hours, time.Minutes, time.Seconds);
        }
    }

    public struct DateTimeRange {
        private DateTime _a, _b;

        /// <value>
        ///     The number of days which the date range
        ///     occurs on.
        /// </value>
        public int CalendarDays {
            get { return ((int) (_b.Date - _a.Date).TotalDays) + 1; }
        }

        /// <value>
        ///     The number of months which the date range
        ///     occurs in.
        /// </value>
        public int CalendarMonths {
            get { return ((_b.Month - _a.Month) + ((_b.Year - _a.Year) * 12)) + 1; }
        }

        /// <value>
        ///     The number of years which the date range
        ///     occurs in.
        /// </value>
        public int CalendarYears {
            get { return (_b.Year - _a.Year) + 1; }
        }

        public DateTimeRange(DateTime a, DateTime b) {
            if (a > b) {
                this._a = b;
                this._b = a;
            }
            else {
                this._a = a;
                this._b = b;
            }
        }

        public bool OccursIn(DateTime a) {
            return a.IsBetween(_a, _b);
        }

        public bool Intercepts(DateTimeRange b) {
            var a = this;

            return Intercepts(a._a, a._b, b._a, b._b);
        }

        public override string ToString() => $"{_a:d} - {_b:d}";

        public string ToString(string dateFormat) => $"{_a.ToString(dateFormat)} - {_b.ToString(dateFormat)}";

        // Provides a method that can be used without creating a DateTimeRange.
        public static bool Intercepts(DateTime startTime1, DateTime endTime1, DateTime startTime2, DateTime endTime2) {
            return (
                (startTime1 <= startTime2 && startTime2 <= endTime1) ||
                (startTime2 <= startTime1 && endTime1 <= endTime2) || (
                    endTime2 <= endTime1 && (
                        startTime1 <= startTime2 ||
                        startTime1 <= endTime2
                    )
                )
            );
        }
    }
}
