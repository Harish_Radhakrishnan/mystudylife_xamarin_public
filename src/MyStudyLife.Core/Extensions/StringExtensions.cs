﻿using System;
using System.Diagnostics;
using System.Text;

// ReSharper disable once CheckNamespace
namespace MyStudyLife {
    public static class StringEx {
        /// <summary>
        /// Returns a value indicating whether the given string occurs within this string.
        /// </summary>
        /// <param name="s">The string to search.</param>
        /// <param name="value">The string to find.</param>
        /// <param name="comparison">The string comparison type.</param>
        [DebuggerStepThrough]
        public static bool Contains(this string s, string value, StringComparison comparison) {
            return (s ?? String.Empty).IndexOf((value ?? String.Empty), comparison) >= 0;
        }

        /// <summary>
        ///    Replaces the format item in a specified string with the string representation
        ///    of a corresponding object in a specified array, using pluralisation
        ///    where the format <c>{x:item;items}</c> is used.
        /// </summary>
        /// <param name="format">
        ///     A composite format string.
        /// </param>
        /// <param name="args">
        ///     An object array that contains zero or more objects to format.
        /// </param>
        public static string PluralFormat(string format, params object[] args)
            => String.Format(new PluralFormatProvider(), format, args);

        /// <summary>
        ///    Replaces the format item in a specified string with the string representation
        ///    of a corresponding object in a specified array, using pluralisation and
        ///    optionally concatenating the value where the format <c>{x:item;items}</c> is used.
        /// </summary>
        /// <param name="concatValue">
        ///     True to concatenate the value when replacing a plural format item.
        /// </param>
        /// <param name="format">
        ///     A composite format string.
        /// </param>
        /// <param name="args">
        ///     An object array that contains zero or more objects to format.
        /// </param>
        public static string PluralFormat(bool concatValue, string format, params object[] args)
            => String.Format(new PluralFormatProvider(concatValue), format, args);

        /// <summary>
        /// Converts a string to title case.
        /// </summary>
        /// <param name="s">
        ///     The string to convert
        /// </param>
        [DebuggerStepThrough]
        public static string ToTitleCase(this string s) {
            if (s == null)
                return null;
            if (s.Length == 0)
                return s;

            StringBuilder result = new StringBuilder(s);
            result[0] = char.ToUpper(result[0]);

            for (int i = 1; i < result.Length; ++i) {
                if (char.IsWhiteSpace(result[i - 1]))
                    result[i] = char.ToUpper(result[i]);
                //else
                //    result[i] = char.ToLower(result[i]);

                // Quick hack to avoid something like "Alegbra VII" into "Algebra Vii"
            }

            return result.ToString();
        }

        /// <summary>
        ///     Converts the current string to be sentence case.
        /// </summary>
        /// <param name="s">
        ///     The string to convert.
        /// </param>
        /// <param name="toLower">
        ///     Initially calls <see cref="String.ToLower()"/> before
        ///     converting to sentence case. This may have unwanted side
        ///     effects, if names  are present in the string etc.
        /// </param>
        [DebuggerStepThrough]
        public static string ToSentenceCase(this string s, bool toLower = false) {
            if (string.IsNullOrEmpty(s)) {
                return s;
            }

            if (toLower) {
                s = s.ToLower();
            }

            return char.ToUpper(s[0]) + s.Substring(1);
        }

        /// <summary>
        ///     <see cref="String.Format"/> but an extension method.
        /// </summary>
        [DebuggerStepThrough]
        public static string WithArgs(this string s, params object[] args) {
            return String.Format(s, args);
        }
    }
}
