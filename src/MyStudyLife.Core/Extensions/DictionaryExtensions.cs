﻿using System.Collections.Generic;

namespace MyStudyLife.Extensions {
    public static class DictionaryExtensions {
        public static TValue GetOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key) where TValue : new() {
            TValue value;
            if (!dict.TryGetValue(key, out value)) {
                dict[key] = value = new TValue();
            }
            return value;
        }
    }
}
