﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStudyLife.Extensions {
    public static class LinqExtensions {
        /// <summary>
        ///     Projects each element of a sequence into a new form, filtering the new form to non-null elements.
        /// </summary>
        public static IEnumerable<TResult> SelectNonNull<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
            => source.Select(selector).Where(x => x != null);
        
        /// <summary>
        ///     Projects each element of a sequence into a new form, filtering the new form to non-null elements.
        /// </summary>
        public static IEnumerable<TResult> SelectNonNull<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult?> selector) where TResult : struct
            => source.Select(selector).Where(x => x.HasValue).Select(x => x.Value);

        /// <summary>
        ///     Projects each element of a sequence into a new form, filtering the new form to non-null elements.
        /// </summary>
        public static IEnumerable<TResult> SelectNonNull<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
            => source.Select(selector).Where(x => x != null);

        /// <summary>
        ///     Projects each element of a sequence into a new form, filtering the new form to non-null elements.
        /// </summary>
        public static IEnumerable<TResult> SelectNonNull<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult?> selector) where TResult : struct
            => source.Select(selector).Where(x => x.HasValue).Select(x => x.Value);

        /// <summary>
        ///     Returns the index of the first element in the current
        ///     collection which matches the given <paramref name="predicate"/> or -1 if no match is found.
        /// </summary>
        public static int FirstIndexOf<T>(this IEnumerable<T> collection, Func<T, bool> predicate) {
            int index = -1;

            foreach (var item in collection) {
                index++;

                if (predicate(item)) {
                    break;
                }
            }

            return index;
        }
    }
}
