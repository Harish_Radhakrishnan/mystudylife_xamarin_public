﻿using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;

namespace MyStudyLife.Messaging {
    public class EntityEventMessage<T> : MvxMessage {
        public T Entity { get; private set; }

        public ItemAction Action { get; private set; }

        public EntityEventMessage(object sender, ItemAction action)
            : base(sender) {

            this.Action = action;
        }

        public EntityEventMessage(object sender, T entity, ItemAction action)
            : base(sender) {

            this.Entity = entity;
            this.Action = action;
        }
    }
}
