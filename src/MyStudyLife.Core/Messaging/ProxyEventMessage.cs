﻿using System;
using MvvmCross.Plugin.Messenger;

namespace MyStudyLife.Messaging {
    /// <summary>
    ///     An <see cref="MvxMessage"/> used as a proxy for
    ///     view models.
    /// </summary>
    /// <remarks>
    ///     If we reference a service event (such as SyncService.Completed)
    ///     from the view model, it will never get GC'd, hence this!
    /// </remarks>
    public class ProxyEventMessage<TArgs> : MvxMessage where TArgs : EventArgs {
        public TArgs Args { get; private set; }

        public ProxyEventMessage(object sender, TArgs args) : base(sender) {
            this.Args = args;
        }
    }

    // This is really just syntactical sugar
    // (constructors have to specify the type, methods like this you don't).
    public static class ProxyEventMessage {
        public static ProxyEventMessage<TArgs> For<TArgs>(object sender, TArgs args) where TArgs : EventArgs {
            return new ProxyEventMessage<TArgs>(sender, args);
        }
    }
}
