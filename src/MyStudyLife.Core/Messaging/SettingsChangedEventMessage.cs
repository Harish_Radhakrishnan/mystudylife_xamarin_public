﻿using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data.Settings;

namespace MyStudyLife.Messaging {
    public abstract class SettingsChangedEventMessage<TSettings> : MvxMessage {
        public TSettings Settings { get; private set; }

        protected SettingsChangedEventMessage(object sender, TSettings settings) : base(sender) {
            this.Settings = settings;
        }
    }

    public class UserSettingsChangedEventMessage : SettingsChangedEventMessage<UserSettings> {
        public bool FirstDayChanged {
            get { return true; }
        }

        public UserSettingsChangedEventMessage(object sender, UserSettings settings)
            : base(sender, settings) { }
    }

    public class DeviceSettingsChangedEventMessage : SettingsChangedEventMessage<DeviceSettings> {
        public DeviceSettingsChangedEventMessage(object sender, DeviceSettings settings)
            : base(sender, settings) { }
    }

    public class TimetableSettingsChangedEventMessage : SettingsChangedEventMessage<TimetableSettings> {
        public TimetableSettingsChangedEventMessage(object sender, TimetableSettings settings)
            : base(sender, settings) { }
    }
}
