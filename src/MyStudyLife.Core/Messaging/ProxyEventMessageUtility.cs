﻿using MvvmCross.Plugin.Messenger;
using System;
using System.Reflection;

namespace MyStudyLife.Messaging {
    public static class ProxyEventMessageUtility {
        /// <summary>
        ///     Publishes events raised from the source using a <see cref="ProxyEventMessage{TArgs}"/>.
        /// </summary>
        /// <typeparam name="TEventArgs">
        ///     The type of the event data generated by the source event.
        /// </typeparam>
        /// <param name="messenger">
        ///     The <see cref="IMvxMessenger"/> to publish the event message to.
        /// </param>
        /// <param name="source">
        ///     The source object.
        /// </param>
        /// <param name="eventName">
        ///     The name of the event to proxy.
        /// </param>
        public static void ProxyEvent<TEventArgs>(this IMvxMessenger messenger, object source, string eventName) where TEventArgs : EventArgs {
            Check.NotNull(source, nameof(source));
            Check.NotNull(eventName, nameof(eventName));

            var eventInfo = source.GetType().GetEvent(eventName);

            if (eventInfo == null) {
                throw new ArgumentException($"Event with name {eventName} does not exist on the source type", nameof(eventName));
            }

            ProxyEvent<TEventArgs>(messenger, source, eventInfo);
        }

        /// <summary>
        ///     Publishes events raised from the source using a <see cref="ProxyEventMessage{TArgs}"/>.
        /// </summary>
        /// <typeparam name="TEventArgs">
        ///     The type of the event data generated by the source event.
        /// </typeparam>
        /// <param name="messenger">
        ///     The <see cref="IMvxMessenger"/> to publish the event message to.
        /// </param>
        /// <param name="source">
        ///     The source object.
        /// </param>
        /// <param name="eventInfo">
        ///     The event to proxy.
        /// </param>
        public static void ProxyEvent<TEventArgs>(this IMvxMessenger messenger, object source, EventInfo eventInfo) where TEventArgs : EventArgs {
            Check.NotNull(messenger, nameof(messenger));
            Check.NotNull(source, nameof(source));
            Check.NotNull(eventInfo, nameof(eventInfo));

            eventInfo.AddEventHandler(source, new EventHandler<TEventArgs>((sender, args) => {
                messenger.Publish(ProxyEventMessage.For(sender, args));
            }));
        }
    }
}
