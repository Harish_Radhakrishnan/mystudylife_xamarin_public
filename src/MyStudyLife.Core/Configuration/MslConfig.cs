﻿using System;
using MvvmCross;
using MyStudyLife.Authorization;
using MyStudyLife.Net;

namespace MyStudyLife.Configuration {
    public abstract class MslConfig : IMslConfig {
        public static IMslConfig Current => Mvx.IoCProvider.Resolve<IMslConfig>();

        public abstract Version AppVersion { get; }

        public abstract string AppVersionName { get; }

        protected virtual string ApiEndpointUri => "https://api.mystudylife.com/";

        protected virtual string ApiVersion => "v6.1";

        protected string VersionedApiEndpointUri => $"{ApiEndpointUri}{ApiVersion}/";

        protected abstract string ApiClientId { get; }

        public virtual HttpApiClientSettings ApiClientSettings =>
            new HttpApiClientSettings(VersionedApiEndpointUri, ApiClientId);

        public string GetOAuthStartUri(AuthenticationProvider? provider = null) {
            // ReSharper disable once UseStringInterpolation
            return String.Format(
                "{0}oauth/authorize?response_type=code&client_id={1}&redirect_uri={2}&scope=device&login_hint={3}",
                VersionedApiEndpointUri,
                ApiClientSettings.ClientId,
                OAuthEndUri,
                provider?.ToString().ToLower() ?? String.Empty
            );
        }

        public string OAuthEndUri => "msl://oauth/callback";

        public string FeedbackUri => "http://feedback.mystudylife.com";

        public string SignUpUri => "https://accounts.mystudylife.com/sign-up";

        public string FindOutMoreUri => "https://www.mystudylife.com/";

        public string AccountRecoveryUri => "https://accounts.mystudylife.com/trouble";

        public string PasswordResetUri => "https://accounts.mystudylife.com/password-reset";

        public string PrivacyPolicyUri => "https://www.mystudylife.com/privacy-policy";

        public string TermsOfServiceUri => "https://www.mystudylife.com/terms-of-service";

        public string WebAppUri => "https://app.mystudylife.com";

        public string SupportEmailAddress => "support@mystudylife.com";

        public virtual bool MockUserPremiumStatus =>
#if DEBUG
            true;
#else
            false;
#endif

        public string FacebookPageUri => "https://www.facebook.com/mystudylife";
        public string FacebookPageDeeplink => "fb://page/187572234675612";

        public string TwitterProfileUri => "https://www.twitter.com/mystudylife";
        public string TwitterProfileDeeplink => "twitter://user?screen_name=mystudylife";
    }

    public abstract class MslConfig<TConfig> : MslConfig where TConfig : MslConfig, new() {
        public new static TConfig Current => new TConfig();
    }
}