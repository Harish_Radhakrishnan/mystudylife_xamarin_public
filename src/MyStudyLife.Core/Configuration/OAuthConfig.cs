﻿using System;

namespace MyStudyLife.Configuration {
	public class OAuthConfig {
		public string ClientId { get; set; }

		public Uri StartUri { get; set; }

		public Uri EndUri { get; set; }
	}
}
