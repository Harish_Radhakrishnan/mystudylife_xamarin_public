﻿using System;
using MyStudyLife.Authorization;
using MyStudyLife.Net;

namespace MyStudyLife.Configuration {
    public interface IMslConfig {
        Version AppVersion { get; }
        
        string AppVersionName { get; }

        HttpApiClientSettings ApiClientSettings { get; }

        string GetOAuthStartUri(AuthenticationProvider? provider = null);

        string OAuthEndUri { get; }

        string FeedbackUri { get; }

        string SignUpUri { get; }

        string FindOutMoreUri { get; }

        string AccountRecoveryUri { get; }

        string PasswordResetUri { get; }

        string SupportEmailAddress { get; }

        string PrivacyPolicyUri { get; }

        string TermsOfServiceUri { get; }

        string WebAppUri { get; }

        bool MockUserPremiumStatus { get; }

        /// <summary>
        ///     The web address of the My Study Life Facebook page, used
        ///     to open the native Facebook app if available.
        /// </summary>
        string FacebookPageUri { get; }
        /// <summary>
        ///     The deeplink of the My Study Life Facebook page, used
        ///     to open the native Facebook app if available.
        /// </summary>
        string FacebookPageDeeplink { get; }

        /// <summary>
        ///     The web address of the My Study Life Twitter page.
        /// </summary>
        string TwitterProfileUri { get; }
        /// <summary>
        ///     The deeplink of the My Study Life Twitter page, used
        ///     to open the native Twitter app if available.
        /// </summary>
        string TwitterProfileDeeplink { get; }
    }
}
