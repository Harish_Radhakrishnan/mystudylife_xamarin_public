﻿using System;

namespace MyStudyLife.Data {
	[Flags]
	public enum RotationDays {
		None = 0,
		Day1 = 1 << 0,
		Day2 = 1 << 1,
		Day3 = 1 << 2,
		Day4 = 1 << 3,
		Day5 = 1 << 4,
		Day6 = 1 << 5,
		Day7 = 1 << 6,
		Day8 = 1 << 7,
		Day9 = 1 << 8,
		Day10 = 1 << 9,
		Day11 = 1 << 10,
		Day12 = 1 << 11,
		Day13 = 1 << 12,
		Day14 = 1 << 13,
		Day15 = 1 << 14,
        Day16 = 1 << 15,
        Day17 = 1 << 16,
        Day18 = 1 << 17,
        Day19 = 1 << 18,
        Day20 = 1 << 19,
    }
}
