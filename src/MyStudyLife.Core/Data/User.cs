﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.Data.Schools;
using MyStudyLife.Data.Settings;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;

namespace MyStudyLife.Data
{
    /// <summary>
    /// Represents a My Study Life user
    /// </summary>
    [JsonObject]
    public class User : BindableBase {
        /// <summary>
        /// The Id of the user.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// The user's firstname.
        /// </summary>
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        /// <summary>
        /// The user's lastname.
        /// </summary>
        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonIgnore]
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// The user's email.
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        ///     The time (if any) the user was setup.
        /// </summary>
        [JsonProperty("setup_at")]
        public DateTime? SetupAt { get; set; }

        [JsonProperty("picture")]
        public Uri? Picture { get; set; }

        [JsonProperty("settings")]
        [DoNotNotify]
        public UserSettings Settings { get; set; }

        [JsonProperty]
        public string Timestamp { get; set; }

        #region Schools

        [JsonProperty("school")]
        [Ignore]
        public School School { get; set; }

        [JsonIgnore]
        [Column("School")]
        public string SchoolJson {
            get { return School == null ? null : JsonConvert.SerializeObject(School); }
            set {
                this.School = value != null ? JsonConvert.DeserializeObject<School>(value) : null;
            }
        }

        [JsonIgnore, Ignore]
        public bool HasSchool {
            get { return this.School != null; }
        }

        [JsonIgnore, Ignore]
        [Obsolete("WARNING: This will show things for students with an unmanaged school")]
        public bool IsStudentWithSchool {
            get { return this.IsStudent && this.HasSchool; }
        }

        [JsonIgnore, Ignore]
        public bool IsStudentWithManagedSchool => this.IsStudent && this.HasSchool && this.School.IsManaged;

        [JsonIgnore, Ignore]
        public bool IsStudentWithUnmanagedSchool => this.IsStudent && this.HasSchool && !this.School.IsManaged;

        [JsonIgnore, Ignore]
        public bool IsStudent => this.Type.Equals(UserType.Student);

        [JsonIgnore, Ignore]
        public bool IsTeacher => this.Type.Equals(UserType.Teacher);

        [JsonProperty("type")]
        public UserType Type { get; set; }

        #endregion

        #region Storage and Retrieval

        private static User? _currentUser;
        private static bool _currentUserSet;

        // TODO: Move User.Current to AppState or similar
        public static User? Current {
            get {
                if (!_currentUserSet) {
                    throw new Exception("User.Current has not been set yet");
                }

                return _currentUser;
            }
            set {
                _currentUser = value;
                _currentUserSet = true;
            }
        }

        /// <summary>
        /// Gets the current user.
        /// </summary>
        /// <param name="userStore">The current storage provider.</param>
        /// <returns>The current user, null if it doesn't exist.</returns>
        public static User? GetCurrent(IUserStore userStore) {
            if (_currentUser != null) {
                return _currentUser;
            }

            try {
                return Current = userStore.GetCurrentUser();
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<User>>().LogError("Caught exception whilst retrieving user: " + ex.ToLongString());

                return null;
            }
        }

        #endregion

        public virtual void ApplyLocaleSettings() {
            var cultureInfo = CultureInfo.CurrentCulture;
            var uiCultureInfo = CultureInfo.CurrentUICulture;

            if (CultureInfo.CurrentCulture.IsReadOnly) {
                cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            }
            if (CultureInfo.CurrentUICulture.IsReadOnly) {
                uiCultureInfo = (CultureInfo)CultureInfo.CurrentUICulture.Clone();
            }

            // Fixes MSL-230: Mono only deep-clones CultureInfo if !IsNeutralCulture
            // (https://github.com/mono/mono/blob/cb46f78eef5263ce24ba12c583e4c72ac64734c1/mcs/class/corlib/System.Globalization/CultureInfo.cs#L413)
            if (cultureInfo.DateTimeFormat.IsReadOnly) {
                cultureInfo.DateTimeFormat = (DateTimeFormatInfo)cultureInfo.DateTimeFormat.Clone();
            }
            if (uiCultureInfo.DateTimeFormat.IsReadOnly) {
                uiCultureInfo.DateTimeFormat = (DateTimeFormatInfo)uiCultureInfo.DateTimeFormat.Clone();
            }

            cultureInfo.DateTimeFormat.FirstDayOfWeek = this.Settings.FirstDayOfWeek;
            uiCultureInfo.DateTimeFormat.FirstDayOfWeek = this.Settings.FirstDayOfWeek;

            var ldp = cultureInfo.DateTimeFormat.LongDatePattern;

            // Fixup for UK culture
            if (ldp == "dd MMMM yyyy") {
                ldp = "dddd, d MMMM yyyy";

                cultureInfo.DateTimeFormat.LongDatePattern = ldp;
                uiCultureInfo.DateTimeFormat.LongDatePattern = ldp;
            }

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = uiCultureInfo;
        }
        
        #region ToString(...)

        public override string ToString() => $"{FirstName} {LastName}";

        #endregion
    }

    public enum UserType {
        [EnumMember(Value = "student")]
        Student,
        [EnumMember(Value = "teacher")]
        Teacher
    }
}
