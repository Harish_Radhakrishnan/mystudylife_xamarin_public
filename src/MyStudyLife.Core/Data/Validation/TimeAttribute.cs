﻿using System;

namespace MyStudyLife.Data.Validation {
    /// <summary>
    ///     Checks the <see cref="TimeSpan"/> represents a
    ///     time:
    ///         - Is not negative
    ///         - Is not greater than 23:59:59
    /// </summary>
    public class TimeAttribute : ValidationAttribute {
        public override bool IsValid(string propertyName, object propertyValue, out string errorMessage) {
            if (!(propertyValue is TimeSpan)) {
                throw new ArgumentException("TimeAttribute can only be applied to TimeSpan properties.");
            }

            var ts = (TimeSpan) propertyValue;

            if (ts.TotalSeconds < 0 || new TimeSpan(23, 59, 59) < ts) {
                errorMessage = "Invalid time (TimeSpan is negative or represents a period greater than 23:59:59)";

                return false;
            }

            errorMessage = null;

            return true;
        }
    }
}