﻿using System;
using System.Text.RegularExpressions;

namespace MyStudyLife.Data.Validation {
    [AttributeUsage(AttributeTargets.Property)]
    public class RegexAttribute : ValidationAttribute {
        public const string InvalidErrorMessageFormat = "{0} is invalid";

        /// <summary>
        /// The pattern to match against.
        /// </summary>
        public string Pattern { get; set; }

        /// <summary>
        /// Regex options.
        /// </summary>
        public RegexOptions Options { get; set; }

        public RegexAttribute(string pattern) {
            this.Pattern = pattern;
            this.Options = RegexOptions.None;
        }

        public override bool IsValid(string propertyName, object propertyValue, out string errorMessage) {
            if (!new Regex(this.Pattern, this.Options).IsMatch((string) propertyValue ?? "")) {
                errorMessage = this.ErrorMessage ?? String.Format(InvalidErrorMessageFormat, this.PropertyName ?? propertyName);
                return false;
            }

            errorMessage = String.Empty;
            return true;
        }
    }
}
