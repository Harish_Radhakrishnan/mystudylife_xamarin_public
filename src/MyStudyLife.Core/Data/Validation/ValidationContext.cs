﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MyStudyLife.Data.Validation {
    // TODO: Make a typed validation context
	public class ValidationContext {
	    private readonly List<Tuple<string, string, Func<object, bool>>> _customChecks = new List<Tuple<string, string, Func<object, bool>>>(); 
         
		private List<string> _errorMessages = null;

		public List<string> ErrorMessages {
			get {
				if (_errorMessages == null)
					throw new Exception("IsValid() must be called before accessing error messages.");

				return _errorMessages;
			}

			private set { _errorMessages = value; }
		}

		private Dictionary<string, string> _errors;

        public Dictionary<string, string> Errors {
			get { return _errors ?? (_errors = new Dictionary<string, string>()); }
		}

		/// <summary>
		/// The object to validate.
		/// </summary>
		public object Instance { get; set; }

		/// <summary>
		///		The group to validate.
		/// </summary>
		public string Group { get; set; }

		/// <summary>
		///		Creates a new <see cref="ValidationContext"/> instance.
		/// </summary>
		/// <param name="instance">The object instance to validate.</param>
		public ValidationContext(object instance) {
		    if (instance == null) {
		        throw new ArgumentNullException(nameof(instance));
		    }
        
			this.Instance = instance;
		}

		/// <summary>
		///		Creates a new <see cref="ValidationContext"/> instance.
		/// </summary>
		/// <param name="instance">The object instance to validate.</param>
		/// <param name="group">The group to validate.</param>
		public ValidationContext(object instance, string group) : this(instance) {
			this.Group = group;
		}

	    public void AddCustomValidationCheck(string errorKey, string errorMessage, Func<object, bool> checkFunc) {
	        _customChecks.Add(new Tuple<string, string, Func<object, bool>>(errorKey, errorMessage, checkFunc));   
	    }

		/// <summary>
		/// Validates the current context.
		/// </summary>
		/// <returns>True if valid.</returns>
		public bool IsValid() {
			ErrorMessages = new List<string>();

			Type vType = Instance.GetType();

			bool isValid = true;

			foreach (PropertyInfo property in vType.GetRuntimeProperties()) {
				var attributes = property.GetCustomAttributes(typeof (ValidationAttribute), false);

				foreach (var attribute in attributes.Cast<ValidationAttribute>().OrderBy(x => x.Priority)) {
				    if (attribute.Groups != null && attribute.Groups.Length > 0 && !attribute.Groups.Contains(this.Group)) {
				        continue;
				    }

					string errorMessage;

				    if (!attribute.IsValid(property.Name, property.GetValue(Instance, null), out errorMessage)) {

#pragma warning disable 618
                        ErrorMessages.Add(errorMessage);
#pragma warning restore 618

                        Errors.Add(property.Name, errorMessage);

				        isValid = false;

				        break;
				    }
				}
			}

		    foreach (var customCheck in _customChecks) {
		        if (!customCheck.Item3(Instance)) {
		            isValid = false;

		            this.Errors[customCheck.Item1] = customCheck.Item2;
		        }
		    }

			return isValid;
		}
	}
}
