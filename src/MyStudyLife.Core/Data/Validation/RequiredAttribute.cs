﻿using System;
using System.Collections;

namespace MyStudyLife.Data.Validation {
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredAttribute : ValidationAttribute {
        public const string RequiredErrorMessageFormat = "{0} is required";

	    public override int Priority {
	        get { return 1; }
	    }

	    public override bool IsValid(string propertyName, object propertyValue, out string errorMessage) {
            IEnumerable enumerableValue;

	        bool isInvalid = false;

	        if (propertyValue == null) {
	            isInvalid = true;
	        }
	        else if (propertyValue is string) {
	            isInvalid = String.IsNullOrWhiteSpace(propertyValue.ToString());
	        }
            else if ((enumerableValue = propertyValue as IEnumerable) != null) {
                isInvalid = !enumerableValue.GetEnumerator().MoveNext();
            }

            errorMessage = isInvalid 
                ? this.ErrorMessage ?? String.Format(RequiredErrorMessageFormat, this.PropertyName ?? propertyName)
                : String.Empty;

            return !isInvalid;
	    }
	}
}
