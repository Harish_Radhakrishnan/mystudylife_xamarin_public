﻿using System;

namespace MyStudyLife.Data.Validation {
    [AttributeUsage(AttributeTargets.Property)]
    public class StringLengthAttribute : ValidationAttribute {
        public const string MinStringLengthErrorMessageFormat = "{0} must be {1} or more characters";

        public const string MaxStringLengthErrorMessageFormat = "{0} must be {1} or less characters";

        /// <summary>
        /// The minimum allowed length of the property.
        /// </summary>
        public int MinLength { get; set; }

        /// <summary>
        /// The maximum allowed length of the property.
        /// 
        /// -1 for unrestricted.
        /// </summary>
        public int MaxLength { get; set; }

        public StringLengthAttribute() {
            this.MaxLength = -1;
        }

        public override bool IsValid(string propertyName, object propertyValue, out string errorMessage) {
            var strPropertyValue = (string) propertyValue;

            if (propertyValue != null && strPropertyValue.Length < MinLength) {
                errorMessage = this.ErrorMessage ?? String.Format(MinStringLengthErrorMessageFormat, this.PropertyName ?? propertyName, MinLength);
                return false;
            }

            if (propertyValue != null && MaxLength > -1 && MaxLength < strPropertyValue.Length) {
                errorMessage = this.ErrorMessage ?? String.Format(MaxStringLengthErrorMessageFormat, this.PropertyName ?? propertyName, MaxLength);
                return false;
            }

            errorMessage = String.Empty;

            return true;
        }
    }
}
