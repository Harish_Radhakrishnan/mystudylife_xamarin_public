﻿using System.Collections.Generic;

namespace MyStudyLife.Data.Validation {
    public interface IValidate {
        bool IsValid(out IDictionary<string, string> errors);
    }
}
