﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyStudyLife.Data.Validation {
	[AttributeUsage(AttributeTargets.Property)]
	public abstract class ValidationAttribute : Attribute {
		private List<string> _groups; 

		/// <summary>
		///		The name of the property.
		/// </summary>
		/// <remarks>
		///		If not set the actual property name will be used.
		/// </remarks>
		public string PropertyName { get; set; }

		/// <summary>
		///		The validation groups for the property.
		/// </summary>
		/// <remarks>
		///		If null the validator will always be run.
		/// </remarks>
		public string[] Groups {
			get { return _groups != null ? _groups.ToArray() : null; }
			set { _groups = value.ToList(); }
		}

		public string Group {
			get {
				throw new NotSupportedException();
			}
			set { (_groups ?? (_groups = new List<string>())).Add(value); }
		}

        /// <summary>
        ///     Overrides the default error message.
        /// </summary>
        public virtual string ErrorMessage { get; set; }

        /// <summary>
        ///     Ordered by this, the lower the number
        ///     the earlier it is executed.
        /// </summary>
        public virtual int Priority { get { return 5; } }
        
	    public abstract bool IsValid(string propertyName, object propertyValue, out string errorMessage);
	}
}
