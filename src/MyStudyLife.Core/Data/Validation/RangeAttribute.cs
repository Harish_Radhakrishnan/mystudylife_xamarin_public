﻿using System;

namespace MyStudyLife.Data.Validation {
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class RangeAttribute : ValidationAttribute {
        public const string MinRangeErrorMessageFormat = "{0} must be {1} or greater";

        public const string MaxRangeErrorMessageFormat = "{0} must be {1} or less";

        private int? _minimumValue;
        private int? _maximumValue;

        /// <summary>
        ///     The minimum allowed value.
        /// </summary>
        public int MinimumValue {
            get { return _minimumValue.GetValueOrDefault(); }
            set { _minimumValue = value; }
        }

        /// <summary>
        ///     The maximum allowed value.
        /// </summary>
        public int MaximumValue {
            get { return _maximumValue.GetValueOrDefault(); }
            set { _maximumValue = value; }
        }

        public bool HasMinimumValue {
            get { return _minimumValue.HasValue; }
        }

        public bool HasMaximumValue {
            get { return _maximumValue.HasValue; }
        }

        /// <summary>
        ///     Creates a new <see cref="RangeAttribute"/> instance.
        /// </summary>
        public RangeAttribute() {}

        /// <summary>
        ///     Creates a new <see cref="RangeAttribute"/> instance.
        /// </summary>
        /// <param name="minimumValue">The minimum allowed value.</param>
        /// <param name="maximumValue">The maximum allowed value.</param>
        public RangeAttribute(int minimumValue, int maximumValue) {
            this.MinimumValue = minimumValue;
            this.MaximumValue = maximumValue;
        }

        public override bool IsValid(string propertyName, object propertyValue, out string errorMessage) {
            if (HasMinimumValue && (int) propertyValue < MinimumValue) {
                errorMessage = this.ErrorMessage ?? String.Format(MinRangeErrorMessageFormat, this.PropertyName ?? propertyName, MinimumValue);
                return false;
            }

            if (HasMaximumValue && (int) propertyValue > MaximumValue) {
                errorMessage = this.ErrorMessage ?? String.Format(MaxRangeErrorMessageFormat, this.PropertyName ?? propertyName, MaximumValue);
                return false;
            }

            errorMessage = String.Empty;
            return true;
        }
    }
}
