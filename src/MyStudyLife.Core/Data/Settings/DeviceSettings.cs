﻿using System;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using PropertyChanged;

namespace MyStudyLife.Data.Settings {
    /// <summary>
    ///     Settings unique to a device that
    ///     the server needs to know about.
    /// </summary>
    [JsonObject]
    public class DeviceSettings : BindableBase, ISimpleSyncable {

        #region Reminders

        [JsonIgnore]
        private bool _remindersEnabled;
        [JsonIgnore]
        private bool _classRemindersEnabled;
        [JsonIgnore]
        private bool _examRemindersEnabled;
        [JsonIgnore]
        private bool _taskRemindersEnabled;
        [JsonIgnore]
        private ReminderTime _classReminderTime;
        [JsonIgnore]
        private ReminderTime _examReminderTime;
        [JsonIgnore]
        private TimeSpan _taskReminderTime;

        [JsonProperty(PropertyName = "reminders_enabled")]
        public bool RemindersEnabled {
            get { return _remindersEnabled; }
            set { SetProperty(ref _remindersEnabled, value); }
        }


        [JsonProperty(PropertyName = "class_reminders_enabled")]
        public bool ClassRemindersEnabled {
            get { return _classRemindersEnabled; }
            set { SetProperty(ref _classRemindersEnabled, value); }
        }


        [JsonProperty(PropertyName = "exam_reminders_enabled")]
        public bool ExamRemindersEnabled {
            get { return _examRemindersEnabled; }
            set { SetProperty(ref _examRemindersEnabled, value); }
        }

        [JsonProperty(PropertyName = "task_reminders_enabled")]
        public bool TaskRemindersEnabled {
            get { return _taskRemindersEnabled; }
            set { SetProperty(ref _taskRemindersEnabled, value); }
        }

        /// <summary>
        ///     The number of minutes before a class
        ///     the user should be notified.
        /// </summary>
        [JsonProperty(PropertyName = "class_reminder_time")]
        public ReminderTime ClassReminderTime {
            get { return _classReminderTime; }
            set { SetProperty(ref _classReminderTime, value); }
        }

        /// <summary>
        ///     The number of minutes before an exam
        ///     the user should be notified.
        /// </summary>
        [JsonProperty(PropertyName = "exam_reminder_time")]
        public ReminderTime ExamReminderTime {
            get { return _examReminderTime; }
            set { SetProperty(ref _examReminderTime, value); }
        }

        /// <summary>
        ///     The time of day to remind the user about
        ///     incomplete tasks due tomorrow.
        /// </summary>
        [JsonProperty(PropertyName = "task_reminder_time")]
        public TimeSpan TaskReminderTime {
            get { return _taskReminderTime; }
            set { SetProperty(ref _taskReminderTime, value); }
        }

        #endregion

        #region Sync

        [JsonIgnore]
        private bool _syncDisabledOverMeteredConnection;

        [JsonProperty(PropertyName = "metered_connection_disable")]
        public bool SyncDisabledOverMeteredConnection {
            get { return _syncDisabledOverMeteredConnection; }
            set { SetProperty(ref _syncDisabledOverMeteredConnection, value); }
        }

        #endregion

        public static DeviceSettings Default {
            get {
                return new DeviceSettings {
                    UpdatedAt = DateTime.UtcNow,
                    RemindersEnabled = true,
                    ClassRemindersEnabled = true,
                    ExamRemindersEnabled = true,
                    TaskRemindersEnabled = true,
                    ClassReminderTime = ReminderTime.Minutes15,
                    ExamReminderTime = ReminderTime.Minutes30,
                    TaskReminderTime = new TimeSpan(17, 00, 00),
                    SyncDisabledOverMeteredConnection = true
                };
            }
        }

        #region ISimpleSyncable

		[DoNotNotify]
        [JsonProperty(SyncService.SyncFlagProp)]
        public bool NeedsSyncing { get; set; }

        [JsonProperty("updated")]
        public DateTime? UpdatedAt { get; set; }
        
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        #endregion
    }
}
