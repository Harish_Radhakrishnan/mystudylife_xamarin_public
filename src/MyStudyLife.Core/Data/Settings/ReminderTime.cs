﻿using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Serialization;

namespace MyStudyLife.Data.Settings {
    [JsonEnum(SerializeAsString = false)]
    public enum ReminderTime {
        [EnumTitle("5")]
        Minutes5 = 5,
        [EnumTitle("15")]
        Minutes15 = 15,
        [EnumTitle("30")]
        Minutes30 = 30
    }
}
