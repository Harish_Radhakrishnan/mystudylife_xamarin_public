﻿using System.Runtime.Serialization;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.Data.Settings {
	public enum TimetableMode {
        /// <summary>
        ///     My Study Life will not differentiate
        ///     between classes based on their week.
        /// </summary>
        [EnumMember(Value = "fixed")]
        Fixed = 0,

		/// <summary>
		///     The timetable rotates on a weekly basis.
		/// </summary>
		/// <remarks>
		///     This maps to <code>TwoWeekTimetableMode.Auto</code>
		///     in the previous version... happy days :)
		/// </remarks>
		[EnumTitle("Week Rotation")]
        [EnumMember(Value = "week_rotation")]
		WeekRotation = 1,

		/// <summary>
		///     The timetable rotates on a daily basis.
        /// </summary>
        [EnumTitle("Day Rotation")]
        [EnumMember(Value = "day_rotation")]
        DayRotation = 2
	}
}
