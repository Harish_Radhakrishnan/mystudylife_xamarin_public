﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyStudyLife.Data.Store;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;

namespace MyStudyLife.Data.Settings {
	[JsonObject(MemberSerialization.OptIn)]
    [Table("TimetableSettings")]
    public class TimetableSettings : BindableBase, ISingleton, ISimpleSyncable {
        public const int MinRotationWeekCount = 2;
        public const int MaxRotationWeekCount = 4;
        public const int MinRotationDayCount = 2;
        public const int MaxRotationDayCount = 20;

        public const int DefaultRotationWeekCount = 2;
	    public const int DefaultRotationDayCount = 6;

        #region ISimpleSyncable

        // TODO: Rename column Column to match this [Column(SyncService.SyncFlagProp)]

        [JsonIgnore]
        [DoNotNotify]
        public bool NeedsSyncing { get; set; }

        [JsonProperty("updated_at")]
        [DoNotNotify]
	    public DateTime? UpdatedAt {
	        get { return this.Set; }
            set { this.Set = value.GetValueOrDefault(DateTime.UtcNow); }
	    }

        // Was previously named wrong, not worth renaming as it's legacy
        [Column("Timetstamp")]
        [JsonProperty("timestamp")]
        [DoNotNotify]
        public string Timestamp { get; set; }

        #endregion

        private TimetableMode _mode;
	    private DaysOfWeek _days;
        private DateTime _set;
        private int? _weekCount;
        private int? _setWeek;
        private int? _dayCount;
        private int? _setDay;

	    [JsonProperty("mode")]
	    public TimetableMode Mode {
	        get { return _mode; }
            set { SetProperty(ref _mode, value); }
	    }

        [JsonProperty("days")]
        public DaysOfWeek Days {
            get { return _days; }
            set { SetProperty(ref _days, value); }
        }

        #region Type:*Rotation
        
		/// <summary>
        ///     The date when either <see cref="SetWeek"/>
        ///     or <see cref="SetDay"/> were set.
        /// </summary>
        [JsonProperty("set_date")]
        public DateTime Set {
            get { return _set; }
            set { SetProperty(ref _set, value); }
        }

        // IMPORTANT: Day and week count must be before set day/week properties
        // for serialization.

        /// <summary>
        ///     The number of weeks that the user's
        ///     timetable rotates by.
        /// </summary>
        [JsonProperty("week_count")]
        public int? WeekCount {
            get { return _weekCount; }
            set {
                if (value < MinRotationWeekCount || value > MaxRotationWeekCount)
                    throw new Exception(String.Format("Week count must be at least {0} and less than or equal to {1} ({2} passed).", MinRotationWeekCount, MaxRotationWeekCount, value));

                SetProperty(ref _weekCount, value);
            }
        }

        /// <summary>
        ///     The week which corresponds to
        ///     <see cref="Set"/>.
        /// </summary>
        [JsonProperty("set_week")]
        public int? SetWeek {
            get { return _setWeek; }
            set {
                if (value < 0)
                    throw new Exception("Cannot be less than 0");

                if (value > DayCount && DayCount > 0) // Allows JSON deserialization?
                    throw new Exception(String.Format("SetWeek ({0} passed) can not exceed WeekCount ({1})", value, this.WeekCount)); 

                SetProperty(ref _setWeek, value);
            }
	    }

        /// <summary>
        ///     The number of days that the user's
        ///     timetable rotates by.
        /// </summary>
        [JsonProperty("day_count")]
        public int? DayCount {
            get { return _dayCount; }
            set {
                if (value < MinRotationDayCount || value > MaxRotationDayCount)
                    throw new Exception(String.Format("Day count must be at least {0} and less than or equal to {1} ({2} passed).", MinRotationDayCount, MaxRotationDayCount, value));

                SetProperty(ref _dayCount, value);
            }
        }
        
	    /// <summary>
	    ///     The days that <see cref="TimetableMode.DayRotation"/>
	    ///     applies to.
	    /// </summary>
        [JsonProperty("set_day")]
        public int? SetDay {
            get { return _setDay; }
            set {
                if (value < 0)
                    throw new Exception("Cannot be less than 0");

                if (value > DayCount && DayCount > 0) // Allows JSON deserialization?
                    throw new Exception(String.Format("SetDay ({0} passed) can not exceed DayCount ({1})", value, this.DayCount));

                SetProperty(ref _setDay, value);
            }
	    }
        
        #endregion
        
		#region Rotation Week Methods

        /// <summary>
        ///     Gets the current timetable week.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        ///     Occurs when <see cref="Mode"/> is not equal to
        ///     <see cref="TimetableMode.WeekRotation"/>.
        /// </exception>
        public int GetCurrentWeek() {
            return GetWeekForDate(DateTimeEx.Today);
        }

        /// <summary>
        ///     Gets the timetable week for the given date.
        /// </summary>
        /// <param name="date">The date to get the week for.</param>
        /// <exception cref="InvalidOperationException">
        ///     Occurs when <see cref="Mode"/> is not equal to
        ///     <see cref="TimetableMode.WeekRotation"/>.
        /// </exception>
        public int GetWeekForDate(DateTime date) {
            if (this.Mode != TimetableMode.WeekRotation) {
                throw new InvalidOperationException("Cannot get current week when timetable mode is not Week");
            }

            int setDateWeek = this.Set.GetWeekOfYear();
            int givenDateWeek = date.GetWeekOfYear();

            var weekCount = this.WeekCount.GetValueOrDefault(DefaultRotationWeekCount);
            var setWeek = this.SetWeek.GetValueOrDefault(1);

            int diff = (givenDateWeek - setDateWeek) % weekCount;

            return ((diff + setWeek + weekCount - 1) % weekCount) + 1;
        }

		#endregion

		#region Rotation Day Methods

        private readonly Dictionary<DateTime, int> _cache = new Dictionary<DateTime, int>(); 

        public int? GetDayForCurrentDate() {
            return GetDayForDate(DateTimeEx.Today);
        }

        public int? GetDayForDate(DateTime date) {
            if (this.Mode != TimetableMode.DayRotation)
                throw new InvalidOperationException("Cannot get current week when timetable mode is not DayRotation");

			var dateDayFlag = date.DayOfWeek.ToFlag();

			if ((this.Days & dateDayFlag) != dateDayFlag) {
				return null;
			}

            int dayForDate;

            if (_cache.TryGetValue(date.Date, out dayForDate)) {
                return dayForDate;
            }
			
            var iDay = this.Set.Date;
            var dayCount = this.DayCount.GetValueOrDefault(DefaultRotationDayCount);
            var setDay = this.SetDay.GetValueOrDefault(1);
            var i = setDay;

			if (date.IsBefore(iDay, DateTimeComparison.Day)) {
				
				while ((iDay - date).TotalDays > 0d) {
					var dowFlag = iDay.DayOfWeek.ToFlag();

					if ((this.Days & dowFlag) == dowFlag) {
						_cache[iDay] = i;

                        // The next "working" day before the current day will
                        // be the day count. For example if today is Monday,
                        // the current day is day 1 and this.Days == DaysOfWeek.Weekdays
                        // then i will become this.DayCount (ie. 6)
                        i = i == 1 ? dayCount : (i - 1 % dayCount);
					}

					iDay = iDay.AddDays(-1d);
				}
			}
			else if (date.IsAfter(iDay, DateTimeComparison.Day)) {
				
				while ((date - iDay).TotalDays > 0d) {
					var dowFlag = iDay.DayOfWeek.ToFlag();

					if ((this.Days & dowFlag) == dowFlag) {
						_cache[iDay] = i;

                        i = i % dayCount + 1;
					}

					iDay = iDay.AddDays(1d);
				}
			}

	        return i;
        }

		/// <summary>
		///		Gets a dictionary containing all the days between the
        ///		given start and end dates.
		/// </summary>
		/// <param name="startDate">The first date in the range.</param>
		/// <param name="endDate">The last date in the range.</param>
		/// <returns>A mapping for each date in the range.</returns>
		public IDictionary<DateTime, int?> GetDaysForDateRange(DateTime startDate, DateTime endDate) {
			if (this.Mode != TimetableMode.DayRotation)
				throw new InvalidOperationException("Cannot get current week when timetable mode is not DayRotation");

			startDate = startDate.Date;
			endDate = endDate.Date;

            var dayMapping = new Dictionary<DateTime, int?>();

			// The below essentially goes back until a day before
			// the current start day is selected as a working day.
			// 
			// This is necessary when the startDate occurs before the
			// current SetDate.
            DateTime jDay = startDate;

            while (!this.Days.HasFlag(jDay.DayOfWeek.ToFlag())) {
				jDay = jDay.AddDays(-1d);
			}

            DateTime iDay = jDay;

// ReSharper disable once PossibleInvalidOperationException
            int firstDay = this.GetDayForDate(jDay).Value;

			int i = firstDay;
		    int dayCount = this.DayCount.GetValueOrDefault(DefaultRotationDayCount);

			while ((endDate - iDay).TotalDays >= 0) {
                var dowFlag = iDay.DayOfWeek.ToFlag();

                bool isDayInSelectedDays = (this.Days & dowFlag) == dowFlag;

                // If it's within range, this is necessary because
                // sometimes we may start from the previous week.
                if ((iDay - startDate).TotalDays >= 0) {

                    if (isDayInSelectedDays) {
                        dayMapping.Add(iDay, i);

                        i = i % dayCount + 1;
				    }
				    else {
					    dayMapping.Add(iDay, null);
				    }
                }
                else if (isDayInSelectedDays) {
                    i = i % dayCount + 1;
                }

			    iDay = iDay.AddDays(1d);
			}

			return dayMapping;
		} 

		#endregion
        
        #region GetNextSchedulableDay(...)

        /// <summary>
        ///     Gets the next day which occurs after 
        ///     the given DateTime whose day of week
        ///     exists in the user-set timetable days.
        /// </summary>
	    public DateTime GetNextSchedulableRotationDay(DateTime d) {

            DaysOfWeek dFlag;

            do {
                d = d.AddDays(1.0);
                dFlag = d.DayOfWeek.ToFlag();
            } while ((this.Days & dFlag) == DaysOfWeek.None);

            return d;
        }

        #endregion

        #region Helper Properties
        // For use in binding / writing `== myenum.value` all the time sucks.

        /// <value>
        ///     True if the settings mode is fixed.
        /// </value>
        public bool IsFixed {
            get { return this.Mode == TimetableMode.Fixed; }
        }

        /// <value>
        ///     True if the settings mode is week rotation.
        /// </value>
	    public bool IsWeekRotation {
	        get { return this.Mode == TimetableMode.WeekRotation; }
	    }

        /// <value>
        ///     True if the settings mode is day rotation.
        /// </value>
        public bool IsDayRotation {
            get { return this.Mode == TimetableMode.DayRotation; }
        }

        #endregion

        #region ISingleton

	    public static async Task<TimetableSettings> Get(ISingletonDataStore dataStore, bool fallbackToDefault = true) {
	        return await dataStore.GetSingleton<TimetableSettings>().ConfigureAwait(false) ?? (fallbackToDefault ? TimetableSettings.Default : null);
		}

	    public System.Threading.Tasks.Task Save(ISingletonDataStore dataStore) {
	        return dataStore.SetSingleton(this);
		} 

		#endregion

        /// <summary>
        ///     Sets the default values for the current mode.
        /// </summary>
	    public void SetDefaults() {
            if (this.IsFixed) {
                return;
            }

            switch (this.Mode) {
                case TimetableMode.DayRotation:
                    this.DayCount = 6;
                    this.SetDay = 1;
                    break;
                case TimetableMode.WeekRotation:
                    this.WeekCount = 2;
                    this.SetWeek = 1;
                    break;
            }
	    }

        /// <summary>
        ///     Nulls any properties not applicable to the current mode.
        /// </summary>
	    public void Purge() {
            // Use backing fields as to not raise prop changed (breaks WP).
            if (this.Mode != TimetableMode.DayRotation) {
                this._dayCount = null;
                this._setDay = null;
            }

            if (this.Mode != TimetableMode.WeekRotation) {
                this._weekCount = null;
                this._setWeek = null;
            }
	    }

		public static TimetableSettings Default {
			get {
				return new TimetableSettings {
					Mode = TimetableMode.Fixed,
					Set = DateTime.UtcNow,
					Days = DaysOfWeek.Weekdays
				};
			}
		}
	}
}