﻿using System;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Serialization;
using MyStudyLife.Data.Validation;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Newtonsoft.Json;

namespace MyStudyLife.Data.Settings {
    [JsonObject]
    public class UserSettings : BindableBase, ISimpleSyncable {
        public bool NeedsSyncing { get; set; }

        public DateTime? UpdatedAt { get; set; }

        [Time(Group = "WelcomeWizard")]
        public TimeSpan DefaultStartTime { get; set; }

        [Range(Data.MinDuration, Data.MaxDuration, Group = "WelcomeWizard")]
        public int DefaultDuration { get; set; }

        [JsonIgnore]
        public bool Is24Hour { get; set; }

        public DayOfWeek FirstDayOfWeek { get; set; }

        public bool IsRotationScheduleLettered { get; set; }

        [JsonProperty("dashboard_tasks_timescale")]
        public DashboardTasksTimescale? DashboardTasksTimescale { get; set; }

        [JsonProperty("dashboard_exams_hidden")]
        public bool? DashboardExamsHidden { get; set; }

        [JsonProperty("dashboard_exams_hidden_when_none")]
        public bool? DashboardExamsHiddenWhenNone { get; set; }

        [JsonProperty("dashboard_exams_timescale")]
        public DashboardExamsTimescale? DashboardExamsTimescale { get; set; }

        public static UserSettings Default {
            get {
                return new UserSettings {
                    DefaultStartTime = new TimeSpan(9, 0, 0),
                    DefaultDuration = 90,
                    Is24Hour = Globalization.DateTimeFormat.Is24Hour,
                    FirstDayOfWeek = Globalization.DateTimeFormat.FirstDayOfWeek,
                    IsRotationScheduleLettered = false,
                    DashboardTasksTimescale = Settings.DashboardTasksTimescale.ThreeDays,
                    DashboardExamsHidden = false,
                    DashboardExamsHiddenWhenNone = false,
                    DashboardExamsTimescale = Settings.DashboardExamsTimescale.SevenDays
                };
            }
        }
    }
    [JsonEnum(SerializeAsString = false)]
    public enum DashboardTasksTimescale {
        [EnumTitle("3 days")]
        ThreeDays = 3,
        [EnumTitle("5 days")]
        FiveDays = 5,
        [EnumTitle("7 days")]
        SevenDays = 7,
        [EnumTitle("14 days")]
        FourteenDays = 14,
    }

    [JsonEnum(SerializeAsString = false)]
    public enum DashboardExamsTimescale {
        [EnumTitle("3 days")]
        ThreeDays = 3,
        [EnumTitle("7 days")]
        SevenDays = 7,
        [EnumTitle("14 days")]
        FourteenDays = 14,
        [EnumTitle("1 month")]
        OneMonth = 28,
    }
}
