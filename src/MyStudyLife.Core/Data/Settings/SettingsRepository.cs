﻿using System;
using MvvmCross;

namespace MyStudyLife.Data.Settings {
    public class SettingsRepository : IDisposable {
        private SingletonDataRepository<DeviceSettings> _deviceSettingsRepository;

        public SettingsRepository() : this(Mvx.IoCProvider.Resolve<IStorageProvider>()) { }

        public SettingsRepository(IStorageProvider storageProvider) {
            _deviceSettingsRepository = SingletonDataRepository<DeviceSettings>.GetInstance(storageProvider);
        }

        public async System.Threading.Tasks.Task<DeviceSettings> GetDeviceSettingsAsync() {
            return await _deviceSettingsRepository.GetStoredOrDefaultAsync(DeviceSettings.Default);
        }

        public async System.Threading.Tasks.Task PersistDeviceSettingsAsync(DeviceSettings deviceSettings) {
            await _deviceSettingsRepository.PersistAsync(deviceSettings);
        }

        public void Dispose() {
            _deviceSettingsRepository = null;
        }
    }
}
