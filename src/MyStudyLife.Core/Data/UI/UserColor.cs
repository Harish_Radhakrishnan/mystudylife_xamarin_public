﻿using System;
using System.Globalization;

namespace MyStudyLife.Data.UI {
    public class UserColor {
        public string Identifier { get; }
        
        public string Value { get; }

        private System.Drawing.Color? _color;
        public System.Drawing.Color Color => _color ??= System.Drawing.Color.FromArgb(Argb);

        public int Argb { get;  }
        
        public bool IsPremium { get;}

        public UserColor(string identifier, string value, bool isPremium = false) : this(identifier, Int32.Parse("FF" + value.Substring(1), NumberStyles.HexNumber), isPremium) { }
        public UserColor(string identifier, uint argb, bool isPremium = false) : this(identifier, (int)argb, isPremium) { }
        public UserColor(string identifier, int argb, bool isPremium = false) {
            this.Identifier = identifier;
            this.Argb = argb;
            this.IsPremium = isPremium;
        }
    }
}
