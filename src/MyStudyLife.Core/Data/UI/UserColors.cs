﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MyStudyLife.Data.UI {
    public static class UserColors {
        private static readonly IEnumerable<UserColor> _colors = new[] {
            new UserColor("0", 0xFF049372),
            new UserColor("1", 0xFFfd5645),
            new UserColor("2", 0xFFf89406),
            new UserColor("3", 0xFF3a539b),
            new UserColor("4", 0xFFdc44b4),
            new UserColor("5", 0xFFe3be0d),
            new UserColor("6", 0xFF95a5a6),
            new UserColor("7", 0xFF1098a5),
            new UserColor("8", 0xFF7d59b6),
            new UserColor("9", 0xFF38acfa),
            new UserColor("10", 0xFF34495e),
            new UserColor("11", 0xFF2574a9),
            new UserColor("12", 0xFFc0392b),
            new UserColor("13", 0xFF00634c),
            new UserColor("14", 0xFF674172),
            new UserColor("15", 0xFF73c667, true),
            new UserColor("16", 0xFFdb0a5b, true),
            new UserColor("17", 0xFFbfbfbf, true),
            new UserColor("18", 0xFF81cfe0, true),
            new UserColor("19", 0xFF1abc9c, true),
            new UserColor("20", 0xFFd2527f, true),
            new UserColor("21", 0xFFf5d76e, true),
            new UserColor("22", 0xFF1a2a3b, true),
            new UserColor("23", 0xFF4183d7, true),
            new UserColor("24", 0xFF762897, true)
        };

        private static readonly IEnumerable<UserColor> _oldColors = new[] {
            new UserColor("maroon", 0xFF771629),
            new UserColor("red", 0xFFa8120e),
            new UserColor("brown", 0xFF775516),
            new UserColor("olive", 0xFF687615),
            new UserColor("orange", 0xFFd98316),
            new UserColor("yellow", 0xFFd9cc16),
            new UserColor("green", 0xFF0a7d2d),
            new UserColor("mint", 0xFF1c868d),
            new UserColor("lblue", 0xFF0a5867),
            new UserColor("mblue", 0xFF264E8C),
            new UserColor("dblue", 0xFF161e73),
            new UserColor("purple", 0xFF3c1172),
            new UserColor("pink", 0xFF870a53),
            new UserColor("grey", 0xFF929ead),
            new UserColor("dgrey", 0xFF373E4E)
        };

        private static readonly Dictionary<string, string> _oldToNewMap = new Dictionary<string, string> {
            ["maroon"] = "12",
            ["red"] = "1",
            ["brown"] = "14",
            ["olive"] = "13",
            ["orange"] = "2",
            ["yellow"] = "5",
            ["green"] = "0",
            ["mint"] = "7",
            ["lblue"] = "9",
            ["mblue"] = "11",
            ["dblue"] = "3",
            ["purple"] = "8",
            ["pink"] = "4",
            ["grey"] = "6",
            ["dgrey"] = "10"
        };

        public static System.Drawing.Color Okay => Color.FromArgb(unchecked((int)0xFF35A863));
        public static System.Drawing.Color Attention => Color.FromArgb(unchecked((int)0xFFC90019));
        public static System.Drawing.Color Warning => Color.FromArgb(unchecked((int)0xFFEB9E0C));
        public static System.Drawing.Color Todo => Color.FromArgb(unchecked((int)0xFF2A83BB));
        public static System.Drawing.Color Empty => Color.FromArgb(unchecked((int)0xFFC3C5C9));

        private static readonly Dictionary<string, UserColor> _all = _colors.Union(_oldColors).ToDictionary(x => x.Identifier);

        public static IEnumerable<UserColor> GetColors(bool isUserPremium) {
            return _colors.Where(c => !c.IsPremium || isUserPremium);
        }

        public static UserColor GetColor(string identifier, bool isUserPremium) {
            identifier = NormalizeColorIdentifier(identifier);

            UserColor color = null;
            if (identifier != null) {
                _all.TryGetValue(identifier, out color);
            }

            if (color != null && color.IsPremium && !isUserPremium) {
                color = new UserColor("empty", Empty.ToArgb());
            }
            
            return color ?? new UserColor(null, "#1c8d76");
        }

        /// <summary>
        ///     Ensures the given <paramref name="identifier"/> is numerical
        ///     and not a name of a color (deprecated).
        /// </summary>
        public static string NormalizeColorIdentifier(string identifier) {
            if (identifier == null) {
                return null;
            }

            // Hack - because _oldColors shortest string is 3
            // ReSharper disable once RedundantLogicalConditionalExpressionOperand
            if (identifier.Length > 2) {
                if (!_oldToNewMap.TryGetValue(identifier, out identifier)) {
                    return null;
                }
            }

            return identifier;
        }
    }
}
