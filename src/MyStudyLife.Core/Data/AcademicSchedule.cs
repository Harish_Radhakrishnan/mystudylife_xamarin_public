﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using MyStudyLife.Data.Schools;
using MyStudyLife.Globalization;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [Table("AcademicYears")]
    public sealed class AcademicYear : BaseUserEntity, IAcademicSchedule, IBelongToSchoolOrUser {
        private DateTime _startDate, _endDate;

        private ObservableCollection<AcademicTerm> _terms;
        private ObservableCollection<Holiday> _holidays;
        private SchedulingOptions _scheduling;

        #region IBelongToSchoolOrUser

        [JsonIgnore]
	    public bool IsFromSchool => this.SchoolId.HasValue;

        #endregion

        [DoNotNotify]
        public DateTime StartDate {
            get => _startDate;
            set {
                if (SetPropertyEx(ref _startDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                }
            }
        }

        [DoNotNotify]
        public DateTime EndDate {
            get => _endDate;
            set {
                if (SetPropertyEx(ref _endDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                }
            }
        }

        [JsonIgnore]
        public string Dates => DateTimeFormat.FormatDateRange(this.StartDate, this.EndDate);

        [JsonIgnore]
        public bool IsCurrent => DateTimeEx.Today.IsBetween(this.StartDate, this.EndDate, DateTimeComparison.Day);

        // TODO: Should be ReadOnly
        [OneToMany]
        public ObservableCollection<AcademicTerm> Terms {
            get => _terms ?? (_terms = new ObservableCollection<AcademicTerm>());
            set => _terms = value;
        }

        [OneToMany]
	    public ObservableCollection<Holiday> Holidays {
	        get => _holidays ?? (_holidays = new ObservableCollection<Holiday>());
            set => _holidays = value;
        }

        [Ignore]
        public SchedulingOptions Scheduling {
            get => _scheduling ?? (_scheduling = new SchedulingOptions());
            set => _scheduling = value;
        }

        #region Nasty Hack
        // ReSharper disable InconsistentNaming
        // ReSharper disable UnusedMember.Local

        [Column("Scheduling_Mode")]
        private SchedulingMode Scheduling_Mode {
            get => this.Scheduling.Mode;
            set => this.Scheduling.Mode = value;
        }
        [Column("Scheduling_Scope")]
        private SchedulingScope Scheduling_Scope {
            get => this.Scheduling.Scope;
            set => this.Scheduling.Scope = value;
        }
        [Column("Scheduling_WeekCount")]
        private short? Scheduling_WeekCount {
            get => this.Scheduling.WeekCount;
            set => this.Scheduling.WeekCount = value;
        }
        [Column("Scheduling_StartWeek")]
        private short? Scheduling_StartWeek {
            get => this.Scheduling.StartWeek;
            set => this.Scheduling.StartWeek = value;
        }
        [Column("Scheduling_DayCount")]
        private int? Scheduling_DayCount {
            get => this.Scheduling.DayCount;
            set => this.Scheduling.DayCount = value;
        }
        [Column("Scheduling_StartDay")]
        private int? Scheduling_StartDay {
            get => this.Scheduling.StartDay;
            set => this.Scheduling.StartDay = value;
        }
        [Column("Scheduling_Days")]
        private DaysOfWeek? Scheduling_Days {
            get => this.Scheduling.Days;
            set => this.Scheduling.Days = value;
        }

        [Column("Scheduling_AdjustmentsJson")]
        private string Scheduling_AdjustmentsJson {
            get => this.Scheduling.Adjustments.Serialized;
            set => this.Scheduling.Adjustments.Serialized = value;
        }

        // ReSharper restore InconsistentNaming
        // ReSharper restore UnusedMember.Local
        #endregion

        /// <summary>
        ///     Adds the given term to list of terms for the academic year.
        /// </summary>
        /// <param name="term">
        ///     The term to add.
        /// </param>
        public void AddTerm(AcademicTerm term) {
            Check.NotNull(term, nameof(term));

            term.Year = this;
            term.YearGuid = this.Guid;

            _terms.Add(term);
        }

        /// <summary>
        ///     Removes a term with the given guid from the academic year.
        /// </summary>
        /// <param name="termGuid">
        ///     The term's globally unique identifier.
        /// </param>
        public void RemoveTerm(Guid termGuid) {
            Check.NotEmpty(ref termGuid, nameof(termGuid));

            _terms.RemoveAll(x => x.Guid == termGuid);
        }

        public override string ToString() {
		    return this.ToString(CultureInfo.CurrentCulture);
		}

	    public string ToString(CultureInfo culture) {
            return this.StartDate.IsSame(this.EndDate, DateTimeComparison.Year)
                        ? this.StartDate.Year.ToString()
                        : $"{this.StartDate.Year} - {this.EndDate.Year}";
        }
    }

    public interface IAcademicSchedule : IDbRow {
        DateTime StartDate { get; set; }

        DateTime EndDate { get; set; }

        string Dates { get; }

        bool IsCurrent { get; }

        string ToString();

        string ToString(CultureInfo culture);
    }

    public static class AcademicScheduleEx {
        /// <summary>
        ///     Checks if the given <see cref="IAcademicSchedule"/>
        ///     is related to this <see cref="IAcademicSchedule"/> either
        ///     by being the same, sharing a year or being a year's term.
        /// </summary>
        public static bool EqualsRecursive(this IAcademicSchedule a, IAcademicSchedule b) {
            if (a == null || b == null) {
                return false;
            }

            if (a.Guid == b.Guid) {
                return true;
            }

            if (a.GetType() != b.GetType()) {
                var term = a as AcademicTerm;
                if (term != null) {
                    return term.YearGuid == b.Guid;
                }

                term = b as AcademicTerm;
                if (term != null) {
                    return term.YearGuid == a.Guid;
                }
            }

            return false;
        }
    }
}
