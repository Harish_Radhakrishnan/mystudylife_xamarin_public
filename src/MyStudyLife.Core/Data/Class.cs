﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Schools;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Validation;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [DebuggerDisplay("{Title}")]
	[JsonObject, DataContract]
	[Table("Classes")]
	public class Class : SubjectDependentEntity, IScheduleRestrictable, IComparable<Class>, IComparable {
		#region Consts

		[JsonIgnore] public const int MinimumClassLength = 5;
		[JsonIgnore] public const string OneOffStudentValidationGroup = "OneOff:Student";
		[JsonIgnore] public const string OneOffTeacherValidationGroup = "OneOff:Teacher";
		[JsonIgnore] public const string RecurringStudentValidationGroup = "Recurring:Student";
		[JsonIgnore] public const string RecurringTeacherValidationGroup = "Recurring:Teacher";

		#endregion

		private TimeSpan? _startTime;
		private TimeSpan? _endTime;
		private DateTime? _startDate;
		private DateTime? _endDate;

        [JsonProperty("type")]
		[Required]
		[DefaultValue(ClassType.Recurring)]
		public ClassType Type { get; set; }

        [JsonIgnore]
	    public bool IsOneOff => this.Type.Equals(ClassType.OneOff);

        /// <summary>
		/// The class' module
		/// </summary>
		[JsonProperty("module"), DataMember(Name = "Module")]
        [Required(Groups = new []{ OneOffTeacherValidationGroup, RecurringTeacherValidationGroup })]
		[StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
		public string Module { get; set; }

		#region Type:OneOff

		/// <summary>
		///		The date on which a class
		///		of type <see cref="ClassType.OneOff"/>
		///		occurs.
		/// </summary>
		[JsonProperty("date")]
		[Required(Groups = new [] { OneOffStudentValidationGroup, OneOffTeacherValidationGroup })]
		public DateTime? Date { get; set; }

        /// <summary>
        ///		The time the class starts.
        /// </summary>
        [JsonProperty("start_time")]
        [Required(Groups = new [] { OneOffStudentValidationGroup, OneOffTeacherValidationGroup })]
        [DoNotNotify]
        public TimeSpan? StartTime {
            get { return _startTime; }
            set { SetPropertyEx(ref _startTime, value); }
        }

        /// <summary>
        ///		The time the class ends.
        /// </summary>
        [JsonProperty("end_time")]
        [Required(Groups = new [] { OneOffStudentValidationGroup, OneOffTeacherValidationGroup })]
        [DoNotNotify]
        public TimeSpan? EndTime {
            get { return _endTime; }
            set { SetPropertyEx(ref _endTime, value); }
        }

		#endregion

		#region Type:Recurring

        [DoNotNotify]
        [ForeignKey(typeof(AcademicYear), OnDeleteAction = OnDeleteAction.Nullify)]
        public Guid? YearGuid { get; set; }

        private AcademicYear _year;

        [JsonIgnore]
        [ManyToOne]
        public AcademicYear Year {
            get { return _year; }
            set {
                _year = value;

                if (_year != null && _term != null && _term.Year == null && _term.YearGuid == _year.Guid) {
                    _term.Year = _year;
                }
            }
        }

        [DoNotNotify]
        [ForeignKey(typeof(AcademicTerm), OnDeleteAction = OnDeleteAction.Nullify)]
        public Guid? TermGuid { get; set; }

        private AcademicTerm _term;

        [JsonIgnore]
        [ManyToOne]
        public AcademicTerm Term {
            get { return _term; }
            set {
                _term = value;

                if (_term != null && _year != null && _term.Year == null && _term.YearGuid == _year.Guid) {
                    _term.Year = _year;
                }
            }
        }

        [JsonIgnore, Ignore]
        public Guid? ScheduleGuid => this.TermGuid ?? this.YearGuid;

        [JsonIgnore]
        public IAcademicSchedule Schedule => (IAcademicSchedule) this.Term ?? this.Year;

        [JsonProperty("times")]
        [OneToMany]
        [Required(
            ErrorMessage = "One or more times are required",
            Groups = new [] { RecurringStudentValidationGroup, RecurringTeacherValidationGroup }
        )]
        public ObservableCollection<ClassTime> Times { get; set; }

		[JsonProperty("start_date")]
        [DoNotNotify] // SetPropertyEx
        public DateTime? StartDate {
			get { return _startDate; }
			set {
                if (SetPropertyEx(ref _startDate, value)) {
                    RaisePropertyChanged(nameof(HasStartEndDates));
                    RaisePropertyChanged(nameof(EffectiveStartDate));
                }
            }
		}

		[JsonProperty("end_date")]
        [DoNotNotify] // SetPropertyEx
        public DateTime? EndDate {
			get { return _endDate; }
			set {
                if (SetPropertyEx(ref _endDate, value)) {
                    RaisePropertyChanged(nameof(HasStartEndDates));
                    RaisePropertyChanged(nameof(EffectiveEndDate));
                }
            }
		}

		[JsonIgnore]
        [DependsOn("StartDate", "EndDate")]
		public bool HasStartEndDates => this.StartDate.HasValue && this.EndDate.HasValue;

        /// <summary>
        ///     The date this class starts,
        ///     taking into account <see cref="StartDate"/>
        ///     and <see cref="ActualSchedule"/>.
        /// </summary>
        /// <remarks>
        ///     This is not the first date the class
        ///     occurs, it's the first date we can
        ///     try to schedule for.
        /// </remarks>
        [JsonIgnore, Ignore]
        public DateTime? EffectiveStartDate => this.StartDate.HasValue
            ? this.StartDate
            : this.ActualSchedule?.StartDate;

        /// <summary>
        ///     The date this class ends,
        ///     taking into account <see cref="StartDate"/>
        ///     and <see cref="ActualSchedule"/>.
        /// </summary>
        /// <remarks>
        ///     This is not the last date the class
        ///     occurs, it's the last date we can
        ///     try to schedule for.
        /// </remarks>
        [JsonIgnore, Ignore]
        public DateTime? EffectiveEndDate => this.EndDate.HasValue
            ? this.EndDate
            : this.ActualSchedule?.EndDate;

        #endregion

		/// <summary>
		///		The class' room.
		/// </summary>
		[JsonProperty("room"), DataMember(Name = "Room")]
        [StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
		public string Room { get; set; }

		/// <summary>
		///		The class' building.
		/// </summary>
		[JsonProperty("building"), DataMember(Name = "Building")]
        [StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
		public string Building { get; set; }

        /// <summary>
        ///		The name of the primary teacher for the class.
        /// </summary>
        [Column("Tutor")] // For legacy reasons
        [JsonProperty("teacher_name"), DataMember(Name = "TeacherName")]
        [StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
		public string TeacherName { get; set; }

        [JsonProperty("teacher"), Ignore]
        public Teacher Teacher { get; set; }

        [JsonIgnore, Ignore]
	    public IAcademicSchedule ActualSchedule => this.ScheduleGuid.HasValue ? this.Schedule : this.Subject.Schedule;

        #region Schools

        [JsonIgnore]
        public bool IsFromSchool => this.SchoolId.HasValue;

        /// <remarks>
        ///     Teacher use only.
        /// </remarks>
        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonIgnore]
        [DependsOn(nameof(Color))]
        public string ActualColor => this.Color ?? this.Subject?.Color;

        #endregion

        #region IScheduledEntry

        [JsonIgnore]
		[DependsOn(nameof(Module))]
		public string Title => String.Concat(
            this.Subject?.Name ?? "<missing subject>",
            !String.IsNullOrWhiteSpace(this.Module)
                ? String.Concat(": ", this.Module)
                : String.Empty
        );

        [JsonIgnore]
        [DependsOn(nameof(Type), nameof(StartTime), nameof(EndTime))]
        public string Time {
            get {
                if (this.Type.Equals(ClassType.OneOff)) {
                    // ReSharper disable PossibleInvalidOperationException
                    return Globalization.DateTimeFormat.FormatTimeRange(this.StartTime.Value, this.EndTime.Value);
                    // ReSharper restore PossibleInvalidOperationException
                }

                return null;
            }
        }

		[JsonIgnore]
        [DependsOn(nameof(Room), nameof(Building))]
        public string Location {
			get {
			    bool hasRoom = !String.IsNullOrWhiteSpace(this.Room);
			    bool hasBuilding = !String.IsNullOrWhiteSpace(this.Building);

			    if (!hasRoom && !hasBuilding) return null;

                return (hasRoom && hasBuilding ? $"{this.Room}, {this.Building}" : String.Concat(this.Room, this.Building));
			}
        }

        #endregion

        protected override void OnSubjectChanged(Subject subject) {
            base.OnSubjectChanged(subject);

            RaisePropertyChanged(nameof(Title));
            RaisePropertyChanged(nameof(ActualColor));
        }

        public void SortTimes() => this.Times?.Sort();

		/// <summary>
		///		Removes anything not applicable to the current
		///		<see cref="Type"/> from the current <see cref="T:MyStudyLife.Data.Class"/>.
		/// </summary>
		public void Purge() {
            if (this.Type == ClassType.OneOff) {
                this.Times?.Clear();

                this.StartDate = null;
                this.EndDate = null;
            }

            if (this.Type == ClassType.Recurring) {
                this.Date = null;
                this.StartTime = null;
                this.EndTime = null;

                // If one is null, ensure both are.
                if (!this.HasStartEndDates) {
                    this.StartDate = null;
                    this.EndDate = null;
                }
            }
		}

        public bool OccursInRotationWeek(short week) {
            return this.IsOneOff || this.Times.Any(x => x.RotationWeek.GetValueOrDefault() == 0 || x.RotationWeek.GetValueOrDefault() == week);
        }

	    /// <summary>
		///		Checks if the class' start/end dates and/or subject's schedule
		///		would allow attempted scheduling for the given date.
		///
		///		Does not indicate whether the class actually occurs on that date.
		/// </summary>
		public bool CanScheduleFor(DateTime date) {
            var classStartDate = this.EffectiveStartDate;
            var classEndDate = this.EffectiveEndDate;

            if (classStartDate == null || classEndDate == null) {
                return true;
            }

            return date.IsBetween(classStartDate.Value, classEndDate.Value);
		}

        public bool CanScheduleForRange(DateTime startDate, DateTime endDate) {
            var classStartDate = this.EffectiveStartDate;
            var classEndDate = this.EffectiveEndDate;

            if (classStartDate == null || classEndDate == null) {
                return true;
            }

            return (
                classStartDate.Value.IsBetween(startDate, endDate) ||
                classEndDate.Value.IsBetween(startDate, endDate) ||
                startDate.IsBetween(classStartDate.Value, classEndDate.Value) ||
                endDate.IsBetween(classStartDate.Value, classEndDate.Value)
            );
        }

        public string GetColor(User user) {
            if (user == null) {
                return this.Subject.Color;
            }

            return user.IsTeacher ? this.ActualColor : this.Subject.Color;
        }

        #region Occurrence Text

	    public string GetOccurrenceText(TimetableSettings tSettings = null, bool includeStartEndDates = true) {
	        if (this.Type == ClassType.OneOff) {
	            return String.Concat("One-off: ", this.Date.HasValue ? this.Date.Value.ToString("D") : "no date");
	        }

            string str = String.Join(", ", this.Times.Select(x => x.GetOccurrenceText(tSettings)));

// ReSharper disable PossibleInvalidOperationException, StringLiteralTypo
	        if (this.HasStartEndDates && includeStartEndDates) {
	            str += String.Format(" from {0:m} {0:yyyy} - {1:m} {1:yyyy}", this.StartDate.Value, this.EndDate.Value);
	        }
// ReSharper restore PossibleInvalidOperationException, StringLiteralTypo

	        return str;
	    }

	    #endregion

        public int CompareTo(Class b) {
            var a = this;

            // Null comes last, ReferenceEquals for a reason.
            // (Some people implement equality operators to use to CompareTo!)
            if (Object.ReferenceEquals(b, null)) {
                return 1;
            }

            // Sort by subject
            if ((a.Subject == null) != (b.Subject == null)) {
                if (a.Subject == null) {
                    return -1;
                }
                if (b.Subject == null) {
                    return 1;
                }
            }
            else if ((a.Subject ?? b.Subject) != null) {
                var subjectNameCompare = a.Subject?.CompareTo(b.Subject) ?? -1;
                if (subjectNameCompare != 0) {
                    return subjectNameCompare;
                }
            }

            // Then by one off vs recurring
            if (a.IsOneOff && b.IsOneOff) {
                if (a.Date > b.Date) {
                    return 1;
                }
                if (a.Date < b.Date) {
                    return -1;
                }

                if (a.StartTime > b.StartTime) {
                    return 1;
                }
                if (a.StartTime < b.StartTime) {
                    return -1;
                }

                return 0;
            }
            if (a.IsOneOff && !b.IsOneOff) {
                return -1;
            }
            if (!a.IsOneOff && b.IsOneOff) {
                return 1;
            }

            // Then by the first time

            // NB: We're assuming the times are already sorted here?
            var aTime = a.Times.FirstOrDefault();
            var bTime = b.Times.FirstOrDefault();

            // The class should always have times at this point, however,
            // this shouldn't throw an exception if we don't.
            if (!Object.ReferenceEquals(aTime, bTime)) {
                // Classes without times(?) come first
                if (Object.ReferenceEquals(aTime, null)) {
                    return -1;
                }
                if (Object.ReferenceEquals(bTime, null)) {
                    return 1;
                }

                var timesCompare = aTime.CompareTo(bTime);
                if (timesCompare != 0) {
                    return timesCompare;
                }

                // TODO: We could sort by number of times as well?
            }

            // Then by the start date
            if (a.StartDate.HasValue && b.StartDate.HasValue) {
                return a.StartDate.Value.CompareTo(b.StartDate.Value);
            }
            if (a.StartDate.HasValue && !b.StartDate.HasValue) {
                return -1;
            }
            if (!a.StartDate.HasValue && b.StartDate.HasValue) {
                return 1;
            }

            return 0;
        }

        public int CompareTo(object obj) => CompareTo((Class) obj);
    }

	public enum ClassDayType {
		[EnumTitle("Day Of The Week")]
		DayOfWeek = 0,
		[EnumTitle("Rotation Day")]
		Rotation = 1
	}
}
