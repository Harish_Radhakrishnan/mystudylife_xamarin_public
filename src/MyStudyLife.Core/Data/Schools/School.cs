﻿using Newtonsoft.Json;

namespace MyStudyLife.Data.Schools {
    [JsonObject(MemberSerialization.OptOut)]
    public class School {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool ClassSharingEnabled { get; set; }

        public bool HasLogo { get; set; }

        public bool IsManaged { get; set; }

        public SchoolLogo Logo { get; set; }
    }

    public class SchoolLogo {
        public string Standard { get; set; }

        /// <summary>
        ///     The image to be displayed as the user's cover
        ///     photo.
        /// 
        ///     Optional. Should fallback to standard.
        /// </summary>
        public string Cover { get; set; }

        /// <summary>
        ///     Optional. Should fallback to standard.
        /// </summary>
        public string Wide { get; set; }

        /// <summary>
        ///     Optional. If not provided, do not display.
        /// </summary>
        public string Emblem { get; set; }
    }
}
