﻿using Newtonsoft.Json;

namespace MyStudyLife.Data.Schools {
    [JsonObject]
    public abstract class Person {
        [JsonProperty("id")]
        public int Id { get; private set; }

        [JsonProperty("first_name")]
        public string FirstName { get; private set; }

        [JsonProperty("last_name")]
        public string LastName { get; private set; }

        [JsonProperty("full_name")]
        public string FullName { get; private set; }

        [JsonProperty("email")]
        public string Email { get; private set; }

        [JsonProperty("picture")]
        public string Picture { get; private set; }
    }

    public sealed class Teacher : Person {
    }

    public sealed class Student : Person {
    }
}
