﻿namespace MyStudyLife.Data.Schools {
    public interface IBelongToSchoolOrUser : ICanBelongToUser, ICanBelongToSchool {}
}
