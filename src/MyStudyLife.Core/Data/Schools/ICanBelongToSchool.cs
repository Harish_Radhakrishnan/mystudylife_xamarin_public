﻿using Newtonsoft.Json;

namespace MyStudyLife.Data.Schools {
    public interface IBelongToSchool {
        [JsonProperty("school_id")]
        int SchoolId { get; set; }
    }

    public interface ICanBelongToSchool {
        [JsonProperty("school_id")]
        int? SchoolId { get; set; }
    }
}
