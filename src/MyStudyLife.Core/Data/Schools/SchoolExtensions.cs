﻿namespace MyStudyLife.Data.Schools {
    public static class SchoolExtensions {
        public static bool CanBeUsedBy(this IBelongToSchoolOrUser item, User user) {
            return user.Id == item.UserId || (user.HasSchool && item.SchoolId.HasValue && user.School.Id == item.SchoolId.Value);
        }

        public static bool CanBeEditedBy(this IBelongToSchoolOrUser item, User user) {
            // ReSharper disable once PossibleInvalidOperationException
            return user.Id == item.UserId || (user.IsTeacher && item.SchoolId.HasValue && user.School.Id == item.SchoolId.Value);
        }
    }
}
