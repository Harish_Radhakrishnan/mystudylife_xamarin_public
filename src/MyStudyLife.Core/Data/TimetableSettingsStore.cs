﻿using System.Threading.Tasks;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;

namespace MyStudyLife.Data {
    public class TimetableSettingsStore : ITimetableSettingsStore {

        private readonly ISingletonDataStore _singletonDataStore;
        private readonly IUserStore _userStore;
        private readonly IDataStore _dataStore;

        public TimetableSettingsStore(ISingletonDataStore singletonDataStore, IUserStore userStore, IDataStore dataStore) {
            this._singletonDataStore = singletonDataStore;
            this._userStore = userStore;
            this._dataStore = dataStore;
        }

        private Task<TimetableSettings> GetInternalAsync() => this._singletonDataStore.GetSingleton<TimetableSettings>();

        public async Task<TimetableSettings> GetAsync() {
            if (await this.NeedsTimetableSettingsAsync()) {
                return await this.GetInternalAsync();
            }
            return null;
        }

        public async Task<bool> NeedsTimetableSettingsAsync() {
            var user = _userStore.GetCurrentUser();
            var tSettings = await this.GetInternalAsync();

            if (user == null ||
                user.IsStudentWithUnmanagedSchool ||
                user.IsStudentWithManagedSchool ||
                tSettings == null ||
                tSettings.Mode == TimetableMode.Fixed
            ) {
                return false;
            }
            
            return await _dataStore.GetConnection().ExecuteScalarAsync<int>(
                "SELECT COUNT(*) FROM [Classes] AS [c] " +
                "INNER JOIN [Subjects] AS [s] ON [c].[SubjectGuid] = [s].[Guid] " +
                "WHERE [s].[DeletedAt] IS NULL AND [c].[DeletedAt] IS NULL AND [c].[Type] = 1 AND COALESCE([c].[TermGuid], [s].[TermGuid], [c].[YearGuid], [s].[YearGuid]) IS NULL AND EXISTS (" +
                    "SELECT 1 FROM [ClassTimes] AS [ct] " +
                    "WHERE [ct].[ClassGuid] = [c].[Guid] AND (COALESCE([ct].[RotationDays], 0) > 0 OR COALESCE([ct].[RotationWeek], 0) > 0)" +
                ")"
            ) > 0;
        }
    }
}
