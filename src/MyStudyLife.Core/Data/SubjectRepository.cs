﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyStudyLife.Sync;

namespace MyStudyLife.Data {
    public class SubjectRepository : Repository<Subject>, ISubjectRepository {

        public SubjectRepository() { }
        public SubjectRepository(IStorageProvider storageProvider) : base(storageProvider) { }

        /// <summary>
        ///     Like <see cref="GetByScheduleAsync"/> but only
        ///     subjects the user can modify.
        /// </summary>
        public async Task<IEnumerable<Subject>> GetModifiableByScheduleAsync(IAcademicSchedule schedule) {
            var subjects = await GetByScheduleAsync(schedule);

            var user = User.Current;

            if (user.IsTeacher) {
                return subjects;
            }

            return subjects.Where(x => x.UserId == user.Id);
        }

		/// <summary>
		///		Gets subjects filtered where:
		///
        ///     - The subject's schedule matches the selected one
        ///     - The subject's schedule is an `AcademicTerm` and is a term which
        ///       exists below the selected schedule (which is an `AcademicYear`).
        ///     - The selected schedule is an `AcademicTerm` and the subject
		///     - exists in the parent `AcademicYear`.
		/// </summary>
		public async Task<IEnumerable<Subject>> GetByScheduleAsync(IAcademicSchedule schedule) {
		    if (schedule == null) {
		        return await this.GetAllNonDeletedAsync();
            }

            Expression<Func<Subject, bool>> predicate;
            Guid? yearGuid, termGuid = null;

		    var term = schedule as AcademicTerm;

            if (term != null) {
		        yearGuid = term.YearGuid;
		        termGuid = schedule.Guid;

                predicate = x => (x.YearGuid == null || x.YearGuid == yearGuid) && (x.TermGuid == null || x.TermGuid == termGuid);
		    }
		    else {
		        yearGuid = schedule.Guid;

                predicate = x => (x.YearGuid == null || x.YearGuid == yearGuid);
            }

			return await this.GetAllNonDeletedAsync(predicate);
		}

        #region Overrides

		public override Task<IEnumerable<Subject>> GetByQueryTextAsync(string queryText) {
			return GetAllNonDeletedAsync(x => x.Name.Contains(queryText));
		}

        /// <summary>
        ///     Deletes a subject, all tasks, exams and classes for the current user.
        /// </summary>
        /// <param name="subject">The subject to delete.</param>
        public override async System.Threading.Tasks.Task DeleteAsync(Subject subject) {
            using (var examRepo = new ExamRepository())
            using (var taskRepo = new TaskRepository())
            using (var classRepo = new ClassRepository()) {

                var exams = await examRepo.GetBySubjectAsync(subject);
                var tasks = await taskRepo.GetBySubjectAsync(subject);
                var classes = await classRepo.GetBySubjectAsync(subject);

                await examRepo.DeleteAsync(exams);
                await taskRepo.DeleteAsync(tasks);
                await classRepo.DeleteAsync(classes);

                subject.DeletedAt = DateTime.UtcNow;

                // Added line to make sure, Subject must be sync with server as it is not being sync sometime due to LastupdateTime mismatch
                subject.Syncify();

                await DataStore.UpdateAsync(subject);

                TrySync();
            }
        }

        public override bool CheckForConflict(Subject a, Subject b) {
            return String.Equals(a.Name, b.Name, StringComparison.CurrentCultureIgnoreCase) && a.Color == b.Color;
        }

        #endregion
    }
}
