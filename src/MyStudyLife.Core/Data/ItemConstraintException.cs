﻿using System;
using System.Collections.Generic;

namespace MyStudyLife.Data {
	/// <summary>
	///		Should be used when an action is attempted
	///		to be made which violates constraints set
	///		by the application (not the database).
	/// 
	///		For example, when a user can't delete
	///		an entity because other entities are dependent
	///		on it (and it wouldn't be a good idea to delete
	///		them all at once).
	/// </summary>
	public class ItemConstraintException<T> : Exception {
		/// <summary>
		///		The items which prevent the action
		///		occurring.
		/// </summary>
		public IEnumerable<T> AffectedItems { get; set; }

		public ItemConstraintException(IEnumerable<T> affectedItems) {
			this.AffectedItems = affectedItems;
		}
	}
}
