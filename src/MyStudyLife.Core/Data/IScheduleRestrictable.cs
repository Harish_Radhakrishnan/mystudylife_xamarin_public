﻿using System;

namespace MyStudyLife.Data {
    public interface IScheduleRestrictable {
        Guid? YearGuid { get; }

        AcademicYear Year { get; }

        Guid? TermGuid { get; }

        AcademicTerm Term { get; }

        Guid? ScheduleGuid { get; }

        IAcademicSchedule Schedule { get; }
    }
}
