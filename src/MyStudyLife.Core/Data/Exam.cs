﻿using System;
using System.Diagnostics;
using MyStudyLife.Data.Validation;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;

namespace MyStudyLife.Data {
    [DebuggerDisplay("{Title}")]
    [JsonObject]
	[Table("Exams")]
    public class Exam : SubjectDependentEntity, IScheduledEntry {

        /// <summary>
        /// The exam's module.
        /// </summary>
        [StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
        public string Module { get; set; }

        /// <summary>
        /// Whether or not the exam is a resit.
        /// </summary>
        public bool Resit { get; set; }

        /// <summary>
        /// The student's seat for the exam.
        /// </summary>
        [StringLength(MinLength = 0, MaxLength = 8)]
        public string Seat { get; set; }

        /// <summary>
        /// The exam's room.
        /// </summary>
        [StringLength(MinLength = 0, MaxLength = Data.MaxCommonTextFieldLength)]
        public string Room { get; set; }

        /// <summary>
        /// The date and time at which the exam occurs.
        /// </summary>
        [Required]
        public DateTime Date { get; set; }

        /// <summary>
        /// The length of the exam in minutes.
        /// </summary>
        [Range(5, 720)]
        public int Duration { get; set; }

        #region IScheduledEntry

        [Ignore]
        [JsonIgnore]
        [DependsOn(nameof(Date))]
        public TimeSpan StartTime {
            get { return this.Date.TimeOfDay; }
            set {
                this.Date = new DateTime(this.Date.Year, this.Date.Month, this.Date.Day, value.Hours, value.Minutes, value.Seconds);
            }
        }

        /// <summary>
        ///     The time the exam ends.
        /// </summary>
        [JsonIgnore]
        [DependsOn(nameof(Date), nameof(Duration))]
        public TimeSpan EndTime {
            get { return this.Date.AddMinutes(this.Duration).TimeOfDay; }
        }

        [JsonIgnore]
        [DependsOn(nameof(Module))]
        public string Title {
            get {
                if (this.Guid == Guid.Empty) {
                    return this.Module;
                }

                return String.Concat(
                    this.Subject?.Name ?? "<missing subject>",
                    String.IsNullOrWhiteSpace(this.Module) ? " Exam" : String.Concat(": ", this.Module),
                    this.Resit ? " (resit)" : String.Empty);
            }
        }

        [JsonIgnore]
        [DependsOn(nameof(Date), nameof(Duration))]
        public string Time {
            get { return String.Concat(this.Date.ToShortTimeStringEx(), ", ", this.Duration, " minutes"); }
        }

        [JsonIgnore]
        [DependsOn(nameof(Room), nameof(Seat))]
        public string Location {
            get {
                return String.IsNullOrWhiteSpace(this.Room) || String.IsNullOrWhiteSpace(this.Seat)
                    ? String.Concat(this.Seat, this.Room)
                    : String.Concat(this.Seat, ", ", this.Room);
            }
        }

        public ExamState State {
            get {
                if (this.Date.IsBefore(DateTimeEx.Now)) {
                    return ExamState.Past;
                }
                var days = DateTimeEx.Now.Range(this.Date).CalendarDays;
                if (days <= 3) {
                    return ExamState.InTheNext3Days;
                }
                else if (days <= 7) {
                    return ExamState.InTheNext7Days;
                }
                return ExamState.Future;
            }
        }

        protected override void OnSubjectChanged(Subject subject) {
            base.OnSubjectChanged(subject);

            RaisePropertyChanged(nameof(Title));
        }

        #endregion
    }

    public enum ExamState {
        Past,
        InTheNext3Days,
        InTheNext7Days,
        Future
    }
}
