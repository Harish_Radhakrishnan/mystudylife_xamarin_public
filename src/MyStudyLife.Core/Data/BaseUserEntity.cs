﻿using Newtonsoft.Json;

namespace MyStudyLife.Data {
	public abstract class BaseUserEntity : BaseEntity {
		[JsonProperty("user_id")]
		public int? UserId { get; set; }

        [JsonProperty("school_id")]
        public int? SchoolId { get; set; }
    }
}
