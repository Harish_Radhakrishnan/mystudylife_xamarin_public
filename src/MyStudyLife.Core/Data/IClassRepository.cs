﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MyStudyLife.Data.Schools;
using MyStudyLife.Scheduling;

namespace MyStudyLife.Data {
    public interface IClassRepository : ISubjectDependentRepository<Class> {
        Task<IEnumerable<Class>> GetByDateRangeAndScheduleGuidsAsync(DateTime startDate, DateTime endDate, params Guid[] scheduleGuids);
        Task<IEnumerable<Student>> GetStudentsAsync(Class cls, CancellationToken cancellationToken);
        Task<IEnumerable<ScheduledClass>> GetForTaskAsync(Task task);
        Task<IEnumerable<ScheduledClass>> GetConflictsForExamAsync(Exam exam);
        Task<bool> AnyClassesForYearHaveTerm(AcademicYear year);
        Task<bool> HasClassesWithoutSchedule();
    }
}
