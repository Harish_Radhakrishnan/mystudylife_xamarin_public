﻿using MyStudyLife.Data.Settings;
using System.Globalization;

namespace MyStudyLife.Data {
    public static class UserExtensions {
        /// <summary>
        ///     Updates all locale related settings to match <see cref="CultureInfo.CurrentCulture"/>.
        /// </summary>
        /// <param name="settings">
        ///     The user settings object.
        /// </param>
        /// <param name="settings"></param>
        public static void SetLocaleSettingsFromCurrentCulture(this UserSettings settings) 
            => SetLocaleSettings(settings, CultureInfo.CurrentCulture);

        /// <summary>
        ///     Updates all locale related settings to match the source <see cref="CultureInfo"/>.
        /// </summary>
        /// <param name="settings">
        ///     The user settings object.
        /// </param>
        /// <param name="sourceCultureInfo">
        ///     The source culture info.
        /// </param>
        public static void SetLocaleSettings(this UserSettings settings, CultureInfo sourceCultureInfo) {
            Check.NotNull(settings, nameof(settings));
            Check.NotNull(sourceCultureInfo, nameof(sourceCultureInfo));

            settings.FirstDayOfWeek = sourceCultureInfo.DateTimeFormat.FirstDayOfWeek;
        }
    }
}
