﻿namespace MyStudyLife.Data {
    public static class Extensions {
        public static bool IsOwnedBy(this IBelongToUser item, User user) {
            return item.UserId == user.Id;
        }

        public static TaskState GetState(this Task task) {
            if (task.IsOverdue) return TaskState.Overdue;
            else if (task.IsComplete) return TaskState.Complete;
            else if (task.IsIncompleteAndDueSoon) return TaskState.IncompleteAndDueSoon;
            else return TaskState.Todo;
        }
    }

    public enum TaskState {
        Overdue,
        IncompleteAndDueSoon,
        Complete,
        Todo
    }
}