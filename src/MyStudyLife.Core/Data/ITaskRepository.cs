﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyStudyLife.Scheduling;

namespace MyStudyLife.Data {
    public interface ITaskRepository : ISubjectDependentRepository<Task> {
        bool CheckForConflict(Task a, Task b);
        Task<IEnumerable<Task>> GetByDueDateAsync(DateTime dueDate);
        Task<TaskCounts> GetCountsForScheduledClassAsync(ScheduledClass sc);
        Task<IEnumerable<Task>> GetForExamAsync(Exam exam);
        Task<IEnumerable<Task>> GetForScheduledClassAsync(ScheduledClass sc, bool getOverdueForPastClass = true);
        Task<IEnumerable<Task>> GetIncompleteByDueDateAsync(DateTime? startDate, DateTime endDate);
        Task<IEnumerable<Task>> GetByDueDateRangeAsync(DateTime startDate, DateTime endDate);
        System.Threading.Tasks.Task UpdateProgressAsync(Task item);
        System.Threading.Tasks.Task UpdateProgressAsync(IEnumerable<Task> items, int progress);
    }
}