﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.Data.Store;
using MyStudyLife.Sync;
using SQLiteNetExtensionsAsync.Extensions;

namespace MyStudyLife.Data {
    public abstract class Repository<T> : IRepository<T>
        where T : BaseUserEntity, new() {

        private IDataStore _ds;
        private ISingletonDataStore _sds;

	    public IDataStore DataStore => _ds ??= Mvx.IoCProvider.GetSingleton<IDataStore>();
        public ISingletonDataStore SingletonDataStore => _sds ??= Mvx.IoCProvider.GetSingleton<ISingletonDataStore>();

        /// <summary>
        ///     Creates a new instance of <see cref="Repository{T}"/>
        ///     using <see cref="Mvx"/> to resolve
        ///     <see cref="MyStudyLife.Data.IStorageProvider"/>.
        /// </summary>
        /// <exception cref="MvxException">
        ///     Thrown when <see cref="Mvx"/> does not
        ///     exist for the current context or is unable to resolve
        ///     <see cref="MyStudyLife.Data.IStorageProvider"/>.
        /// </exception>
        protected Repository() {}

		protected Repository(IStorageProvider ignored) : this() { }

        #region SyncService

        private ISyncService _syncService;

        public ISyncService SyncService {
            get { return _syncService ?? (_syncService = Mvx.IoCProvider.GetSingleton<ISyncService>()); }
        }

        #endregion

        #region Instance Handling

        private static Repository<T> _instance;

        public static Repository<T> GetInstance(IStorageProvider storageProvider) {
            if (_instance != null) return _instance;

            if (storageProvider != null && !Mvx.IoCProvider.CanResolve<IStorageProvider>())
                throw new ArgumentNullException(nameof(storageProvider), "Storage provider cannot be null when Mvx.IoCProvider.Resolve is inaccessible.");

            var types = typeof(T).GetTypeInfo().Assembly.DefinedTypes;

            var tRepo = types.SingleOrDefault(x => x.IsSubclassOf(typeof (Repository<T>)) && !x.IsAbstract);

	        if (tRepo == null) {
		        throw new Exception(
					String.Format(
						"Given type does not have a repository, create a class which inherits from Repository<{0}>.",
						typeof(T).FullName
					)
				);
	        }

            return _instance = (Repository<T>) Activator.CreateInstance(tRepo.AsType(), storageProvider);
        }

        #endregion

        #region Get

        public virtual async Task<T> GetAsync(Guid guid) {
            var item = await DataStore.GetAsync<T>(guid);

            if (item != null && item.DeletedAt.HasValue) {
                return null;
            }

            return item;
        }

		public virtual Task<IEnumerable<T>> GetAllNonDeletedAsync() {
            return DataStore.GetAsync<T>(x => x.DeletedAt == null);
        }

        public virtual Task<IEnumerable<T>> GetAllNonDeletedAsync(Expression<Func<T, bool>> filter) {
            return DataStore.GetAsync<T>(x => x.DeletedAt == null, filter);
        }

	    public abstract Task<IEnumerable<T>> GetByQueryTextAsync(string queryText);

        #endregion

        #region Count

        public virtual Task<int> Count() {
            return DataStore.CountAsync<T>(x => x.DeletedAt == null);
        }

        public virtual Task<int> Count(Expression<Func<T, bool>> filter) {
            throw new NotImplementedException();
        }

        #endregion

        #region Add, Update, Delete

		public virtual async System.Threading.Tasks.Task AddOrUpdateAsync(T item) {
            Check.NotNull(item, nameof(item));

            await AssertNoConflictingItems(item);

            item.Syncify();

            if (item.Guid.IsEmpty()) {
                item.Guid = Guid.NewGuid();
                item.CreatedAt = DateTime.UtcNow;
                item.UserId = User.Current.Id;
                
                try {
                    await OnAdding(item);

                    await DataStore.AddAsync(item);
                }
                catch {
                    item.Guid = Guid.Empty;

                    throw;
                }

                await OnAdded(item);
            }
			else {
				item.UpdatedAt = DateTime.UtcNow;

                await OnUpdating(item);

				await DataStore.UpdateAsync(item);

                await OnUpdated(item);
            }

            TrySync();
        }

        protected async System.Threading.Tasks.Task AssertNoConflictingItems(params T[] items) {
            var conn = DataStore.GetConnection();
            // Retrieve all items without references for SPEED!
            var nonDeletedDbItems = await conn.Table<T>().Where(x => x.DeletedAt == null).ToListAsync();

            foreach (T item in items) {
                var conflictingItems = nonDeletedDbItems.Where(x => x.Guid != item.Guid && CheckForConflict(x, item)).ToList();

                if (conflictingItems.Any()) {
                    await conn.SetChildrenAsync(conflictingItems);

                    throw new ItemConflictException<T>(conflictingItems);
                }
            }
        }

        public virtual async System.Threading.Tasks.Task AddOrUpdateAsync(IEnumerable<T> items) {
            var itemList = items.ToList();

	        await AssertNoConflictingItems(itemList.ToArray());

            itemList.Apply(x => x.Syncify());

            var toAdd = itemList.Where(x => x.Guid.IsEmpty()).ToList();

            if (toAdd.Any()) {
				toAdd.Apply(x => x.CreatedAt = DateTime.UtcNow);

				await DataStore.AddAsync<T>(toAdd);
            }
			else {
                var conn = DataStore.GetConnection();

                // Doing it this way means if we're bulk adding (unlikely) then it's faster.
                var existingGuids = itemList.Where(x => !x.Guid.IsEmpty()).Select(x => x.Guid).ToList();
                var existingDbItems = await conn.Table<T>().Where(x => existingGuids.Contains(x.Guid)).ToListAsync();
                var existingDbItemGuids = existingDbItems.Select(x => x.Guid).ToList();

                toAdd = itemList.Where(x => existingDbItemGuids.Contains(x.Guid) == false).ToList();

                if (toAdd.Any()) {
                    toAdd.Apply(x => x.CreatedAt = DateTime.UtcNow);

                    await DataStore.AddAsync<T>(toAdd);
                }

                var toUpdate = itemList.Where(x => !x.Guid.IsEmpty() && toAdd.All(y => x.Guid != y.Guid)).ToList();

				toUpdate.Apply(x => x.UpdatedAt = DateTime.UtcNow);

				await DataStore.UpdateAsync<T>(toUpdate);
            }

            TrySync();
        }

        public virtual async System.Threading.Tasks.Task DeleteAsync(T item) {
            if (item.DeletedAt.HasValue) {
                return;
            }

            item.DeletedAt = DateTime.UtcNow;

            item.Syncify();

			await DataStore.UpdateAsync(item);

            TrySync();
        }

        public virtual async System.Threading.Tasks.Task DeleteAsync(IEnumerable<T> items) {
            items = items.ToList();

            items.Apply(x => {
                x.DeletedAt = DateTime.UtcNow;
                x.Syncify();
            });

			await DataStore.UpdateAsync(items);

            TrySync();
        }

        protected virtual System.Threading.Tasks.Task OnAdding(T item) => System.Threading.Tasks.Task.CompletedTask;
        protected virtual System.Threading.Tasks.Task OnAdded(T item) => System.Threading.Tasks.Task.CompletedTask;

        protected virtual System.Threading.Tasks.Task OnUpdating(T item) => System.Threading.Tasks.Task.CompletedTask;
        protected virtual System.Threading.Tasks.Task OnUpdated(T item) => System.Threading.Tasks.Task.CompletedTask;

        #endregion

        #region Validation

        public abstract bool CheckForConflict(T a, T b);

        #endregion

        #region Protected Methods

        protected void TrySync() {
            if (SyncService != null) {
                SyncService.TrySync(SyncTriggerKind.DataChange);
            }
        }

        #endregion

	    #region Implementation of IDisposable

	    public void Dispose() { }

	    #endregion
    }
}
