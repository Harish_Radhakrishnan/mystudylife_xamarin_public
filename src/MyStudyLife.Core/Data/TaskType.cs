﻿using System.Runtime.Serialization;

namespace MyStudyLife.Data {
    /// <summary>
    /// The type of a task.
    /// </summary>
    public enum TaskType {
        /// <summary>
        /// A piece of written work, activity or 
        /// similar that is due for a given date
        /// not associated with an exam.
        /// </summary>
        [EnumMember(Value = "assignment")]
        Assignment = 0,

        /// <summary>
        /// A reminder to do something on a given
        /// date.
        /// </summary>
        [EnumMember(Value = "reminder")]
        Reminder = 1,

        /// <summary>
        /// Similar to assignment, any piece of work
        /// that involves revising for a test or exam.
        /// </summary>
        [EnumMember(Value = "revision")]
        Revision = 2
    }
}
