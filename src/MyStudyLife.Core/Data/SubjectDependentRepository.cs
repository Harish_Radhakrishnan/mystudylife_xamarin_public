﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using SQLiteNetExtensionsAsync.Extensions;

namespace MyStudyLife.Data
{
    public abstract class SubjectDependentRepository<T> : Repository<T>, ISubjectDependentRepository<T>
        where T : SubjectDependentEntity, new() {

        protected SubjectDependentRepository() {}
        protected SubjectDependentRepository(IStorageProvider storageProvider) : base(storageProvider) {}

        public Task<IEnumerable<T>> GetBySubjectAsync(Subject subject) {
            Check.NotNull(subject, nameof(subject));

            return this.GetBySubjectGuidAsync(subject.Guid);
        }

        public Task<IEnumerable<T>> GetBySubjectGuidAsync(Guid subjectGuid) {
            Check.NotEmpty(ref subjectGuid, nameof(subjectGuid));

            return this.GetAllNonDeletedAsync(x => x.SubjectGuid == subjectGuid);
        }

		#region GetByScheduleAsync(...)

		/// <summary>
		///		Gets items by the given <paramref name="schedule"/>
		///		any of the following are true:
		///
		///		<list type="bullet">
		///			<item>
		///				<description>
		///					The subject does not have an assigned <see cref="IAcademicSchedule"/>.
		///				</description>
		///			</item>
		///			<item>
		///				<description>
		///					The subject's <see cref="IAcademicSchedule"/> is the same as the given
		///					<paramref name="schedule" />.
		///				</description>
		///			</item>
		///			<item>
		///				<description>
		///					The subject's <see cref="IAcademicSchedule"/> is a term
		///					and the given <paramref name="schedule"/> is the term's year.
		///				</description>
		///			</item>
		///			<item>
		///				<description>
		///					The subject's <see cref="IAcademicSchedule"/> is a year
		///					and the given <paramref name="schedule"/> is one of the year's terms
		///				</description>
		///			</item>
		///		</list>
		/// </summary>
		/// <param name="schedule"></param>
		/// <returns></returns>
		public virtual async Task<IEnumerable<T>> GetByScheduleAsync(IAcademicSchedule schedule) {
		    if (schedule == null) {
		        return await this.GetAllNonDeletedAsync();
		    }

		    var allowedSubjectGuids = (await new SubjectRepository().GetByScheduleAsync(schedule)).Select(x => x.Guid).ToList();

			return await this.GetAllNonDeletedAsync(x => allowedSubjectGuids.Contains(x.SubjectGuid));
		}

	    #endregion

	    #region Overrides of Repository<T>

        public override async Task<T> GetAsync(Guid guid) {
            // Basically sets the children of the subject (ie. the schedule)
            var t = await base.GetAsync(guid);

            if (t != null) {
                if (t.Subject != null) {
                    var conn = DataStore.GetConnection();
                    await conn.SetChildrenAsync(new[] { t.Subject });
                }
                else {
                    // This is to fix MSL-402 (and a couple of other issues) where
                    // somehow an item's subject is null. This would only occur when
                    // the has been soft deleted as we have foreign key constraints in SQLite
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger("SubjectDependentRepository").LogWarning("Retrieved item with null subject, assuming item deleted");

                    return null;
                }
            }

            return t;
        }

        public override async Task<IEnumerable<T>> GetAllNonDeletedAsync() {
		    return await SetSubjectChildrenAsync(await base.GetAllNonDeletedAsync());
	    }

		public override async Task<IEnumerable<T>> GetAllNonDeletedAsync(Expression<Func<T, bool>> filter) {
		    return await SetSubjectChildrenAsync(await base.GetAllNonDeletedAsync(filter));
	    }

	    #endregion

		// Currently SQLite extensions only goes one
		// level deep with relationships
		private async Task<IEnumerable<T>> SetSubjectChildrenAsync(IEnumerable<T> items) {
			items = items.ToList();

		    var conn = DataStore.GetConnection();
			await conn.SetChildrenAsync(items.Select(x => x.Subject).Where(x => x != null));

			return items.AsEnumerable();
		}
	}
}
