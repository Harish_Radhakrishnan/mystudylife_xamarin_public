﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyStudyLife.Sync;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensionsAsync.Extensions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data {
    public interface IAcademicYearRepository : IRepository<AcademicYear> {
        /// <summary>
        ///     Retrieves the current schedule (being an academic year or term)
        ///     based on the current date (<c>DateTimeEx.Today</c>).
        /// </summary>
        /// <returns></returns>
        Task<IAcademicSchedule> GetCurrentScheduleAsync();

        /// <summary>
        ///     Retrieves the year which is closest to the given
        ///     date.
        /// </summary>
        Task<AcademicYear> GetClosestToCurrentAsync();

        Task<IAcademicSchedule> GetClosestScheduleToCurrentAsync();

        /// <summary>
        ///     Gets the current or closest schedule to the given <paramref name="date"/>.
        /// </summary>
        /// <param name="date">
        ///     The date to use to find the closest schedule.
        /// </param>
        /// <param name="bias">
        ///     Whether there is a bias towards future or past schedules.
        /// </param>
        /// <param name="closestTerm">
        ///     If <c>false</c>, will only return a term if it is current.
        ///     If there are no current terms, the year will be returned.
        ///     Defaults to <c>true</c>.
        /// </param>
        Task<IAcademicSchedule> GetClosestScheduleToDateAsync(DateTime date, TenseBias bias, bool closestTerm = true);

        /// <summary>
        ///     Retrieves a year or term from the given guid.
        /// </summary>
        Task<IAcademicSchedule> GetSchedule(Guid guid);

        Task<IEnumerable<AcademicYear>> GetForDateRange(DateTime startDate, DateTime endDate);

        Task<AcademicYear> GetForDateAsync(DateTime date);

        Task<AcademicYear> GetYearAfterDateAsync(DateTime date);

        // TODO: Split to IHolidayRepository
        Task<IEnumerable<Holiday>> GetHolidaysAsync(Guid yearGuid);

        Task<Holiday> GetHolidayForDate(DateTime date);

        AsyncTask AddOrUpdateHolidayAsync(AcademicYear year, Holiday holiday);

        AsyncTask DeleteHolidayAsync(Holiday item);
    }

    public class AcademicYearRepository : Repository<AcademicYear>, IAcademicYearRepository {

        public AcademicYearRepository() { }
		public AcademicYearRepository(IStorageProvider storageProvider) : base(storageProvider) { }

        public async Task<IAcademicSchedule> GetSchedule(Guid guid) {
            IAcademicSchedule schedule = await this.GetAsync(guid);

            if (schedule == null) {
                AcademicTerm term;

                schedule = term = await DataStore.GetAsync<AcademicTerm>(guid);

                if (term != null) {
                    // The Year will always be set here except in the case of a term
                    // being deleted before MSL-357 was fixed which caused terms to
                    // not actually be deleted, they would just have their YearGuid
                    // set to the default Guid value. If one of these non-deleted terms
                    // had items assigned to the, this function would throw a null reference.
                    if (term.Year == null) {
                        return null;
                    }

                    var conn = DataStore.GetConnection();
                    await conn.SetChildrenAsync(new [] { term.Year });
                }
            }

            return schedule;
        }

		/// <summary>
		///		Gets the current academic schedule (either a year
		///		or term) based on the current date.
		/// </summary>
		/// <returns>The current year or term. Or null.</returns>
		public async Task<IAcademicSchedule> GetCurrentScheduleAsync() {
			DateTime today = DateTimeEx.Today;

			var acYears = (await this.GetAllNonDeletedAsync(x => x.StartDate <= today && today <= x.EndDate));

// ReSharper disable PossibleMultipleEnumeration
			Debug.Assert(acYears.Count() <= 1, "Found multiple 'current' academic years.");

			// There's a chance that they'll overlap due to syncing.
			var acYear = acYears.FirstOrDefault();
// ReSharper restore PossibleMultipleEnumeration

			if (acYear != null) {
				var term = acYear.Terms.FirstOrDefault(x => x.StartDate <= today && today <= x.EndDate);

				if (term != null) return term;
			}

			return acYear;
		}

        #region GetClosestToCurrent

        /// <summary>
        ///     Gets the current or closest academic year to the current date, favoring
        ///     future academic years.
        /// </summary>
        public async Task<AcademicYear> GetClosestToCurrentAsync()
            => GetClosestToDate(await this.GetAllNonDeletedAsync(), DateTimeEx.Today, TenseBias.FavorFuture);

        /// <summary>
        ///     Gets the current or closest academic year or temr to the current date,
        ///     favoring future years or terms
        /// </summary>
        public Task<IAcademicSchedule> GetClosestScheduleToCurrentAsync()
            => GetClosestScheduleToDateAsync(DateTimeEx.Today, TenseBias.FavorFuture);

        /// <summary>
        ///     Gets the current or closest schedule to the given <paramref name="date"/>.
        /// </summary>
        /// <remarks>
        ///     Any time as part of the <see cref="DateTime"/> will be ignored.
        /// </remarks>
        /// <param name="date">
        ///     The date to use to find the closest schedule.
        /// </param>
        /// <param name="bias">
        ///     Whether there is a bias towards future or past schedules.
        /// </param>
        /// <param name="closestTerm">
        ///     If <c>false</c>, will only return a term if it is current.
        ///     If there are no current terms, the year will be returned.
        ///     Defaults to <c>true</c>.
        /// </param>
        public async Task<IAcademicSchedule> GetClosestScheduleToDateAsync(DateTime date, TenseBias bias, bool closestTerm = true) {
            date = date.Date;

            var year = GetClosestToDate(await this.GetAllNonDeletedAsync(), date, bias);

            if (year != null) {
                var term = GetClosestToDate(year.Terms, date, bias);

                if (term != null && (closestTerm || term.IsCurrent)) return term;
            }

            return year;
        }

        /// <summary>
        ///     Gets the closest to current schedule based on a delta so:
        ///
        ///     - If there is a current schedule, it is returned.
        ///
        ///     [TenseBias.FavorFuture]
        ///     - else if there are any future schedules, the closest is returned
        ///     - else the most recent past schedule is returned.
        ///
        ///     [TenseBias.FavorPast]
        ///     - else if there are any past schedules, the most recent is returned
        ///     - else most closest future schedule is returned
        ///
        ///     [TenseBias.None]
        ///     - else if the closest schedule (past or future) is returned
        /// </summary>
        private T GetClosestToDate<T>(IEnumerable<T> schedules, DateTime date, TenseBias bias) where T : IAcademicSchedule {
            Predicate<T> biasFunc;

            switch (bias) {
                case TenseBias.FavorFuture:
                    biasFunc = x => date.Date < x.StartDate.Date;
                    break;
                case TenseBias.FavorPast:
                    biasFunc = x => x.EndDate.Date < date.Date;
                    break;
                case TenseBias.None:
                default:
                    biasFunc = x => false;
                    break;
            }

            return schedules
                .OrderByDescending(x => (x.StartDate <= date && date <= x.EndDate) || biasFunc(x))
                .ThenBy(x => Math.Min(Math.Abs((date.Date - x.StartDate.Date).TotalDays), Math.Abs((x.EndDate.Date - date.Date).TotalDays)))
                .FirstOrDefault();
        }

        #endregion

        /// <summary>
        ///     Returns the any academic years which occur in the given date range.
        /// </summary>
        /// <remarks>
        ///     Any time as part of the <see cref="DateTime"/> will be ignored.
        /// </remarks>
        /// <param name="startDate">
        ///     The first date in the range.
        /// </param>
        /// <param name="endDate">
        ///     The last date in the range.
        /// </param>
        public Task<IEnumerable<AcademicYear>> GetForDateRange(DateTime startDate, DateTime endDate) {
            startDate = startDate.Date;
            endDate = endDate.Date;

            if (startDate > endDate) throw new ArgumentException($"{nameof(endDate)} must be greater than or equal to the {nameof(startDate)}");

            return this.GetAllNonDeletedAsync(y =>
                // Start date is between academic schedule dates
                (y.StartDate <= startDate && startDate <= y.EndDate) ||
                    // End date is between academic schedule dates
                (y.StartDate <= endDate && endDate <= y.EndDate) ||
                    // Academic schedule's start and end dates are between
                    // the given start and end dates
                (startDate <= y.StartDate && y.EndDate <= endDate)
            );
	    }

        /// <summary>
        ///     Returns an academic year for the given <paramref name="date"/>, or
        ///     null if one does not exist.
        /// </summary>
        /// <remarks>
        ///     Any time as part of the <see cref="DateTime"/> will be ignored.
        /// </remarks>
        public async Task<AcademicYear> GetForDateAsync(DateTime date) {
            date = date.Date;

            return (await GetAllNonDeletedAsync(y => date >= y.StartDate && date <= y.EndDate)).FirstOrDefault();
        }

        /// <summary>
        ///     Returns the first academic year which occurs after the given
        ///     <paramref name="date"/>.
        /// </summary>
        /// <remarks>
        ///     Any time as part of the <see cref="DateTime"/> will be ignored.
        /// </remarks>
        public async Task<AcademicYear> GetYearAfterDateAsync(DateTime date) {
            date = date.Date;

            return (await GetAllNonDeletedAsync(y => y.StartDate > date)).FirstOrDefault();
        }

        [Obsolete("No longer needed with terms split. Use GetAllNonDeletedAsync()")]
        public Task<IEnumerable<AcademicYear>> GetAllNonDeletedYearsAsync(
            Expression<Func<AcademicYear, bool>> filter = null
		) {
            Expression<Func<AcademicYear, bool>> yearFilter = x => x.DeletedAt == null;

			if (filter != null) {
				return DataStore.GetAsync(yearFilter, filter);
			}

			return DataStore.GetAsync(yearFilter);
		}

        /// <summary>
        ///     Like <see cref="GetAllNonDeletedYearsAsync"/> but only
        ///     academic years the user can modify.
        /// </summary>
        public async Task<IEnumerable<AcademicYear>> GetModifiableYearsAsync() {
            var user = User.Current;

            if (user.IsTeacher) {
                return await GetAllNonDeletedAsync();
            }

            return await GetAllNonDeletedAsync(x => x.UserId == user.Id);
        }

		#region Overrides of Repository<AcademicSchedule>

        public async Task<IEnumerable<Subject>> GetAssociatedSubjects(AcademicYear item) {
            var termGuids = item.Terms.Select(x => x.Guid).ToList();

            return await DataStore.GetAsync<Subject>(x => x.DeletedAt == null && ((x.YearGuid != null && x.YearGuid == item.Guid) || (x.TermGuid != null && termGuids.Contains((Guid)x.TermGuid))));
		}

        public async Task<IEnumerable<Class>> GetAssociatedClasses(AcademicYear item) {
            var termGuids = item.Terms.Select(x => x.Guid).ToList();

            return await DataStore.GetAsync<Class>(x => x.DeletedAt == null && ((x.YearGuid != null && x.YearGuid == item.Guid) || (x.TermGuid != null && termGuids.Contains((Guid)x.TermGuid))));
	    }

		/// <summary>
		///		Deletes an academic schedule but only if
		///		the AcademicSchedule has no subjects assigned to
		///		it (and any terms with subjects assigned if it's a year).
		/// </summary>
		public override async System.Threading.Tasks.Task DeleteAsync(AcademicYear item) {
			var subjects = await GetAssociatedSubjects(item);
			var classes = await GetAssociatedClasses(item);

// ReSharper disable PossibleMultipleEnumeration
			if (subjects.Any()) {
				throw new ItemConstraintException<Subject>(subjects);
			}
		    if (classes.Any()) {
		        throw new ItemConstraintException<Class>(classes);
		    }
// ReSharper restore PossibleMultipleEnumeration

            item.DeletedAt = DateTime.UtcNow;

            item.Syncify();

            await DataStore.UpdateAsync(item);

            SyncService.TrySync(SyncTriggerKind.DataChange);
		}

		public override System.Threading.Tasks.Task DeleteAsync(IEnumerable<AcademicYear> items) {
			throw new NotSupportedException();
		}

		public override Task<IEnumerable<AcademicYear>> GetByQueryTextAsync(string queryText) {
			throw new NotSupportedException();
		}

        public override bool CheckForConflict(AcademicYear a, AcademicYear b) {
		    if (a.Guid == b.Guid) {
		        return false;
		    }

			return a.StartDate.IsBetween(b.StartDate, b.EndDate) || a.EndDate.IsBetween(b.StartDate, b.EndDate);
		}

        #endregion

        #region Term Handling

        private void SetTermGuids(AcademicYear item) {
            item.Terms.Apply(x => {
                x.YearGuid = item.Guid;

                if (x.Guid.IsEmpty()) {
                    x.Guid = Guid.NewGuid();
                }
            });
        }

        protected override AsyncTask OnAdding(AcademicYear item) {
            SetTermGuids(item);
            return AsyncTask.CompletedTask;
        }

        protected override async AsyncTask OnAdded(AcademicYear item) {
            var conn = DataStore.GetConnection();

            await conn.InsertAllAsync(item.Terms, typeof(AcademicTerm));
        }

        protected override AsyncTask OnUpdating(AcademicYear item) {
            SetTermGuids(item);
            return AsyncTask.CompletedTask;
        }

        protected override async AsyncTask OnUpdated(AcademicYear item) {
            await AddOrUpdateSubRows(item, item.Terms);
        }

        // Overridden to not use conn.UpdateWithChildren (which Repository will
        // use by default) as it fails to correctly delete a term as well as
        // failing to nullify relationships.
        public override async AsyncTask AddOrUpdateAsync(AcademicYear item) {
            var conn = DataStore.GetConnection();

            await AssertNoConflictingItems(item);

            item.Syncify();

            if (item.Guid.IsEmpty()) {
                item.Guid = Guid.NewGuid();
                item.CreatedAt = DateTime.UtcNow;
                item.UserId = User.Current.Id;

                try {
                    await OnAdding(item);

                    await conn.InsertAsync(item);
                }
                catch {
                    item.Guid = Guid.Empty;

                    throw;
                }

                await OnAdded(item);
            }
            else {
                item.UpdatedAt = DateTime.UtcNow;

                await OnUpdating(item);

                await conn.UpdateAsync(item);

                await OnUpdated(item);
            }

            TrySync();
        }

        private async System.Threading.Tasks.Task AddOrUpdateSubRows<T>(AcademicYear item, ICollection<T> subrows) where T : IDbRow {

            var conn = DataStore.GetConnection();

            var subrowGuids = subrows.Select(x => x.Guid).ToList();

            var deleteParams = new List<object> {
                item.Guid
            };
            deleteParams.AddRange(subrowGuids.Select(x => (object)x.ToString()).ToArray());

            await conn.RunInTransactionAsync(c => {
                foreach (var t in subrows) {
                    c.InsertOrReplace(t, typeof(T));
                }
            });

            if (subrowGuids.Any()) {
                await conn.ExecuteAsync(
                    String.Format(
                        "DELETE FROM [{0}] WHERE [YearGuid] = ? AND [Guid] NOT IN ({1})",
                        typeof(T).GetTableName(),
                        String.Join(",", subrowGuids.Select(x => "?"))
                    ),
                    deleteParams.ToArray()
                );
            }
            else {
                // Fix for MSL-368, when the academic year doesn't have any terms
                // associated, remove any terms from the database for that year guid
                await conn.ExecuteAsync($"DELETE FROM [{typeof(T).GetTableName()}] WHERE [YearGuid] = ?", item.Guid);
            }
        }

        #endregion

        #region Holidays

        public Task<IEnumerable<Holiday>> GetHolidaysAsync(Guid yearGuid) {
            return DataStore.GetAsync<Holiday>(x => x.YearGuid == yearGuid && x.DeletedAt == null);
        }

        /// <summary>
        ///     Gets the first holiday (if any) that exists for
        ///     the given date.
        /// </summary>
        public async Task<Holiday> GetHolidayForDate(DateTime date) {
            var potentialHolidays = await DataStore.GetAsync<Holiday>(x =>
                x.DeletedAt == null && x.StartDate <= date && date <= x.EndDate
            );

            return potentialHolidays.FirstOrDefault(x => x.Year.DeletedAt == null);
        }

        public async AsyncTask AddOrUpdateHolidayAsync(AcademicYear year, Holiday holiday) {
            var conn = DataStore.GetConnection();
            var nonDeletedDbHolidays = await conn.Table<Holiday>().Where(x => x.DeletedAt == null).ToListAsync();

            var conflictingHolidays = nonDeletedDbHolidays.Where(x => x.Guid != holiday.Guid && x.YearGuid == year.Guid && (
                x.StartDate.IsBetween(holiday.StartDate, holiday.EndDate) ||
                x.EndDate.IsBetween(holiday.StartDate, holiday.EndDate)
            )).ToList();

            if (conflictingHolidays.Any()) {
                throw new ItemConflictException<Holiday>(conflictingHolidays);
            }

            holiday.Year = year;
            holiday.YearGuid = year.Guid;

            holiday.Syncify();

            if (holiday.Guid.IsEmpty()) {
                holiday.Guid = Guid.NewGuid();
                holiday.CreatedAt = DateTime.UtcNow;

                await DataStore.AddAsync(holiday);
            }
            else {
                holiday.UpdatedAt = DateTime.UtcNow;

                await DataStore.UpdateAsync(holiday);
            }

            TrySync();
        }

        public async AsyncTask DeleteHolidayAsync(Holiday item) {
            if (item.DeletedAt.HasValue) {
                return;
            }

            item.DeletedAt = DateTime.UtcNow;

            item.Syncify();

            await DataStore.UpdateAsync(item);

            TrySync();
        }

        #endregion
	}

    public enum TenseBias {
        None,
        FavorPast,
        FavorFuture
    }
}
