﻿using System;
using System.Linq;
using System.Reflection;

namespace MyStudyLife.Data.Annotations {
    public static class AnnotationsExtensions {
        /// <summary>
        ///     Gets the <see cref="EnumTitleAttribute" /> of an <see cref="Enum" /> 
        ///     type value.
        /// </summary>
        /// <param name="value">The <see cref="Enum" /> type value.</param>
        /// <returns>A string containing the text of the
        /// <see cref="EnumTitleAttribute"/>.</returns>
        public static string GetTitle(this Enum value) {
            Check.NotNull(value, nameof(value));

            var valueName = value.ToString();
            var fieldInfo = value.GetType().GetRuntimeField(valueName);

            return fieldInfo.GetCustomAttributes<EnumTitleAttribute>(false).FirstOrDefault()?.Description ?? valueName;
        }
    }
}
