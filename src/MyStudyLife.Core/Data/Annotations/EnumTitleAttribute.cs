﻿using System;

namespace MyStudyLife.Data.Annotations {
    // TODO: Enable localization

    /// <summary>
    /// Provides a description for an enumerated type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class EnumTitleAttribute : Attribute {
        private readonly string _title;

        /// <summary>
        /// Gets the description stored in this attribute.
        /// </summary>
        /// <value>The description stored in the attribute.</value>
        public string Description {
            get { return this._title; }
        }

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="EnumTitleAttribute"/> class.
        /// </summary>
        /// <param name="title">The title to store in this attribute.
        /// </param>
        public EnumTitleAttribute(string title) {
            this._title = title;
        }
    }
}
