﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data.Caching;
using MyStudyLife.Data.Schools;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using SQLiteNetExtensionsAsync.Extensions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data {
    public class ClassRepository : SubjectDependentRepository<Class>, IClassRepository {

        public ClassRepository() { }
        public ClassRepository(IStorageProvider storageProvider) : base(storageProvider) { }

        #region Gets

        private const string NoScheduleSql =
            "SELECT {0} FROM [Classes] AS [C] " +
            "INNER JOIN [Subjects] AS [S] ON [C].[SubjectGuid] = [S].[Guid] " +
            "WHERE [S].[DeletedAt] IS NULL AND [C].[DeletedAt] IS NULL AND [S].[YearGuid] IS NULL AND [S].[TermGuid] IS NULL AND [C].[YearGuid] IS NULL AND [C].[TermGuid] IS NULL";

        public override async Task<IEnumerable<Class>> GetByScheduleAsync(IAcademicSchedule schedule) {
            string sql;
            object[] args;

            if (schedule == null) {
                sql = String.Format(NoScheduleSql, "[C].*");
                args = new object[0];
            }
            else {
                var term = schedule as AcademicTerm;
                var isTerm = term != null;

                sql =
                    "SELECT [C].* FROM [Classes] AS [C] " +
                    "INNER JOIN [Subjects] AS [S] ON [C].[SubjectGuid] = [S].[Guid] " +
                    "WHERE [S].[DeletedAt] IS NULL AND [C].[DeletedAt] IS NULL AND " +
                    "(" +
                        "COALESCE([C].[YearGuid], [S].[YearGuid]) IS NULL OR " +
                        "(" +
                            "COALESCE([C].[YearGuid], [S].[YearGuid]) = ?1 " + (
                                isTerm ?
                                " AND (" +
                                    "COALESCE([C].[TermGuid], [S].[TermGuid]) IS NULL OR " +
                                    "COALESCE([C].[TermGuid], [S].[TermGuid]) = ?2" +
                                ")"
                                : String.Empty
                            ) +
                        ")" +
                    ")";

                Guid? yearGuid;
                Guid? termGuid = null;

                if (isTerm) {
                    yearGuid = term.YearGuid;
                    termGuid = schedule.Guid;
                }
                else {
                    yearGuid = schedule.Guid;
                }

                args = new object[] {
                    yearGuid, termGuid
                };
            }

            IEnumerable<Class> classes;

            var conn = DataStore.GetConnection();
            classes = await conn.QueryAsync<Class>(sql, args);

            await conn.SetChildrenAsync(classes);
            await conn.SetChildrenAsync(classes.Select(x => x.Subject));

            return classes;
        }

        public async Task<IEnumerable<Class>> GetByDateRangeAndScheduleGuidsAsync(DateTime startDate, DateTime endDate, params Guid[] scheduleGuids) {
            var queryParams = new List<object> {
                startDate, endDate
            };
            queryParams.AddRange(scheduleGuids.Select(x => x.ToString()));

            var guidSqlParams = String.Join(",", scheduleGuids.Select((x, i) => "?" + (i + 2)));

            var conn = DataStore.GetConnection();
            var classes = (await conn.ExecuteDeferredQueryAsync<Class, Subject, AcademicYear, AcademicTerm, Class>($@"
				SELECT [C].*, [S].*, [AY].*, [AT].* FROM [Classes] AS [C]
				INNER JOIN [Subjects] AS [S] ON [C].[SubjectGuid] = [S].[Guid] AND [S].[DeletedAt] IS NULL
				LEFT OUTER JOIN [AcademicYears] AS [AY] ON [C].[YearGuid] = [AY].[Guid] AND [AY].[DeletedAt] IS NULL
				LEFT OUTER JOIN [AcademicTerms] AS [AT] ON [C].[TermGuid] = [AT].[Guid]
                WHERE [C].[DeletedAt] IS NULL AND (
                    ([C].[Type] = 0 AND [C].[Date] IS NOT NULL AND (?1 <= [C].[Date] AND [C].[Date] <= ?2)) OR
                    (
                        [C].[Type] = 1 AND (
                            (
                                [C].[StartDate] IS NOT NULL AND [C].[EndDate] IS NOT NULL AND (
					                ([C].[StartDate] <= ?1 AND ?1 <= [C].[EndDate]) OR
					                ([C].[StartDate] <= ?2 AND ?2 <= [C].[EndDate])				
				                )
                            )
                            OR (
                                (
                                    ([C].[YearGuid] IS NULL OR [C].[YearGuid] IN ({guidSqlParams})) OR 
                                    ([C].[TermGuid] IS NULL OR [C].[TermGuid] IN ({guidSqlParams}))
                                )
                                AND
                                (
                                    ([S].[YearGuid] IS NULL OR [S].[YearGuid] IN ({guidSqlParams})) OR 
                                    ([S].[TermGuid] IS NULL OR [S].[TermGuid] IN ({guidSqlParams}))
                                )
				            )
                        )
                    )
                )",
                queryParams.ToArray(),
                (c, s, ay, at) => {
                    c.Subject = s;
                    c.Year = ay;

                    if (c.Year != null && at != null && c.Year.Guid == at.YearGuid) {
                        at.Year = c.Year;

                        c.Term = at;
                    }

                    return c;
                }
            )).ToList();

            await conn.SetChildrenAsync(classes);
            await conn.SetChildrenAsync(classes.Select(x => x.Subject));
            await conn.SetChildrenAsync(classes.Where(x => x.Subject.Term != null).Select(x => x.Subject.Term));

            return classes;
        }

        public async Task<bool> HasClassesWithoutSchedule() {
            var conn = DataStore.GetConnection();
            return await conn.ExecuteScalarAsync<int>(String.Format(NoScheduleSql, "COUNT(*)")) > 0;
        }

        /// <summary>
        ///     Determines if any of the classes assigned to the
        ///     given <paramref name="year"/> are also assigned to
        ///     a term.
        /// </summary>
        public async Task<bool> AnyClassesForYearHaveTerm(AcademicYear year) {
            var conn = DataStore.GetConnection();
            return await conn.ExecuteScalarAsync<int>(
                "SELECT COUNT(*) FROM [Classes] AS [C] " +
                "INNER JOIN [Subjects] AS [S] ON [C].[SubjectGuid] = [S].[Guid] " +
                "WHERE [S].[DeletedAt] IS NULL AND [C].[DeletedAt] IS NULL AND " +
                "(" +
                    "COALESCE([C].[YearGuid], [S].[YearGuid]) = ?1 AND " +
                    "COALESCE([C].[TermGuid], [S].[TermGuid]) IS NOT NULL" +
                ")",
                year.Guid
            ) > 0;
        }

        public async Task<IEnumerable<Class>> GetJoinedByScheduleAsync(IAcademicSchedule schedule) {
            return (
                from c in await GetByScheduleAsync(schedule)
                where c.IsFromSchool
                select c
            );
        }

        /// <summary>
        ///     Gets potential classes this task could be due for.
        /// </summary>
        /// <param name="task">The task to get the classes for.</param>
        public async Task<IEnumerable<ScheduledClass>> GetForTaskAsync(Task task) {
            var entries = await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetForDateAsync(task.DueDate, AgendaSchedulerOptions.None);

            return entries.Where(x => x.IsClass && x.Base.Subject.Guid == task.Subject.Guid).Select(x => (ScheduledClass)x.Base);
        }

        /// <summary>
        ///     Gets classes which clash with the given exam.
        /// </summary>
        /// <param name="exam">The exam.</param>
        public async Task<IEnumerable<ScheduledClass>> GetConflictsForExamAsync(Exam exam) {
            DateTime startTime = exam.Date,
                     endTime = exam.Date.AddMinutes(exam.Duration);

            var entries = await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetForDateAsync(exam.Date.Date, AgendaSchedulerOptions.None);

            return entries.Where(x =>
                x.IsClass && (
                    //          EXAM START        END
                    //	             |             |
                    // 1       FROM--|--TO         |
                    // 2             |  FROM---TO  |
                    // 3      FROM---|-------------|---TO
                    // 4             |       FROM--|--TO
                    (x.StartTime <= startTime && startTime <= x.EndTime) ||
                    (startTime <= x.StartTime && x.EndTime <= endTime) ||
                    (x.StartTime <= startTime && endTime <= x.EndTime) ||
                    (x.StartTime <= endTime && endTime <= x.EndTime)
                )
            ).Select(x => (ScheduledClass)x.Base);
        }

        #endregion

        #region Api Calls

        public async AsyncTask Join(IEnumerable<Class> classes, CancellationToken cancellationToken = default(CancellationToken)) {
            using (var apiClient = Mvx.IoCProvider.Resolve<IHttpApiClient>()) {
                await apiClient.PostJsonAsync(
                    "/classes/join",
                    JsonConvert.SerializeObject(classes.DistinctBy(x => x.Guid).Select(x => x.Guid))
                );

                Mvx.IoCProvider.Resolve<ISyncService>().TrySync(SyncTriggerKind.Programmatic);
            }
        }
        public async AsyncTask Leave(Class cls) {
            if (cls == null) {
                throw new ArgumentNullException(nameof(cls));
            }

            using (var apiClient = Mvx.IoCProvider.Resolve<IHttpApiClient>()) {
                await apiClient.PostFormDataAsync("/classes/" + cls.Guid + "/leave", null);

                var conn = this.DataStore.GetConnection();

                await conn.DeleteAsync(cls);
            }
        }

        public async Task<IEnumerable<Student>> GetStudentsAsync(Class cls, CancellationToken cancellationToken) {
            if (cls == null) {
                throw new ArgumentNullException(nameof(cls));
            }

            string url = String.Format("/classes/{0}/students", cls.Guid);

            IEnumerable<Student> students;

            if (!Cache.Current.TryGet(url, out students)) {
                using (var apiClient = Mvx.IoCProvider.Resolve<IHttpApiClient>()) {
                    students = await apiClient.GetAsync<IEnumerable<Student>>(url, cancellationToken);

                    Cache.Current.Set(url, students);
                }
            }

            return students;
        }

        #endregion

        #region Search

        public override async Task<IEnumerable<Class>> GetByQueryTextAsync(string queryText) {
            queryText = String.Concat("%", queryText, "%");

            var conn = DataStore.GetConnection();
            var results = await conn.QueryAsync<Class>(
                "SELECT C.* FROM [Classes] AS C " +
                "INNER JOIN [Subjects] AS S ON C.SubjectGuid = S.Guid " +
                "WHERE [S].[DeletedAt] IS NULL AND [C].[DeletedAt] IS NULL AND " +
                "(" +
                    "S.Name LIKE ?1 OR " +
                    "C.Module LIKE ?1 OR " +
                    "C.Room LIKE ?1 OR " +
                    "C.Building LIKE ?1 OR " +
                    "C.Tutor LIKE ?1" +
                ")",
                queryText
            );

            await conn.SetChildrenAsync(results);

            return results.AsEnumerable();
        }

        #endregion

        #region Validation

        public override bool CheckForConflict(Class c1, Class c2) {
            return ConflictChecks.Classes(c1, c2);
        }

        public static bool CheckForConflict(TimeSpan aStart, TimeSpan aEnd, TimeSpan bStart, TimeSpan bEnd) {
            return (
                       (aStart <= bStart && bStart < aEnd) ||
                       (bStart <= aStart && aEnd < bEnd) ||
                       (aStart <= bStart && bEnd < aEnd) ||
                       (aStart < bEnd && bEnd < aEnd)
                   );
        }

        public static bool CheckForConflict(DateTime aStart, DateTime aEnd, DateTime bStart, DateTime bEnd) {
            return (
                       (aStart <= bStart && bStart < aEnd) ||
                       (bStart <= aStart && aEnd < bEnd) ||
                       (aStart <= bStart && bEnd < aEnd) ||
                       (aStart < bEnd && bEnd < aEnd)
                   );
        }

        public static class ConflictChecks {
            // ReSharper disable PossibleInvalidOperationException
            private static bool OneOffToOneOff(Class a, Class b) {
                return a.Date.Value.IsSame(b.Date.Value, DateTimeComparison.Day) &&
                       CheckForConflict(a.StartTime.Value, a.EndTime.Value, b.StartTime.Value, b.EndTime.Value);
            }

            private static bool OneOffToRecurring(Class a, Class b) {
                return false;

#warning Class conflict check not implemented

                //if (a.Type != ClassType.OneOff) throw new ArgumentException("Type must be OneOff", "a");
                //if (b.Type != ClassType.Recurring) throw new ArgumentException("Type must be Recurring", "b");

                //// This would require settings
                //if (b.DayType.Value == ClassDayType.Rotation) return false;

                //// ... and so would this
                //// plus the calculation may change so we can't assume it's always
                //// conflicting.
                //if (b.RotationWeek.HasValue && b.RotationWeek.Value > 0) return false;

                //// Class b has start and end dates but class a doesn't occur between them.
                //if (!(b.HasStartEndDates && b.StartDate.Value <= a.Date.Value && a.Date.Value <= b.EndDate.Value)) return false;

                //var dowFlag = a.Date.Value.DayOfWeek.ToFlag();

                //return (b.Days.Value & dowFlag) == dowFlag && CheckForConflict(a.StartTime, a.EndTime, b.StartTime, b.EndTime);
            }

            /// <summary>
            ///		Checks for a conflict between two classes with
            ///		a <see cref="Class.Type"/> of <see cref="ClassType.Recurring"/>.
            /// </summary>
            private static bool RecurringToRecurring(Class a, Class b) {
                return false;

#warning Class conflict check not implemented

                //return
                //(
                //    (
                //        a.HasStartEndDates != b.HasStartEndDates ||
                //        (a.HasStartEndDates && b.HasStartEndDates && CheckForConflict(a.StartDate.Value, a.EndDate.Value, b.StartDate.Value, b.EndDate.Value))
                //    ) &&
                //    (a.RotationWeek == b.RotationWeek || !a.RotationWeek.HasValue || a.RotationWeek == 0 || !b.RotationWeek.HasValue || b.RotationWeek == 0) &&
                //    (
                //        (b.Days.HasValue && a.Days.HasValue && ((b.Days.Value & a.Days.Value) == a.Days.Value)) ||
                //        (b.RotationDays.HasValue && a.RotationDays.HasValue && (b.RotationDays.Value & a.RotationDays.Value) > 0)
                //    ) &&
                //    CheckForConflict(a.StartTime, a.EndTime, b.StartTime, b.EndTime)
                //);
            }

            public static bool Classes(Class a, Class b) {
                if (a.Guid == b.Guid) return false;

                if (a.Type == ClassType.OneOff) {
                    if (b.Type == ClassType.OneOff) {
                        return OneOffToOneOff(a, b);
                    }
                    if (b.Type == ClassType.Recurring) {
                        return OneOffToRecurring(a, b);
                    }
                }
                else if (a.Type == ClassType.Recurring) {
                    if (b.Type == ClassType.Recurring) {
                        return RecurringToRecurring(a, b);
                    }
                    if (b.Type == ClassType.OneOff) {
                        return OneOffToRecurring(b, a);
                    }
                }

                return false;
            }
            // ReSharper restore PossibleInvalidOperationException
        }

        #endregion

        private void SetClassTimeGuids(Class item) {
            if (item.IsOneOff) {
                return;
            }

            item.Times.Apply(x => {
                x.ClassGuid = item.Guid;

                if (x.Guid.IsEmpty()) {
                    x.Guid = Guid.NewGuid();
                }
            });
        }

        protected override AsyncTask OnAdding(Class item) {
            SetClassTimeGuids(item);
            return AsyncTask.CompletedTask;
        }

        protected override async AsyncTask OnAdded(Class item) {
            if (item.Type == ClassType.OneOff || item.Times == null) {
                return;
            }

            var conn = DataStore.GetConnection();

            await conn.InsertAllAsync(item.Times, typeof(ClassTime));
        }

        protected override AsyncTask OnUpdating(Class item) {
            SetClassTimeGuids(item);
            return AsyncTask.CompletedTask;
        }

        protected override async AsyncTask OnUpdated(Class item) {
            if (item.IsOneOff) {
                return;
            }

            var timeGuids = item.Times.Select(x => x.Guid).ToList();

            var deleteParams = new List<object> {
                item.Guid
            };
            deleteParams.AddRange(timeGuids.Select(x => (object)x.ToString()).ToArray());

            await DataStore.GetConnection().RunInTransactionAsync(conn => {
                foreach (var time in item.Times) {
                    conn.InsertOrReplace(time, typeof(ClassTime));
                }
                
                if (timeGuids.Any()) {
                    conn.Execute(
                        $"DELETE FROM [ClassTimes] WHERE [ClassGuid] = ? AND [Guid] NOT IN ({String.Join(",", timeGuids.Select(x => "?"))})",
                        deleteParams.ToArray()
                    );
                }
            });
        }
    }
}
