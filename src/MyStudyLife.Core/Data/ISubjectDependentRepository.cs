﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyStudyLife.Data {
    public interface ISubjectDependentRepository<T> : IRepository<T>
        where T : SubjectDependentEntity {

        Task<IEnumerable<T>> GetBySubjectAsync(Subject subject);

        Task<IEnumerable<T>> GetBySubjectGuidAsync(Guid subjectGuid);

	    Task<IEnumerable<T>> GetByScheduleAsync(IAcademicSchedule schedule);
    }
}
