﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyStudyLife.Data {
    public interface ISubjectRepository : IRepository<Subject> {
        Task<IEnumerable<Subject>> GetByScheduleAsync(IAcademicSchedule schedule);
        Task<IEnumerable<Subject>> GetModifiableByScheduleAsync(IAcademicSchedule schedule);
    }
}