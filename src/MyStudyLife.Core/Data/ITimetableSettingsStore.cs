﻿using System.Threading.Tasks;
using MyStudyLife.Data.Settings;

namespace MyStudyLife.Data {
    public interface ITimetableSettingsStore {
        /// <summary>
        ///     Returns the the timetable for the current user, if NeedsTimetableSettings
        ///     returns true.
        /// </summary>
        Task<TimetableSettings> GetAsync();
        /// <summary>
        ///     Returns whether (due to legacy) the current user requires a timetable.
        /// </summary>
        /// <returns>bool</returns>
        Task<bool> NeedsTimetableSettingsAsync();
    }
}
