﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using SQLiteNetExtensionsAsync.Extensions;

namespace MyStudyLife.Data {
    public class TaskRepository : SubjectDependentRepository<Task>, ITaskRepository {

        public TaskRepository() {}
        public TaskRepository(IStorageProvider storageProvider) : base(storageProvider) { }

        #region Gets

        /// <summary>
        /// Retrieves tasks due for the class on the given date.
        /// </summary>
        /// <param name="sc">The class.</param>
        /// <param name="getOverdueForPastClass">
        /// True to get overdue tasks for past classes. Defaults to true.
        /// </param>
        public async Task<IEnumerable<Task>> GetForScheduledClassAsync(ScheduledClass sc, bool getOverdueForPastClass = true) {

	        DateTime today = DateTimeEx.Today;
	        DateTime scDate = sc.Date.StartOfDay();

            bool isPast = sc.Date <= today && sc.StartTime < DateTimeEx.Now.TimeOfDay;

            Expression<Func<Task, bool>> predicate;

            if (!isPast || getOverdueForPastClass) {
                predicate = t => t.SubjectGuid.Equals(sc.Class.SubjectGuid) && (
                    t.DueDate == scDate ||
                    (t.Progress < 100 && t.DueDate < today)
                );
            }
            else {
                predicate = t => t.SubjectGuid.Equals(sc.Class.SubjectGuid) && t.DueDate == scDate;
            }

	        return await GetAllNonDeletedAsync(predicate);
        }

        public async Task<TaskCounts> GetCountsForScheduledClassAsync(ScheduledClass sc) {
            // With clauses were only introduced for SQLite in 3.8.3
            // so only available in Android L and greater, not sure about iOS :(
            //string sql =
            //    "WITH [TasksForSubject] AS (" +
            //        "SELECT * FROM [Tasks] WHERE [Deleted] IS NULL AND [SubjectGuid] = ?1" +
            //    ")" +
            //    "SELECT " +
            //        "(SELECT COUNT(*) FROM [TasksForSubject] WHERE [DueDate] = ?2 AND [Progress] = 100) AS [Complete], " +
            //        "(SELECT COUNT(*) FROM [TasksForSubject] WHERE [DueDate] = ?2 AND [Progress] < 100) AS [Incomplete], " +
            //        "(SELECT COUNT(*) FROM [TasksForSubject] WHERE [DueDate] < ?2 AND [Progress] < 100) AS [Overdue]";

            string sql =
                "SELECT " +
                    "(SELECT COUNT(*) FROM [Tasks] WHERE [DeletedAt] IS NULL AND [SubjectGuid] = ?1 AND [DueDate] = ?2 AND [Progress] = 100) AS [Complete], " +
                    "(SELECT COUNT(*) FROM [Tasks] WHERE [DeletedAt] IS NULL AND [SubjectGuid] = ?1 AND [DueDate]= ?2 AND [Progress] < 100) AS [Incomplete], " +
                    "(SELECT COUNT(*) FROM [Tasks] WHERE [DeletedAt] IS NULL AND [SubjectGuid] = ?1 AND [DueDate] < ?2 AND [Progress] < 100) AS [Overdue]";

            return (await DataStore.GetConnection().QueryAsync<TaskCounts>(sql, sc.Subject.Guid, sc.Date.Date))
                .Single();
        }



        /// <summary>
        /// Gets revision tasks for the given exam.
        /// </summary>
        /// <param name="exam">The exam.</param>
        public Task<IEnumerable<Task>> GetForExamAsync(Exam exam) {
            return GetAllNonDeletedAsync(t => t.ExamGuid == exam.Guid);
        }

        public Task<IEnumerable<Task>> GetByDueDateAsync(DateTime dueDate) {
            return GetAllNonDeletedAsync(t => t.DueDate == dueDate.Date);
        }

        /// <summary>
        ///     Returns incomplete tasks with a due date that is in the given date range.
        /// </summary>
        /// <param name="startDate">
        ///     Optionally, the minimum date <see cref="Task.DueDate" /> can be.
	///
	///	When null, all incomplete tasks up until the <paramref name="endDate" /> will be returned.
        /// </param>
        /// <param name="endDate">
        ///     The maximum date <see cref="Task.DueDate" /> can be.
        /// </param>
        public Task<IEnumerable<Task>> GetIncompleteByDueDateAsync(DateTime? startDate, DateTime endDate) {
            if (startDate != null && endDate < startDate) {
                throw new ArgumentOutOfRangeException(nameof(endDate), endDate, "End date must be equal to or greater than now");
            }

            if (startDate == null) {
                return GetAllNonDeletedAsync(x => x.Progress < 100 && x.DueDate <= endDate);
            }

            return GetAllNonDeletedAsync(x => startDate.Value <= x.DueDate && x.DueDate <= endDate && x.Progress < 100);
        }

        /// <summary>
        ///     Returns tasks with a due date that is in the given date range.
        /// </summary>
        /// <param name="startDate">The minimum date <see cref="Task.DueDate" /> can be.</param>
        /// <param name="endDate">The maximum date <see cref="Task.DueDate" /> can be.</param>
        public Task<IEnumerable<Task>> GetByDueDateRangeAsync(DateTime startDate, DateTime endDate) {
            if (endDate < startDate) {
                throw new ArgumentOutOfRangeException(nameof(endDate), endDate, "End date must be equal to or greater than the start date");
            }

            return GetAllNonDeletedAsync(x => startDate <= x.DueDate && x.DueDate <= endDate);
        }

        #endregion

        #region UpdateProgress

        /// <summary>
        ///     A lightweight method that just updates the progress of
        ///     a task.
        /// </summary>
        public System.Threading.Tasks.Task UpdateProgressAsync(Task item) {
            return UpdateProgressAsync(new[] {item}, item.Progress);
        }

        public async System.Threading.Tasks.Task UpdateProgressAsync(IEnumerable<Task> items, int progress) {
            if (progress < 0 || 100 < progress) {
                throw new ArgumentOutOfRangeException(nameof(progress), "Must be between 0 and 100");
            }

            var itemList = items as IList<Task> ?? items.ToList();

            if (!itemList.Any()) {
                return;
            }

            var updatedAt = DateTime.UtcNow;
            DateTime? completedAt = null;

            if (progress == 100) {
                completedAt = updatedAt;
            }

            var sqlArgs = new object[3 + itemList.Count];

            sqlArgs[0] = updatedAt;
            sqlArgs[1] = progress;
            sqlArgs[2] = completedAt;

            for (int i = 0; i < itemList.Count; i++) {
                var item = itemList[i];

                item.Progress = progress;

                if (completedAt.HasValue) {
                    if (item.CompletedAt == null || (item.UpdatedAt != null && item.CompletedAt.Value == item.UpdatedAt.Value)) {
                        item.CompletedAt = completedAt;
                    }
                }
                else {
                    item.CompletedAt = null;
                }

                item.UpdatedAt = updatedAt;
                item.Syncify();

                sqlArgs[3 + i] = item.Guid;
            }

            var conn = DataStore.GetConnection();
            await conn.ExecuteAsync(
                "UPDATE " +
                    "[Tasks] " +
                "SET " +
                    "[" + Sync.SyncService.SyncFlagProp + "] = 1, " +
                    "[UpdatedAt] = ?, " +
                    "[Progress] = ?, " +
                    "[CompletedAt] = ? " +
                "WHERE " +
                    "[Guid] IN (" + String.Join(",", itemList.Select(x => "?")) + ") AND " +
                    "[DeletedAt] IS NULL",
                sqlArgs
            );

            TrySync();
        }

        #endregion

        #region Search

		public override async Task<IEnumerable<Task>> GetByQueryTextAsync(string queryText) {
			queryText = String.Concat("%", queryText, "%");

            var conn = DataStore.GetConnection();
			var results = await conn.QueryAsync<Task>(
				"SELECT T.* FROM [Tasks] AS T " +
				"INNER JOIN [Subjects] AS S ON T.SubjectGuid = S.Guid " +
				"WHERE [S].[DeletedAt] IS NULL AND [T].[DeletedAt] IS NULL AND " +
				"(" +
					"S.Name LIKE ?1 OR " +
                    "T.Title LIKE ?1 OR " +
					"T.Detail LIKE ?1" +
				")",
				queryText
			);

            await conn.SetChildrenAsync(results);

            return results.AsEnumerable();
		}

        #endregion

        #region Overrides of RepositoryBase<Task>

        public override bool CheckForConflict(Task a, Task b) {
            return a.Guid != b.Guid &&
                   a.SubjectGuid.Equals(b.SubjectGuid) &&
                   a.DueDate == b.DueDate &&
                   a.Title == b.Title;
        }

        #endregion
    }

    public sealed class TaskCounts {
        public int Complete { get; set; }

        public int Incomplete { get; set; }

        public int Overdue { get; set; }
    }
}
