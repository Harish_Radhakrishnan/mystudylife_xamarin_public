﻿using System;

namespace MyStudyLife.Data {
	[Flags]
	public enum DaysOfWeek {
		None = 0,
		Sunday = 1 << 0,
		Monday = 1 << 1,
		Tuesday = 1 << 2,
		Wednesday = 1 << 3,
		Thursday = 1 << 4,
		Friday = 1 << 5,
		Saturday = 1 << 6,
		Weekdays = Monday | Tuesday | Wednesday | Thursday | Friday,
		Weekends = Sunday | Saturday,
		All = Weekdays | Weekends
	}

	public static class DaysOfWeekExtensions {
		public static DaysOfWeek ToFlag(this DayOfWeek dow) {
			return (DaysOfWeek) (1 << (int) dow);
		}
	}

	public static class DaysOfWeekHelper {
		public static DaysOfWeek GetForRange(DateTime a, DateTime b) {
			if (a > b) {
			    var b2 = a;

				b = a;
                a = b2;
			}

			if ((b - a).TotalDays >= 7) {
				return DaysOfWeek.All;
			}

			DaysOfWeek daysInRange = DaysOfWeek.None;

			DateTime d = a;

			while (d <= b) {
				daysInRange |= d.DayOfWeek.ToFlag();

				d = d.AddDays(1d);
			}

			return daysInRange;
		}
	}
}
