﻿using System;
using Newtonsoft.Json;

namespace MyStudyLife.Data.Access {
    public static class SqlJsonConvert {
        private static readonly Lazy<JsonSerializerSettings> SerializerSettings = new Lazy<JsonSerializerSettings>(() => new JsonSerializerSettings {
            ObjectCreationHandling = ObjectCreationHandling.Replace,
            ContractResolver = SqlJsonContractResolver.Instance
        });
        
        public static string SerializeObject(object value) => JsonConvert.SerializeObject(value, SerializerSettings.Value);
        public static T DeserializeObject<T>(string json) => JsonConvert.DeserializeObject<T>(json, SerializerSettings.Value);
    }
}
