﻿using System;
using System.Reflection;
using Newtonsoft.Json.Serialization;

namespace MyStudyLife.Data.Access {
    /// <summary>
    ///     The contract resolver used for storing complex types as
    ///     JSON in SQL server. This is designed to match the naming
    ///     schema we have used for the SQL database (so DefaultContractResolver).
    /// </summary>
    public class SqlJsonContractResolver : DefaultContractResolver {
        #region Instance
        // As of V7, it's recommended this is a singleton vs. sharing cache.

        private static readonly Lazy<SqlJsonContractResolver> _instance = new Lazy<SqlJsonContractResolver>(() => new SqlJsonContractResolver());

        public static SqlJsonContractResolver Instance => _instance.Value;

        private SqlJsonContractResolver() { }
        #endregion

        protected override JsonContract CreateContract(Type objectType) {
            var contract = base.CreateContract(objectType);

            // Ensure enums are stored as ints, just like we do with normal SQL columns
            if (objectType.GetTypeInfo().IsEnum) {
                contract.Converter = null;
            }

            return contract;
        }
    }
}
