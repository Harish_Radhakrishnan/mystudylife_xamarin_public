﻿namespace MyStudyLife.Data.Access {
    /// <summary>
    ///     Tells Entity Framework to treat this object as
    ///     a <i>ComplexType</i> which will be stored serialized
    ///     in the database.
    /// </summary>
    public interface ISerialize {
        /// <summary>
        ///     Gets or sets the serialized form of the object.
        /// </summary>
        string Serialized { get; set; }
    }
}
