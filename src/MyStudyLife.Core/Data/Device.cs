﻿using Newtonsoft.Json;

namespace MyStudyLife.Data {
    [JsonObject]
    public class Device {
        /// <summary>
        /// The unique identifier of the device hardware.
        /// </summary>
        [JsonProperty("hardware_id")]
        public string HardwareId { get; set; }

        /// <summary>
        /// The name of the hardware.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The device's manufacturer.
        /// </summary>
        [JsonProperty("manufacturer")]
        public string Manufacturer { get; set; }

        /// <summary>
        /// The device's model number.
        /// </summary>
        [JsonProperty("model")]
        public string Model { get; set; }

        [JsonProperty("os_version")] public string OSVersion { get; set; }

        [JsonConstructor]
        public Device() { }
    }
}