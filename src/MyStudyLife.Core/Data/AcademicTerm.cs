using System;
using System.Globalization;
using MyStudyLife.Data.Validation;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [Table("AcademicTerms")]
    [JsonObject]
    public sealed class AcademicTerm : BindableBase, IAcademicSchedule {
        private DateTime _startDate, _endDate;

        [DoNotNotify]
        [PrimaryKey]
        public Guid Guid { get; set; }

        [DoNotNotify]
        [JsonIgnore]
        [ForeignKey(typeof(AcademicYear), OnDeleteAction = OnDeleteAction.Cascade)]
        public Guid YearGuid { get; set; }

        [DoNotNotify]
        [JsonIgnore]
        [ManyToOne]
        public AcademicYear Year { get; set; }

        [MaxLength(Data.MaxPrimaryTextFieldLength)]
        [Required]
        [StringLength(
            MinLength = Data.MinPrimaryTextFieldLength,
            MaxLength = Data.MaxPrimaryTextFieldLength
        )]
        public string Name { get; set; }

        [DoNotNotify]
        public DateTime StartDate {
            get => _startDate;
            set {
                if (SetPropertyEx(ref _startDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                }
            }
        }

        [DoNotNotify]
        public DateTime EndDate {
            get => _endDate;
            set {
                if (SetPropertyEx(ref _endDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                }
            }
        }

        [JsonIgnore, Ignore]
        public string Dates => DateTimeFormat.FormatDateRange(this.StartDate, this.EndDate);

        [JsonIgnore]
        public bool IsCurrent => DateTime.Today.IsBetween(this.StartDate, this.EndDate, DateTimeComparison.Day);

        public override string ToString() => this.ToString(CultureInfo.CurrentCulture);
        public string ToString(CultureInfo culture) => $"{this.Year} | {this.Name}";
    }
}