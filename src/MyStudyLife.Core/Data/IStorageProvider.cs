﻿using System.IO;
using System;
using System.Threading.Tasks;

namespace MyStudyLife.Data {
    /// <summary>
    /// The provider which interfaces with platform
    /// specific APIs and file storage.
    /// </summary>
    public interface IStorageProvider {
        #region Local Store

        DateTimeOffset GetLastModified(string path);

        /// <summary>
        /// Loads the contents from a file with the given name.
        ///
        /// Creates the file if it does not already exist, returning
        /// an empty string.
        /// </summary>
        /// <param name="fileName">The name of the file to load.</param>
        /// <returns>The file contents.</returns>
        Task<string> LoadFileContentsAsync(string fileName);

        Task<Stream> FileOpenReadAsync(string fileName);
        
        Stream FileOpenRead(string filePath);

        /// <summary>
        /// Writes the given <paramref name="contents"/> to a
        /// file with the given <paramref name="fileName"/>.
        ///
        /// Creates the file if it does not already exist.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="contents">The file contents eg. text, json xml.</param>
        System.Threading.Tasks.Task WriteFileContentsAsync(string fileName, string contents);

        /// <summary>
        /// Removes all files from the local store.
        /// </summary>
        System.Threading.Tasks.Task RemoveFilesAsync();

        #endregion

        #region Application Settings Store

        /// <summary>
        /// Gets the value for the stored application
        /// setting with the given <paramref name="settingName"/>.
        /// </summary>
        /// <typeparam name="T">The type stored in the setting.</typeparam>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The setting's value.</returns>
        T GetSetting<T>(string settingName);

        /// <summary>
        /// Adds or updates the application setting
        /// with the given <paramref name="settingName"/>
        /// and <paramref name="settingValue."/>
        /// </summary>
        /// <typeparam name="T">The type to store in the setting.</typeparam>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="settingValue">The setting's value.</param>
        void AddOrUpdateSetting<T>(string settingName, T settingValue);

        /// <summary>
        /// Removes the application setting
        /// with the given <paramref name="settingName"/>
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        void RemoveSetting(string settingName);

        #endregion
    }
}
