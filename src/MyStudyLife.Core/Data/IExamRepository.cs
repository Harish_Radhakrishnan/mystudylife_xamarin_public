﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyStudyLife.Data {
    public interface IExamRepository : ISubjectDependentRepository<Exam> {
        Task<IEnumerable<Exam>> GetBySubjectGuidAndDateRangeAsync(Guid subjectGuid, DateTime? startDate, DateTime? endDate);
        Task<IEnumerable<Exam>> GetForDateRangeAsync(DateTime startDate, DateTime endDate);
    }
}
