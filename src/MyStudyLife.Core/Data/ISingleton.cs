﻿using MyStudyLife.Data.Store;

namespace MyStudyLife.Data {
    public interface ISingleton {
        /// <summary>
        ///     Persists the current singleton to storage.
        /// </summary>
        System.Threading.Tasks.Task Save(ISingletonDataStore dataStore);
    }
}
