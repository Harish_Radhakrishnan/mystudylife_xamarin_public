﻿namespace MyStudyLife.Data {
    public interface IUserStore {
        User GetCurrentUser();

        System.Threading.Tasks.Task PersistUserAsync(User user);
    }
}