﻿using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data {
    public class SingletonDataRepository<T>
        where T : class, new() {

        #region Settings

        protected string GetJsonFileName() {
            return String.Concat(typeof(T).Name, ".json");
        }

        /// <summary>
        ///     The formatting which will be used when
        ///     saving the data.
        /// </summary>
        protected virtual Formatting JsonFormatting {
            get {
#if DEBUG
                return Formatting.Indented;
#else
                return Formatting.None;
#endif
            }
        }

        #endregion

        #region Repository Instances and Creation

        private static SingletonDataRepository<T> Instance { get; set; }

        public static SingletonDataRepository<T> GetInstance(IStorageProvider storageProvider) {
            return Instance ?? (Instance = new SingletonDataRepository<T>(storageProvider));
        }

        protected IStorageProvider StorageProvider { get; private set; }

        public SingletonDataRepository(IStorageProvider storageProvider) {
            if (storageProvider == null)
                throw new ArgumentNullException(nameof(storageProvider));

            this.StorageProvider = storageProvider;
        }

        /// <summary>
        ///     Ensures the data has been loaded from file.
        /// </summary>
        public async System.Threading.Tasks.Task EnsureInitialized() {
            if (this._stored == null && !_loadedFromFile)
                await GetStoredAsync();
        }

        #endregion

        private T _stored;
        private bool _loadedFromFile;

        /// <summary>
        ///     Retrieves the stored instance of type T.
        ///     Returns null if not exists.
        /// </summary>
        public async Task<T> GetStoredAsync() {
            if (this._stored == null && !_loadedFromFile) {
                string jsonContent = await this.StorageProvider.LoadFileContentsAsync(GetJsonFileName());

	            if (jsonContent != null) {
		            _stored = JsonConvert.DeserializeObject<T>(jsonContent);
	            }

	            _loadedFromFile = true;
            }

            return _stored;
        }

        /// <summary>
        ///     Retrieves the stored instance of type T.
        /// 
        ///     Returns the default value if it doesn't exist.
        /// </summary>
        /// <param name="defaultValue">The default value for <typeparamref name="T" /></param>>
        public async Task<T> GetStoredOrDefaultAsync(T defaultValue) {
            return _stored = await GetStoredAsync() ?? defaultValue;
        }

        /// <summary>
        ///     Serializes and stores object of type T.
        ///     Overwrites any existing instances of T.
        /// </summary>
        /// <param name="toPersist">The object to persist.</param>
        /// <returns>The T object just saved</returns>
        public async Task<T> PersistAsync(T toPersist) {
            this._stored = toPersist;

            string jsonContent = JsonConvert.SerializeObject(toPersist, JsonFormatting);

            await this.StorageProvider.WriteFileContentsAsync(GetJsonFileName(), jsonContent);

            return toPersist;
        }

        public void ClearCachedData() {
            this._stored = null;
        }
    }
}
