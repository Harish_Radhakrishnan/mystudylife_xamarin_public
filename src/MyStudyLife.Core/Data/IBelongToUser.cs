﻿namespace MyStudyLife.Data {
    /// <summary>
    ///     Signifies that the implementing
    ///     class is an entity which belongs
    ///     to a user.
    /// </summary>
    public interface IBelongToUser {
        /// <summary>
        ///     The ID of the user who this
        ///     entity belongs to.
        /// </summary>
        int UserId { get; set; }
    }

    public interface ICanBelongToUser {
        int? UserId { get; set; }
    }
}
