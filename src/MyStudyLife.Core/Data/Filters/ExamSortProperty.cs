﻿using MyStudyLife.Data.Annotations;

namespace MyStudyLife.Data.Filters {
	public enum ExamSortProperty {
		/// <summary>
		/// Sorts by the name of the subject.
		/// </summary>
		Subject = 0,

		/// <summary>
		/// Sorts by the module of the exam.
		/// </summary>
		Module = 1,

		/// <summary>
		/// Sorts by the date and start time of the exam.
		/// </summary>
		[EnumTitle("Date + Time")]
		DateTime = 2,

		/// <summary>
		/// Sorts by the length of the exam.
		/// </summary>
		Length = 3
	}
}
