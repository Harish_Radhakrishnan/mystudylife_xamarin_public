﻿namespace MyStudyLife.Data.Filters
{
    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }
}
