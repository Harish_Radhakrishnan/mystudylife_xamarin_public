﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;

namespace MyStudyLife.Data.Filters {
    [JsonObject]
    public class TaskFilter : FilterBase<Task, TaskFilterOption>, IFilter<Task> {
        public static readonly Predicate<Task> FilterCurrentPredicate = t => !t.IsComplete || t.DueDate.Date >= DateTimeEx.Today;
        public static readonly Predicate<Task> FilterPastPredicate = t => t.IsComplete && t.DueDate.Date < DateTimeEx.Today;

        private readonly Func<Task, object> _sortBySubjectOrderByKeySelector = t => t.Subject.Name;
        private readonly Func<Task, object> _sortByTitleOrderByKeySelector = t => t.Title;
        private readonly Func<Task, object> _sortByDueDateOrderByKeySelector = t => t.DueDate;
        private readonly Func<Task, object> _sortByProgressOrderByKeySelector = t => t.Progress;

        [JsonIgnore]
        private TaskSortProperty _sortProperty;

        [JsonProperty(PropertyName = "sort_property")]
        public TaskSortProperty SortProperty {
            get { return _sortProperty; }
            set { SetProperty(ref _sortProperty, value); }
        }

        /// <summary>
        ///     Gets the filter predicate for the current <see cref="TaskFilterOption"/>.
        /// </summary>
        public override Predicate<Task> GetFilterPredicate() {
            return this.Filter == TaskFilterOption.Past ? FilterPastPredicate : FilterCurrentPredicate;
        }

        public override IEnumerable<IGrouping<string, Task>> GetGrouped(IEnumerable<Task> items) {
            var grouped = new Dictionary<string, List<Task>>();

            foreach (var t in items.OrderBy(x => x.DueDate.Date)) {
                var key = t.IsOverdue ? "Overdue" : t.DueDate.ToString("D");

                if (!grouped.ContainsKey(key)) {
                    grouped.Add(key, new List<Task> {
                        t
                    });
                }
                else {
                    grouped[key].Add(t);
                }
            }

            return grouped.Select(x => (IGrouping<string, Task>)new Grouping<string, Task>(x.Key, x.Value));
        }

        /// <summary>
        /// Gets the key selector for the current
        /// <see cref="SortProperty"/>.
        /// </summary>
        public Func<Task, object> GetOrderByKeySelector() {
            switch (this.SortProperty) {
                case TaskSortProperty.Subject:
                    return _sortBySubjectOrderByKeySelector;
                case TaskSortProperty.Title:
                    return _sortByTitleOrderByKeySelector;
                case TaskSortProperty.Progress:
                    return _sortByProgressOrderByKeySelector;

                default:
                case TaskSortProperty.DueDate:
                    return _sortByDueDateOrderByKeySelector;
            }
        }



        /// <summary>
        /// Gets the default task filter.
        /// </summary>
        public static TaskFilter Default {
            get {
                return new TaskFilter {
                    Filter = TaskFilterOption.Current,
                    SortProperty = TaskSortProperty.DueDate,
                    SortDirection = SortDirection.Ascending
                };
            }
        }
    }
}
