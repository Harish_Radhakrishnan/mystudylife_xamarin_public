﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;

namespace MyStudyLife.Data.Filters {
    [JsonObject]
    public class ExamFilter : FilterBase<Exam, ExamFilterOption>, IFilter<Exam> {

        public static readonly Predicate<Exam> FilterCurrentPredicate = e => e.Date.AddMinutes(e.Duration) >= DateTimeEx.Now;
        public static readonly Predicate<Exam> FilterPastPredicate = e => e.Date.AddMinutes(e.Duration) < DateTimeEx.Now;

        private readonly Func<Exam, object> _sortBySubjectOrderByKeySelector = e => e.Subject.Name;
        private readonly Func<Exam, object> _sortByModuleOrderByKeySelector = e => e.Module;
        private readonly Func<Exam, object> _sortByDateTimeOrderByKeySelector = e => e.Date;
        private readonly Func<Exam, object> _sortByStartTimeOrderByKeySelector = e => e.Duration;

        [JsonIgnore]
        private ExamSortProperty _sortProperty;

        [JsonProperty(PropertyName = "sort_property")]
        public ExamSortProperty SortProperty {
            get => _sortProperty;
            set => SetProperty(ref _sortProperty, value);
        }

        /// <summary>
        ///     Gets the filter predicate for the current filter.
        /// </summary>
        public override Predicate<Exam> GetFilterPredicate() {
            return Filter == ExamFilterOption.Past ? FilterPastPredicate : FilterCurrentPredicate;
        }

        public override IEnumerable<IGrouping<string, Exam>> GetGrouped(IEnumerable<Exam> items) {
            return items.GroupBy(x => x.Date.Date.ToString("D")).Select(x => (IGrouping<string, Exam>)new Grouping<string, Exam>(x.Key, x));
        }

        /// <summary>
        /// Gets the key selector for the current
        /// <see cref="SortProperty"/>.
        /// </summary>
        public Func<Exam, object> GetOrderByKeySelector() {
            switch (SortProperty) {
                case ExamSortProperty.Subject:
                    return _sortBySubjectOrderByKeySelector;
                case ExamSortProperty.Module:
                    return _sortByModuleOrderByKeySelector;
                case ExamSortProperty.Length:
                    return _sortByStartTimeOrderByKeySelector;
                default:
                    return _sortByDateTimeOrderByKeySelector;
            }
        }

        /// <summary>
        /// Gets the default exam filter.
        /// </summary>
        public static ExamFilter Default =>
            new ExamFilter {
                Filter = ExamFilterOption.Current,
                SortProperty = ExamSortProperty.DateTime,
                SortDirection = SortDirection.Ascending
            };
    }
}
