﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;

namespace MyStudyLife.Data.Filters {
    public abstract class FilterBase<TEntity, TFilterOption> : BindableBase
        where TEntity : SubjectDependentEntity
        where TFilterOption : struct, IComparable, IFormattable {

        [JsonIgnore]
        private TFilterOption _filter;
        [JsonIgnore]
        private SortDirection _sortDirection;

        [JsonProperty(PropertyName = "filter")]
        public TFilterOption Filter {
            get { return _filter; }
            set { SetProperty(ref _filter, value); }
        }

        [JsonProperty(PropertyName = "sort_direction")]
        public SortDirection SortDirection {
            get { return _sortDirection; }
            set { SetProperty(ref _sortDirection, value); }
        }

        protected FilterBase() {
            if (!typeof(TFilterOption).GetTypeInfo().IsEnum) {
                throw new Exception("TFilterOption must be an enum");
            }
        }

        /// <summary>
        ///     Gets the filter predicate for the current <see cref="Filter"/>.
        /// </summary>
        public abstract Predicate<TEntity> GetFilterPredicate();

        public abstract IEnumerable<IGrouping<string, TEntity>> GetGrouped(IEnumerable<TEntity> items);

        public string GetFilterTitle() {
            // ReSharper disable once PossibleNullReferenceException
            return Enum.GetName(typeof(TFilterOption), this.Filter).ToLower();
        }
    }
}
