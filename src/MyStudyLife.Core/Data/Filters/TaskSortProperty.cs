﻿using MyStudyLife.Data.Annotations;

namespace MyStudyLife.Data.Filters
{
    public enum TaskSortProperty
    {
        /// <summary>
        /// Sorts by the name of the subject.
        /// </summary>
        Subject = 0,

        /// <summary>
        /// Sorts by the title of the task.
        /// </summary>
        Title = 1,

        /// <summary>
        /// Sorts by the due date of the task.
        /// </summary>
        [EnumTitle("Due Date")]
        DueDate = 2,

        /// <summary>
        /// Sorts by the progress of the task.
        /// </summary>
        Progress = 3
    }
}
