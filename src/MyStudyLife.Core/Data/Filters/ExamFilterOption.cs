﻿namespace MyStudyLife.Data.Filters {
    public enum ExamFilterOption {

        /// <summary>
        ///     Shows exams in the future.
        /// </summary>
        Current = 0,

        /// <summary>
        ///     Shows exams exams in the past.
        /// </summary>
        Past = 1
    }
}