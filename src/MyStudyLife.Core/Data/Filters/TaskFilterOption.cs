﻿namespace MyStudyLife.Data.Filters {
    public enum TaskFilterOption {
        /// <summary>
        ///     Shows overdue and/or completed tasks
        ///     due in the future.
        /// </summary>
        Current = 0,

        /// <summary>
        ///     Shows completed tasks due in the past.
        /// </summary>
        Past = 1
    }
}
