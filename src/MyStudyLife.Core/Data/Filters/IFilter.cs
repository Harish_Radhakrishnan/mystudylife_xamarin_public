﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MyStudyLife.Data.Filters {
    public interface IFilter<TEntity> : INotifyPropertyChanged
        where TEntity : SubjectDependentEntity {

        SortDirection SortDirection { get; }

        Func<TEntity, object> GetOrderByKeySelector();

        Predicate<TEntity> GetFilterPredicate();

        IEnumerable<IGrouping<string, TEntity>> GetGrouped(IEnumerable<TEntity> items);

        string GetFilterTitle();
    }
}
