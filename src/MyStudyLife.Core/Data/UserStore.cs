﻿using MyStudyLife.Data.Schools;
using MyStudyLife.Data.Settings;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data {
    public class UserStore : IUserStore {
        private static readonly object _lock = new object();

        private const string UserFileName = "User.json";

        private readonly IStorageProvider _storageProvider;

        private User _user;

        public UserStore(IStorageProvider storageProvider) {
            _storageProvider = storageProvider;
        }

        public User GetCurrentUser() {
            if (_user != null) {
                return _user;
            }

            User user;
            
            lock (_lock) {
                if (_user != null) {
                    return _user;
                }

                var stream = _storageProvider.FileOpenRead(UserFileName);

                if (stream == null || stream.Length == 0) {
                    stream?.Dispose();
                    return null;
                }

                using (stream)
                using (var streamReader = new StreamReader(stream))
                using (var jsonReader = new JsonTextReader(streamReader)) {
                    var userJson = JObject.Load(jsonReader);
                    var userTypeJson = userJson["type"];
                    var settingsJson = userJson["settings"];
                    var schoolJson = userJson["school"];
                    var pictureJson = userJson["picture"];

                    School school = null;

                    if (schoolJson.Type != JTokenType.Null && schoolJson.Type != JTokenType.None) {
                        school = new School {
                            Id = schoolJson.Value<int>("id"),
                            Name = schoolJson.Value<string>("name"),
                            HasLogo = schoolJson.Value<bool>("has_logo"),
                            ClassSharingEnabled = schoolJson.Value<bool>("class_sharing_enabled"),
                            IsManaged = schoolJson.Value<bool>("is_managed")
                        };

                        if (school.HasLogo) {
                            var logoJson = schoolJson["logo"];

                            if (logoJson.Type != JTokenType.Null && logoJson.Type != JTokenType.None) {
                                school.Logo = new SchoolLogo {
                                    Standard = logoJson.Value<string>("standard"),
                                    Cover = logoJson.Value<string>("cover"),
                                    Wide = logoJson.Value<string>("wide"),
                                    Emblem = logoJson.Value<string>("emblem")
                                };
                            }
                        }
                    }

                    user = new User {
                        Id = userJson.Value<int>("id"),
                        FirstName = userJson.Value<string>("first_name"),
                        LastName = userJson.Value<string>("last_name"),
                        Email = userJson.Value<string>("email"),
                        Type = userTypeJson.Type == JTokenType.Integer ? (UserType)userTypeJson.Value<int>() : (UserType)Enum.Parse(typeof(UserType), userTypeJson.Value<string>(), true),
                        SetupAt = userJson.Value<DateTime?>("setup_at"),
                        Picture = pictureJson.Type == JTokenType.String ? new Uri(pictureJson.Value<string>()) : null,
                        Settings = new UserSettings {
                            DefaultStartTime = TimeSpan.Parse(settingsJson.Value<string>("default_start_time")),
                            DefaultDuration = settingsJson.Value<int>("default_duration"),
                            FirstDayOfWeek = (DayOfWeek)settingsJson.Value<int>("first_day_of_week"),
                            IsRotationScheduleLettered = settingsJson.Value<bool>("is_rotation_schedule_lettered"),
                            DashboardTasksTimescale = (DashboardTasksTimescale?)settingsJson.Value<int?>("dashboard_tasks_timescale"),
                            DashboardExamsHidden = settingsJson.Value<bool?>("dashboard_exams_hidden"),
                            DashboardExamsTimescale = (DashboardExamsTimescale?)settingsJson.Value<int?>("dashboard_exams_timescale"),
                            DashboardExamsHiddenWhenNone = settingsJson.Value<bool?>("dashboard_exams_hidden_when_none")
                        },
                        School = school
                    };
                }
            }

            return _user = user;
        }

        public async AsyncTask PersistUserAsync(User user) {
            string jsonContent = JsonConvert.SerializeObject(user);

            await _storageProvider.WriteFileContentsAsync(UserFileName, jsonContent).ConfigureAwait(false);

            _user = user;

            // TODO: Move elsewhere!
            user?.ApplyLocaleSettings();
        }
    }
}