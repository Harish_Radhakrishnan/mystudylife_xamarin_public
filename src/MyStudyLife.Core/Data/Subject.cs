﻿using System;
using MyStudyLife.Data.Schools;
using MyStudyLife.Data.Validation;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [Table("Subjects")]
    public class Subject : BaseUserEntity, IComparable<Subject>, IBelongToSchoolOrUser, IScheduleRestrictable, IComparable {

        #region IBelongToSchoolOrUser

        [JsonIgnore]
        public bool IsFromSchool => this.SchoolId.HasValue;

        #endregion

        /// <summary>
        /// The subject's name
        /// </summary>
        [JsonProperty("name")]
        [Required]
		[StringLength(MinLength = Data.MinPrimaryTextFieldLength, MaxLength = Data.MaxPrimaryTextFieldLength)]
		public string Name { get; set; }

	    private string _color, _colorValue;

	    /// <summary>
	    /// The css name of the color of the subject.
	    /// </summary>
	    [JsonProperty("color")]
	    [Required]
	    public string Color {
	        get { return _color; }
            set {
                _color = value;
                _colorValue = null;
            }
	    }

        #region Academic Schedule

        [DoNotNotify]
        [ForeignKey(typeof(AcademicYear), OnDeleteAction = OnDeleteAction.Nullify)]
        public Guid? YearGuid { get; set; }

        private AcademicYear _year;

        [JsonIgnore]
        [ManyToOne]
        public AcademicYear Year {
            get { return _year; }
            set {
                _year = value;

                if (_year != null && _term != null && _term.Year == null && _term.YearGuid == _year.Guid) {
                    _term.Year = _year;
                }
            }
        }

        [DoNotNotify]
        [ForeignKey(typeof(AcademicTerm), OnDeleteAction = OnDeleteAction.Nullify)]
        public Guid? TermGuid { get; set; }

        private AcademicTerm _term;

        [JsonIgnore]
        [ManyToOne]
        public AcademicTerm Term {
            get { return _term; }
            set {
                _term = value;

                if (_term != null && _year != null && _term.Year == null && _term.YearGuid == _year.Guid) {
                    _term.Year = _year;
                }
            }
        }

        [JsonIgnore, Ignore]
        public Guid? ScheduleGuid {
            get { return this.TermGuid ?? this.YearGuid; }
        }

        [JsonIgnore]
        public IAcademicSchedule Schedule {
            get { return (IAcademicSchedule)this.Term ?? this.Year; }
        }

		#endregion

		#region IComparable

		public int CompareTo(Subject other) {
            // Null comes last, ReferenceEquals for a reason. 
            // (Some people implement equality operators to use to CompareTo!)
            if (ReferenceEquals(other, null)) {
                return 1;
            }

			return String.Compare(this.Name, other.Name, StringComparison.CurrentCultureIgnoreCase);
		}

		public int CompareTo(object obj) {
			return this.CompareTo((Subject) obj);
		}

		#endregion

		#region Overrides

		public override string ToString() {
			return this.Name;
		}

		#endregion
	}
}
