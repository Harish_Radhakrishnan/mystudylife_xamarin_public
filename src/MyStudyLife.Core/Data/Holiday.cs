﻿using System;
using MyStudyLife.Data.Validation;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [Table("Holidays")]
    [JsonObject]
    public class Holiday : BaseEntity {
        private DateTime _startDate, _endDate;
        
        [ForeignKey(typeof(AcademicYear), OnDeleteAction = OnDeleteAction.Cascade)]
        public Guid YearGuid { get; set; }

        [JsonIgnore, ManyToOne]
        public AcademicYear Year { get; set; }

        [Required]
        [StringLength(MinLength = Data.MinPrimaryTextFieldLength, MaxLength = Data.MaxPrimaryTextFieldLength)]
        public string Name { get; set; }

        [DoNotNotify]
        public DateTime StartDate {
            get { return _startDate; }
            set {
                if (SetPropertyEx(ref _startDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                    RaisePropertyChanged(nameof(Duration));
                }
            }
        }

        [DoNotNotify]
        public DateTime EndDate {
            get { return _endDate; }
            set {
                if (SetPropertyEx(ref _endDate, value)) {
                    RaisePropertyChanged(nameof(Dates));
                    RaisePropertyChanged(nameof(Duration));
                }
            }
        }

        public bool PushesSchedule { get; set; }

        [JsonIgnore, Ignore]
        public string Dates {
            get { return DateTimeFormat.FormatDateRange(this.StartDate, this.EndDate); }
        }

        [JsonIgnore, Ignore]
        public int Duration {
            get { return this.StartDate.Range(this.EndDate).CalendarDays; }
        }

        [JsonIgnore, Ignore]
        [DependsOn("PushesSchedule")]
        public bool ActualPushesSchedule {
            get { return EffectivePushesSchedule(this.Year); }
        }

        public bool EffectivePushesSchedule(AcademicYear year) {
            return this.PushesSchedule && (
                year.Scheduling.IsDayRotation || (year.Scheduling.IsWeekRotation && this.Duration >= AgendaScheduler.WeekRotationHolidayThreshold)
            );
        }
    }
}
