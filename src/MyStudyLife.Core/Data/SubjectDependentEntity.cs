﻿using System;
using System.Diagnostics;
using MyStudyLife.Data.Validation;
using Newtonsoft.Json;
using PropertyChanged;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [DebuggerDisplay("Subject={Subject.Name}")]
    public abstract class SubjectDependentEntity : BaseUserEntity {
        [JsonProperty("subject_guid")]
        [ForeignKey(typeof(Subject), OnDeleteAction = OnDeleteAction.Cascade)]
        [DoNotNotify]
        public Guid SubjectGuid { get; set; }

        [JsonProperty("subject"), Required]
        [ManyToOne]
        public Subject Subject { get; set; }

        // Used to fixes #MSL-79, but we shouldn't really care about this in our data models (called by Fody.PropertyChanged)
        protected virtual void OnSubjectChanged(Subject subject) { }

        // Prevents serializing subject but doesn't prevent deserializing
        // (like when getting class groups for join classes wizard).
        public bool ShouldSerializeSubject() => false;
    }
}
