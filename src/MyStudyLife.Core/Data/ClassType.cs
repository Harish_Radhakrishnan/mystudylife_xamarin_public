﻿using System.Runtime.Serialization;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.Data {
	public enum ClassType {
		/// <summary>
		///		A class which occurs once
		///		on the specified <see cref="Class.Date"/>.
		/// </summary>
		[EnumTitle("Does not repeat")]
        [EnumMember(Value = "one_off")]
        OneOff = 0,
		[EnumTitle("Repeats")]
        [EnumMember(Value = "recurring")]
        Recurring = 1
	}
}