﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data {
    public interface IRepository<T> : IDisposable
        where T : BaseUserEntity {

        Task<T> GetAsync(Guid guid);

        Task<IEnumerable<T>> GetAllNonDeletedAsync();

        Task<IEnumerable<T>> GetAllNonDeletedAsync(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> GetByQueryTextAsync(string queryText);

        Task<int> Count();

        Task<int> Count(Expression<Func<T, bool>> predicate);

        AsyncTask AddOrUpdateAsync(T item);

        AsyncTask DeleteAsync(T item);

        AsyncTask DeleteAsync(IEnumerable<T> items);
    }
}
