﻿using System;
using System.ComponentModel;
using System.Text;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Validation;
using MyStudyLife.Extensions;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [Table("ClassTimes")]
    [JsonObject]
    public class ClassTime : BindableBase, IComparable<ClassTime>, IComparable, IDbRow {
		[JsonIgnore] public const string DayOfWeekValidationGroup = "DayOfWeek";
		[JsonIgnore] public const string RotationDayValidationGroup = "RotationDay";

        [JsonIgnore] private const string DaysErrorMessage = "One or more days are required";
        [JsonIgnore] private const string RotationDaysErrorMessage = "One or more rotation days are required";

        private TimeSpan _startTime, _endTime;

        [JsonIgnore, Ignore]
        // Something to bind the ClassOccurrenceConverter to
        public ClassTime Self => this;

        [JsonProperty("guid")]
        [PrimaryKey]
        public Guid Guid { get; set; }

        [JsonProperty("class_guid")]
        [ForeignKey(typeof(Class), OnDeleteAction = OnDeleteAction.Cascade)]
        public Guid ClassGuid { get; set; }

        [JsonProperty("start_time")]
        [Required]
        [DoNotNotify]
        public TimeSpan StartTime {
            get { return _startTime; }
            set {
                if (SetPropertyEx(ref _startTime, value)) {
                    RaisePropertyChanged(nameof(Time));
                }
            }
        }

        [JsonProperty("end_time")]
        [Required]
        [DoNotNotify]
        public TimeSpan EndTime {
            get { return _endTime; }
            set {
                if (SetPropertyEx(ref _endTime, value)) {
                    RaisePropertyChanged(nameof(Time));
                }
            }
        }

        [JsonProperty("days")]
        [Required(Group = DayOfWeekValidationGroup, ErrorMessage = DaysErrorMessage)]
        [Range(MinimumValue = 1, Group = DayOfWeekValidationGroup, ErrorMessage = DaysErrorMessage)]
        public DaysOfWeek? Days { get; set; }

        [JsonProperty("rotation_days")]
        [Required(Group = RotationDayValidationGroup, ErrorMessage = RotationDaysErrorMessage)]
        [Range(MinimumValue = 1, Group = RotationDayValidationGroup, ErrorMessage = RotationDaysErrorMessage)]
        public RotationDays? RotationDays { get; set; }

        [JsonProperty("rotation_week")]
        public int? RotationWeek { get; set; }

        [JsonProperty]
        public string Room { get; set; }

        [JsonProperty]
        public string Building { get; set; }

        [JsonIgnore]
        [DependsOn(nameof(Room), nameof(Building))]
        public string Location {
            get {
                bool hasRoom = !String.IsNullOrWhiteSpace(this.Room);
                bool hasBuilding = !String.IsNullOrWhiteSpace(this.Building);

                if (!hasRoom && !hasBuilding) return null;

                return (hasRoom && hasBuilding ? $"{this.Room}, {this.Building}" : String.Concat(this.Room, this.Building));
            }
        }

        public string TeacherName { get; set; }

        [JsonIgnore]
        public string Time {
            get { return DateTimeFormat.FormatTimeRange(this.StartTime, this.EndTime); }
        }

        public ClassTime() {
            this.PropertyChanged += OnPropertyChangedInternal;
        }

        public ClassTime(Class cls) : this() {
            Check.NotNull(cls, nameof(cls));

            if (cls.Guid.IsEmpty()) {
                throw new ArgumentException("Class must not have an empty guid.", nameof(cls));
            }

            this.ClassGuid = cls.Guid;
        }

        private void OnPropertyChangedInternal(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName != "Self") {
                // ReSharper disable once ExplicitCallerInfoArgument
                RaisePropertyChanged("Self");
            }
        }

        public string GetOccurrenceText(TimetableSettings tSettings = null, DateComponentNaming naming = DateComponentNaming.Full, bool prependRotationWeek = true) {
            bool lettered = User.Current.Settings.IsRotationScheduleLettered;

            var sb = new StringBuilder();

            if (this.RotationDays.HasValue && this.RotationDays.Value > 0) {
                int dayCount = 0;

                for (int i = 1; i <= SchedulingOptions.MaxRotationDayCount; i++) {
                    int rDayFlag = 1 << (i - 1);

                    if (((int)this.RotationDays.Value & rDayFlag) == rDayFlag) {
                        sb.AppendFormat("{0}, ", lettered ? Humanize.NumberToLetter(i) : i.ToString());

                        dayCount++;
                    }
                }

                sb.Insert(0, dayCount != 1 ? "Days " : "Day ");
            }
            else if (this.Days.HasValue && this.Days.Value > 0) {
                if (this.RotationWeek.HasValue && this.RotationWeek.Value > 0 && prependRotationWeek) {
                    sb.Append("Week " + (lettered ? Humanize.NumberToLetter(this.RotationWeek.Value) : this.RotationWeek.Value.ToString())).Append(" ");
                }

                for (int i = 0; i < 7; i++) {
                    int dayFlag = 1 << i;

                    if (((int)this.Days.Value & dayFlag) == dayFlag) {
                        sb.AppendFormat("{0}, ", DateTimeFormat.GetDayName((DayOfWeek)i, naming));
                    }
                }
            }

            return sb.ToString().TrimEnd(' ', ',');
        }

        #region Utils

        public bool HasDay(DayOfWeek day) {
            var flag = day.ToFlag();

            return this.Days.HasValue && (this.Days.Value & flag) == flag;
        }

        public bool HasRotationDay(int rDay) {
            var flag = 1 << (rDay - 1);

            return this.RotationDays.HasValue && ((int)this.RotationDays.Value & flag) == flag;
        }

        #endregion

        public int CompareTo(ClassTime b) => Compare(this, b);
        public int CompareTo(object obj) => CompareTo((ClassTime)obj);

        /// <param name="dayFirst">
        ///     True to order by day first. If false orders by time first.
        /// </param>
        public static int Compare(ClassTime a, ClassTime b, bool dayFirst = false) {
            // Null comes last.
            // We use ReferenceEquals as some implementations of Equals use CompareTo
            if (Object.ReferenceEquals(a, b)) {
                return 0;
            }
            else if (Object.ReferenceEquals(a, null)) {
                return -1;
            }
            else if (Object.ReferenceEquals(b, null)) {
                return 1;
            }

            if (!dayFirst) {
                int startTimeCompare = a.StartTime.CompareTo(b.StartTime);

                if (startTimeCompare != 0) {
                    return startTimeCompare;
                }
            }

            if (a.Days.HasValue) {
                if (!b.Days.HasValue) {
                    return -1;
                }

                var daysCompare = L10n.GetFirstDayIndexInFlag(a.Days.Value).CompareTo(
                    L10n.GetFirstDayIndexInFlag(b.Days.Value)
                );

                if (daysCompare != 0) {
                    return daysCompare;
                }

                if (a.RotationWeek.HasValue && b.RotationWeek.HasValue) {
                    return a.RotationWeek.Value.CompareTo(b.RotationWeek.Value);
                }
                if (a.RotationWeek.HasValue && !b.RotationWeek.HasValue) {
                    return -1;
                }
                if (!a.RotationWeek.HasValue && b.RotationWeek.HasValue) {
                    return 1;
                }
            }
            else if (a.RotationDays.HasValue) {
                if (!b.RotationDays.HasValue) {
                    return 1;
                }

                return EnumEx.GetFirstFlagIndex<RotationDays>((int)a.RotationDays.Value).CompareTo(
                    EnumEx.GetFirstFlagIndex<RotationDays>((int)b.RotationDays.Value)
                );
            }

            return dayFirst ? a.StartTime.CompareTo(b.StartTime) : 0;
        }
    }
}
