﻿using System;
using MyStudyLife.ObjectModel;

namespace MyStudyLife.Data
{
	[Obsolete("Replaced with MyStudyLife.Data.Statistics.UpcomingStatistics.")]
	public class AgendaStatistics : BindableBase
	{
		private int _todayStatistic1Value;
		private string _todayStatistic1Label = "CLASSES REMAINING TODAY";
		private int _todayStatistic2Value;
		private string _todayStatistic2Label = "MINUTES UNTIL NEXT CLASS";
		private int _tasksStatistic1Value;
		private string _tasksStatistic1Label = "TASKS DUE TODAY";
		private int _tasksStatistic2Value;
		private string _tasksStatistic2Label = "TASKS DUE IN THE NEXT 3 DAYS";
		private int _examsStatistic1Value;
		private string _examsStatistic1Label = "EXAMS IN THE NEXT 14 DAYS";
		private int _examsStatistic2Value;
		private string _examsStatistic2Label = "INCOMPLETE REVISION TASKS";

		public int TodayStatistic1Value
		{
			get { return _todayStatistic1Value; }
			set { SetProperty(ref _todayStatistic1Value, value); }
		}

		public string TodayStatistic1Label // classes remaining
		{
			get { return _todayStatistic1Label; }
			set { SetProperty(ref _todayStatistic1Label, value); }
		}

		public int TodayStatistic2Value
		{
			get { return _todayStatistic2Value; }
			set { SetProperty(ref _todayStatistic2Value, value); }
		}

		public string TodayStatistic2Label // minutes until enxt class
		{
			get { return _todayStatistic2Label; }
			set { SetProperty(ref _todayStatistic2Label, value); }
		}

		public int TasksStatistic1Value
		{
			get { return _tasksStatistic1Value; }
			set { SetProperty(ref _tasksStatistic1Value, value); }
		}

		public string TasksStatistic1Label // tasks due today
		{
			get { return _tasksStatistic1Label; }
			set { SetProperty(ref _tasksStatistic1Label, value); }
		}

		public int TasksStatistic2Value
		{
			get { return _tasksStatistic2Value; }
			set { SetProperty(ref _tasksStatistic2Value, value); }
		}

		public string TasksStatistic2Label // task due in next 3 days
		{
			get { return _tasksStatistic2Label; }
			set { SetProperty(ref _tasksStatistic2Label, value); }
		}

		public int ExamsStatistic1Value
		{
			get { return _examsStatistic1Value; }
			set { SetProperty(ref _examsStatistic1Value, value); }
		}

		public string ExamsStatistic1Label // exams in the next 14
		{
			get { return _examsStatistic1Label; }
			set { SetProperty(ref _examsStatistic1Label, value); }
		}

		public int ExamsStatistic2Value
		{
			get { return _examsStatistic2Value; }
			set { SetProperty(ref _examsStatistic2Value, value); }
		}

		public string ExamsStatistic2Label // incomplete revision tasks
		{
			get { return _examsStatistic2Label; }
			set { SetProperty(ref _examsStatistic2Label, value); }
		}

		public AgendaStatistics()
		{
		}

		public void SetPlaceholderValues()
		{
			this.TodayStatistic1Label = "CLASSES REMAINING TODAY";
			this.TodayStatistic2Label = "MINUTES UNTIL NEXT CLASS";
			this.TasksStatistic1Label = "TASKS DUE TODAY";
			this.TasksStatistic2Label = "TASKS DUE IN THE NEXT 3 DAYS";
			this.ExamsStatistic1Label = "EXAMS IN THE NEXT 14 DAYS";
			this.ExamsStatistic2Label = "INCOMPLETE REVISION TASKS";
		}

	    public static AgendaStatistics Default
	    {
	        get
            {
                var stats = new AgendaStatistics();
            
                stats.SetPlaceholderValues();

	            return stats;
	        }
	    }
	}
}
