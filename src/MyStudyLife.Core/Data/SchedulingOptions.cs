﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using MyStudyLife.Data.Access;
using MyStudyLife.Data.Annotations;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MyStudyLife.Data.Serialization;

namespace MyStudyLife.Data {
    [JsonObject]
    public class SchedulingOptions : BindableBase {
        public const short DefaultWeekCount = 2;
        public const int DefaultDayCount = 6;
        public const short MinRotationCount = 2;
        public const short MaxRotationWeekCount = 4;
        public const int MaxRotationDayCount = 20;
        public const DaysOfWeek DefaultDays = DaysOfWeek.Weekdays;

        private short? _weekCount, _startWeek;
        private int? _dayCount, _startDay;
        private DaysOfWeek? _days;

        [JsonConverter(typeof(StringEnumConverter))]
        public SchedulingMode Mode { get; set; }

        /// <summary>
        ///     The scope to which these options apply, either
        ///     the academic year or auto, which will attempt to
        ///     use terms if they exist, or default back to the year.
        /// </summary>
        public SchedulingScope Scope { get; set; }

        public short? WeekCount {
            get { return _weekCount; }
            set { _weekCount = value; }
        }

        public short? StartWeek {
            get { return _startWeek; }
            set { _startWeek = value; }
        }

        public int? DayCount {
            get { return _dayCount; }
            set { _dayCount = value; }
        }

        public int? StartDay {
            get { return _startDay; }
            set { _startDay = value; }
        }

        public DaysOfWeek? Days {
            get { return _days == 0 ? null : _days; }
            set { _days = value; }
        }

        public SchedulingAdjustments Adjustments { get; set; } = new SchedulingAdjustments();

        [JsonIgnore]
        public bool IsFixed => this.Mode == SchedulingMode.Fixed;
        [JsonIgnore]
        public bool IsWeekRotation => this.Mode == SchedulingMode.WeekRotation;
        [JsonIgnore]
        public bool IsDayRotation => this.Mode == SchedulingMode.DayRotation;


        /// <summary>
        ///     Sets the default values for the current mode.
        /// </summary>
        /// <param name="ignoreIfNotNull">
        ///     If true, it will ignore any non null when setting
        ///     defaults. ie. Values won't be overwritten.
        /// </param>
        public void SetDefaults(bool ignoreIfNotNull = false) {
            if (this.IsFixed) {
                return;
            }

            switch (this.Mode) {
                case SchedulingMode.WeekRotation:
                    if (!this.WeekCount.HasValue || !ignoreIfNotNull) this.WeekCount = 2;
                    if (!this.StartWeek.HasValue || !ignoreIfNotNull) this.StartWeek = 1;
                    break;
                case SchedulingMode.DayRotation:
                    if (!this.DayCount.HasValue || !ignoreIfNotNull) this.DayCount = 6;
                    if (!this.StartDay.HasValue || !ignoreIfNotNull) this.StartDay = 1;
                    if (!this.Days.HasValue) this.Days = DaysOfWeek.Weekdays;
                    break;
            }
        }

        /// <summary>
        ///     Nulls any properties not applicable to the current mode.
        /// </summary>
        public void Purge() {
            // Use backing fields as to not raise prop changed (breaks WP).
            if (this.Mode != SchedulingMode.DayRotation) {
                this._dayCount = null;
                this._startDay = null;
                this._days = null;
            }

            if (this.Mode != SchedulingMode.WeekRotation) {
                this._weekCount = null;
                this._startWeek = null;
            }
        }

        public static SchedulingOptions Default => new SchedulingOptions {
            Mode = SchedulingMode.Fixed,
            Scope = SchedulingScope.Year
        };
    }

    public enum SchedulingMode {
        [EnumMember(Value = "fixed")]
        Fixed,
        [EnumMember(Value = "week_rotation")]
        [EnumTitle("Week Rotation")]
        WeekRotation,
        [EnumMember(Value = "day_rotation")]
        [EnumTitle("Day Rotation")]
        DayRotation
    }

    public enum SchedulingScope {
        [EnumMember(Value = "year")]
        Year,
        [EnumMember(Value = "auto")]
        Auto
    }

    public class SchedulingAdjustments : Collection<SchedulingAdjustment>, ISerialize {
        [JsonIgnore]
        public string Serialized
        {
            get { return SqlJsonConvert.SerializeObject(this); }
            set
            {
                if (String.IsNullOrEmpty(value)) {
                    return;
                }

                var items = SqlJsonConvert.DeserializeObject<List<SchedulingAdjustment>>(value);
                Items.Clear();

                foreach (var item in items) {
                    Items.Add(item);
                }
            }
        }

        public void Add(DateTime date, SchedulingAdjustmentType type, short value) => this.Add(new SchedulingAdjustment(date, type, value));
    }

    public class SchedulingAdjustment {
        [JsonConverter(typeof(DateJsonConverter))]
        public DateTime Date { get; private set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public SchedulingAdjustmentType Type { get; private set; }

        public short? Value { get; private set; }

        public SchedulingAdjustment(DateTime date, SchedulingAdjustmentType type, short value) {
            this.Date = date;
            this.Type = type;
            this.Value = value;
        }
    }

    public enum SchedulingAdjustmentType {
        [EnumMember(Value = "rotation_week")]
        RotationWeek = 1,
        [EnumMember(Value = "rotation_day")]
        RotationDay = 2
    }
}
