﻿using System;
using System.Collections.Generic;

namespace MyStudyLife.Data {
    public class ItemConflictException<T> : Exception {
        public IEnumerable<T> ConflictingItems { get; set; }

        public ItemConflictException(IEnumerable<T> conflictingItems) {
            this.ConflictingItems = conflictingItems;
        }
    }
}
