﻿namespace MyStudyLife.Data.Serialization {
    /// <summary>
    ///     For perf critical classes
    /// </summary>
    public interface IDeserializeJson {
        void SetValuesFromJson(string json);
    }
}