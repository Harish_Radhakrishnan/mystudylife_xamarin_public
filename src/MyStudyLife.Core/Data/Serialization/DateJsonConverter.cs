﻿using Newtonsoft.Json.Converters;

namespace MyStudyLife.Data.Serialization {
    public class DateJsonConverter : IsoDateTimeConverter {
        public DateJsonConverter() {
            this.DateTimeFormat = "yyyy-MM-dd";
        }
    }
}