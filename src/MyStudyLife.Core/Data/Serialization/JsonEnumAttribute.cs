﻿using System;

namespace MyStudyLife.Data.Serialization {
    [AttributeUsage(AttributeTargets.Enum)]
    public class JsonEnumAttribute : Attribute {
        public bool SerializeAsString { get; set; } = true;
    }
}
