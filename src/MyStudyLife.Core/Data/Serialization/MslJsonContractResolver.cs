﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace MyStudyLife.Data.Serialization {
    public class MslJsonContractResolver : DefaultContractResolver {
        #region Instance
        // As of V7, it's recommended this is a singleton vs. sharing cache.

        // ReSharper disable once InconsistentNaming
        private static readonly Lazy<MslJsonContractResolver> _instance = new Lazy<MslJsonContractResolver>(() => new MslJsonContractResolver());

        public static MslJsonContractResolver Default => _instance.Value;

        public MslJsonContractResolver() { }
        public MslJsonContractResolver(bool serializeEnumsAsStrings) {
            this.SerializeEnumsAsStrings = serializeEnumsAsStrings;
        }

        #endregion

        private static readonly Lazy<Regex> UnderscoreRegex = new Lazy<Regex>(() => new Regex(@"([A-Z])([A-Z][a-z])|([a-z0-9])([A-Z0-9])"));

        public bool SerializeEnumsAsStrings { get; } = true;

        protected override string ResolvePropertyName(string propertyName) => UnderscoreRegex.Value.Replace(propertyName, "$1$3_$2$4").ToLower();

        protected override JsonContract CreateContract(Type objectType) {
            var contract = base.CreateContract(objectType);

            var typeInfo = objectType.GetTypeInfo();
            if (typeInfo.IsEnum) {
                bool serializeEnumAsString = SerializeEnumsAsStrings;

                if (objectType == typeof (DayOfWeek)) {
                    serializeEnumAsString = false;
                }
                else {
                    var jsonEnumAttribute = typeInfo.GetCustomAttribute<JsonEnumAttribute>();

                    if (jsonEnumAttribute != null) {
                        serializeEnumAsString = jsonEnumAttribute.SerializeAsString;
                    }
                }

                if (serializeEnumAsString) {
                    contract.Converter = new StringEnumConverter();
                }
            }

            return contract;
        }
    }
}
