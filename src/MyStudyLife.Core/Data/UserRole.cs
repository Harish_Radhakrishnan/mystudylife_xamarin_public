﻿namespace MyStudyLife.Data {
    public enum UserRole {
        Student,
        Teacher,
        Lecturer,
        Other
    }
}
