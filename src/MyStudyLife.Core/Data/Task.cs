﻿using System;
using System.ComponentModel;
using MyStudyLife.Data.Validation;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
    [JsonObject]
	[Table("Tasks")]
    public class Task : SubjectDependentEntity {
        #region "Privates"
        // ReSharper disable InconsistentNaming

        [EditorBrowsable(EditorBrowsableState.Never), JsonProperty(PropertyName = "type")]
        public TaskType _type;
        [EditorBrowsable(EditorBrowsableState.Never), JsonProperty(PropertyName = "title")]
        public string _title;
        [EditorBrowsable(EditorBrowsableState.Never), JsonProperty(PropertyName = "detail")]
        public string _detail;
        [EditorBrowsable(EditorBrowsableState.Never), JsonProperty(PropertyName = "due_date")]
        public DateTime _dueDate;
        [EditorBrowsable(EditorBrowsableState.Never), JsonProperty(PropertyName = "progress")]
        public int _progress;

        // ReSharper restore InconsistentNaming
        #endregion

        /// <summary>
        /// The type of the task.
        /// </summary>
        [JsonIgnore]
        [Required]
        public TaskType Type {
            get { return _type; }
            set { SetProperty(ref _type, value); }
        }

        /// <summary>
        /// The Id of the exam (if any) that is associated with the task.
        /// </summary>
        [JsonProperty("exam_guid")]
        [DoNotNotify]
		[ForeignKey(typeof(Exam), OnDeleteAction = OnDeleteAction.Nullify)]
        public Guid? ExamGuid { get; set; }

        [JsonIgnore]
		[ManyToOne]
        public Exam Exam { get; set; }

        /// <summary>
        /// The task's title.
        /// </summary>
        [JsonIgnore]
        [Required]
        [StringLength(MinLength = Data.MinPrimaryTextFieldLength, MaxLength = Data.MaxPrimaryTextFieldLength)]
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        /// <summary>
        /// The task's detail.
        /// </summary>
        [JsonIgnore]
		[Column("Detail"), MaxLength(Int32.MaxValue)]
        public string Detail {
            get { return _detail; }
            set { SetProperty(ref _detail, value); }
        }

        /// <summary>
        /// The task's due date.
        /// </summary>
        [JsonIgnore]
        [Required]
        public DateTime DueDate {
            get { return _dueDate; }
            set { SetProperty(ref _dueDate, value); }
        }

        /// <summary>
        /// The completion progress of the task.
        /// </summary>
        [JsonIgnore]
        public int Progress {
            get { return _progress; }
            set { SetProperty(ref _progress, value); }
        }

        private DateTime? _completedAt;

        [JsonProperty("completed_at")]
        public DateTime? CompletedAt {
            get { return this.IsComplete ? _completedAt ?? this.UpdatedAt : null; }
            set { _completedAt = this.IsComplete ? value : null; }
        }

        #region Calculated / Helper

        /// <summary>
        ///     Returns true if the <see cref="Progress"/>
        ///     of the task is 100, false otherwise.
        /// </summary>
        [JsonIgnore]
        public bool IsComplete => this.Progress == 100;

        [JsonIgnore]
        public bool IsIncomplete {
            get { return this.Progress < 100 && this.DueDate.Date >= DateTimeEx.Today; }
        }

        [JsonIgnore]
        public bool IsIncompleteAndDueSoon {
            get {
                var diff = (this.DueDate.Date - DateTimeEx.Today).TotalDays;
            
                return this.IsIncomplete && 0 <= diff && diff <= 3;
            }
        }

        [JsonIgnore]
        public bool IsOverdue {
            get { return !this.IsComplete && this.DueDate.Date < DateTimeEx.Today; }
        }

        [JsonIgnore]
        public bool IsRevision => this.Type == TaskType.Revision;

        #endregion
    }
}
