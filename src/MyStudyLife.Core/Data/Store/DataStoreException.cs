﻿using System;

namespace MyStudyLife.Data.Store {
    public class DataStoreException : Exception {
        public Type Type { get; private set; }

        internal DataStoreException(Type type) {
            if (type == null)
                throw new ArgumentNullException();

            this.Type = type;
        }

        internal DataStoreException(Type type, string message) : base(message) {
            if (type == null)
                throw new ArgumentNullException();

            this.Type = type;
        }

        internal DataStoreException(Type type, string message, Exception inner) : base(message, inner) {
            if (type == null)
                throw new ArgumentNullException();

            this.Type = type;
        }
    }
}
