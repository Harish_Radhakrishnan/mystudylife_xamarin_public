﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.Data.Settings;
using SQLite;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensionsAsync.Extensions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data.Store
{
    public class DataStore : IDataStore, ISingletonDataStore {

		private static volatile bool _hasInit;
// ReSharper disable once InconsistentNaming
		private static readonly object m_initLock = new object();

	    public static bool TraceEnabled { get; set; } =
#if DEBUG
            true;
#else
            false;
#endif

        /// <summary>
        ///     Prevents SQLite checking tables / indexes
        ///     exist and trying to create them.
        /// </summary>
        /// <remarks>
        ///     Used in Windows Phone background task
        ///     where low memory usage (and CPU time) is
        ///     important.
        /// </remarks>
	    public static bool NoInitialization = false;

		/// <summary>
		///		Returns a new SQLiteConnection
		/// </summary>
		public SQLiteAsyncConnection GetConnection() {
			var folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			const string databaseName = "MyStudyLife.sqlite3";

            var connString = new SQLiteConnectionString(
                Path.Combine(folder, databaseName),
                SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.FullMutex,
                storeDateTimeAsTicks: false,
                storeTimeSpanAsTicks: true);
                
			var conn = new SQLiteAsyncConnection(connString);
			
            if (TraceEnabled) {
                conn.TimeExecution = true;
                conn.Tracer = (trace) => Mvx.IoCProvider.Resolve<ILogger<DataStore>>().LogInformation(trace);
                conn.Trace = true;
            }

            if (!_hasInit && !NoInitialization) {
				CreateAndMigrateTables(conn.GetConnection());
			}

			return conn;
		}

		#region Tables

        /// <summary>
        ///     A collection of types which will
        ///     automatically have tables created
        ///     by the datastore.
        /// </summary>
		public static IEnumerable<Type> SupportedCollectionTypes {
		    get {
		        return new [] {
		            typeof(AcademicYear),
		            typeof(AcademicTerm),
		            typeof(Holiday),
                    typeof(Subject),
                    typeof(Class),    
                    typeof(ClassTime),
                    typeof(Task),
                    typeof(Exam)
		        };
		    }
		}

		/// <summary>
		///		A collection of types to safeguard the possibly
		///		destructive methods against non-singleton types.
		/// </summary>
        public static IEnumerable<Type> SupportedSingletonTypes {
			get {
                return new [] {
			        typeof(TimetableSettings)
			    };
            }
		} 

		#endregion
		
		public static void CreateAndMigrateTables(SQLiteConnectionWithLock conn, bool force = false) {
			lock (m_initLock) {
                if (_hasInit && !force) return;

                using (conn.Lock()) {
                    Mvx.IoCProvider.Resolve<ILogger<DataStore>>().LogTrace("Initializing SQLite DB");

                    conn.SetForeignKeysPermissions(true);

                    foreach (Type type in SupportedCollectionTypes) {
                        conn.CreateTable(type);
                    }

                    foreach (Type type in SupportedSingletonTypes) {
                        conn.CreateTable(type);
                    }

                    _hasInit = true;
                }
            }

			Mvx.IoCProvider.Resolve<ILogger<DataStore>>().LogTrace("Initializing SQLite DB - done");
		}

		#region Implementation of IDataStore

		public async Task<IEnumerable<T>> GetAsync<T>(params Expression<Func<T, bool>>[] predicates) where T : class, new() {
            var conn = GetConnection();
            var query = conn.Table<T>();

// ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var p in predicates) {
                query = query.Where(p);
            }

            var items = await query.ToListAsync();
            await conn.SetChildrenAsync(items);

            return items;
		}

		public async Task<T> GetAsync<T>(object identity) where T : class, new() {
			var conn = GetConnection();
			var item = await conn.FindAsync<T>(identity);

			if (item != null) {
				await conn.SetChildrenAsync(new[] {item});
			}

			return item;
		}

		public async Task<IEnumerable<T>> GetAsync<T>() where T : class, new() {
            var conn = GetConnection();
            var items = await conn.Table<T>().ToListAsync();

            await conn.SetChildrenAsync(items);

            return items;
        }

        public async Task<int> CountAsync<T>(Expression<Func<T, bool>> predicate) where T : class, new() {
            return await GetConnection().Table<T>().CountAsync(predicate);
        }

		public async System.Threading.Tasks.Task AddAsync<T>(T item) where T : class, new() {
			if (item is IEnumerable) {
				throw new DataStoreException(typeof(T), "To update collections you must specify typeparam T explicitly.");
			}

			var conn = GetConnection();
			await conn.InsertWithChildrenAsync(item);
		}

		public async System.Threading.Tasks.Task AddAsync<T>(IEnumerable<T> items) where T : class, new() {
			var conn = GetConnection();
			foreach (var item in items) {
				await conn.InsertWithChildrenAsync(item);
			}
		}

		public async System.Threading.Tasks.Task UpdateAsync<T>(T item) where T : class, new() {
			if (item is IEnumerable) {
				throw new DataStoreException(typeof(T), "To update collections you must specify typeparam T explicitly.");
			}

			var conn = GetConnection();
            await conn.UpdateWithChildrenAsync(item);
		}

		public async System.Threading.Tasks.Task UpdateAsync<T>(IEnumerable<T> items) where T : class, new() {
			var conn = GetConnection();
			foreach (var item in items) {
				await conn.UpdateWithChildrenAsync(item);
			}
		}

		public async System.Threading.Tasks.Task RemoveAsync<T>(T item) where T : class, new() {
			if (item is IEnumerable) {
				throw new DataStoreException(typeof(T), "To update collections you must specify typeparam T explicitly.");
			}

            await GetConnection().DeleteAsync<T>(item);
		}

		public async System.Threading.Tasks.Task RemoveAsync<T>(IEnumerable<T> items) where T : class, new() {
            await GetConnection().RunInTransactionAsync(conn => {
                foreach (var item in items) {
                    conn.Delete(item);
                }
            });
		}

		public async System.Threading.Tasks.Task RemoveAsync<T>(object identity) where T : class, new() {
            await GetConnection().DeleteAsync<T>(identity);
		}

		public async System.Threading.Tasks.Task RemoveAsync<T>(params object[] identities) where T : class, new() {
            await GetConnection().RunInTransactionAsync(conn => {
                foreach (var ident in identities) {
                    conn.Delete<T>(ident);
                }
            });
		}

		#region Truncate

		public async AsyncTask TruncateAsync<T>() {
            await GetConnection().DeleteAllAsync<T>();
		}

		public async AsyncTask TruncateAllAsync() {
            await GetConnection().RunInTransactionAsync(conn => {
                conn.Execute("PRAGMA defer_foreign_keys = ON;");

                Action<Type> deleteAll = (t) => {
                    var tableName = t.GetTableName();

                    int rowsDeleted = conn.Execute($"DELETE FROM [{tableName}]");

                    Mvx.IoCProvider.Resolve<ILogger<DataStore>>().LogTrace("Deleted {0} rows from {1}", rowsDeleted, tableName);
                };

                SupportedCollectionTypes.Apply(deleteAll);
                SupportedSingletonTypes.Apply(deleteAll);
            });
		}

		#endregion

		#endregion

		#region Implementation of ISingletonDataStore

        private static readonly ConcurrentDictionary<string, object> SingletonCache = new ConcurrentDictionary<string, object>(); 

		public async Task<T> GetSingleton<T>() where T : new() {
			EnsureSingleton<T>();

		    string typeName = typeof (T).FullName;

            object singleton;

            if (SingletonCache.TryGetValue(typeName, out singleton)) {
                return (T) singleton;
            }
            
			var conn = GetConnection();
            return (T) (SingletonCache[typeName] = await conn.Table<T>().FirstOrDefaultAsync());
		}

		public async AsyncTask SetSingleton<T>(T value) where T : new() {
// ReSharper disable once CompareNonConstrainedGenericWithNull
			if (value == null) {
				await RemoveSingleton<T>();
			}

			EnsureSingleton<T>();

            string typeName = typeof(T).FullName;

			var conn = GetConnection();
			await conn.DeleteAllAsync<T>();
            await conn.InsertAsync(value, typeof (T));

			SingletonCache[typeName] = value;
		}

		public async AsyncTask RemoveSingleton<T>() where T : new() {
			EnsureSingleton<T>();

		    string typeName = typeof (T).FullName;

			SingletonCache.TryRemove(typeName, out _);

			var conn = GetConnection();
			await conn.DeleteAllAsync<T>();
		}

		private void EnsureSingleton<T>() {
			if (!SupportedSingletonTypes.Contains(typeof(T)))
				throw new DataStoreException(typeof(T), "Is not a singleton type. Amend the SingletonTypes property or call a non-singleton method.");
		}

		#endregion

		#region Implementation of IDisposable

		public void Dispose() {
			
		}

		#endregion
	}
}
