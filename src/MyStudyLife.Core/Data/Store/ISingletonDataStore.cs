﻿using System.Threading.Tasks;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data.Store {
	public interface ISingletonDataStore {
		Task<T> GetSingleton<T>() where T : new();

		AsyncTask SetSingleton<T>(T value) where T : new();

		AsyncTask RemoveSingleton<T>() where T : new();
	}
}
