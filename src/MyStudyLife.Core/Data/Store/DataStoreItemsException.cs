﻿using System;
using System.Collections.Generic;

namespace MyStudyLife.Data.Store {
    public class DataStoreItemsException<T> : Exception {
        public IEnumerable<T> ItemsAffected { get; private set; }

        internal DataStoreItemsException(T itemAffected, string message)
            : base(message) {
            this.ItemsAffected = new List<T> { itemAffected };
        }

        internal DataStoreItemsException(IEnumerable<T> itemsAffected, string message) : base(message) {
            this.ItemsAffected = itemsAffected;
        }
    }
}
