﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SQLite;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data.Store {
    public interface IDataStore : IDisposable {

		SQLiteAsyncConnection GetConnection();

        Task<T> GetAsync<T>(object identity) where T : class, new();

		Task<IEnumerable<T>> GetAsync<T>() where T : class, new();

	    Task<IEnumerable<T>> GetAsync<T>(params Expression<Func<T, bool>>[] predicates) where T : class, new();

        Task<int> CountAsync<T>(Expression<Func<T, bool>> filter) where T : class, new();

        AsyncTask AddAsync<T>(T item) where T : class, new();

		AsyncTask AddAsync<T>(IEnumerable<T> items) where T : class, new();

		AsyncTask UpdateAsync<T>(T item) where T : class, new();

		AsyncTask UpdateAsync<T>(IEnumerable<T> items) where T : class, new();

		AsyncTask RemoveAsync<T>(T item) where T : class, new();

		AsyncTask RemoveAsync<T>(IEnumerable<T> items) where T : class, new();

		AsyncTask RemoveAsync<T>(object identity) where T : class, new();

		AsyncTask RemoveAsync<T>(params object[] identities) where T : class, new();

		/// <summary>
		///		Erases all data of the given type (<typeparamref name="T"/>)
		///		stored in the current <see cref="IDataStore"/>.
		/// </summary>
		AsyncTask TruncateAsync<T>();

		/// <summary>
		///		Erases all data stored in the current <see cref="IDataStore"/>.
		/// </summary>
	    AsyncTask TruncateAllAsync();
    }
}
