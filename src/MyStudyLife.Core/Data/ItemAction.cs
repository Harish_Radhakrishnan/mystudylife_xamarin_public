﻿
namespace MyStudyLife.Data
{
    public enum ItemAction
    {
        /// <summary>
        /// An item has been/is being added.
        /// </summary>
        Add,

        /// <summary>
        /// An item has been/is being updated.
        /// </summary>
        Update,

        /// <summary>
        /// An item has been/is being deleted.
        /// </summary>
        Delete
    }
}
