﻿using System;
using System.Runtime.Serialization;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using PropertyChanged;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace MyStudyLife.Data {
	/// <summary>
	///		The base entity, contains identifier and sync properties.
	/// </summary>
	[JsonObject]
	[DataContract]
	public abstract class BaseEntity : BindableBase, IDbRow, ISyncable {
		[PrimaryKey, AutoIncrement]
		[JsonProperty("guid")]
        [DoNotNotify]
		public Guid Guid { get; set; }

		#region Sync Properties

        [JsonIgnore, Column(SyncService.SyncFlagProp)]
        [DoNotNotify]
		public bool NeedsSyncing { get; set; }

		/// <summary>
		/// The time the entity was created.
		/// </summary>
		[JsonProperty("created_at")]
        [DoNotNotify]
		public DateTime CreatedAt { get; set; }

		/// <summary>
		/// The time (if any) the entity was last updated.
		/// </summary>
        [JsonProperty("updated_at")]
        [DoNotNotify]
		public DateTime? UpdatedAt { get; set; }

		/// <summary>
		/// The time (if any) the entity was deleted.
		/// </summary>
        [JsonProperty("deleted_at")]
        [DoNotNotify]
		[Column("DeletedAt"), SoftDelete]
		public DateTime? DeletedAt { get; set; }

        [JsonProperty("timestamp")]
        [DoNotNotify]
		public string Timestamp { get; set; }

		#endregion
	}
}
