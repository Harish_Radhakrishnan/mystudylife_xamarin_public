﻿namespace MyStudyLife.Data.Caching {
    public interface ICache {
        string[] Keys { get; }

        void Set<T>(string key, T value, ICacheExpiration expiration = null);

        void Clear(string key);

        bool Exists(string key);

        bool TryGet<T>(string key, out T value);
    }

    public interface ICacheExpiration {
        bool HasExpired { get; }
    }
}
