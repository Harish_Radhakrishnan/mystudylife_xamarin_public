﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Base;

namespace MyStudyLife.Data.Caching {
    // TODO: When possible, updated to ConcurrentDictionary
    public sealed class Cache : ICache {
        private static readonly Lazy<Cache> Instance = new Lazy<Cache>();
        public static Cache Current => Instance.Value;

        private readonly Dictionary<string, CacheItem> _store;
        private readonly object _syncRoot = new object();

        public Cache() {
            _store = new Dictionary<string, CacheItem>();
        }

        public string[] Keys {
            get {
                lock (_syncRoot) {
                    return _store.Keys.ToArray();
                }
            }
        }

        public void Set<T>(string key, T value, ICacheExpiration expiration = null) {
            Check.NotNullOrEmpty(ref key, nameof(key));

            var cacheItem = new CacheItem(value, expiration ?? CacheExpiration.Default);
            lock (_syncRoot) {
                _store[key] = cacheItem;
            }
        }

        public void Clear(string key) {
            Check.NotNullOrEmpty(ref key, nameof(key));

            lock (_syncRoot) {
                _store.Remove(key);
            }
        }

        public bool Exists(string key) {
            Check.NotNullOrEmpty(ref key, nameof(key));

            lock (_syncRoot) {
                object value;
                return TryGet(key, out value);
            }
        }

        public bool TryGet<T>(string key, out T value) {
            Check.NotNullOrEmpty(ref key, nameof(key));

            CacheItem item;
            lock (_syncRoot) {
                _store.TryGetValue(key, out item);
            }

            if (item != null) {
                if (!item.HasExpired && item.Value != null) {
                    value = (T) item.Value;

                    return true;
                }

                lock (_syncRoot) {
                    _store.Remove(key);
                }

                item.Dispose();
            }

            value = default(T);

            return false;
        }

        class CacheItem : IDisposable {
            private readonly ICacheExpiration _expiration;

            public object Value { get; }

            public bool HasExpired => _expiration.HasExpired;

            public CacheItem(object value, ICacheExpiration expiration) {
                this.Value = value;
                this._expiration = expiration;
            }

            public void Dispose() {
                this._expiration?.DisposeIfDisposable();
            }
        }
    }

    public class CacheExpiration : ICacheExpiration {
        /// <summary>
        ///     The number of minutes the cache lives for.
        /// </summary>
        public const int DefaultCacheLifespan = 5;

        private DateTime? _expiresAt;

        public bool HasExpired => _expiresAt.HasValue && _expiresAt <= DateTimeEx.Now;

        private CacheExpiration() {

        }

        public static CacheExpiration Never => new CacheExpiration();

        public static CacheExpiration Default => ExpiresIn(DefaultCacheLifespan);

        public static CacheExpiration ExpiresAt(DateTime time) {
            return new CacheExpiration {
                _expiresAt = time
            };
        }

        public static CacheExpiration ExpiresIn(int minutes) {
            return new CacheExpiration {
                _expiresAt = DateTime.Now.AddMinutes(minutes)
            };
        }
    }
}
