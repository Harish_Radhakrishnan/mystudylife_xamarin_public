﻿using MyStudyLife.Data.Settings;

namespace MyStudyLife.Data.Services {
    public interface IUserService {
        System.Threading.Tasks.Task SetupUser(UserSettings settings = null);
        System.Threading.Tasks.Task SignOutAsync(bool force = false);
    }
}