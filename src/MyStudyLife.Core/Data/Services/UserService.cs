﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Authorization;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Net;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Data.Services
{
    public class UserService : RequireStorageProviderBase, IUserService {
        private readonly IMvxMessenger _messenger;
        private readonly INetworkProvider _networkProvider;
        private readonly IHttpApiClient _httpApiClient;
        private readonly ISyncService _syncService;
        private readonly IDataStore _dataStore;
        private readonly IUserStore _userStore;

        public UserService(
            IMvxMessenger messenger,
            IStorageProvider storageProvider,
            INetworkProvider networkProvider,
            IHttpApiClient httpApiClient,
            ISyncService syncService,
            IDataStore dataStore,
            IUserStore userStore
        ) : base(storageProvider) {
            this._messenger = messenger;
            this._networkProvider = networkProvider;
            this._httpApiClient = httpApiClient;
            this._syncService = syncService;
            this._dataStore = dataStore;
            this._userStore = userStore;
        }

        /// <summary>
        ///     Sends data gathered during setup to the server
        ///     and marks the current user as "setup".
        /// </summary>
        /// <param name="settings">
        ///     The updated user settings. Null if the user has chosen to skip the wizard
        /// </param>
        public async AsyncTask SetupUser(UserSettings settings = null) {
            var user = _userStore.GetCurrentUser();

            // If the user skips the wizard, we still want to set the first day of week
            settings = settings ?? user.Settings;

            user.SetupAt = DateTime.UtcNow;

			string jsonContent = null;

			if (settings != null) {
			    jsonContent =  JsonConvert.SerializeObject(
			        new UserSetupModel {
			            DefaultStartTime = settings.DefaultStartTime,
			            DefaultDuration = settings.DefaultDuration,
                        FirstDayOfWeek = settings.FirstDayOfWeek
			        }
			    );
            }

            await _httpApiClient.PostJsonAsync($"/users/{user.Id}/setup", jsonContent);

            if (settings != null) {
                settings.NeedsSyncing = false;

                if (!ReferenceEquals(user.Settings, settings)) {
                    user.Settings = settings;
                }
            }

            await _userStore.PersistUserAsync(user);
        }

        public AsyncTask SignOutAsync(bool force = false) => new SignOutHandler(this).SignOutAsync(force);

        private async AsyncTask PerformSignOutAsync() {
            try {
                await _httpApiClient.PostAsync("/oauth/revoke", false, new CancellationTokenSource(TimeSpan.FromSeconds(15)).Token);
            }
            // Fail silently
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<UserService>>().LogTrace("Failed to revoke access token: " + ex.ToLongString());
            }

            _syncService.Reset();

            var accessToken = AccessToken.GetCurrent(StorageProvider);

            accessToken?.Remove(StorageProvider);

            await _dataStore.TruncateAllAsync();

            await StorageProvider.RemoveFilesAsync();

            _messenger.Publish(new AuthStatusChangedEventMessage(this, false, AuthStatusChangeCause.SignOut));
        }

        /// <summary>
        ///     Encapsulates the lack of awaiting we can do on <see cref="SyncService"/>
        ///     since <see cref="UserService"/> is (or can be used as) a singleton.
        /// </summary>
        class SignOutHandler {
            private readonly UserService _userService;

            private readonly TaskCompletionSource<bool> _taskCompletionSource = new TaskCompletionSource<bool>();

            public SignOutHandler(UserService userService) {
                this._userService = userService;
            }

            public AsyncTask SignOutAsync(bool force = false) {
                // TODO: Refactor sync service so we don't have to do this nasty event handler stuff

                // This is not very scientific, we'd be better checking the same way sync service does
                var lastSynced = _userService._syncService.LastUpdated;

                if (force || (lastSynced.HasValue && (DateTime.UtcNow - lastSynced.Value).TotalMinutes <= 1d && _userService._syncService.Status != SyncStatus.Syncing)) {
                    _userService.PerformSignOutAsync().ContinueWith((t) => {
                        _taskCompletionSource.TrySetResult(true);
                    });
                }
                else {
                    if (!_userService._networkProvider.IsOnline) {
                        throw new SignOutException("Cannot verify data is synced. Use force flag.");
                    }

                    _userService._syncService.Error += SignOutOnSyncError;
                    _userService._syncService.Completed += SignOutOnSyncCompleted;

                    _userService._syncService.TrySync(SyncTriggerKind.Manual);
                }

                return _taskCompletionSource.Task;
            }

            private void SignOutOnSyncCompleted(object sender, SyncCompletedEventArgs e) {
                ((ISyncService)sender).Completed -= SignOutOnSyncCompleted;
                ((ISyncService)sender).Error -= SignOutOnSyncError;

                _userService.PerformSignOutAsync().ContinueWith((t) => {
                    _taskCompletionSource?.TrySetResult(true);
                });
            }

            private void SignOutOnSyncError(object sender, SyncErrorEventArgs e) {
                ((ISyncService)sender).Completed -= SignOutOnSyncCompleted;
                ((ISyncService)sender).Error -= SignOutOnSyncError;

                _taskCompletionSource?.TrySetException(
                    new SignOutException("Caught exception whilst syncing data", e.Exception)
                );
            }
        }

        private class UserSetupModel {
            [JsonProperty("default_start_time")]
            public TimeSpan DefaultStartTime { get; set; }

            [JsonProperty("default_duration")]
            public int DefaultDuration { get; set; }

            public DayOfWeek FirstDayOfWeek { get; set; }
        }
    }

    [Serializable]
    public class SignOutException : Exception {
        internal SignOutException(string message) : base(message) { }
        internal SignOutException(string message, Exception inner) : base(message, inner) { }
    }
}
