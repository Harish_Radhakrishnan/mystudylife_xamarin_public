﻿namespace MyStudyLife.Data {
    public static class Data {
        public const int MinDuration = 5;
        public const int MaxDuration = 720;

        /// <summary>
        ///     The minimum number of characters that a primary
        ///     text field require.
        /// 
        ///     A common field being a subject's name, task's title
        ///     etc.
        /// </summary>
        public const int MinPrimaryTextFieldLength = 1;
        public const int MaxPrimaryTextFieldLength = 50;
        
        /// <summary>
        ///     The number of characters that a common
        ///     text field should not exceed.
        /// 
        ///     A common field being module, room, building etc.
        /// </summary>
        public const int MaxCommonTextFieldLength = 40;
    }
}
