﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SQLiteNetExtensionsAsync.Extensions;

namespace MyStudyLife.Data {
    public class ExamRepository : SubjectDependentRepository<Exam>, IExamRepository {

        public ExamRepository() {}
        public ExamRepository(IStorageProvider storageProvider) : base(storageProvider) { }

        public async Task<IEnumerable<Exam>> GetBySubjectGuidAndDateRangeAsync(Guid subjectGuid, DateTime? startDate, DateTime? endDate) {
            Check.NotEmpty(ref subjectGuid, nameof(subjectGuid));

            if (startDate == null && endDate == null) {
                throw new ArgumentException($"Either {nameof(startDate)}, {nameof(endDate)} or both must be provided");
            }

            if (startDate > endDate) {
                throw new ArgumentException($"{nameof(startDate)} must be less than or equal to {nameof(endDate)}");
            }

            Expression<Func<Exam, bool>> predicate;

            if (startDate.HasValue && endDate.HasValue) {
                predicate = x => x.SubjectGuid == subjectGuid && startDate <= x.Date && x.Date <= endDate;
            }
            else if (startDate.HasValue) {
                predicate = x => x.SubjectGuid == subjectGuid && startDate <= x.Date;
            }
            else {
                predicate = x => x.SubjectGuid == subjectGuid && x.Date <= endDate;
            }

            return await GetAllNonDeletedAsync(predicate);
        }

        public Task<IEnumerable<Exam>> GetForDateRangeAsync(DateTime startDate, DateTime endDate) {
            startDate = startDate.Date;
            endDate = endDate.EndOfDay();

            return this.GetAllNonDeletedAsync(x => startDate <= x.Date && x.Date <= endDate);
        }

        #region Search

        public override async Task<IEnumerable<Exam>> GetByQueryTextAsync(string queryText) {
			queryText = String.Concat("%", queryText, "%");

			var conn = DataStore.GetConnection();
			var results = await conn.QueryAsync<Exam>(
				"SELECT E.* FROM [Exams] AS E " +
				"INNER JOIN [Subjects] AS S ON E.SubjectGuid = S.Guid " +
				"WHERE [S].[DeletedAt] IS NULL AND [E].[DeletedAt] IS NULL AND " +
				"(" +
					"S.Name LIKE ? OR " +
					"E.Module LIKE ? OR " +
					"E.Seat LIKE ? OR " +
					"E.Room LIKE ?"  +
				")",
				queryText,
				queryText,
				queryText,
				queryText
			);

			await conn.SetChildrenAsync(results);

			return results.AsEnumerable();
		}

        #endregion

        #region Overrides of RepositoryBase<Exam>

        public override bool CheckForConflict(Exam a, Exam b) {
            var startTime = b.Date;
            var endTime = b.Date.AddMinutes(b.Duration);

            return a.Guid != b.Guid &&
                   ((a.Date <= startTime && startTime <= a.Date.AddMinutes(a.Duration)) ||
                    (startTime <= a.Date && a.Date.AddMinutes(a.Duration) <= endTime) ||
                    (a.Date <= startTime && endTime <= a.Date.AddMinutes(a.Duration)) ||
                    (a.Date <= endTime && endTime <= a.Date.AddMinutes(a.Duration)));
        }

        #endregion
    }
}
