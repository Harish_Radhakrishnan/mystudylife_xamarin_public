﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;

namespace MyStudyLife.Resources {
    public interface ISchoolResource {
        Task<IEnumerable<Class>> GetClassesForSubjectsAsync(IEnumerable<Subject> subjects, IAcademicSchedule schedule);
        Task<IEnumerable<Class>> GetClassesForTeachersAsync(IEnumerable<Teacher> teachers, IAcademicSchedule schedule);

        /// <summary>
        ///     Gets a collection of subjects which belong to the current
        ///     user's school.
        /// </summary>
        Task<IEnumerable<Subject>> GetSubjectsAsync();

        /// <summary>
        ///     Gets a collection of teachers registered to the current
        ///     user's school.
        /// </summary>
        Task<IEnumerable<Teacher>> GetTeachersAsync();
    }
}