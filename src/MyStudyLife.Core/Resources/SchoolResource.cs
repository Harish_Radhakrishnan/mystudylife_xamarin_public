﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Net;

namespace MyStudyLife.Resources {
    public class SchoolResource : ISchoolResource {
        private readonly IHttpApiClient _httpApiClient;
        private readonly IUserStore _userStore;

        public SchoolResource(IHttpApiClient httpApiClient, IUserStore userStore) {
            this._httpApiClient = httpApiClient;
            this._userStore = userStore;
        }

        public async Task<IEnumerable<Teacher>> GetTeachersAsync() {
            var schoolId = (await GetAndAssertUserHasSchool()).School.Id;

            return await this._httpApiClient.GetAsync<IEnumerable<Teacher>>(
                $"schools/{schoolId}/teachers"
            );
        }

        public async Task<IEnumerable<Subject>> GetSubjectsAsync() {
            var schoolId = (await GetAndAssertUserHasSchool()).School.Id;

            return await this._httpApiClient.GetAsync<IEnumerable<Subject>>(
                $"schools/{schoolId}/subjects"
            );
        }

        public async Task<IEnumerable<Class>> GetClassesForTeachersAsync(IEnumerable<Teacher> teachers, IAcademicSchedule schedule) {
            var schoolId = (await GetAndAssertUserHasSchool()).School.Id;

            if (!teachers.Any()) {
                return Enumerable.Empty<Class>();
            }

            var query = teachers.Select(x => new KeyValuePair<string, string>("teacher_ids", x.Id.ToString())).ToList();

            query.Add(new KeyValuePair<string, string>("with_related_data", "true"));

            if (schedule != null) {
                query.Add(new KeyValuePair<string, string>("schedule_guid", schedule.Guid.ToString()));
            }

            return await this._httpApiClient.GetAsync<IEnumerable<Class>>(
                $"/schools/{schoolId}/teachers/classes",
                query.ToArray()
            );
        }

        public async Task<IEnumerable<Class>> GetClassesForSubjectsAsync(IEnumerable<Subject> subjects, IAcademicSchedule schedule) {
            var schoolId = (await GetAndAssertUserHasSchool()).School.Id;

            if (!subjects.Any()) {
                return Enumerable.Empty<Class>();
            }

            var query = subjects.Select(x => new KeyValuePair<string, string>("subject_guids", x.Guid.ToString())).ToList();

            query.Add(new KeyValuePair<string, string>("with_related_data", "true"));

            if (schedule != null) {
                query.Add(new KeyValuePair<string, string>("schedule_guid", schedule.Guid.ToString()));
            }

            return await this._httpApiClient.GetAsync<IEnumerable<Class>>(
                $"/schools/{schoolId}/subjects/classes",
                query.ToArray()
            );
        }

        private Task<User> GetAndAssertUserHasSchool() {
            var user = _userStore.GetCurrentUser();

            if (!user.HasSchool && !user.School.IsManaged) {
                throw new UnauthorizedAccessException("Cannot perform operation for user who does not belong to an unmanaged school.");
            }

            return System.Threading.Tasks.Task.FromResult(user);
        }
    }
}
