﻿using System.ComponentModel;

namespace MyStudyLife.ObjectModel {
    public delegate void PropertyChangedHandlerEx(object sender, PropertyChangedEventArgsEx e);

    public interface INotifyPropertyChangedEx : INotifyPropertyChanged {
        new event PropertyChangedHandlerEx PropertyChanged;
    }

    public class PropertyChangedEventArgsEx : PropertyChangedEventArgs {
        public object OldValue { get; private set; }

        public object NewValue { get; private set; }

        public PropertyChangedEventArgsEx(string propertyName, object oldValue, object newValue) : base(propertyName) {
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }
}
