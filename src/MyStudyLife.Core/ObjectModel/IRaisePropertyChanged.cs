﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MyStudyLife.ObjectModel {
    public interface IRaisePropertyChanged : INotifyPropertyChanged {
        void RaisePropertyChanged([CallerMemberName] string prop = null);
    }
}
