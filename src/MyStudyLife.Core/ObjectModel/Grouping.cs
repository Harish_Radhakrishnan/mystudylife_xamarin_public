﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace MyStudyLife.ObjectModel {
	public class Grouping<TKey, TItem> : IGrouping<TKey, TItem>, INotifyCollectionChanged {
		#region Equality members

		protected bool Equals(Grouping<TKey, TItem> other) {
			return EqualityComparer<TKey>.Default.Equals(Key, other.Key) && Equals(Items, other.Items);
		}

		public override int GetHashCode() {
			unchecked {
				return (EqualityComparer<TKey>.Default.GetHashCode(Key) * 397) ^ (Items != null ? Items.GetHashCode() : 0);
			}
		}

		#endregion
        
		public TKey Key { get; set; }

        public string Name { get; set; }

		public IList<TItem> Items { get; private set; }

	    public Grouping(TKey key) {
	        this.Key = key;
            this.Items = new List<TItem>();
	    }

		public Grouping(TKey key, IEnumerable<TItem> items) {
			this.Key = key;
			this.Items = new List<TItem>(items);
		}

		public virtual void AddItem(TItem item) {
            Check.NotNull(item, nameof(item));

			this.Items.Add(item);

            NotifyCollectionChanged(NotifyCollectionChangedAction.Add, item, this.Items.IndexOf(item));
		}

		public void InsertItem(int index, TItem item) {
            Check.NotNull(item, nameof(item));

            this.Items.Insert(index, item);

            NotifyCollectionChanged(NotifyCollectionChangedAction.Add, item, index);
		}

		public bool RemoveItem(TItem item) {
            Check.NotNull(item, nameof(item));

            int idx = this.Items.IndexOf(item);

			if (idx == -1) return false;

			this.Items.RemoveAt(idx);

            NotifyCollectionChanged(NotifyCollectionChangedAction.Remove, item, idx);

            return true;
		}

		#region Overrides

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Grouping<TKey, TItem>)obj);
		}

		#endregion

		#region IEnumerable<T> Members

		public IEnumerator<TItem> GetEnumerator() {
			return this.Items.GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			return this.Items.GetEnumerator();
		}

		#endregion

		#region Implementation of INotifyCollectionChanged

		public event NotifyCollectionChangedEventHandler CollectionChanged;

        protected void NotifyCollectionChanged(NotifyCollectionChangedAction action, TItem item, int index) {
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, item, index));
        }

		#endregion
	}
}
