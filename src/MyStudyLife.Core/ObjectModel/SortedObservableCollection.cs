﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using MyStudyLife.Data.Filters;
using MyStudyLife.Threading;

namespace MyStudyLife.ObjectModel {
    [CollectionDataContract]
    public class SortedObservableCollection<T> : ObservableCollection<T> {
        #region Sort / OrderBy / ThenBy Properties

        private SortDirection _sortDirection;
        private Func<T, object> _orderByKeySelector;
        private Func<T, object> _thenByKeySelector;

        [DataMember]
        public SortDirection SortDirection {
            get { return _sortDirection; }
            set {
                if (_sortDirection != value) {
                    _sortDirection = value;
                    ReSort();
                }
            }
        }

        [DataMember]
        public Func<T, object> OrderByKeySelector {
            get { return _orderByKeySelector; }
            set {
                if (_orderByKeySelector != value) {
                    _orderByKeySelector = value;
                    ReSort();
                }
            }
        }

        [DataMember]
        public Func<T, object> ThenByKeySelector {
            get { return _thenByKeySelector; }
            set {
                if (_thenByKeySelector != value) {
                    _thenByKeySelector = value;
                    ReSort();
                }
            }
        }

        #endregion

        // Default constructor for serialization.
        public SortedObservableCollection() {
        }

        #region Order By Constructors

        public SortedObservableCollection(Func<T, object> orderByKeySelector, SortDirection sortOrder = SortDirection.Ascending) {
            this._orderByKeySelector = orderByKeySelector;
            this._sortDirection = sortOrder;
        }

        public SortedObservableCollection(Func<T, object> orderByKeySelector, IEnumerable<T> collection, SortDirection sortOrder = SortDirection.Ascending)  {
            this._orderByKeySelector = orderByKeySelector;
			this._sortDirection = sortOrder;

			// Use add range as opposed to base constructor as we override InsertItem.
			this.AddRange(collection.OrderBy(orderByKeySelector).ToList());
        }

		public SortedObservableCollection(Func<T, object> orderByKeySelector, List<T> collection, SortDirection sortOrder = SortDirection.Ascending) {
            this._orderByKeySelector = orderByKeySelector;
            this._sortDirection = sortOrder;

			this.AddRange(collection.OrderBy(orderByKeySelector).ToList());
        }

        #endregion

        #region Then By Constructors

        public SortedObservableCollection(Func<T, object> orderByKeySelector, Func<T, object> thenByKeySelector, SortDirection sortOrder = SortDirection.Ascending)
            : this(orderByKeySelector) {
            this._thenByKeySelector = thenByKeySelector;
            this._sortDirection = sortOrder;
        }

        public SortedObservableCollection(Func<T, object> orderByKeySelector, Func<T, object> thenByKeySelector, IEnumerable<T> collection, SortDirection sortOrder = SortDirection.Ascending)
            : base(collection.OrderBy(orderByKeySelector).ThenBy(thenByKeySelector).ToList()) {

            this._orderByKeySelector = orderByKeySelector;
            this._thenByKeySelector = thenByKeySelector;
            this._sortDirection = sortOrder;
        }

        public SortedObservableCollection(Func<T, object> orderByKeySelector, Func<T, object> thenByKeySelector, List<T> list, SortDirection sortOrder = SortDirection.Ascending)
            : base(list.OrderBy(orderByKeySelector).ThenBy(thenByKeySelector).ToList()) {
            this._orderByKeySelector = orderByKeySelector;
            this._thenByKeySelector = thenByKeySelector;
            this._sortDirection = sortOrder;
        }

        #endregion

#pragma warning disable 618
		public void ReSort() {
            var orderedItems = _sortDirection == SortDirection.Ascending
                                    ? this.Items.OrderBy(_orderByKeySelector)
                                    : this.Items.OrderByDescending(_orderByKeySelector);

            if (_thenByKeySelector != null) {
                orderedItems = _sortDirection == SortDirection.Ascending
                                    ? orderedItems.ThenBy(_thenByKeySelector)
                                    : orderedItems.ThenByDescending(_orderByKeySelector);
            }

            var orderedItemsList = orderedItems.ToList();

            PortableDispatcher.InvokeIfRequired(() => {
                foreach (T item in orderedItemsList) {
                    this.Items.Remove(item);

                    this.Items.Insert(orderedItemsList.IndexOf(item), item);
                }
            });
        }

        public void AddRange(IEnumerable<T> items) {
            var existingItems = base.Items.ToList();
            existingItems.AddRange(items);

            var orderedItems = _sortDirection == SortDirection.Ascending
                                    ? this.Items.OrderBy(_orderByKeySelector)
                                    : this.Items.OrderByDescending(_orderByKeySelector);

            if (_thenByKeySelector != null) {
                orderedItems = _sortDirection == SortDirection.Ascending
                                    ? orderedItems.ThenBy(_thenByKeySelector)
                                    : orderedItems.ThenByDescending(_orderByKeySelector);
            }

            IList<T> orderedItemsList = orderedItems.ToList();

            PortableDispatcher.InvokeIfRequired(() => {
                foreach (T item in items) {
                    InsertItem(0, item);
                }
            });
        }

        /// <param name="removeItems">True to remove items which do not exist in the <paramref name="items"/> collection from the collection.</param>
        public void AddWhereNotExists(IEnumerable<T> items, bool removeItems = true) {
            var itemsToRemove = removeItems ? this.Items.Where(t => !items.Any(_t => _t.Equals(t))).ToList() : null;
            var itemsToAdd = items.Where(t => !this.Items.Any(_t => _t.Equals(t))).ToList();

            if (removeItems)
                PortableDispatcher.InvokeIfRequired(() => this.RemoveRange(itemsToRemove));

            AddRange(itemsToAdd);
        }

        /// <param name="removeItems">True to remove items which do not exist in the <paramref name="items"/> collection from the collection.</param>
        public void AddOrUpdateWhereNotExists(IEnumerable<T> items, bool removeItems = true) {
            var itemsToRemove = removeItems ? this.Items.Where(t => !items.Any(_t => _t.Equals(t))).ToList() : null;
            var itemsToAdd = items.Where(t => !this.Items.Any(_t => _t.Equals(t))).ToList();

            if (removeItems)
                PortableDispatcher.InvokeIfRequired(() => this.RemoveRange(itemsToRemove));

            var itemsToUpdate = items.Where(t => this.Items.Any(_t => _t.Equals(t))).ToList();

            foreach (var item in itemsToUpdate) {
                item.CopyPropertyValuesTo(this.Items.First(t => t.Equals(item)));
            }

            AddRange(itemsToAdd);
        }

        #region Overrides

        protected override void InsertItem(int index, T item) {
            IList<T> items = base.Items.ToList();
            items.Add(item);

            IOrderedEnumerable<T> orderedItems = _sortDirection == SortDirection.Ascending ? items.OrderBy(_orderByKeySelector)
                                                                                   : items.OrderByDescending(_orderByKeySelector);

            if (_thenByKeySelector != null) {
                orderedItems = _sortDirection == SortDirection.Ascending ? orderedItems.ThenBy(_thenByKeySelector)
                                                                 : orderedItems.ThenByDescending(_orderByKeySelector);
            }

            IList<T> orderedItemsList = orderedItems.ToList();

            // Insert item can only be called once as it triggers the CollectionChanged event.
            try {
                PortableDispatcher.InvokeIfRequired(() => base.InsertItem(orderedItemsList.IndexOf(item), item));
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
        }

        protected override void ClearItems() {
            PortableDispatcher.InvokeIfRequired(() => base.ClearItems());
		}
#pragma warning restore 618

        #endregion
    }
}
