﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using MvvmCross.WeakSubscription;

namespace MyStudyLife.ObjectModel {
    // TODO: Deep change support (look INPC properties)
    public class ChangeTracker : IDisposable {
        private IDisposable _subscription;

        private readonly INotifyPropertyChanged _obj;
        private readonly Type _objType;

        private readonly Dictionary<string, object> _originalValues;
        private readonly HashSet<string> _changedProperties = new HashSet<string>();

        public EmptyStringComparison EmptyStringComparison { get; set; }

        public ChangeTracker(INotifyPropertyChanged obj) {
            Check.NotNull(obj, nameof(obj));

            _obj = obj;
            _objType = obj.GetType();

            var properties = _objType.GetRuntimeProperties().Where(x => x.CanWrite);

            _originalValues = new Dictionary<string, object>();

            foreach (var prop in properties) {
                _originalValues.Add(prop.Name, prop.GetValue(obj));
            }

            _subscription = obj.WeakSubscribe(OnPropertyChanged);

            EmptyStringComparison = EmptyStringComparison.TreatWhiteSpaceAsNull;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            // Ignore properties that we aren't tracking (such as getter only props)
            if (_originalValues.ContainsKey(e.PropertyName)) {
                _changedProperties.Add(e.PropertyName);
            }
        }

        public bool HasChanges() {
            if (_changedProperties.Count > 0) {
                foreach (var propName in _changedProperties) {
                    var prop = _objType.GetRuntimeProperty(propName);

                    var originalValue = _originalValues[propName];
                    var currentValue = prop.GetValue(_obj);

                    if (prop.PropertyType == typeof (string)) {
                        if (EmptyStringComparison == EmptyStringComparison.TreatEmptyAsNull) {
                            originalValue = String.IsNullOrEmpty((string)originalValue) ? null : originalValue;
                            currentValue = String.IsNullOrEmpty((string)currentValue) ? null : currentValue;
                        }
                        else if (EmptyStringComparison == EmptyStringComparison.TreatWhiteSpaceAsNull) {
                            originalValue = String.IsNullOrWhiteSpace((string)originalValue) ? null : originalValue;
                            currentValue = String.IsNullOrWhiteSpace((string)currentValue) ? null : currentValue;
                        }
                    }

                    var originalValueIsNull = originalValue == null;
                    var currentValueIsNull = currentValue == null;

                    if (originalValueIsNull != currentValueIsNull) {
                        return true;
                    }

                    if (!originalValueIsNull /* && !currentValueIsNull */ && !originalValue.Equals(currentValue)) {
                        return true;
                    }
                }
            }

            return false;
        }

        public void Dispose() {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }
        }
    }

    public enum EmptyStringComparison {
        None,
        TreatEmptyAsNull,
        TreatWhiteSpaceAsNull
    }

    public static class ChangeTrackerExtensions {
        public static ChangeTracker TrackChanges(this INotifyPropertyChanged obj) {
            return new ChangeTracker(obj);
        }
    }
}
