﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;

namespace MyStudyLife.ObjectModel {
    internal static class RangeEnforcerHelper {
        internal static PropertyInfo GetPropertyInfoFromExpression(LambdaExpression ex) {
            MemberExpression memberEx;

            // Unary expression handles non nullable on a nullable expression
            if (ex.Body is UnaryExpression) {
                memberEx = (MemberExpression)((UnaryExpression)ex.Body).Operand;
            }
            else {
                memberEx = (MemberExpression)ex.Body;
            }

            return (PropertyInfo)memberEx.Member;
        }
    }

    public sealed class DateRangeEnforcer<T> : IDisposable where T : INotifyPropertyChanged {
        private IDisposable _subscription;

        private readonly Func<T, DateTime?> _startProperty, _endProperty;
        private readonly PropertyInfo _startPropertyInfo, _endPropertyInfo;

        private bool _changedEndDate, _ignoreStartDateChange, _ignoreEndDateChange;

        private DaysOfWeek _allowedDays = DaysOfWeek.All;

        public int MinRangeDays { get; set; } = 1;

        /// <summary>
        ///     The minimum date that the enforcer will allow.
        /// </summary>
        public DateTime? MinDate { get; set; }

        /// <summary>
        ///     The maximum date that the enforcer will allow.
        /// </summary>
        public DateTime? MaxDate { get; set; }

        /// <summary>
        ///     Days that the enforcer is allowed to use when
        ///     updating the start/end dates. Defaults to all days.
        ///
        ///     This does not prevent a day in allowed days being
        ///     set outside the enforcer.
        /// </summary>
        public DaysOfWeek AllowedDays {
            get { return _allowedDays; }
            set {
                if (_allowedDays == DaysOfWeek.None) {
                    throw new ArgumentOutOfRangeException(nameof(value), "Cannot be 'DaysOfWeek.None'");
                }

                _allowedDays = value;
            }
        }

        public DateRangeEnforcer(T item, Expression<Func<T, DateTime?>> startProperty, Expression<Func<T, DateTime?>> endProperty) {
            _subscription = item.WeakSubscribe(ItemOnPropertyChanged);

            _startProperty = startProperty.Compile();
            _endProperty = endProperty.Compile();

            _startPropertyInfo = RangeEnforcerHelper.GetPropertyInfoFromExpression(startProperty);
            _endPropertyInfo = RangeEnforcerHelper.GetPropertyInfoFromExpression(endProperty);
        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var item = (T) sender;

            bool isStart = e.PropertyName == _startPropertyInfo.Name;
            bool isEnd = e.PropertyName == _endPropertyInfo.Name;

            if ((!isStart && !isEnd) || (isStart && _ignoreStartDateChange) || (isEnd && _ignoreEndDateChange)) {
                return;
            }

            var nullableStartDate = _startProperty(item);
            var nullableEndDate = _endProperty(item);

            if (nullableStartDate == null || nullableEndDate == null) {
                return;
            }

            var startDate = nullableStartDate.Value;
            var endDate = nullableEndDate.Value;

            if (isStart) {
                if (MinDate.HasValue && startDate < MinDate.Value) {
                    startDate = SetStartDate(item, MinDate.Value, endDate, true);
                }
                else if (MaxDate.HasValue && startDate > MaxDate.Value.AddDays(-MinRangeDays)) {
                    startDate = SetStartDate(item, MaxDate.Value.AddDays(-MinRangeDays), endDate, true);
                }

                var argsEx = e as PropertyChangedEventArgsEx;

                if (argsEx == null) {
                    throw new Exception($"{typeof(T).FullName}.{_startPropertyInfo.Name} must call PropertyChanged with PropertyChangedEventArgsEx");
                }

                var diffBeforeChange = Math.Max(MinRangeDays, (endDate - (DateTime)argsEx.OldValue).TotalDays);
                // Ignore closest when start and end dates were the same
                bool ignoreClosest = -1 < diffBeforeChange && diffBeforeChange < 1;

                if (!_changedEndDate) {
                    SetEndDate(item, startDate.AddDays(diffBeforeChange), startDate, ignoreClosest);

                    _changedEndDate = false;
                }
                else if (endDate <= startDate) {
                    SetEndDate(item, startDate.AddDays(MinRangeDays), startDate, ignoreClosest);
                }
            }
            else { // It's the end date
                _changedEndDate = true;

                if (MinDate.HasValue && endDate < MinDate.Value.AddDays(MinRangeDays)) {
                    endDate = SetEndDate(item, MinDate.Value.AddDays(MinRangeDays), startDate, true);
                }
                else if (MaxDate.HasValue && endDate > MaxDate.Value) {
                    endDate = SetEndDate(item, MaxDate.Value, startDate, true);
                }

                if (endDate <= startDate) {
                    SetStartDate(item, endDate.AddDays(-MinRangeDays), endDate);
                }
            }
        }

        // ignoreClosest does not get the closest value for the given date,
        // but will still do that if the given date fails other checks.

        private DateTime SetStartDate(T item, DateTime date, DateTime endDate, bool ignoreClosest = false) {
            try {
                _ignoreStartDateChange = true;

                if (MinDate.HasValue && MinDate > date) {
                    date = MinDate.Value;
                }
                else if (!ignoreClosest) {
                    var originalDate = date;

                    date = date.GetClosestInDays(
                        AllowedDays,
                        // Only predict days in the future (Tense.Any) when the current range
                        // exceeds the minimum specified range
                        (endDate - date).TotalDays > MinRangeDays ? Tense.Any : Tense.Past
                    );

                    // Even with the above check, we may still have predicted an invalid date, check again
                    if (endDate < endDate.AddDays(-MinRangeDays)) {
                        date = originalDate.GetClosestInDays(AllowedDays, Tense.Future);
                    }

                    if (MinDate.HasValue && MinDate > date) {
                        date = MinDate.Value;
                    }
                }

                _startPropertyInfo.SetValue(item, date);

                return date;
            }
            finally {
                _ignoreStartDateChange = false;
            }
        }

        private DateTime SetEndDate(T item, DateTime date, DateTime startDate, bool ignoreClosest = false) {
            try {
                _ignoreEndDateChange = true;

                if (MaxDate.HasValue && MaxDate < date) {
                    date = MaxDate.Value;
                }
                else if (!ignoreClosest) {
                    var originalDate = date;

                    date = date.GetClosestInDays(
                        AllowedDays,
                        // Only predict days in the future (Tense.Any) when the current range
                        // exceeds the minimum specified range
                        (date - startDate).TotalDays > MinRangeDays ? Tense.Any : Tense.Future
                    );

                    // Even with the above check, we may still have predicted an invalid date, check again
                    if (date < startDate.AddDays(MinRangeDays)) {
                        date = originalDate.GetClosestInDays(AllowedDays, Tense.Future);
                    }

                    if (MaxDate.HasValue && MaxDate < date) {
                        date = MaxDate.Value;
                    }
                }

                _endPropertyInfo.SetValue(item, date);

                return date;
            }
            finally {
                _ignoreEndDateChange = false;
            }
        }

        public void Dispose() {
            _subscription?.Dispose();
            _subscription = null;
        }
    }
    public sealed class TimeRangeEnforcer<T> : IDisposable where T : INotifyPropertyChanged {
        private IDisposable _subscription;

        private readonly Func<T, TimeSpan?> _startProperty, _endProperty;
        private readonly PropertyInfo _startPropertyInfo, _endPropertyInfo;

        private bool _changedEndTime;

        public int MinRangeMinutes { get; set; } = 5;

        public TimeRangeEnforcer(T item, Expression<Func<T, TimeSpan?>> startProperty, Expression<Func<T, TimeSpan?>> endProperty) {
            _subscription = item.WeakSubscribe(ItemOnPropertyChanged);

            _startProperty = startProperty.Compile();
            _endProperty = endProperty.Compile();

            _startPropertyInfo = RangeEnforcerHelper.GetPropertyInfoFromExpression(startProperty);
            _endPropertyInfo = RangeEnforcerHelper.GetPropertyInfoFromExpression(endProperty);
        }

        private void ItemOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var item = (T)sender;

            bool isStart = e.PropertyName == _startPropertyInfo.Name;
            bool isEnd = e.PropertyName == _endPropertyInfo.Name;

            if (!isStart && !isEnd) {
                return;
            }

            var nullableStartTime = _startProperty(item);
            var nullableEndTime = _endProperty(item);

            if (nullableStartTime == null || nullableEndTime == null) {
                return;
            }

            var startTime = nullableStartTime.Value;
            var endTime = nullableEndTime.Value;

            if (isStart) {
                // Restore previous value after set
                bool changedEndTime = _changedEndTime;

                if (!changedEndTime) {
                    var argsEx = e as PropertyChangedEventArgsEx;

                    if (argsEx == null) {
                        throw new Exception(typeof(T).FullName + "." + _startPropertyInfo.Name + " must call PropertyChanged with PropertyChangedEventArgsEx");
                    }

                    var diffBeforeChange = Math.Ceiling((endTime - (TimeSpan)argsEx.OldValue).TotalMinutes);

                    endTime = startTime.Add(TimeSpan.FromMinutes(diffBeforeChange));
                }
                else if (endTime <= startTime) {
                    endTime = startTime.Add(TimeSpan.FromMinutes(MinRangeMinutes));
                }

                if (endTime >= TimeSpan.FromDays(1d)) {
                    endTime = new TimeSpan(23,59,00);
                }

                _endPropertyInfo.SetValue(item, endTime);

                _changedEndTime = changedEndTime;
            }
            else { // It's the end time
                _changedEndTime = true;

                if (endTime <= startTime) {
                    endTime = endTime.Subtract(TimeSpan.FromMinutes(MinRangeMinutes));

                    if (endTime < TimeSpan.FromMinutes(0)) {
                        endTime = TimeSpan.FromMinutes(0); // Start of day
                    }

                    _startPropertyInfo.SetValue(item, endTime);
                }
            }
        }

        public void Dispose() {
            _subscription?.Dispose();
            _subscription = null;
        }
    }
}
