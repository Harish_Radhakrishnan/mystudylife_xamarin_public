﻿using System;
using MvvmCross;

namespace MyStudyLife {
    public static class DateTimeEx {
        private static IDateTimeProvider _provider;

        public static DateTime Now {
            get {
                if (_provider == null && !Mvx.IoCProvider.TryResolve(out _provider)) {
                    return DateTime.Now;
                }

                return _provider.Now;
            }
        }

        public static DateTime Today {
            get { return Now.Date; }
        }

        public static DateTime ToUniversalTimeEx(this DateTime dt) {
            if (dt.Kind == DateTimeKind.Utc) {
                return dt;
            }

            return dt.Add(DateTime.UtcNow - Now);
        }

        /// <summary>
        ///     Returns the smaller of two dates.
        /// </summary>
        public static DateTime Min(DateTime a, DateTime b) => a < b ? a : b;

        /// <summary>
        ///     Returns the larger of two dates.
        /// </summary>
        public static DateTime Max(DateTime a, DateTime b) => a < b ? b : a;
    }

    public interface IDateTimeProvider {
        DateTime Now { get; }
    }

    /// <summary>
    ///     Provider used to mock the current date time,
    ///     allowing for testing of time sensitive areas of
    ///     the app as well as consistent screenshots for stores.
    /// </summary>
    public class MockDateTimeProvider : IDateTimeProvider {
        private static DateTime? _now;

        private IDateTimeProvider _fallbackProvider;

        public DateTime Now => _now ?? _fallbackProvider?.Now ?? DateTime.Now;

        public MockDateTimeProvider(IDateTimeProvider fallbackProvider = null) {
            this._fallbackProvider = fallbackProvider;
        }

        public MockDateTimeProvider(DateTime now) {
            _now = now;
        }

        public static void SetNow(DateTime now) {
            if (now.Kind != DateTimeKind.Local) {
                now = DateTime.SpecifyKind(now, DateTimeKind.Local);
            }

            _now = now;
        }
    }
}
