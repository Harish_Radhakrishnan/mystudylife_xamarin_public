﻿using System;
using System.Threading;

namespace MyStudyLife.Threading {
    /// <summary>
    ///     A portable implementation of dispatcher
    ///     round in WP7 and Windows 8.
    /// </summary>
    [Obsolete("Use methods provided in MvxViewModel")]
    public static class PortableDispatcher {
        /// <summary>
        /// Gets or sets the current synchronization context.
        /// 
        /// To use correctly, set after first UI element (ie. Frame)
        /// has been created.
        /// </summary>
        public static SynchronizationContext Context { get; private set; }

        static PortableDispatcher() {
            // No harm in trying!
            Context = SynchronizationContext.Current;
        }

        /// <summary>
        /// Sets the current synchronization context.
        /// </summary>
        /// <param name="context">The current context.</param>
        public static void SetContext(SynchronizationContext context) {
            Context = context;
        }

        /// <summary>
        ///     Dispatches an asynchronous message to
        ///     the current synchronization context.
        /// </summary>
        /// <param name="action">The action to run.</param>
        public static void InvokeIfRequired(Action action) {
            if (Context == null || Context == SynchronizationContext.Current) {
                action();
            }
            else {
                ManualResetEvent m = new ManualResetEvent(false);

                Context.Post((o) => {
                        action.Invoke();

                        m.Set();

                    }, null);

                m.WaitOne();
            }
        }
    }
}
