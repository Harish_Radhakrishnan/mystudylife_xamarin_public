﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MyStudyLife.Threading {
    // http://www.hanselman.com/blog/ComparingTwoTechniquesInNETAsynchronousCoordinationPrimitives.aspx 
    public sealed class AsyncLock {
        private readonly SemaphoreSlim _semaphore;
        private readonly Task<IDisposable> _releaser;

        public static AsyncLock CreateLocked(out IDisposable releaser) {
            var asyncLock = new AsyncLock(true);
            releaser = asyncLock._releaser.Result;
            return asyncLock;
        }

        public AsyncLock(bool isLocked) {
            _semaphore = new SemaphoreSlim(isLocked ? 0 : 1, 1);
            _releaser = Task.FromResult((IDisposable)new Releaser(this));
        }

        public Task<IDisposable> LockAsync() {
            var wait = _semaphore.WaitAsync();
            return wait.IsCompleted ?
                _releaser :
                wait.ContinueWith((_, state) => (IDisposable)state,
                    _releaser.Result, CancellationToken.None,
                    TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
        }

        sealed class Releaser : IDisposable {
            readonly AsyncLock m_toRelease;
            internal Releaser(AsyncLock toRelease) { m_toRelease = toRelease; }
            public void Dispose() { m_toRelease._semaphore.Release(); }
        }
    }
}
