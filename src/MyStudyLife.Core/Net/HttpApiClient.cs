﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using Newtonsoft.Json;

namespace MyStudyLife.Net
{
    public class HttpApiClient : IHttpApiClient {
	    private const string VersionHeader = "X-MSL-Version";

	    private readonly IStorageProvider _storageProvider;
        private readonly Lazy<HttpClient> _internalClient;

        private HttpClient InternalClient => _internalClient.Value;

        public HttpApiClientSettings Settings { get; }

        [Obsolete("Use other overload")]
	    public HttpApiClient(
	        IMslConfig config,
	        IHttpClientFactory clientFactory,
	        IStorageProvider storageProvider
	    ) : this(config.ApiClientSettings, clientFactory, storageProvider) { }

		public HttpApiClient(
            HttpApiClientSettings settings,
            IHttpClientFactory clientFactory,
            IStorageProvider storageProvider
        ) {
			this.Settings = settings;

            this._internalClient  = new Lazy<HttpClient>(clientFactory.Get, LazyThreadSafetyMode.ExecutionAndPublication);
            this._storageProvider = storageProvider;
		}

		public async Task<HttpApiClientResponse> GetAsync(string requestUrl, params KeyValuePair<string, string>[] queryParameters) {
			if (queryParameters.Length > 0) {
				requestUrl = String.Concat(requestUrl, "?", queryParameters.ToQueryString());
			}

		    var request = CreateRequest(requestUrl, HttpMethod.Get);

            return await GetClientResponse(request, true);
		}

        /// <summary>
        ///     Gets and deserializes an object to the
        ///     given <typeparamref name="TResponse"/> type.
        /// </summary>
        /// <typeparam name="TResponse">The type to deserialize to.</typeparam>
        /// <param name="requestUrl">The resource URL.</param>
        /// <param name="queryParameters">Any query params.</param>
        public Task<TResponse> GetAsync<TResponse>(string requestUrl, params KeyValuePair<string, string>[] queryParameters) {
            return GetAsync<TResponse>(requestUrl, default(CancellationToken), queryParameters);
        }

	    public async Task<TResponse> GetAsync<TResponse>(string requestUrl, CancellationToken cancellationToken, params KeyValuePair<string, string>[] queryParameters) {
            if (queryParameters.Length > 0) {
                requestUrl = String.Concat(requestUrl, "?", queryParameters.ToQueryString());
            }

            var request = CreateRequest(requestUrl, HttpMethod.Get);

            var response = await GetClientResponse(request, true, cancellationToken);

            return JsonConvertEx.DeserializeObject<TResponse>(response.Content);
	    }

	    public Task<HttpApiClientResponse> PostAsync(string requestPath, bool ensureSuccess = true, CancellationToken cancellationToken = default(CancellationToken)) {
	        return GetClientResponse(CreateRequest(requestPath, HttpMethod.Post), ensureSuccess, cancellationToken);
	    }

	    public async Task<HttpApiClientResponse> PostJsonAsync(string requestUrl, string jsonContent, bool ensureSuccess = true) {
            var request = CreateRequest(requestUrl, HttpMethod.Post);

	        if (!String.IsNullOrEmpty(jsonContent)) {
	            request.Content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
	        }

	        return await GetClientResponse(request, ensureSuccess);
		}

		public async Task<HttpApiClientResponse> PostFormDataAsync(string requestUrl, IEnumerable<KeyValuePair<string , string>> formData, bool ensureSuccess = true) {
			var request = CreateRequest(requestUrl, HttpMethod.Post);

		    if (formData != null) {
                request.Content = new FormUrlEncodedContent(formData);

                Debug.WriteLine(await request.Content.ReadAsStringAsync());
		    }

            return await GetClientResponse(request, ensureSuccess);
		}

		public async Task<HttpApiClientResponse> PutJsonAsync(string requestUrl, string jsonContent, bool ensureSuccess = true) {
            var request = CreateRequest(requestUrl, HttpMethod.Put);

            request.Content = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            return await GetClientResponse(request, ensureSuccess);
		}

		public async Task<HttpApiClientResponse> DeleteAsync<T>(string requestUrl, T id, bool ensureSuccess = true) {
			var request = CreateRequest($"{requestUrl}/{id}", HttpMethod.Delete);

            return await GetClientResponse(request, ensureSuccess);
		}

		#region Utils / Helpers

		[DebuggerStepThrough]
		protected virtual HttpRequestMessage CreateRequest(string requestUri, HttpMethod method) {
		    return new HttpRequestMessage(method, String.Concat(this.Settings.EndpointUri, requestUri.Trim('/')));
		}

		/// <summary>
		/// Gets the response for the given <see cref="HttpWebRequest"/>.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="ensureSuccess">
		/// True to throw an exception if the response is not
		/// successfull.
		/// </param>
		/// <param name="requestContent">
		/// The content sent with the request such as JSON or form data.
		///
		/// Used to compute the hash without reading the request stream.
		/// </param>
		/// <returns></returns>
		[DebuggerStepThrough]
		protected virtual async Task<HttpApiClientResponse> GetClientResponse(HttpRequestMessage request, bool ensureSuccess, CancellationToken cancellationToken = default(CancellationToken)) {
			HttpResponseMessage response;
		    double elapsedMilliseconds;

            request = PrepareForSend(request);

            var sw = Stopwatch.StartNew();
            try {
                response = await _internalClient.Value.SendAsync(request, cancellationToken);
            }
            catch (WebException ex) {
                // Enables status checking in Raygun
                ex.Data["Status"] = (int)ex.Status;
                throw;
            }
            sw.Stop();

            elapsedMilliseconds = sw.ElapsedMilliseconds;

		    if (response == null) {
				throw new HttpApiException(HttpApiErrorMessage.ServiceUnreachable, request.RequestUri);
            }

            Trace("{0} request to {1} returned {2} in {3}ms", request.Method, request.RequestUri, response.StatusCode, elapsedMilliseconds);

            var content = await response.Content.ReadAsStreamAsync();

            DateTime serverDate = default;

            if (response.Headers.TryGetValues("Date", out var dateHeaderValues)) {
                string dateHeader = dateHeaderValues.FirstOrDefault();

		        if (String.IsNullOrWhiteSpace(dateHeader) || !DateTime.TryParse(dateHeader, out serverDate)) {
		            serverDate = DateTime.UtcNow;
                    Trace($"Unable to parse server date header with value '{dateHeader}'");
		        }
            }

            var clientResponse = new HttpApiClientResponse(request.RequestUri, response.StatusCode, response.ReasonPhrase, content, serverDate);

            if (ensureSuccess) {
                await EnsureSuccessAsync(clientResponse);
            }

            return clientResponse;
		}

        protected virtual HttpRequestMessage PrepareForSend(HttpRequestMessage request) {
            request.Headers.Add(VersionHeader, MslConfig.Current.AppVersion.ToString());

			AccessToken accessToken = AccessToken.GetCurrent(_storageProvider);

            if (accessToken != null) {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.Token);
            }

            return request;
		}

		[DebuggerStepThrough]
		private async System.Threading.Tasks.Task EnsureSuccessAsync(HttpApiClientResponse response) {
            if (response == null) {
                throw new HttpApiException(HttpApiErrorMessage.ServiceUnreachable, null);
            }

            if (IsSuccessStatusCode(response.StatusCode)) {
                return;
            }

            string responseContent;

            using (var streamReader = new StreamReader(response.Content)) {
                responseContent = await streamReader.ReadToEndAsync();
            }

            try {
                var error = JsonConvert.DeserializeObject<HttpApiErrorMessage>(responseContent);

                if (response.StatusCode == HttpStatusCode.Unauthorized) {
                    var ex = new HttpApiUnauthorizedException(error, response.RequestUrl, response.StatusCode);

                    Trace($"HTTP request to {response.RequestUrl.AbsolutePath} returned an unauthorized response with cause: '{ex.Cause}'");

                    throw ex;
                }

                // Access token has expired due to user updating... This is legacy handling when the HMACKeyPair used to change per app version,
                // TODO: Review this
                if (response.StatusCode == HttpStatusCode.Forbidden) {
                    Trace($"HTTP request to {response.RequestUrl.AbsolutePath} request returned a forbidden response");

                    throw new HttpApiUnauthorizedException(HttpApiUnauthorizedCause.AccessTokenInvalid, response.RequestUrl, response.StatusCode);
                }

                throw new HttpApiException(error, response.RequestUrl, response.StatusCode);
            }
            catch (JsonReaderException ex) when (ex.LineNumber + ex.LinePosition == 0 && ex.Message.StartsWith("Unexpected char")) {
                Trace($"HTTP request to {response.RequestUrl.AbsolutePath} returned a non-JSON response content: {responseContent}");

                // We're assuming the response content will be a string of sorts at this point
                throw new HttpApiException(response.RequestUrl, response.StatusCode);
            }
		}

	    [Conditional("DEBUG")]
	    private static void Trace(string message, params object[] args) {
	        Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(HttpApiClient)).LogDebug(message, args);
	    }

        /// <summary>
        /// Determines whether the given status code
        /// is in the range 200 - 299.
        /// </summary>
        /// <param name="statusCode">The response to check.</param>
        /// <returns>True if the status code indicates success.</returns>
        private static bool IsSuccessStatusCode(HttpStatusCode statusCode)
            => (200 <= (int)statusCode && (int)statusCode <= 299) || statusCode == HttpStatusCode.NotModified;

        #endregion

        #region IDisposable

        ~HttpApiClient() {
            Dispose(false);
        }

        public void Dispose() => Dispose(true);

        protected void Dispose(bool disposing) {
            if (disposing) {
                if (_internalClient.IsValueCreated) {
                    _internalClient.Value.Dispose();
                }
            }
        }

		#endregion
	}
}
