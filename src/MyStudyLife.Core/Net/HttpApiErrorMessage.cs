﻿using System;
using Newtonsoft.Json;

namespace MyStudyLife.Net {
    // Must has Serializable attrib in order to be used as exception data.
    [JsonObject, Serializable]
    public class HttpApiErrorMessage {
        [JsonProperty("error")]
        public string Name { get; set; }

        [JsonProperty("error_message")]
        public string Message { get; set; }

        [JsonProperty("correlation_id")]
        public string CorrelationId { get; set; }

        /// <summary>
        /// Optional property used to convay
        /// more information if the error is displayed on the client.
        /// </summary>
        [JsonProperty("error_detail")]
        public string Detail { get; set; }

        [JsonConstructor]
        public HttpApiErrorMessage(string name, string message) {
            this.Name = name;
            this.Message = message;
        }

        public static HttpApiErrorMessage ServiceUnreachable {
            get {
                return new HttpApiErrorMessage("service_unreachable", "A null response was returned from the remote service.");
            }
        }
    }
}
