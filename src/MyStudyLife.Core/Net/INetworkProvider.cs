﻿using System.Threading.Tasks;

namespace MyStudyLife.Net {
    public delegate void NetworkConnectivityChangedDelegate(object sender, NetworkConnectivityChangedEventArgs e);

    /// <summary>
    /// Provides commonly used network methods.
    /// </summary>
    public interface INetworkProvider {
        /// <summary>
        /// Occurs when the connectivity changes type
        /// or availability.
        /// </summary>
        event NetworkConnectivityChangedDelegate ConnectivityChanged;

        /// <summary>
        /// Indicates whether the device
        /// is currently connected to the internet.
        /// </summary>
        bool IsOnline { get; }

        /// <summary>
        /// Indicates whether the current connection
        /// has been disabled by the user's settings.
        /// </summary>
        bool IsConnectionDisabled { get; }

        /// <summary>
        /// Gets a value which indicates whether
        /// the current connection can be used
        /// taking into account the network status
        /// and user preferences.
        /// </summary>
        Task<bool> GetCanUseConnectionAsync();
    }
}
