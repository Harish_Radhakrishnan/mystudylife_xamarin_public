﻿namespace MyStudyLife.Net {
	/// <summary>
	/// The cause of the user being
	/// unauthorized.
	/// </summary>
	public enum HttpApiUnauthorizedCause {
		/// <summary>
		/// The cause is unknown.
		/// </summary>
		Undetermined = 0,

		/// <summary>
		/// The access token has expired.
		/// </summary>
		AccessTokenExpired = 1,

		/// <summary>
		/// The access token is invalid,
		/// most likely it has become void.
		/// </summary>
		AccessTokenInvalid = 2,

		/// <summary>
		/// The user has signed out.
		/// </summary>
		UserSignOut = 3
	}
}
