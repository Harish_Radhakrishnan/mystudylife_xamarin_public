﻿using System.Net.Http;

namespace MyStudyLife.Net {
    public interface IHttpClientFactory {
        HttpClient Get();
    }
}
