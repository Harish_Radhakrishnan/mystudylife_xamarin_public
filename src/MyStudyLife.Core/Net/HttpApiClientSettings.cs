﻿namespace MyStudyLife.Net {
	public class HttpApiClientSettings {
		/// <summary>
		///     The URL of the My Study Life API
		///     excluding the version number.
		/// 
		///     eg. https://api.mystudylife.com/v5/
		/// </summary>
		public string EndpointUri { get; set; }

		/// <summary>
		/// The public key sent with HTTP requests.
		/// </summary>
		public string ClientId { get; set; }

        public HttpApiClientSettings(string endpointUri, string clientId) {
            this.EndpointUri = endpointUri;
            this.ClientId = clientId;
        }
	}
}
