﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace MyStudyLife.Net {
    public interface IHttpApiClient : IDisposable {
        HttpApiClientSettings Settings { get; }

        Task<HttpApiClientResponse> GetAsync(string requestUrl, params KeyValuePair<string, string>[] queryParameters);

        Task<TResponse> GetAsync<TResponse>(string requestUrl, params KeyValuePair<string, string>[] queryParameters);

        Task<TResponse> GetAsync<TResponse>(string requestUrl, CancellationToken cancellationToken, params KeyValuePair<string, string>[] queryParameters);

        Task<HttpApiClientResponse> PostAsync(string requestPath, bool ensureSuccess = true, CancellationToken cancellationToken = default(CancellationToken));
        Task<HttpApiClientResponse> PostJsonAsync(string requestUrl, string jsonContent, bool ensureSuccess = true);
        Task<HttpApiClientResponse> PostFormDataAsync(string request, IEnumerable<KeyValuePair<string, string>> formData, bool ensureSuccess = true);

        Task<HttpApiClientResponse> PutJsonAsync(string requestUrl, string jsonContent, bool ensureSuccess = true);

        Task<HttpApiClientResponse> DeleteAsync<T>(string requestUrl, T id, bool ensureSuccess = true);
    }
}
