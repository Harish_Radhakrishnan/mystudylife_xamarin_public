﻿using System;
using System.IO;
using System.Net;

namespace MyStudyLife.Net {

	public class HttpApiClientResponse {
		/// <summary>
		///		The request URL.
		/// </summary>
		public Uri RequestUrl { get; private set; }

		/// <summary>
		///		The HTTP status code returned.
		/// </summary>
		public HttpStatusCode StatusCode { get; private set; }

		/// <summary>
		///		The description of the <see cref="StatusCode"/>.
		/// </summary>
		public string StatusDescription { get; private set; }

		/// <summary>
		///		True if the status code is in the range 200 - 299.
		/// </summary>
		public bool IsSuccessStatusCode {
			get { return 200 <= (int)StatusCode && (int)StatusCode <= 299; }
		}

		/// <summary>
		///		The raw content returned.
		/// </summary>
		public Stream Content { get; private set; }

		/// <summary>
		///		The <see cref="DateTime"/> on the server.
		/// </summary>
		public DateTime ServerDate { get; private set; }

		/// <summary>
		///		Creates a new HTTP response.
		/// </summary>
		/// <param name="requestUrl">The request URL.</param>
		/// <param name="statusCode">The HTTP status code returned.</param>
		/// <param name="statusDescription">The description of the <see cref="StatusCode"/>.</param>
		/// <param name="content">The raw content returned.</param>
		/// <param name="serverDate">The <see cref="DateTime"/> on the server.</param>
		public HttpApiClientResponse(Uri requestUrl, HttpStatusCode statusCode, string statusDescription, Stream content, DateTime serverDate) {
			this.RequestUrl = requestUrl;
			this.StatusCode = statusCode;
			this.StatusDescription = statusDescription;
			this.Content = content;
			this.ServerDate = serverDate;
		}
	}
}
