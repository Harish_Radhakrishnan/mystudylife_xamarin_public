﻿using System;
using System.Collections.Generic;
using System.Net;

namespace MyStudyLife.Net {
    public class UriHelper {
        // Taken from here: http://referencesource.microsoft.com/#System.Web/httpserverutility.cs,a711aeeae301c09c
        public static IDictionary<string, IList<string>> ParseQueryString(string query) {
            if (query == null) {
                throw new ArgumentNullException(nameof(query));
            }

            if (query.Length > 0 && query[0] == '?') {
                query = query.Substring(1);
            }

            var queryValues = new Dictionary<string, IList<string>>();

            string s = query;

            int l = (s != null) ? s.Length : 0;
            int i = 0;

            while (i < l) {
                int si = i;
                int ti = -1;

                while (i < l) {
                    char ch = s[i];

                    if (ch == '=') {
                        if (ti < 0)
                            ti = i;
                    }
                    else if (ch == '&') {
                        break;
                    }

                    i++;
                }

                // Extract the name / value pair
                string name = null;
                string value = null;

                if (ti >= 0) {
                    name = WebUtility.UrlDecode(s.Substring(si, ti - si));
                    value = WebUtility.UrlDecode(s.Substring(ti + 1, i - ti - 1));
                }
                else {
                    value = WebUtility.UrlDecode(s.Substring(si, i - si));
                }

                // Add name / value pair to the collection
                IList<string> values;
                if (queryValues.TryGetValue(name, out values)) {
                    values.Add(value);
                }
                else {
                    queryValues.Add(name, new List<string> { value });
                }

                // Trailing '&'
                if (i == (l - 1) && s[i] == '&') {
                    queryValues.Add(null, new List<string> { String.Empty });
                }

                i++;
            }

            return queryValues;
        }

        public static bool TryGetQueryStringValue(string query, string name, out string value) {
            name = String.Concat(name, "=");

            var index = query.IndexOf(name, StringComparison.OrdinalIgnoreCase);

            if (index > -1) {
                var valueIndex = index + name.Length;
                var ampersandIndex = query.IndexOf('&', valueIndex);

                value = ampersandIndex > -1 ? query.Substring(valueIndex, ampersandIndex - valueIndex) : query.Substring(valueIndex);

                if (value != null) {
                    value = WebUtility.UrlDecode(value);
                }

                return true;
            }

            value = null;
            return false;
        }
    }
}
