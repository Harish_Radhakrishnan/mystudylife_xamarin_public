﻿using System;
using System.Net;

namespace MyStudyLife.Net {
	public class HttpApiException : Exception {
        private Uri _requestUrl;
        private HttpStatusCode? _statusCode;
        private HttpApiErrorMessage _errorMessage;

		public Uri RequestUrl {
            get { return _requestUrl; }
            set { Data[nameof(RequestUrl)] = _requestUrl = value; }
        }

		public HttpStatusCode? StatusCode {
            get { return _statusCode; }
            set { Data[nameof(StatusCode)] = _statusCode = value; }
        }

        public HttpApiErrorMessage ErrorMessage {
            get { return _errorMessage; }
            set { Data[nameof(ErrorMessage)] = _errorMessage = value; }
        }

        public HttpApiException(Uri requestUrl) : this(requestUrl, $"An error occurred during a request to '{requestUrl}'") { }
		public HttpApiException(Uri requestUrl, string message) : base(message) {
			this.RequestUrl = requestUrl;
		}

        public HttpApiException(Uri requestUrl, HttpStatusCode statusCode)
            : this(requestUrl, statusCode, $"A request to '{requestUrl}' returned the error status code {(int)statusCode} without a JSON API error message") { }
		public HttpApiException(Uri requestUrl, HttpStatusCode statusCode, string message) : base(message) {
			this.RequestUrl = requestUrl;
			this.StatusCode = statusCode;
		}

		public HttpApiException(HttpApiErrorMessage errorMessage, Uri requestUrl) : this(
            requestUrl,
            $"A request to '{requestUrl}' returned " +
            errorMessage != null
                ?  $"the error '{errorMessage.Name}': {errorMessage.Message}"
                : "a null response, likely not found"
        ) {
			this.ErrorMessage = errorMessage;
		}

		public HttpApiException(HttpApiErrorMessage errorMessage, Uri requestUrl, HttpStatusCode statusCode)
            : this(errorMessage, requestUrl) {

            this.StatusCode = statusCode;
		}

		public HttpApiException(HttpApiErrorMessage errorMessage, Uri requestUrl, HttpStatusCode statusCode, string message)
			: base(message) {

			this.RequestUrl = requestUrl;
			this.StatusCode = statusCode;
			this.ErrorMessage = errorMessage;
		}
	}
}