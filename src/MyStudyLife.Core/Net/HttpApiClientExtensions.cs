﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyStudyLife.Net
{
    /// <summary>
    /// Extension methods which aid integration
    /// with the HttpApiClient.
    /// </summary>
    public static class HttpApiClientExtensions
    {
        /// <summary>
        /// Converts the <see cref="KeyValuePair{TKey, TValue}"/> collection
        /// to a URL encoded query string.
        /// </summary>
        /// <param name="values">The collection of values to convert.</param>
        /// <returns>The URL encoded string.</returns>
        public static string ToQueryString(this IEnumerable<KeyValuePair<string, string>> values)
        {
            values = values.ToList();

            if (values.Any())
            {
                StringBuilder queryStringBuilder = new StringBuilder();

                foreach (var queryParam in values)
                {
                    queryStringBuilder.AppendFormat("{0}={1}&", queryParam.Key, Uri.EscapeDataString(queryParam.Value));
                }

                return queryStringBuilder.ToString().TrimEnd(new[] {'&'});
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
