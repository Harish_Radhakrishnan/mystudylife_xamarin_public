﻿using System;
using MyStudyLife.Data.Settings;

namespace MyStudyLife.Net {
    public class NetworkConnectivityChangedEventArgs : EventArgs {
        /// <summary>
        /// The <see cref="INetworkProvider.IsOnline"/> value
        /// before the connectivity changed.
        /// </summary>
        public bool WasOnline { get; private set; }

        /// <summary>
        /// The new <see cref="INetworkProvider.IsOnline"/> value.
        /// </summary>
        public bool IsOnline { get; private set; }

        public bool WasConnectionMetered { get; private set; }

        public bool IsConnectionMetered { get; private set; }

        /// <summary>
        /// The <see cref="INetworkProvider.IsConnectionDisabled"/>
        /// value before the connectivity changed.
        /// </summary>
        public bool WasConnectionDisabled { get; private set; }

        /// <summary>
        /// The new <see cref="INetworkProvider.IsConnectionDisabled"/>
        /// value.
        /// </summary>
        public bool IsConnectionDisabled { get; private set; }

        /// <summary>
        /// The value from <see cref="INetworkProvider.GetCanUseConnectionAsync"/>
        /// before the connectivity changed.
        /// </summary>
        public bool CouldUseConnection { get; private set; }

        /// <summary>
        /// The new <see cref="INetworkProvider.GetCanUseConnectionAsync"/> value.
        /// </summary>
        public bool CanUseConnection { get; private set; }

        /// <summary>
        /// Event Args used when the connectivity changes.
        /// </summary>
        /// <param name="wasOnline">The old <see cref="INetworkProvider.IsOnline"/> value.</param>
        /// <param name="wasConnectionDisabled">The old <see cref="INetworkProvider.IsConnectionDisabled"/> value.</param>
        /// <param name="isOnline">The new <see cref="INetworkProvider.IsOnline"/> value.</param>
        /// <param name="isConnectionDisabled">The new <see cref="INetworkProvider.IsConnectionDisabled"/> value.</param>
        public NetworkConnectivityChangedEventArgs(bool wasOnline, bool wasConnectionDisabled,
                                                   bool isOnline, bool isConnectionDisabled) {
            this.WasOnline = wasOnline;
            this.WasConnectionDisabled = wasConnectionDisabled;
            this.CouldUseConnection = this.WasOnline && !this.WasConnectionDisabled;

            this.IsOnline = isOnline;
            this.IsConnectionDisabled = isConnectionDisabled;
            this.CanUseConnection = this.IsOnline && !this.IsConnectionDisabled;
        }

        public NetworkConnectivityChangedEventArgs(bool wasOnline, bool wasConnectionMetered, bool isOnline, bool isConnectionMetered, DeviceSettings settings) {

            this.WasOnline = wasOnline;
            this.WasConnectionMetered = wasConnectionMetered;
            this.WasConnectionDisabled = wasConnectionMetered && settings.SyncDisabledOverMeteredConnection;
            this.CouldUseConnection = this.WasOnline && !this.WasConnectionDisabled;

            this.IsOnline = isOnline;
            this.IsConnectionMetered = isConnectionMetered;
            this.IsConnectionDisabled = isConnectionMetered && settings.SyncDisabledOverMeteredConnection;
            this.CanUseConnection = this.IsOnline && !this.IsConnectionDisabled;
        }
    }
}
