﻿using System.Net;
using System.Net.Http;

namespace MyStudyLife.Net {
    public class DefaultHttpClientFactory : IHttpClientFactory {
        public HttpClient Get() {
            var handler = new HttpClientHandler {
                UseDefaultCredentials = true
            };

            if (handler.SupportsAutomaticDecompression) {
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            return new HttpClient(handler);
        }
    }
}
