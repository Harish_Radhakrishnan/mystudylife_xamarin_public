﻿using System;
using System.Net;

namespace MyStudyLife.Net {
	public class HttpApiUnauthorizedException : HttpApiException {
		private const string UnauthorizedMessage = "An unauthroized response was returned from the HTTP request.";

        private HttpApiUnauthorizedCause _cause;

		/// <summary>
		///		The cause of the user being unauthorized.
		/// </summary>
		public HttpApiUnauthorizedCause Cause {
            get { return _cause; }
            set { Data[nameof(Cause)] = _cause = value; }
        }

		public HttpApiUnauthorizedException(HttpApiUnauthorizedCause cause, Uri requestUrl, string message = UnauthorizedMessage)
			: base(requestUrl, HttpStatusCode.Unauthorized, message) {

			this.Cause = cause;
		}

		public HttpApiUnauthorizedException(HttpApiUnauthorizedCause cause, Uri requestUrl, HttpStatusCode statusCode, string message = UnauthorizedMessage)
			: base(requestUrl, statusCode, message) {

			this.Cause = cause;
		}

		public HttpApiUnauthorizedException(HttpApiErrorMessage errorMessage, Uri requestUrl, HttpStatusCode statusCode, string message = UnauthorizedMessage)
			: base(errorMessage, requestUrl, statusCode, message) {

			var cause = HttpApiUnauthorizedCause.Undetermined;

            if (errorMessage != null && errorMessage.Message == "access_token_expired") {
                cause = HttpApiUnauthorizedCause.AccessTokenExpired;
            }
            else if (errorMessage != null && errorMessage.Message == "access_token_invalid") {
                cause = HttpApiUnauthorizedCause.AccessTokenInvalid;
            }

			this.Cause = cause;
		}
	}
}
