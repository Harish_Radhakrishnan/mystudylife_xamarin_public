﻿using System.Net;

namespace MyStudyLife.Net {
    /// <summary>
    ///       Specifies the status of a network request.
    /// </summary>
    /// <remarks>
    ///     This contains many more values in mono than <see cref="WebExceptionStatus"/>
    ///     provided by System.Net.Requests.dll. The integer values are the same in both.
    ///
    ///     https://github.com/mono/mono/blob/f16c0866a83499c11b622bcc9c7fe5ea4020062a/mcs/class/referencesource/System/net/System/Net/WebExceptionStatus.cs
    /// </remarks>
    public enum WebExceptionStatusEx {
        /// <summary>
        ///    <para>
        ///       No error was encountered.
        ///    </para>
        /// </summary>
        Success = 0,

        /// <summary>
        ///    <para>
        ///       The name resolver service could not resolve the host name.
        ///    </para>
        /// </summary>
        NameResolutionFailure = 1,

        /// <summary>
        ///    <para>
        ///       The remote service point could not be contacted at the transport level.
        ///    </para>
        /// </summary>
        ConnectFailure = 2,

        /// <summary>
        ///    <para>
        ///       A complete response was not received from the remote server.
        ///    </para>
        /// </summary>
        ReceiveFailure = 3,

        /// <summary>
        ///    <para>
        ///       A complete request could not be sent to the remote server.
        ///    </para>
        /// </summary>
        SendFailure = 4,

        /// <internalonly/>
        /// <summary>
        /// </summary>
        PipelineFailure = 5,

        /// <summary>
        ///    <para>
        ///       The request was cancelled.
        ///    </para>
        /// </summary>
        RequestCanceled = 6,

        /// <summary>
        ///    <para>
        ///       The response received from the server was complete but indicated a
        ///       protocol-level error. For example, an HTTP protocol error such as 401 Access
        ///       Denied would use this status.
        ///    </para>
        /// </summary>
        ProtocolError = 7,

        /// <summary>
        ///    <para>
        ///       The connection was prematurely closed.
        ///    </para>
        /// </summary>
        ConnectionClosed = 8,

        /// <summary>
        ///    <para>
        ///       A server certificate could not be validated.
        ///    </para>
        /// </summary>
        TrustFailure = 9,

        /// <summary>
        ///    <para>
        ///       An error occurred in a secure channel link.
        ///    </para>
        /// </summary>
        SecureChannelFailure = 10,

        /// <summary>
        ///    <para>[To be supplied.]</para>
        /// </summary>
        ServerProtocolViolation = 11,

        /// <summary>
        ///    <para>[To be supplied.]</para>
        /// </summary>
        KeepAliveFailure = 12,

        /// <summary>
        ///    <para>[To be supplied.]</para>
        /// </summary>
        Pending = 13,

        /// <summary>
        ///    <para>[To be supplied.]</para>
        /// </summary>
        Timeout = 14,

        /// <summary>
        ///    <para>
        ///       Similar to NameResolution Failure, but for proxy failures.
        ///    </para>
        /// </summary>
        ProxyNameResolutionFailure = 15,

        /// <summary>
        ///    <para>[To be supplied.]</para>
        /// </summary>
        UnknownError = 16,

        /// <summary>
        ///    <para>
        ///       Sending the request to the server or receiving the response from it,
        ///       required handling a message that exceeded the specified limit.
        ///    </para>
        /// </summary>
        MessageLengthLimitExceeded = 17,

        /// <summary>
        ///     A request could be served from Cache but was not found and effective CachePolicy=CacheOnly
        /// </summary>
        CacheEntryNotFound = 18,

        /// <summary>
        ///     A request is not suitable for caching and effective CachePolicy=CacheOnly
        /// </summary>
        RequestProhibitedByCachePolicy = 19,

        /// <summary>
        ///     The proxy script (or other proxy logic) declined to provide proxy info, effectively blocking the request.
        /// </summary>
        RequestProhibitedByProxy = 20
    }
}
