﻿namespace MyStudyLife.Globalization {
    public class Catalog : ICatalog {
        public string GetString(string str) {
            return str;
        }

        public string GetPluralString(string singularStr, string pluralStr, int n) {
            return n == 1 ? singularStr : pluralStr;
        }
    }
}
