﻿namespace MyStudyLife.Globalization {
    /// <summary>
    ///     Loosly based on this so it's easier to translate in
    ///     the future: https://github.com/rdio/vernacular/blob/master/Vernacular.Catalog/Vernacular/Catalog.cs
    /// </summary>
    public interface ICatalog {
        string GetString(string str);

        string GetPluralString(string singularStr, string pluralStr, int n);
    }
}
