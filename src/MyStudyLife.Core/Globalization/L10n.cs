﻿using System;
using System.Globalization;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Caching;

namespace MyStudyLife.Globalization {
// ReSharper disable once InconsistentNaming
    public static class L10n {
        private static ICache _cache;

        private static ICache Cache {
            get { return _cache ?? (_cache = Mvx.IoCProvider.Resolve<ICache>()); }
        }

        public static DayOfWeek GetDayFromIndex(int index) {
            return (DayOfWeek)(((int)DateTimeFormat.FirstDayOfWeek + index) % 7);
        }

        public static int GetDayIndex(int dow) {
            var weekStart = (int)DateTimeFormat.FirstDayOfWeek;

            if (weekStart == dow) {
                return 0;
            }

            if (dow > weekStart) {
                return dow - weekStart;
            }

            return ((6 - weekStart) + dow) + 1;
        }

        public static int GetDayIndex(DayOfWeek dow) {
            return GetDayIndex((int) dow);
        }

        /// <summary>
        ///     Determines the index relative to <see cref="DateTimeFormat.FirstDayOfWeek"/>
        ///     of the first day that exists in the given days.
        /// </summary>
        public static int GetFirstDayIndexInFlag(DaysOfWeek days) {
            string cacheKey = String.Concat("FIRST_DOW_FLAG_", (int) days);
            
            int index;

            if (Cache.TryGet(cacheKey, out index)) {
                return index;
            }

            var orderedDays = DateTimeFormat.Days;

            for (int i = 0; i < 7; i++) {
                var dayFlag = orderedDays[i].DayOfWeek.ToFlag();

                if ((dayFlag & days) == dayFlag) {
                    index = i;
                    break;
                }
            }

            Cache.Set(cacheKey, index, new FirstDayFlagIndexCacheExpiration());

            return index;
        }

        class FirstDayFlagIndexCacheExpiration : ICacheExpiration {
            private readonly DayOfWeek _firstDayOfWeekWhenSet;

            public bool HasExpired {
                get { return _firstDayOfWeekWhenSet != CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek; }
            }

            public FirstDayFlagIndexCacheExpiration() {
                _firstDayOfWeekWhenSet = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
            }
        }
    }
}
