﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace MyStudyLife.Globalization {
    public static class DateTimeFormat {
        private static Lazy<Regex> _removeYearRegex = new Lazy<Regex>(() => new Regex(@",?\s?Y{2,4}", RegexOptions.IgnoreCase));
        public static Regex RemoveYearRegex => _removeYearRegex.Value;

        #region Patterns

        private static Lazy<Regex> _shortTimePatternRegex = new Lazy<Regex>(() => new Regex("[:.]ss?"));

        private static bool? _areDayAndMonthTheWrongWayRound;
        private static string _shortTimePattern, _dayNameAndMonthPattern, _shortLongDatePattern, _abbrLongDatePattern, _shortFullDateTimePattern;

		/// <summary>
		///		True if the month precedes the day (ie. US format).
		/// </summary>
		public static bool AreDayAndMonthTheWrongWayRound {
		    get {
				if (!_areDayAndMonthTheWrongWayRound.HasValue) {
				    DateTimeFormatInfo dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;

					_areDayAndMonthTheWrongWayRound = new Regex(@"M[-\/.]d").IsMatch(dateTimeFormat.ShortDatePattern);
			    }

				return _areDayAndMonthTheWrongWayRound.Value;
		    }
	    }

		/// <summary>
		///		True if the day precedes the month (ie. UK format).
		/// </summary>
	    public static bool AreDayAndMonthTheCorrectWayRound {
		    get { return !AreDayAndMonthTheWrongWayRound; }
	    }

        /// <summary>
        /// Gets the short time format pattern using the
        /// format specified by the devices' user preferences
        /// specifc short time format.
        /// </summary>
        /// <remarks>Fixes .NET issue.</remarks>
        public static string ShortTimePattern {
            get {
                // Fixes .NET issue: http://stackoverflow.com/questions/1292246/why-doesnt-datetime-toshorttimestring-respect-the-short-time-format-in-regio

                if (_shortTimePattern == null) {
                    DateTimeFormatInfo dateTimeFormat = CultureInfo.CurrentCulture.DateTimeFormat;

                    bool replaced = false;
                    _shortTimePattern = _shortTimePatternRegex.Value.Replace(dateTimeFormat.LongTimePattern, m => {
                        // used to check if we actually found seconds to replace, in the event that there's a
                        // weird delimiter we didn't prepare for.
                        replaced = true;
                        return String.Empty;
                    });

                    if (!replaced) {
                        // we've not found seconds to replace, go with the maybe broken (see link above) short time pattern.
                        _shortTimePattern = dateTimeFormat.ShortTimePattern;
                    }
                }

                return _shortTimePattern;
            }
        }

	    public static string DayNameAndMonthPattern {
		    get {
			    return _dayNameAndMonthPattern ??
			           (_dayNameAndMonthPattern = AreDayAndMonthTheWrongWayRound ? "ddd M/d" : "ddd d/M");
		    }
	    }

        /// <summary>
        ///     The long date pattern but with short day and month names.
        ///
        ///     eg. "Mon, 9 Oct, 2015"
        /// </summary>
        public static string ShortLongDatePattern {
            get {
                if (_shortLongDatePattern == null) {
                    var ldp = CultureInfo.CurrentCulture.DateTimeFormat.LongDatePattern;

                    _shortLongDatePattern = ldp.Replace("dddd", "ddd").Replace("MMMM", "MMM");
                }

                return _shortLongDatePattern;
            }
        }

        /// <summary>
        ///     The full date pattern but with short day and month names.
        ///
        ///     eg. "09:00 Mon, 9 Oct, 2015"
        /// </summary>
        public static string ShortFullDateTimePattern {
            get {
                if (_shortFullDateTimePattern == null) {
                    var pattern = CultureInfo.CurrentCulture.DateTimeFormat.FullDateTimePattern;

                    _shortFullDateTimePattern = _shortTimePatternRegex.Value.Replace(
                        pattern.Replace("dddd", "ddd").Replace("MMMM", "MMM"),
                        String.Empty
                    );
                }

                return _shortFullDateTimePattern;
            }
        }

        /// <summary>
        ///		Gets a culture specific time
        ///		format for an abbreviated long
        ///		date time pattern.
        ///
        ///		eg. "8 Oct 12"
        /// </summary>
        public static string AbbrLongDatePattern {
			get {
				return _abbrLongDatePattern ??
					   (_abbrLongDatePattern = AreDayAndMonthTheWrongWayRound ? "MMM d yy" : "d MMM yy");
		    }
	    }

        #endregion

        #region 24/12 Hour

        private static readonly string[] TwentyFourHourHours = {
                "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"
            };

        private static readonly string[] TwelveHourHours = {
                "12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
                "12", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
            };

        /// <summary>
        /// Gets a value indicating whether
        /// the user's device preferences
        /// specify a 24 hour clock.
        /// </summary>
        public static bool Is24Hour {
            get { return CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern.Contains("H"); }
        }

        /// <summary>
        /// Gets a collection of 24 digits
        /// representing each hour of the day.
        ///
        /// Based on the user's device preferences
        /// specify a 24 hour clock.
        /// </summary>
        public static string[] Hours {
            get { return Is24Hour ? TwentyFourHourHours : TwelveHourHours; }
        }

        public static void Ensure24Hours(DateTimeFormatInfo dtf) {
            dtf.LongTimePattern = "HH:mm:ss";
            dtf.ShortTimePattern = "HH:mm";

            var index = dtf.FullDateTimePattern.IndexOf("h", StringComparison.Ordinal);

            if (index > -1) {
                dtf.FullDateTimePattern = dtf.FullDateTimePattern.Substring(0, index) + dtf.LongTimePattern;
            }
        }

        public static void Ensure12Hours(DateTimeFormatInfo dtf) {
            dtf.LongTimePattern = "h:mm:ss tt";
            dtf.ShortTimePattern = "h:mm tt";

            var index = dtf.FullDateTimePattern.IndexOf("H", StringComparison.Ordinal);

            if (index > -1) {
                dtf.FullDateTimePattern = dtf.FullDateTimePattern.Substring(0, index) + dtf.LongTimePattern;
            }
        }

        #endregion

        #region Day Names

        /// <summary>
        ///     Fix for lack of day names in some mono cultures.
        /// </summary>
        public static void EnsureDayNames(DateTimeFormatInfo dtf) {
            if (String.IsNullOrEmpty(dtf.DayNames[0])) {
                dtf.DayNames = new[] {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
            }
            if (String.IsNullOrEmpty(dtf.AbbreviatedDayNames[0])) {
                dtf.AbbreviatedDayNames = new[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
            }
            if (String.IsNullOrEmpty(dtf.ShortestDayNames[0])) {
                dtf.ShortestDayNames = new[] { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };
            }
        }

        public static DayMapping[] Days {
            get { return GetOrderedWeekdays(CultureInfo.CurrentCulture.DateTimeFormat.DayNames); }
        }

        public static DayMapping[] ShortDays {
            get { return GetOrderedWeekdays(CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames); }
        }

        public static string[] DayNames {
            get { return Days.Select(x => x.Name).ToArray(); }
        }

        public static string[] ShortDayNames {
            get { return ShortDays.Select(x => x.Name).ToArray(); }
        }

        public static string[] ShortestDayNames {
            get { return GetOrderedWeekdays(CultureInfo.CurrentCulture.DateTimeFormat.ShortestDayNames).Select(x => x.Name).ToArray(); }
        }

        public static string GetDayName(DayOfWeek day) {
            return CultureInfo.CurrentCulture.DateTimeFormat.DayNames[(int)day];
        }

        public static string GetShortDayName(DayOfWeek day) {
            return CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedDayNames[(int)day];
        }

        public static string GetShortestDayName(DayOfWeek day) {
            return CultureInfo.CurrentCulture.DateTimeFormat.ShortestDayNames[(int)day];
        }
        public static string GetDayName(DayOfWeek day, DateComponentNaming naming) {
            switch (naming) {
                case DateComponentNaming.Full:
                    return GetDayName(day);
                case DateComponentNaming.Abbreviated:
                    return GetShortDayName(day);
                case DateComponentNaming.Shortest:
                    return GetShortestDayName(day);
                default:
                    throw new ArgumentOutOfRangeException(nameof(naming), naming, "Must be a defined value in the " + nameof(DateComponentNaming) + " enum");
            }
        }

        private static DayMapping[] GetOrderedWeekdays(string[] dayNames) {
            if (dayNames.Length != 7) {
                throw new ArgumentException("Must have 7", nameof(dayNames));
            }

            var indexedDayNames = dayNames.Select((x, i) => new DayMapping((DayOfWeek)i, x)).ToArray();

            Array.Sort(indexedDayNames, (a, b) => {
                int aIndex = L10n.GetDayIndex((int)a.DayOfWeek),
                    bIndex = L10n.GetDayIndex((int)b.DayOfWeek);

                if (aIndex < bIndex) {
                    return -1;
                }

                if (aIndex > bIndex) {
                    return 1;
                }

                return 0;
            });

            return indexedDayNames;
        }

        #endregion

        #region First Day of Week

        public static DayOfWeek FirstDayOfWeek {
            get { return CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek; }
        }

        public static DayOfWeek LastDayOfWeek {
            get {
                var firstDoW = FirstDayOfWeek;

                if (firstDoW == DayOfWeek.Sunday) {
                    return DayOfWeek.Saturday;
                }

                return firstDoW - 1;
            }
        }

        #endregion

        #region Extensions

        /// <summary>
        ///		Converts the value of the current <see cref="System.DateTime"/>
        ///		object to it's equivalent string representation using the
        ///		format specified by the devices' user preferences
        ///		specific short time format.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>The formatted string.</returns>
        public static string ToShortTimeStringEx(this DateTime date) {
            return date.ToString(ShortTimePattern);
        }

		/// <summary>
		///		<para>
		///			Converts the value of the current <see cref="System.DateTime"/>
		///			object to it's equivalent string representation using the
		///			format specified by the devices' user preferences
		///			specific short date format.
		///		</para>
		///		<para>
		///			Displayed in format ddd M/d if US date style, ddd d/M anywhere
		///			else.
		///		</para>
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public static string ToDayAndMonthString(this DateTime date) {
			return date.ToString(DayNameAndMonthPattern);
		}

        /// <summary>
        ///
        /// </summary>
        /// <remarks>
        ///     .NET doesn't seem to return the correct week of the year so this has been copied
        ///     from moment JS :)
        ///
        ///     http://stackoverflow.com/questions/11154673/get-the-correct-week-number-of-a-given-date
        /// </remarks>
        public static int GetWeekOfYear(this DateTime date) =>  GetWeekOfYear(date, FirstDayOfWeek);

        public static int GetWeekOfYear(this DateTime date, DayOfWeek firstDayOfWeek, DayOfWeek firstDayOfWeekOfYear = DayOfWeek.Thursday) {
            var end = firstDayOfWeekOfYear - firstDayOfWeek;
            var daysToDayOfWeek = firstDayOfWeekOfYear - date.DayOfWeek;

            if (daysToDayOfWeek > end) {
                daysToDayOfWeek -= 7;
            }

            if (daysToDayOfWeek < end - 7) {
                daysToDayOfWeek += 7;
            }

            return (int) Math.Ceiling(date.AddDays(daysToDayOfWeek).DayOfYear/7d);
        }

        #endregion

        #region Formatting

        public static string FormatDateRange(DateTime startDate, DateTime endDate) {
            if (startDate.Date == endDate.Date) {
                return startDate.ToString("MMM d yyyy");
            }

            string dateTimeFormat;

            if (startDate.IsSame(endDate, DateTimeComparison.Month)) {
                dateTimeFormat = "{0:MMM d} - {1:d yyyy}";
            }
            else if (startDate.IsSame(endDate, DateTimeComparison.Year)) {
                dateTimeFormat = "{0:MMM d} - {1:MMM d yyyy}";
            }
            else {
                dateTimeFormat = "{0:MMM d yyyy} - {1:MMM d yyyy}";
            }

            return String.Format(dateTimeFormat, startDate, endDate);
        }

        /// <summary>
        ///     Returns the given time range as a formatted string eg. 18:00 - 19:00.
        ///
        ///     If the time format is 12-hour and the <paramref name="startTime"/> and
        ///     <paramref name="endTime"/> share a common AM/PM, the designator is only
        ///     appended to the end of the range eg 9:00 - 10:30 AM.
        /// </summary>
        public static string FormatTimeRange(TimeSpan startTime, TimeSpan endTime) {
            var shortTimePattern = ShortTimePattern;
            string startTimeFormat = null;

            if (!Is24Hour && ((startTime.Hours < 12 && endTime.Hours < 12) || (12 <= startTime.Hours && 12 <= endTime.Hours))) {
                startTimeFormat = ShortTimePattern.Replace("tt", String.Empty).Trim();
            }

            return String.Concat(
                new DateTime(startTime.Ticks).ToString(startTimeFormat ?? shortTimePattern),
                " - ",
                new DateTime(endTime.Ticks).ToString(shortTimePattern)
            );
        }

        #endregion
    }

    public sealed class DayMapping {
        public DayOfWeek DayOfWeek { get; private set; }

        public string Name { get; private set; }

        public DayMapping(DayOfWeek dow, string name) {
            this.DayOfWeek = dow;
            this.Name = name;
        }

        public override string ToString() {
            return this.Name;
        }
    }

    /// <summary>
    ///     The naming used for components of a date such
    ///     as the name of a day or month.
    /// </summary>
    public enum DateComponentNaming {
        /// <summary>
        ///     The full name such as February for a month
        ///     or Tuesday for a day.
        /// </summary>
        Full,
        /// <summary>
        ///     The abbreviated name such as Feb for a month
        ///     or Tue for a day.
        /// </summary>
        Abbreviated,
        /// <summary>
        ///     The shortest distinguishable name such as Tu for a day.
        /// </summary>
        Shortest
    }
}
