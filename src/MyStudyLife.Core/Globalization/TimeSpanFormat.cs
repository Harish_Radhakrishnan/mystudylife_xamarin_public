﻿using System;

namespace MyStudyLife.Globalization
{
    public static class TimeSpanFormat
    {
        #region Extensions
        
        /// <summary>
        /// Converts the value of the current <see cref="System.TimeSpan"/>
        /// object to it's equivalent string representation using the
        /// format specified by the devices' user preferences
        ///  specifc short time format. 
        /// </summary>
        /// <param name="time">The time to convert.</param>
        /// <returns>The formatted string.</returns>
        public static string ToShortTimeStringEx(this TimeSpan time)
        {
            // Fixes .NET issue: http://stackoverflow.com/questions/1292246/why-doesnt-datetime-toshorttimestring-respect-the-short-time-format-in-regio

            return new DateTime(1970, 1, 1, time.Hours, time.Minutes, time.Seconds).ToShortTimeStringEx();
        }

        #endregion
    }
}
