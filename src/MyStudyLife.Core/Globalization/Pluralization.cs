﻿namespace MyStudyLife.Globalization {
	/// <summary>
	///		A lazy pluralization helper.
	/// </summary>
	public static class Pluralization {

	    public static string TextForNumber(int number, string singular, string plural) {
	        return number == 1 ? singular : plural;
	    }
	}
}
