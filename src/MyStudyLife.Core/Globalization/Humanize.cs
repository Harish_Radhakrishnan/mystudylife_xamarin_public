﻿using System;

namespace MyStudyLife.Globalization {
    public static class Humanize {
        public static string NumberToLetter(int number) {
            int charCode = 65 + (number - 1);

            if (charCode < 65 || 95 < charCode)
                throw new ArgumentOutOfRangeException(nameof(number),
                    String.Format("Number must be greater than 0 and less than or equal to 26, {0} passed.", number));

            return Convert.ToChar(charCode).ToString();
        }
        
        #region Pluralization

		public static string Pluralize(this string singular) {
			// No such thing as nulls or even nulles!
			if (singular == null) return null;

			if (singular.EndsWith("s")) {
				return singular + "es";
			}

			return singular + "s";
		}
        
        #endregion
    }

    public static class HumanizeExtensions {
        public static string ToLetter(this int number ) {
            return Humanize.NumberToLetter(number);
        }
    }
}
