﻿namespace MyStudyLife.Services {
    public interface IVersioningService {
        bool IsNewUser { get; }

        void EnsureCurrentVersion();
    }
}
