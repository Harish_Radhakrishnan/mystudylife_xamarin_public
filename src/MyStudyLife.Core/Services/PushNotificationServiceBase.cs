﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.Sync;

namespace MyStudyLife.Services
{
    public abstract class PushNotificationServiceBase
    {
        private readonly ILogger<PushNotificationServiceBase> _logger;

        public PushNotificationServiceBase(ILogger<PushNotificationServiceBase> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public virtual bool HandleRawNotification(IDictionary<string, string> notificationContent)
        {
            if (notificationContent.ContainsKey("send_to_sync"))
            {
                _logger.LogDebug("Received send_to_sync notification, syncing...");

                if (Mvx.IoCProvider.TryResolve(out ISyncService syncService))
                {
                    syncService.TrySync(SyncTriggerKind.Programmatic);
                }
                else
                {
                    _logger.LogDebug("Could not resolve ISyncService");
                }

                return true;
            }

            _logger.LogDebug("Received notification that hasn't been handled");

            return false;
        }
    }
}