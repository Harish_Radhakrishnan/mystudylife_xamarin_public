﻿using System.Threading.Tasks;

namespace MyStudyLife.Services {
    public interface IScheduledService {
        Task RunAsync();
    }
}
