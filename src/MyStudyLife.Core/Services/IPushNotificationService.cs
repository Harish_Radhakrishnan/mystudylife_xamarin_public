﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyStudyLife.Services {
    public interface IPushNotificationService {
        Task RegisterTokenAsync(string token);
        
        Task EnsureNotificationChannelAsync();

        Task CloseNotificationChannelAsync();

        bool HandleRawNotification(IDictionary<string, string> content);
    }
}
