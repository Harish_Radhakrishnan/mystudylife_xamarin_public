﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;

namespace MyStudyLife.Scheduling {
    public sealed class AgendaEntry : BindableBase {

        private readonly Guid _baseGuid;

        /// <summary>
        ///     Gets the <see cref="Guid"/> for the
        ///     <see cref="IScheduledEntry"/> this
        ///     <see cref="AgendaEntry"/> represents.
        /// </summary>
        public Guid BaseGuid => _baseGuid;

        public IScheduledEntry Base { get; set; }

        #region Type

        public AgendaEntryType Type { get; private set; }

        /// <summary>
        ///     The type, as a string :O
        /// </summary>
        [JsonProperty("type")]
        public string TypeName {
            get { return this.Type == AgendaEntryType.Class ? "class" : "exam"; }
            set { this.Type = value == "class" ? AgendaEntryType.Class : AgendaEntryType.Exam; }
        }

        public bool IsClass {
            get { return this.Type == AgendaEntryType.Class; }
        }

        /// <value>
        ///     True if the current <see cref="AgendaEntry"/>
        ///     is for an <see cref="Exam"/>.
        /// </value>
        /// <remarks>
        ///     Used for binding in calendar week.
        /// </remarks>
        public bool IsExam {
            get { return this.Type == AgendaEntryType.Exam; }
        }

        #endregion

        public string Title { get; set; }

        public string Time { get; set; }

        public string Location { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public TimeSpan Duration => EndTime - StartTime;

        /// <summary>
        ///     Will be null when <see cref="IsExam"/> is true.
        /// </summary>
        public string TeacherName { get; }

        #region Conflict

        public ScheduleConflictType ConflictType { get; set; }

        /// <summary>
        ///     Shorthand for
        ///     <code>this.ConflictType != ScheduleConflictType.NoConflict</code>
        /// </summary>
        public bool IsConflicting {
            get { return this.ConflictCount > 0; }
        }

        public int ConflictCount { get; internal set; }

        public int ConflictIndex { get; internal set; }

        public ICollection<AgendaEntry> ConflictEntries { get; internal set; }

        #endregion

        // This is written so the state property will always
        // be up to date, but we can track when the state
        // is changed on the dashboard. Ideally this would be
        // in a model and not a getter-only property, but
        // that's for another day.
        private AgendaEntryState? _state;
        public AgendaEntryState State {
            get {
                var now = DateTimeEx.Now;

                if (this.EndTime <= now) {
                    return AgendaEntryState.Past;
                }
                else if (this.StartTime <= now && now < this.EndTime) {
                    return AgendaEntryState.Current;
                }

                return AgendaEntryState.Future;
            }
        }

        public bool IsPast => this.State == AgendaEntryState.Past;

        public bool IsCurrent => this.State == AgendaEntryState.Current;

        [Obsolete("Behaviour did not match name. Use IsFuture instead.")]
        public bool IsNext => IsFuture;

        public bool IsFuture => this.State == AgendaEntryState.Future;

        #region Tasks

        /// <summary>
        ///     Gets or sets the overdue task count.
        /// </summary>
        public int TasksOverdue { get; set; }

        /// <summary>
        ///     Gets or sets the incomplete task count.
        /// </summary>
        public int TasksIncomplete { get; set; }

        /// <summary>
        ///     Gets or sets the complete task count.
        /// </summary>
        public int TasksComplete { get; set; }

        /// <summary>
        /// Returns the sum of <see cref="AgendaEntry.TasksOverdue"/>,
        /// <see cref="AgendaEntry.TasksIncomplete"/> and
        /// <see cref="AgendaEntry.TasksComplete"/>.
        /// </summary>
        public int TasksDue {
            get { return TasksComplete + TasksIncomplete + TasksOverdue; }
        }

        #endregion

        public int MinutesRemaining {
            get { return Math.Max((int)Math.Ceiling((this.EndTime - DateTimeEx.Now).TotalMinutes), 0); }
        }

        public bool IsFromSchool {
            get {
                if (this.Type != AgendaEntryType.Class) {
                    return false;
                }

                return ((ScheduledClass)this.Base).Class.IsFromSchool;
            }
        }

        private string _color;

        public string Color {
            get { return _color ?? (_color = this.Base.Subject.Color); }
            private set { _color = value; }
        }

        public System.Drawing.Color ColorValue { get; private set;}

        /// <summary>
        ///     Sets the color of the agenda entry using the given user.
        ///
        ///     If <see cref="User.IsTeacher"/> is true, the color used
        ///     will be that of the classes's group, if available.
        /// </summary>
        public void SetColor(User user, bool extendedColors) {
            if (user.IsTeacher) {
                ScheduledClass scheduledClass;

                if ((scheduledClass = this.Base as ScheduledClass) != null && scheduledClass.Class.Color != null) {
                    this.Color = scheduledClass.Class.Color;
                    this.ColorValue = UserColors.GetColor(this.Color, extendedColors).Color;

                    return;
                }
            }

            this.Color = this.Base.Subject.Color;
            this.ColorValue = UserColors.GetColor(this.Color, extendedColors).Color;
        }

        private void Init<T>(T entity) where T : IScheduledEntry, INotifyPropertyChanged {
            this.Title = entity.Title;
            this.Time = entity.Time;
            this.Location = entity.Location;

            this.StartTime = DateTimeHelper.FromDateAndTime(entity.Date, entity.StartTime);
            this.EndTime = DateTimeHelper.FromDateAndTime(entity.Date, entity.EndTime);

            // Prevents bug where you can input something like startime: 23:10 and an end time of 00:00
            // would amount to a negetive day.
            if (this.EndTime.Date == this.StartTime.Date && this.EndTime.TimeOfDay < this.StartTime.TimeOfDay) {
                this.EndTime = this.EndTime.AddDays(1d);
            }

            entity.PropertyChanged += EntityOnPropertyChanged;
        }

        private void EntityOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In("Title", "Time", "Location", "StartTime", "EndTime")) {
                // ReSharper disable once ExplicitCallerInfoArgument
                RaisePropertyChanged(e.PropertyName);
            }
        }

		// USED BY JSON DESERIALIZATION IN SCHEDULED TASKS
		// DO NOT DELETE!
		[JsonConstructor]
	    public AgendaEntry() {

	    }

        /// <summary>
        ///     Creates a new <see cref="AgendaEntry"/> for the
        ///     given <see cref="ScheduledClass"/>.
        /// </summary>
        /// <param name="c">The scheduled class.</param>
        public AgendaEntry(ScheduledClass c) {
            if (c == null) throw new ArgumentNullException(nameof(c));

            this.Type = AgendaEntryType.Class;

            this._baseGuid = c.ClassGuid;
            this.Base = c;

            this.Init(c);

            this.TeacherName = c.TeacherName;
        }

        public AgendaEntry(Exam e) {
            if (e == null) throw new ArgumentNullException(nameof(e));

            this.Type = AgendaEntryType.Exam;

            this._baseGuid = e.Guid;
            this.Base = e;

            this.Init(e);
        }

        /// <summary>
        ///     Returns true if the base entries
        ///     are the same (does not take into account
        ///     the date of the entry).
        /// </summary>
        /// <param name="b">The <see cref="AgendaEntry"/> to compare.</param>
        /// <returns>True if they are the same.</returns>
        public bool IsForSameBase(AgendaEntry b) {
            if (this.Type != b.Type) return false;

            if (this.Type == AgendaEntryType.Exam) {
                return this._baseGuid == b._baseGuid;
            }

            var scA = (ScheduledClass)this.Base;
            var scB = (ScheduledClass)b.Base;

            // guids would be null for one-off classes!
            return scA.ClassGuid == scB.ClassGuid && (
                scA.ClassTime?.Guid == scB.ClassTime?.Guid);
        }

        public void SetTimeSensitiveProperties() {
            RaisePropertyChanged(nameof(MinutesRemaining));

            if (_state != State) {
                _state = State;
                RaisePropertyChanged(nameof(State));
            }
        }
    }

    public enum AgendaEntryType {
        Class,
        Exam
    }

    public enum AgendaEntryState {
        Past,
        Current,
        Future
    }

    /// <summary>
    ///     The type of scheduling conflict.
    ///     Used to determine whether we should
    ///     send reminders for a class :)
    /// </summary>
    public enum ScheduleConflictType {
        /// <summary>
        ///     No conflict exists. Happy days.
        /// </summary>
        NoConflict,
        /// <summary>
        ///     The conflict starts before
        ///     the class. Do not send a reminder.
        /// </summary>
        ConflictStartsBefore,
        /// <summary>
        ///     The conflict starts at the same
        ///     time. Do not send a reminder.
        /// </summary>
        ConflictStartsAtSameTime,
        /// <summary>
        ///     The conflict starts during, send
        ///     a reminder if the time is past a threshold.
        /// </summary>
        ConflictStartsDuring
    }
}
