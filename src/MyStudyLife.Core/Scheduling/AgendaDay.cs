﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;

namespace MyStudyLife.Scheduling {
    public class ScheduleMapEntry {
        private DaysOfWeek? _dayFlag;
        private int? _rotationDayFlag;

        public DateTime Date { get; private set; }

        public DayOfWeek Day {
            get { return this.Date.DayOfWeek; }
        }

        public DaysOfWeek DayFlag {
            get { return (_dayFlag ?? (_dayFlag = Day.ToFlag())).Value; }
        }

        public short? RotationWeek { get; internal set; }

        public int? RotationDay { get; internal set; }

        public int? RotationDayFlag {
            get {
                if (!_rotationDayFlag.HasValue && RotationDay.HasValue) {
                    _rotationDayFlag = 1 << (RotationDay.Value - 1);
                }

                return _rotationDayFlag;
            }
        }

        public Holiday Holiday { get; internal set; }

        public bool IsHoliday {
            get { return this.Holiday != null; }
        }

        public bool Schedulable { get; set; }

        internal ScheduleMapEntry(DateTime date) {
            this.Date = date;
        }
    }

    public class AgendaDay : ScheduleMapEntry {
        private readonly List<AgendaEntry> _entries;

        public IEnumerable<AgendaEntry> Entries {
            get { return _entries.AsEnumerable(); }
        }

        internal AgendaDay(DateTime date)
            : base(date) {
            _entries = new List<AgendaEntry>();
        }

        /// <remarks>
        ///     Does no verification (eg. date checking). 
        /// </remarks>
        internal void AddEntry(AgendaEntry entry) {
            _entries.Add(entry);
        }
    }

    public static class AgendaDayExtensions {
        public static AgendaDay ToAgendaDay(this ScheduleMapEntry map) {
            Check.NotNull(map, nameof(map));

            return new AgendaDay(map.Date) {
                RotationWeek = map.RotationWeek,
                RotationDay = map.RotationDay,
                Holiday = map.Holiday
            };
        }
    }
}
