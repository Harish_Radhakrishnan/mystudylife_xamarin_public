﻿using System;
using MyStudyLife.Data;

namespace MyStudyLife.Scheduling {
	public class ScheduleException : Exception {
		public DateTime TargetDate { get; private set; }
		public Class Class { get; private set; }

		public ScheduleException(DateTime targetDate) : this(targetDate, "Given class does not occur on the proposed target date.") { }
		public ScheduleException(DateTime targetDate, string message)
			: base(message) {
			this.TargetDate = targetDate;
		}
		public ScheduleException(string message, Exception inner) : base(message, inner) { }
	}
}
