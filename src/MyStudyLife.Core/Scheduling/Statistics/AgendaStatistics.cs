﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using Newtonsoft.Json;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.Scheduling.Statistics {
	public class AgendaStatistics {
		private SchedulableEntryStatistics _classes;
		private TasksStatistics _tasks;
		private SchedulableEntryStatistics _exams;

		public DateTime ForDate { get; set; }

        public bool IsHoliday { get; private set; }

        public Holiday Holiday { get; private set; }

		public SchedulableEntryStatistics Classes {
			get { return _classes ?? (_classes = new SchedulableEntryStatistics()); }
			set { _classes = value; } // WP7 Fix
		}

		public TasksStatistics Tasks {
			get { return _tasks ?? (_tasks = new TasksStatistics()); }
			set { _tasks = value; } // WP7 Fix
		}

		public SchedulableEntryStatistics Exams {
			get { return _exams ?? (_exams = new SchedulableEntryStatistics()); }
			set { _exams = value; } // WP7 Fix
		}

        public AgendaStatistics(DateTime forDate) {
            this.ForDate = forDate;
        }

        public static async Task<AgendaStatistics> ForDateAsync(DateTime d, IStorageProvider storageProvider) {
            var s = new AgendaStatistics(d);

            AgendaDay day = await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetDayForDateAsync(d);
            List<Task> tasks;

            var subjectGuids = day.Entries.Select(x => x.Base.Subject.Guid).Distinct().ToList();

            using (var taskRepo = new TaskRepository(storageProvider)) {
                tasks = (await taskRepo.GetAllNonDeletedAsync(t => t.DueDate == d ||
                    (
                        subjectGuids.Contains(t.SubjectGuid) &&
                        t.Progress < 100 && t.DueDate <= d
                    ))).ToList();
            }

            s.IsHoliday = day.IsHoliday;
            s.Holiday = day.Holiday;

            s.Classes.Attended = day.Entries.Count(x => x.Type == AgendaEntryType.Class && x.EndTime < d);
            s.Classes.Remaining = day.Entries.Count(x => x.Type == AgendaEntryType.Class && d < x.EndTime);

            s.Exams.Attended = day.Entries.Count(x => x.Type == AgendaEntryType.Exam && x.EndTime < d);
            s.Exams.Remaining = day.Entries.Count(x => x.Type == AgendaEntryType.Exam && d < x.EndTime);

            s.Tasks.Overdue = tasks.Count(t => t.IsOverdue);
            s.Tasks.Complete = tasks.Count(t => t.IsComplete);
            s.Tasks.Incomplete = tasks.Count(t => t.IsIncomplete);

            return s;
        }

		#region Statistics Classes

		[JsonObject]
		public class SchedulableEntryStatistics {
			[JsonProperty("remaining")]
			public int Remaining { get; set; }

			[JsonProperty("attended")]
			public int Attended { get; set; }

			[JsonProperty("total")]
			public int Total {
				get { return this.Remaining + this.Attended; }
			}
		}

		[JsonObject]
		public class TasksStatistics {
			[JsonProperty("overdue")]
			public int Overdue { get; set; }

			[JsonProperty("incomplete")]
			public int Incomplete { get; set; }

			[JsonProperty("complete")]
			public int Complete { get; set; }

			[JsonProperty("total")]
			public int Total {
				get { return this.Overdue + this.Incomplete + this.Complete; }
			}
		}

		#endregion
	}
}
