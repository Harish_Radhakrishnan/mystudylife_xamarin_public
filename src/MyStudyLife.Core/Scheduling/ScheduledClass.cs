﻿using System;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using Newtonsoft.Json;

namespace MyStudyLife.Scheduling {
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class ScheduledClass : BindableBase, IScheduledEntry {
        private Class _class;

        public Class Class {
            get { return _class; }
            private set {
                _class = value;

                if (value != null)
                    this.ClassGuid = value.Guid;
            }
        }
        
        public ClassTime ClassTime { get; private set; }

        public Guid ClassGuid { get; private set; }

        public DateTime Date { get; internal set; }

        public TimeSpan StartTime { get; private set; }

        public TimeSpan EndTime { get; private set; }

        public string Time => DateTimeFormat.FormatTimeRange(this.StartTime, this.EndTime);

        public Subject Subject => this.Class.Subject;

        public string Title => this.Class.Title;

        public string Location => this.ClassTime?.Location ?? this.Class.Location;

        public string TeacherName => this.ClassTime?.TeacherName ?? this.Class.TeacherName;

        internal ScheduledClass() { }

        internal ScheduledClass(Class cls, TimeSpan startTime, TimeSpan endTime) {
            Class = cls;
            StartTime = startTime;
            EndTime = endTime;
        }

        /// <summary>
        ///     Creates a new instance of <see cref="ScheduledClass"/>
        ///     for the given <paramref name="date"/> with no checks.
        /// </summary>
        public ScheduledClass(Class cls, ClassTime time, DateTime date)
            : this(cls, time.StartTime, time.EndTime) {

            this.ClassTime = time;
            this.Date = date;
        }

        public static ScheduledClass FromClass(Class cls) {
            if (cls == null) {
                throw new ArgumentNullException(nameof(cls));
            }

            if (!cls.Type.Equals(ClassType.OneOff)) {
                throw new ArgumentException(
                    String.Format(
                        "Class with type={0} not supported",
                        cls.Type
                    )
                );
            }

            // ReSharper disable PossibleInvalidOperationException
            return new ScheduledClass(cls, cls.StartTime.Value, cls.EndTime.Value) {
                Date = cls.Date.Value
            };
        }
    }
}