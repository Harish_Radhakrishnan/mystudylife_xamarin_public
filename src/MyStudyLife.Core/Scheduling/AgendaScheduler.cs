﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Data.Caching;
using MyStudyLife.Data.Settings;
using MyStudyLife.Extensions;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.Sync;
using MyStudyLife.Utility;

namespace MyStudyLife.Scheduling {
    public sealed class AgendaScheduler : IAgendaScheduler {
        public const int WeekRotationHolidayThreshold = 5;

        private readonly IMvxMessenger _messenger;
        private readonly IAcademicYearRepository _academicYearRepository;
        private readonly IClassRepository _classRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IExamRepository _examRepository;
        private readonly ICache _cache;
        private readonly IFeatureService _featureService;
        private readonly ITimetableSettingsStore _timetableSettingsStore;
        private readonly IUserStore _userStore;

        public AgendaScheduler(
            IMvxMessenger messenger,
            IAcademicYearRepository academicYearRepository,
            IClassRepository classRepository,
            ITaskRepository taskRepository,
            IExamRepository examRepository,
            ICache cache,
            IFeatureService featureService,
            ITimetableSettingsStore timetableSettingsStore,
            IUserStore userStore
        ) {
            this._messenger = messenger;
            this._academicYearRepository = academicYearRepository;
            this._classRepository = classRepository;
            this._taskRepository = taskRepository;
            this._examRepository = examRepository;
            this._cache = cache;
            this._featureService = featureService;
            this._timetableSettingsStore = timetableSettingsStore;
            this._userStore = userStore;
        }

        #region Maps

        class ScheduleCacheExpiration : ICacheExpiration, IDisposable {
            private MvxSubscriptionToken _acYearSubscription, _holSubscription, _syncSubscription;

            private readonly Guid _yearGuid;

            public bool HasExpired { get; private set; }

            public ScheduleCacheExpiration(IMvxMessenger messenger, AcademicYear year) {
                this._yearGuid = year.Guid;

                _acYearSubscription = messenger.SubscribeOnThreadPoolThread<EntityEventMessage<AcademicYear>>(OnMessage);
                _holSubscription = messenger.SubscribeOnThreadPoolThread<EntityEventMessage<Holiday>>(OnMessage);
                _syncSubscription = messenger.SubscribeOnThreadPoolThread<ProxyEventMessage<SyncCompletedEventArgs>>(OnMessage);
            }

            private void OnMessage(EntityEventMessage<AcademicYear> message) {
                if (this.HasExpired) return;

                if (message.Entity != null && message.Entity.Guid == _yearGuid) {
                    this.HasExpired = true;
                }
            }

            private void OnMessage(EntityEventMessage<Holiday> message) {
                if (this.HasExpired) return;

                if (message.Entity != null && message.Entity.YearGuid == _yearGuid) {
                    this.HasExpired = true;
                }
            }

            private void OnMessage(ProxyEventMessage<SyncCompletedEventArgs> message) {
                if (this.HasExpired) return;

                if (message.Args.AcademicYearsModified &&
                    message.Args.ModifiedAcademicYears.Any(x => x.Guid == _yearGuid)) {

                    this.HasExpired = true;
                }

                if (message.Args.HolidaysModified &&
                    message.Args.ModifiedHolidays.Any(x => x.YearGuid == _yearGuid)) {

                    this.HasExpired = true;
                }
            }

            public void Dispose() {
                if (_acYearSubscription != null) {
                    _acYearSubscription.Dispose();
                    _acYearSubscription = null;
                }

                if (_holSubscription != null) {
                    _holSubscription.Dispose();
                    _holSubscription = null;
                }

                if (_syncSubscription != null) {
                    _syncSubscription.Dispose();
                    _syncSubscription = null;
                }
            }
        }

        private IReadOnlyDictionary<DateTime, ScheduleMapEntry> GetScheduleMap(AcademicYear schedule, DateTime startDate, DateTime endDate) {
            Check.NotNull(schedule);

            IReadOnlyDictionary<DateTime, ScheduleMapEntry> readOnlyMap;

            // See GetDayInfoMap for key explaination
            var cacheKey = $"{schedule.Guid:n}_{schedule.UpdatedAt:yyyyMMddHHmmss_}{startDate:yyyyMMdd}_{endDate:yyyyMMdd}";

            if (_cache.TryGet(cacheKey, out readOnlyMap)) {
                return readOnlyMap;
            }

            Func<DateTime, ScheduleMapEntry> getSchedulePropertiesForDay;

            switch (schedule.Scheduling.Mode) {
                case SchedulingMode.WeekRotation:
                    var rotationWeekMap = GetRotationWeekMap(schedule, endDate);

                    getSchedulePropertiesForDay = (date) => {
                        short? rotationWeek;

                        rotationWeekMap.TryGetValue(date, out rotationWeek);

                        return new ScheduleMapEntry(date) {
                            RotationWeek = rotationWeek
                        };
                    };
                    break;
                case SchedulingMode.DayRotation:
                    var rotationDayMap = GetRotationDayMap(schedule, endDate);

                    getSchedulePropertiesForDay = (date) => {
                        int? rotationDay;

                        rotationDayMap.TryGetValue(date, out rotationDay);

                        return new ScheduleMapEntry(date) {
                            RotationDay = rotationDay
                        };
                    };
                    break;
                default:
                    getSchedulePropertiesForDay = (date) => new ScheduleMapEntry(date);
                    break;
            }

            var map = new Dictionary<DateTime, ScheduleMapEntry>();

            var dayInfoMap = GetDayInfoMap(schedule);

            var diff = (int)(endDate - startDate).TotalDays + 1;
            var iDate = startDate;

            for (int d = 0; d < diff; d++) {
                var mapEntry = getSchedulePropertiesForDay(iDate);
                DayInfo dayInfo;

                if (dayInfoMap.TryGetValue(iDate, out dayInfo)) {
                    mapEntry.Holiday = dayInfo.Holiday;
                    mapEntry.Schedulable = dayInfo.Schedulable;
                }
                else {
                    mapEntry.Schedulable = false;
                }

                map[iDate] = mapEntry;

                iDate = iDate.AddDays(1d);
            }

            readOnlyMap = new ReadOnlyDictionary<DateTime, ScheduleMapEntry>(map);

            _cache.Set(cacheKey, readOnlyMap, new ScheduleCacheExpiration(_messenger, schedule));

            return readOnlyMap;
        }

        private IReadOnlyDictionary<DateTime, int?> GetRotationDayMap(AcademicYear schedule, DateTime? toDate = null) {
            Check.NotNull(schedule);

            if (schedule.Scheduling.Mode != SchedulingMode.DayRotation) {
                throw new ArgumentException("Scheduling mode is not day rotation.", nameof(schedule));
            }

            var map = new Dictionary<DateTime, int?>();

            if (toDate == null || toDate.Value.IsAfter(schedule.EndDate)) {
                toDate = schedule.EndDate;
            }

            var diff = (int)(toDate.Value.Date - schedule.StartDate.Date).TotalDays + 1;
            var iDate = schedule.StartDate.Date;
            int? rotationDay = schedule.Scheduling.StartDay.GetValueOrDefault(1);
            var dayCount = schedule.Scheduling.DayCount.GetValueOrDefault(SchedulingOptions.DefaultDayCount);
            var days = schedule.Scheduling.Days.GetValueOrDefault(SchedulingOptions.DefaultDays);
            var adjustments = new Dictionary<DateTime, short>();

            if (schedule.Scheduling.Adjustments != null) {
                foreach (var adjustment in schedule.Scheduling.Adjustments) {
                    if (adjustment.Type == SchedulingAdjustmentType.RotationDay && adjustment.Value.HasValue) {
                        adjustments[adjustment.Date] = adjustment.Value.Value;
                    }
                }
            }

            var dayInfoMap = GetDayInfoMap(schedule);

            for (int i = 0; i < diff; i++) {
                var dayFlag = iDate.DayOfWeek.ToFlag();

                DayInfo dayInfo;
                if (!dayInfoMap.TryGetValue(iDate, out dayInfo)) {
                    dayInfo = DayInfo.Default;
                }

                short adjustedDay;
                if (adjustments.TryGetValue(iDate, out adjustedDay)) {
                    rotationDay = adjustedDay;
                }
                else if (schedule.Scheduling.Scope == SchedulingScope.Auto && dayInfo.Special == SpecialDayKind.TermStart) {
                    rotationDay = schedule.Scheduling.StartDay.GetValueOrDefault(1);
                }

                if ((days & dayFlag) == dayFlag) {
                    var holidayForDate = dayInfo.Holiday;

                    map[iDate] = holidayForDate != null ? null : rotationDay;

                    if (holidayForDate == null || !holidayForDate.PushesSchedule) {
                        rotationDay = (rotationDay % dayCount + 1);
                    }
                }
                else {
                    map[iDate] = null;
                }

                iDate = iDate.AddDays(1d);
            }

            return map;
        }

        private IReadOnlyDictionary<DateTime, short?> GetRotationWeekMap(AcademicYear schedule, DateTime? toDate = null) {
            Check.NotNull(schedule);

            if (schedule.Scheduling.Mode != SchedulingMode.WeekRotation) {
                throw new ArgumentException("Scheduling mode is not week rotation.", nameof(schedule));
            }

            var map = new Dictionary<DateTime, short?>();

            if (toDate == null || toDate.Value.IsAfter(schedule.EndDate)) {
                toDate = schedule.EndDate;
            }

            var lastDayOfWeek = DateTimeFormat.LastDayOfWeek;
            var diff = schedule.StartDate.Range(toDate.Value).CalendarDays;
            var iDate = schedule.StartDate.Date;
            short rotationWeek = schedule.Scheduling.StartWeek.GetValueOrDefault(1);
            var weekCount = schedule.Scheduling.WeekCount.GetValueOrDefault(SchedulingOptions.DefaultWeekCount);
            var adjustments = new Dictionary<DateTime, short>();

            if (schedule.Scheduling.Adjustments != null) {
                foreach (var adjustment in schedule.Scheduling.Adjustments) {
                    if (adjustment.Type == SchedulingAdjustmentType.RotationWeek && adjustment.Value.HasValue) {
                        adjustments[adjustment.Date] = adjustment.Value.Value;
                    }
                }
            }

            var dayInfoMap = GetDayInfoMap(schedule);
            int daysOfHolidayThatPushesScheduleInWeek = 0;

            for (int d = 0; d < diff; d++) {
                DayInfo dayInfo;
                if (!dayInfoMap.TryGetValue(iDate, out dayInfo)) {
                    dayInfo = DayInfo.Default;
                }

                short adjustedWeek;
                if (adjustments.TryGetValue(iDate, out adjustedWeek)) {
                    rotationWeek = adjustedWeek;
                }
                else if (schedule.Scheduling.Scope == SchedulingScope.Auto && dayInfo.Special == SpecialDayKind.TermStart) {
                    rotationWeek = schedule.Scheduling.StartWeek.GetValueOrDefault(1);
                }

                if (dayInfo.Holiday != null) {
                    map[iDate] = null;

                    if (dayInfo.Holiday.PushesSchedule) {
                        daysOfHolidayThatPushesScheduleInWeek++;
                    }
                }
                else {
                    map[iDate] = rotationWeek;
                }

                if (iDate.DayOfWeek == lastDayOfWeek) {
                    if (daysOfHolidayThatPushesScheduleInWeek < WeekRotationHolidayThreshold) {
                        rotationWeek = (short)(rotationWeek % weekCount + 1);
                    }

                    daysOfHolidayThatPushesScheduleInWeek = 0;
                }

                iDate = iDate.AddDays(1);
            }

            return map;
        }

        class DayInfo {
            public Holiday Holiday { get; set; }
            public bool Schedulable { get; set; }
            public SpecialDayKind Special { get; set; }

            public static DayInfo Default => new DayInfo();
        }

        enum SpecialDayKind {
            None,
            TermStart,
            TermEnd,
            YearStart,
            YearEnd
        }

        private IReadOnlyDictionary<DateTime, DayInfo> GetDayInfoMap(AcademicYear schedule) {
            IReadOnlyDictionary<DateTime, DayInfo> readOnlyMap;

            // We store the cache keys with the updated at timestamp our method of clearing
            // the cache is subject to race conditions as demonstrated in MSL-403 where the
            // day info map is accessed from the cache before it's invalidated with different
            // start/end dates causing a KeyNotFoundException.
            var cacheKey = $"{schedule.Guid:n}_{schedule.UpdatedAt:yyyyMMddHHmmss_}day_info";

            if (_cache.TryGet(cacheKey, out readOnlyMap)) {
                return readOnlyMap;
            }

            var map = new Dictionary<DateTime, DayInfo>();

            var scopeIsTerm = schedule.Scheduling.Scope == SchedulingScope.Auto && schedule.Terms.Any();

            int diff;
            DateTime iDay;
            int i;

            if (scopeIsTerm) {
                foreach (var term in schedule.Terms) {
                    diff = term.StartDate.Range(term.EndDate).CalendarDays;
                    iDay = term.StartDate;

                    for (i = 0; i < diff; i++) {
                        map[iDay] = new DayInfo {
                            Special = i == 0 ? SpecialDayKind.TermStart : ((i + 1 == diff) ? SpecialDayKind.TermEnd : SpecialDayKind.None),
                            Schedulable = true
                        };

                        iDay = iDay.AddDays(1);
                    }
                }
            }

            foreach (var holiday in schedule.Holidays) {
                diff = holiday.StartDate.Range(holiday.EndDate).CalendarDays;
                iDay = holiday.StartDate;

                for (i = 0; i < diff; i++) {
                    var dayInfo = map.GetOrAdd(iDay);

                    dayInfo.Holiday = holiday;
                    dayInfo.Schedulable = false;

                    iDay = iDay.AddDays(1);
                }
            }

            diff = schedule.StartDate.Range(schedule.EndDate).CalendarDays;
            iDay = schedule.StartDate;

            for (i = 0; i < diff; i++) {
                DayInfo dayInfo;

                if (!map.TryGetValue(iDay, out dayInfo)) {
                    dayInfo = new DayInfo();

                    if (!scopeIsTerm) {
                        dayInfo.Special = i == 0 ? SpecialDayKind.YearStart : (i + 1 == diff ? SpecialDayKind.YearEnd : SpecialDayKind.None);
                        dayInfo.Schedulable = true;
                    }
                    else {
                        dayInfo.Schedulable = false;
                    }
                    map[iDay] = dayInfo;
                }

                iDay = iDay.AddDays(1);
            }

            readOnlyMap = new ReadOnlyDictionary<DateTime, DayInfo>(map);

            _cache.Set(cacheKey, readOnlyMap, new ScheduleCacheExpiration(_messenger, schedule));

            return readOnlyMap;
        }

        private IReadOnlyDictionary<DateTime, ScheduleMapEntry> GetLegacyScheduleMap(TimetableSettings tSettings, DateTime startDate, DateTime endDate) {
            Check.NotNull(tSettings, nameof(tSettings));

            var map = new Dictionary<DateTime, ScheduleMapEntry>();

            var diff = (int) (endDate.Date - startDate.Date).TotalDays + 1;
            var iDate = startDate.Date;

            var isWeekRotation = tSettings.Mode == TimetableMode.WeekRotation;
            var isDayRotation = tSettings.Mode == TimetableMode.DayRotation;
            var rotationDayMap = isDayRotation ? tSettings.GetDaysForDateRange(startDate, endDate) : null;

            for (int d = 0; d < diff; d++) {
                var mapEntry = map[iDate] = new ScheduleMapEntry(iDate);

                if (isWeekRotation) {
                    mapEntry.RotationWeek = (short?)tSettings.GetWeekForDate(iDate);
                }
                else if (isDayRotation) {
                    mapEntry.RotationDay = rotationDayMap[iDate];
                }

                iDate = iDate.AddDays(1d);
            }

            return new ReadOnlyDictionary<DateTime, ScheduleMapEntry>(map);
        }

        #endregion

        #region Entries

        private Func<AgendaEntry, bool> GetConflictPredicate(AgendaEntry ae) {
            return x => ae.BaseGuid != x.BaseGuid && ClassRepository.CheckForConflict(x.StartTime, x.EndTime, ae.StartTime, ae.EndTime);
        }

        private void SetConflictCount(IEnumerable<AgendaEntry> entries) {

            bool anyConflicting = false;

            foreach (var entry in entries) {
                var ae = entry;

                // WARNING: Conflict count isn't really a "count", it's a value used
                // to determine the width of the entry... perhaps this should be renamed
                // in the future.
                ae.ConflictCount = 0;

                var conflicting = entries.Where(GetConflictPredicate(ae)).ToList();

                foreach (var ae1 in conflicting) {
                    // +1 to account for `ae`
                    var conflictCount = conflicting.Count(GetConflictPredicate(ae1)) + 1;

                    if (conflictCount > ae.ConflictCount) {
                        ae.ConflictCount = conflictCount;
                    }
                }

                ae.ConflictEntries = conflicting;

                if (ae.ConflictCount > 0) {
                    anyConflicting = true;
                }
            }

            // Don't bother sorting/looping again if there's no conflicts
            if (!anyConflicting) {
                return;
            }

            foreach (var ae in entries.OrderBy(x => x.ConflictCount)) {
                ae.ConflictIndex = 0;

                foreach (var x in ae.ConflictEntries) {
                    if (x.ConflictEntries.Count <= ae.ConflictEntries.Count && x.ConflictIndex == ae.ConflictIndex) {
                        ae.ConflictIndex++;
                    }
                }
            }
        }

        public async System.Threading.Tasks.Task SetTaskCountsAsync(IEnumerable<AgendaEntry> entries) {
            foreach (var ae in entries) {
                var sc = ae.Base as ScheduledClass;

                if (sc == null) {
                    continue;
                }

                var counts = await _taskRepository.GetCountsForScheduledClassAsync(sc);

                ae.TasksComplete = counts.Complete;
                ae.TasksIncomplete = counts.Incomplete;

                if (!ae.IsPast) {
                    ae.TasksOverdue = counts.Overdue;
                }
            }
        }

        public short? GetRotationWeekForDate(AcademicYear schedule, DateTime date) {
            var weekStart = date.StartOfWeek();

            if (schedule.Scheduling.Mode != SchedulingMode.WeekRotation || weekStart < schedule.StartDate.StartOfWeek() || schedule.EndDate.StartOfWeek() < weekStart) {
                return null;
            }

            short? rotationWeek;

            GetRotationWeekMap(schedule, date).TryGetValue(date.Date, out rotationWeek);

            return rotationWeek;
        }

        public int? GetRotationDayForDate(AcademicYear schedule, DateTime date) {
            if (schedule.Scheduling.Mode != SchedulingMode.DayRotation || date < schedule.StartDate || schedule.EndDate < date) {
                return null;
            }

            int? rotationDay;

            GetRotationDayMap(schedule, date).TryGetValue(date.Date, out rotationDay);

            return rotationDay;
        }

		public async Task<IEnumerable<AgendaEntry>> GetForDateRangeAsync(DateTime startDate, DateTime endDate) {
		    return (await GetByDayForDateRangeAsync(startDate, endDate)).SelectMany(x => x.Entries);
		}

        public async Task<IEnumerable<AgendaDay>> GetByDayForDateRangeAsync(DateTime startDate, DateTime endDate) {
            if (startDate > endDate) throw new ArgumentException("Start date must not be greater than end date.");

            var user = _userStore.GetCurrentUser();
            var canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);

            startDate = startDate.StartOfDay();
            endDate = endDate.EndOfDay();

            #region Map Setup

            var map = new Dictionary<DateTime, AgendaDay>();

            var diff = (int) (endDate - startDate).TotalDays + 1;
            var iDate = startDate;

            for (int d = 0; d < diff; d++) {
                map[iDate] = new AgendaDay(iDate);

                iDate = iDate.AddDays(1d);
            }

            var academicYears = (await _academicYearRepository.GetForDateRange(startDate, endDate)).ToList();

            var tSettings = await _timetableSettingsStore.GetAsync();

            var scheduleGuidsInRange = academicYears.Select(x => x.Guid).ToList();

            if (academicYears.Any()) {
                foreach (var y in academicYears) {
                    var scheduleMap = GetScheduleMap(
                        y,
                        y.StartDate.IsBefore(startDate) ? startDate : y.StartDate,
                        y.EndDate.IsAfter(endDate) ? endDate : y.EndDate
                    );

                    foreach (var dMap in scheduleMap) {
                        map[dMap.Key] = dMap.Value.ToAgendaDay();
                    }

                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var t in y.Terms) {
                        if (DateTimeRange.Intercepts(startDate, endDate, t.StartDate, t.EndDate)) {
                            scheduleGuidsInRange.Add(t.Guid);
                        }
                    }
                }
            }
            else if (tSettings != null && tSettings.Mode != TimetableMode.Fixed) {
                var scheduleMap = GetLegacyScheduleMap(tSettings, startDate, endDate);

                foreach (var dMap in scheduleMap) {
                    map[dMap.Key] = dMap.Value.ToAgendaDay();
                }
            }

            #endregion

            List<Class> classes = (await _classRepository.GetByDateRangeAndScheduleGuidsAsync(startDate, endDate, scheduleGuidsInRange.ToArray())).ToList(); ;
            List<Exam> exams = (await _examRepository.GetForDateRangeAsync(startDate, endDate)).ToList();

            foreach (var c in classes) {
                if (c.Date.HasValue) {
                    if (c.Date.Value.IsBetween(startDate, endDate)) {
                        var ae = new AgendaEntry(ScheduledClass.FromClass(c));
                        ae.SetColor(user, canUseExtendedColors);
                        map[c.Date.Value.StartOfDay()].AddEntry(ae);
                    }

                    continue;
                }

                var schedule = c.ActualSchedule;

                if (schedule != null && !scheduleGuidsInRange.Contains(schedule.Guid)) {
                    continue;
                }

                var classStartDate = c.EffectiveStartDate ?? startDate;
                var classEndDate = c.EffectiveEndDate ?? endDate;

                if (c.HasStartEndDates) {
                    if ((
                        /**
                         *      START        END
                         * 1 -----|-----      |
                         * 2 -----|-----------|-----
                         * 3      |  -------  |
                         * 4      |      -----|-----
                         */
                        (classStartDate <= startDate && startDate <= classEndDate && classEndDate <= endDate) ||
                        (classStartDate <= startDate && endDate <= classEndDate) ||
                        (startDate <= classStartDate && classEndDate <= endDate) ||
                        (startDate <= classStartDate && classStartDate <= endDate && endDate <= classEndDate)
                    ) == false) {
                        continue;
                    }
                }

                IReadOnlyDictionary<DateTime, ScheduleMapEntry> scheduleMap;
                bool isWeekRotation = false, isDayRotation = false;

                if (schedule != null) {
                    var yearGuid = (schedule as AcademicYear)?.Guid ?? ((AcademicTerm) schedule).YearGuid;

                    var year = academicYears.FirstOrDefault(x => x.Guid == yearGuid);

                    // If the year doesn't exist in our collection of academic years, retrieve it
                    // from the repo as we don't recursively set the children on objects
                    if (year == null) {
                        year = await _academicYearRepository.GetAsync(yearGuid);
                    }

                    scheduleMap = GetScheduleMap(year, startDate, endDate);
                    isWeekRotation = year.Scheduling.Mode == SchedulingMode.WeekRotation;
                    isDayRotation = year.Scheduling.Mode == SchedulingMode.DayRotation;
                }
                else if (tSettings != null) {
                    scheduleMap = GetLegacyScheduleMap(tSettings, startDate, endDate);
                    isWeekRotation = tSettings.Mode == TimetableMode.WeekRotation;
                    isDayRotation = tSettings.Mode == TimetableMode.DayRotation;
                }
                else {
                    var fixedScheduleMap = new Dictionary<DateTime, ScheduleMapEntry>();

                    var classDiff = (int) (endDate - startDate).TotalDays + 1;
                    var jDate = startDate;

                    for (int d = 0; d < classDiff; d++) {
                        fixedScheduleMap[jDate] = new ScheduleMapEntry(jDate);

                        jDate = jDate.AddDays(1d);
                    }

                    scheduleMap = new ReadOnlyDictionary<DateTime, ScheduleMapEntry>(fixedScheduleMap);
                }

                foreach (var dMap in scheduleMap) {
                    var mapEntry = map[dMap.Key];

                    if (!(dMap.Value?.Schedulable).GetValueOrDefault(true) || !c.CanScheduleFor(dMap.Key)) {
                        continue;
                    }

                    foreach (var t in c.Times) {
                        if (t.RotationDays.HasValue && t.RotationDays.Value > 0) {
                            if (!isDayRotation) {
                                continue;
                            }

                            if (!dMap.Value.RotationDayFlag.HasValue || ((int)t.RotationDays.Value & dMap.Value.RotationDayFlag.Value) != dMap.Value.RotationDayFlag.Value) {
                                continue;
                            }
                        }
                        else {
                            if (t.RotationWeek.HasValue && t.RotationWeek.Value > 0) {
                                if (!isWeekRotation) {
                                    continue;
                                }

                                if (!dMap.Value.RotationWeek.HasValue || dMap.Value.RotationWeek.Value != (short)t.RotationWeek.Value) {
                                    continue;
                                }
                            }

                            if (dMap.Value.IsHoliday || (t.Days.Value & dMap.Value.DayFlag) != dMap.Value.DayFlag) {
                                continue;
                            }
                        }

                        var sc = new ScheduledClass(c, t, dMap.Key);
                        var ae = new AgendaEntry(sc);
                        ae.SetColor(user, canUseExtendedColors);
                        mapEntry.AddEntry(ae);
                    }
                }
            }

            foreach (var e in exams) {
                var ae = new AgendaEntry(e);
                ae.SetColor(user, canUseExtendedColors);
                map[e.Date.StartOfDay()].AddEntry(ae);
            }

            return map.Values;
        }

		[Obsolete("Use overload with AgendaSchedulerOptions")]
		public Task<IEnumerable<AgendaEntry>> GetForDateAsync(DateTime date, bool setConflictCounts = false) {
		    return GetForDateAsync(date, AgendaSchedulerOptions.SetConflictCount);
		}

        public async Task<AgendaDay> GetDayForDateAsync(DateTime date, AgendaSchedulerOptions options = AgendaSchedulerOptions.None) {
            var day = (await GetByDayForDateRangeAsync(date.StartOfDay(), date.EndOfDay())).First();

            if (options.HasFlag(AgendaSchedulerOptions.SetConflictCount)) {
                SetConflictCount(day.Entries);
            }

            if (options.HasFlag(AgendaSchedulerOptions.SetTaskCounts)) {
                await SetTaskCountsAsync(day.Entries);
            }

            return day;
        }

        /// <summary>
        ///		Gets classes and exams (settings dependent) for the given date.
        /// </summary>
        /// <param name="date">The date to get the entries for.</param>
        /// <param name="options">Extra data retrieval options.</param>
        public async Task<IEnumerable<AgendaEntry>> GetForDateAsync(DateTime date, AgendaSchedulerOptions options = AgendaSchedulerOptions.None) {
            // TODO: Agenda entries should throw exceptions when conflict and task are accessed without being set

            var entries = (await GetForDateRangeAsync(date.StartOfDay(), date.EndOfDay())).ToList();

            if (options.HasFlag(AgendaSchedulerOptions.SetConflictCount)) {
                SetConflictCount(entries);
            }

            if (options.HasFlag(AgendaSchedulerOptions.SetTaskCounts)) {
                await SetTaskCountsAsync(entries);
            }

            return entries;
        }

        public async Task<IEnumerable<AgendaDay>> GetByDayForWeekAsync(DateTime weekBeginning) {
            var weekEnding = weekBeginning.EndOfWeek();

            var days = (await GetByDayForDateRangeAsync(weekBeginning, weekEnding)).ToList();

            foreach (var d in days) {
                SetConflictCount(d.Entries);
            }

            return days;
        }

        public async Task<NextDay> GetNextScheduledDayAsync(DateTime startDate) {
            var thisYear = await _academicYearRepository.GetForDateAsync(startDate);

            if (thisYear != null) {
                var dayInfoMap = GetDayInfoMap(thisYear);

                // We use a gradually increasing number of ranges as the further away
                // we get from the current day, the less likely there are to be events/tasks
                var ranges = new [] { 0, 1, 3, 7, 28 };
                int? currentRange = null;
                var date = startDate.Date.AddDays(1d);
                Holiday holiday = null;
                DateTime? potentialNextDay = null;
                DateTime? potentialNextDayTask = null;

                for (var i = 0; i < ranges.Length;) {
                    if (thisYear.EndDate < date || potentialNextDay != null || potentialNextDayTask != null) {
                        break;
                    }

                    var rangeStart = date;
                    var range = currentRange.GetValueOrDefault(ranges[i]);
                    var rangeEnd = DateTimeEx.Min(rangeStart.AddDays(range), thisYear.EndDate);

                    var entries = await this.GetForDateRangeAsync(rangeStart, rangeEnd);
                    // Not awaited, postpone the work until we know we need to do it!

                    if (entries.Any()) {
                        potentialNextDay = entries.OrderBy(x => x.StartTime).First().StartTime.Date;
                    }
                    // Check if there's a sooner "next day" based on the user's tasks, if the entries don't
                    // occur on the first day of the range
                    else if (rangeStart < potentialNextDay || potentialNextDay == null) {
                        var tasks = await _taskRepository.GetIncompleteByDueDateAsync(rangeStart, rangeEnd);
                        if (tasks.Any()) {
                            potentialNextDayTask = tasks.OrderBy(t => t.DueDate).First().DueDate;
                        }
                    }

                    for (var d = rangeStart; d <= rangeEnd; d = d.AddDays(1)) {
                        var h = dayInfoMap[d].Holiday;
                        if (h != null) {
                            holiday = h;

                            // If holiday ends after current range, use the remaining duration
                            // as the next range, as chances are there are no events/tasks.
                            if (h.EndDate > rangeEnd) {
                                currentRange = (int)(holiday.EndDate - rangeEnd).TotalDays;
                                break;
                            }
                        }
                    }

                    // Increment for next loop
                    date = rangeEnd.AddDays(1);

                    if (i < (ranges.Length - 1)) {
                        i++;
                    }
                }

                if (potentialNextDay != null && (potentialNextDayTask == null || potentialNextDay <= potentialNextDayTask)) {
                    var nextDay = potentialNextDay.Value;
                    var nextTerm = thisYear.Terms?.OrderBy(t => t.StartDate).Where(t => t.StartDate.IsAfter(startDate)).FirstOrDefault();
                    var type = NextDayType.NextDay;

                    if (nextDay.IsTomorrow()) {
                        type = NextDayType.Tomorrow;
                    }
                    // If the date we've selected is in the next term (even if not the start date) then display as such
                    else if (nextTerm != null && nextDay.IsBetween(nextTerm.StartDate, nextTerm.EndDate, DateTimeComparison.Day)) {
                        type = NextDayType.NextTerm;
                    }
                    else if (holiday != null && nextDay.IsAfter(holiday.EndDate, DateTimeComparison.Day)) {
                        return new NextDay(nextDay, holiday);
                    }

                    return new NextDay(type, nextDay);
                }
                else if (potentialNextDayTask != null) {
                    return new NextDay(
                        potentialNextDayTask.Value.IsTomorrow() ? NextDayType.Tomorrow : NextDayType.None,
                        potentialNextDayTask.Value
                    );
                }
            }
            else {
                // TODO: Handle what happens when first day of new year is a holiday?
                var nextYear = await _academicYearRepository.GetYearAfterDateAsync(startDate);
                if (nextYear != null) {
                    return new NextDay(NextDayType.NextAcademicYear, nextYear.StartDate);
                }
            }

            return null;
        }

        #endregion
    }

    [Flags]
    public enum AgendaSchedulerOptions {
        None,
        SetConflictCount,
        SetTaskCounts
    }

    /// <summary>
    ///     Represents the next day that the user has an event,
    ///     task or otherwise on.
    /// </summary>
    public class NextDay {
        /// <summary>
        ///     The reason why we've selected this date.
        /// </summary>
        public NextDayType Type { get; }

        /// <summary>
        ///     The date for the next day.
        /// </summary>
        public DateTime Date { get; }

        /// <summary>
        ///     The holiday that occurs immediately before
        ///     this date, if any.
        /// </summary>
        public Holiday PreviousHoliday { get; }

        public NextDay(NextDayType type, DateTime date) {
            Type = type;
            Date = date;
        }

        public NextDay(DateTime date, Holiday previousHoliday) : this(NextDayType.DayAfterHoliday, date) {
            PreviousHoliday = previousHoliday;
        }
    }

    public enum NextDayType {
        None,
        Tomorrow,
        AfterWeekend,
        DayAfterHoliday,
        NextTerm,
        NextAcademicYear,
        NextDay
    }
}
