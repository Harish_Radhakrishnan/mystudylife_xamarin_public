﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyStudyLife.Data;

namespace MyStudyLife.Scheduling {
    public interface IAgendaScheduler {
        Task<IEnumerable<AgendaDay>> GetByDayForDateRangeAsync(DateTime startDate, DateTime endDate);
        Task<IEnumerable<AgendaDay>> GetByDayForWeekAsync(DateTime weekBeginning);
        Task<AgendaDay> GetDayForDateAsync(DateTime date, AgendaSchedulerOptions options = AgendaSchedulerOptions.None);
        Task<IEnumerable<AgendaEntry>> GetForDateAsync(DateTime date, bool setConflictCounts = false);
        Task<IEnumerable<AgendaEntry>> GetForDateAsync(DateTime date, AgendaSchedulerOptions options = AgendaSchedulerOptions.None);
        Task<IEnumerable<AgendaEntry>> GetForDateRangeAsync(DateTime startDate, DateTime endDate);
        int? GetRotationDayForDate(AcademicYear schedule, DateTime date);
        short? GetRotationWeekForDate(AcademicYear schedule, DateTime date);
        System.Threading.Tasks.Task SetTaskCountsAsync(IEnumerable<AgendaEntry> entries);
        Task<NextDay> GetNextScheduledDayAsync(DateTime startDate);
    }
}