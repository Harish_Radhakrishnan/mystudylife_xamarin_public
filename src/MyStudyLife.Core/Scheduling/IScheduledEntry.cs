﻿using System;
using MyStudyLife.Data;

namespace MyStudyLife.Scheduling {
	public interface IScheduledEntry {
		Subject Subject { get; }

		string Title { get; }

		DateTime Date { get; }

		TimeSpan StartTime { get; }

		TimeSpan EndTime { get; }

		string Time { get; }

		string Location { get; }
    }
}
