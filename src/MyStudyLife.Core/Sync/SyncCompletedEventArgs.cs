﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;

namespace MyStudyLife.Sync {
    public sealed class SyncCompletedEventArgs : EventArgs {
        public User User { get; internal set; }

        public UserSettings UserSettings { get; internal set; }

        public IEnumerable<AcademicYear> ModifiedAcademicYears { get; internal set; }

        public IEnumerable<Holiday> ModifiedHolidays { get; internal set; } 

        public IEnumerable<Subject> ModifiedSubjects { get; internal set; }

        public IEnumerable<Class> ModifiedClasses { get; internal set; }

        public IEnumerable<Task> ModifiedTasks { get; internal set; }

        public IEnumerable<Exam> ModifiedExams { get; internal set; }

        public TimetableSettings TimetableSettings { get; internal set; }

        public DeviceSettings DeviceSettings { get; internal set; }

        public bool UserModified {
            get { return User != null; }
        }

        public bool UserSettingsModified {
            get { return UserSettings != null; }
        }

        public bool AcademicYearsModified {
            get { return ModifiedAcademicYears != null && ModifiedAcademicYears.Any(); }
        }

        public bool HolidaysModified {
            get { return ModifiedHolidays != null && ModifiedHolidays.Any(); }
        }

        public bool SubjectsModified {
            get { return ModifiedSubjects != null && ModifiedSubjects.Any(); }
        }

        public bool ClassesModified {
            get { return ModifiedClasses != null && ModifiedClasses.Any(); }
        }

        public bool TasksModified {
            get { return ModifiedTasks != null && ModifiedTasks.Any(); }
        }

        public bool ExamsModified {
            get { return ModifiedExams != null && ModifiedExams.Any(); }
        }

        public bool TimetableSettingsModified {
            get { return TimetableSettings != null; }
        }

        public bool DeviceSettingsModified {
            get { return DeviceSettings != null; }
        }
        
        /// <summary>
        ///     Returns true if anything at all has changed
        ///     (include device settings and user profile).
        /// </summary>
        public bool HasAnythingChanged {
            get {
                return UserModified || DeviceSettingsModified || HasAnyUserDataChanged;
            }
        }

        /// <summary>
        ///     Returns true if any user data has changed
        ///     (everything but user and device settings).
        /// </summary>
        public bool HasAnyUserDataChanged {
            get {
                return UserSettingsModified || AcademicYearsModified || HolidaysModified || SubjectsModified ||
                       ClassesModified || TasksModified || ExamsModified || TimetableSettingsModified;
            }
        }
    }
}
