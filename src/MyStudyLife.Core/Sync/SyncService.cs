﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Sync.Processors;
using Newtonsoft.Json;
using SQLite;
using SQLiteNetExtensions.Extensions;
using SQLiteNetExtensionsAsync.Extensions;
using AsyncTask = System.Threading.Tasks.Task;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.Sync {
    public sealed class SyncService : ISyncService {
        private readonly IUserStore _userStore;

        #region Properties

        public const string SyncFlagProp = "_needsSyncing";

        private const string LastUpdatedKey = "MSL_SYNC_LAST_UPDATED";
        private const string TimestampKey = "MSL_SYNC_TIMESTAMP";

        private IStorageProvider StorageProvider { get; set; }
        private INetworkProvider NetworkProvider { get; set; }
        private IHttpApiClient ApiClient { get; set; }

        private DateTime? _lastUpdated;
        public DateTime? LastUpdated {
            get => _lastUpdated ?? (_lastUpdated = StorageProvider.GetSetting<DateTime?>(LastUpdatedKey));
            private set {
                StorageProvider.AddOrUpdateSetting(LastUpdatedKey, value);

                _lastUpdated = value;
            }
        }

        private string? _timestamp;
        public string Timestamp {
            get => _timestamp ??= StorageProvider.GetSetting<string>(TimestampKey);
            private set {
                StorageProvider.AddOrUpdateSetting(TimestampKey, value);

                _timestamp = value;
            }
        }

        private SyncStatus _status;

        public SyncStatus Status {
            get => _status;
            private set {
                if (_status != value) {
                    _status = value;

                    Trace("Status changed to {0}", value);

                    try {
                        StatusChanged?.Invoke(this, new SyncStatusChangedEventArgs(_status, value));
                    }
                    catch (Exception ex) {
                        Trace("Caught exception whilst triggering StatusChanged event:\n" + ex);
                    }
                }
            }
        }

        /// <summary>
        ///		True when <see cref="TrySync"/>
        ///		is called during a sync - a secondary
        ///		sync should occur after the current
        ///		is complete.
        /// </summary>
        private volatile bool _syncQueued = false;

        /// <summary>
        ///     Counter to prevent indefinite queueing.
        /// </summary>
        private int _queueLoopCount = 0;

        /// <summary>
        ///     The maximum amount of times we continuously
        ///     sync based on the _syncQueued flag.
        /// </summary>
        private const int QueueLoopThreshold = 3;

        /// <summary>
        ///     The number of times the service has
        ///     been consecutively throttled.
        ///
        ///     Reset after a successful sync.
        /// </summary>
        private int _throttledCount = 0;

        /// <summary>
        ///     The maximum amount of times we will attempt
        ///     to retry the request when receiving a "throttled"
        ///     before giving up.
        /// </summary>
        private const int ThrottleThreshold = 3;

        #endregion

        #region Events

        /// <summary>
        ///     Occurs when the sync status changes.
        /// </summary>
        public event EventHandler<SyncStatusChangedEventArgs> StatusChanged;

        /// <summary>
        ///     Occurs when a sync is complete.
        /// </summary>
        public event EventHandler<SyncCompletedEventArgs> Completed;

        /// <summary>
        ///     Occurs when an exception is thrown during
        ///     a sync.
        /// </summary>
        public event EventHandler<SyncErrorEventArgs> Error;

        #endregion

        #region Data Stores

        private IDataStore _dataStore;
        private ISingletonDataStore _singletonDataStore;

        public IDataStore DataStore => _dataStore ?? (_dataStore = Mvx.IoCProvider.Resolve<IDataStore>());

        public ISingletonDataStore SingletonDataStore => _singletonDataStore ?? (_singletonDataStore = Mvx.IoCProvider.Resolve<ISingletonDataStore>());

        #endregion

        #region ctor(...)

        public SyncService(
            IStorageProvider storageProvider,
            INetworkProvider networkProvider,
            IHttpApiClient apiClient,
            IUserStore userStore
        ) {
            StorageProvider = storageProvider;
            NetworkProvider = networkProvider;
            ApiClient = apiClient;
            _userStore = userStore;
        }

        #endregion

        #region Sync Methods

        /// <summary>
        ///     Attempts to persist modified items to the
        ///     server whilst retrieving new or modified items
        ///     from the server.
        /// </summary>
        /// <param name="kind">
        ///     How the sync is being triggered. Used to determine
        ///     whether a second sync should happen if the current
        ///     status is <see cref="SyncStatus.Syncing"/>.
        /// </param>
        public void TrySync(SyncTriggerKind kind) {
            if (!NetworkProvider.GetCanUseConnectionAsync().Result) {
                Status = SyncStatus.Offline;

                // Only show an error if the sync was triggered manually.
                if (kind == SyncTriggerKind.Manual) {
                    Error?.Invoke(this, new SyncErrorEventArgs(true));
                }

                return;
            }

            if (Status == SyncStatus.Syncing) {

                if (kind == SyncTriggerKind.DataChange) {
                    _syncQueued = true;

                    Trace("TrySync with kind={0} called but currently syncing, queued sync.", kind);
                }
                else {
                    Trace("TrySync with kind={0} called but currently syncing, ignored.", kind);
                }

                return;
            }

            Trace("TrySync called, initiating sync.");

            // Assume we will be able to sync before invoking sync,
            // this prevents it being triggered more than once during
            // the delay of starting a new thread.
            Status = SyncStatus.Syncing;

            AsyncTask.Factory.StartNew(InvokeSyncAsync);
        }

        public void Reset() {
            LastUpdated = null;
            Timestamp = null;
        }

        #endregion

        #region Private:InvokeSyncAsync

        private async void InvokeSyncAsync() {
            if (!NetworkProvider.IsOnline) {
                Status = SyncStatus.Offline;

                return;
            }

            Status = SyncStatus.Syncing;
            _syncQueued = false;

            if (AccessToken.GetCurrent(StorageProvider) == null) {
                Status = SyncStatus.Idle;

                return;
            }

            try {
                var toSync = await GetDataToSyncAsync();
#if DEBUG
                var tb = new StringBuilder();

                tb.AppendFormat("Syncing with timestamp '{0}'\n", Timestamp);
                tb.AppendFormat("\t {0} years/terms\n", toSync.AcademicYears.Count());
                tb.AppendFormat("\t {0} subjects\n", toSync.Subjects.Count());
                tb.AppendFormat("\t {0} classes\n", toSync.Classes.Count());
                tb.AppendFormat("\t {0} tasks\n", toSync.Tasks.Count());
                tb.AppendFormat("\t {0} exams\n", toSync.Exams.Count());

                if (toSync.User != null) {
                    tb.AppendLine("\t user settings");
                }
                if (toSync.DeviceSettings != null) {
                    tb.AppendLine("\t device settings");
                }

                Trace(tb.ToString());
#endif
                var result = await ApiClient.PostJsonAsync("/sync", JsonConvert.SerializeObject(new SyncRequestEnvelope {
                    Timestamp = Timestamp,
                    Data = toSync
                }));

                var response = JsonConvertEx.DeserializeObject<SyncResponseEnvelope>(result.Content);
                await ProcessResponse(response, toSync);

                //To Fix Reminder notification trigger issue 
                if (response.Data.HasAnyUserData || toSync.HasAnyUserData) {
                    await Mvx.IoCProvider.Resolve<IReminderService>().RunAsync();
                }

                // Success! Reset the throttled count...
                _throttledCount = 0;

                // Prevent indefinite syncing
                if (_queueLoopCount == QueueLoopThreshold) {
                    _syncQueued = false;
                }

                // Changed here to prevent it overriding the status set in handle error...
                if (!_syncQueued) {
                    _queueLoopCount = 0;
                    Status = SyncStatus.Idle;
                }
            }
            catch (Exception ex) {
                await HandleErrorAsync(ex);
            }

            Trace("Sync complete", _syncQueued ? ", another sync queued" : ", no additional sync queued.");

            if (_syncQueued) {
                _queueLoopCount++;
                InvokeSyncAsync();
            }
        }

        #endregion

        #region Private:GetDataToSyncAsync

        private async Task<SyncData> GetDataToSyncAsync() {
            Trace("Retrieving data to sync");

            var ds = DataStore;

            IEnumerable<AcademicYear> academicYears;
            IEnumerable<Holiday> holidays;
            IEnumerable<Subject> subjects;
            IEnumerable<Class> classes;
            IEnumerable<Task> tasks;
            IEnumerable<Exam> exams;

            var user = _userStore.GetCurrentUser();

            if (user == null) {
                return new SyncData();
            }

            var conn = ds.GetConnection();
            // TODO(maybe) Replace _needSyncing and Created/Updated/Deleted checks with a single State column

            academicYears = await GetRequiresSyncQuery<AcademicYear>(conn, user, LastUpdated);
            subjects = await GetRequiresSyncQuery<Subject>(conn, user, LastUpdated);
            classes = await GetRequiresSyncQuery<Class>(conn, user, LastUpdated);
            tasks = await GetRequiresSyncQuery<Task>(conn, user, LastUpdated);
            exams = await GetRequiresSyncQuery<Exam>(conn, user, LastUpdated);

            await conn.SetChildrenAsync(academicYears, CancellationToken.None, "Terms");
            await conn.SetChildrenAsync(classes, CancellationToken.None, "Times");

            #region Holidays

            const string UserIdSql = "( [ay].[UserId] IS NULL OR [ay].[UserId] = 0 OR [ay].[UserId] = ?1 )";

            var sb = new StringBuilder("SELECT [h].* FROM [Holidays] AS [h] INNER JOIN [AcademicYears] AS [ay] ON [h].[YearGuid] = [ay].[Guid] WHERE ");

            var args = new List<object> {
                user.Id
            };

            if (user.IsTeacher) {
                sb.Append("( ");
                sb.Append(UserIdSql);
                sb.Append(" OR [ay].[SchoolId] = ?2");
                sb.Append(" )");

                args.Add(user.School.Id);
            }
            else {
                sb.Append(UserIdSql);
            }

            if (LastUpdated.HasValue) {
                sb.AppendFormat(
                    @" AND (
                        [h].[{0}] = 1 OR [h].[Timestamp] IS NULL OR (
                            [h].[CreatedAt] >= ?{1} OR
                            ([h].[UpdatedAt] IS NOT NULL AND [h].[UpdatedAt] >= ?{1}) OR
                            ([h].[DeletedAt] IS NOT NULL AND [h].[DeletedAt] >= ?{1})
                        )
                    )",
                    SyncFlagProp,
                    args.Count + 1
                );

                args.Add(LastUpdated);
            }

            holidays = await conn.QueryAsync<Holiday>(sb.ToString(), args.ToArray());

            #endregion

            var timetableSettings = await TimetableSettings.Get(SingletonDataStore, false);
            var deviceSettings = await SingletonDataRepository<DeviceSettings>.GetInstance(StorageProvider).GetStoredAsync();

            Trace("Retrieved data to sync");

            var data = new SyncData {
                AcademicYears = academicYears,
                Holidays = holidays,
                Subjects = subjects,
                Classes = classes,
                Tasks = tasks,
                Exams = exams,
                User = user.Settings != null && user.Settings.RequiresSync(LastUpdated) ? user : null,
                DeviceSettings = deviceSettings != null && deviceSettings.RequiresSync(LastUpdated) ? deviceSettings : null
            };

#pragma warning disable 612
            if (!user.IsStudentWithUnmanagedSchool && timetableSettings != null) {
                data.TimetableSettings = timetableSettings.RequiresSync(LastUpdated) ? timetableSettings : null;
            }
#pragma warning restore 612

            return data;
        }

        #endregion

        #region Private:ProcessResponse

        /// <summary>
        ///     Processes the response from the API.
        /// </summary>
        private async AsyncTask ProcessResponse(SyncResponseEnvelope response, SyncData requestData) {
            Trace("Received response");

            if (response?.Status != "OK") {
                HandleError("Response was null or status was not equal to 'OK'");
                return;
            }

            try {
                var conn = DataStore.GetConnection().GetConnection();

                // If we didn't send any data and didn't receive any then there's
                // no point opening an empty connection!
                if (requestData.HasAnyUserData || response.Data.HasAnyUserData) {
                    var academicYearsProcessor = new AcademicYearSyncResponseProcessor(this, conn, response.Data.AcademicYears, response.Timestamp);
                    var holidaysProcessor = new SyncResponseProcessor<Holiday>(this, conn, response.Data.Holidays, response.Timestamp);
                    var subjectsProcessor = new SyncResponseProcessor<Subject>(this, conn, response.Data.Subjects, response.Timestamp);
                    var classesProcessor = new ClassSyncResponseProcessor(this, conn, response.Data.Classes, response.Timestamp);
                    var tasksProcessor = new SyncResponseProcessor<Task>(this, conn, response.Data.Tasks, response.Timestamp);
                    var examsProcessor = new SyncResponseProcessor<Exam>(this, conn, response.Data.Exams, response.Timestamp);

                    using (conn.Lock()) {
                        try {
                            conn.BeginTransaction();

                            conn.Execute("PRAGMA defer_foreign_keys = ON;");

                            // The order of the stuff below is V. important
                            academicYearsProcessor.PerformAddAndUpdate();
                            holidaysProcessor.PerformAddAndUpdate();
                            subjectsProcessor.PerformAddAndUpdate();
                            classesProcessor.PerformAddAndUpdate();
                            examsProcessor.PerformAddAndUpdate();
                            tasksProcessor.PerformAddAndUpdate();

                            tasksProcessor.PerformDelete();
                            examsProcessor.PerformDelete();
                            classesProcessor.PerformDelete();
                            subjectsProcessor.PerformDelete();
                            holidaysProcessor.PerformDelete();
                            academicYearsProcessor.PerformDelete();

                            if (requestData.HasAnyUserData) {
                                tasksProcessor.PurgeDeleted(requestData.Tasks);
                                examsProcessor.PurgeDeleted(requestData.Exams);
                                classesProcessor.PurgeDeleted(requestData.Classes);
                                subjectsProcessor.PurgeDeleted(requestData.Subjects);
                                holidaysProcessor.PurgeDeleted(requestData.Holidays);
                                academicYearsProcessor.PurgeDeleted(requestData.AcademicYears);

                                tasksProcessor.MarkAsSynced(requestData.Tasks);
                                examsProcessor.MarkAsSynced(requestData.Exams);
                                classesProcessor.MarkAsSynced(requestData.Classes);
                                subjectsProcessor.MarkAsSynced(requestData.Subjects);
                                holidaysProcessor.MarkAsSynced(requestData.Holidays);
                                academicYearsProcessor.MarkAsSynced(requestData.AcademicYears);
                            }

                            conn.Commit();
                        }
                        catch (Exception ex) {
                            conn.Rollback();

                            // This helps to diagnose foreign key constraint issues. Different platforms give different error messages so test for both messages regardless of case.
                            if (!ex.Message.Contains("foreign key", StringComparison.OrdinalIgnoreCase) && !ex.Message.Contains("constraint", StringComparison.OrdinalIgnoreCase)) {
                                throw;
                            }

                            var newEx = new SyncException("Error occurred whilst processing the response", ex);

                            newEx.Data["Request_Timestamp"] = Timestamp;

                            var holidaysWithoutYearInResponse = response.Data.Holidays.Where(x => response.Data.AcademicYears.All(y => y.Guid != x.YearGuid)).ToList();
                            var subjectsWithoutScheduleInResponse = response.Data.Subjects.Count(x => x.ScheduleGuid.HasValue && response.Data.AcademicYears.All(y => y.Guid != x.ScheduleGuid));
                            var classesWithoutScheduleInResponse = response.Data.Classes.Count(x => x.ScheduleGuid.HasValue && response.Data.AcademicYears.All(y => y.Guid != x.ScheduleGuid));
                            var classesWithoutSubjectInResponse = response.Data.Classes.Count(x => response.Data.Subjects.All(y => y.Guid != x.SubjectGuid));
                            var tasksWithoutSubjectInResponse = response.Data.Tasks.Count(x => response.Data.Subjects.All(y => y.Guid != x.SubjectGuid));
                            var tasksWithoutExamInResponse = response.Data.Tasks.Count(x => x.ExamGuid.HasValue && response.Data.Exams.All(y => y.Guid != x.ExamGuid.Value));
                            var examsWithoutSubjectInResponse = response.Data.Exams.Count(x => response.Data.Subjects.All(y => y.Guid != x.SubjectGuid));

                            // Stuff missing in the error probably isn't the root cause of the problem as the sync is differential
                            // but this should help us diagnose the issue.
                            newEx.Data["Response_MissingYears"] = $"{holidaysWithoutYearInResponse.Count()} holidays";
                            newEx.Data["Response_MissingSchedules"] =
                                $"{subjectsWithoutScheduleInResponse} subjects, {classesWithoutScheduleInResponse} classes";
                            newEx.Data["Response_MissingSubjects"] =
                                $"{classesWithoutSubjectInResponse} classes, {tasksWithoutSubjectInResponse} tasks, {examsWithoutSubjectInResponse} exams";
                            newEx.Data["Response_MissingExams"] = $"{tasksWithoutExamInResponse} tasks";

                            newEx.Data["ProcessorTrace_AcademicYears"] = academicYearsProcessor.GetTrace();
                            newEx.Data["ProcessorTrace_Holidays"] = holidaysProcessor.GetTrace();
                            newEx.Data["ProcessorTrace_Subjects"] = subjectsProcessor.GetTrace();
                            newEx.Data["ProcessorTrace_Classes"] = classesProcessor.GetTrace();
                            newEx.Data["ProcessorTrace_Tasks"] = tasksProcessor.GetTrace();
                            newEx.Data["ProcessorTrace_Exams"] = examsProcessor.GetTrace();

                            // Diagnostics for MSL-241, below error occurs when *adding* an academic year from the server
                            if (ex.Message.Contains("UNIQUE constraint failed: AcademicTerms.Guid", StringComparison.OrdinalIgnoreCase)) {
                                try {
                                    // Guids of years in the local db
                                    var dbYearGuids = conn.Table<AcademicYear>().Select(x => x.Guid).ToList();
                                    var dbTerms = conn.Table<AcademicTerm>().ToList();

                                    // Years returned by the server which do not yet exist in the local db (transaction will be rolled back by now)
                                    // and whose terms do exist in the database
                                    var newYearsWithDupeTerms = response.Data.AcademicYears
                                        .Where(x =>
                                            !dbYearGuids.Contains(x.Guid) &&
                                            x.Terms.Any(t => dbTerms.Any(t2 => t2.Guid == t.Guid))
                                        )
                                        .ToList();

                                    if (newYearsWithDupeTerms.Any()) {
                                        // New years with a term that already exists in the local db
                                        foreach (var year in newYearsWithDupeTerms) {
                                            var dbDupeTerms = dbTerms.Where(x => year.Terms.Any(t => t.Guid == x.Guid));

                                            // This will help us diagnose if it's a transaction rollback/race conditon issue or something
                                            // else entirely
                                            var termsWithSameYearGuid = dbDupeTerms.Where(x => x.YearGuid == year.Guid).Select(x => x.Guid.ToString("N"));
                                            var termsWithDifferentYearGuid = dbDupeTerms.Where(x => x.YearGuid != year.Guid).Select(x => x.Guid.ToString("N"));

                                            newEx.Data[$"MSL-241 {year.Guid.ToString("N")}"] =
                                                $"{termsWithSameYearGuid.Count()} terms with same year ({String.Join(",", termsWithSameYearGuid)}), " +
                                                $"{termsWithDifferentYearGuid.Count()} terms with different year ({String.Join(",", termsWithDifferentYearGuid)})";
                                        }
                                    }
                                    else {
                                        newEx.Data["MSL-241"] = "0 new years with term guids in local db";
                                    }
                                }
                                catch (Exception ex241) {
                                    newEx.Data["MSL-241"] = "Error: " + ex241.ToLongString();
                                }
                            }

                            throw newEx;
                        }
                    }

                    if (academicYearsProcessor.NeedsResync ||
                        holidaysProcessor.NeedsResync ||
                        subjectsProcessor.NeedsResync ||
                        classesProcessor.NeedsResync ||
                        tasksProcessor.NeedsResync ||
                        examsProcessor.NeedsResync) {

                        Trace("One or more items contained in the sync request have been updated since the sync started, queuing sync.");

                        _syncQueued = true;
                    }
                }

                var completedEventArgs = new SyncCompletedEventArgs();

                // Don't need to set needs syncing = false as the server will always
                // return the timetable settings again with a new timestamp
#pragma warning disable 612
                if (response.Data.TimetableSettings != null) {
                    await SingletonDataStore.SetSingleton(response.Data.TimetableSettings);

                    if (ShouldBroadcast(requestData.TimetableSettings, response.Data.TimetableSettings)) {
                        completedEventArgs.TimetableSettings = response.Data.TimetableSettings;
                    }
                }
#pragma warning restore 612

                var user = _userStore.GetCurrentUser();

                if (response.Data.User != null) {
                    // #MSL-103 - we're handling a scenario here where User.GetCurrentAsync might return null
                    var settingsBeforeSync = requestData.User?.Settings ?? user?.Settings;

                    await _userStore.PersistUserAsync(response.Data.User);

                    if (requestData.User == null || ShouldBroadcast(settingsBeforeSync, response.Data.User.Settings)) {
                        completedEventArgs.UserSettings = response.Data.User.Settings;
                    }
                }
                else {
                    if (user?.Settings != null) {
                        user.Settings.NeedsSyncing = false;

                        await _userStore.PersistUserAsync(user);
                    }
                    else {
                        Debug.Assert(false, "User in storage is null with no user returned from sync request.");
                    }
                }

                var deviceSettingsRepo = SingletonDataRepository<DeviceSettings>.GetInstance(StorageProvider);

                if (response.Data.DeviceSettings != null) {
                    await deviceSettingsRepo.PersistAsync(response.Data.DeviceSettings);

                    if (ShouldBroadcast(requestData.DeviceSettings, response.Data.DeviceSettings)) {
                        completedEventArgs.DeviceSettings = response.Data.DeviceSettings;
                    }
                }
                else {
                    var deviceSettings = await deviceSettingsRepo.GetStoredOrDefaultAsync(DeviceSettings.Default);

                    deviceSettings.NeedsSyncing = false;

                    await deviceSettingsRepo.PersistAsync(deviceSettings);
                }

                // This needs to be the device's time to best sync the other entries
                LastUpdated = DateTime.UtcNow;
                Timestamp = response.Timestamp;

                var completedHandler = Completed;

                if (completedHandler == null) {
                    return;
                }

                // As of API V5 entities are only returned from the server when they have been
                // added/updated before this sync request (ie. the request entities aren't returned
                // unless the server had a more recently updated copy).
                var years = response.Data.AcademicYears.ToList();
                var holidays = response.Data.Holidays.ToList();
                var subjects = response.Data.Subjects.ToList();
                var classes = response.Data.Classes.ToList();
                var tasks = response.Data.Tasks.ToList();
                var exams = response.Data.Exams.ToList();

                using (conn.Lock()) {
                    // Without this we are sending out entities that may not have subjects assigned.
                    //
                    // TODO: In some fringe cases, we will be sending out modified classes, tasks
                    // and exams with null subjects. This occurs where a subject has been deleted
                    // causing any classes, tasks or exams which belong to it to also be deleted.
                    conn.SetChildren(years);
                    conn.SetChildren(holidays);
                    conn.SetChildren(subjects);
                    conn.SetChildren(classes);
                    conn.SetChildren(tasks);
                    conn.SetChildren(exams);
                }

                completedEventArgs.User = response.Data.User;
                completedEventArgs.ModifiedAcademicYears = years;
                completedEventArgs.ModifiedHolidays = holidays;
                completedEventArgs.ModifiedSubjects = subjects;
                completedEventArgs.ModifiedClasses = classes;
                completedEventArgs.ModifiedTasks = tasks;
                completedEventArgs.ModifiedExams = exams;

                completedHandler(this, completedEventArgs);
            }
            // We're catching and rethrow in an attempt to get the line numbers on all platforms
            catch (Exception ex) when (!(ex is SyncException)) {
                throw new SyncException("Error while processing the response.", ex);
            }
        }

        #endregion

        #region Private:HandleError

        private async AsyncTask HandleErrorAsync(Exception ex) {
            if (!await NetworkProvider.GetCanUseConnectionAsync()) {
                Status = SyncStatus.Offline;

                Error?.Invoke(this, new SyncErrorEventArgs(ex, true));

                // Don't log any exceptions if we're offline... 99% change that they
                // are web related. It's also difficult to determine whether it's a
                // WebException because MonoDroid/MonoTouch will throw non standard
                // exceptions (such as java.net.UnknownHostException)
                return;
            }

            if (!NetworkProvider.IsOnline) {
                Status = SyncStatus.Offline;

                return;
            }

            // Don't send transient exceptions to the bug reporter.

            // TOOD: Handle naughty hotspot exceptions
            // TODO: Retry on transient exception
            if (!ex.IsTransientWebOrNaughtyHotspotException()) {
                Trace("Caught exception during sync: " + ex.ToLongString());
                Mvx.IoCProvider.Resolve<Diagnostics.IBugReporter>().Send(ex, new List<string> { "SyncService" });
            }
            else if (ex is TaskCanceledException || (ex is HttpApiException && (int)((HttpApiException)ex).StatusCode.GetValueOrDefault() == 429)) {
                _throttledCount++;

                // iOS throw a TaskCanceledException for the first request immediately after sign in on iOS.
                // Handle this and invoke another sync right away, if this occurs more than once, handle as
                // standard 429 backoff.
                // https://github.com/xamarin/xamarin-macios/issues/6443
                if (ex is TaskCanceledException && _throttledCount == 1) {
                    InvokeSyncAsync();

                    return;
                }

                if (_throttledCount <= ThrottleThreshold) {
                    double waitPeriod = _throttledCount * 3;

                    Debug.WriteLine("SyncService: Throttled, backing off for {0} seconds.", waitPeriod);

                    _syncQueued = false;

                    await AsyncTask.Delay(TimeSpan.FromSeconds(waitPeriod));

                    Debug.WriteLine("SyncService: Retrying after receiving throttled response.");

                    InvokeSyncAsync();

                    return;
                }

                Debug.WriteLine("SyncService: Still throttled after {0} attempts, giving up.", ThrottleThreshold);
            }


            Status = SyncStatus.Idle;

            Error?.Invoke(this, new SyncErrorEventArgs(ex));
        }

        private void HandleError(string message) {
            Error?.Invoke(this, new SyncErrorEventArgs(message));
        }

        #endregion

        #region Utils

        private async Task<List<T>> GetRequiresSyncQuery<T>(SQLiteAsyncConnection conn, User user, DateTime? lastUpdated)
            where T : ISyncable, new() {

            const string UserIdSql = "( [UserId] IS NULL OR [UserId] = 0 OR [UserId] = ?1 )";

            var sb = new StringBuilder();

            sb.AppendFormat("SELECT * FROM [{0}] WHERE ", typeof(T).GetTableName());

            var args = new List<object> {
                user.Id
            };

            if (user.IsTeacher) {
                sb.Append("( ");
                sb.Append(UserIdSql);

                if (typeof(T).GetRuntimeProperty("SchoolId") != null) {
                    sb.Append(" OR [SchoolId] = ?2");

                    args.Add(user.School.Id);
                }

                sb.Append(" )");
            }
            else {
                sb.Append(UserIdSql);
            }

            if (lastUpdated.HasValue) {
                sb.AppendFormat(
                    @" AND (
                        [{0}] = 1 OR [Timestamp] IS NULL OR (
                            [CreatedAt] >= ?{1} OR
                            ([UpdatedAt] IS NOT NULL AND [UpdatedAt] >= ?{1}) OR
                            ([DeletedAt] IS NOT NULL AND [DeletedAt] >= ?{1})
                        )
                    )",
                    SyncFlagProp,
                    args.Count + 1
                );

                args.Add(LastUpdated);
            }

            return await conn.QueryAsync<T>(sb.ToString(), args.ToArray());
        }

        private bool ShouldBroadcast<T>(T client, T server) where T : class, ISimpleSyncable {
            if (client == null && server != null) {
                return true;
            }

            if (server == null) {
                return false;
            }

            return server.UpdatedAt > client.UpdatedAt;
        }

        [Conditional("DEBUG")]
        internal void Trace(string message, params object[] args) {
            Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(SyncService)).LogTrace(message, args);
        }

        #endregion
    }
}