﻿namespace MyStudyLife.Sync {
    public enum SyncStatus {
        Idle,
        Syncing,
        Throttled,
        Offline,
        Disabled
    }
}
