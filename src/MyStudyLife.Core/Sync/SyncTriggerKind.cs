﻿namespace MyStudyLife.Sync {
    public enum SyncTriggerKind {
        /// <summary>
        ///     The sync has been triggered by part of
        ///     the application during startup or network
        ///     connectivity change.
        /// </summary>
        Programmatic,

        /// <summary>
        ///     The user has manually triggered a sync.
        /// </summary>
        Manual,

        /// <summary>
        ///     User has modified data on the device.
        /// </summary>
        DataChange
    }
}
