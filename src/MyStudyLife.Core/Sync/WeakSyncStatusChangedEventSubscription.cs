﻿using System;
using System.Reflection;
using MvvmCross.WeakSubscription;

namespace MyStudyLife.Sync {
    public sealed class WeakSyncStatusChangedEventSubscription : MvxWeakEventSubscription<ISyncService, SyncStatusChangedEventArgs> {
        private static readonly EventInfo SyncServiceEventInfo = typeof(ISyncService).GetRuntimeEvent(nameof(ISyncService.StatusChanged));

        public WeakSyncStatusChangedEventSubscription(ISyncService source, EventHandler<SyncStatusChangedEventArgs> targetEventHandler)
            : base(source, SyncServiceEventInfo, targetEventHandler) { }

        protected override Delegate CreateEventHandler() {
            return new EventHandler<SyncStatusChangedEventArgs>(OnSourceEvent);
        }
    }
}
