﻿using Newtonsoft.Json;

namespace MyStudyLife.Sync {
    [JsonObject]
    public class SyncRequestEnvelope {
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("data")]
        public SyncData Data { get; set; }
    }
}
