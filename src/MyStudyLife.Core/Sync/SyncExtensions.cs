﻿using System;

namespace MyStudyLife.Sync {
    public static class SyncExtensions {
        public static void Syncify(this ISyncable syncable) {
            syncable.NeedsSyncing = true;
        }

        public static void Syncify(this ISimpleSyncable syncable) {
            syncable.NeedsSyncing = true;
        }

        public static bool RequiresSync(this ISyncable syncable, DateTime? lastUpdated) {
            return (
                syncable.NeedsSyncing ||
                syncable.CreatedAt >= lastUpdated || syncable.UpdatedAt >= lastUpdated || syncable.DeletedAt >= lastUpdated
            );
        }

        public static bool RequiresSync(this ISimpleSyncable syncable, DateTime? lastUpdated) {
            return syncable.NeedsSyncing || syncable.UpdatedAt >= lastUpdated;
        }

        public static WeakSyncStatusChangedEventSubscription WeakSubscribe(this ISyncService syncService, EventHandler<SyncStatusChangedEventArgs> handler) {
            return new WeakSyncStatusChangedEventSubscription(syncService, handler);
        }
    }
}
