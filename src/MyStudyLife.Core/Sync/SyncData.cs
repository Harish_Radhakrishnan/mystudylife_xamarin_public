﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using Newtonsoft.Json;

namespace MyStudyLife.Sync {
    [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public sealed class SyncData {
        private IEnumerable<AcademicYear> _academicYears;
        private IEnumerable<Holiday> _holidays;
        private IEnumerable<Subject> _subjects;
        private IEnumerable<Class> _classes;
        private IEnumerable<Task> _tasks;
        private IEnumerable<Exam> _exams;

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("timetable_settings"), Obsolete]
        public TimetableSettings TimetableSettings { get; set; }

        [JsonProperty("academic_years")]
        public IEnumerable<AcademicYear> AcademicYears {
            get { return _academicYears ?? (_academicYears = Enumerable.Empty<AcademicYear>()); }
            set { _academicYears = value; }
        }

        [JsonProperty("holidays")]
        public IEnumerable<Holiday> Holidays {
            get { return _holidays ?? (_holidays = Enumerable.Empty<Holiday>()); }
            set { _holidays = value; }
        }

        [JsonProperty("subjects")]
        public IEnumerable<Subject> Subjects {
            get { return _subjects ?? (_subjects = Enumerable.Empty<Subject>()); }
            set { _subjects = value; }
        }

        [JsonProperty("classes")]
        public IEnumerable<Class> Classes {
            get { return _classes ?? (_classes = Enumerable.Empty<Class>()); }
            set { _classes = value; }
        }

        [JsonProperty("tasks")]
        public IEnumerable<Task> Tasks {
            get { return _tasks ?? (_tasks = Enumerable.Empty<Task>()); }
            set { _tasks = value; }
        }

        [JsonProperty("exams")]
        public IEnumerable<Exam> Exams {
            get { return _exams ?? (_exams = Enumerable.Empty<Exam>()); }
            set { _exams = value; }
        }
        /// <summary>
        ///     Settings specific to the device but also used on
        ///     the server. An example being reminder settings.
        /// 
        ///     These settings can be updated on the device or server.
        /// </summary>
        [JsonProperty("device_settings")]
        public DeviceSettings DeviceSettings { get; set; }

        /// <value>
        ///     True if any of the user's data (collections, excludes settings)
        ///     has changed.
        /// </value>
        public bool HasAnyUserData {
            get {
                return (
                    AcademicYears.Any() ||
                    Holidays.Any() ||
                    Subjects.Any() ||
                    Classes.Any() ||
                    Tasks.Any() ||
                    Exams.Any()
                );
            }
        }
    }
}
