﻿using System;
using Newtonsoft.Json;
using PropertyChanged;

namespace MyStudyLife.Sync {
    public interface ISimpleSyncable {
        [DoNotNotify]
        [JsonProperty(SyncService.SyncFlagProp)]
        bool NeedsSyncing { get; set; }

        [JsonProperty("updated")]
        DateTime? UpdatedAt { get; }
    }
}
