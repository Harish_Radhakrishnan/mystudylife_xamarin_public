﻿using System;
using MyStudyLife.Net;

namespace MyStudyLife.Sync {
    public sealed class SyncErrorEventArgs : EventArgs {
        public Exception Exception { get; private set; }

        public string Message { get; private set; }

        public bool IsOffline { get; private set; }

        public SyncErrorEventArgs(bool isOffline) {
            IsOffline = isOffline;

            if (isOffline) {
                Message = "No internet connection detected.";
            }
        }

        public SyncErrorEventArgs(Exception ex, bool isOffline = false) : this(isOffline) {
            if (ex == null)
                throw new ArgumentNullException(nameof(ex));
             
            Exception = ex;

            if (ex.Message.Contains("Unable to connect") ||
                (ex.InnerException != null && ex.InnerException.Message.Contains("Unable to connect"))) {

                Message = "Unable to connect to the server";
            }
            else {
                if (ex is HttpApiException || ex is System.Net.Http.HttpRequestException) {
                    Message = "Server error";
                }
                else {
                    Message = "Local error";
                }
            }
        }

        public SyncErrorEventArgs(string message, bool isOffline = false) : this(isOffline) {
            if (String.IsNullOrWhiteSpace(message))
                throw new ArgumentException("Must not be null, empty or white space.", nameof(message));

            Message = message;
        }
    }
}
