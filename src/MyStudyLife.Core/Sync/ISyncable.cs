﻿using System;

namespace MyStudyLife.Sync {
    public interface ISyncable {
        bool NeedsSyncing { get; set; }
        
        DateTime CreatedAt { get; }

        DateTime? UpdatedAt { get; }

        DateTime? DeletedAt { get; }
    }
}
