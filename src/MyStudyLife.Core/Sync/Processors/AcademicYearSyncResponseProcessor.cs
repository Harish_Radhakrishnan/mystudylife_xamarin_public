using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;
using SQLite;

namespace MyStudyLife.Sync.Processors {
    internal class AcademicYearSyncResponseProcessor : SyncResponseProcessor<AcademicYear> {
        public AcademicYearSyncResponseProcessor(
            SyncService service,
            SQLiteConnection conn,
            IEnumerable<AcademicYear> itemsFromServer,
            string timestamp
        ) : base(
            service,
            conn,
            itemsFromServer,
            timestamp
        ) {}


        protected override void OnAdded(IEnumerable<AcademicYear> items) {
            var termsToAdd = items.SelectMany(x => {
                x.Terms.Apply(y => y.YearGuid = x.Guid);

                return x.Terms;
            });

            // Insert() instead of InsertAll() so no transaction
            foreach (var t in termsToAdd) {
                Conn.Insert(t, typeof(AcademicTerm));
            }
        }

        protected override void OnUpdated(IEnumerable<AcademicYear> items) {
            items = items as ICollection<AcademicYear> ?? items.ToList();

            var terms = items.SelectMany(x => {
                x.Terms.Apply(y => y.YearGuid = x.Guid);

                return x.Terms;
            }).ToList();

            var itemGuids = items.Select(x => x.Guid).ToList();
            var termGuids = terms.Select(x => x.Guid).ToArray();

            foreach (var t in terms) {
                Conn.InsertOrReplace(t, typeof(AcademicTerm));
            }

            var termDeleteParams = new List<object>(itemGuids.Select(x => (object)x.ToString()));
            termDeleteParams.AddRange(termGuids.Select(x => (object)x.ToString()).ToArray());

            if (itemGuids.Any()) {
                Conn.Execute(
                    String.Format(
                        "DELETE FROM [AcademicTerms] WHERE [YearGuid] IN ({0}) AND [Guid] NOT IN ({1})",
                        String.Join(",", itemGuids.Select(x => "?")),
                        String.Join(",", termGuids.Select(x => "?"))
                    ),
                    termDeleteParams.ToArray()
                );
            }
        }
    }
}