using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;
using SQLite;

namespace MyStudyLife.Sync.Processors {
    internal sealed class ClassSyncResponseProcessor : SyncResponseProcessor<Class> {
        public ClassSyncResponseProcessor(SyncService service, SQLiteConnection conn, IEnumerable<Class> itemsFromServer, string timestamp)
            : base(service, conn, itemsFromServer, timestamp) { }

	    protected override void OnAdded(IEnumerable<Class> items) {
	        var toAdd = items.Where(x => x.Times != null).SelectMany(x => {
	            x.Times.Apply(y => y.ClassGuid = x.Guid);

	            return x.Times;
	        });

            // Insert() instead of InsertAll() so no transaction
	        foreach (var time in toAdd) {
                Conn.Insert(time, typeof(ClassTime));
	        }
	    }

        protected override void OnUpdated(IEnumerable<Class> items) {
            items = items as ICollection<Class> ?? items.ToList();

            var times = items.Where(x => x.Times != null).SelectMany(x => {
                x.Times.Apply(y => y.ClassGuid = x.Guid);

                return x.Times;
            }).ToList();


            var itemGuids = items.Select(x => x.Guid).ToList();
            var timeGuids = times.Select(x => x.Guid).ToArray();

            foreach (var time in times) {
                Conn.InsertOrReplace(time, typeof (ClassTime));
            }

            var deleteParams = new List<object>(itemGuids.Select(x => (object)x.ToString()));
            deleteParams.AddRange(timeGuids.Select(x => (object)x.ToString()).ToArray());

            if (itemGuids.Any()) {
                Conn.Execute(
                    String.Format(
                        "DELETE FROM [ClassTimes] WHERE [ClassGuid] IN ({0}) AND [Guid] NOT IN ({1})",
                        String.Join(",", itemGuids.Select(x => "?")),
                        String.Join(",", timeGuids.Select(x => "?"))
                    ),
                    deleteParams.ToArray()
                );
            }
        }
	}
}