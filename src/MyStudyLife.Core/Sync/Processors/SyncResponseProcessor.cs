using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MyStudyLife.Data;
using MyStudyLife.Extensions;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace MyStudyLife.Sync.Processors {
    internal class SyncResponseProcessor<T> where T : BaseEntity, ISyncable, new() {
        private readonly SyncService _syncService;

        private readonly SQLiteConnection _conn;
        private readonly IEnumerable<T> _items;
        private readonly string _timestamp;

        protected SQLiteConnection Conn => _conn;

        private readonly List<T> _toAddOrUpdate;
        private readonly List<T> _toIgnore;
        private readonly List<T> _toDelete;

// ReSharper disable MemberCanBePrivate.Global
        public int ItemsAdded { get; private set; }
        public int ItemsUpdated { get; private set; }
        public int ItemsDeleted { get; private set; }
        public int ItemsPurged { get; private set; }
// ReSharper restore MemberCanBePrivate.Global

        public bool NeedsResync { get; private set; }

        public SyncResponseProcessor(SyncService service, SQLiteConnection conn, IEnumerable<T> itemsFromServer, string timestamp) {
            _syncService = service;
            _conn = conn;
            _items = itemsFromServer;
            _timestamp = timestamp;

            _toAddOrUpdate = new List<T>();
            _toDelete = new List<T>();
            _toIgnore = new List<T>();

            // Server should never return multiple guids, just a quick fix for BETT
            foreach (var item in _items.DistinctBy(x => x.Guid)) {

                // Types deleted on the server return a guid and deleted flag.
                //
                // This may include something we've tried to add but the parent
                // entity (for example a task's subject) has been deleted
                // on the server.
                if (item.DeletedAt.HasValue) {
                    _toDelete.Add(item);
                }
                // Newly added types (either by this client or the server) return
                // a guid and revision.
                //
                // Types updated on the server (or updated on this client) return
                // a guid, updated flag and revision.
                //
                // As we don't know whether this has been added on the client or
                // the server we'll need to run further checks as to not cause local duplicates.
                else {
                    _toAddOrUpdate.Add(item);
                }
            }
        }

        public void PerformAddAndUpdate() {
            if (!_toAddOrUpdate.Any()) {
                return;
            }

            var toUpdate = new List<T>();

            // MSL-213 Fixes issue where when user syncs for the first time, this SQL statement
            // potentially generates more parameters than SQLite is configured to handle.
            //
            // TODO: Move queries to repos with a max limit, add batch handling the entire sync processor
            var dbItems = new List<T>();
            // 100 is an arbitrary number - actual max param count on Android is 999, but I don't think
            // using those 999 would be the most performant
            const int BATCH_SIZE = 100;
            List<Guid> currentBatch = new List<Guid>(BATCH_SIZE);

            for (int i = 0, count = _toAddOrUpdate.Count; i < count; i++) {
                var item = _toAddOrUpdate[i];

                currentBatch.Add(item.Guid);

                if (currentBatch.Count == BATCH_SIZE || (i == (count - 1) && currentBatch.Count > 0)) {
                    dbItems.AddRange(_conn.Table<T>().Where(x => currentBatch.Contains(x.Guid)));
                    currentBatch.Clear();
                }
            }

            var toAdd = _toAddOrUpdate.Where(x => dbItems.All(y => x.Guid != y.Guid)).ToList();

            foreach (var item in _toAddOrUpdate.Where(x => toAdd.All(y => x.Guid != y.Guid))) {
                // There may be a local item if it's been added
                // by the client and we're getting a revision back.
                var localItem = dbItems.Single(x => x.Guid == item.Guid);

                // It might just be the revision that's changed.

                // TODO(sometime) some sort of version ID that both the client and server can set

                // This fixes a time related bug where devices times can be out
                // of sync. If an item has been deleted on one device it should
                // be deleted on all.
                if (localItem.DeletedAt.HasValue && !item.DeletedAt.HasValue) {
                    item.DeletedAt = localItem.DeletedAt;
                    item.NeedsSyncing = true;

                    _toIgnore.Add(item);
                }
                else {
                    item.NeedsSyncing = false;
                }

                toUpdate.Add(item);
            }

            // Not using Insert/UpdateAll() as it wraps it in transaction
            // (which just loops with Insert/Update() like below anyway)

            if (toAdd.Any()) {
                foreach (var item in toAdd) {
                    _conn.Insert(item, typeof (T));

                    ItemsAdded++;
                }

                OnAdded(toAdd);
            }

            if (toUpdate.Any()) {
                foreach (var item in toUpdate) {
                    _conn.Update(item, typeof(T));

                    ItemsUpdated++;
                }

                OnUpdated(toUpdate);
            }
        }

        public void PerformDelete() {
            // Delete all items returned from the server
            // as deleted.
            foreach (var item in _toDelete) {
                _conn.Delete(item);

                ItemsDeleted++;
            }
        }

        public void PurgeDeleted(IEnumerable<T> requestItems) {
            // Delete all remaining entries which have been marked as deleted
            // but not returned from the response.
            //
            // (but only if we've just sent them to be deleted)
            var guids = requestItems.Select(x => x.Guid).Where(x => !_toIgnore.Any(y => y.Guid == x)).ToList();

            if (!guids.Any()) {
                return;
            }

            var toDelete = _conn.Table<T>()
                .Where(x => x.DeletedAt != null && x.NeedsSyncing && guids.Contains(x.Guid))
                .ToList();

            foreach (var item in toDelete) {
                _conn.Delete(item);

                ItemsPurged++;
            }
        }

        /// <summary>
        ///     Marks the items that were sent to the server (and
        ///     not returned) as synced.
        /// </summary>
        /// <param name="requestItems">
        ///     The items that were sent to the server.
        /// </param>
        public void MarkAsSynced(IEnumerable<T> requestItems) {
            // Don't bother clearing the flag for items which were deleted on the client but not the server
            // because that overrides the check above.
            //
            // We're also not clearing the flag for any items that were returned from the server as this
            // is handled by the add/update/delete methods (and would screw up the needs sync checking)
            var items = requestItems.Where(x => !_toIgnore.Contains(x) && _items.All(y => y.Guid != x.Guid)).ToList();

            if (!items.Any()) {
                return;
            }

            string tableName = typeof (T).GetTableName();

            // Set new timestamp on all items regardless of whether they have been updated on the client during sync
            // (otherwise the server will ignore the update)
            var timestampArgs = new object[items.Count + 1];
            timestampArgs[0] = _timestamp;

            for (int i = 0; i < items.Count; i++) {
                timestampArgs[i + 1] = items[i].Guid;
            }

            _conn.Execute(
                String.Format(
                    "UPDATE [{0}] SET [Timestamp] = ? WHERE [Guid] IN ({1})",
                    tableName,
                    String.Join(",", Enumerable.Repeat("?", items.Count))
                ),
                timestampArgs
            );

            // Only update items whose UpdatedAt property has not changed during the sync request
            var sqlBuilder = new StringBuilder();
            var args = new List<object>();

            sqlBuilder.AppendFormat("UPDATE [{0}] SET [{1}] = 0 WHERE ", typeof(T).GetTableName(), SyncService.SyncFlagProp);

            for (int i = 0; i < items.Count; i++) {
                var item = items[i];

                sqlBuilder.Append("( [Guid] = ? AND [UpdatedAt] ");
                args.Add(item.Guid);

                if (item.UpdatedAt.HasValue) {
                    sqlBuilder.Append("= ?");
                    args.Add(item.UpdatedAt);
                }
                else {
                    sqlBuilder.Append("IS NULL");
                }

                sqlBuilder.Append(" AND [DeletedAt] ");

                if (item.DeletedAt.HasValue) {
                    sqlBuilder.Append("= ?");
                    args.Add(item.DeletedAt);
                }
                else {
                    sqlBuilder.Append("IS NULL");
                }

                sqlBuilder.Append(" )");

                if (i < (items.Count - 1)) {
                    sqlBuilder.Append(" OR ");
                }
            }

            int updated = _conn.Execute(sqlBuilder.ToString(), args.ToArray());

            // If the number of rows updated is different to the number of items then
            // something has changed whilst syncing
            if (updated < items.Count) {
                NeedsResync = true;
            }

#if DEBUG
            string pluralName = typeof(T).ToString();

            pluralName += pluralName.EndsWith("s") ? "es" : "s";

            _syncService.Trace("Cleared sync flag and updated timestamp for {0}/{1} {2}", updated, items.Count(), pluralName);
#endif
        }

        protected virtual void OnAdded(IEnumerable<T> items) { }
        protected virtual void OnUpdated(IEnumerable<T> items) { }

        public string GetTrace() {
            return $"{ItemsAdded} added, {ItemsUpdated} updated, {ItemsDeleted} deleted, {ItemsPurged} purged";
        }
    }
}