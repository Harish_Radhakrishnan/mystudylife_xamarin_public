﻿using System;

namespace MyStudyLife.Sync {
    public interface ISyncService {
        DateTime? LastUpdated { get; }

        SyncStatus Status { get; }

        #region Temp?

        event EventHandler<SyncStatusChangedEventArgs> StatusChanged;

        event EventHandler<SyncCompletedEventArgs> Completed;

        event EventHandler<SyncErrorEventArgs> Error;

        #endregion

        void TrySync(SyncTriggerKind kind);

        void Reset();
    }
}
