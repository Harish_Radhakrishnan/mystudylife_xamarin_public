﻿using System;

namespace MyStudyLife.Sync {
    [Serializable]
    public class SyncException : Exception {
        internal SyncException(string message, Exception inner) : base(message, inner) { }
    }
}
