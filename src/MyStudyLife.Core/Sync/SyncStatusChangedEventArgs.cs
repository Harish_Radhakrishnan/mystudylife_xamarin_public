﻿using System;

namespace MyStudyLife.Sync {
    public sealed class SyncStatusChangedEventArgs : EventArgs {
        /// <summary>
        /// The status before the change occured.
        /// </summary>
        public SyncStatus OldStatus { get; private set; }

        /// <summary>
        /// The new status.
        /// </summary>
        public SyncStatus NewStatus { get; private set; }

        public SyncStatusChangedEventArgs(SyncStatus oldStatus, SyncStatus newStatus) {
            OldStatus = oldStatus;
            NewStatus = newStatus;
        }
    }
}
