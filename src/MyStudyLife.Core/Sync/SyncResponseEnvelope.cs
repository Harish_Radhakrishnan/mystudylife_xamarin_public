﻿using Newtonsoft.Json;

namespace MyStudyLife.Sync {
    [JsonObject]
    public class SyncResponseEnvelope {
        [JsonProperty("status")]
        public string Status { get; set; }
        
        [JsonProperty("timestamp")]
        public string Timestamp { get; set; }

        [JsonProperty("data")]
        public SyncData Data { get; set; }
    }
}
