﻿using System;

namespace MyStudyLife {
    public class PluralFormatProvider : IFormatProvider, ICustomFormatter {
        /// <summary>
        /// Sets whether to concat the value before the argument.
        /// Defaults to false.
        /// </summary>
        public bool ConcatValue { get; set; }

        public PluralFormatProvider() { }

        /// <param name="concatValue">Sets whether to concat the value before the argument.</param>
        public PluralFormatProvider(bool concatValue) {
            this.ConcatValue = concatValue;
        }

        public object GetFormat(Type formatType) {
            return this;
        }

        /// <summary>
        /// Enables pluralisation of words for string formating.
        /// Eg. 
        /// <code>String.Format(new PluralFormatProvider(), "You have {0:life;lives} left, {1:apple;apples} and {2:eye;eyes}.", 1, 0, 2)</code>
        /// </summary>
        /// <param name="format"></param>
        /// <param name="arg"></param>
        /// <param name="formatProvider"></param>
        /// <returns></returns>
        public string Format(string format, object arg, IFormatProvider formatProvider) {
            if (format == null)
				return arg != null ? arg.ToString() : null;

            string[] forms = format.Split(';');
            int value = (int)arg;
            int form = value == 1 ? 0 : 1;

            return ConcatValue ? String.Concat(value, " ", forms[form]) : forms[form];
        }
    }
}
