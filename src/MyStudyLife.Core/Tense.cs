﻿using System;

namespace MyStudyLife {
    [Flags]
    public enum Tense {
        Past = 1 << 0,
        Future = 1 << 1,
        Any = Past | Future
    }
}
