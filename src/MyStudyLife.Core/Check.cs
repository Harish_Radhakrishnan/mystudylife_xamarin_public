﻿using System;

namespace MyStudyLife {
    public static class Check {
        public static void NotNull(object obj, string paramName = null) {
            if (obj == null) {
                throw new ArgumentNullException(paramName);
            }
        }

        public static void NotEmpty(ref Guid guid, string paramName = null) {
            if (guid == Guid.Empty) {
                throw new ArgumentException("Must not be an empty or default Guid.", paramName);
            }
        }

        public static void NotNullOrEmpty(ref string s, string paramName = null) {
            if (String.IsNullOrEmpty(s)) {
                throw new ArgumentException("Must not be a null or empty string.", paramName);

            }
        }
    }
}
