﻿using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;

namespace MyStudyLife.Versioning {
    public static class LegacyHelper {
        public static Task<bool> NeedsTimetableSettings() {
            return Mvx.IoCProvider.Resolve<ITimetableSettingsStore>().NeedsTimetableSettingsAsync();
        }
    }
}