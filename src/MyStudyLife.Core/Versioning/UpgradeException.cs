﻿using System;

namespace MyStudyLife.Versioning {
    [Serializable]
    public sealed class UpgradeException : Exception {
        public UpgradeException(Version previousAppVersion, Version from, Version to, Exception inner) : base(
            String.Format("An error occurred while upgrading from V{0} to V{1}. See the inner exception for details.", from, to), inner
        ) {
            Data["PreviousAppVersion"] = previousAppVersion;
        }
    }
}