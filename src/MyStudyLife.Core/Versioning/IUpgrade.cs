﻿using System;

namespace MyStudyLife.Versioning {
	/// <summary>
	///		Defines an interface which upgrades
	///		the application from a version which is great
	///		or equal to <see cref="IUpgrade.FromVersion"/>
	///		and before <see cref="IUpgrade.ToVersion"/>.
	/// </summary>
	public interface IUpgrade {
		Version FromVersion { get; }

		Version ToVersion { get; }

		/// <summary>
		///		Attempts to upgrade the the application.
		/// </summary>
		/// <returns>
		///		True if the upgrade was successful. ie.
		///		false will be returned when an upgrade
		///		was attempted but no data was present.
		/// </returns>
		System.Threading.Tasks.Task<bool> DoUpgradeAsync();
	}
}
