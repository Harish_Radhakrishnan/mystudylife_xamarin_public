﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace MyStudyLife.Versioning.Shared {
    public abstract class HolidaysUpgrade : IUpgrade {
        public abstract Version FromVersion { get; }

        public abstract Version ToVersion { get; }

        public async Task<bool> DoUpgradeAsync() {
            var dataStore = Mvx.IoCProvider.Resolve<IDataStore>();

            var conn = dataStore.GetConnection().GetConnection();

            using (conn.Lock()) {
                if (conn.TableExists("AcademicSchedules")) {
                    try {
                        conn.BeginTransaction();

                        const string entityOldColumnsSql =
                            "[t].[Guid], [t].[Created], [t].[Updated], [t].[Deleted], [t].[Timestamp], [t].[_needsSyncing], [t].[UserId]";
                        const string entityNewColumnsSql =
                            "[Guid], [CreatedAt], [UpdatedAt], [DeletedAt], [Timestamp], [_needsSyncing], [UserId]";

                        #region Academic Years + Terms

                        conn.Execute(
                            "INSERT INTO [AcademicYears] " +
                                "(" + entityNewColumnsSql + ", [SchoolId], [StartDate], [EndDate]) " +
                            "SELECT " +
                                entityOldColumnsSql + ", [SchoolId], [StartDate], [EndDate] " +
                            "FROM [AcademicSchedules] AS [t]" +
                            "WHERE [ParentGuid] IS NULL AND NOT EXISTS (" +
                                "SELECT 1 FROM [AcademicYears] AS [y_new] WHERE [y_new].[Guid] = [t].[Guid]" +
                            ")"
                        );

                        conn.Execute(
                            "INSERT INTO [AcademicTerms] " +
                                "([Guid], [YearGuid], [Name], [StartDate], [EndDate]) " +
                            "SELECT " +
                                "[Guid], [ParentGuid], [Name], [StartDate], [EndDate] " +
                            "FROM [AcademicSchedules] AS [t_old] " +
                            "WHERE [ParentGuid] IS NOT NULL AND NOT EXISTS (" +
                                "SELECT 1 FROM [AcademicTerms] AS [t_new] WHERE [t_new].[Guid] = [t_old].[Guid]" +
                            ")"
                        );

                        #endregion

                        #region Subjects

                        RenameToOldAndRecreate<Subject>(conn);

                        conn.Execute(
                            "INSERT INTO [Subjects] " +
                                "(" + entityNewColumnsSql + ", [SchoolId], [YearGuid], [TermGuid], " +
                                "[Name], [Color]) " +
                            "SELECT " +
                                entityOldColumnsSql + ", [t].[SchoolId], " +
                                "(CASE WHEN [term].[Guid] IS NOT NULL THEN [term].[YearGuid] ELSE [year].[Guid] END), [term].[Guid], " +
                                "[t].[Name], [t].[Color] " +
                            "FROM [Subjects_Old] AS [t] " +
                            "LEFT OUTER JOIN [AcademicYears] AS [year] ON [t].[ScheduleGuid] = [year].[Guid] " +
                            "LEFT OUTER JOIN [AcademicTerms] AS [term] ON [t].[ScheduleGuid] = [term].[Guid] "
                        );

                        #endregion

                        #region Classes

                        conn.Execute("DROP TABLE IF EXISTS [ClassTimes_Old]");
                        conn.Execute("ALTER TABLE [ClassTimes] RENAME TO [ClassTimes_Old]");
                        RenameToOldAndRecreate<Class>(conn);
                        conn.CreateTable(typeof(ClassTime));

                        conn.Execute(
                            "INSERT INTO [Classes] " +
                                "(" + entityNewColumnsSql + ", [SchoolId], [YearGuid], [TermGuid], " +
                                "[SubjectGuid], [Color], [Module], [Room], [Building], [Tutor], [Type], [Date], [StartTime], [EndTime], [StartDate], [EndDate]) " +
                            "SELECT " +
                                entityOldColumnsSql + ", [t].[SchoolId], " +
                                "(CASE WHEN [term].[Guid] IS NOT NULL THEN [term].[YearGuid] ELSE [year].[Guid] END), [term].[Guid], " +
                                "[t].[SubjectGuid], [t].[Color], [t].[Module], [t].[Room], [t].[Building], [t].[Tutor], [t].[Type], [t].[Date], [t].[StartTime], [t].[EndTime], [t].[StartDate], [t].[EndDate] " +
                            "FROM [Classes_Old] AS [t] " +
                            "LEFT OUTER JOIN [AcademicYears] AS [year] ON [t].[ScheduleGuid] = [year].[Guid] " +
                            "LEFT OUTER JOIN [AcademicTerms] AS [term] ON [t].[ScheduleGuid] = [term].[Guid] "
                        );

                        // This is necessary due to the foreign key :(
                        conn.Execute(
                            "INSERT INTO [ClassTimes] " +
                                "([Guid], [ClassGuid], [StartTime], [EndTime], [Days], [RotationDays], [RotationWeek]) " +
                            "SELECT " +
                                "[Guid], [ClassGuid], [StartTime], [EndTime], [Days], [RotationDays], [RotationWeek] " +
                            "FROM [ClassTimes_Old]"
                        );

                        #endregion

                        #region Exams

                        RenameToOldAndRecreate<Exam>(conn);

                        conn.Execute(
                            "INSERT INTO [Exams] " +
                                "(" + entityNewColumnsSql + ", [SubjectGuid], [Module], [Resit], [Seat], [Room], [Date], [Duration])" +
                            "SELECT " +
                                entityOldColumnsSql +
                                ", [t].[SubjectGuid], [t].[Module], [t].[Resit], [t].[Seat], [t].[Room], [t].[Date], [t].[Duration]" +
                            "FROM [Exams_Old] AS [t]"
                        );

                        #endregion

                        #region Tasks

                        RenameToOldAndRecreate<Data.Task>(conn);

                        conn.Execute(
                            "INSERT INTO [Tasks] " +
                                "(" + entityNewColumnsSql + ", [SubjectGuid], [Type], [ExamGuid], [DueDate], [Progress], [CompletedAt], [Title], [Detail])" +
                            "SELECT " +
                                entityOldColumnsSql +
                                ", [t].[SubjectGuid], [t].[Type], [t].[ExamGuid], [t].[DueDate], [t].[Progress], [t].[CompletedAt], [t].[Title], [t].[Detail]" +
                            "FROM [Tasks_Old] AS [t]"
                        );

                        #endregion

                        conn.Commit();
                    }
                    catch {
                        conn.Rollback();

                        throw;
                    }

                    conn.Execute("DROP TABLE IF EXISTS [Tasks_Old]");
                    conn.Execute("DROP TABLE IF EXISTS [Exams_Old]");
                    conn.Execute("DROP TABLE IF EXISTS [ClassTimes_Old]");
                    conn.Execute("DROP TABLE IF EXISTS [Classes_Old]");
                    conn.Execute("DROP TABLE IF EXISTS [Subjects_Old]");
                    conn.Execute("DROP TABLE IF EXISTS [AcademicSchedules]");
                }
            }

            bool isRotationScheduleLettered = false;

            try {
                isRotationScheduleLettered = conn.ExecuteScalar<int>("SELECT COUNT(*) FROM [TimetableSettings] WHERE [UseLetters] = 1") > 0;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            // Column might not exist... table might not exist
            catch {}

            await UpgradeTimetableSettingsAsync(dataStore, conn);

            var storageProvider = Mvx.IoCProvider.Resolve<IStorageProvider>();

            string userJson = await storageProvider.LoadFileContentsAsync("User.json");

            if (!String.IsNullOrWhiteSpace(userJson)) {
                var userJObj = JObject.Parse(userJson);

                var userSettings = userJObj.GetValue("settings").ToObject<TeacherSharingUpgrade.UserSettings>();

                if (userSettings == null || userSettings.Time == null) {
                    return true;
                }

                var newUserSettings = new UserSettings {
                    Is24Hour = DateTimeFormat.Is24Hour,
                    FirstDayOfWeek = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek,
                    IsRotationScheduleLettered = isRotationScheduleLettered,
                    DefaultStartTime = userSettings.Time.DefaultStartTime,
                    DefaultDuration = userSettings.Time.DefaultDuration
                };

                userJObj["settings"] = JObject.FromObject(newUserSettings);

                await storageProvider.WriteFileContentsAsync("User.json", userJObj.ToString(Formatting.None));

                // Force load into cache
                var user = Mvx.IoCProvider.Resolve<IUserStore>().GetCurrentUser();

                if (user != null) {
                    user.Settings = newUserSettings;
                }
            }

            return true;
        }

        private async System.Threading.Tasks.Task UpgradeTimetableSettingsAsync(IDataStore dataStore, SQLiteConnectionWithLock conn) {

            var tSettings = await TimetableSettings.Get(Mvx.IoCProvider.Resolve<ISingletonDataStore>(), false);

            if (tSettings == null) {
                return;
            }

            if (tSettings.IsFixed) {
                using (conn.Lock()) {
                    conn.DeleteAll<TimetableSettings>();
                }

                return;
            }

            var years = await dataStore.GetAsync<AcademicYear>();

            foreach (var year in years) {
                year.Scheduling.Mode = (SchedulingMode)(int)tSettings.Mode;

                if (tSettings.IsWeekRotation && !year.Scheduling.StartWeek.HasValue) {
                    year.Scheduling.WeekCount = (short)tSettings.WeekCount.GetValueOrDefault(SchedulingOptions.DefaultWeekCount);
                    year.Scheduling.StartWeek = (short)tSettings.GetWeekForDate(year.StartDate);
                }

                if (tSettings.IsDayRotation && !year.Scheduling.StartDay.HasValue) {
                    year.Scheduling.DayCount = (short)tSettings.DayCount.GetValueOrDefault(SchedulingOptions.DefaultDayCount);
                    year.Scheduling.StartDay = (short)tSettings.GetDayForDate(year.StartDate).GetValueOrDefault(1);
                    year.Scheduling.Days = tSettings.Days;
                }

                await dataStore.UpdateAsync(year);
            }

            if (!await LegacyHelper.NeedsTimetableSettings()) {
                using (conn.Lock()) {
                    conn.DeleteAll<TimetableSettings>();
                }
            }
        }

        private void RenameToOldAndRecreate<T>(SQLiteConnection conn) {
            var tableName = typeof(T).GetTableName();

            conn.Execute($"DROP TABLE IF EXISTS [{tableName}]");
            conn.Execute($"ALTER TABLE [{tableName}] RENAME TO [{tableName}_Old]");
            conn.CreateTable(typeof(T));
        }
    }
}