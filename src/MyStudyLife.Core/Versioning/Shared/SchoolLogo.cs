﻿using MvvmCross;
using MyStudyLife.Data;
using System;
using System.Threading.Tasks;

namespace MyStudyLife.Versioning.Shared {
    /// <summary>
    ///     Just sets the class sharing flag on the user's
    ///     school. Before this update, it was the only
    ///     option the school had, so we didn't have a flag.
    /// </summary>
    public abstract class SchoolLogoUpgrade : IUpgrade {
        public abstract Version FromVersion { get; }
        public abstract Version ToVersion { get; }

        public async Task<bool> DoUpgradeAsync() {
            var userStore = Mvx.IoCProvider.Resolve<IUserStore>();

            var user = User.GetCurrent(userStore);

            if (user != null && user.HasSchool) {
                user.School.ClassSharingEnabled = true;

                await userStore.PersistUserAsync(user);
            }

            return true;
        }
    }
}
