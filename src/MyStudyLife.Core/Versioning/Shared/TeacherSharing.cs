﻿using System;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SQLite;
using SQLiteNetExtensions.Extensions;

namespace MyStudyLife.Versioning.Shared {
    public abstract class TeacherSharingUpgrade : IUpgrade {
        public abstract Version FromVersion { get; }

        public abstract Version ToVersion { get; }

        public async Task<bool> DoUpgradeAsync() {
            // Tables will be created in initialization
            var dataStore = Mvx.IoCProvider.Resolve<IDataStore>();

            DataStore.NoInitialization = true;

            var conn = dataStore.GetConnection().GetConnection();

            // Class times
            using (conn.Lock()) {
                // Only upgrade if we have any classes and the class times table doesn't
                // exist or it's empty.
                //
                // We have to check whether the classes table exists because we've disabled
                // initialization
                bool shouldUpgradeClassTimes =
                    conn.TableExists("Classes") &&
                    conn.ExecuteScalar<int>("SELECT COUNT(*) FROM [Classes]") > 0
                    && (
                        conn.TableExists("ClassTimes") == false ||
                        conn.ExecuteScalar<int>("SELECT COUNT(*) FROM [ClassTimes]") == 0
                    );

                if (shouldUpgradeClassTimes) {
                    try {
                        conn.BeginTransaction();

                        var classes = conn.Table<OldClass>().ToList();

                        // DataStore.NoInitialization should prevent the need for this
                        // but this may be required for multiple upgrades where not prevented.
                        //
                        // Because of foreign key REFERENCES constraint and ON DELETE CASCADE,
                        // renaming the class table means that the when we drop it, all class times
                        // are deleted.
                        conn.Execute("DROP TABLE IF EXISTS [ClassTimes]");
                        conn.Execute("DROP TABLE IF EXISTS [Classes_Old]");
                        conn.Execute("ALTER TABLE [Classes] RENAME TO [Classes_Old]");

                        conn.CreateTable<NewClass>();
                        conn.CreateTable<ClassTime>();

                        foreach (var cls in classes) {
                            conn.Insert(new NewClass {
                                Guid = cls.Guid,
                                Created = cls.Created,
                                Updated = cls.Updated,
                                Deleted = cls.Deleted,
                                Timestamp = cls.Timestamp,
                                UserId = cls.UserId,
                                SubjectGuid = cls.SubjectGuid,
                                Type = cls.Type,
                                Date = cls.Date,
                                StartTime = cls.Date.HasValue ? new TimeSpan?(cls.StartTime) : null,
                                EndTime = cls.Date.HasValue ? new TimeSpan?(cls.EndTime) : null,
                                Module = cls.Module,
                                Room = cls.Room,
                                Building = cls.Building,
                                Tutor = cls.Tutor,
                                StartDate = cls.StartDate,
                                EndDate = cls.EndDate
                            });

                            if (cls.Type == ClassType.Recurring) {
                                // We can't batch this because SQLite doesn't have a NewId() function
                                conn.Insert(new ClassTime {
                                    Guid = Guid.NewGuid(),
                                    ClassGuid = cls.Guid,
                                    StartTime = cls.StartTime,
                                    EndTime = cls.EndTime,
                                    Days = cls.Days,
                                    RotationDays = cls.RotationDays,
                                    RotationWeek = cls.RotationWeek
                                });
                            }
                        }

                        conn.Commit();
                    }
                    catch {
                        conn.Rollback();

                        throw;
                    }
                    finally {
                        DataStore.NoInitialization = false;
                    }

                    conn.Execute("DROP TABLE IF EXISTS [Classes_Old]");
                }
                else {
                    DataStore.NoInitialization = false;
                }
            }
            
            DataStore.CreateAndMigrateTables(conn, true);

            // User setup was renamed to setup_at
            var storageProvider = Mvx.IoCProvider.Resolve<IStorageProvider>();

            string userJson = await storageProvider.LoadFileContentsAsync("User.json");

            if (!String.IsNullOrWhiteSpace(userJson)) {
                var oldUser = JObject.Parse(userJson);
                var newUser = JObject.Parse(userJson);
                
                newUser["setup_at"] = oldUser["setup"].Value<DateTime?>();

                // Settings
                string settingsJson = await storageProvider.LoadFileContentsAsync("UserSettings.json");
            
                if (settingsJson != null) {
                    var userSettings = JsonConvert.DeserializeObject<OldUserSettings>(settingsJson);

                    if (userSettings != null) {
                        newUser["settings"] = JObject.FromObject(new UserSettings {
                            UpdatedAt = userSettings.Updated,
                            Time = userSettings.Time
                        });

                        if (userSettings.Timetable != null) {
                            await userSettings.Timetable.Save(Mvx.IoCProvider.Resolve<ISingletonDataStore>());
                        }
                    }
                }

                await storageProvider.WriteFileContentsAsync("User.json", newUser.ToString(Formatting.None));
            }

            return true;
        }

        [JsonObject]
        public class TimeSettings {
            [JsonProperty("default_start_time")]
            public TimeSpan DefaultStartTime { get; set; }

            [JsonProperty("default_duration")]
            public int DefaultDuration { get; set; }
        }

        [JsonObject]
        public class OldUserSettings {
            [JsonProperty("updated")]
            public DateTime? Updated { get; set; }

            [JsonProperty("time")]
            public TimeSettings Time { get; set; }

            [JsonProperty("timetable")]
            public TimetableSettings Timetable { get; set; }
        }

        [JsonObject]
        public class UserSettings {
            [JsonProperty("updated_at")]
            public DateTime? UpdatedAt { get; set; }

            [JsonProperty("time")]
            public TimeSettings Time { get; set; }
        }

        [Table("Classes")]
        public class OldClass {
            public Guid Guid { get; set; }
            public DateTime Created { get; set; }
            public DateTime? Updated { get; set; }
            public DateTime? Deleted { get; set; }
            public string Timestamp { get; set; }
            public int UserId { get; set; }
            public Guid SubjectGuid { get; set; }
            public ClassType Type { get; set; }
            public string Module { get; set; }
            public DateTime? Date { get; set; }
            public DaysOfWeek? Days { get; set; }
            public RotationDays? RotationDays { get; set; }
            public int? RotationWeek { get; set; }
            public TimeSpan StartTime { get; set; }
            public TimeSpan EndTime { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string Room { get; set; }
            public string Building { get; set; }
            public string Tutor { get; set; }
        }

        [Table("Classes")]
        public class NewClass {
            public Guid Guid { get; set; }
            public DateTime Created { get; set; }
            public DateTime? Updated { get; set; }
            public DateTime? Deleted { get; set; }
            public string Timestamp { get; set; }
            [Column("_needsSyncing")]
            public bool NeedsSyncing { get; set; }
            public int UserId { get; set; }
            public int? SchoolId { get; set; }
            public Guid SubjectGuid { get; set; }
            public Guid? ScheduleGuid { get; set; }
            public ClassType Type { get; set; }
            public string Module { get; set; }
            public string Room { get; set; }
            public string Building { get; set; }
            public string Tutor { get; set; }
            public string Color { get; set; }
            public DateTime? Date { get; set; }
            public TimeSpan? StartTime { get; set; }
            public TimeSpan? EndTime { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
    }
}
