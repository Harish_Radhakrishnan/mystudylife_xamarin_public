﻿using System;
using System.Threading.Tasks;

namespace MyStudyLife.Versioning {
	public interface IUpgradeService {
		/// <summary>
		///		Ascertains whether the application
		///		requires an upgrade by comparing
		///		the current <see cref="CurrentAppVersion"/>
		///		with the stored version.
		/// 
		///		An upgrade is only required when an <see cref="IUpgrade"/>
		///		has been registered which can upgrade the previous version
		///		(or greater) to the current version.
		/// </summary>
		bool RequiresUpgrade { get; }

		/// <summary>
		///		The previously upgraded (or stored) application
		///		version.
		/// 
		///		This will be null if it's the first time the user
		///		has opened the app.
		/// </summary>
		Version PreviousAppVersion { get; }

		/// <summary>
		///		Returns the current version of the application.
		/// </summary>
		Version CurrentAppVersion { get; }

		/// <summary>
		///		Adds an upgrade to the upgrade service.
		/// </summary>
		/// <typeparam name="TUpgrade">
		///		The type of upgrade to add.
		/// </typeparam>
		/// <exception cref="InvalidOperationException">
		///		Thrown if the same type is attempted to
		///		be registered twice.
		/// </exception>
		void RegisterUpgrade<TUpgrade>() where TUpgrade : IUpgrade;

		/// <summary>
		///		Attempts to upgrade the application.
		/// 
		///		Silently fails if <see cref="RequiresUpgrade"/>
		///		is false.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		///		Thrown when the one of the registered upgrade's
		///		<see cref="IUpgrade.ToVersion"/>
		///		is greater than the application's current version.
		/// </exception>
		Task DoUpgradeAsync();

		/// <summary>
		///		Forces the app to assume that the user
		///		has successfully performed an update.
		/// </summary>
        Task ForceCurrentVersionAsync();
	}
}
