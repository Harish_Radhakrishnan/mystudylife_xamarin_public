﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MvvmCross;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Store;
using MyStudyLife.Diagnostics;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Versioning {
	public class UpgradeService : RequireStorageProviderBase, IUpgradeService {
		public const string CurrentAppVersionKey = "MSL_CURRENT_VERSION";

	    private readonly IMslConfig _config;

		private readonly List<Type> _upgrades = new List<Type>();

		/// <summary>
		///		Ascertains whether the application
		///		requires an upgrade by comparing
		///		the current <see cref="IUpgradeService.CurrentAppVersion"/>
		///		with the <see cref="IUpgradeService.PreviousAppVersion"/>
		/// 
		///		An upgrade is only required when an <see cref="IUpgrade"/>
		///		has been registered which can upgrade the previous version
		///		(or greater) to the current version.
		/// </summary>
		public bool RequiresUpgrade {
			get {
				if (PreviousAppVersion != CurrentAppVersion) {
					if (GetApplicableUpgrades().Any()) {
						return true;
					}

					// If the current app version is greater than the previous app
					// version but there are no upgrades then we set the previous
					// version to the current to avoid finding applicable upgrades
					// upon next startup.
					PreviousAppVersion = CurrentAppVersion;
				}

				return false;
			}
		}

		private Version _previousAppVersion;
		private bool _previousAppVersionSet;

		/// <summary>
		///		The previously upgraded (or stored) application
		///		version.
		/// 
		///		This will be null if it's the first time the user
		///		has opened the app.
		/// </summary>
		public Version PreviousAppVersion {
			get {
				// Version could be null
				if (_previousAppVersionSet) return _previousAppVersion;

				string version = this.StorageProvider.GetSetting<string>(CurrentAppVersionKey);

				if (version == null || !Version.TryParse(version, out _previousAppVersion)) {
					return null;
				}

				_previousAppVersionSet = true;

				return _previousAppVersion;
			}
			protected set {
				_previousAppVersion = value;

				this.StorageProvider.AddOrUpdateSetting(CurrentAppVersionKey, value == null ? null : value.ToString());
			}
		}

	    /// <summary>
	    ///		Returns the current version of the application.
	    /// </summary>
	    public Version CurrentAppVersion {
	        get { return _config.AppVersion; }
	    }

		public UpgradeService(IMslConfig config ,IStorageProvider provider) : base(provider) {
		    this._config = config;
		}

        /// <summary>
        ///     Allows applications to set the previous version
        ///     property before an attempted upgrade is made.
        /// 
        ///     Only called if <see cref="PreviousAppVersion"/> is null.
        /// </summary>
#pragma warning disable 1998
	    protected virtual async Task EnsurePreviousVersionAsync() { }
#pragma warning restore 1998

		/// <summary>
		///		Adds an upgrade to the upgrade service.
		/// </summary>
		/// <typeparam name="TUpgrade">
		///		The type of upgrade to add.
		/// </typeparam>
		/// <exception cref="InvalidOperationException">
		///		Thrown if the same type is attempted to
		///		be registered twice.
		/// </exception>
		public void RegisterUpgrade<TUpgrade>() where TUpgrade : IUpgrade {
			_upgrades.Add(typeof(TUpgrade));
		}

		/// <summary>
		///		Attempts to upgrade the application.
		/// 
		///		Silently fails if <see cref="IUpgradeService.RequiresUpgrade"/>
		///		is false.
		/// </summary>
		/// <exception cref="InvalidOperationException">
		///		Thrown when the one of the registered upgrade's
		///		<see cref="IUpgrade.ToVersion"/>
		///		is greater than the application's current version.
		/// </exception>
		public async Task DoUpgradeAsync() {
		    try {
		        if (PreviousAppVersion == null) {
		            await EnsurePreviousVersionAsync();
		        }

		        var applicableUpgrades = GetApplicableUpgrades();
		        var previousAppVersion = PreviousAppVersion;

		        foreach (var upgrade in applicableUpgrades) {
		            try {
		                if (!await upgrade.DoUpgradeAsync()) {
		                    Debug.WriteLine("Unable to upgrade from version {0} to {1}.", upgrade.FromVersion, upgrade.ToVersion);
		                }
		                else {
		                    PreviousAppVersion = upgrade.ToVersion;
		                }
		            }
		            catch (Exception ex) {
		                throw new UpgradeException(previousAppVersion, upgrade.FromVersion, upgrade.ToVersion, ex);
		            }
		        }

		        PreviousAppVersion = CurrentAppVersion;
		    }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, new List<string> { "UpgradeService" });

		        throw;
		    }
		}

		/// <summary>
		///		Forces the app to assume that the user
		///		has successfully performed an update.
		/// </summary>
		public async Task ForceCurrentVersionAsync() {
		    var conn = Mvx.IoCProvider.Resolve<IDataStore>().GetConnection();
            var tables = await conn.QueryAsync<TableInfo>("SELECT [tbl_name] FROM [sqlite_master] WHERE [type] = 'table'");

            try {
                await conn.SetForeignKeysPermissionsAsync(false);

		        foreach (var table in tables) {
		            await conn.ExecuteAsync($"DROP TABLE IF EXISTS [{table.Name}]");
		        }
		    }
		    finally {
		        await conn.SetForeignKeysPermissionsAsync(true);
		    }

		    var storageProvider = Mvx.IoCProvider.Resolve<IStorageProvider>();

		    await storageProvider.RemoveFilesAsync();

            DataStore.CreateAndMigrateTables(conn.GetConnection(), true);

            PreviousAppVersion = CurrentAppVersion;
		}

		private IEnumerable<IUpgrade> GetApplicableUpgrades() {
		    if (this.PreviousAppVersion == null || PreviousAppVersion == CurrentAppVersion) {
		        return Enumerable.Empty<IUpgrade>();
		    }

			return _upgrades.Select(Activator.CreateInstance)
                            .Cast<IUpgrade>()
                            .Where(x => this.PreviousAppVersion < x.ToVersion)
                            .OrderBy(x => x.ToVersion);
		}

	    class TableInfo {
            [Column("tbl_name")]
	        public string Name { get; set; }
	    }
	}
}
