﻿using System;
using System.Threading.Tasks;

namespace MyStudyLife.Authorization {
    public interface IAuthorizationService : IDisposable {
        bool IsAuthorized { get; }

        Task<AuthorizationServiceResponse> AuthorizeCredentials(string email, string password);

        Task<AuthorizationServiceResponse> AuthorizeOAuthResult(string code);
        
        Task<AuthorizationServiceResponse> AuthorizeAppleIdResult(string idToken, string? firstName, string? lastName);

    }
}
