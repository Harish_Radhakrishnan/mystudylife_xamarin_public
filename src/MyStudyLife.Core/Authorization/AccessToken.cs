﻿using System;
using System.IO;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Serialization;
using MyStudyLife.Diagnostics;
using Newtonsoft.Json;

namespace MyStudyLife.Authorization {
    [JsonObject]
    public class AccessToken : IDeserializeJson {
        /// <summary>
        /// The 21 character token.
        /// </summary>
        [JsonProperty("token")]
        public string Token { get; set; }

        /// <summary>
        /// The date the access token was granted.
        /// </summary>
        [JsonProperty("granted")]
        public DateTime Granted { get; set; }

        /// <summary>
        /// The date the access token expires.
        /// </summary>
        [JsonProperty("expires")]
        public DateTime? Expires { get; set; }

        #region Storage and Retrieval

        private const string AccessTokenSettingName = "MSL_AT";

        private static AccessToken _currentToken;

        /// <summary>
        /// Gets the current access token.
        /// </summary>
        /// <param name="storageProvider">The current storage provider.</param>
        /// <returns>The current access token, null if it doesn't exist.</returns>
        public static AccessToken GetCurrent(IStorageProvider storageProvider) {
            if (_currentToken != null) {
                return _currentToken;
            }

            try {
                return _currentToken = storageProvider.GetSetting<AccessToken>(AccessTokenSettingName);
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);

                return null;
            }
        }

        /// <summary>
        /// Persists the current access token
        /// to application settings.
        /// </summary>
        /// <param name="storageProvider">The current storage provider.</param>
        public void Persist(IStorageProvider storageProvider) {
            _currentToken = this;

            storageProvider.AddOrUpdateSetting(AccessTokenSettingName, _currentToken);
        }

        /// <summary>
        /// Removes the current access token
        /// from application settings.
        /// </summary>
        /// <param name="storageProvider">The current storage provider.</param>
        public void Remove(IStorageProvider storageProvider) {
            _currentToken = null;

            storageProvider.RemoveSetting(AccessTokenSettingName);
        }

        void IDeserializeJson.SetValuesFromJson(string json) {
            using (var stringReader = new StringReader(json))
            using (var reader = new JsonTextReader(stringReader))
                while (reader.Read()) {
                    if (reader.TokenType != JsonToken.PropertyName) {
                        continue;
                    }

                    switch (reader.Value.ToString()) {
                        case "token":
                            this.Token = reader.ReadAsString();
                            break;
                        case "granted":
                            this.Granted = reader.ReadAsDateTime().Value;
                            break;
                        case "expires":
                            this.Expires = reader.ReadAsDateTime().Value;
                            break;
                    }
                }
        }

        #endregion
    }
}
