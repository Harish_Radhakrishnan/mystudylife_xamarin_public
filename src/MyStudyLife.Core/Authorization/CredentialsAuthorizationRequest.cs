﻿using Newtonsoft.Json;
using MyStudyLife.Data;

namespace MyStudyLife.Authorization {
    public class CredentialsAuthorizationRequest : BaseAuthorizationRequest {
        [JsonProperty("grant_type")]
        public string GrantType {
            get { return "password"; }
        }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        /// <summary>
        ///     Creates a new instance of the <see cref="CredentialsAuthorizationRequest"/> class.
        /// </summary>
        /// <param name="email">The email of the user to authorize.</param>
        /// <param name="password">The password of the user to authorize.</param>
        /// <param name="device">Information about the current device.</param>
        public CredentialsAuthorizationRequest(string email, string password, Device device)
            : base(device) {

            this.Email = email;
            this.Password = password;
        }
    }
}