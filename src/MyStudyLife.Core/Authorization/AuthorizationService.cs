﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Net;
using MyStudyLife.Utility;
using Newtonsoft.Json;

namespace MyStudyLife.Authorization {
    // Shouldn't this be called Authentication?
    public class AuthorizationService : IAuthorizationService {
        private readonly IStorageProvider _storageProvider;
        private readonly IDeviceInfoProvider _deviceInfoProvider;
        private readonly IHttpApiClient _httpClient;
        private readonly IMvxMessenger _messenger;
        private readonly IUserStore _userStore;

        public AuthorizationService(
            IDeviceInfoProvider deviceInfoProvider,
            IStorageProvider storageProvider,
            IHttpApiClient apiClient,
            IMvxMessenger messenger,
            IUserStore userStore
        ) {
            this._deviceInfoProvider = deviceInfoProvider;
            this._storageProvider = storageProvider;
            this._httpClient = apiClient;
            this._messenger = messenger;
            this._userStore = userStore;
        }

        /// <summary>
        ///     Checks whether the current user is authorized.
        /// </summary>
        public bool IsAuthorized =>
            // False as if the user isn't stored locally then there is a good chance the user isn't logged on.
            AccessToken.GetCurrent(_storageProvider) != null;

        /// <summary>
        ///     Authorizes the given credentials against a My Study Life
        ///     account.
        /// </summary>
        /// <param name="email">The user's email address.</param>
        /// <param name="password">The user's password.</param>
        public async Task<AuthorizationServiceResponse> AuthorizeCredentials(string email, string password) {
            var device = await _deviceInfoProvider.GetDeviceInformationAsync();

            var request = new ExchangePasswordRequest {
                ClientId = MslConfig.Current.ApiClientSettings.ClientId,
                Email = email,
                Password = password,
                Device = device
            };

            return await ExchangeForTokenAsync(request);
        }

        /// <summary>
        ///     Authorizes the given authorization result from
        ///     My Study Life's OAuth API.
        /// </summary>
        /// <param name="code">
        ///     The code to exchange for an access token.
        /// </param>
        public async Task<AuthorizationServiceResponse> AuthorizeOAuthResult(string code) {
            Check.NotNullOrEmpty(ref code, nameof(code));

            var device = await _deviceInfoProvider.GetDeviceInformationAsync();

            var request = new ExchangeCodeRequest {
                ClientId = MslConfig.Current.ApiClientSettings.ClientId,
                Code = code,
                Device = device,
                RedirectUri = MslConfig.Current.OAuthEndUri
            };

            return await ExchangeForTokenAsync(request);
        }

        /// <summary>
        ///     Authorizes the given authorization result from Apple ID.
        /// </summary>
        /// <param name="idToken">
        ///     The JWT token provided by the iOS API.
        /// </param>
        /// <param name="firstName">
        ///    The user's first name.
        /// </param>
        /// <param name="lastName">
        ///    The user's last name.
        /// </param>
        public async Task<AuthorizationServiceResponse> AuthorizeAppleIdResult(string idToken, string? firstName, string? lastName) {
            Check.NotNullOrEmpty(ref idToken, nameof(idToken));

            var device = await _deviceInfoProvider.GetDeviceInformationAsync();

            var request = new ExchangeAppleIdRequest {
                ClientId = MslConfig.Current.ApiClientSettings.ClientId,
                IdToken = idToken,
                User = (firstName ?? lastName) != null
                    ? new AppleIdUser {
                        FirstName = firstName,
                        LastName = lastName
                    }
                    : null,
                Device = device
            };

            return await ExchangeForTokenAsync(request);
        }

        private async Task<AuthorizationServiceResponse> ExchangeForTokenAsync(object exchangeRequestModel) {
            try {
                var jsonContent = JsonConvert.SerializeObject(exchangeRequestModel);

                var response = await _httpClient.PostJsonAsync("oauth/token", jsonContent);

                var authResponse = JsonConvertEx.DeserializeObject<AuthorizationResponse>(response.Content);

                if (authResponse == null || !authResponse.IsValid()) {
                    throw new HttpApiException(HttpApiErrorMessage.ServiceUnreachable, response.RequestUrl);
                }

                Debug.WriteLine("AuthService: Successful response returned, access token {0}, user {1}",
                    authResponse.AccessToken,
                    authResponse.User);

                await _userStore.PersistUserAsync(authResponse.User);

                new AccessToken {
                    Token = authResponse.AccessToken,
                    Granted = DateTime.UtcNow,
                    Expires = authResponse.ExpiresIn.HasValue
                        ? new DateTime?(DateTime.UtcNow.AddMilliseconds(authResponse.ExpiresIn.Value))
                        : null
                }.Persist(_storageProvider);

                _messenger.Publish(new AuthStatusChangedEventMessage(this, true, AuthStatusChangeCause.SignIn));

                return new AuthorizationServiceResponse(authResponse.User);
            }
            catch (HttpApiException ex) when (ex.ErrorMessage != null) {
#if DEBUG
                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger("AuthService")
                    .LogTrace($"{ex.StatusCode} response returned, {ex.ErrorMessage.Name}");
#endif
                return new AuthorizationServiceResponse(ex.ErrorMessage);
            }
        }

        public void Dispose() {
            _httpClient?.Dispose();
        }
        abstract class ExchangeTokenRequest {
            [JsonProperty("grant_type")] public abstract string GrantType { get; }
            [JsonProperty("scope")] public string Scope => "device";
            [JsonProperty("client_id")] public string ClientId { get; set; } = null!;
            [JsonProperty("device")] public Device Device { get; set; } = null!;
        }

        [JsonObject]
        class ExchangeCodeRequest : ExchangeTokenRequest {
            [JsonProperty("grant_type")] public override string GrantType => "code";

            [JsonProperty("redirect_uri")] public string RedirectUri { get; set; } = null!;
            [JsonProperty("code")] public string Code { get; set; } = null!;
        }

        [JsonObject]
        class ExchangeAppleIdRequest : ExchangeTokenRequest {
            [JsonProperty("grant_type")] public override string GrantType => "appleid";
            [JsonProperty("id_token")] public string IdToken { get; set; } = null!;
            [JsonProperty("user")] public AppleIdUser? User { get; set; }
        }

        [JsonObject]
        class AppleIdUser {

            [JsonProperty("first_name")] public string? FirstName { get; set; }
            [JsonProperty("last_name")] public string? LastName { get; set; }
        }

        [JsonObject]
        class ExchangePasswordRequest : ExchangeTokenRequest {
            [JsonProperty("grant_type")] public override string GrantType => "password";
            [JsonProperty("email")] public string Email { get; set; } = null!;
            [JsonProperty("password")] public string Password { get; set; } = null!;
        }
    }
}