﻿namespace MyStudyLife.Authorization {
    /// <summary>
    ///     Represents the action that caused the authorization
    ///     status to change.
    /// </summary>
    public enum AuthStatusChangeCause {
        /// <summary>
        ///     The user manully signed in.
        /// </summary>
        SignIn,
        /// <summary>
        ///     The user manually signed out.
        /// </summary>
        SignOut,
        /// <summary>
        ///     An unauthorized response was received
        ///     from the server.
        /// </summary>
        UnauthorizedHttpResponse
    }
}
