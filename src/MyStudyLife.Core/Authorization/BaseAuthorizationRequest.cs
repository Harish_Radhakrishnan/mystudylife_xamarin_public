﻿using System;
using MyStudyLife.Data;
using Newtonsoft.Json;

namespace MyStudyLife.Authorization {
    [JsonObject]
    public abstract class BaseAuthorizationRequest {
        [JsonProperty("device")]
        public Device Device { get; set; }

        protected BaseAuthorizationRequest(Device device) {
            if (device == null)
                throw new ArgumentException("device");

            this.Device = device;
        }
    }
}
