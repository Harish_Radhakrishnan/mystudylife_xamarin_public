﻿using System;
using MyStudyLife.Data;
using Newtonsoft.Json;

namespace MyStudyLife.Authorization {
    /// <summary>
    /// The object returned to the client if authorization is successfull.
    /// </summary>
    [JsonObject]
    public sealed class AuthorizationResponse {
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int? ExpiresIn { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }
        
        // Not sure what the point of this is?
        public bool IsValid() {
            return !String.IsNullOrWhiteSpace(AccessToken) && User != null;
        }
    }
}
