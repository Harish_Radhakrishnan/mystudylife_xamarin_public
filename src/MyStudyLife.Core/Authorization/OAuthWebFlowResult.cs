﻿using MyStudyLife.Net;
using System;
using System.Linq;
using System.Collections.Generic;

namespace MyStudyLife.Authorization {
    public class OAuthWebFlowResult {
        const string QUERY_STRING_INVALID_EXCEPTION_MESSAGE = "Query string is not valid for an OAuth redirect URI";

        /// <summary>
        ///     The OAuth code returned by My Study Life
        /// </summary>
        public string? Code { get; }

        /// <summary>
        ///     The name of the error, if any.
        /// </summary>
        public string? Error { get; }

        /// <summary>
        ///     A description of the error. Should not be shown to the user
        ///     in most cases.
        /// </summary>
        public string? ErrorMessage { get; }

        /// <summary>
        ///     Any extra detail associated with the error message.
        /// </summary>
        public string? ErrorDetail { get; }

        /// <summary>
        ///     A unique identifier for the request returned from the server
        ///     in the event of an error.
        /// </summary>
        public string? CorrelationId { get; }

        public OAuthWebFlowResult(string code) {
            Check.NotNullOrEmpty(ref code, nameof(code));

            Code = code;
        }

        public OAuthWebFlowResult(string error, string errorMessage, string? errorDetail = null, string? correlationId = null) {
            Check.NotNullOrEmpty(ref error, nameof(error));

            // This is a special case where no error message is required
            if (error != OAuthWebFlowResultError.UserCancel) {
                Check.NotNullOrEmpty(ref errorMessage, nameof(errorMessage));
            }

            Error = error;
            ErrorMessage = errorMessage;
            ErrorDetail = errorDetail;
            CorrelationId = correlationId;
        }

        /// <summary>
        ///     Creates a <see cref="OAuthUrlWebFlowResult"/> from the query string
        ///     provided to the application via a native redirect.
        /// </summary>
        /// <param name="queryString">
        ///     The query string. Should contain either "code" or "error" and "error_message".
        /// </param>
        public static OAuthWebFlowResult FromQueryString(string queryString) {
            var queryItems = UriHelper.ParseQueryString(queryString);

            IList<string> codeQueryValues;
            if (queryItems.TryGetValue("code", out codeQueryValues)) {
                return new OAuthWebFlowResult(codeQueryValues.First());
            }

            IList<string> errorValues;
            if (queryItems.TryGetValue("error", out errorValues)) {
                IList<string> errorMessageValues, errorDetailValues, correlationIdValues;

                try {
                    return new OAuthWebFlowResult(
                         errorValues.First(),
                         queryItems.TryGetValue("error_message", out errorMessageValues) ? errorMessageValues.First() : null,
                         queryItems.TryGetValue("error_detail", out errorDetailValues) ? errorDetailValues.First() : null,
                         queryItems.TryGetValue("correlation_id", out correlationIdValues) ? correlationIdValues.First() : null
                    );
                }
                catch (Exception ex) {
                    throw new ArgumentException(QUERY_STRING_INVALID_EXCEPTION_MESSAGE, nameof(queryString), ex);
                }
            }

            throw new ArgumentException(QUERY_STRING_INVALID_EXCEPTION_MESSAGE, nameof(queryString));
        }
    }

    public static class OAuthWebFlowResultError {
        public const string UserCancel = "user_cancel";
    }
}
