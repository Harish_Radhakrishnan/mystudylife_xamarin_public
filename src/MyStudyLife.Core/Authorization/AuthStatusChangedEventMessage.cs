﻿using MvvmCross.Plugin.Messenger;

namespace MyStudyLife.Authorization {
    public class AuthStatusChangedEventMessage : MvxMessage {
        public bool IsAuthorized { get; set; }

        public AuthStatusChangeCause Cause { get; set; }

        public AuthStatusChangedEventMessage(object sender, bool isAuthorized, AuthStatusChangeCause cause) : base(sender) {
            this.IsAuthorized = isAuthorized;
            this.Cause = cause;
        }
    }
}