﻿using System.Runtime.Serialization;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.Authorization {
    /// <summary>
    /// The provider of authentication 
    /// (verifying identity).
    /// </summary>
    public enum AuthenticationProvider {
        [EnumMember(Value = "msl")]
        MyStudyLife = 0,
        [EnumMember(Value = "google")]
        Google = 1,
        [EnumMember(Value = "facebook")]
        Facebook = 2,
        [EnumMember(Value = "azure"), EnumTitle("Office 365")]
        Azure = 3
    }
}
