﻿using System;
using MyStudyLife.Data;
using Newtonsoft.Json;

namespace MyStudyLife.Authorization {
    [JsonObject]
    public sealed class TokenExchangeRequest : BaseAuthorizationRequest {
        [JsonProperty("code")]
        public string Code { get; private set; }

        public TokenExchangeRequest(string code, Device device) : base(device) {
            if (String.IsNullOrEmpty(code)) {
                throw new ArgumentNullException(nameof(code));
            }
            
            this.Code = code;
        }
    }
}
