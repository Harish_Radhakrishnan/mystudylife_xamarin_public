﻿using MyStudyLife.Data;
using MyStudyLife.Net;

namespace MyStudyLife.Authorization {
	public sealed class AuthorizationServiceResponse {
		public User User { get; private set; }

		public bool IsAuthorized {
			get { return this.User != null; }
		}

		public HttpApiErrorMessage ErrorMessage { get; private set; }

		public AuthorizationServiceResponse(User user) {
			this.User = user;
		}

		public AuthorizationServiceResponse(HttpApiErrorMessage errorMessage) {
			this.ErrorMessage = errorMessage;
		}
	}
}
