﻿using System;
using MyStudyLife.Data;

namespace MyStudyLife {
    public abstract class RequireStorageProviderBase : IDisposable {
        public IStorageProvider StorageProvider { get; private set; }

        protected RequireStorageProviderBase(IStorageProvider provider) {
            Check.NotNull(provider, nameof(provider));

            this.StorageProvider = provider;
        }

        public void Dispose() {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {}

        ~RequireStorageProviderBase() {
            this.Dispose(false);
        }
    }
}
