﻿using System;
using System.Diagnostics;

namespace MyStudyLife.Diagnostics {
    public class Logger {
        #region Instance

        private static Logger _instance;

        /// <summary>
        ///     Gets an existing (or new if it does not yet exist)
        ///     instance of <see cref="Logger"/> resolving the
        ///     <see cref="IBugReporter"/> using <c>Mvx.IoCProvider.Resolve</c>
        /// </summary>
        public static Logger GetInstance() {
	        throw new NotImplementedException();
        }
        
        /// <summary>
        ///     Gets an existing (or new if it does not yet exist)
        ///     instance of <see cref="Logger"/> using the given 
        ///     <paramref name="bugReporter"/>.
        /// </summary>
        /// <param name="bugReporter">The bug reporter to use.</param>
        /// <exception cref="Exception">
        ///     Thrown when the current instance's <see cref="IBugReporter"/>
        ///     does not match the given <paramref name="bugReporter"/>.
        /// </exception>
        public static Logger GetInstance(IBugReporter bugReporter) {
            if (_instance == null) {
                return _instance = new Logger(bugReporter);
            }
            
            if (_instance.BugReporter != bugReporter) {
                throw new Exception("Cannot support multiple loggers with different bug reporters.");
            }

            return _instance;
        }

        #endregion

        #region Bug Reporter

        public IBugReporter BugReporter { get; private set; }

        #endregion

        #region Constructors

        private Logger(IBugReporter bugReporter) {
            this.BugReporter = bugReporter;
        }

        #endregion

        [Conditional("DEBUG")]
        public void Debug(string text) {
            LogTrace(text);
        }
        
        private void LogTrace(string text) {
            System.Diagnostics.Debug.WriteLine("[{0:d}] DebugBugReporter: {1}", DateTime.Now, text);
        }
    }
}
