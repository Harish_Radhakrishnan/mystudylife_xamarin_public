﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyStudyLife.Net;
using MyStudyLife.Data;

namespace MyStudyLife.Diagnostics {
	public abstract class BugReporter : IBugReporter {
        #region Abstract Implementation of IBugReporter

        protected abstract void SetUserImpl(string uid);
		protected abstract void SendImpl(Exception exception, IList<string> tags, IDictionary customUserData);

        #endregion

        #region Implementation of IBugReporter

        public void SetUser(User user) => SetUserImpl(user != null ? user.Id.ToString() : null);

        public void Send(Exception exception) => Send(exception, false);
		public void Send(Exception exception, IDictionary customUserData) => Send(exception, false, customUserData: customUserData);
		public void Send(Exception exception, IList<string> tags) => Send(exception, false, tags);
        public void Send(Exception exception, IList<string> tags, IDictionary customUserData) => Send(exception, false, tags, customUserData);
        public void Send(Exception exception, bool handled = false, IList<string> tags = null, IDictionary customUserData = null) {
            if (handled) {
                (tags ?? (tags = new List<string>())).Add("Handled");
            }

            SendImpl(exception, tags, PrepareForSend(exception, customUserData));
        }

        #endregion

        private IDictionary PrepareForSend(Exception ex, IDictionary customUserData) {
            customUserData = customUserData ?? new Dictionary<string, object>();

            HttpApiException apiEx = ex as HttpApiException;

			if (apiEx != null) {
				customUserData["StatusCode"] = apiEx.StatusCode;
				customUserData["RequestUrl"] = apiEx.RequestUrl.ToString();
				customUserData["ErrorMessage"] = apiEx.ErrorMessage;
			}

			return customUserData;
		}
	}
}
