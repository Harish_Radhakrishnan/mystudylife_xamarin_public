﻿using MyStudyLife.Data;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MyStudyLife.Diagnostics {
    /// <summary>
    ///     Defines an interface for a service
    ///     which reports exceptions.
    /// </summary>
    public interface IBugReporter {
	    void SetUser(User user);

	    void Send(Exception exception);

	    void Send(Exception exception, IDictionary customUserData);

	    void Send(Exception exception, IList<string> tags);

	    void Send(Exception exception, IList<string> tags, IDictionary customUserData);
        
        void Send(Exception exception, bool handled = false, IList<string> tags = null, IDictionary customUserData = null);
    }
}
