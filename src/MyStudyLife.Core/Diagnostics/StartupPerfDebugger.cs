﻿using System.Collections.Generic;
using System.Text;

namespace MyStudyLife.Diagnostics {
    /// <summary>
    ///     Should not be used in production.
    /// </summary>
    public static class StartupPerfDebugger {
        private static List<Event> _events = new List<Event>();
        private static System.Diagnostics.Stopwatch _sw;

        private static long GetElapsedMillis() {
            if (_sw == null) {
                _sw = System.Diagnostics.Stopwatch.StartNew();
                return 0;
            }
            else {
                return _sw.ElapsedMilliseconds;
            }
        }

        public static void LogEvent(string name) {
            _events.Add(new Event(name, GetElapsedMillis()));
        }

        public static Frame LogFrame(string name) {
            _events.Add(new Event(name, GetElapsedMillis()) {
                IsStart = true
            });

            return new Frame(name);
        }

        public static string GetEvents() {
            var sb = new StringBuilder();
            var indent = new StringBuilder();

            foreach (var e in _events) {
                if (e.IsEnd) {
                    indent.Remove(indent.Length - 1, 1);
                }

                sb.AppendLine(e.ElapsedMillis.ToString().PadRight(6, ' ') + indent + e.Name);

                if (e.IsStart) {
                    indent.Append('\t');
                }
            }

            return sb.ToString();
        }

        private class Event {
            public string Name { get; }
            public long ElapsedMillis { get; }

            public bool IsStart { get; set; }

            public bool IsEnd { get; set; }

            public Event(string name, long elapsedMillis) {
                this.Name = name;
                this.ElapsedMillis = elapsedMillis;
            }
        }

        public class Frame {
            public string Name { get; }

            protected internal Frame(string name) {
                this.Name = name;
            }

            public void Complete() {
                _events.Add(new Event(this.Name, _sw.ElapsedMilliseconds) {
                    IsEnd = true
                });
            }
        }
    }
}