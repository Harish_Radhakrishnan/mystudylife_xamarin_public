﻿using System.Threading.Tasks;

namespace MyStudyLife {
    public static class TaskEx {
        /// <summary>
        ///     Returns a task already in the completed state.
        /// </summary>
        public static Task NoOp => Task.FromResult(0);
    }
}
