using System;
using System.Threading;
using System.Threading.Tasks;

namespace MyStudyLife.Utility
{
    public static class MvxLockableObjectHelpers
    {
        public static void RunSyncWithLock(object lockObject, Action action)
        {
            lock (lockObject)
            {
                action();
            }
        }

        public static void RunAsyncWithLock(object lockObject, Action action)
        {
            Task.Run(() =>
            {
                lock (lockObject)
                {
                    action();
                }
            });
        }

        public static void RunSyncOrAsyncWithLock(object lockObject, Action action, Action whenComplete = null)
        {
            if (Monitor.TryEnter(lockObject))
            {
                try
                {
                    action();
                }
                finally
                {
                    Monitor.Exit(lockObject);
                }

                whenComplete?.Invoke();
            }
            else
            {
                Task.Run(() =>
                {
                    lock (lockObject)
                    {
                        action();
                    }

                    whenComplete?.Invoke();
                });
            }
        }
    }
}
