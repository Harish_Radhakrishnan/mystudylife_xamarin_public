MvvmCross.Plugin.File
==============

##Details##
The File plugin provides cross-platform access to a File Store API.

We have manually added plugin code into each solution [Core, iOS and Android].   

* Git Repo : https://github.com/MvvmCross/MvvmCross/tree/7.0.1/MvvmCross.Plugins/File
*Comment hash : cd279b4ed2c240c0b21cb2158e59cfceb6749660

We have added MvvmCross file plugin version 7.0.1 into manually and removed package dependency as it was internally depend on IMvxLog, which support removed in latest MvvMCross version 8.0.2. And Officially, MvvmCross team has stopped maintenance of file plugin. 

### Changes We have done on original source code  ###

1. Remove package dependency from each project. 
2. Replace usage of IMvxLog with ILogger/ILoggerProvider for logging exception and warning.
3. Rename plugin's file from Setup.cs to FilePluginSetupAndroid.cs for Android platform.
4. Rename plugin's file from Setup.cs to FilePluginSetupiOS.cs for iOS platform.
5. We have write logic in each platform [iOS and Android] to configure file plugin in method InitializeFirstChance in file Setup.cs
6. Comment "TODO - do more here?" is written in file MvxFileStoreBase in method TryReadBinaryFile done by MvvmCross team. 