﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.Configuration;
using MyStudyLife.Data;

namespace MyStudyLife.Utility
{
    public class FeatureService : IFeatureService {
        private readonly IUserStore _userStore;

        private readonly HashSet<FeatureCheck> _checks = new HashSet<FeatureCheck>();

        public FeatureService(IUserStore userStore) {
            this._userStore = userStore;

            AddCheck(Feature.WriteUserProfile, IfNotManagedSchool);
            AddCheck(Feature.WriteAcademicYears, IfNotManagedSchool);
            AddCheck(Feature.WriteHolidays, IfNotManagedSchool);
            AddCheck(Feature.WriteSubjects, IfNotManagedSchool);
            AddCheck(Feature.WriteClasses, IfNotManagedSchool);
            AddCheck(Feature.JoinClasses, IfStudentWithUnmanagedSchool);
            AddCheck(Feature.CustomizeDashboard, IfPremium);
            AddCheck(Feature.ExtendedColors, IfPremium);
        }

        public async Task<bool> CanUseFeatureAsync(Feature feature, CancellationToken cancellationToken = new CancellationToken()) {
            bool anyChecks = false;

            foreach (var check in _checks) {
                if (check.Feature == feature) {
                    anyChecks = true;

                    if (await check.CheckFn() == false) {
                        return false;
                    }
                }
            }

            if (!anyChecks) {
               Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(FeatureService)).LogTrace($"No checks registered for feature '{feature}'");
            }

            return true;
        }

        private void AddCheck(Feature feature, Func<Task<bool>> checkFn) {
            _checks.Add(new FeatureCheck(feature, checkFn));
        }

        private Task<bool> IfPremium() => UserCheckAsync(u => (u.HasSchool && u.School.IsManaged) || MslConfig.Current.MockUserPremiumStatus);
        private Task<bool> IfNotManagedSchool() => UserCheckAsync(u => !u.HasSchool || !u.School.IsManaged);
        private Task<bool> IfStudentWithUnmanagedSchool() => UserCheckAsync(u => u.IsStudent && u.HasSchool && u.School.ClassSharingEnabled);

        /// <summary>
        ///     Wrapper for checking the user is eliable for a feature
        ///     to reduce code duplication.
        /// </summary>
        /// <param name="checkFn">
        ///     The check to run against the user. User will never be null.
        /// </param>
        private Task<bool> UserCheckAsync(Func<User, bool> checkFn) {
            var user = _userStore.GetCurrentUser();

            return System.Threading.Tasks.Task.FromResult(user != null && checkFn(user));
        }

        private class FeatureCheck {
            public Feature Feature { get; }

            public Func<Task<bool>> CheckFn { get; }

            public FeatureCheck(Feature feature, Func<Task<bool>> checkFn) {
                this.Feature = feature;
                this.CheckFn = checkFn;
            }
        }
    }
}
