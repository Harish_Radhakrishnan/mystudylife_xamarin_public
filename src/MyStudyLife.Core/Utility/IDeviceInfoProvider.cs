﻿using System.Threading.Tasks;
using MyStudyLife.Data;

namespace MyStudyLife.Utility {
    public interface IDeviceInfoProvider {
        /// <summary>
        ///     Returns information about the current device,
        ///     including a unique identifier, manufacturer and model.
        /// </summary>
        Task<Device> GetDeviceInformationAsync();
    }
}
