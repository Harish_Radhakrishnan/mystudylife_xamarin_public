﻿using System.Threading;
using System.Threading.Tasks;

namespace MyStudyLife.Utility {
    public interface IFeatureService {
        Task<bool> CanUseFeatureAsync(Feature feature, CancellationToken cancellationToken = default(CancellationToken));
    }
}