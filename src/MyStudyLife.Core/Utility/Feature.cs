﻿namespace MyStudyLife.Utility {
    public enum Feature {
        WriteUserProfile,
        WriteAcademicYears,
        WriteHolidays,
        WriteSubjects,
        WriteClasses,
        JoinClasses,
        CustomizeDashboard,
        ExtendedColors
    }
}