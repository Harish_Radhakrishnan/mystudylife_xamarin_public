using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using AndroidX.AppCompat.App;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data.UI;
using MyStudyLife.Droid.Fragments.Dialogs;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.ColorPicker")]
    public class ColorPicker : View, IListPicker, ColorPickerDialogFragment.IOnColorSetListener {

        public event EventHandler SelectedItemChanged;

        object IListPicker.SelectedItem {
            get { return SelectedItem; }
            set { SelectedItem = (UserColor)value; }
        }

        IEnumerable IListPicker.ItemsSource {
            get { return ItemsSource; }
            set { ItemsSource = (IEnumerable<UserColor>)value; }
        }

        private UserColor _selectedItem;

        public UserColor SelectedItem {
            get { return _selectedItem; }
            set {
                if (!ReferenceEquals(_selectedItem, value)) {
                    _selectedItem = value;

                    this.Invalidate();
                }
            }
        }

        public string SelectedValue {
            get { return _selectedItem != null ? _selectedItem.Identifier : null; }
            set {
                if (this.SelectedItem != null && this.SelectedItem.Identifier == value || value == null) {
                    this.SelectedItem = null;

                    return;
                }

                if (this.ItemsSource == null || !this.ItemsSource.Any()) {
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogWarning("ColorPicker: SelectedValue set whilst ItemsSource null or empty.");

                    return;
                }

                var item = this.ItemsSource.FirstOrDefault(x => x.Identifier == value);

                if (item == null) {
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogWarning("ColorPicker: SelectedValue '{0}' not found in ItemsSource.", value);

                    return;
                }

                this.SelectedItem = item;
            }
        }

        private MvxNotifyCollectionChangedEventSubscription _itemsSourceSubscription;

        private IEnumerable<UserColor> _itemsSource; 

        public IEnumerable<UserColor> ItemsSource {
            get => _itemsSource;
            set {
                if (_itemsSourceSubscription != null) {
                    _itemsSourceSubscription.Dispose();
                    _itemsSourceSubscription = null;
                }

                if (!ReferenceEquals(_itemsSource, value)) {
                    _itemsSource = value;

                    this.Invalidate();
                }

                if (_itemsSource is INotifyCollectionChanged observable) {
                    _itemsSourceSubscription = observable.WeakSubscribe(ItemsSourceOnCollectionChanged);
                }
            }
        }

        private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.Invalidate();
        }

        #region Constructors

        protected ColorPicker(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer) {
            Initialize();
        }
        public ColorPicker(Context context)
            : base(context) {
            Initialize();
        }
        public ColorPicker(Context context, IAttributeSet attrs)
            : base(context, attrs) {
            Initialize();
        }
        public ColorPicker(Context context, IAttributeSet attrs, int defStyle)
            : base(context, attrs, defStyle) {
            Initialize();
        }

        #endregion
        
        private void Initialize() {
            this.Clickable = true;
            
            this.Click += OnClick;
        }

        private void OnClick(object sender, EventArgs e) {
            new ColorPickerDialogFragment(this, this.ItemsSource, this.SelectedItem).Show(
                ((AppCompatActivity)this.Context).SupportFragmentManager,
                "color"
            );
        }

        public override bool Pressed {
            get { return base.Pressed; }
            set {
                base.Pressed = value;
                
                this.Invalidate();
            }
        }

        protected override void OnDraw(Canvas canvas) {
            base.OnDraw(canvas);

            var size = Math.Min(canvas.Width - (PaddingLeft + PaddingRight), canvas.Height - (PaddingTop + PaddingBottom));
            var radius = size/2f;

            var color = this.SelectedItem != null ? this.SelectedItem.Color.ToNativeColor() : Color.Transparent;

            if (this.Pressed) {
                color.A = 255/2;
            }

            canvas.DrawCircle(PaddingLeft + radius, PaddingTop + radius, radius, new Paint {
                AntiAlias = true,
                Color = color
            });
        }

        #region Implementation of IOnColorSetListener

        public void OnColorSet(UserColor color) {
            this.SelectedItem = color;

            var handler = SelectedItemChanged;

            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        #endregion

        public class SelectedValueBinding : MvxAndroidTargetBinding {
            public const string Name = "SelectedValue";

            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<ColorPicker>("SelectedValue", colorPicker => new SelectedValueBinding(colorPicker));
            }

            private new ColorPicker Target {
                get { return (ColorPicker)base.Target; }
            }

            public SelectedValueBinding(ColorPicker target) : base(target) { }

            public override void SubscribeToEvents() {
                this.Target.SelectedItemChanged += TargetOnValueChanged;
            }

            protected override void SetValueImpl(object target, object value) {
                ((ColorPicker)target).SelectedValue = (string)value;
            }

            public override Type TargetType {
                get { return typeof(string); }
            }

            public override MvxBindingMode DefaultMode {
                get { return MvxBindingMode.TwoWay; }
            }
            protected override void Dispose(bool isDisposing) {
                if (isDisposing) {
                    this.Target.SelectedItemChanged -= TargetOnValueChanged;
                }

                base.Dispose(isDisposing);
            }

            private void TargetOnValueChanged(object sender, EventArgs e) {
                if (this.Target == null) return;

                this.FireValueChanged(this.Target.SelectedValue);
            }
        }
    }
}