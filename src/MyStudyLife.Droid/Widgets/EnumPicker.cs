using System;
using System.Linq;
using AFollestad.MaterialDialogs;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using Java.Lang;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Data.Annotations;
using Enum = System.Enum;
using AndroidX.AppCompat.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.EnumPicker")]
    public class EnumPicker : Picker, MaterialDialog.IListCallback {
        public event EventHandler ValueChanged;

        private string[] _valueTitles;

        private Type _valueType;
        private int _value;

        public Type ValueType {
            get => _valueType;
            set {
                _valueType = value;

                _valueTitles = Enum.GetValues(ValueType).OfType<Enum>().Select(x => x.GetTitle()).ToArray();

                UpdateText();
            }
        }

        public int Value {
            get => _value;
            set {
                _value = value;

                UpdateText();
            }
        }

        public string FormatText { get; set; }

        public bool LowercaseValue { get; set; }

        public PickerMode Mode { get; set; }

        protected EnumPicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public EnumPicker(Context context) : this(context, null) { }
        public EnumPicker(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.pickerStyle) { }
        public EnumPicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.EnumPicker);

            FormatText = a.GetText(Resource.Styleable.EnumPicker_formatText);
            LowercaseValue = a.GetBoolean(Resource.Styleable.EnumPicker_lowercaseValue, false);

            a.Recycle();
        }

        private void UpdateText() {

            if (_valueTitles != null) {
                string valueStr =_valueTitles[Value];

                if (LowercaseValue) {
                    valueStr = valueStr.ToLower();
                }

                Text = !System.String.IsNullOrEmpty(FormatText) ? System.String.Format(FormatText, valueStr) : valueStr;
            }
            else {
                Text = null;
            }
        }

        protected override void OnClick() {
            if (Mode == PickerMode.Dialog) {
                new MaterialDialog.Builder(Context)
                    .Items(_valueTitles)
                    .ItemsCallbackSingleChoice(Value, (dialog, view, position, text) => {
                        SetValue(position);
                        return true;
                    })
                    .Show();
            }
            else {
                var popup = new MslPopupMenu(Context, this);

                for (int i = 0; i < _valueTitles.Length; i++) {
                    popup.Menu.Add(0, 0, i, _valueTitles[i]);
                }

                popup.MenuItemClick += PopupOnMenuItemClick;

                popup.Show();
            }
        }

        private void PopupOnMenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e) {
            SetValue(e.Item.Order);
        }

        public void OnSelection(MaterialDialog dialog, View view, int which, ICharSequence text) {
            dialog.Dismiss();

            SetValue(which);
        }

        private void SetValue(int value) {
            Value = value;

            var handler = ValueChanged;
            handler?.Invoke(this, EventArgs.Empty);
        }

        public class TargetBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<EnumPicker>("Value", picker => new TargetBinding(picker));
            }

            public EnumPicker Picker => (EnumPicker) Target;

            public override Type TargetType => typeof(object);

            public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

            public TargetBinding(EnumPicker target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                var picker = (EnumPicker) target;

                if (value == null) {
                    return;
                }

                if (picker.ValueType == null) {
                    picker.ValueType = value.GetType();
                }

                picker.Value = (int) value;
            }

            public override void SubscribeToEvents() {
                Picker.ValueChanged += PickerOnValueChanged;
            }

            private void PickerOnValueChanged(object sender, EventArgs e) {
                if (Target == null) return;

                FireValueChanged(((EnumPicker) sender).Value);
            }

            protected override void Dispose(bool isDisposing) {
                if (isDisposing) {
                    Picker.ValueChanged -= PickerOnValueChanged;
                }
                
                base.Dispose(isDisposing);
            }
        }
    }

    public enum PickerMode {
        Popup,
        Dialog
    }
}