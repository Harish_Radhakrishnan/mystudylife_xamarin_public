using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.Core.Content;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.LonelyView")]
    public sealed class LonelyView : LinearLayout {
        private readonly Icon _iconView;
        private readonly Color _textColor;
        private readonly Color _iconColor;
        private readonly TextView _textView;
        private readonly TextView _subTextView;

        private string _text;
        private string _subText;

        public Glyph Glyph {
            get { return _iconView.Glyph; }
            set {
                _iconView.Glyph = value;
            }
        }

        public string Text {
            get { return _text; }
            set {
                _text = value;

                if (_textView != null) {
                    _textView.Text = value;
                }
            }
        }

        public string SubText {
            get { return _subText; }
            set {
                _subText = value;

                if (_subTextView != null) {
                    _subTextView.Text = value;
                }
            }
        }

        private LonelyView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
        public LonelyView(Context context) : this(context, null) {}
        public LonelyView(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public LonelyView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.LonelyView);

            _text = a.GetText(Resource.Styleable.LonelyView_text);
            _subText = a.GetText(Resource.Styleable.LonelyView_subText);

            var isSmall = a.GetInt(Resource.Styleable.LonelyView_size, 1) == 0;
            var margin = Resources.GetDimensionPixelSize(Resource.Dimension.margin_s);

            _textColor = a.GetColor(Resource.Styleable.LonelyView_textColor, ContextCompat.GetColor(context, Resource.Color.foreground_light));
            _iconColor = a.GetColor(Resource.Styleable.LonelyView_iconColor, ContextCompat.GetColor(context, Resource.Color.foreground_light));

            a.Recycle();

            this.Orientation = Orientation.Vertical;
            this.SetPadding(margin, margin, margin, margin);
            
            _iconView = new Icon(context, attrs);
            _iconView.SetTextColor(_iconColor);
            _iconView.SetTextSize(ComplexUnitType.Sp, isSmall ? 64 : 96);

            _textView = new RobotoTextView(context) {
                Text = _text,
                Gravity = GravityFlags.CenterHorizontal
            };
            _textView.SetPadding(0, margin, 0, 0);
            _textView.SetTextColor(_textColor);
            _textView.SetTextSize(ComplexUnitType.Px, Resources.GetDimension(isSmall ? Resource.Dimension.text_body : Resource.Dimension.text_subhead));

            _subTextView = new RobotoTextView(context) {
                Text = _subText,
                Gravity = GravityFlags.CenterHorizontal
            };
            _subTextView.SetPadding(0, 0, 0, margin);
            _subTextView.SetTextColor(_textColor);
            _subTextView.SetTextSize(ComplexUnitType.Px, Resources.GetDimension(isSmall ? Resource.Dimension.text_caption : Resource.Dimension.text_body));

            AddView(_iconView, new LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent) {
                Gravity = GravityFlags.CenterHorizontal
            });
            AddView(_textView, new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent));
            AddView(_subTextView, new LayoutParams(LayoutParams.MatchParent, LayoutParams.WrapContent));
        }

        public void SetText(int resId) {
            this.Text = Resources.GetText(resId);
        }

        public class TextTargetBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<LonelyView>("Text", view => new TextTargetBinding(view));
            }

            public override Type TargetType {
                get { return typeof (string); }
            }

            public override MvxBindingMode DefaultMode {
                get { return MvxBindingMode.OneWay; }
            }

            public TextTargetBinding(LonelyView target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                var textView = target as LonelyView;

                if (textView == null) {
                    return;
                }

                textView.Text = value as string;
            }
        }

        public class SubTextTargetBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<LonelyView>("SubText", view => new SubTextTargetBinding(view));
            }

            public override Type TargetType {
                get { return typeof(string); }
            }

            public override MvxBindingMode DefaultMode {
                get { return MvxBindingMode.OneWay; }
            }

            public SubTextTargetBinding(LonelyView target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                var textView = target as LonelyView;

                if (textView == null) {
                    return;
                }

                textView.SubText = value as string;
            }
        }

        public class GlyphTargetBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<LonelyView>("Glyph", view => new GlyphTargetBinding(view));
            }

            public override Type TargetType => typeof(Glyph);

            public GlyphTargetBinding(LonelyView target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                var lonelyView = target as LonelyView;
                var glyph = value as Glyph?;
                if (lonelyView != null && glyph != null) {
                    lonelyView.Glyph = (Glyph)glyph;
                }
            }
        }
    }
}