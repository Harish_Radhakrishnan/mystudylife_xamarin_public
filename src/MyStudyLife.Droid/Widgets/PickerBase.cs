using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.Picker")]
    public class Picker : Button {
        protected Picker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public Picker(Context context) : this(context, null) { }
        public Picker(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.pickerStyle) { }
        public Picker(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            this.Click += (s, e) => OnClick();
        }

        protected virtual void OnClick() { }
    }
}