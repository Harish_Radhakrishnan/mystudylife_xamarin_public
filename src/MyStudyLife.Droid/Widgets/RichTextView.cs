using System;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Text;
using Android.Text.Method;
using Android.Text.Style;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
    // Crap name, change it if you can think of something better
    // http://stackoverflow.com/questions/8558732/listview-textview-with-linkmovementmethod-makes-list-item-unclickable
    [Register("mystudylife.droid.widgets.RichTextView")]
    public class RichTextView : RobotoTextView {
        public event EventHandler<LocalLinkEventArgs> LinkClick;

        public RichTextView(Context context) : this(context, null) { }
        public RichTextView(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public RichTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            this.MovementMethod = LocalLinkMovementMethod.Instance;
        }

        class LocalLinkMovementMethod : LinkMovementMethod {
            private static LocalLinkMovementMethod _instance;

            public new static LocalLinkMovementMethod Instance {
                get { return _instance ?? (_instance = new LocalLinkMovementMethod()); }
            }

            public override bool OnTouchEvent(TextView widget, ISpannable buffer, MotionEvent e) {
                MotionEventActions action = e.Action;

                if (action == MotionEventActions.Up || action == MotionEventActions.Down) {
                    int x = (int)e.GetX();
                    int y = (int)e.GetY();

                    x -= widget.PaddingLeft;
                    y -= widget.PaddingTop;

                    x += widget.ScrollX;
                    y += widget.ScrollY;

                    Layout layout = widget.Layout;

                    // This shouldn't happen, but it seems to sometimes :S
                    if (layout == null) {
                        Log.Error("LocalLinkMovementMethod", "TextView.Layout is null, ignoring click.");
                        return false;
                    }

                    int line = layout.GetLineForVertical(y);
                    int off = layout.GetOffsetForHorizontal(line, x);

                    var link = buffer.GetSpans(off, off, Java.Lang.Class.FromType(typeof(ClickableSpan))).Select(s => s.JavaCast<ClickableSpan>()).ToArray();

                    if (link.Length > 0) {
                        if (action == MotionEventActions.Up) {
                            var rtv = widget as RichTextView;

                            if (rtv != null) {
                                var handler = rtv.LinkClick;

                                if (handler != null) {
                                    var args = new LocalLinkEventArgs(link);

                                    handler(widget, args);

                                    if (args.Handled) {
                                        return true;
                                    }
                                }
                            }

                            link[0].OnClick(widget);
                        }
                        else if (action == MotionEventActions.Down) {
                            Selection.SetSelection(buffer, buffer.GetSpanStart(link[0]), buffer.GetSpanEnd(link[0]));
                        }

                        return true;
                    }

                    Selection.RemoveSelection(buffer);
                    base.OnTouchEvent(widget, buffer, e);
                    return false;
                }

                return base.OnTouchEvent(widget, buffer, e);
            }
        }
    }

    public class LocalLinkEventArgs : EventArgs {
        public ClickableSpan[] ClickedSpans { get; private set; }

        public bool Handled { get; set; }

        public LocalLinkEventArgs(ClickableSpan[] spans) {
            ClickedSpans = spans;
        }
    }
}