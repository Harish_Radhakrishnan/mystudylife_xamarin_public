using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
	/// <summary>
	///		A view which ensures the height matches the width.
	/// </summary>
	[Register("mystudylife.droid.widgets.SquareView")]
	public class SquareView : LinearLayout {
		protected SquareView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
		public SquareView(Context context) : base(context) { }
		public SquareView(Context context, IAttributeSet attrs) : base(context, attrs) { }
		public SquareView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

		#region Overrides of View

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

			SetMeasuredDimension(MeasuredWidth, MeasuredWidth);
		}

		#endregion
	}
}