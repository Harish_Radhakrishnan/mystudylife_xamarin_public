using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.IconInputLayout")]
    public sealed class IconInputLayout : FrameLayout {
        private readonly Icon _iconView;

        private string _icon;
        private bool _autoAlignChildren;

        public string Icon {
            get { return _icon; }
            set {
                _icon = value;
                _iconView.Text = value;
            }
        }

        public bool AutoAlignChildren {
            get { return _autoAlignChildren; }
        }
        
        public IconInputLayout(Context context) : this(context, null) { }
        public IconInputLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public IconInputLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, 0) {
            var styles = context.ObtainStyledAttributes(attrs, Resource.Styleable.IconInputLayout);
            var iconStyles = context.ObtainStyledAttributes(attrs, Resource.Styleable.Icon);

            _iconView = new Icon(context) {
                Gravity = GravityFlags.Left | GravityFlags.CenterVertical
            };

            var glyphIndex = iconStyles.GetInt(Resource.Styleable.Icon_glyph, -1);

            if (0 <= glyphIndex) {
                _iconView.Glyph = Widgets.Icon.GlyphIndexMapping[glyphIndex];
            }

            iconStyles.Recycle();

            _iconView.SetTextColor(ContextCompatEx.GetColor(_iconView.Context, Resource.Color.foreground_mid));
            _iconView.SetTextSize(ComplexUnitType.Px, Resources.GetDimensionPixelSize(Resource.Dimension.icon_size));
            _iconView.SetPadding(Resources.GetDimensionPixelSize(Resource.Dimension.margin), 0, 0, 0);

            _autoAlignChildren = styles.GetBoolean(Resource.Styleable.IconInputLayout_input_autoAlignChildren, true);

            styles.Recycle();

            AddView(_iconView, ViewGroup.LayoutParams.WrapContent, Resources.GetDimensionPixelSize(Resource.Dimension.input_height));
        }

        public override void AddView(View child, int index, ViewGroup.LayoutParams @params) {
            if (_autoAlignChildren && !(child is Icon)) {
                @params = new LayoutParams(@params) {
                    LeftMargin = Resources.GetDimensionPixelSize(Resource.Dimension.toolbar_margin_start)
                };
            }

            base.AddView(child, index, @params);
        }
    }
}