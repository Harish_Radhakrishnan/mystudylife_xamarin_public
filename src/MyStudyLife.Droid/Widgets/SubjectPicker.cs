using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using AndroidX.AppCompat.App;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Droid.Fragments.Dialogs;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.SubjectPicker")]
    public class SubjectPicker : Button, IListPicker, SubjectPickerDialogFragment.IOnSubjectSetListener {

        public event EventHandler SelectedItemChanged;

        private Subject _selectedItem;
        private IDisposable _itemsSourceSubscription;
        private ObservableCollection<Subject> _itemsSource; 

        object IListPicker.SelectedItem {
            get { return SelectedItem; }
            set { SelectedItem = (Subject)value; }
        }

        IEnumerable IListPicker.ItemsSource {
            get { return ItemsSource; }
            set { ItemsSource = (ObservableCollection<Subject>)value; }
        }

        public Subject SelectedItem {
            get { return _selectedItem; }
            set {
                if (!ReferenceEquals(_selectedItem, value)) {
                    _selectedItem = value;

                    SetButtonText();
                }
            }
        }

        public ObservableCollection<Subject> ItemsSource {
            get { return _itemsSource; }
            set {
                if (_itemsSourceSubscription != null) {
                    _itemsSourceSubscription.Dispose();
                    _itemsSourceSubscription = null;
                }

                if (_itemsSource != value) {
                    _itemsSource = value;

                    SetButtonText();
                }

                if (_itemsSource != null) {
                    _itemsSourceSubscription = _itemsSource.WeakSubscribe(ItemsSourceOnCollectionChanged);
                }
            }
        }

        private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            SetButtonText();
        }

        #region Constructors

        protected SubjectPicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public SubjectPicker(Context context) : this(context, null) { }
        public SubjectPicker(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public SubjectPicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            this.Click += OnClick;
        }

        #endregion

        private void OnClick(object sender, EventArgs e) {
            new SubjectPickerDialogFragment(this.Context, this, this.ItemsSource, this.SelectedItem).Show(
                ((AppCompatActivity) this.Context).SupportFragmentManager,
                "subject"
            );
        }

        private void SetButtonText() {
            this.Text = this.SelectedItem?.Name ?? String.Empty;
        }

        #region Implementation of IOnSubjectSetListener

        public void OnSubjectSet(Subject subject) {
            this.SelectedItem = subject;

            SelectedItemChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}