using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using MyStudyLife.Droid.Common;
using Android.Util;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.UI.Utilities;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.MultiLengthTextView")]
    public class MultiLengthTextView : RobotoTextView {
        public override int MaxLines {
            get {
                if (OS.CheckSystemVersion(BuildVersionCodes.JellyBean)) {
                    return base.MaxLines;
                }

                try {
                    var @class = Java.Lang.Class.FromType(typeof(TextView));

                    var maximumField = @class.GetDeclaredField("mMaximum");
                    var maxModeField = @class.GetDeclaredField("mMaxMode");

                    if (maximumField != null && maxModeField != null) {
                        maxModeField.Accessible = true;

                        if (maxModeField.GetInt(this) == 1 /* 1 == Lines */) {
                            maximumField.Accessible = true;

                            return maximumField.GetInt(this);
                        }
                    }
                }
                catch { }

                return int.MaxValue;
            }
        }

        private MultiLengthText _multiLengthText;
        public MultiLengthText MultiLengthText {
            get { return _multiLengthText; }
            set {
                if (!ReferenceEquals(_multiLengthText, value)) {
                    _multiLengthText = value;
                    this.SetMultiLengthText();
                }
            }
        }

        protected MultiLengthTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public MultiLengthTextView(Context context) : this(context, null) { }
        public MultiLengthTextView(Context context, IAttributeSet attrs) : this(context, attrs, Android.Resource.Attribute.TextViewStyle) { }
        public MultiLengthTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

            this.SetMultiLengthText();

            // View requires remeasuring as it may have wrapped before
            if (this.MaxLines != 1) {
                base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            }
        }

        private void SetMultiLengthText() {
            var mlt = this.MultiLengthText;
            string text = null;

            if (mlt != null) {
                var textViewWidth = this.MeasuredWidth - (this.PaddingLeft + this.PaddingRight);
                var textPaint = this.Paint;
                float textWidth;

                for (int i = 0; i < 3; i++) {
                    switch (i) {
                        case 0:
                            text = mlt.Full;
                            break;
                        case 1:
                            if (mlt.Abbreviated != null) {
                                text = mlt.Abbreviated;
                            }
                            break;
                        case 2:
                            if (mlt.Shortest != null) {
                                text = mlt.Shortest;
                            }
                            break;
                        default:
                            throw new Exception();
                    }

                    textWidth = textPaint.MeasureText(text);

                    // If it fits, break
                    if (0 < textViewWidth && textWidth < textViewWidth) {
                        break;
                    }
                }
            }

            this.Text = text;
        }

        public class Binding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<MultiLengthTextView>("MultiLengthText", view => new Binding(view));
            }

            public override Type TargetType => typeof(MultiLengthText);

            public Binding(MultiLengthTextView textView) : base(textView) { }

            protected override void SetValueImpl(object target, object value) {
                var textView = target as MultiLengthTextView;

                if (textView != null) {
                    textView.MultiLengthText = value as MultiLengthText;
                }
            }
        }
    }
}