using System;
using System.Linq;
using AFollestad.MaterialDialogs;
using Android.Content;
using Android.Runtime;
using Android.Util;
using MvvmCross.Binding;
using Java.Lang;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Globalization;
using String = System.String;
using JInteger = Java.Lang.Integer;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.DaysPicker")]
	public class DaysPicker : Picker, MaterialDialog.IListCallbackMultiChoice {
		public event EventHandler ValueChanged;

		private DaysOfWeek? _value;

		public DaysOfWeek? Value {
			get { return _value; }
			set {
				_value = value;

				UpdateText();
			}
		}

        public int MinDays { get; set; }

		#region Constructors

		protected DaysPicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public DaysPicker(Context context) : this(context, null) { }
		public DaysPicker(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.pickerStyle) { }
        public DaysPicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.DaysPicker);

            MinDays = a.GetInt(Resource.Styleable.DaysPicker_minDays, 0);

            if (MinDays == 0 && a.GetBoolean(Resource.Styleable.DaysPicker_required, true)) {
                MinDays = 1;
		}

            a.Recycle();
		}

		#endregion

        protected override void OnClick() {
            var days = DateTimeFormat.Days.ToList();

            MaterialDialog dialog = null;

            var builder = new MaterialDialog.Builder(this.Context)
                .Title(Resource.String.Days)
                .Items(days.Select(x => x.Name).ToArray())
                .AlwaysCallMultiChoiceCallback()
                .ItemsCallbackMultiChoice(
                    days.Where(x => this.Value.HasValue && this.Value.Value.HasFlag(x.DayOfWeek.ToFlag())).Select(x => new JInteger(L10n.GetDayIndex(x.DayOfWeek))).ToArray(),
                    this
                )
                .Callback(new DialogHelper.ButtonCallbackShim(
                    onPositive: () => {
                        // ReSharper disable AccessToModifiedClosure
                        if (dialog == null) {
                            return;
		}

                        DaysOfWeek value;

                        var positions = dialog.GetSelectedIndices();

	                    if (positions.Length > 0) {
                            var daysOfWeek = dialog.GetSelectedIndices().Select(x => L10n.GetDayFromIndex(x.IntValue()).ToFlag());

	                        value = daysOfWeek.Aggregate((x, y) => x | y);
	                    }
	                    else {
	                        value = DaysOfWeek.None;
                        }

                        if (value != Value) {
                            Value = value;

                            var handler = ValueChanged;
                            if (handler != null) {
                                handler(this, EventArgs.Empty);
                            }
                        }
                        // ReSharper restore AccessToModifiedClosure
		}
                ))
                .PositiveText(Resource.String.Common_Ok)
                .NegativeText(Resource.String.Common_Cancel);

            dialog = builder.Build();

            UpdateEnabled(dialog, dialog.GetSelectedIndices());

            dialog.Show();
	    }

		private void UpdateText() {
		    if (this.Value.HasValue && this.Value.Value > 0) {
		        var days = DateTimeFormat.ShortDays;

		        this.Text = String.Join(", ", days.Where(x => this.Value.Value.HasFlag(x.DayOfWeek.ToFlag())).Select(x => x.Name));
			}
			else {
		        this.Text = null;
			}
		}

        private void UpdateEnabled(MaterialDialog dialog, Integer[] positions) {
            dialog.GetActionButton(DialogAction.Positive).Enabled = MinDays <= positions.Length;
        }

	    public bool OnSelection(MaterialDialog dialog, Integer[] positions, ICharSequence[] items) {
	        UpdateEnabled(dialog, positions);

            return true;
		}
	}

	public class DaysPickerValueBinding : MvxAndroidTargetBinding {
		public const string Name = "Value";

		public new DaysPicker Target {
			get { return (DaysPicker) base.Target; }
		}

		public DaysPickerValueBinding(DaysPicker target)
			: base(target) {

		}

	    #region Overrides of MvxTargetBinding

        public override void SubscribeToEvents() {
            this.Target.ValueChanged += TargetOnValueChanged;
	    }

	    #endregion

	    #region Overrides of MvxConvertingTargetBinding

	    protected override void SetValueImpl(object target, object value) {
            ((DaysPicker)target) .Value = (DaysOfWeek?)value;
	    }

	    #endregion

	    public override Type TargetType {
			get { return typeof(DaysOfWeek?); }
		}

		public override MvxBindingMode DefaultMode {
			get { return MvxBindingMode.TwoWay; }
		}

		#region Overrides of MvxBinding

		protected override void Dispose(bool isDisposing) {
			if (isDisposing) {
				this.Target.ValueChanged -= TargetOnValueChanged;
			}

			base.Dispose(isDisposing);
		}

		private void TargetOnValueChanged(object sender, EventArgs e) {
			if (this.Target == null) return;

			this.FireValueChanged(this.Target.Value);
		}

		#endregion
	}
}