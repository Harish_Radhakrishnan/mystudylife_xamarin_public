using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.Icon")]
	public sealed class Icon : TextView {
        // Enum.GetValues doesn't keep hardcoded order
        public static readonly Glyph[] GlyphIndexMapping = {
            Glyph.Dashboard,
            Glyph.Calendar,
            Glyph.Schedule,
            Glyph.Subject,
            Glyph.Class,
            Glyph.Task,
            Glyph.Exam,
            Glyph.Search,
            Glyph.Settings, //8

            Glyph.Delete,
            Glyph.Time,
            Glyph.Location,
            Glyph.Seat,
            Glyph.Teacher,
            Glyph.Attention,
            Glyph.Tick,
            Glyph.Progress,
            Glyph.Student,
            Glyph.School,
            Glyph.Holiday, //19

            Glyph.Pushes,
            Glyph.Notification,

            Glyph.Today,
            Glyph.Tickbox,
            Glyph.Stopwatch,
            Glyph.Sync,
            Glyph.Left
        };

		private Typeface _iconTypeface;
        private Glyph _glyph;

		private Typeface IconTypeface {
			get { return _iconTypeface ?? (_iconTypeface = Typeface.CreateFromAsset(this.Context.Assets, "fonts/MslIcons.ttf")); }
		}

        public Glyph Glyph {
            get { return _glyph; }
            set {
                if (_glyph != value) {
                    _glyph = value;

                    this.Text = Char.ConvertFromUtf32((int)value);
                }
            }
        }

        private Icon(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
		public Icon(Context context) : this(context, null) { }
		public Icon(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
		public Icon(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            this.SetTypeface(IconTypeface, TypefaceStyle.Normal);

            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.Icon);

            var glyphIndex = a.GetInt(Resource.Styleable.Icon_glyph, -1);

		    if (0 <= glyphIndex) {
                this.Glyph = GlyphIndexMapping[glyphIndex];
		    }

            a.Recycle();
		}
	}
}