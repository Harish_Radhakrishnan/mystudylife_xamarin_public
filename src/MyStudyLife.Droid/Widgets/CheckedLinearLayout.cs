using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views.Accessibility;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.CheckedLinearLayout")]
    public class CheckedLinearLayout : LinearLayout, ICheckable {

        private static readonly int[] CheckedStateSet = { Android.Resource.Attribute.StateChecked };

        private bool _checked;

        public bool Checked {
            get { return _checked; }
            set {
                if (_checked != value) {
                    _checked = value;

                    this.RefreshDrawableState();
                }
            }
        }

        protected CheckedLinearLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
        public CheckedLinearLayout(Context context) : base(context) {}
        public CheckedLinearLayout(Context context, IAttributeSet attrs) : base(context, attrs) {}
        public CheckedLinearLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {}

        public void Toggle() {
            this.Checked = !this.Checked;
        }

        protected override int[] OnCreateDrawableState(int extraSpace) {
            var drawableState = base.OnCreateDrawableState(extraSpace + 1);

            if (this.Checked) {
                MergeDrawableStates(drawableState, CheckedStateSet);
            }

            return drawableState;
        }

        public override bool DispatchPopulateAccessibilityEvent(AccessibilityEvent e) {
            var populated = base.DispatchPopulateAccessibilityEvent(e);

            if (!populated) {
                e.Checked = _checked;
            }

            return populated;
        }
    }
}