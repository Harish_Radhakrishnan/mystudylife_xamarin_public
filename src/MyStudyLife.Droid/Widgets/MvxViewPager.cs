using System;
using System.Collections;
using System.Collections.Specialized;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding;
using MvvmCross.Binding.Attributes;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Extensions;
using MvvmCross.Exceptions;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.WeakSubscription;
using Exception = System.Exception;
using JavaObject = Java.Lang.Object;

namespace MyStudyLife.Droid.Widgets
{
    // Inspired by http://slodge.blogspot.co.uk/2013/02/binding-to-androids-horizontal-pager.html
    // and existing MvvmCross stuff.
    public class MvxViewPager : AndroidX.ViewPager.Widget.ViewPager {
        public new MvxDynamicPagerAdapter Adapter {
            get { return base.Adapter as MvxDynamicPagerAdapter; }
            set {
                var existing = Adapter;
                if (existing == value)
                    return;

                if (existing != null && value != null) {
                    value.ItemsSource = existing.ItemsSource;
                    value.ItemTemplateId = existing.ItemTemplateId;
                }

                base.Adapter = value;
            }
        }

        [MvxSetToNullAfterBinding]
        public IEnumerable ItemsSource {
            get { return Adapter.ItemsSource; }
            set { Adapter.ItemsSource = value; }
        }

        public object SelectedItem {
            get { return Adapter.GetRawItem(this.CurrentItem); }
            set {
                // POST IS REQUIRED, do not remove!
                this.Post(() => this.SetSelectedItem(value));
            }
        }

        protected virtual void SetSelectedItem(object item) {
            this.SetCurrentItem(Adapter.GetPosition(item), true);
        }

        public int ItemTemplateId {
            get { return Adapter.ItemTemplateId; }
            set { Adapter.ItemTemplateId = value; }
        }

        protected MvxViewPager(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public MvxViewPager(Context context) : base(context) {
            this.Adapter = new MvxDynamicPagerAdapter(context);

            this.Initialize();
        }

        public MvxViewPager(Context context, IAttributeSet attrs) : base(context, attrs) {
            var itemTemplateId = MvxAttributeHelpers.ReadListItemTemplateId(context, attrs);

            this.Adapter = new MvxDynamicPagerAdapter(context) {
                ItemTemplateId = itemTemplateId
            };

            this.Initialize();
        }

        private void Initialize() {
            this.PageSelected += OnPageSelected;
        }

        private void OnPageSelected(object sender, PageSelectedEventArgs e) {
            var itemView = this.GetCurrentItemView();

            itemView?.OnSelected();
        }

        public MvxViewPagerItem GetCurrentItemView() {
            var selectedItem = this.SelectedItem;

            if (selectedItem == null) {
                return null;
            }

            return this.FindViewWithTag(this.Adapter.GetItemId(selectedItem)) as MvxViewPagerItem;
        }
    }

    public class MvxDynamicPagerAdapter : AndroidX.ViewPager.Widget.PagerAdapter {
        private readonly Context _context;
        private readonly IMvxAndroidBindingContext _bindingContext;

        protected Context Context {
            get { return _context; }
        }
        protected IMvxAndroidBindingContext BindingContext {
            get { return _bindingContext; }
        }

        private IEnumerable _itemsSource;
        private int _itemTemplateId;
        private IDisposable _subscription;

        [MvxSetToNullAfterBinding]
        public IEnumerable ItemsSource {
            get { return _itemsSource; }
            set { SetItemsSource(value); }
        }

        public virtual int ItemTemplateId {
            get { return _itemTemplateId; }
            set {
                if (_itemTemplateId == value) {
                    return;
                }

                _itemTemplateId = value;

                if (_itemsSource != null) {
                    NotifyDataSetChanged();
                }
            }
        }
        protected void SetItemsSource(IEnumerable value) {
            if (ReferenceEquals(_itemsSource, value)) {
                return;
            }

            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            _itemsSource = value;

            if (_itemsSource != null && !(_itemsSource is IList)) {
                Mvx.IoCProvider.Resolve<ILogger<MvxDynamicPagerAdapter>>().LogWarning(
                    "Binding to IEnumerable rather than IList - this can be inefficient, especially for large lists"
                );
            }

            if (_itemsSource is INotifyCollectionChanged newObservable) {
                _subscription = newObservable.WeakSubscribe(OnItemsSourceCollectionChanged);
            }

            NotifyDataSetChanged();
        }

        private void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            // Found here: https://github.com/toggl/mobile/blob/master/Joey/UI/Adapters/BaseDataViewAdapter.cs#L43

            // Need to access the Handle property, else mono optimises/loses the context and we get a weird
            // low-level exception about "'jobject' must not be IntPtr.Zero".
            if (Handle == IntPtr.Zero) {
                return;
            }
            NotifyDataSetChanged(e);
        }

        public virtual void NotifyDataSetChanged(NotifyCollectionChangedEventArgs e) {
            RealNotifyDataSetChanged();
        }

        public override void NotifyDataSetChanged() {
            RealNotifyDataSetChanged();
        }

        protected virtual void RealNotifyDataSetChanged() {
            try {
                base.NotifyDataSetChanged();
            }
            catch (Exception exception) {
                Mvx.IoCProvider.Resolve<ILogger<MvxDynamicPagerAdapter>>().LogWarning("Exception masked during PagerAdapter RealNotifyDataSetChanged {0}", exception.ToLongString());
            }
        }

        protected MvxDynamicPagerAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public MvxDynamicPagerAdapter(Context context) : this(context, MvxAndroidBindingContextHelpers.Current()) { }

        public MvxDynamicPagerAdapter(Context context, IMvxAndroidBindingContext bindingContext) {
            _context = context;
            _bindingContext = bindingContext;
        }

        public override bool IsViewFromObject(View view, JavaObject @object) {
            return view == @object;
        }

        public override int Count {
            get { return _itemsSource.Count(); }
        }

        public override int GetItemPosition(JavaObject @object) {
            // This enables backwards paging (eg. inserting pages from the view model
            // at 0 index).
            return this.GetPosition(((MvxViewPagerItem)@object).DataContext);
        }

        public virtual int GetPosition(object item) {
            return _itemsSource.GetPosition(item);
        }

        public object GetRawItem(int position) {
            return _itemsSource.ElementAt(position);
        }

        protected virtual MvxViewPagerItem GetView(int position, object item) {
            return new MvxViewPagerItem(_context, _bindingContext.LayoutInflaterHolder, item, this.ItemTemplateId);
        }

        public virtual JavaObject GetItemId(object item) {
            return item.GetHashCode();
        }

        public override JavaObject InstantiateItem(ViewGroup container, int position) {
            var item = GetRawItem(position);
            var view = GetView(position, item);

            view.Tag = GetItemId(item);

            container.AddView(view);

            return view;
        }

        public override void DestroyItem(ViewGroup container, int position, JavaObject @object) {
            var view = (MvxViewPagerItem)@object;

            container.RemoveView(view);

            view.Dispose();
        }
    }

    public class MvxViewPagerItem : FrameLayout, IMvxBindingContextOwner {
        // Majority of ideas come from mvxBaseListItemView

        private readonly IMvxAndroidBindingContext _bindingContext;

        public IMvxAndroidBindingContext AndroidBindingContext {
            get { return _bindingContext; }
        }

        public IMvxBindingContext BindingContext {
            get { return _bindingContext; }
            set { throw new NotImplementedException("BindingContext is readonly in the list item"); }
        }

        public object DataContext {
            get { return _bindingContext.DataContext; }
            set {
                if (_isAttachedToWindow) {
                    _bindingContext.DataContext = value;
                }
                else {
                    _cachedDataContext = value;

                    if (_bindingContext.DataContext != null) {
                        _bindingContext.DataContext = null;
                    }
                }
            }
        }

        protected MvxViewPagerItem(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public MvxViewPagerItem(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext, int templateId)
            : base(context) {
            _bindingContext = new MvxAndroidBindingContext(context, layoutInflater, dataContext);

            if (templateId > 0) {
                AndroidBindingContext.BindingInflate(templateId, this);
            }
        }

        private object _cachedDataContext;
        private bool _isAttachedToWindow;

        protected override void OnAttachedToWindow() {
            base.OnAttachedToWindow();
            _isAttachedToWindow = true;
            if (_cachedDataContext != null
                && DataContext == null) {
                DataContext = _cachedDataContext;
            }
        }

        protected override void OnDetachedFromWindow() {
            _cachedDataContext = DataContext;
            DataContext = null;
            base.OnDetachedFromWindow();
            _isAttachedToWindow = false;
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                this.ClearAllBindings();
                _cachedDataContext = null;
            }

            base.Dispose(disposing);
        }

        public virtual void OnSelected() {

        }
    }

    public class MvxViewPagerSelecteditemBinding : MvxAndroidTargetBinding {
        public const string Name = "SelectedItem";

        public new MvxViewPager Target {
            get { return (MvxViewPager)base.Target; }
        }

        public MvxViewPagerSelecteditemBinding(MvxViewPager target) : base(target) { }

        public override void SubscribeToEvents() {
            this.Target.PageSelected += TargetOnPageSelected;
            this.Target.PageScrollStateChanged += TargetOnPageScrollStateChanged;
        }

        protected override void SetValueImpl(object target, object value) {
            ((MvxViewPager)target).SelectedItem = value;
        }

        public override Type TargetType {
            get { return typeof(object); }
        }

        public override MvxBindingMode DefaultMode {
            get { return MvxBindingMode.TwoWay; }
        }

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                this.Target.PageSelected -= TargetOnPageSelected;
            }

            base.Dispose(isDisposing);
        }

        private bool _pageSelected;

        private void TargetOnPageSelected(object sender, AndroidX.ViewPager.Widget.ViewPager.PageSelectedEventArgs e) {
            this._pageSelected = true;
        }

        private void TargetOnPageScrollStateChanged(object sender, AndroidX.ViewPager.Widget.ViewPager.PageScrollStateChangedEventArgs e) {
            if (this.Target == null) return;

            if (e.State == (int)ScrollState.Idle && this._pageSelected) {
                this._pageSelected = false;

                this.FireValueChanged(this.Target.SelectedItem);
            }
        }
    }
}