using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
    /// <summary>
    ///		Expandable list view that grows instead of scrolling.
    /// </summary>
    [Register("mystudylife.droid.widgets.NoScrollExpandableListView")]
    public class NoScrollExpandableListView : ExpandableListView {
        protected NoScrollExpandableListView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}

        public NoScrollExpandableListView(Context context) : base(context) {}

        public NoScrollExpandableListView(Context context, IAttributeSet attrs) : base(context, attrs) {}

        public NoScrollExpandableListView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {}

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            // Calculate entire height by providing a very large height hint.
            // View.MeasuredSizeMask represents the largest height possible.
            int expandSpec = MeasureSpec.MakeMeasureSpec(MeasuredSizeMask, MeasureSpecMode.AtMost);

            base.OnMeasure(widthMeasureSpec, expandSpec);

            LayoutParameters.Height = MeasuredHeight;
        }
    }
}