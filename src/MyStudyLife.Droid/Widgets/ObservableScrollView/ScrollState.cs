namespace MyStudyLife.Droid.Widgets.ObservableScrollView {
    public enum ScrollState {
        Stop,
        Up,
        Down
    }
}