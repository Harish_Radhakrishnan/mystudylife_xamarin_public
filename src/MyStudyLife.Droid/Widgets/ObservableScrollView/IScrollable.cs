using System;
using Android.Views;

namespace MyStudyLife.Droid.Widgets.ObservableScrollView {
    public interface IScrollable {
        event EventHandler<ScrollChangedEventArgs> ScrollChanged;
        
        event EventHandler<ScrollStateChangedEventArgs> ScrollStateChanged;

        /// <summary>
        ///     Scrolls vertically to the absolute Y.
        ///     Implemented classes are expected to scroll to the exact Y pixels from the top,
        ///     but it depends on the type of the widget.
        /// </summary>
        /// <param name="y">Vertical position to scroll to</param>
        void ScrollVerticallyTo(int y);

        /// <value>
        ///     The current Y of the scrollable view.
        /// </value>
        int CurrentScrollY { get; }
        
        /// <summary>
        ///     Sets a touch motion event delegation ViewGroup.
        ///     This is used to pass motion events back to parent view.
        ///     It's up to the implementation classes whether or not it works.
        /// </summary>
        /// <param name="viewGroup">ViewGroup object to dispatch motion events</param>
        void SetTouchInterceptionViewGroup(ViewGroup viewGroup);
    }

    public class ScrollStateChangedEventArgs : EventArgs {
        private readonly ScrollState _scrollState;

        public ScrollState ScrollState {
            get { return _scrollState; }
        }

        public ScrollStateChangedEventArgs(ScrollState scrollState) {
            _scrollState = scrollState;
        }
    }

    public class ScrollChangedEventArgs : EventArgs {
        private readonly int _scrollY;
        private readonly bool _firstScroll;
        private readonly bool _dragging;

        public int ScrollY {
            get { return _scrollY; }
        }

        public bool FirstScroll {
            get { return _firstScroll; }
        }

        public bool Dragging {
            get { return _dragging; }
        }

        public ScrollChangedEventArgs(int scrollY, bool firstScroll, bool dragging) {
            _scrollY = scrollY;
            _firstScroll = firstScroll;
            _dragging = dragging;
        }
    }
}