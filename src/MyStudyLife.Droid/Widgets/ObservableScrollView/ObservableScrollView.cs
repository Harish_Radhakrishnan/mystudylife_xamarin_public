using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.Core.View;
using AndroidX.ViewPager.Widget;
using Java.Interop;
using Java.Lang;
using MyStudyLife.UI.Annotations;
using Object = Java.Lang.Object;

namespace MyStudyLife.Droid.Widgets.ObservableScrollView {
    // PORT OF https://github.com/ksoichiro/Android-ObservableScrollView
    // ...with fling from https://github.com/SnowdogApps/Material-Flexible-Space-Header/.../ObservableScrollViewWithFling.java

    [Register("mystudylife.droid.widgets.observablescrollview.ObservableScrollView")]
    public class ObservableScrollView : ScrollView, IScrollable {
        public event EventHandler<ScrollChangedEventArgs> ScrollChanged;
        public event EventHandler<ScrollStateChangedEventArgs> ScrollStateChanged;

        public event EventHandler FlingStarted;
        public event EventHandler FlingStopped;

        // Fields that should be saved onSaveInstanceState
        private int _prevScrollY;
        private int _scrollY;

        // Fields that don't need to be saved onSaveInstanceState
        private ScrollState _scrollState;
        private bool _firstScroll;
        private bool _dragging;
        private bool _intercepted;
        private MotionEvent _prevMoveEvent;
        private ViewGroup _touchInterceptionViewGroup;
        private int _previousPosition;
        private bool _parentIsViewPager;

        public int CurrentScrollY => _scrollY;

        protected ObservableScrollView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public ObservableScrollView(Context context) : base(context) { }
        public ObservableScrollView(Context context, IAttributeSet attrs) : base(context, attrs) { }
        public ObservableScrollView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

        private void CheckFling() {
            int position = ScrollY;

            if (_previousPosition - position == 0) {
                FlingStopped?.Invoke(this, EventArgs.Empty);
            }
            else {
                _previousPosition = ScrollY;
                PostDelayed(CheckFling, 50); // 50ms
            }
        }

        public override void Fling(int velocityY) {
            base.Fling(velocityY);

            FlingStarted?.Invoke(this, EventArgs.Empty);

            Post(CheckFling);
        }

        protected override void OnScrollChanged(int l, int t, int oldl, int oldt) {
            base.OnScrollChanged(l, t, oldl, oldt);

            var scrollHandler = ScrollChanged;

            if (scrollHandler != null) {
                _scrollY = t;

                scrollHandler(this, new ScrollChangedEventArgs(t, _firstScroll, _dragging));

                if (_firstScroll) {
                    _firstScroll = false;
                }

                if (_prevScrollY < t) {
                    _scrollState = ScrollState.Up;
                }
                else if (t < _prevScrollY) {
                    _scrollState = ScrollState.Down;
                    //} else {
                    // Keep previous state while dragging.
                    // Never makes it STOP even if scrollY not changed.
                    // Before Android 4.4, onTouchEvent calls onScrollChanged directly for ACTION_MOVE,
                    // which makes mScrollState always STOP when onUpOrCancelMotionEvent is called.
                    // STOP state is now meaningless for ScrollView.
                }
                _prevScrollY = t;
            }
        }

        public override bool OnInterceptTouchEvent(MotionEvent ev) {
            var stateChangedHandler = ScrollStateChanged;

            if (stateChangedHandler != null) {
                switch (ev.ActionMasked) {
                    case MotionEventActions.Down:
                        // Whether or not motion events are consumed by children,
                        // flag initializations which are related to ACTION_DOWN events should be executed.
                        // Because if the ACTION_DOWN is consumed by children and only ACTION_MOVEs are
                        // passed to parent (this view), the flags will be invalid.
                        // Also, applications might implement initialization codes to onDownMotionEvent,
                        // so call it here.
                        _firstScroll = _dragging = true;
                        stateChangedHandler(this, new ScrollStateChangedEventArgs(ScrollState.Down));
                        break;
                }
            }

            return base.OnInterceptTouchEvent(ev);
        }

        public override bool OnTouchEvent(MotionEvent ev) {
            var scrollHandler = ScrollChanged;
            var stateChangedHandler = ScrollStateChanged;

            if (scrollHandler != null || stateChangedHandler != null) {
                switch (ev.ActionMasked) {
                    case MotionEventActions.Up:
                    case MotionEventActions.Cancel:
                        _intercepted = false;
                        _dragging = false;

                        stateChangedHandler?.Invoke(this, new ScrollStateChangedEventArgs(_scrollState));
                        break;
                    case MotionEventActions.Move:
                        if (_prevMoveEvent == null) {
                            _prevMoveEvent = ev;
                        }
                        float diffY = ev.GetY() - _prevMoveEvent.GetY();
                        float prevX = _prevMoveEvent.GetX();
                        _prevMoveEvent = MotionEvent.ObtainNoHistory(ev);
                        if (CurrentScrollY - diffY <= 0) {
                            // Can't scroll anymore.

                            if (_intercepted) {
                                // Already dispatched ACTION_DOWN event to parents, so stop here.
                                return false;
                            }

                            // Apps can set the interception target other than the direct parent.
                            ViewGroup parent;
                            if (_touchInterceptionViewGroup == null) {
                                parent = (ViewGroup)Parent;

                                if (parent is ViewPager) {
                                    _parentIsViewPager = true;
                                }
                            }
                            else {
                                parent = _touchInterceptionViewGroup;
                            }

                            if (_parentIsViewPager && System.Math.Abs(ev.GetX() - prevX) < 40) {
                                return false;
                            }

                            // Get offset to parents. If the parent is not the direct parent,
                            // we should aggregate offsets from all of the parents.
                            float offsetX = 0;
                            float offsetY = 0;
                            for (View v = this; v != null && v != parent; v = (View)v.Parent) {
                                offsetX += v.Left - v.ScrollX;
                                offsetY += v.Top - v.ScrollY;
                            }
                            var @event = MotionEvent.ObtainNoHistory(ev);
                            @event.OffsetLocation(offsetX, offsetY);

                            // Candidate fix for MSL-285: parent.OnInterceptTouchEvent throwing
                            // "pointerIndex out of range"
                            //
                            // Solution is to check the pointer index < pointer count
                            // from: http://stackoverflow.com/a/27934594/491468
                            int pointerCount = MotionEventCompat.GetPointerCount(ev);
                            int index = MotionEventCompat.GetActionIndex(ev);
                            int activePointerId = MotionEventCompat.GetPointerId(ev, index);
                            index = MotionEventCompat.FindPointerIndex(ev, activePointerId);

                            if (-1 < index && index < pointerCount && parent.OnInterceptTouchEvent(@event)) {
                                _intercepted = true;

                                // If the parent wants to intercept ACTION_MOVE events,
                                // we pass ACTION_DOWN event to the parent
                                // as if these touch events just have began now.
                                @event.Action = MotionEventActions.Down;

                                // Return this onTouchEvent() first and set ACTION_DOWN event for parent
                                // to the queue, to keep events sequence.
                                Post(new Runnable(() => parent.DispatchTouchEvent(@event)));
                                return false;
                            }
                            // Even when this can't be scrolled anymore,
                            // simply returning false here may cause subView's click,
                            // so delegate it to super.
                            return base.OnTouchEvent(ev);
                        }
                        break;
                }
            }
            return base.OnTouchEvent(ev);
        }

        public void SetTouchInterceptionViewGroup(ViewGroup viewGroup) {
            _touchInterceptionViewGroup = viewGroup;
        }

        public void ScrollVerticallyTo(int y) {
            ScrollTo(0, y);
        }

        protected override void OnRestoreInstanceState(IParcelable state) {
            var ss = (SavedState)state;
            _prevScrollY = ss.PrevScrollY;
            _scrollY = ss.ScrollY;
            base.OnRestoreInstanceState(ss.SuperState);
        }

        protected override IParcelable OnSaveInstanceState() {
            var superState = base.OnSaveInstanceState();

            return new SavedState(superState) {
                PrevScrollY = _prevScrollY,
                ScrollY = _scrollY
            };
        }

        private class SavedState : BaseSavedState {
            [ExportField("CREATOR"), UsedImplicitly]
            public static IParcelableCreator InitializeCreator() {
                return new SavedStateCreator();
            }

            public int PrevScrollY { get; set; }
            public int ScrollY { get; set; }

            /// <remarks>
            ///    Called by onSaveInstanceState.
            /// </remarks>
            public SavedState(IParcelable superState) : base(superState) { }

            /// <remarks>
            ///    Called by CREATOR.
            /// </remarks>
            private SavedState(Parcel source) : base(source) {
                PrevScrollY = source.ReadInt();
                ScrollY = source.ReadInt();
            }

            public override void WriteToParcel(Parcel dest, ParcelableWriteFlags flags) {
                base.WriteToParcel(dest, flags);
                dest.WriteInt(PrevScrollY);
                dest.WriteInt(ScrollY);
            }

            class SavedStateCreator : Java.Lang.Object, IParcelableCreator {
                public Object CreateFromParcel(Parcel source) => new SavedState(source);
                public Object[] NewArray(int size) => new Object[size];
            }
        }
    }
}