using System;
using System.Collections.Generic;
using System.Linq;
using Android.Content;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.MslPopupMenu")]
    public class MslPopupMenu :PopupMenu {
        protected MslPopupMenu(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public MslPopupMenu(Context context, View anchor) : this(context, anchor, 0) {}
        public MslPopupMenu(Context context, View anchor, int gravity)
            : base(context, anchor, gravity, Resource.Attribute.actionOverflowMenuStyle, Resource.Style.MyStudyLife_Widget_PopupMenu) { }

        public class Builder {
            private readonly Context _context;
            private readonly MslPopupMenu _menu; 

            public Builder(Context context, View anchor) {
                _context = context;
                _menu = new MslPopupMenu(context, anchor);
            }

            public Builder Items(IEnumerable<string> items) {
                var itemsArray = items.ToArray();

                for (int i = 0; i < itemsArray.Length; i++) {
                    _menu.Menu.Add(-1, -1, i, itemsArray[i]);
                }

                return this;
            }

            public Builder Items(IEnumerable<int> itemResIds) {
                return Items(itemResIds.Select(x => _context.Resources.GetString(x)));
            }

            public Builder MenuItemClick(Action<int> onClick) {
                _menu.MenuItemClick += (s, e) => onClick(e.Item.Order);

                return this;
            }

            public void Show() {
                _menu.Show();
            }
        }
    }
}