using System;
using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Java.Interop;

namespace MyStudyLife.Droid.Widgets {
    public enum RobotoTypeface {
        Thin = 0,
        ThinItalic = 1,
        Light = 2,
        LightItalic = 3,
        Regular = 4,
        Italic = 5,
        Medium = 6,
        MediumItalic = 7,
        Bold = 8,
        BoldItalic = 9,
        Black = 10,
        BlackItalic = 11,
        Condensed = 12,
        CondensedItalic = 13,
        CondensedBold = 14,
        CondensedBoldItalic = 15,
    }

    public static class RobotoWidgetHelper {
        private static readonly Dictionary<RobotoTypeface, Typeface> _typefaces = new Dictionary<RobotoTypeface, Typeface>(16);

        public static void SetRobotoTypeface(this TextView textView, RobotoTypeface robotoTypeface) {
            textView.SetTypeface(GetRobotoTypeface(textView.Context, robotoTypeface), TypefaceStyle.Normal);
        }

        public static Typeface GetRobotoTypeface(Context context, RobotoTypeface robotoTypeface) {
            Typeface typeface;

            if (_typefaces.TryGetValue(robotoTypeface, out typeface)) {
                return typeface;
            }

            typeface = Typeface.CreateFromAsset(context.Assets, $"fonts/Roboto-{robotoTypeface}.ttf");

            if (typeface == null) {
                throw new ArgumentException("Could not find Roboto typeface '" + robotoTypeface + "'");
            }
            
            _typefaces.Add(robotoTypeface, typeface);

            return typeface;
        }
    }

    [Register("mystudylife.droid.widgets.RobotoTextView")]
    public class RobotoTextView : TextView {
        private RobotoTypeface _typeface;

        public RobotoTypeface RobotoTypeface {
            [Export("getRobotoTypeface")]
            get { return _typeface; }
            [Export("setRobotoTypeface")]
            set {
                _typeface = value;
                this.SetRobotoTypeface(value);
            }
        }

        protected RobotoTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public RobotoTextView(Context context) : this(context, null) { }
        public RobotoTextView(Context context, IAttributeSet attrs) : this(context, attrs, Android.Resource.Attribute.TextViewStyle) { }
        public RobotoTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.RobotoWidget);

            this.SetRobotoTypeface((RobotoTypeface) a.GetInt(Resource.Styleable.RobotoWidget_typeface, (int)RobotoTypeface.Regular));

            a.Recycle();
        }
    }

    [Register("mystudylife.droid.widgets.RobotoButton")]
    public class RobotoButton : Button {
        private RobotoTypeface _typeface;

        public RobotoTypeface RobotoTypeface {
            [Export("getRobotoTypeface")]
            get { return _typeface; }
            [Export("setRobotoTypeface")]
            set {
                _typeface = value;
                this.SetRobotoTypeface(value);
            }
        }

        protected RobotoButton(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
        public RobotoButton(Context context) : this(context, null) {}
        public RobotoButton(Context context, IAttributeSet attrs) : this(context, attrs, Android.Resource.Attribute.ButtonStyle) { }
        public RobotoButton(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.RobotoWidget);

            this.SetRobotoTypeface((RobotoTypeface) a.GetInt(Resource.Styleable.RobotoWidget_typeface, (int)RobotoTypeface.Regular));

            a.Recycle();
        }
    }
}