using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target.Construction;
using Java.Interop;
using Java.IO;
using MvvmCross.Platforms.Android.Binding.Target;
using AndroidX.Core.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.ErrorLabelLayout")]
    public sealed class ErrorLabelLayout : LinearLayout {

        private const float DEFAULT_LABEL_PADDING_LEFT = 8f;
        private const float DEFAULT_LABEL_PADDING_TOP = 0;
        private const float DEFAULT_LABEL_PADDING_RIGHT = 16f;
        private const float DEFAULT_LABEL_PADDING_BOTTOM = 0;

        private EditText _editText;
        private readonly TextView _errorLabel;

        private string _errorText;
        private readonly IInterpolator _interpolator;

        /// <returns>
        ///     The <see cref="Android.Widget.EditText" /> text input.
        /// </returns>
        public EditText EditText {
            get { return _editText; }
        }

        public TextView ErrorLabel {
            get { return _errorLabel; }
        }

        public string ErrorText {
            [Export("getErrorText")]
            get { return _errorText; }
            [Export("setErrorText")]
            set {
                _errorText = value;
                _errorLabel.Text = value;

                UpdateLabelVisibility(true);
            }
        }

        private bool HasError {
            get { return !String.IsNullOrWhiteSpace(_errorText); }
        }

        public ErrorLabelLayout(Context context) : this(context, null) { }
        public ErrorLabelLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public ErrorLabelLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            Orientation = Orientation.Horizontal;

            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.ErrorLabelLayout);

            int leftPadding = a.GetDimensionPixelSize(
                Resource.Styleable.ErrorLabelLayout_errorLabelPaddingLeft,
                DipsToPix(DEFAULT_LABEL_PADDING_LEFT)
            );
            int topPadding = a.GetDimensionPixelSize(
                Resource.Styleable.ErrorLabelLayout_errorLabelPaddingTop,
                DipsToPix(DEFAULT_LABEL_PADDING_TOP)
            );
            int rightPadding = a.GetDimensionPixelSize(
                Resource.Styleable.ErrorLabelLayout_errorLabelPaddingRight,
                DipsToPix(DEFAULT_LABEL_PADDING_RIGHT)
            );
            int bottomPadding = a.GetDimensionPixelSize(
                Resource.Styleable.ErrorLabelLayout_errorLabelPaddingBottom,
                DipsToPix(DEFAULT_LABEL_PADDING_BOTTOM)
            );
            _errorText = a.GetText(Resource.Styleable.ErrorLabelLayout_errorText);

            _errorLabel = new TextView(context) {
                Gravity = GravityFlags.CenterVertical,
                Text = _errorText
            };
            _errorLabel.SetPadding(leftPadding, topPadding, rightPadding, bottomPadding);

            TextViewCompat.SetTextAppearance(
                _errorLabel,
                a.GetResourceId(
                    Resource.Styleable.ErrorLabelLayout_errorLabelTextAppearance,
                    Resource.Style.TextAppearance_MyStudyLife_ErrorLabel
                )
            );

            a.Recycle();

            AddView(_errorLabel, LayoutParams.WrapContent, LayoutParams.MatchParent);

            UpdateLabelVisibility(false);

            _interpolator = AnimationUtils.LoadInterpolator(
                context,
                Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop
                    ? Android.Resource.Interpolator.FastOutSlowIn
                    : Android.Resource.Animation.DecelerateInterpolator
            );
        }

        public override void AddView(View child, int index, ViewGroup.LayoutParams @params) {
            var editText = child as EditText;
            if (editText != null) {
                SetEditText(editText);
            }

            index = Math.Max(0, index);

            if (!ReferenceEquals(child, _errorLabel)) {
                @params = new LayoutParams(0, LayoutParams.MatchParent, 1);
            }

            // Carry on adding the View...
            base.AddView(child, index, @params);
        }

        private void SetEditText(EditText editText) {
            // If we already have an EditText, throw an exception
            if (_editText != null) {
                throw new InvalidObjectException("We already have an EditText, can only have one");
            }

            _editText = editText;

            // Add a TextWatcher so that we know when the text input has changed
            //_editText.AfterTextChanged += (s, e) => UpdateLabelVisibility(true);

            // Add focus listener to the EditText so that we can notify the label that it is activated.
            // Allows the use of a ColorStateList for the text color on the label
            _editText.FocusChange += (s, e) => UpdateLabelVisibility(true);

            // If we do not have a valid hint, try and retrieve it from the EditText
            //if (System.String.IsNullOrEmpty(_hint)) {
            //    Hint = _editText.Hint;
            //}
        }

        private void UpdateLabelVisibility(bool animate) {
            bool isFocused = _editText != null && _editText.IsFocused;

            // _label.Activated = isFocused;

            if (HasError && !isFocused) {
                // We should be showing the label so do so if it isn't already
                if (_errorLabel.Visibility != ViewStates.Visible) {
                    ShowErrorLabel(animate);
                }
            }
            else {
                // We should not be showing the label so hide it
                if (_errorLabel.Visibility == ViewStates.Visible) {
                    HideErrorLabel(animate);
                }
            }
        }

        /// <summary>
        ///     Show the label
        /// </summary>
        private void ShowErrorLabel(bool animate) {
            _errorLabel.Visibility = ViewStates.Visible;
        }

        /// <summary>
        ///     Hide the label
        /// </summary>
        private void HideErrorLabel(bool animate) {
            _errorLabel.Visibility = ViewStates.Gone;
        }


        /// <summary>
        ///    Helper method to convert dips to pixels.
        /// </summary>
        private int DipsToPix(float dps) {
            return (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, dps, Resources.DisplayMetrics);
        }

        public class TargetBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<ErrorLabelLayout>("ErrorText", view => new TargetBinding(view));
            }

            public override Type TargetType {
                get { return typeof (string); }
            }

            public TargetBinding(ErrorLabelLayout target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                ((ErrorLabelLayout) target).ErrorText = (string) value;
            }
        }
    }
}