using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Util;
using Android.Content.Res;
using MyStudyLife.Droid.Common;
using AndroidX.Core.Graphics.Drawable;
using AndroidX.AppCompat.Widget;

namespace MyStudyLife.Droid.Widgets {
    /// <summary>
    ///     A <see cref="SwitchCompat"/> that can be tinted at runtime.
    /// </summary>
    [Register("mystudylife.droid.widgets.MslSwitch")]
    public class MslSwitch : SwitchCompat {
        private readonly Color _colorForeground;
        private readonly Color _colorSwitchThumbNormal;
        private Color _checkedTintColor;

        public Color CheckedTintColor {
            get { return _checkedTintColor; }
            set {
                if (_checkedTintColor != value) {
                    _checkedTintColor = value;

                    SetTintLists();
                }
            }
        }

        public override Drawable TrackDrawable {
            get { return base.TrackDrawable; }
            set {
                base.TrackDrawable = value;
                SetTintLists();
            }
        }

        public override Drawable ThumbDrawable {
            get { return base.ThumbDrawable; }
            set {
                base.ThumbDrawable = value;
                SetTintLists();
            }
        }

        protected MslSwitch(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public MslSwitch(Context context) : this(context, null) { }
        public MslSwitch(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.switchStyle) { }
        public MslSwitch(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            var a = context.Theme.ObtainStyledAttributes(new[] {
                Resource.Attribute.colorSwitchThumbNormal,
                Resource.Attribute.colorControlActivated
            });

            _colorForeground = ContextCompatEx.GetColor(context, Resource.Color.foreground); // AppCompat doesn't have Resource.Attribute.colorForeground
            _colorSwitchThumbNormal = a.GetColor(0, Color.Black);
            _checkedTintColor = a.GetColor(1, Color.Black);

            a.Recycle();
        }

        private void SetTintLists() {
            DrawableCompat.SetTintList(
                this.TrackDrawable,
                new ColorStateList(
                    new [] {
                        new [] { -Android.Resource.Attribute.StateEnabled },
                        new [] { Android.Resource.Attribute.StateChecked },
                        new int[0] 
                    }, 
                    new [] {
                        // Alpha values based on 
                        // https://github.com/android/platform_frameworks_base/blob/master/core/res/res/drawable/switch_track_material.xml
                        (int) new Color(_colorForeground) {
                            A = 26 // 10%
                        },
                            new Color(CheckedTintColor) {
                            A = 77 // 30%
                        },
                        new Color(_colorForeground) {
                            A = 77 // 30%
                        }
                    }
                )
            );

            DrawableCompat.SetTintList(
                this.ThumbDrawable,
                new ColorStateList(
                    new[] {
                        new [] { -Android.Resource.Attribute.StateEnabled },
                        new [] { Android.Resource.Attribute.StateChecked },
                        new int[0] 
                    },
                    new[] {
                        (int) _colorSwitchThumbNormal,
                        CheckedTintColor,
                        _colorSwitchThumbNormal
                    }
                )
            );
        }
    }
}