using System;
using Android.Animation;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;
using AndroidX.Core.View;
using AndroidX.Core.Widget;
using Java.Interop;
using Java.IO;

namespace MyStudyLife.Droid.Widgets {
    /// <summary>
    ///    Layout which an <see cref="Android.Widget.EditText"/> to show a floating label when the hint is hidden
    ///    due to the user inputting text.
    /// </summary>
    /// <remarks>
    ///     Tweaked C# port of https://gist.github.com/chrisbanes/11247418 to handle
    ///     a row layout with vertically centered text.
    /// </remarks>
    [Register("mystudylife.droid.widgets.FloatLabelLayout")]
    public sealed class FloatLabelLayout : FrameLayout, Animator.IAnimatorListener {

        private const long ANIMATION_DURATION = 150;

        private const float DEFAULT_LABEL_PADDING_LEFT = 0;
        private const float DEFAULT_LABEL_PADDING_TOP = 5f;
        private const float DEFAULT_LABEL_PADDING_RIGHT = 0;
        private const float DEFAULT_LABEL_PADDING_BOTTOM = 4f;

        private const float DEFAULT_EDIT_TEXT_OFFSET_TOP = -2f;

        private EditText _editText;
        private readonly TextView _label;

        private string _hint;
        private readonly IInterpolator _interpolator;

        /// <returns>
        ///     The <see cref="Android.Widget.EditText" /> text input.
        /// </returns>
        public EditText EditText {
            get { return _editText; }
        }

        /// <returns>
        ///     The <see cref="Android.Widget.TextView" /> label.
        /// </returns>
        public TextView Label {
            get { return _label; }
        }

        /// <summary>
        ///     Gets or sets the hint to be displayed in the floating label.
        /// </summary>
        public string Hint {
            [Export("getHint")]
            get { return _hint; }
            [Export("setHint")]
            set {
                _hint = value;
                _label.Text = value;

                if (_editText != null) {
                    _editText.Hint = value;
                }
            }
        }

        public FloatLabelLayout(Context context) : this(context, null) { }
        public FloatLabelLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public FloatLabelLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.FloatLabelLayout);

            int leftPadding = a.GetDimensionPixelSize(
                Resource.Styleable.FloatLabelLayout_floatLabelPaddingLeft,
                DipsToPix(DEFAULT_LABEL_PADDING_LEFT)
            );
            int topPadding = a.GetDimensionPixelSize(
                Resource.Styleable.FloatLabelLayout_floatLabelPaddingTop,
                DipsToPix(DEFAULT_LABEL_PADDING_TOP)
            );
            int rightPadding = a.GetDimensionPixelSize(
                Resource.Styleable.FloatLabelLayout_floatLabelPaddingRight,
                DipsToPix(DEFAULT_LABEL_PADDING_RIGHT)
            );
            int bottomPadding = a.GetDimensionPixelSize(
                Resource.Styleable.FloatLabelLayout_floatLabelPaddingBottom,
                DipsToPix(DEFAULT_LABEL_PADDING_BOTTOM)
            );
            _hint = a.GetText(Resource.Styleable.FloatLabelLayout_floatLabelHint);

            _label = new TextView(context) {
                Visibility = ViewStates.Invisible,
                Text = _hint
            };
            _label.SetPadding(leftPadding, topPadding, rightPadding, bottomPadding);
            ViewCompat.SetPivotX(_label, 0f);
            ViewCompat.SetPivotY(_label, 0f);

            TextViewCompat.SetTextAppearance(
                _label,
                a.GetResourceId(
                    Resource.Styleable.FloatLabelLayout_floatLabelTextAppearance,
                    Resource.Style.TextAppearance_MyStudyLife_FloatLabel
                )
            );

            a.Recycle();

            AddView(_label, LayoutParams.WrapContent, LayoutParams.WrapContent);

            _label.LayoutChange += LabelOnLayoutChange;

            _interpolator = AnimationUtils.LoadInterpolator(
                context,
                Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop
                    ? Android.Resource.Interpolator.FastOutSlowIn
                    : Android.Resource.Animation.DecelerateInterpolator
            );
        }

        private void LabelOnLayoutChange(object sender, LayoutChangeEventArgs e) {
            // Ensures correct layout once the label has a height
            if (!String.IsNullOrEmpty(_editText?.Text)) {
                UpdateLabelVisibility(false, true);
            }
        }

        public override void AddView(View child, int index, ViewGroup.LayoutParams @params) {
            if (child is EditText editText) {
                SetEditText(editText);
            }

            // Carry on adding the View...
            base.AddView(child, index, @params);
        }

        private void SetEditText(EditText editText) {
            // If we already have an EditText, throw an exception
            if (_editText != null) {
                throw new InvalidObjectException("We already have an EditText, can only have one");
            }
            
            _editText = editText;

            // Update the label visibility with no animation
            UpdateLabelVisibility(false);

            // Add a TextWatcher so that we know when the text input has changed
            _editText.AfterTextChanged += (s, e) => UpdateLabelVisibility(true);

            // Add focus listener to the EditText so that we can notify the label that it is activated.
            // Allows the use of a ColorStateList for the text color on the label
            _editText.FocusChange += (s, e) => UpdateLabelVisibility(true);

            // If we do not have a valid hint, try and retrieve it from the EditText
            if (String.IsNullOrEmpty(_hint)) {
                Hint = _editText.Hint; // For some reason this is null on lollipop when the edittext has text
            }
            else {
                _editText.Hint = _hint;
            }
        }

        private void UpdateLabelVisibility(bool animate, bool force = false) {
            bool hasText = !String.IsNullOrEmpty(_editText.Text);
            bool isFocused = _editText.IsFocused;

            _label.Activated = isFocused;

            if (hasText || isFocused) {
                // We should be showing the label so do so if it isn't already
                if (force || _label.Visibility != ViewStates.Visible) {
                    ShowLabel(animate);
                }
            }
            else {
                // We should not be showing the label so hide it
                if (force || _label.Visibility == ViewStates.Visible) {
                    HideLabel(animate);
                }
            }
        }

        private ViewPropertyAnimator _labelAnimation;
        private ViewPropertyAnimator _editTextAnimation;

        /// <summary>
        ///     Show the label
        /// </summary>
        private void ShowLabel(bool animate) {
            CancelAnimations();

            if (animate) {
                _label.Visibility = ViewStates.Visible;
                _label.TranslationY = _label.Height;

                float scale = _editText.TextSize / _label.TextSize;

                _label.ScaleX = scale;
                _label.ScaleY = scale;

                _labelAnimation = _label.Animate()
                    .TranslationY(0f)
                    .ScaleX(1f)
                    .ScaleY(1f)
                    .SetDuration(ANIMATION_DURATION)
                    .SetListener(null)
                    .SetInterpolator(_interpolator);

                _editTextAnimation = _editText.Animate()
                    .TranslationY((_label.Height + DEFAULT_EDIT_TEXT_OFFSET_TOP) - _editText.PaddingTop)
                    .SetDuration(ANIMATION_DURATION)
                    .SetInterpolator(_interpolator);

                _labelAnimation.Start();
                _editTextAnimation.Start();
            }
            else {
                _label.Visibility = ViewStates.Visible;
                _editText.TranslationY = (_label.Height + DEFAULT_EDIT_TEXT_OFFSET_TOP) - _editText.PaddingTop;
            }

            _editText.Gravity = GravityFlags.Top;
            _editText.Hint = null;
        }

        /// <summary>
        ///     Hide the label
        /// </summary>
        private void HideLabel(bool animate) {
            CancelAnimations();

            if (animate) {
                float scale = _editText.TextSize / _label.TextSize;

                _label.ScaleX = 1f;
                _label.ScaleY = 1f;
                _label.TranslationY = 0f;

                _labelAnimation = _label.Animate()
                    .TranslationY((this.Height - (_label.Height*scale))/2f)
                    .SetDuration(ANIMATION_DURATION)
                    .ScaleX(scale)
                    .ScaleY(scale)
                    .SetListener(this)
                    .SetInterpolator(_interpolator);

                _editTextAnimation = _editText.Animate()
                    .TranslationY(0f)
                    .SetDuration(ANIMATION_DURATION)
                    .SetInterpolator(_interpolator);

                _labelAnimation.Start();
                _editTextAnimation.Start();
            }
            else {
                _label.Visibility = ViewStates.Invisible;
                _editText.TranslationY = 0;
                _editText.Hint = _hint;
            }

            _editText.Gravity = GravityFlags.CenterVertical;
        }

        private void CancelAnimations() {
            if (_labelAnimation != null) {
                _labelAnimation.Cancel();
                _labelAnimation.Dispose();
                _labelAnimation = null;
            }
            _label?.ClearAnimation();

            if (_editTextAnimation != null) {
                _editTextAnimation.Cancel();
                _editTextAnimation.Dispose();
                _editTextAnimation = null;
            }
            _editText?.ClearAnimation();

        }


        /// <summary>
        ///    Helper method to convert dips to pixels.
        /// </summary>
        private int DipsToPix(float dps) {
            return (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, dps, Resources.DisplayMetrics);
        }

        #region IAnimatorListener

        // On animate label hide

        public void OnAnimationCancel(Animator animation) { }
        public void OnAnimationRepeat(Animator animation) { }
        public void OnAnimationStart(Animator animation) { }
        public void OnAnimationEnd(Animator animation) {
            _label.Visibility = ViewStates.Invisible;
            _editText.Hint = Hint;
        }

        #endregion
    }
}