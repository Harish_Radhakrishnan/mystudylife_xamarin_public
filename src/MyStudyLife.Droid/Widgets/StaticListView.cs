using System;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Input;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding.Attributes;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Widgets
{
    // Only supports vertical orientation atm
    [Register("mystudylife.droid.widgets.StaticListView")]
    public class StaticListView : LinearLayout, IMvxWithChangeAdapter {
        public event EventHandler<ItemClickEventArgs> OnItemClick;

        private int _dividerHeight;

        public int DividerHeight {
            get => _dividerHeight;
            set {
                if (_dividerHeight == value) {
                    return;
                }

                _dividerHeight = value;

                var drawable = OS.CheckSystemVersion(BuildVersionCodes.JellyBean) ? DividerDrawable : null;

                if (drawable == null) {
                    SetDividerDrawable(null);
                }
                else if (drawable.Class.Name == Java.Lang.Class.FromType(typeof(DividerDrawableWrapper)).Name) {
                    SetDividerDrawable(drawable.JavaCast<DividerDrawableWrapper>().WrappedDrawable);
                }
            }
        }

        protected StaticListView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public StaticListView(Context context, IAttributeSet attrs) : this(context, attrs, new MvxAdapterWithChangedEvent(context)) { }
        public StaticListView(Context context, IAttributeSet attrs, IMvxAdapterWithChangedEvent adapter) : base(context, attrs) {
            Orientation = Orientation.Vertical;

            var itemTemplateId = MvxAttributeHelpers.ReadListItemTemplateId(context, attrs);
            if (adapter != null) {
                Adapter = adapter;
                Adapter.ItemTemplateId = itemTemplateId;
            }

            this.ChildViewAdded += OnChildViewAdded;
            this.ChildViewRemoved += OnChildViewRemoved;

            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.StaticListView);

            DividerHeight = a.GetDimensionPixelSize(Resource.Styleable.StaticListView_dividerHeight, 0);
            ShowDividers = ShowDividers.Middle;
            
            a.Recycle();
        }

        public override void SetDividerDrawable(Drawable divider) {
            base.SetDividerDrawable(new DividerDrawableWrapper(DividerHeight, divider));
        }

        public ICommand ItemClick { get; set; }

        protected virtual void ExecuteCommandOnItem(ICommand command, int position) {
            if (command == null) {
                return;
            }

            var item = Adapter.GetRawItem(position);

            if (item == null || !command.CanExecute(item)) {
                return;
            }

            command.Execute(item);
        }
        
        public void AdapterOnDataSetChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.UpdateDataSetFromChange(sender, e);
        }

        private void OnChildViewAdded(object sender, ChildViewAddedEventArgs e) {
            e.Child.Click += ChildViewOnClick;
        }

        private void OnChildViewRemoved(object sender, ChildViewRemovedEventArgs e) {
            (e.Child as IMvxBindingContextOwner)?.ClearAllBindings();

            e.Child.Click -= ChildViewOnClick;
        }

        private void ChildViewOnClick(object sender, EventArgs e) {
            int index = IndexOfChild((View)sender);

            var item = Adapter.GetRawItem(index);
            OnItemClick?.Invoke(this, new ItemClickEventArgs((View)sender, index, item));

            ExecuteCommandOnItem(ItemClick, index);
        }

        private IMvxAdapterWithChangedEvent _adapter;

        public IMvxAdapterWithChangedEvent Adapter {
            get { return _adapter; }
            set {
                var existing = _adapter;
                if (existing == value) {
                    return;
                }

                if (existing != null) {
                    if (value != null) {
                        value.ItemsSource = existing.ItemsSource;
                        value.ItemTemplateId = existing.ItemTemplateId;
                    }

                    existing.DataSetChanged -= AdapterOnDataSetChanged;
                    existing.ItemsSource = null;
                }

                SetAdapter(value);
            }
        }

        [MvxSetToNullAfterBinding]
        public IEnumerable ItemsSource {
            get { return Adapter.ItemsSource; }
            set { Adapter.ItemsSource = value; }
        }

        public int ItemTemplateId {
            get { return Adapter.ItemTemplateId; }
            set { Adapter.ItemTemplateId = value; }
        }

        private void SetAdapter(IMvxAdapterWithChangedEvent adapter) {
            _adapter = adapter;

            if (_adapter == null) {
                Mvx.IoCProvider.Resolve<ILogger<StaticListView>>().LogWarning("Setting Adapter to null is not recommended - you amy lose ItemsSource binding when doing this");
                return;

            }

            _adapter.DataSetChanged += AdapterOnDataSetChanged;

            this.RemoveAllViews();
            var count = adapter.Count;
            for (var i = 0; i < count; i++) {
                this.AddView(adapter.GetView(i, null, this));
            }
        }

        // Hack to prevent creating a different drawable for each divider size
        class DividerDrawableWrapper : Drawable {
            // TODO: Override everything else too

            private readonly Drawable _wrappedDrawable;
            private readonly int _height;

            public Drawable WrappedDrawable {
                get { return _wrappedDrawable; }
            }

            public override int IntrinsicHeight {
                get { return _height; }
            }

            public DividerDrawableWrapper(int height) {
                _height = height;
            }

            public DividerDrawableWrapper(int height, Drawable drawable) {
                _height = height;
                _wrappedDrawable = drawable;
            }

            public override void Draw(Canvas canvas) {
                _wrappedDrawable?.Draw(canvas);
            }

            public override void SetAlpha(int alpha) {
                _wrappedDrawable?.SetAlpha(alpha);
            }

            public override void SetColorFilter(ColorFilter cf) {
                _wrappedDrawable?.SetColorFilter(cf);
            }

            public override int Opacity {
                get {
                    if (_wrappedDrawable != null) {
                        return _wrappedDrawable.Opacity;
                    }

                    return 0; // Transparent (range is 0-255)
                }
            }
        }

        public class ItemClickEventArgs {
            public View View { get; private set; }

            public int Position { get; private set; }

            public object DataContext { get; private set; }

            public ItemClickEventArgs(View view, int position, object dataContext) {
                this.View = view;
                this.Position = position;
                this.DataContext = dataContext;
            }
        }
    }
}