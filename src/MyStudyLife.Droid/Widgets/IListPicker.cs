using System;
using System.Collections;

namespace MyStudyLife.Droid.Widgets {
    public interface IListPicker {
        event EventHandler SelectedItemChanged;

        object SelectedItem { get; set; }

        IEnumerable ItemsSource { get; set; } 
    }
}