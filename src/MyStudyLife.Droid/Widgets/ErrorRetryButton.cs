using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.ErrorRetryButton")]
    public sealed class ErrorRetryButton : FrameLayout {
        public ErrorRetryButton(Context context, IAttributeSet attrs) : base(context, attrs) {

            var inflater = (LayoutInflater) context.GetSystemService(Context.LayoutInflaterService);

            inflater.Inflate(Resource.Layout.Component_ErrorRetryButton, this, true);

            this.Clickable = true;
        }
    }
}