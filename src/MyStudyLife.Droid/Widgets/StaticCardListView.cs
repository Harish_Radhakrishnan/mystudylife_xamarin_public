using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using MvvmCross.Platforms.Android.Binding.Views;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.StaticCardListView")]
    public class StaticCardListView : StaticListView {
        public int ItemSpacing { get; set; } = DisplayHelper.DpsToPixels(6);

        protected StaticCardListView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public StaticCardListView(Context context, IAttributeSet attrs) : base(context, attrs) { }
        public StaticCardListView(Context context, IAttributeSet attrs, IMvxAdapterWithChangedEvent adapter) : base(context, attrs, adapter) { }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            var width = MeasureSpec.GetSize(widthMeasureSpec);

            int widthUsed = 0;
            int heightUsed = 0;

            for (int i = 0; i < ChildCount; i++) {
                var view = GetChildAt(i);

                if (0 < i) {
                    heightUsed += GetSpacing(i, view);
                }
                
                var layoutParams = (MarginLayoutParams)view.LayoutParameters;

                MeasureChildWithMargins(view, widthMeasureSpec, widthUsed, heightMeasureSpec, heightUsed);

                heightUsed += view.MeasuredHeight + layoutParams.TopMargin + layoutParams.BottomMargin;
            }

            int height = heightUsed + PaddingTop + PaddingBottom;

            SetMeasuredDimension(width, height);
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b) {
            int currentTop = PaddingTop;

            for (int i = 0; i < ChildCount; i++) {
                var view = GetChildAt(i);

                if (0 < i) {
                    currentTop += GetSpacing(i, view);
                }

                var layoutParams = (MarginLayoutParams)view.LayoutParameters;

                int left = layoutParams.LeftMargin;
                int top = currentTop + layoutParams.TopMargin;
                int right = left + view.MeasuredWidth;
                int bottom = top + view.MeasuredHeight;

                view.Layout(left, top, right, bottom);

                currentTop = bottom + layoutParams.BottomMargin;
            }
        }

        /// <summary>
        ///     Get the spacing in pixels around the item in the
        ///     given <paramref name="position"/>.
        /// </summary>
        protected virtual int GetSpacing(int position, View view) => ItemSpacing;
    }
}