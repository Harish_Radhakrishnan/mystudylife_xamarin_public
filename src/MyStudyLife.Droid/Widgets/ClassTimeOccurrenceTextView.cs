using System;
using System.Globalization;
using Android.Content;
using Android.Runtime;
using Android.Util;
using MvvmCross.Binding.Bindings.Target.Construction;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI.Converters;
using MyStudyLife.Droid.Common;
using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.ClassTimeOccurrenceTextView")]
    public class ClassTimeOccurrenceTextView : RobotoTextView {
        private static ClassOccurrenceConverter _converter = new ClassOccurrenceConverter();

        private ClassTime _value;
        private bool _withTime;

        public ClassTime Value {
            get { return _value; }
            set {
                if (!ReferenceEquals(_value, value)) {
                    _value = value;
                    SetOccurrenceText();
                }
            }
        }

        public bool WithTime {
            get { return _withTime; }
            set {
                if (_withTime != value) {
                    _withTime = value;
                    SetOccurrenceText();
                }
            }
        }

        public override int MaxLines {
            get {
                if (OS.CheckSystemVersion(BuildVersionCodes.JellyBean)) {
                    return base.MaxLines;
                }

                try {
                    var @class = Java.Lang.Class.FromType(typeof(TextView));

                    var maximumField = @class.GetDeclaredField("mMaximum");
                    var maxModeField = @class.GetDeclaredField("mMaxMode");

                    if (maximumField != null && maxModeField != null) {
                        maxModeField.Accessible = true;

                        if (maxModeField.GetInt(this) == 1 /* 1 == Lines */) {
                            maximumField.Accessible = true;

                            return maximumField.GetInt(this);
                        }
                    }
                }
                catch { }

                return int.MaxValue;
            }
        }

        protected ClassTimeOccurrenceTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public ClassTimeOccurrenceTextView(Context context) : this(context, null) { }
        public ClassTimeOccurrenceTextView(Context context, IAttributeSet attrs) : this(context, attrs, Android.Resource.Attribute.TextViewStyle) { }
        public ClassTimeOccurrenceTextView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.ClassTimeOccurrenceTextView);

            _withTime = a.GetBoolean(Resource.Styleable.ClassTimeOccurrenceTextView_withTime, false);

            a.Recycle();
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

            this.SetOccurrenceText();
            
            // View requires remeasuring as it may have wrapped before
            if (this.MaxLines != 1) {
                base.OnMeasure(widthMeasureSpec, heightMeasureSpec);
            }
        }

        public void NotifyValueChanged() {
            this.SetOccurrenceText();
        }

        private void SetOccurrenceText() {
            var time = this.Value;

            if (time == null) {
                this.Text = "--";
                return;
            }

            DateComponentNaming naming = DateComponentNaming.Full;

            var textViewWidth = this.MeasuredWidth - (this.PaddingLeft + this.PaddingRight);
            var textPaint = this.Paint;
            float textWidth;

            string timeStr = _withTime ? (time.Time + " ") : String.Empty;
            string text;

            do {
                text = String.Concat(timeStr, _converter.Convert(time, typeof(string), naming, CultureInfo.CurrentCulture));

                textWidth = textPaint.MeasureText(text);

                naming++;
            } while (0 < textViewWidth && textViewWidth < textWidth && naming <= DateComponentNaming.Shortest);

            this.Text = text;
        }

        public class Binding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<ClassTimeOccurrenceTextView>("Value", view => new Binding(view));
            }

            public override Type TargetType {
                get { return typeof(ClassTime); }
            }

            public Binding(ClassTimeOccurrenceTextView textView) : base(textView) { }

            protected override void SetValueImpl(object target, object value) {
                var textView = target as ClassTimeOccurrenceTextView;

                if (textView != null) {
                    textView.Value = value as ClassTime;
                }
            }
        }
    }
}