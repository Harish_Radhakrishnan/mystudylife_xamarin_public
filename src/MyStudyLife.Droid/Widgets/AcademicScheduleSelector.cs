using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.AcademicScheduleSelector")]
	public abstract class AcademicScheduleSelector : Button, IListPicker, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener {
		#region Constructors

		protected AcademicScheduleSelector(IntPtr javaReference, JniHandleOwnership transfer)
			: base(javaReference, transfer) {
			Initialize();
		}
		public AcademicScheduleSelector(Context context)
			: base(context) {
			Initialize();
		}
		public AcademicScheduleSelector(Context context, IAttributeSet attrs)
			: base(context, attrs) {
			Initialize();
		}

		public AcademicScheduleSelector(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
			Initialize();
		}

		#endregion

        private AppCompatActivity _activity;

        private IAcademicSchedule _selectedItem;
        private ObservableCollection<AcademicYear> _itemsSource; 

	    public event EventHandler SelectedItemChanged;

	    object IListPicker.SelectedItem {
	        get { return SelectedItem; }
            set { SelectedItem = (IAcademicSchedule)value; }
	    }

	    IEnumerable IListPicker.ItemsSource {
	        get { return ItemsSource; }
            set { ItemsSource = (ObservableCollection<AcademicYear>)value; }
	    }

	    public IAcademicSchedule SelectedItem {
			get { return _selectedItem; }
			set {
                if (!ReferenceEquals(_selectedItem, value)) {
					_selectedItem = value;

					SetButtonText();
				}
			}
		}

	    private MvxNotifyCollectionChangedEventSubscription _itemsSourceSubscription;

		public ObservableCollection<AcademicYear> ItemsSource {
			get { return _itemsSource; }
			set {
                _itemsSourceSubscription?.Dispose();

                if (_itemsSource != value) {
					_itemsSource = value;

					SetButtonText();
				}

				if (_itemsSource != null) {
                    _itemsSourceSubscription = _itemsSource.WeakSubscribe(ItemsSourceOnCollectionChanged);
				}
			}
		}

		private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
			SetButtonText();
		}

		private void Initialize() {
            _activity = Context as AppCompatActivity;

			if (_activity == null) {
                throw new Exception("AcademicScheduleSelector can only be used on activities which inherit from AppCompatActivity.");
			}

			this.Click += OnClick;
		}

		private void OnClick(object sender, EventArgs e) {
			ShowDialog();
		}

		public void ShowDialog() {
			var d = GetDialogToShow();

			d.Show(_activity.SupportFragmentManager, "AcademicScheduleSelector");
		}

		protected AcademicSchedulePickerDialogFragment GetDialogToShow() {
			return new AcademicSchedulePickerDialogFragment(Context, this, ItemsSource, SelectedItem, HelpText);
		}

	    protected abstract string HelpText { get; }

		public void OnScheduleSet(IAcademicSchedule schedule) {
			this.SelectedItem = schedule;

            SelectedItemChanged?.Invoke(this, EventArgs.Empty);

			SetButtonText();
		}

        private void SetButtonText() {
            if (!this.IsInMonoLimbo()) {
                this.SetButtonTextImpl();
	        }
        }

		protected abstract void SetButtonTextImpl();
    }

	/// <summary>
	///		An <see cref="AcademicScheduleSelector" />
	///		tailored to filtering list views.
	/// </summary>
    [Register("mystudylife.droid.widgets.ListFilterAcademicScheduleSelector")]
	public class ListFilterAcademicScheduleSelector : AcademicScheduleSelector {
		#region Constructors

		protected ListFilterAcademicScheduleSelector(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
		public ListFilterAcademicScheduleSelector(Context context) : base(context) {}
		public ListFilterAcademicScheduleSelector(Context context, IAttributeSet attrs) : base(context, attrs) {}
		public ListFilterAcademicScheduleSelector(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

		#endregion

		#region Overrides of AcademicScheduleSelector

	    protected override string HelpText  {
	        get { return String.Empty; }
	    }

	    protected override void SetButtonTextImpl() {
			// TODO: Localize
			string s = null;

			if (this.SelectedItem != null) {
				if (!this.SelectedItem.IsCurrent) {
					s = String.Format(
						"Filtered for non-current {0} ({1})",
						this.SelectedItem is AcademicYear ? "year" : "term",
						this.SelectedItem.Dates
						);
				}
				else {
					if (this.SelectedItem is AcademicYear && ((AcademicYear)this.SelectedItem).Terms.Any()) {
						s = "Showing all for the current year";
					}
				}
			}
			else if (this.ItemsSource != null && this.ItemsSource.Count > 0) {
				s = "Filter for subjects without a year/term assigned";
			}

			if (s == null) {
				this.Visibility = ViewStates.Gone;

				this.Text = "Hidden";
			}
			else {
				this.Visibility = ViewStates.Visible;

				this.Text = s;
			}
		}

		#endregion
	}
    
    [Register("mystudylife.droid.widgets.InputAcademicSchedulePicker")]
	public class InputAcademicSchedulePicker : AcademicScheduleSelector {
		#region Constructors

		protected InputAcademicSchedulePicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
		public InputAcademicSchedulePicker(Context context) : this(context, null) {}
        public InputAcademicSchedulePicker(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.pickerStyle) { }
        public InputAcademicSchedulePicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

		#endregion

		#region Overrides of AcademicScheduleSelector

        protected override string HelpText {
            get { return R.SubjectInput.AcademicScheduleInputHelp; }
			}

		protected override void SetButtonTextImpl() {
		    this.Text = this.SelectedItem == null ? null : this.SelectedItem.ToString();
		}

		#endregion
	}
}