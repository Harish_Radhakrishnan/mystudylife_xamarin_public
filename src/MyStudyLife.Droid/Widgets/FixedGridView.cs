using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.MvxFixedGridView")]
    public class MvxFixedGridView : MvxGridView {
		public MvxFixedGridView(Context context, IAttributeSet attrs) : base(context, attrs) { }
        public MvxFixedGridView(Context context, IAttributeSet attrs, IMvxAdapter adapter)
			: base(context, attrs, adapter) { }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {   
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            int expandSpec = MeasureSpec.MakeMeasureSpec(MeasuredSizeMask, MeasureSpecMode.AtMost);

			base.OnMeasure(widthMeasureSpec, expandSpec);

			LayoutParameters.Height = MeasuredHeight;
		}
    }

    [Register("mystudylife.droid.widgets.FixedGridView")]
	public class FixedGridView : GridView{

		public FixedGridView(Context context, IAttributeSet attrs) : base(context, attrs) { }

		protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {   
            // Calculate entire height by providing a very large height hint.
            // View.MEASURED_SIZE_MASK represents the largest height possible.
            int expandSpec = MeasureSpec.MakeMeasureSpec(MeasuredSizeMask, MeasureSpecMode.AtMost);

			base.OnMeasure(widthMeasureSpec, expandSpec);

			LayoutParameters.Height = MeasuredHeight;
		}
	}
}