using System;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Util;
using AndroidX.AppCompat.Widget;
using AndroidX.Core.Graphics.Drawable;

namespace MyStudyLife.Droid.Widgets {
    /// <summary>
    ///     Basic backwards compatible checkbox for runtime tinting. <see cref="AppCompatCheckBox"/>
    ///     doesn't expose <c>ButtonTintList</c> below API 21 :S
    /// </summary>
    [Register("mystudylife.droid.widgets.MslCheckBox")]
    public sealed class MslCheckBox : AppCompatCheckBox {
        private Drawable _buttonDrawable;
        private Color _colorControlNormal;
        private Color _checkedTintColor;

        public Color CheckedTintColor {
            get { return _checkedTintColor; }
            set {
                if (_checkedTintColor != value) {
                    _checkedTintColor = value;

                    if (_buttonDrawable != null) {
                        SetButtonDrawable(_buttonDrawable);
                    }
                }
            }
        }

        private MslCheckBox(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public MslCheckBox(Context context) : this(context, null) { }
        public MslCheckBox(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.checkboxStyle) { }
        public MslCheckBox(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            var a = context.Theme.ObtainStyledAttributes(new[] { Resource.Attribute.colorControlNormal, Resource.Attribute.colorControlActivated });

            _colorControlNormal = a.GetColor(0, Color.Black);
            _checkedTintColor = a.GetColor(1, Color.Black);

            a.Recycle();
        }

        public override void SetButtonDrawable(Drawable d) {
            _buttonDrawable = d;

            // if required as SetButtonDrawable is called by the base ctor
            // before we can set the colors
            if (_colorControlNormal != default(Color)) {
                DrawableCompat.SetTintList(d, new ColorStateList(
                    new[] {
                        new[] { Android.Resource.Attribute.StateChecked },
                        new int[0]
                    },
                    new[] {
                        (int) _checkedTintColor,
                        _colorControlNormal
                    }
                ));
            }

            base.SetButtonDrawable(d);
        }
    }
}