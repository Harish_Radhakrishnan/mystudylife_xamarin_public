using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Input;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using MvvmCross.Binding.Attributes;
using MvvmCross.Binding.BindingContext;
using MvvmCross;
using MvvmCross.Binding.Extensions;
using MvvmCross.Exceptions;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.WeakSubscription;
using Microsoft.Extensions.Logging;
using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Widgets {
    // ReSharper disable once IdentifierTypo
    [Register("mystudylife.droid.widgets.MvxRecyclerView")]
    public class MvxRecyclerView : RecyclerView {
        private bool _temporarilyDetached = false;

        public new MvxRecyclerAdapter Adapter {
            get { return base.GetAdapter() as MvxRecyclerAdapter; }
            set {
                var existing = Adapter;

                if (existing == value) {
                    return;
                }

                if (value != null && existing != null) {
                    value.ItemsSource = existing.ItemsSource;
                    value.ItemTemplateId = existing.ItemTemplateId;
                    value.ItemClick = existing.ItemClick;
                    value.ItemLongClick = existing.ItemLongClick;

                    SwapAdapter(value, false);
                }
                else {
                    SetAdapter(value);
                }

                if (existing != null) {
                    existing.ItemsSource = null;
                }
            }
        }

        [MvxSetToNullAfterBinding]
        public IEnumerable ItemsSource {
            get { return Adapter.ItemsSource; }
            set { Adapter.ItemsSource = value; }
        }

        public int ItemTemplateId {
            get { return Adapter.ItemTemplateId; }
            set { Adapter.ItemTemplateId = value; }
        }

        public ICommand ItemClick {
            get { return this.Adapter.ItemClick; }
            set { this.Adapter.ItemClick = value; }
        }

        public ICommand ItemLongClick {
            get { return this.Adapter.ItemLongClick; }
            set { this.Adapter.ItemLongClick = value; }
        }

        #region ctor

        protected MvxRecyclerView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public MvxRecyclerView(Context context, IAttributeSet attrs) : this(context, attrs, 0, new MvxRecyclerAdapter()) { }
        public MvxRecyclerView(Context context, IAttributeSet attrs, int defStyle) : this(context, attrs, defStyle, new MvxRecyclerAdapter()) { }
        public MvxRecyclerView(Context context, IAttributeSet attrs, int defStyle, MvxRecyclerAdapter adapter) : base(context, attrs, defStyle) {
            // Note: Any calling derived class passing a null adapter is responsible for setting
            // it's own itemTemplateId
            if (adapter == null) {
                return;
            }

            SetLayoutManager(new LinearLayoutManager(context));

            var itemTemplateId = MvxAttributeHelpers.ReadListItemTemplateId(context, attrs);
            adapter.ItemTemplateId = itemTemplateId;
            Adapter = adapter;
        }

        #endregion

        public sealed override void SetLayoutManager(LayoutManager layout) {
            base.SetLayoutManager(layout);
        }

        public override void OnStartTemporaryDetach() {
            _temporarilyDetached = true;
            base.OnStartTemporaryDetach();
        }

        public override void OnFinishTemporaryDetach() {
            _temporarilyDetached = false;
            base.OnFinishTemporaryDetach();
        }

        protected override void OnDetachedFromWindow() {
            base.OnDetachedFromWindow();

            if (!_temporarilyDetached) {
                // Clear out all bindings from the ViewHolders created by the current adapter.
                // This is for https://github.com/MvvmCross/MvvmCross/issues/1379
                // According to the documentation Adapters can be owned by multiple
                // RecyclerViews so this might not be entirely accurate.
                this.Adapter?.ClearAllBindings();
            }
        }
    }

    // Support lib doesn't seem to have anything similar to IListAdapter yet
    public interface IMvxRecyclerAdapter {
        [MvxSetToNullAfterBinding]
        IEnumerable ItemsSource { get; set; }

        int ItemTemplateId { get; set; }
        ICommand ItemClick { get; set; }
        ICommand ItemLongClick { get; set; }

        object GetItem(int position);

        void ClearAllBindings();
    }

    public class MvxRecyclerAdapter : RecyclerView.Adapter, IMvxRecyclerAdapter, MvxRecyclerViewHolder.IClickPropagator {

        public event EventHandler DataSetChanged;

        private readonly IMvxAndroidBindingContext _bindingContext;

        // Keep track of all ViewHolders created by this adapter.
        private readonly List<WeakReference<IMvxRecyclerViewHolder>> _viewHolders = new List<WeakReference<IMvxRecyclerViewHolder>>();
        private int _attachedRecyclerViews;

        private IEnumerable _itemsSource;
        private IDisposable _subscription;
        private int _itemTemplateId;

        protected IMvxAndroidBindingContext BindingContext => _bindingContext;

        public MvxRecyclerAdapter() : this(MvxAndroidBindingContextHelpers.Current()) { }
        public MvxRecyclerAdapter(IMvxAndroidBindingContext bindingContext) {
            this._bindingContext = bindingContext;
        }

        public bool ReloadOnAllItemsSourceSets { get; set; }

        public ICommand ItemClick { get; set; }
        public ICommand ItemLongClick { get; set; }

        [MvxSetToNullAfterBinding]
        public virtual IEnumerable ItemsSource {
            get { return _itemsSource; }
            set { SetItemsSource(value); }
        }

        public virtual int ItemTemplateId {
            get { return _itemTemplateId; }
            set {
                if (_itemTemplateId == value) {
                    return;
                }

                _itemTemplateId = value;

                // since the template has changed then let's force the list to redisplay by firing NotifyDataSetChanged()
                if (_itemsSource != null) {
                    NotifyAndRaiseDataSetChanged();
                }
            }
        }

        public override void OnViewAttachedToWindow(Java.Lang.Object holder) {
            base.OnViewAttachedToWindow(holder);

            var viewHolder = (IMvxRecyclerViewHolder)holder;
            viewHolder.OnAttachedToWindow();
        }

        public override void OnViewDetachedFromWindow(Java.Lang.Object holder) {
            base.OnViewDetachedFromWindow(holder);

            var viewHolder = (IMvxRecyclerViewHolder)holder;
            viewHolder.OnDetachedFromWindow();
        }



        public override void OnViewRecycled(Java.Lang.Object holder) {
            base.OnViewRecycled(holder);

            var viewHolder = (IMvxRecyclerViewHolder)holder;
            viewHolder.OnViewRecycled();
        }

        public override void OnAttachedToRecyclerView(RecyclerView recyclerView) {
            ++_attachedRecyclerViews;
            base.OnAttachedToRecyclerView(recyclerView);
        }

        public override void OnDetachedFromRecyclerView(RecyclerView recyclerView) {
            --_attachedRecyclerViews;
            base.OnDetachedFromRecyclerView(recyclerView);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, _bindingContext.LayoutInflaterHolder);

            return new MvxRecyclerViewHolder(InflateViewForHolder(parent, ItemTemplateId, itemBindingContext), itemBindingContext) {
                ClickPropagator = this,
                Click = ItemClick,
                LongClick = ItemLongClick
            };
        }

        protected virtual View InflateViewForHolder(ViewGroup parent, int viewType, IMvxAndroidBindingContext bindingContext) {
            return bindingContext.BindingInflate(this._itemTemplateId, parent, false);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((MvxRecyclerViewHolder)holder).DataContext = _itemsSource.ElementAt(position);
        }

        public override int ItemCount {
            get { return _itemsSource.Count(); }
        }

        public virtual object GetItem(int position) {
            return _itemsSource.ElementAt(position);
        }

        protected virtual void SetItemsSource(IEnumerable value) {
            if (ReferenceEquals(_itemsSource, value) && !ReloadOnAllItemsSourceSets) {
                return;
            }

            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            _itemsSource = value;

            if (_itemsSource != null && !(_itemsSource is IList)) {
                Mvx.IoCProvider.Resolve<ILogger<MvxRecyclerAdapter>>().LogWarning(
                    "Binding to IEnumerable rather than IList - this can be inefficient, especially for large lists");
            }

            if (_itemsSource is INotifyCollectionChanged newObservable) {
                _subscription = newObservable.WeakSubscribe(OnItemsSourceCollectionChanged);
            }

            NotifyAndRaiseDataSetChanged();
        }

        protected virtual void OnItemsSourceCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            NotifyDataSetChanged(e);
        }

        public virtual void NotifyDataSetChanged(NotifyCollectionChangedEventArgs e) {
            try {
                switch (e.Action) {
                    case NotifyCollectionChangedAction.Add:
                        this.NotifyItemRangeInserted(e.NewStartingIndex, e.NewItems.Count);
                        this.RaiseDataSetChanged();
                        break;
                    case NotifyCollectionChangedAction.Move:
                        for (int i = 0; i < e.NewItems.Count; i++) {
                            this.NotifyItemMoved(e.OldStartingIndex + i, e.NewStartingIndex + i);
                            this.RaiseDataSetChanged();
                        }
                        break;
                    case NotifyCollectionChangedAction.Replace:
                        this.NotifyItemRangeChanged(e.NewStartingIndex, e.NewItems.Count);
                        this.RaiseDataSetChanged();
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        this.NotifyItemRangeRemoved(e.OldStartingIndex, e.OldItems.Count);
                        this.RaiseDataSetChanged();
                        break;
                    case NotifyCollectionChangedAction.Reset:
                        this.NotifyAndRaiseDataSetChanged();
                        break;
                }
            }
            catch (Exception exception) {
                Mvx.IoCProvider.Resolve<ILogger<MvxRecyclerAdapter>>().LogWarning("Exception masked during Adapter RealNotifyDataSetChanged {0}", exception.ToLongString());
            }
        }

        private void RaiseDataSetChanged() {
            DataSetChanged?.Invoke(this, EventArgs.Empty);
        }

        private void NotifyAndRaiseDataSetChanged() {
            this.RaiseDataSetChanged();
            this.NotifyDataSetChanged();
        }

        public event EventHandler OnItemClick;
        public event EventHandler OnItemLongClick;

        public void HandleClick(MvxRecyclerViewHolder viewHolder) {
            OnItemClick?.Invoke(viewHolder, EventArgs.Empty);
        }

        public void HandleLongClick(MvxRecyclerViewHolder viewHolder) {
            OnItemLongClick?.Invoke(viewHolder, EventArgs.Empty);
        }

        public virtual void ClearAllBindings() {
            // Only clear bindings if we're attached to at least one RecyclerView.
            // This might be wrong if we're sharing the adapter with multiple RecyclerViews but
            // I haven't hit this case in the wild.  We may need to revisit this if it's a problem.
            if (_attachedRecyclerViews > 0) {
                foreach (var vhRef in _viewHolders) {
                    IMvxRecyclerViewHolder vh;
                    if (vhRef.TryGetTarget(out vh))
                        vh.ClearAllBindings();
                }

                _viewHolders.Clear();
            }
        }
    }

    public interface IMvxRecyclerViewHolder : IMvxBindingContextOwner {
        object DataContext { get; set; }

        void OnAttachedToWindow();
        void OnDetachedFromWindow();
        void OnViewRecycled();
    }

    public class MvxRecyclerViewHolder : RecyclerView.ViewHolder, IMvxRecyclerViewHolder {
        private readonly IMvxBindingContext _bindingContext;

        private object _cachedDataContext;
        private ICommand _click, _longClick;
        private bool _clickOverloaded, _longClickOverloaded;

        public IMvxBindingContext BindingContext {
            get { return _bindingContext; }
            set { throw new NotImplementedException("BindingContext is readonly in the list item"); }
        }

        public object DataContext {
            get { return _bindingContext.DataContext; }
            set { _bindingContext.DataContext = value; }
        }
        
        public ICommand Click {
            get { return this._click; }
            set { this._click = value; if (this._click != null) this.EnsureClickOverloaded(); }
        }

        private void EnsureClickOverloaded() {
            if (this._clickOverloaded)
                return;
            this._clickOverloaded = true;
            this.ItemView.Click += (sender, args) => ExecuteCommandOnItem(this.Click);
        }

        public ICommand LongClick {
            get { return this._longClick; }
            set { this._longClick = value; if (this._longClick != null) this.EnsureLongClickOverloaded(); }
        }

        private void EnsureLongClickOverloaded() {
            if (this._longClickOverloaded)
                return;
            this._longClickOverloaded = true;
            this.ItemView.LongClick += (sender, args) => ExecuteCommandOnItem(this.LongClick);
        }

        protected virtual void ExecuteCommandOnItem(ICommand command) {
            if (command == null)
                return;

            var item = DataContext;
            if (item == null)
                return;

            if (!command.CanExecute(item))
                return;

            command.Execute(item);
        }

        public MvxRecyclerViewHolder(View itemView, IMvxAndroidBindingContext context)
            : base(itemView) {
            this._bindingContext = context;
        }

        public void OnAttachedToWindow() {
            if (_cachedDataContext != null && DataContext == null)
                DataContext = _cachedDataContext;
        }

        public void OnDetachedFromWindow() {
            _cachedDataContext = DataContext;
            DataContext = null;
        }

        public void OnViewRecycled() {
            _cachedDataContext = null;
            DataContext = null;
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _bindingContext.ClearAllBindings();
                _cachedDataContext = null;
            }

            base.Dispose(disposing);
        }

        private bool _clicksOverloaded;
        private IClickPropagator _clickPropagator;
        
        public IClickPropagator ClickPropagator {
            get { return _clickPropagator; }
            set { _clickPropagator = value; if (this._clickPropagator != null) EnsureClicksOverloaded(); }
        }

        private void EnsureClicksOverloaded() {
            if (this._clicksOverloaded) {
                return;
            }

            this._clicksOverloaded = true;

            this.ItemView.Click += (s, e) => PropagateClick(ClickPropagator.HandleClick);
            this.ItemView.LongClick += (s, e) => PropagateClick(ClickPropagator.HandleLongClick);
        }
        
        private void PropagateClick(Action<MvxRecyclerViewHolder> action) {
            if (DataContext == null) return;

            action(this);
        }

        public interface IClickPropagator {
            void HandleClick(MvxRecyclerViewHolder viewHolder);

            void HandleLongClick(MvxRecyclerViewHolder viewHolder);
        }
    }
}