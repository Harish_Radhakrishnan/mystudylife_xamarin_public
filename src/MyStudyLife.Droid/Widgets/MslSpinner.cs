using Android.Content;
using Android.Runtime;
using Android.Util;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Widgets {
    // Fixes a bug whereby the selected item gets reset
    // when the adapter is set.
    [Register("mystudylife.droid.widgets.MslSpinner")]
    public class MslSpinner : MvxSpinner {
        public new IMvxAdapter Adapter {
            get { return base.Adapter; }
            set {
                var existing = Adapter;

                if (existing == value) {
                    return;
                }

                if (existing != null && value != null) {
                    value.ItemsSource = existing.ItemsSource;
                    value.ItemTemplateId = existing.ItemTemplateId;
                }

                var currentSelectedItem = this.SelectedItemPosition;

                base.Adapter = value;

                if (value != null && currentSelectedItem < value.Count) {
                    this.SetSelection(currentSelectedItem);
                }
            }
        }

        #region ctor(...)

        public MslSpinner(Context context, IAttributeSet attrs) : base(context, attrs) {}
        public MslSpinner(Context context, IAttributeSet attrs, IMvxAdapter adapter) : base(context, attrs, adapter) {}
    
        #endregion
    }
}