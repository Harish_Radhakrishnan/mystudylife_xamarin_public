using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using MvvmCross.Binding;
using MyStudyLife.Globalization;
using TimePickerDialog = Wdullaer.MaterialDateTimePicker.Time.TimePickerDialog;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Platforms.Android.Views.Base;
using AndroidX.AppCompat.App;
using Wdullaer.MaterialDateTimePicker.Date;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.TimePicker")]
    public class TimePicker : Picker, TimePickerDialog.IOnTimeSetListener {
        private const string FragmentTag = "time_picker";

        public System.Drawing.Color AccentColor { get; set; }

        public event EventHandler ValueChanged;

        private TimeSpan _value = DateTimeEx.Now.TimeOfDay;

        public TimeSpan Value {
            get => _value;
            set {
                _value = value;

                this.Text = value.ToDateTime().ToString("t");
            }
        }

        #region Constructors

        protected TimePicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public TimePicker(Context context) : base(context) {
            Initialize();
        }
        public TimePicker(Context context, IAttributeSet attrs) : base(context, attrs) {
            Initialize();
        }
        public TimePicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            Initialize();
        }

        private void Initialize() {
            var eventSourceActivity = this.Context as IMvxEventSourceActivity;

            if (eventSourceActivity == null) {
                throw new InvalidOperationException("DatePicker can only be used in activities which implement IMvxEventSourceActivity");
            }

            eventSourceActivity.ResumeCalled += EventSourceActivityOnResumeCalled;
        }

        // Reattach on rotate or app resume
        private void EventSourceActivityOnResumeCalled(object sender, EventArgs e) {
            var activity = (AndroidX.AppCompat.App.AppCompatActivity)sender;

            var picker = (TimePickerDialog)activity.SupportFragmentManager.FindFragmentByTag(FragmentTag);

            if (picker != null) {
                picker.OnTimeSetListener = this;
            }
        }
        #endregion

        protected override void OnClick() {
            var picker = TimePickerDialog.NewInstance(this, this.Value.Hours, this.Value.Minutes, DateTimeFormat.Is24Hour);
            picker.AccentColor = AccentColor.ToArgb();
            picker.Version = TimePickerDialog.AppVersion.Version1;

            picker.Show(((AppCompatActivity)this.Context).SupportFragmentManager, FragmentTag);
        }

        #region Implementation of IOnTimeSetListener

        public void OnTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            this.Value = new TimeSpan(hourOfDay, minute, 0);

            ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }

    public class TimePickerAccentColorBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TimePicker>("AccentColor", view => new TimePickerAccentColorBinding(view));
        }

        public new TimePicker Target {
            get { return (TimePicker)base.Target; }
        }

        public override Type TargetType => typeof(System.Drawing.Color);

        public TimePickerAccentColorBinding(object target) : base(target) {
        }

        protected override void SetValueImpl(object target, object value) {
            ((TimePicker)target).AccentColor = (System.Drawing.Color)value;
        }
    }

    public class TimePickerValueBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TimePicker>("Value", view => new TimePickerValueBinding(view));
        }

        public new TimePicker Target {
            get { return (TimePicker)base.Target; }
        }

        public TimePickerValueBinding(TimePicker target) : base(target) { }

        #region Overrides of MvxTargetBinding

        public override void SubscribeToEvents() {
            this.Target.ValueChanged += TargetOnValueChanged;
        }

        #endregion

        protected override void SetValueImpl(object target, object value) {
            ((TimePicker)target).Value = (TimeSpan)value;
        }

        public override Type TargetType {
            get { return typeof(TimeSpan); }
        }

        public override MvxBindingMode DefaultMode {
            get { return MvxBindingMode.TwoWay; }
        }

        #region Overrides of MvxBinding

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                this.Target.ValueChanged -= TargetOnValueChanged;
            }

            base.Dispose(isDisposing);
        }

        private void TargetOnValueChanged(object sender, EventArgs e) {
            if (this.Target == null) return;

            this.FireValueChanged(this.Target.Value);
        }

        #endregion
    }
}