using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using AndroidX.AppCompat.App;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Platforms.Android.Views.Base;
using MyStudyLife.Globalization;
using DatePickerDialog = Wdullaer.MaterialDateTimePicker.Date.DatePickerDialog;

namespace MyStudyLife.Droid.Widgets {
    [Register("mystudylife.droid.widgets.DatePicker")]
    public class DatePicker : Picker, DatePickerDialog.IOnDateSetListener {
        private const string FragmentTag = "date_picker";

        public System.Drawing.Color? AccentColor { get; set; }

        public event EventHandler ValueChanged;

        // private readonly string _propertyPath;
        private DateTime _value = DateTimeEx.Now;

        public DateTime Value {
            get { return _value; }
            set {
                _value = value;

                var dateStr = value.ToString(DateTimeFormat.ShortLongDatePattern);

                if (!String.IsNullOrEmpty(FormatText)) {
                    this.Text = String.Format(FormatText, dateStr);
                }
                else {
                    this.Text = dateStr;
                }
            }
        }

        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }

        public string FormatText { get; set; }

        #region Constructors

        protected DatePicker(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public DatePicker(Context context) : this(context, null) { }
        public DatePicker(Context context, IAttributeSet attrs) : this(context, attrs, Resource.Attribute.pickerStyle) { }
        public DatePicker(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.DatePicker);

            FormatText = a.GetText(Resource.Styleable.DatePicker_dp_formatText);

            a.Recycle();

            Initialize();
        }

        private void Initialize() {
            var eventSourceActivity = this.Context as IMvxEventSourceActivity;

            if (eventSourceActivity == null) {
                throw new InvalidOperationException("DatePicker can only be used in activities which implement IMvxEventSourceActivity");
            }

            eventSourceActivity.ResumeCalled += EventSourceActivityOnResumeCalled;
        }

        // Reattach on rotate or app resume
        private void EventSourceActivityOnResumeCalled(object sender, EventArgs e) {
            var activity = (AppCompatActivity)sender;

            var picker = (DatePickerDialog)activity.SupportFragmentManager.FindFragmentByTag(FragmentTag);

            if (picker != null) {
                picker.OnDateSetListener = this;
            }
        }

        #endregion

        protected override void OnClick() {
            var picker = DatePickerDialog.NewInstance(this, this.Value.Year, this.Value.Month - 1, this.Value.Day);
            picker.SetVersion(DatePickerDialog.Version.Version1);
            picker.FirstDayOfWeek = (int)DateTimeFormat.FirstDayOfWeek + 1;

            var minDate = this.MinDate;
            var maxDate = this.MaxDate;
            var accentColor = this.AccentColor;

            // For some reason, ? doesn't work with the Calendar extension method (odd as it used to) - must be a Xamarin bug
            if (minDate.HasValue) {
                picker.MinDate = minDate.Value.ToCalendar();
            }

            if (maxDate.HasValue) {
                picker.MaxDate = maxDate.Value.ToCalendar();
            }

            if (accentColor != null) {
                picker.AccentColor = accentColor.Value.ToArgb();
            }

            picker.Show((((AppCompatActivity)this.Context).SupportFragmentManager), FragmentTag);
        }

        public void OnDateSet(DatePickerDialog picker, int year, int monthOfYear, int dayOfMonth) {
            this.Value = new DateTime(year, monthOfYear + 1, dayOfMonth, this.Value.Hour, this.Value.Minute, this.Value.Second, this.Value.Millisecond);

            this.ValueChanged?.Invoke(this, EventArgs.Empty);
        }

        public class ValueBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<DatePicker>("Value", picker => new ValueBinding(picker));
            }

            public new DatePicker Target => (DatePicker)base.Target;
            public override Type TargetType => typeof(DateTime);
            public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

            public ValueBinding(DatePicker target) : base(target) { }

            public override void SubscribeToEvents() {
                this.Target.ValueChanged += TargetOnValueChanged;
            }

            protected override void SetValueImpl(object target, object value) {
                ((DatePicker)target).Value = (DateTime)value;
            }

            protected override void Dispose(bool isDisposing) {
                if (isDisposing) {
                    this.Target.ValueChanged -= TargetOnValueChanged;
                }

                base.Dispose(isDisposing);
            }

            private void TargetOnValueChanged(object sender, EventArgs e) {
                if (this.Target == null) return;

                this.FireValueChanged(this.Target.Value);
            }
        }

        public class MinDateBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<DatePicker>("MinDate", picker => new MinDateBinding(picker));
            }

            public new DatePicker Target => (DatePicker)base.Target;
            public override Type TargetType => typeof(DateTime);
            public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

            public MinDateBinding(DatePicker target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                ((DatePicker)target).MinDate = (DateTime)value;
            }
        }

        public class MaxDateBinding : MvxAndroidTargetBinding {
            public static void Register(IMvxTargetBindingFactoryRegistry registry) {
                registry.RegisterCustomBindingFactory<DatePicker>("MaxDate", picker => new MaxDateBinding(picker));
            }

            public new DatePicker Target => (DatePicker)base.Target;
            public override Type TargetType => typeof(DateTime);
            public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

            public MaxDateBinding(DatePicker target) : base(target) { }

            protected override void SetValueImpl(object target, object value) {
                ((DatePicker)target).MaxDate = (DateTime)value;
            }
        }
    }

    public class DatePickerAccentColorBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<DatePicker>("AccentColor", view => new DatePickerAccentColorBinding(view));
        }

        public new DatePicker Target {
            get { return (DatePicker)base.Target; }
        }

        public override Type TargetType => typeof(System.Drawing.Color);

        public DatePickerAccentColorBinding(object target) : base(target) {
        }

        protected override void SetValueImpl(object target, object value) {
            ((DatePicker)target).AccentColor = (System.Drawing.Color?)value;
        }
    }
}