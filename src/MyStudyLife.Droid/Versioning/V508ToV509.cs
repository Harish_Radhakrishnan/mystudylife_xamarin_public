using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.Droid.Versioning {
    public sealed class V508ToV509 : SchoolLogoUpgrade {
        public override Version FromVersion => new Version(5, 0, 8);
        public override Version ToVersion => new Version(5, 0, 9);
    }
}