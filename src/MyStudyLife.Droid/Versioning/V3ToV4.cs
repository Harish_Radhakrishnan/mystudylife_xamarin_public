using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.Droid.Versioning {
    public sealed class V3ToV4 : HolidaysUpgrade {
        public override Version FromVersion {
            get { return new Version(3,0); }
        }

        public override Version ToVersion {
            get { return new Version(4,0); }
        }
    }
}