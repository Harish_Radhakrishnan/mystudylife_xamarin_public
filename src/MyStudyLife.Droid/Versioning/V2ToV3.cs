using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.Droid.Versioning {
    public sealed class V2ToV3 : TeacherSharingUpgrade {
        public override Version FromVersion {
            get { return new Version(2,0); }
        }

        public override Version ToVersion {
            get { return new Version(3,0); }
        }
    }
}