using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Common {
    public class DividerDecoration : RecyclerView.ItemDecoration {
        private readonly Drawable _divider;
        private readonly int _dividerHeight;

        public DividerDecoration(Context context) {
            _divider = new ColorDrawable(ContextCompatEx.GetColor(context, Resource.Color.border));
            _dividerHeight = context.Resources.DisplayMetrics.DpsToPixels(1);
        }

        public override void OnDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            base.OnDraw(c, parent, state);

            int left = parent.PaddingLeft;
            int right = parent.Width - parent.PaddingRight;

            int childCount = parent.ChildCount;
            for (int i = 0; i < childCount; i++) {
                View child = parent.GetChildAt(i);
                var @params = child.LayoutParameters.JavaCast<RecyclerView.LayoutParams>();
                int top = child.Bottom + @params.BottomMargin;
                int bottom = top + _dividerHeight;
                _divider.SetBounds(left, top, right, bottom);
                _divider.Draw(c);
            }
        }

        public override void GetItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            base.GetItemOffsets(outRect, view, parent, state);

            outRect.Set(0, 0, 0, _dividerHeight);
        }
    }
}