using Android.Content;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Common {
	public class DefaultAdapter : MvxAdapter {
		public DefaultAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext) {
			this.DropDownItemTemplateId = Resource.Layout.SpinnerDropDownItem;
			this.ItemTemplateId = Resource.Layout.SpinnerItem;
		}
	}
}