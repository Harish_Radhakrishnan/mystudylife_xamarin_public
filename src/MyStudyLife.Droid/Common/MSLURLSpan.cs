using System;
using Android.Text.Style;
using Android.Views;
using MvvmCross;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.Droid.Common {
    class MSLURLSpan : URLSpan {
        public MSLURLSpan(String url) : base(url) { }

        public override void OnClick(View widget) {
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new Uri(this.URL));
        }
    }
}