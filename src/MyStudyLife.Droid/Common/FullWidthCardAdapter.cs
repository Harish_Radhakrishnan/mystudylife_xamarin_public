using System;
using Android.Content;
using Android.Runtime;
using Android.Views;
using AndroidX.CardView.Widget;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Common {
    public class FullWidthCardAdapter : MvxAdapterWithChangedEvent {
        public FullWidthCardAdapter(Context context) : base(context) { }
        protected FullWidthCardAdapter(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer) { }

        protected override IMvxListItemView CreateBindableView(object dataContext, ViewGroup parent, int templateId)
            => new ListItemView(Context, BindingContext.LayoutInflaterHolder, dataContext, parent, templateId);

        protected class ListItemView : MvxListItemView {
            private readonly CardView _cardView;

            public ListItemView(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext, ViewGroup parent, int templateId)
                : base(context, layoutInflater, dataContext, parent, templateId) {

                _cardView = RecursiveFindCardView(Content);
                
                if (_cardView == null) {
                    throw new NotSupportedException($"Could not find CardView in hierarchy for templateId '{templateId}'");
                }

                Content.ViewAttachedToWindow += ContentOnViewAttachedToWindow;
            }

            private void ContentOnViewAttachedToWindow(object sender, View.ViewAttachedToWindowEventArgs e) {
                AlignToEdges();
            }

            private CardView RecursiveFindCardView(View view) {
                if (view is CardView cardView) {
                    return cardView;
                }

                if (view is ViewGroup viewGroup) {
                    for (int i = 0; i < viewGroup.ChildCount; i++) {
                        cardView = RecursiveFindCardView(viewGroup.GetChildAt(i));
                        if (cardView != null) {
                            return cardView;
                        }
                    }
                }

                return null;
            }

            private void AlignToEdges() {
                var layoutParams = (ViewGroup.MarginLayoutParams)Content.LayoutParameters;
                if (layoutParams == null) {
                    return;
                }

                var card = _cardView;

                // Normalize margin based on the card's elevation, this also allows
                // views to overlap since clipping doesn't seem to play nice with CardView
                if (layoutParams.LeftMargin != -card.PaddingLeft) {
                    layoutParams.LeftMargin = -card.PaddingLeft;
                    layoutParams.TopMargin = -card.PaddingTop;
                    layoutParams.RightMargin = -card.PaddingRight;
                    layoutParams.BottomMargin = -card.PaddingBottom;
                    Content.RequestLayout();
                }
            }
        }
    }
}