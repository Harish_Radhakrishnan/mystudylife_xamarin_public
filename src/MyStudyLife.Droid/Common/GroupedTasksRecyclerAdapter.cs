using System;
using System.ComponentModel;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.Data;

namespace MyStudyLife.Droid.Common {
    abstract class GroupedTasksRecyclerAdapter : MvxGroupedRecyclerAdapter<TaskGrouping> {
        public GroupedTasksRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

        protected override void OnBindHeaderViewHolder(AndroidX.RecyclerView.Widget.RecyclerView.ViewHolder holder, int position, TaskGrouping grouping)
        {
            base.OnBindHeaderViewHolder(holder, position, grouping);
            if (grouping == null) return;

            var textView = (TextView)holder.ItemView;

            switch (grouping.Kind)
            {
                case TaskGroupingKind.Overdue:
                    textView.SetTextColor(ContextCompatEx.GetColor(textView.Context, Resource.Color.attention_foreground));
                    break;
                case TaskGroupingKind.DueSoon:
                    textView.SetTextColor(ContextCompatEx.GetColor(textView.Context, Resource.Color.warning_foreground));
                    break;
            }
        } 
    
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, BindingContext.LayoutInflaterHolder);

            return new ViewHolder(InflateViewForHolder(parent, ItemTemplateId, itemBindingContext), itemBindingContext) {
                ClickPropagator = this,
                Click = ItemClick,
                LongClick = ItemLongClick
            };
        }

        class ViewHolder : MvxRecyclerViewHolder, ITaskListItem {
            private IDisposable _subscription;

            private readonly Lazy<View> _colorView;
            private readonly Lazy<View> _innerView;
            private readonly Lazy<TextView> _titleView;
            private readonly Lazy<TextView> _subTitleView;
            private readonly Lazy<TextView> _captionView;

            public View ColorView => _colorView.Value;
            public View InnerView => _innerView.Value;
            public TextView TitleView => _titleView.Value;
            public TextView SubTitleView => _subTitleView.Value;
            public TextView CaptionView => _captionView.Value;

            public ViewHolder(View itemView, IMvxAndroidBindingContext context) : base(itemView, context) {
                _colorView = new Lazy<View>(() => itemView.FindViewById(Resource.Id.ListItemColor));
                _innerView = new Lazy<View>(() => itemView.FindViewById(Resource.Id.ListItemInner));
                _titleView = new Lazy<TextView>(() => itemView.FindViewById<TextView>(Resource.Id.ListItemTitle));
                _subTitleView = new Lazy<TextView>(() => itemView.FindViewById<TextView>(Resource.Id.ListItemSubTitle));
                _captionView = new Lazy<TextView>(() => itemView.FindViewById<TextView>(Resource.Id.ListItemCaption));

                BindingContext.DataContextChanged += (s, e) => OnDataContextChanged();

                OnDataContextChanged();
            }

            private void OnDataContextChanged() {
                _subscription?.Dispose();

                if (DataContext is Task task) {
                    _subscription = task.WeakSubscribe(DataContextOnPropertyChanged);
                    this.ApplyStyles(ItemView.Context);
                }
                else {
                    _subscription = null;
                }
            }

            private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
                if (e.PropertyName.In(nameof(Task.IsComplete), nameof(Task.DueDate))) {
                    this.ApplyStyles(ItemView.Context);
                }
            }
        }
    }
}