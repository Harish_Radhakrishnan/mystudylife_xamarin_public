using Android.Content;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Data;

namespace MyStudyLife.Droid.Common {
    internal static class TaskListItemHelper {
        public static void ApplyStyles(this ITaskListItem listItem, Context context) {
            var task = (listItem.BindingContext.DataContext as Task);

            if (task == null) {
                return;
            }

            var isComplete = task.IsComplete;
            var alpha = isComplete ? 0.5f : 1;

            if (listItem.ColorView != null) {
                listItem.ColorView.Alpha = alpha;
            }
            listItem.InnerView.Alpha = alpha;

            var colorResId = Resource.Color.foreground_mid;

            if (!isComplete) {
                if (task.IsIncompleteAndDueSoon) {
                    colorResId = Resource.Color.warning_foreground;
                }
                else if (task.IsOverdue) {
                    colorResId = Resource.Color.attention_foreground;
                }
            }

            listItem.CaptionView.SetTextColor(ContextCompatEx.GetColor(context, colorResId));
        }
    }

    internal interface ITaskListItem : IMvxBindingContextOwner {
        View ColorView { get; }
        View InnerView { get; }
        TextView TitleView { get; }
        TextView SubTitleView { get; }
        TextView CaptionView { get; }
    }
}