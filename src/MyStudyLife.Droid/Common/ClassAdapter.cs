using System;
using System.ComponentModel;
using Android.Views;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Utility;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.WeakSubscription;
using Microsoft.Extensions.Logging;
using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Common {
    public class ClassAdapter : MvxRecyclerAdapter {
        private readonly Lazy<bool> _canUseExtendedColors;
        
        private BaseViewModel ViewModel => (BaseViewModel)BindingContext.DataContext;

        public ClassAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) {

            _canUseExtendedColors = new Lazy<bool>(() => Mvx.IoCProvider.Resolve<IFeatureService>()
                .CanUseFeatureAsync(Feature.ExtendedColors)
                .GetAwaiter().GetResult());
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, BindingContext.LayoutInflaterHolder);

            return new ViewHolder(InflateViewForHolder(parent, ItemTemplateId, itemBindingContext), itemBindingContext) {
                ClickPropagator = this,
                Click = ItemClick,
                LongClick = ItemLongClick
            };
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((ViewHolder)holder).CanUseExtendedColors = _canUseExtendedColors.Value;
            
            base.OnBindViewHolder(holder, position);

            var cls = (Class)GetItem(position);
            var user = ViewModel.User;

            holder.ItemView.FindViewById(Resource.Id.SchoolIndicator)?.SetVisible(
                cls.IsFromSchool && (!user.School?.IsManaged).GetValueOrDefault()
            );
        }

        class ViewHolder : MvxRecyclerViewHolder {
            private IDisposable _colorSubscription;

            public bool CanUseExtendedColors { get; set; }

            public ViewHolder(View itemView, IMvxAndroidBindingContext context) : base(itemView, context) {
                context.DataContextChanged += Context_DataContextChanged;
            }

            private void Context_DataContextChanged(object sender, EventArgs e) {
                _colorSubscription?.Dispose();

                if (DataContext != null) {
                    SetColor();

                    _colorSubscription = ((Class)DataContext).WeakSubscribe(DataContextOnPropertyChanged);
                }
            }

            private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
                try {
                    // try block to fix MSL-271 where the subject, in an edge case,
                    // can be null; resulting in a null reference exception on
                    // cls.Subject.Color

                    if (e.PropertyName == nameof(Data.Class.ActualColor)) {
                        SetColor();
                    }
                }
                catch (Exception ex) {
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogWarning("Exception masked during ClassAdapter.DataContextOnPropertyChanged: {0}", ex.ToLongString());
                }
            }

            private void SetColor() {
                var cls = (Class)DataContext;

                ItemView.FindViewById(Resource.Id.ColorIndicator).SetBackgroundColor(
                    UserColors.GetColor(cls.GetColor(User.Current), CanUseExtendedColors).Color.ToNativeColor()
                );
            }

            protected override void Dispose(bool disposing) {
                if (disposing) {
                    _colorSubscription?.Dispose();
                }

                base.Dispose(disposing);
            }
        }
    }
}