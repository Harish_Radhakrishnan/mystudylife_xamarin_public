using System.Collections.Generic;
using Android.Views;
using MvvmCross.Binding.BindingContext;
using Java.Lang;
using MvvmCross.Platforms.Android.Binding.BindingContext;

namespace MyStudyLife.Droid.Common {
	public class FilterPagerAdapter : AndroidX.ViewPager.Widget.PagerAdapter {

		private readonly IMvxBindingContextOwner _owner;
		private readonly List<FilterPage> _pages; 

		public FilterPagerAdapter(IMvxBindingContextOwner owner, List<FilterPage> pages) {
			_owner = owner;
			_pages = pages;
		}
		
		public override bool IsViewFromObject(View view, Object @object) {
			return view == @object;
		}

		public override int Count {
			get { return _pages.Count; }
		}
		
		public override Object InstantiateItem(ViewGroup container, int position) {
		    View view = _pages[position].InstantiateView(container, (IMvxAndroidBindingContext) _owner.BindingContext);

			container.AddView(view, 0);

			return view;
		}

		public override Java.Lang.ICharSequence GetPageTitleFormatted(int position) {
			return new Java.Lang.String(_pages[position].Title);
		}

		public override void DestroyItem(ViewGroup container, int position, Object @object) {
			container.RemoveView((View) @object);
		}

		public class FilterPage {
			public string Title { get; set; }

			public int LayoutResId { get; set; }

            public virtual View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                return bindingContext.BindingInflate(this.LayoutResId, container, false);
		    }
		}
	}
}