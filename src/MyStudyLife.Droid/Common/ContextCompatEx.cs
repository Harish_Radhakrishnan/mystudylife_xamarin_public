using Android.Content;
using Android.Graphics;
using AndroidX.Core.Content;

namespace MyStudyLife.Droid.Common {
    static class ContextCompatEx {
        public static Color GetColor(Context context, int resourceId) => new Color(ContextCompat.GetColor(context, resourceId));
    }
}