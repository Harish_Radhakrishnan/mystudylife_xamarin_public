using Android.Content;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Common {
    /// <summary>
    ///     <see cref="RecyclerView.ItemDecoration"/> that creates an empty
    ///     space between the items.
    /// </summary>
    public class SpacerDecoration : RecyclerView.ItemDecoration {
        private readonly int _size;

        public SpacerDecoration(Context context) {
            _size = context.Resources.GetDimensionPixelSize(Resource.Dimension.card_spacing);
        }
        public SpacerDecoration(int size) {
            this._size = size;
        }

        public override void GetItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            base.GetItemOffsets(outRect, view, parent, state);

            outRect.Set(0, 0, 0, _size);
        }
    }
}