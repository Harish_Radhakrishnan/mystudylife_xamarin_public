using System.Collections.Generic;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.Droid.Common {
	public class DrawerMenuAdapter : ArrayAdapter<DrawerMenuAdapter.Item> {
		public static readonly IList<Item> MenuItems = new List<Item> {
			Item.ForViewModel<DashboardViewModel>(Resource.String.Views_Dashboard, Glyph.Dashboard),
			Item.ForViewModel<CalendarWeekViewModel>(Resource.String.Views_Calendar, Glyph.Calendar),
			Item.ForViewModel<TasksViewModel>(Resource.String.Views_Tasks, Glyph.Task),
			Item.ForViewModel<ExamsViewModel>(Resource.String.Views_Exams, Glyph.Exam),
			Item.ForViewModel<ScheduleViewModel>(Resource.String.Schedule, Glyph.Schedule),
			Item.ForViewModel<SearchViewModel>(Resource.String.Views_Search, Glyph.Search),
			Item.ForViewModel<SettingsViewModel>(Resource.String.Views_Settings, Glyph.Settings),
		};

		private Typeface _typeface;

		private Typeface Typeface {
            get { return _typeface ?? (_typeface = Typeface.CreateFromAsset(this.Context.Assets, "Fonts/Roboto-Medium.ttf")); }
		}

		public DrawerMenuAdapter(Context context)
			: base(context, Resource.Id.Drawer, MenuItems) {

		}

		public override View GetView(int position, View convertView, ViewGroup parent) {
			var inflater = (LayoutInflater) Context.GetSystemService(Context.LayoutInflaterService);

			View rowView = inflater.Inflate(Resource.Layout.ListItem_NavigationDrawer, parent, false);

			var iconView = rowView.FindViewById<Icon>(Resource.Id.DrawerItemIcon);
			var textView = rowView.FindViewById<TextView>(Resource.Id.DrawerItemText);

            textView.SetTypeface(Typeface, TypefaceStyle.Normal);
            iconView.Glyph = MenuItems[position].IconGlyph;
			textView.SetText(MenuItems[position].TextResourceId);

			return rowView;
		}

		public class Item {
			public int TextResourceId { get; private set; }
			public Glyph IconGlyph { get; private set; }
			public System.Type ViewModelType { get; private set; }

			public Item(int textResId, Glyph glyph) {
				this.TextResourceId = textResId;
                this.IconGlyph = glyph;
			}

			public static Item ForViewModel<TViewModel>(int textResId, Glyph glyph) {
				var item = new Item(textResId, glyph) {ViewModelType = typeof (TViewModel)};

				return item;
			}
		}
	}
}