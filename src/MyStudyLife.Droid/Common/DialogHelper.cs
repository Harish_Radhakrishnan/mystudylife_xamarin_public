using System;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.Content;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Common {
    public static class DialogHelper {
        public static void ShowOrHideProgressDialog(Context context, WeakReference<Dialog> store, bool show, string message = null) {
            if (store == null) {
                throw new ArgumentNullException(nameof(store), "WeakReference store must be created before calling ShowOrHideProgressDialog");
            }

            try {
                store.TryGetTarget(out var dialog);

                if (show) {
                    if (dialog == null) {
                        dialog = new MaterialDialog.Builder(context)
                            .Cancelable(false)
                            .Progress(true, 0)
                            .Build();

                        store.SetTarget(dialog);
                    }

                    if (dialog.IsShowing) {
                        return;
                    }

                    ((MaterialDialog)dialog).SetContent(message ?? R.Common.LoadingWithEllipsis);

                    dialog.Show();
                }
                else if (dialog != null) {
                    dialog.Dismiss();

                    store.SetTarget(null);
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(DialogHelper)).LogError("Exception caught whilst hiding or showing progress dialog: " + ex.ToLongString());
            }
        }

        public static void ShowOrHideProgressDialog(Context context, WeakReference<Dialog> store, bool show, int messageResId) {
            ShowOrHideProgressDialog(context, store, show, context.Resources.GetString(messageResId));
        }

        public class ButtonCallbackShim : MaterialDialog.ButtonCallback {
            private readonly Action _onPositive;
            private readonly Action _onNegative;
            private readonly Action _onNeutral;

            public ButtonCallbackShim(Action onPositive, Action onNegative = null, Action onNeutral = null) {
                _onPositive = onPositive;
                _onNegative = onNegative;
                _onNeutral = onNeutral;
            }

            public override void OnPositive(MaterialDialog dialog) {
                dialog.Dismiss();

                if (_onPositive != null) {
                    _onPositive();
                }
            }

            public override void OnNegative(MaterialDialog dialog) {
                dialog.Dismiss();

                if (_onNegative != null) {
                    _onNegative();
                }
            }

            public override void OnNeutral(MaterialDialog dialog) {
                dialog.Dismiss();

                if (_onNeutral != null) {
                    _onNeutral();
                }
            }
        }
    }
}