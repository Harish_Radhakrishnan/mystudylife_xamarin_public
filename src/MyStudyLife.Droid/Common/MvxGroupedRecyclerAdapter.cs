using System;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.Data;

namespace MyStudyLife.Droid.Common
{
    abstract class MvxGroupedRecyclerAdapter<TGrouping> : MvxRecyclerAdapter, IStickyRecyclerHeadersAdapter
        where TGrouping : class, IUIGrouping
    {
        protected MvxGroupedRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

        protected abstract TGrouping GetGrouping(int position);

        // This is how we determine how many items per section
        public virtual long GetHeaderId(int position)
        {
            if (position < 0) return -1;

            var grouping = GetGrouping(position);

            if (grouping == null) return -1;

            return grouping.Id;
        }

        public virtual RecyclerView.ViewHolder OnCreateHeaderViewHolder(ViewGroup parent)
        {
            var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Component_Header, parent, false);

            return new HeaderViewHolder(view);
        }

        public void OnBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (position < 0) return;

            var grouping = GetGrouping(position);

            if (grouping == null) return;

            OnBindHeaderViewHolder(holder, position, grouping);
        }

        protected virtual void OnBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position, TGrouping grouping)
        {
            var textView = (TextView)holder.ItemView;
            textView.Text = grouping.Name;
        }

        protected class HeaderViewHolder : RecyclerView.ViewHolder
        {
            protected HeaderViewHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
            public HeaderViewHolder(View itemView) : base(itemView) { }
        }
    }
}