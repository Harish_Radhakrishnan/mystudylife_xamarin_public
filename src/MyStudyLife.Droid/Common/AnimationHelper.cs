using System;
using Android.Animation;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.Animations;
using Java.Lang;

namespace MyStudyLife.Droid.Common {
    public static class AnimationHelper {
        public const long DurationShort = 150;
        public const long DurationMedium = 300;
        public const long DurationLong = 600;

        public static IInterpolator GetFastOutSlowInInterpolator(Context context) {
            return AnimationUtils.LoadInterpolator(
                context,
                Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop
                    ? Android.Resource.Interpolator.FastOutSlowIn
                    : Android.Resource.Animation.DecelerateInterpolator
            );
        }

        public static ViewPropertyAnimator SetDefaultInterpolator(this ViewPropertyAnimator animator, Context context)
            => animator.SetInterpolator(GetFastOutSlowInInterpolator(context));

        public static ViewPropertyAnimator SetUpdateListener(this ViewPropertyAnimator animator, Action<ValueAnimator> listener)
            => animator.SetUpdateListener(new AnimatorUpdateListener(listener));

        public static ViewPropertyAnimator WithEndActionCompat(this ViewPropertyAnimator animator, Action action) {
            if (OS.CheckSystemVersion(BuildVersionCodes.JellyBean)) {
                animator.WithEndAction(new Runnable(action));
            }
            else {
                var listener = new AnimatorListener();
                listener.AnimationEnd += (s, e) => action();
                animator.SetListener(listener);
            }

            return animator;
        }
    }
}