using Android.Graphics;

namespace MyStudyLife.Droid.Common {
    public class MaterialPalette {
        private readonly Color _primaryColor, _primaryColorDark, _primaryColorLight;

        // Primary 500
        public Color PrimaryColor {
            get { return _primaryColor; }
        }

        // Primary 700
        public Color PrimaryColorDark {
            get { return _primaryColorDark; }
        }

        // Primary 100
        public Color PrimaryColorLight {
            get { return _primaryColorLight; }
        }

        public MaterialPalette(Color primaryColor) {
            _primaryColor = primaryColor;

            var hsv = new float[3];

            Color.ColorToHSV(primaryColor, hsv);
            hsv[2] *= 0.8f;
            _primaryColorDark = Color.HSVToColor(hsv);

            Color.ColorToHSV(primaryColor, hsv);
            hsv[2] = 1.0f - 0.8f * (1.0f - hsv[2]);
            _primaryColorLight = Color.HSVToColor(hsv);
        }
    }
}