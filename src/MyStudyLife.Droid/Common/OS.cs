using Android.OS;

namespace MyStudyLife.Droid.Common {
    public static class OS {
        /// <summary>
        ///     Returns true if the OS is Lollipop or greater.
        /// </summary>
        public static bool HasLollipops => CheckSystemVersion(BuildVersionCodes.Lollipop);

        public static bool CheckSystemVersion(BuildVersionCodes version) => Build.VERSION.SdkInt >= version;
    }
}