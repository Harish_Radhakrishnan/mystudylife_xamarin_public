using Android.Content;
using Android.Views;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Common {
    public sealed class TupleAdapter : MvxAdapter {
        public TupleAdapter(Context context, IMvxAndroidBindingContext bindingContext)
            : base(context, bindingContext) {
            this.DropDownItemTemplateId = Resource.Layout.SpinnerDropDownItem_Tuple;
            this.ItemTemplateId = Resource.Layout.SpinnerItem_Tuple;
        }

        protected override View GetBindableView(View convertView, object dataContext, ViewGroup parent, int templateId) {
            return base.GetBindableView(convertView, dataContext, parent, templateId == 0 ? Resource.Layout.SpinnerItem_Tuple : templateId);
        }
    }
}