using Android.Views;
using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Caching {
    /** Implemented by objects that provide header views for decoration */
    public interface IHeaderProvider {
        /**
        * Will provide a header view for a given position in the RecyclerView
        *
        * @param recyclerView that will display the header
        * @param position     that will be headed by the header
        * @return a header view for the given position and list
        */
        public View GetHeader(RecyclerView recyclerView, int position);

        void Invalidate();
    }
}
