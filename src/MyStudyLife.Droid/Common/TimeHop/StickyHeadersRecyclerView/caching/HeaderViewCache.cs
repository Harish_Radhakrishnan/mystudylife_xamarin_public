using Android.Views;
using AndroidX.Collection;
using AndroidX.RecyclerView.Widget;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Caching {
    public class HeaderViewCache : IHeaderProvider {
        private IStickyRecyclerHeadersAdapter _adapter;
        private LongSparseArray _headerViews = new LongSparseArray();
        private IOrientationProvider _orientationProvider;

        public HeaderViewCache(IStickyRecyclerHeadersAdapter adapter,
        IOrientationProvider orientationProvider) {
            _adapter = adapter;
            _orientationProvider = orientationProvider;
        }

        public View GetHeader(RecyclerView recyclerView, int position) {
            int headerId = (int)_adapter.GetHeaderId(position);
            View header = (View)_headerViews.Get(headerId);
            if (header == null) {
                RecyclerView.ViewHolder viewHolder = _adapter.OnCreateHeaderViewHolder(recyclerView);
                _adapter.OnBindHeaderViewHolder(viewHolder, position);
                header = viewHolder.ItemView;
                if (header.LayoutParameters == null) {
                    header.LayoutParameters = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                }

                int widthSpec;
                int heightSpec;

                if (_orientationProvider.GetOrientation(recyclerView) == LinearLayoutManager.Vertical) {
                    widthSpec = View.MeasureSpec.MakeMeasureSpec(recyclerView.Width, MeasureSpecMode.Exactly);
                    heightSpec = View.MeasureSpec.MakeMeasureSpec(recyclerView.Height, MeasureSpecMode.Unspecified);
                }
                else {
                    widthSpec = View.MeasureSpec.MakeMeasureSpec(recyclerView.Width, MeasureSpecMode.Unspecified);
                    heightSpec = View.MeasureSpec.MakeMeasureSpec(recyclerView.Height, MeasureSpecMode.Exactly);
                }

                int childWidth = ViewGroup.GetChildMeasureSpec(widthSpec,
                    recyclerView.PaddingLeft + recyclerView.PaddingRight, header.LayoutParameters.Width);
                int childHeight = ViewGroup.GetChildMeasureSpec(heightSpec,
                    recyclerView.PaddingTop + recyclerView.PaddingBottom, header.LayoutParameters.Height);
                header.Measure(childWidth, childHeight);
                header.Layout(0, 0, header.MeasuredWidth, header.MeasuredHeight);
                _headerViews.Put(headerId, header);
            }
            return header;
        }

        public void Invalidate() {
            _headerViews.Clear();
        }
    }
}
