using System;
using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Caching;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Calculation;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView {
    public class HeaderPositionCalculator {
        private IStickyRecyclerHeadersAdapter _adapter;
        private IOrientationProvider _orientationProvider;
        private IHeaderProvider _headerProvider;
        private DimensionCalculator _dimensionCalculator;

        /**
        * The following fields are used as buffers for internal calculations. Their sole purpose is to avoid
        * allocating new Rect every time we need one.
        */
        private Rect _tempRect1 = new Rect();
        private Rect _tempRect2 = new Rect();

        public HeaderPositionCalculator(IStickyRecyclerHeadersAdapter adapter, IHeaderProvider headerProvider,
            IOrientationProvider orientationProvider, DimensionCalculator dimensionCalculator) {
            _adapter = adapter;
            _headerProvider = headerProvider;
            _orientationProvider = orientationProvider;
            _dimensionCalculator = dimensionCalculator;
        }

        /**
         * Determines if a view should have a sticky header.
         * The view has a sticky header if:
         * 1. It is the first element in the recycler view
         * 2. It has a valid ID associated to its position
         *
         * @param itemView given by the RecyclerView
         * @param orientation of the Recyclerview
         * @param position of the list item in question
         * @return True if the view should have a sticky header
         */
        public bool HasStickyHeader(View itemView, int orientation, int position) {
            int offset, margin;
            _dimensionCalculator.InitMargins(_tempRect1, itemView);
            if (orientation == LinearLayoutManager.Vertical) {
                offset = itemView.Top;
                margin = _tempRect1.Top;
            }
            else {
                offset = itemView.Left;
                margin = _tempRect1.Left;
            }

            return offset <= margin && _adapter.GetHeaderId(position) >= 0;
        }

        /**
         * Determines if an item in the list should have a header that is different than the item in the
         * list that immediately precedes it. Items with no headers will always return false.
         *
         * @param position of the list item in questions
         * @param isReverseLayout TRUE if layout manager has flag isReverseLayout
         * @return true if this item has a different header than the previous item in the list
         */
        public bool HasNewHeader(int position, bool isReverseLayout) {
            if (IndexOutOfBounds(position)) {
                return false;
            }

            long headerId = _adapter.GetHeaderId(position);

            if (headerId < 0) {
                return false;
            }

            long nextItemHeaderId = -1;
            int nextItemPosition = position + (isReverseLayout ? 1 : -1);
            if (!IndexOutOfBounds(nextItemPosition)) {
                nextItemHeaderId = _adapter.GetHeaderId(nextItemPosition);
            }
            int firstItemPosition = isReverseLayout ? _adapter.ItemCount - 1 : 0;

            return position == firstItemPosition || headerId != nextItemHeaderId;
        }

        private bool IndexOutOfBounds(int position) {
            return position < 0 || position >= _adapter.ItemCount;
        }

        public void InitHeaderBounds(Rect bounds, RecyclerView recyclerView, View header, View firstView, bool firstHeader) {
            int orientation = _orientationProvider.GetOrientation(recyclerView);
            InitDefaultHeaderOffset(bounds, recyclerView, header, firstView, orientation);

            if (firstHeader && IsStickyHeaderBeingPushedOffscreen(recyclerView, header)) {
                View viewAfterNextHeader = GetFirstViewUnobscuredByHeader(recyclerView, header);
                int firstViewUnderHeaderPosition = recyclerView.GetChildAdapterPosition(viewAfterNextHeader);
                View secondHeader = _headerProvider.GetHeader(recyclerView, firstViewUnderHeaderPosition);
                TranslateHeaderWithNextHeader(recyclerView, _orientationProvider.GetOrientation(recyclerView), bounds,
                    header, viewAfterNextHeader, secondHeader);
            }
        }

        private void InitDefaultHeaderOffset(Rect headerMargins, RecyclerView recyclerView, View header, View firstView, int orientation) {
            int translationX, translationY;
            _dimensionCalculator.InitMargins(_tempRect1, header);

            ViewGroup.LayoutParams layoutParams = firstView.LayoutParameters;
            int leftMargin = 0;
            int topMargin = 0;
            if (layoutParams is ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams)layoutParams;
                leftMargin = marginLayoutParams.LeftMargin;
                topMargin = marginLayoutParams.TopMargin;
            }

            if (orientation == LinearLayoutManager.Vertical) {
                translationX = firstView.Left - leftMargin + _tempRect1.Left;
                translationY = Math.Max(
                    firstView.Top - topMargin - header.Height - _tempRect1.Bottom,
                    GetListTop(recyclerView) + _tempRect1.Top);
            }
            else {
                translationY = firstView.Top - topMargin + _tempRect1.Top;
                translationX = Math.Max(
                    firstView.Left - leftMargin - header.Width - _tempRect1.Right,
                    GetListLeft(recyclerView) + _tempRect1.Left);
            }

            headerMargins.Set(translationX, translationY, translationX + header.Width,
                    translationY + header.Height);
        }

        private bool IsStickyHeaderBeingPushedOffscreen(RecyclerView recyclerView, View stickyHeader) {
            View viewAfterHeader = GetFirstViewUnobscuredByHeader(recyclerView, stickyHeader);
            int firstViewUnderHeaderPosition = recyclerView.GetChildAdapterPosition(viewAfterHeader);
            if (firstViewUnderHeaderPosition == RecyclerView.NoPosition) {
                return false;
            }

            bool isReverseLayout = _orientationProvider.IsReverseLayout(recyclerView);
            if (firstViewUnderHeaderPosition > 0 && HasNewHeader(firstViewUnderHeaderPosition, isReverseLayout)) {
                View nextHeader = _headerProvider.GetHeader(recyclerView, firstViewUnderHeaderPosition);
                _dimensionCalculator.InitMargins(_tempRect1, nextHeader);
                _dimensionCalculator.InitMargins(_tempRect2, stickyHeader);

                if (_orientationProvider.GetOrientation(recyclerView) == LinearLayoutManager.Vertical) {
                    int topOfNextHeader = viewAfterHeader.Top - _tempRect1.Bottom - nextHeader.Height - _tempRect1.Top;
                    int bottomOfThisHeader = recyclerView.PaddingTop + stickyHeader.Bottom + _tempRect2.Top + _tempRect2.Bottom;
                    if (topOfNextHeader < bottomOfThisHeader) {
                        return true;
                    }
                }
                else {
                    int leftOfNextHeader = viewAfterHeader.Left - _tempRect1.Right - nextHeader.Width - _tempRect1.Left;
                    int rightOfThisHeader = recyclerView.PaddingLeft + stickyHeader.Right + _tempRect2.Left + _tempRect2.Right;
                    if (leftOfNextHeader < rightOfThisHeader) {
                        return true;
                    }
                }
            }

            return false;
        }

        private void TranslateHeaderWithNextHeader(RecyclerView recyclerView, int orientation, Rect translation,
          View currentHeader, View viewAfterNextHeader, View nextHeader) {
            _dimensionCalculator.InitMargins(_tempRect1, nextHeader);
            _dimensionCalculator.InitMargins(_tempRect2, currentHeader);
            if (orientation == LinearLayoutManager.Vertical) {
                int topOfStickyHeader = GetListTop(recyclerView) + _tempRect2.Top + _tempRect2.Bottom;
                int shiftFromNextHeader = viewAfterNextHeader.Top - nextHeader.Height - _tempRect1.Bottom - _tempRect1.Top - currentHeader.Height - topOfStickyHeader;
                if (shiftFromNextHeader < topOfStickyHeader) {
                    translation.Top += shiftFromNextHeader;
                }
            }
            else {
                int leftOfStickyHeader = GetListLeft(recyclerView) + _tempRect2.Left + _tempRect2.Right;
                int shiftFromNextHeader = viewAfterNextHeader.Left - nextHeader.Width - _tempRect1.Right - _tempRect1.Left - currentHeader.Width - leftOfStickyHeader;
                if (shiftFromNextHeader < leftOfStickyHeader) {
                    translation.Left += shiftFromNextHeader;
                }
            }
        }

        /**
         * Returns the first item currently in the RecyclerView that is not obscured by a header.
         *
         * @param parent Recyclerview containing all the list items
         * @return first item that is fully beneath a header
         */
        private View GetFirstViewUnobscuredByHeader(RecyclerView parent, View firstHeader) {
            bool isReverseLayout = _orientationProvider.IsReverseLayout(parent);
            int step = isReverseLayout ? -1 : 1;
            int from = isReverseLayout ? parent.ChildCount - 1 : 0;
            for (int i = from; i >= 0 && i <= parent.ChildCount - 1; i += step) {
                View child = parent.GetChildAt(i);
                if (!ItemIsObscuredByHeader(parent, child, firstHeader, _orientationProvider.GetOrientation(parent))) {
                    return child;
                }
            }
            return null;
        }

        /**
         * Determines if an item is obscured by a header
         *
         *
         * @param parent
         * @param item        to determine if obscured by header
         * @param header      that might be obscuring the item
         * @param orientation of the {@link RecyclerView}
         * @return true if the item view is obscured by the header view
         */
        private bool ItemIsObscuredByHeader(RecyclerView parent, View item, View header, int orientation) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams)item.LayoutParameters;
            _dimensionCalculator.InitMargins(_tempRect1, header);

            int adapterPosition = parent.GetChildAdapterPosition(item);
            if (adapterPosition == RecyclerView.NoPosition || _headerProvider.GetHeader(parent, adapterPosition) != header) {
                // Resolves https://github.com/timehop/sticky-headers-recyclerview/issues/36
                // Handles an edge case where a trailing header is smaller than the current sticky header.
                return false;
            }

            if (orientation == LinearLayoutManager.Vertical) {
                int itemTop = item.Top - layoutParams.TopMargin;
                int headerBottom = GetListTop(parent) + header.Bottom + _tempRect1.Bottom + _tempRect1.Top;
                if (itemTop >= headerBottom) {
                    return false;
                }
            }
            else {
                int itemLeft = item.Left - layoutParams.LeftMargin;
                int headerRight = GetListLeft(parent) + header.Right + _tempRect1.Right + _tempRect1.Left;
                if (itemLeft >= headerRight) {
                    return false;
                }
            }

            return true;
        }

        private int GetListTop(RecyclerView view) {
            if (view.GetLayoutManager().ClipToPadding) {
                return view.PaddingTop;
            }
            else {
                return 0;
            }
        }

        private int GetListLeft(RecyclerView view) {
            if (view.GetLayoutManager().ClipToPadding) {
                return view.PaddingLeft;
            }
            else {
                return 0;
            }
        }
    }
}
