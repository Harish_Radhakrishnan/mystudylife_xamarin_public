using Android.Graphics;
using Android.Views;
using AndroidX.RecyclerView.Widget;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Calculation;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Rendering {
    public class HeaderRenderer {
        private DimensionCalculator _dimensionCalculator;
        private IOrientationProvider _orientationProvider;

        /**
         * The following field is used as a buffer for internal calculations. Its sole purpose is to avoid
         * allocating new Rect every time we need one.
         */
        private Rect _tempRect = new Rect();

        public HeaderRenderer(IOrientationProvider orientationProvider) :
            this(orientationProvider, new DimensionCalculator()) {
        }

        private HeaderRenderer(IOrientationProvider orientationProvider,
            DimensionCalculator dimensionCalculator) {
            _orientationProvider = orientationProvider;
            _dimensionCalculator = dimensionCalculator;
        }

        /**
         * Draws a header to a canvas, offsetting by some x and y amount
         *
         * @param recyclerView the parent recycler view for drawing the header into
         * @param canvas       the canvas on which to draw the header
         * @param header       the view to draw as the header
         * @param offset       a Rect used to define the x/y offset of the header. Specify x/y offset by setting
         *                     the {@link Rect#left} and {@link Rect#top} properties, respectively.
         */
        public void DrawHeader(RecyclerView recyclerView, Canvas canvas, View header, Rect offset) {
            canvas.Save();

            if (recyclerView.ClipToPadding) {
                // Clip drawing of headers to the padding of the RecyclerView. Avoids drawing in the padding
                InitClipRectForHeader(_tempRect, recyclerView, header);
                canvas.ClipRect(_tempRect);
            }
            canvas.Translate(offset.Left, offset.Top);
            header.Draw(canvas);
            canvas.Restore();
        }

        /**
         * Initializes a clipping rect for the header based on the margins of the header and the padding of the
         * recycler.
         * FIXME: Currently right margin in VERTICAL orientation and bottom margin in HORIZONTAL
         * orientation are clipped so they look accurate, but the headers are not being drawn at the
         * correctly smaller width and height respectively.
         *
         * @param clipRect {@link Rect} for clipping a provided header to the padding of a recycler view
         * @param recyclerView for which to provide a header
         * @param header       for clipping
         */
        private void InitClipRectForHeader(Rect clipRect, RecyclerView recyclerView, View header) {
            _dimensionCalculator.InitMargins(clipRect, header);
            if (_orientationProvider.GetOrientation(recyclerView) == LinearLayoutManager.Vertical) {
                clipRect.Set(
                    recyclerView.PaddingLeft,
                    recyclerView.PaddingTop,
                    recyclerView.Width - recyclerView.PaddingRight - clipRect.Right,
                    recyclerView.Height - recyclerView.PaddingBottom);
            }
            else {
                clipRect.Set(
                  recyclerView.PaddingLeft,
                  recyclerView.PaddingTop,
                  recyclerView.Width - recyclerView.PaddingRight,
                  recyclerView.Height - recyclerView.PaddingBottom - clipRect.Bottom);
            }
        }
    }
}
