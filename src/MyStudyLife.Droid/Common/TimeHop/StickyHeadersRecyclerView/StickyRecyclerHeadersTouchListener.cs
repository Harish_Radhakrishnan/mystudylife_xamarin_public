using Android.Views;
using AndroidX.RecyclerView.Widget;
using Java.Lang;
using static AndroidX.RecyclerView.Widget.RecyclerView;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView {
    public class StickyRecyclerHeadersTouchListener : GestureDetector.SimpleOnGestureListener, IOnItemTouchListener {
        public GestureDetector TapDetector;
        public RecyclerView RecyclerView;
        public StickyRecyclerHeadersDecoration Decor;
        public IOnHeaderClickListener OnHeaderClickListener;

        public interface IOnHeaderClickListener {
            void OnHeaderClick(View header, int position, long headerId);
        }

        public StickyRecyclerHeadersTouchListener(RecyclerView recyclerView,
                                                   StickyRecyclerHeadersDecoration decor) {
            TapDetector = new GestureDetector(recyclerView.Context, this);
            RecyclerView = recyclerView;
            Decor = decor;
        }

        public IStickyRecyclerHeadersAdapter GetAdapter() {
            if (RecyclerView.GetAdapter() is IStickyRecyclerHeadersAdapter) {
                return (IStickyRecyclerHeadersAdapter)RecyclerView.GetAdapter();
            }
            else {
                throw new IllegalStateException("A RecyclerView with " +
                    nameof(StickyRecyclerHeadersTouchListener) +
          " requires a " + nameof(IStickyRecyclerHeadersAdapter));
            }
        }

        public void setOnHeaderClickListener(IOnHeaderClickListener listener) {
            OnHeaderClickListener = listener;
        }
        public bool OnInterceptTouchEvent(RecyclerView recyclerView, MotionEvent @event) {
            if (this.OnHeaderClickListener != null) {
                bool tapDetectorResponse = this.TapDetector.OnTouchEvent(@event);
                if (tapDetectorResponse) {
                    // Don't return false if a single tap is detected
                    return true;
                }
                if (@event.Action == MotionEventActions.Down) {
                    int position = Decor.FindHeaderPositionUnder((int)@event.GetX(), (int)@event.GetY());
                    return position != -1;
                }
            }
            return false;
        }

        public void OnRequestDisallowInterceptTouchEvent(bool disallow) {
            // No need to do anything
        }

        public void OnTouchEvent(RecyclerView recyclerView, MotionEvent @event) {
            // No need to do anything
        }

        public override bool OnSingleTapUp(MotionEvent e) {
            int position = Decor.FindHeaderPositionUnder((int)e.GetX(), (int)e.GetY());
            if (position != -1) {
                View headerView = Decor.GetHeaderView(RecyclerView, position);
                long headerId = GetAdapter().GetHeaderId(position);
                OnHeaderClickListener.OnHeaderClick(headerView, position, headerId);
                RecyclerView.PlaySoundEffect(SoundEffects.Click);
                headerView.OnTouchEvent(e);
                return true;
            }
            return false;
        }

        public override bool OnDoubleTap(MotionEvent e) {
            return true;
        }
    }
}
