using Android.Graphics;
using Android.Views;
using AndroidX.Collection;
using AndroidX.RecyclerView.Widget;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Caching;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Calculation;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Rendering;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView {
    public class StickyRecyclerHeadersDecoration : RecyclerView.ItemDecoration {
        private IStickyRecyclerHeadersAdapter _adapter;
        private IItemVisibilityAdapter _visibilityAdapter;
        private LongSparseArray _headerRects = new LongSparseArray();
        private IHeaderProvider _headerProvider;
        private IOrientationProvider _orientationProvider;
        private HeaderPositionCalculator _headerPositionCalculator;
        private HeaderRenderer _renderer;
        private DimensionCalculator _dimensionCalculator;
        /**
        * The following field is used as a buffer for internal calculations. Its sole purpose is to avoid
        * allocating new Rect every time we need one.
        */
        private Rect _tempRect = new Rect();
        // TODO: Consider passing in orientation to simplify orientation accounting within calculation
        public StickyRecyclerHeadersDecoration(IStickyRecyclerHeadersAdapter adapter) :
        this(adapter, new LinearLayoutOrientationProvider(), new DimensionCalculator(), null) {
        }
        public StickyRecyclerHeadersDecoration(IStickyRecyclerHeadersAdapter adapter, IItemVisibilityAdapter visibilityAdapter)
            : this(adapter, new LinearLayoutOrientationProvider(), new DimensionCalculator(), visibilityAdapter) {
        }
        private StickyRecyclerHeadersDecoration(IStickyRecyclerHeadersAdapter adapter, IOrientationProvider orientationProvider,
            DimensionCalculator dimensionCalculator, IItemVisibilityAdapter visibilityAdapter) :
            this(adapter, orientationProvider, dimensionCalculator, new HeaderRenderer(orientationProvider),
                new HeaderViewCache(adapter, orientationProvider), visibilityAdapter) {
        }
        private StickyRecyclerHeadersDecoration(IStickyRecyclerHeadersAdapter adapter, IOrientationProvider orientationProvider,
            DimensionCalculator dimensionCalculator, HeaderRenderer headerRenderer, IHeaderProvider headerProvider, IItemVisibilityAdapter visibilityAdapter) :
            this(adapter, headerRenderer, orientationProvider, dimensionCalculator, headerProvider,
                new HeaderPositionCalculator(adapter, headerProvider, orientationProvider,
                    dimensionCalculator), visibilityAdapter) {
        }
        private StickyRecyclerHeadersDecoration(IStickyRecyclerHeadersAdapter adapter, HeaderRenderer headerRenderer,
            IOrientationProvider orientationProvider, DimensionCalculator dimensionCalculator, IHeaderProvider headerProvider,
            HeaderPositionCalculator headerPositionCalculator, IItemVisibilityAdapter visibilityAdapter) {
            _adapter = adapter;
            _headerProvider = headerProvider;
            _orientationProvider = orientationProvider;
            _renderer = headerRenderer;
            _dimensionCalculator = dimensionCalculator;
            _headerPositionCalculator = headerPositionCalculator;
            _visibilityAdapter = visibilityAdapter;
        }
        /**
        * Sets the offsets for the first item in a section to make room for the header view
        *
         * @param itemOffsets rectangle to define offsets for the item
         * @param header      view used to calculate offset for the item
        * @param orientation used to calculate offset for the item
        */
        private void SetItemOffsetsForHeader(Rect itemOffsets, View header, int orientation) {
            _dimensionCalculator.InitMargins(_tempRect, header);
            if (orientation == LinearLayoutManager.Vertical) {
                itemOffsets.Top = header.Height + _tempRect.Top + _tempRect.Bottom;
            }
            else {
                itemOffsets.Left = header.Width + _tempRect.Left + _tempRect.Right;
            }
        }
        /**
         * Gets the position of the header under the specified (x, y) coordinates.
         *
         * @param x x-coordinate
         * @param y y-coordinate
         * @return position of header, or -1 if not found
         */
        public int FindHeaderPositionUnder(int x, int y) {
            for (int i = 0; i < _headerRects.Size(); i++) {
                Rect rect = (Rect)_headerRects.Get(_headerRects.KeyAt(i));
                if (rect.Contains(x, y)) {
                    int position = (int)_headerRects.KeyAt(i);
                    if (_visibilityAdapter == null || _visibilityAdapter.IsPositionVisible(position)) {
                        return position;
                    }
                }
            }
            return -1;
        }
        /**
         * Gets the header view for the associated position.  If it doesn't exist yet, it will be
         * created, measured, and laid out.
         *
         * @param parent the recyclerview
         * @param position the position to get the header view for
         * @return Header view
         */
        public View GetHeaderView(RecyclerView parent, int position) {
            return _headerProvider.GetHeader(parent, position);
        }
        /**
         * Invalidates cached headers.  This does not invalidate the recyclerview, you should do that manually after
         * calling this method.
         */
        public void InvalidateHeaders() {
            _headerProvider.Invalidate();
            _headerRects.Clear();
        }
        public override void GetItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            base.GetItemOffsets(outRect, view, parent, state);
            int itemPosition = parent.GetChildAdapterPosition(view);
            if (itemPosition == RecyclerView.NoPosition) {
                return;
            }
            if (_headerPositionCalculator.HasNewHeader(itemPosition, _orientationProvider.IsReverseLayout(parent))) {
                View header = GetHeaderView(parent, itemPosition);
                SetItemOffsetsForHeader(outRect, header, _orientationProvider.GetOrientation(parent));
            }
        }
        public override void OnDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
            base.OnDrawOver(c, parent, state);
            int childCount = parent.ChildCount;
            if (childCount <= 0 || _adapter.ItemCount <= 0) {
                return;
            }
            for (int i = 0; i < childCount; i++) {
                View itemView = parent.GetChildAt(i);
                int position = parent.GetChildAdapterPosition(itemView);
                if (position == RecyclerView.NoPosition) {
                    continue;
                }
                bool hasStickyHeader = _headerPositionCalculator.HasStickyHeader(itemView, _orientationProvider.GetOrientation(parent), position);
                if (hasStickyHeader || _headerPositionCalculator.HasNewHeader(position, _orientationProvider.IsReverseLayout(parent))) {
                    View header = _headerProvider.GetHeader(parent, position);
                    //re-use existing Rect, if any.
                    Rect headerOffset = (Rect)_headerRects.Get(position);
                    if (headerOffset == null) {
                        headerOffset = new Rect();
                        _headerRects.Put(position, headerOffset);
                    }
                    _headerPositionCalculator.InitHeaderBounds(headerOffset, parent, header, itemView, hasStickyHeader);
                    _renderer.DrawHeader(parent, c, header, headerOffset);
                }
            }
        }
    }
}
