using Android.Graphics;
using Android.Views;
using static Android.Views.ViewGroup;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Calculation {
    /**
    * Helper to calculate various view dimensions
    */
    public class DimensionCalculator {
        /**
        * Populates {@link Rect} with margins for any view.
        *
        *
        * @param margins rect to populate
        * @param view for which to get margins
        */
        public void InitMargins(Rect margins, View view) {
            LayoutParams layoutParams = view.LayoutParameters;

            if (layoutParams is MarginLayoutParams) {
                MarginLayoutParams marginLayoutParams = (MarginLayoutParams)layoutParams;
                InitMarginRect(margins, marginLayoutParams);
            }
            else {
                margins.Set(0, 0, 0, 0);
            }
        }

        /**
         * Converts {@link MarginLayoutParams} into a representative {@link Rect}.
         *
         * @param marginRect Rect to be initialized with margins coordinates, where
         * {@link MarginLayoutParams#leftMargin} is equivalent to {@link Rect#left}, etc.
         * @param marginLayoutParams margins to populate the Rect with
         */
        private void InitMarginRect(Rect marginRect, MarginLayoutParams marginLayoutParams) {
            marginRect.Set(
              marginLayoutParams.LeftMargin,
              marginLayoutParams.TopMargin,
              marginLayoutParams.RightMargin,
              marginLayoutParams.BottomMargin
          );
        }
    }
}
