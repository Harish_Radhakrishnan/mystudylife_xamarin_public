using AndroidX.RecyclerView.Widget;
using Java.Lang;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils {
    /**
    * OrientationProvider for ReyclerViews who use a LinearLayoutManager
    */
    public class LinearLayoutOrientationProvider : IOrientationProvider {
        public int GetOrientation(RecyclerView recyclerView) {
            RecyclerView.LayoutManager layoutManager = recyclerView.GetLayoutManager();
            ThrowIfNotLinearLayoutManager(layoutManager);
            return ((LinearLayoutManager)layoutManager).Orientation;
        }

        public bool IsReverseLayout(RecyclerView recyclerView) {
            RecyclerView.LayoutManager layoutManager = recyclerView.GetLayoutManager();
            ThrowIfNotLinearLayoutManager(layoutManager);
            return ((LinearLayoutManager)layoutManager).ReverseLayout;
        }

        private void ThrowIfNotLinearLayoutManager(RecyclerView.LayoutManager layoutManager) {
            if (!(layoutManager is LinearLayoutManager)) {
                throw new IllegalStateException("StickyListHeadersDecoration can only be used with a " +
                    "LinearLayoutManager.");
            }
        }
    }
}
