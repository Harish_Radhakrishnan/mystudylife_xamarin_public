using AndroidX.RecyclerView.Widget;

namespace MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView.Utils {
    /**
    * Interface for getting the orientation of a RecyclerView from its LayoutManager
    */
    public interface IOrientationProvider {
        public int GetOrientation(RecyclerView recyclerView);

        public bool IsReverseLayout(RecyclerView recyclerView);
    }
}
