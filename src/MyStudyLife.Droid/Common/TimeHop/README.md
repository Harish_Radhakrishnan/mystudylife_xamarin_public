TimeHop Sticky Header Library for Android platform
==============

##Details##
TimeHop Sticky Header Library allows you to easily create section headers for RecyclerViews using a LinearLayoutManager in either vertical or horizontal orientation.

We have migrated native Android/Java code to Xamarin.Android and added in MSL project form below comment hash 

* Git Repo : https://github.com/timehop/sticky-headers-recyclerview
*Comment hash : 24cff3608c6dd949a81c6fd3bd730bfb5228b89a

We have migrated to Xamarin.Android because It (https://github.com/mystudylife/xamarin-stickyheadersrecyclerview) is outdated and not compatible with MvvmCross version 8.0.2 and Android OS 10 and above. With migration to C# we have also done requried changes and make it compatible with MvvmCross 8.0.2 and Android OS 10. 

### Changes we have done on original source code  ###

1. Remove Android support libs V4 and V7 packages. 
2. Add support for AndroidX packages to make it compatible with MvvmCross 8.0.2 and Android 10
3. Replace depricated methods and code blocks.
4. Migrate java code to Xamarin.Android and added in MSL project    
