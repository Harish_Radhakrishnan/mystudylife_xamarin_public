using System;
using Android.Animation;
using Android.Views;
using JavaObject = Java.Lang.Object;

namespace MyStudyLife.Droid.Common {
    /// <summary>
    ///     As the <see cref="View.Animate()"/> API seems
    ///     to be missing C# style events.
    /// </summary>
    public sealed class AnimatorListener : JavaObject, Animator.IAnimatorListener {
        public event EventHandler<Animator.AnimationCancelEventArgs> AnimationCancel;
        public event EventHandler<Animator.AnimationEndEventArgs> AnimationEnd;
        public event EventHandler<Animator.AnimationRepeatEventArgs> AnimationRepeat;
        public event EventHandler<Animator.AnimationStartEventArgs> AnimationStart;

        public void OnAnimationCancel(Animator animation) {
            AnimationCancel?.Invoke(this, new Animator.AnimationCancelEventArgs(animation));
        }

        public void OnAnimationEnd(Animator animation) {
            AnimationEnd?.Invoke(this, new Animator.AnimationEndEventArgs(animation));
        }

        public void OnAnimationRepeat(Animator animation) {
            AnimationRepeat?.Invoke(this, new Animator.AnimationRepeatEventArgs(animation));
        }

        public void OnAnimationStart(Animator animation) {
            AnimationStart?.Invoke(this, new Animator.AnimationStartEventArgs(animation));
        }
    }

    public sealed class AnimatorUpdateListener : JavaObject, ValueAnimator.IAnimatorUpdateListener {
        private readonly Action<ValueAnimator> _onUpdate;

        public event EventHandler<ValueAnimator.AnimatorUpdateEventArgs> AnimationUpdate;

        public AnimatorUpdateListener() {}
        public AnimatorUpdateListener(Action<ValueAnimator> onUpdate) {
            this._onUpdate = onUpdate;
        }

        public void OnAnimationUpdate(ValueAnimator animation) {
            this._onUpdate?.Invoke(animation);
            this.AnimationUpdate?.Invoke(this, new ValueAnimator.AnimatorUpdateEventArgs(animation));
        }
    }
}