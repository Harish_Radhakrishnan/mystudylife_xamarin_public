using System;
using Android.Content;
using Android.Runtime;
using Android.Views;
using AndroidX.RecyclerView.Widget;
using Google.Android.Material.FloatingActionButton;

namespace MyStudyLife.Droid.Common {
    /// <summary>
    ///     <see cref="LinearLayoutManager"/> with bottom padding
    ///     for a floating action button.
    /// </summary>
    class FABLinearLayoutManager : LinearLayoutManager {
        private FloatingActionButton _fab;

        private int _paddingBottomWithFab;
        private int _paddingBottom;

        public override int PaddingBottom => _paddingBottom;

        protected FABLinearLayoutManager(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public FABLinearLayoutManager(Context context, FloatingActionButton fab = null) : base(context) {
            Initialize(context, fab);
        }

        public FABLinearLayoutManager(Context context, int orientation, bool reverseLayout, FloatingActionButton fab = null) : base(context, orientation, reverseLayout) {
            Initialize(context, fab);
        }

        private void Initialize(Context context, FloatingActionButton fab) {
            _fab = fab;
            _paddingBottomWithFab = context.Resources.DisplayMetrics.DpsToPixels(88); // 56dp FAB, 2x16dp margin
            SetPaddingBottom();

            if (_fab != null) {
                _fab.LayoutChange += (s, e) => SetPaddingBottom();
            }
        }

        private void SetPaddingBottom() {
            var oldPaddingBottom = _paddingBottom;
            _paddingBottom = _fab == null || _fab.Visibility == ViewStates.Visible ? _paddingBottomWithFab : 0;

            if (oldPaddingBottom != _paddingBottom) {
                RequestLayout();
            }
        }
    }
}