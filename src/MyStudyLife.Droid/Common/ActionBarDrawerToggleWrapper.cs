using System;
using Android.App;
using Android.Views;
using AndroidX.AppCompat.App;
using AndroidX.AppCompat.Widget;
using AndroidX.DrawerLayout.Widget;

namespace MyStudyLife.Droid.Common {
	public class ActionBarDrawerEventArgs : EventArgs {
		public View DrawerView { get; set; }

	    public ActionBarDrawerEventArgs(View drawerView) {
	        this.DrawerView = drawerView;
	    }
	}

    public sealed class ActionBarDrawerSlideEventArgs : ActionBarDrawerEventArgs {
        public float SlideOffset { get; set; }

        public ActionBarDrawerSlideEventArgs(View drawerView, float slideOffset) : base(drawerView) {
	        this.SlideOffset = slideOffset;
	    }
    }

    public sealed class ActionBarDrawerStateChangeEventArgs : EventArgs {
        public int NewState { get; set; }

        public ActionBarDrawerStateChangeEventArgs(int newState) {
            this.NewState = newState;
	    }
    }

	/// <summary>
	///		A wrapper around <see cref="ActionBarDrawerToggle"/> to provide
	///		events instead of overridable methods.
	/// </summary>
	public sealed class ActionBarDrawerToggleWrapper : ActionBarDrawerToggle {
        private ActionBarDrawerToggleWrapper(IntPtr javaReference, Android.Runtime.JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public ActionBarDrawerToggleWrapper(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes)
            : base(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes) { }

		public event EventHandler<ActionBarDrawerEventArgs> DrawerClosed;
        public event EventHandler<ActionBarDrawerEventArgs> DrawerOpened;
        public event EventHandler<ActionBarDrawerSlideEventArgs> DrawerSlide;
        public event EventHandler<ActionBarDrawerStateChangeEventArgs> DrawerStateChanged;

		public override void OnDrawerClosed(View drawerView) {
            DrawerClosed?.Invoke(this, new ActionBarDrawerEventArgs(drawerView));
		    base.OnDrawerClosed(drawerView);
		}

        public override void OnDrawerOpened(View drawerView) {
            DrawerOpened?.Invoke(this, new ActionBarDrawerEventArgs(drawerView));
			base.OnDrawerOpened(drawerView);
		}

        public override void OnDrawerSlide(View drawerView, float slideOffset) {
            DrawerSlide?.Invoke(this, new ActionBarDrawerSlideEventArgs(drawerView, slideOffset));
			base.OnDrawerSlide(drawerView, slideOffset);
		}

        public override void OnDrawerStateChanged(int newState) {
            DrawerStateChanged?.Invoke(this, new ActionBarDrawerStateChangeEventArgs(newState));
			base.OnDrawerStateChanged(newState);
		}
	}
}