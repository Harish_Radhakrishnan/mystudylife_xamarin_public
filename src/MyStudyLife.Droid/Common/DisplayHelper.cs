using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;

namespace MyStudyLife.Droid.Common {
    public static class DisplayHelper {
        public static bool IsTablet {
            get {
                // For devices without SmallestScreenWidthDp reference calculate if device screen is over 600
                if (Build.VERSION.SdkInt < BuildVersionCodes.HoneycombMr2) {
                    var metrics = GetDisplayMetrics();

                    return (metrics.WidthPixels / metrics.Density) >= 600;
                }

                return Application.Context.Resources.Configuration.SmallestScreenWidthDp >= 600;
            }
        }

        public static DisplayMetrics GetDisplayMetrics() {
            var windowManager = Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();

            var metrics = new DisplayMetrics();

            windowManager.DefaultDisplay.GetMetrics(metrics);

            return metrics;
        }

        /// <summary>
        ///     If you already have <see cref="DisplayMetrics"/> or
        ///     are making multiple calls, use the overload.
        /// </summary>
        public static int DpsToPixels(float dps) {
            return DpsToPixels(GetDisplayMetrics(), dps);
        }

        [Obsolete("Use extension method on displaymetrics")]
        public static int DpsToPixels(float dps, DisplayMetrics displayMetrics) {
            return DpsToPixels(displayMetrics, dps);
        }

        public static int DpsToPixels(this DisplayMetrics displayMetrics, float dps) {
            return (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, dps, displayMetrics);
        }

        public static double PixelsToDps(int pixels) {
            return PixelsToDps(GetDisplayMetrics(), pixels);
        }

        public static double PixelsToDps(this DisplayMetrics displayMetrics, int pixels) {
            return pixels/displayMetrics.Density;
        }
    }
}