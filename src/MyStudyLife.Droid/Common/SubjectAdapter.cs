using Android.Content;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Common {
	public class SubjectAdapter : MvxAdapter {
		public SubjectAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext) {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
			this.ItemTemplateId = Resource.Layout.SpinnerItem_Subject;
			this.DropDownItemTemplateId = Resource.Layout.SpinnerDropDownItem_Subject;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}
	}
}