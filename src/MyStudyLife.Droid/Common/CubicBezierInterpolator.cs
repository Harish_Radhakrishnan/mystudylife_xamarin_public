using System;
using Android.Graphics;
using Android.Views.Animations;

// Ported from https://github.com/codesoup/android-cubic-bezier-interpolator/blob/master/src/nl/codesoup/CubicBezierInterpolator.java

namespace MyStudyLife.Droid.Common {
    public class CubicBezierInterpolator : Java.Lang.Object, IInterpolator {
        private PointF _start;
        private PointF _end;
        private PointF _a = new PointF();
        private PointF _b = new PointF();
        private PointF _c = new PointF();

        public CubicBezierInterpolator(PointF start, PointF end) {
            if (start.X < 0 || start.X > 1) {
                throw new ArgumentException("startX value must be in the range [0, 1]");
            }
            if (end.X < 0 || end.X > 1) {
                throw new ArgumentException("endX value must be in the range [0, 1]");
            }
            this._start = start;
            this._end = end;
        }

        public CubicBezierInterpolator(float startX, float startY, float endX, float endY) : this(new PointF(startX, startY), new PointF(endX, endY)) { }

        public CubicBezierInterpolator(double startX, double startY, double endX, double endY) : this((float)startX, (float)startY, (float)endX, (float)endY) { }

        public float GetInterpolation(float time) {
            return GetBezierCoordinateY(GetXForTime(time));
        }

        protected float GetBezierCoordinateY(float time) {
            _c.Y = 3 * _start.Y;
            _b.Y = 3 * (_end.Y - _start.Y) - _c.Y;
            _a.Y = 1 - _c.Y - _b.Y;
            return time * (_c.Y + time * (_b.Y + time * _a.Y));
        }

        protected float GetXForTime(float time) {
            float x = time;
            float z;
            for (int i = 1; i < 14; i++) {
                z = GetBezierCoordinateX(x) - time;
                if (Math.Abs(z) < 1e-3) {
                    break;
                }
                x -= z / GetXDerivate(x);
            }
            return x;
        }

        private float GetXDerivate(float t) {
            return _c.X + t * (2 * _b.X + 3 * _a.X * t);
        }

        private float GetBezierCoordinateX(float time) {
            _c.X = 3 * _start.X;
            _b.X = 3 * (_end.X - _start.X) - _c.X;
            _a.X = 1 - _c.X - _b.X;
            return time * (_c.X + time * (_b.X + time * _a.X));
        }
    }
}