using Android.Widget;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.Droid.Common {
    class GroupedExamsRecyclerAdapter : MvxGroupedRecyclerAdapter<ExamGrouping> {
        public GroupedExamsRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

        protected override ExamGrouping GetGrouping(int position) {
            return ((ExamsViewModel)BindingContext.DataContext).GetGrouping(position);
        }

        protected override void OnBindHeaderViewHolder(AndroidX.RecyclerView.Widget.RecyclerView.ViewHolder holder, int position, ExamGrouping grouping)
        {
            base.OnBindHeaderViewHolder(holder, position, grouping);
            if (grouping == null) return;

            var textView = (TextView)holder.ItemView;

            switch (grouping.Kind)
            {
                case ExamGroupingKind.Days3:
                    textView.SetTextColor(ContextCompatEx.GetColor(textView.Context, Resource.Color.attention_foreground));
                    break;
                case ExamGroupingKind.Days7:
                    textView.SetTextColor(ContextCompatEx.GetColor(textView.Context, Resource.Color.warning_foreground));
                    break;
            }
        }
     }
}
