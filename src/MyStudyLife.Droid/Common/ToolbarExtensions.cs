using System;
using Android.Views;
using Android.Widget;
using MyStudyLife.Droid.Widgets;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;

namespace MyStudyLife.Droid.Common {
    internal static class ToolbarExtensions {
        public static void EnsureConsistentStyle(this Toolbar toolbar) {
            // Ensure title set else the view won't exist
            string originalTitle = toolbar.Title;
            string title = originalTitle;

            if (String.IsNullOrEmpty(originalTitle)) {
                title = toolbar.Title = "My Study Life";
            }

            TextView titleTextView = null;
            Func<ViewGroup, bool> findTitleView = null;

            findTitleView = (parent) => {
                for (int i = 0; i < parent.ChildCount; i++) {
                    var view = parent.GetChildAt(i);
                    var textView = view as TextView;

                    if (textView != null && textView.Text == title) {
                        titleTextView = textView;
                        return true;
                    }

                    var viewGroup = view as ViewGroup;

                    if (viewGroup != null && findTitleView(viewGroup)) {
                        return true;
                    }
                }

                return false;
            };

            if (findTitleView(toolbar)) {
                titleTextView.SetRobotoTypeface(RobotoTypeface.Regular);
                titleTextView.SetTextSize(Android.Util.ComplexUnitType.Px, toolbar.Resources.GetDimensionPixelSize(Resource.Dimension.text_title));
            }

            toolbar.Title = originalTitle;
        }
    }
}