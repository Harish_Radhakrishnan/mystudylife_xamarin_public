using System.Reflection;
using MvvmCross.Exceptions;

namespace MyStudyLife.Droid.Common {
    public static class ReflectionHelper {
        public static TraversedProperty TraversePropertyPath(object obj, string propertyPath) {
            var propertyNames = propertyPath.Split('.');

            object currentObject = obj;
            PropertyInfo targetProperty = null;

            for (int i = 0; i < propertyNames.Length; i++) {
                var propName = propertyNames[i];

                targetProperty = currentObject.GetType().GetProperty(propName);

                if (targetProperty == null) {
                    throw new MvxException("Invalid property path '" + propertyPath + "'. Could not find property with name ' " + propName + "'.");
                }

                if (i < (propertyNames.Length - 1)) {
                    currentObject = targetProperty.GetValue(currentObject);
                }

                if (currentObject == null) {
                    // TODO: Log
                    return null;
                }
            }

            return new TraversedProperty(currentObject, targetProperty);
        }
    }

    public class TraversedProperty {
        private readonly object _object;
        private readonly PropertyInfo _propertyInfo;

        public object Value {
            get { return _propertyInfo.GetValue(_object); }
            set { _propertyInfo.SetValue(_object, value); }
        }

        public TraversedProperty(object obj, PropertyInfo propertyInfo) {
            this._object = obj;
            this._propertyInfo = propertyInfo;
        }
    }
}