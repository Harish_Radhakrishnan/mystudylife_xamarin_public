using System;
using Android.Content;
using Android.Views;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using AndroidX.ViewPager.Widget;
using Google.Android.Material.Tabs;

namespace MyStudyLife.Droid.Common {
    /// <summary>
    ///     A pager adapter for a fixed number of pages (such as a tab view)
    ///     that binds to the parent context.
    /// </summary>
    public class MvxFixedPagerAdapter : PagerAdapter {
        private readonly Context _context;
        private readonly IMvxAndroidBindingContext _bindingContext;
        private List<Page> _pages;
        private TabLayout _tabLayout;

        public event EventHandler<ItemInstantiatedEventArgs> ItemInstantiated;

        public IEnumerable<Page> Pages => _pages.AsEnumerable();

        public override int Count => _pages.Count;
        
        /// <summary>
        ///     Gets or sets the tab layout to be controlled by this
        ///     pager adapter. When set, tabs will be created by
        ///     calling <see cref="Page.InstantiateTab(TabLayout, IMvxAndroidBindingContext)"/>.
        /// </summary>
        public TabLayout TabLayout {
            get { return _tabLayout; }
            set {
                if (!Equals(_tabLayout, value)) {
                    _tabLayout = value;
                    SetTabsFromPages();
                }
            }
        }

        public MvxFixedPagerAdapter(Context context, IMvxAndroidBindingContext bindingContext, params Page[] pages) {
            this._context = context;
            this._bindingContext = bindingContext;
            this.SetPages(pages?.ToList() ?? new List<Page>());
        }
        
        public override bool IsViewFromObject(View view, Java.Lang.Object @object) => view == @object;

        public override Java.Lang.Object InstantiateItem(ViewGroup container, int position) {
            var p = _pages[position];
            var v = p.InstantiateView(container, _bindingContext);

            container.AddView(v, Math.Min(position, container.ChildCount));

            ItemInstantiated?.Invoke(this, new ItemInstantiatedEventArgs(p, v));

            return v;
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position) {
            return new Java.Lang.String(this._context.Resources.GetString(_pages[position].TitleResId));
        }
        
        public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object) {
            container.RemoveView((View)@object);
        }

        public void SetPages(params Page[] pages) => this.SetPages((IEnumerable<Page>)pages);

        public void SetPages(IEnumerable<Page> pages) {
            Check.NotNull(pages, nameof(pages));
            
            // MICRO OPTIMIZATION: Prevent multiple enumeration
            var pageList = pages.ToList();

            if ((this._pages == null || !this._pages.Any()) && !pageList.Any()) {
                return;
            }
            
            this._pages = pageList;

            this.SetTabsFromPages();

            this.NotifyDataSetChanged();
        }

        public void AddPage(Page page) {
            Check.NotNull(page, nameof(page));

            this._pages.Add(page);
            this._tabLayout?.AddTab(page.InstantiateTab(_tabLayout, _bindingContext));
            this.NotifyDataSetChanged();
        }

        public void InsertPage(int index, Page page) {
            Check.NotNull(page, nameof(page));

            this._pages.Insert(index, page);
            this._tabLayout?.AddTab(page.InstantiateTab(_tabLayout, _bindingContext), index);
            this.NotifyDataSetChanged();
        }

        public bool RemovePage(Page page) {
            Check.NotNull(page, nameof(page));

            int pageIndex = this._pages.IndexOf(page);

            if (pageIndex < 0) {
                return false;
            }

            this._pages.Remove(page);
            this._tabLayout?.RemoveTabAt(pageIndex);
            this.NotifyDataSetChanged();

            return true;
        }

        private void SetTabsFromPages() {
            if (this._tabLayout != null) {
                this._tabLayout.RemoveAllTabs();

                if (this._pages != null) {
                    foreach (var p in this._pages) {
                        this._tabLayout.AddTab(p.InstantiateTab(_tabLayout, _bindingContext));
                    }
                }
            }
        }

        public class Page {
            public Context Context { get; }
            public int TitleResId { get; }
            public int LayoutResId { get; }

            public Page(Context context, int titleResId, int layoutResId) {
                this.Context = context;
                this.TitleResId = titleResId;
                this.LayoutResId = layoutResId;
            }

            public virtual View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                return bindingContext.BindingInflate(this.LayoutResId, container, false);
            }

            public virtual TabLayout.Tab InstantiateTab(TabLayout tabLayout, IMvxAndroidBindingContext bindingContext) {
                return tabLayout.NewTab()
                    .SetContentDescription(TitleResId)
                    .SetText(TitleResId);
            }
        }

        public class ItemInstantiatedEventArgs : EventArgs {
            public Page Page { get; }
            public View View { get; }

            public ItemInstantiatedEventArgs(Page p, View v) {
                this.Page = p;
                this.View = v;
            }
        }
    }

    public static class TabLayoutExtensions {
        /// <summary>
        ///     Sets up the given <paramref name="tabLayout"/> to be controlled
        ///     by the given <paramref name="viewPager"/> and <paramref name="adapter"/>.
        /// </summary>
        /// <remarks>
        ///     We could get the adapter from the <see cref="ViewPager"/> but then we'd
        ///     have to think of another name for this method!
        /// </remarks>
        /// <param name="tabLayout"></param>
        /// <param name="viewPager"></param>
        /// <param name="adapter"></param>
        public static void SetupWithViewPager(this TabLayout tabLayout, ViewPager viewPager, MvxFixedPagerAdapter adapter) {
            Check.NotNull(tabLayout, nameof(tabLayout));
            Check.NotNull(viewPager, nameof(viewPager));

            adapter.TabLayout = tabLayout;

            tabLayout.SetOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
            viewPager.AddOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        }
    }
}