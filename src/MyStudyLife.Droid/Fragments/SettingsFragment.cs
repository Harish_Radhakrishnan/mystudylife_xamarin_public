using System;
using Android.App;
using Android.Runtime;
using Android.Views;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI.ViewModels.Settings;
using ActionBar = AndroidX.AppCompat.App.ActionBar;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.SettingsFragment")]
    public class SettingsFragment : BaseFragment<SettingsViewModel> {
		public override int LayoutResourceId => Resource.Layout.View_Settings;
        public override int? MenuResourceId => Resource.Menu.SettingsView;

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener
                .Listen(() => this.ViewModel.IsSigningOut, OnIsSigningOutChanged);
        }

        protected override void OnSetActionBar(ActionBar ab) {
			ab.SetTitle(Resource.String.Views_Settings);
		}

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (item.ItemId == Resource.Id.ActionSignOut && !this.ViewModel.IsSigningOut) {
                this.ViewModel.SignOutCommand.Execute(null);
            }

            return base.OnOptionsItemSelected(item);
        }
        
        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

        private void OnIsSigningOutChanged() {
            var activity = this.SafeGet(x => x.Activity);

            if (activity != null) {
                DialogHelper.ShowOrHideProgressDialog(activity, _progressDialogReference, this.ViewModel.IsSigningOut, Resource.String.SigningOutWithEllipsis);
            }
        }
	}
}