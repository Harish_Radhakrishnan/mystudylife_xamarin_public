using System;
using Android.Animation;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.Animations;
using MvvmCross.Binding.BindingContext;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Android;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Droid.Views;
using Microsoft.Extensions.Logging;
using AndroidX.Core.View;
using AndroidX.AppCompat.App;
using AndroidX.Fragment.App;
using Google.Android.Material.FloatingActionButton;
using Android.Widget;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;
using SearchView = AndroidX.AppCompat.Widget.SearchView;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView;

namespace MyStudyLife.Droid.Fragments {
    public abstract class EntityListFragment<TEntity, TViewModel> : BaseFragment<TViewModel>
        where TEntity : SubjectDependentEntity, new()
        where TViewModel : BaseEntitiesViewModel<TEntity> {

        private const string FilterFragmentTag = "filter";

        private Color _primaryColor;

        private Toolbar _toolbar;
        private View _titleBarView;
        private TextView _filterTextView;
        private FloatingActionButton _floatingActionButton;

        public override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            var outValue = new TypedValue();
            Activity.Theme.ResolveAttribute(Resource.Attribute.colorPrimary, outValue, true);
            _primaryColor = new Color(outValue.Data);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            _toolbar = Activity.FindViewById<Toolbar>(Resource.Id.Toolbar);
            _filterTextView = view.FindViewById<TextView>(Resource.Id.FilterText);
            _floatingActionButton = view.FindViewById<FloatingActionButton>(Resource.Id.FloatingActionButton);

            var recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.Recycler);

            recyclerView.SetLayoutManager(new FABLinearLayoutManager(this.Activity));

            var adapter = GetAdapter((IMvxAndroidBindingContext)this.BindingContext);
            recyclerView.Adapter = adapter;

            adapter.OnItemClick += AdapterOnItemClick;

            if (adapter is IStickyRecyclerHeadersAdapter stickyHeadersAdapter) {
                var headersDecor = new StickyRecyclerHeadersDecoration(stickyHeadersAdapter);
                recyclerView.AddItemDecoration(headersDecor);

                adapter.DataSetChanged += (s, e) => {
                    headersDecor.InvalidateHeaders();
                    recyclerView.InvalidateItemDecorations();
                };
            }

            recyclerView.AddItemDecoration(new DividerDecoration(this.Activity));

            return view;
        }

        public override void OnResume() {
            base.OnResume();

            var vm = this.ViewModel;

            if (vm != null) {
                this.SetColor();

                var filter = this.Activity.SupportFragmentManager.FindFragmentByTag(FilterFragmentTag) as EntityListFilterDialogFragment<TEntity>;

                if (filter != null) {
                    filter.ViewModel = this.ViewModel;
                }
            }
        }

        private void AdapterOnItemClick(object sender, EventArgs e) {
            //To avoid Action bar title overlap
            _titleBarView.Visibility = ViewStates.Gone;
            var viewHolder = (MvxRecyclerViewHolder)sender;

            Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntity((SubjectDependentEntity)viewHolder.DataContext, viewHolder.ItemView);
        }

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener.Listen(nameof(this.ViewModel.Color), SetColor);

            SetColor();
        }

        private void SetColor() {
            // Activity could be null if the fragment is being removed from the activity
            if (this.IsInMonoLimbo() || _toolbar == null || this.Activity == null) {
                return;
            }

            var primaryColor = ViewModel.Color?.ToNativeColor() ?? ContextCompatEx.GetColor(this.Context, Resource.Color.accent);
            var palette = new MaterialPalette(primaryColor);

            _toolbar.SetBackgroundColor(primaryColor);

            if (OS.HasLollipops) {
                ((CoreView)Activity).StatusBarColor = primaryColor;

                _floatingActionButton.BackgroundTintList = ColorStateList.ValueOf(primaryColor);
            }
            else {
                ViewCompat.SetBackgroundTintList(_floatingActionButton, new ColorStateList(
                    new[] {
                        new [] { Android.Resource.Attribute.StatePressed },
                        new int[0]
                    },
                    new[] {
                        palette.PrimaryColorLight,
                        (int) primaryColor
                    }
                ));
            }
        }

        protected override void OnSetActionBar(ActionBar ab) {
            base.OnSetActionBar(ab);

            ab.SetDisplayShowTitleEnabled(false);

            _titleBarView = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);
            var subtitleView = _titleBarView.FindViewById<TextView>(Resource.Id.ActionBarSubtitle);
            var titleView = _titleBarView.FindViewById<TextView>(Resource.Id.ActionBarTitle);

            _titleBarView.Click += ActionBarTitleOnClick;

            var set = this.CreateBindingSet<EntityListFragment<TEntity, TViewModel>, TViewModel>();

            set.Bind(titleView)
                .For(x => x.Text)
                .To(x => x.Title);
            set.Bind(subtitleView)
                .For(x => x.Text)
                .To(x => x.SelectedScheduleText);
            set.Bind(subtitleView)
                .For(x => x.Visibility)
                .To(x => x.SelectedScheduleText)
                .WithConversion("Visibility");

            set.Apply();

            ab.SetCustomView(_titleBarView, new ActionBar.LayoutParams((int)(GravityFlags.Left | GravityFlags.CenterVertical)));
            ab.SetDisplayShowCustomEnabled(true);
        }

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            GetFilterDialogFragment().Show(this.Activity.SupportFragmentManager, FilterFragmentTag);
        }

        protected abstract EntityListFilterDialogFragment<TEntity> GetFilterDialogFragment();

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
            base.OnCreateOptionsMenu(menu, inflater);

            if (!(this.ViewModel is ISearchable)) return;

            var searchItem = menu.FindItem(Resource.Id.ActionSearch);

            if (searchItem == null) return;

            var searchView = MenuItemCompat.GetActionView(searchItem).JavaCast<SearchView>();

            searchView.QueryTextChange += async (s, e) => {
                e.Handled = true;
                await this.ViewModel.ProcessQueryText(searchView.Query);
            };

            MenuItemCompat.SetOnActionExpandListener(searchItem, new SearchViewExpandListener(this, menu, this.ViewModel));

            try {
                var searchTextView = searchView.FindViewById<SearchView.SearchAutoComplete>(Resource.Id.search_src_text);

                // Half-prevents text selection (we can't prevent double tap)
                searchTextView.LongClickable = false;

                var field = Java.Lang.Class.FromType(typeof(TextView)).GetDeclaredField("mCursorDrawableRes");

                field.Accessible = true;
                field.Set(searchTextView, Resource.Drawable.cursor_white);
            }
            catch { }

            OnPrepareSearchView(searchView);
        }

        protected virtual void OnPrepareSearchView(SearchView view) { }

        public override void OnActionModeStarted(ActionMode mode) {
            base.OnActionModeStarted(mode);

            // Force text selection in search view to never happen as it overlays the search view
            if (!String.IsNullOrWhiteSpace(this.ViewModel?.QueryText)) {
                mode.Finish();

                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger("EntityListFragment").LogDebug("ActionMode force finish on start to prevent search view overlay.");
            }
        }

        protected abstract MvxRecyclerAdapter GetAdapter(IMvxAndroidBindingContext bindingContext);

        class SearchViewExpandListener : Java.Lang.Object, MenuItemCompat.IOnActionExpandListener {
            private readonly Fragment _fragment;
            private readonly IMenu _menu;
            private readonly ISearchable _viewModel;

            private readonly ITimeInterpolator _interpolator = new AccelerateDecelerateInterpolator();

            public SearchViewExpandListener(Fragment fragment, IMenu menu, ISearchable viewModel) {
                _fragment = fragment;
                _menu = menu;
                _viewModel = viewModel;
            }

            public bool OnMenuItemActionCollapse(IMenuItem item) {
                _menu.FindItem(Resource.Id.ActionToggleFilter).SetVisible(true);
                ToggleFAB(true);
                _viewModel.ClearQuery();
                return true;
            }

            public bool OnMenuItemActionExpand(IMenuItem item) {
                _menu.FindItem(Resource.Id.ActionToggleFilter).SetVisible(false);
                ToggleFAB(false);
                return true;
            }

            private void ToggleFAB(bool visible) {
                var fab = _fragment.View.FindViewById<FloatingActionButton>(Resource.Id.FloatingActionButton);

                if (fab == null) {
                    return;
                }

                var translationY = visible ? 0 : (fab.Height + ((fab.LayoutParameters as ViewGroup.MarginLayoutParams)?.BottomMargin ?? 0));

                fab.Animate()
                   .SetInterpolator(_interpolator)
                   .SetDuration(200)
                   .TranslationY(translationY);
            }
        }
    }
}