using System;
using System.ComponentModel;
using System.Globalization;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Scheduling;
using MyStudyLife.UI.ViewModels.Calendar;
using MvvmCross;

using MvvmCross.WeakSubscription;
using MyStudyLife.Droid.Platform;
using MyStudyLife.UI;
using MyStudyLife.Data;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Android;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.CalendarMonthFragment")]
	public class CalendarMonthFragment : CalendarFragment<CalendarMonthViewModel> {
		public override int LayoutResourceId => Resource.Layout.View_CalendarMonth;
	    public override int? MenuResourceId => Resource.Menu.CalendarMonthView;
        public override ScreenOrientation RequestedOrientation =>  ScreenOrientation.Portrait;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var androidBindingContext = (IMvxAndroidBindingContext)BindingContext;

            var pager = view.FindViewById<MvxViewPager>(Resource.Id.CalendarMonthPager);
            pager.Adapter = new CalendarMonthAdapter(Activity, androidBindingContext);

            using (new MvxBindingContextStackRegistration<IMvxAndroidBindingContext>((IMvxAndroidBindingContext) BindingContext)) {
                var agendaListView = view.FindViewById<StaticListView>(Resource.Id.AgendaList);
                agendaListView.Adapter = new FullWidthCardAdapter(Activity);
                agendaListView.OnItemClick += AgendaListOnItemClick;

                var taskListView = view.FindViewById<StaticListView>(Resource.Id.TaskList);
                taskListView.Adapter = new TasksAdapter(Activity);
                taskListView.OnItemClick += TaskListOnItemClick;
            }

            return view;
        }

        private void AgendaListOnItemClick(object sender, StaticListView.ItemClickEventArgs e)
            => Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntry((AgendaEntry)e.DataContext, e.View);
        private void TaskListOnItemClick(object sender, StaticListView.ItemClickEventArgs e)
            => Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntity((Task)e.DataContext, e.View);

        class TasksAdapter : FullWidthCardAdapter {
            public TasksAdapter(Context context) : base(context) { }

            protected override IMvxListItemView CreateBindableView(object dataContext, ViewGroup parent, int templateId)
                => new TaskListItemView(Context, BindingContext.LayoutInflaterHolder, dataContext, parent, templateId);

            class TaskListItemView : ListItemView, ITaskListItem {
                private IDisposable _subscription;

                private readonly Lazy<View> _colorView;
                private readonly Lazy<View> _innerView;
                private readonly Lazy<TextView> _titleView;
                private readonly Lazy<TextView> _subTitleView;
                private readonly Lazy<TextView> _captionView;

                public View ColorView => _colorView.Value;
                public View InnerView => _innerView.Value;
                public TextView TitleView => _titleView.Value;
                public TextView SubTitleView => _subTitleView.Value;
                public TextView CaptionView => _captionView.Value;

                public TaskListItemView(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext, ViewGroup parent, int templateId)
                    : base(context, layoutInflater, dataContext, parent, templateId) {

                    _colorView = new Lazy<View>(() => Content.FindViewById(Resource.Id.ListItemColor));
                    _innerView = new Lazy<View>(() => Content.FindViewById(Resource.Id.ListItemInner));
                    _titleView = new Lazy<TextView>(() => Content.FindViewById<TextView>(Resource.Id.ListItemTitle));
                    _subTitleView = new Lazy<TextView>(() => Content.FindViewById<TextView>(Resource.Id.ListItemSubTitle));
                    _captionView = new Lazy<TextView>(() => Content.FindViewById<TextView>(Resource.Id.ListItemCaption));

                    BindingContext.DataContextChanged += (s, e) => OnDataContextChanged();

                    OnDataContextChanged();
                }

                private void OnDataContextChanged() {
                    _subscription?.Dispose();

                    if (DataContext is Task task) {
                        _subscription = task.WeakSubscribe(DataContextOnPropertyChanged);
                        this.ApplyStyles(Content.Context);
                    }
                    else {
                        _subscription = null;
                    }
                }

                private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
                    if (e.PropertyName.In(nameof(Task.IsComplete), nameof(Task.DueDate))) {
                        this.ApplyStyles(Content.Context);
                    }
                }
            }
        }
    }

    public class CalendarMonthAdapter : MvxDynamicPagerAdapter {
        protected CalendarMonthAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CalendarMonthAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext) { }

        protected override MvxViewPagerItem GetView(int position, object item)
            => new CalendarMonthLayout(Context, BindingContext.LayoutInflaterHolder, item);
    }

    public sealed class CalendarMonthLayout : MvxViewPagerItem {

        public const float DayHeight = 60;

        private int? _outlineWidth;
        private const int DaySeparatorWidth = 1;

        private int? _headerHeightPixels;
        private int? _dayWidthPixels;
        private int? _dayHeightPixels;

        private int OutlineWidth {
            get {
                if (!_outlineWidth.HasValue) {
                    _outlineWidth = (int)Math.Round(Resources.DisplayMetrics.Density);
                }

                return _outlineWidth.Value;
            }
        }

        private int HeaderHeightPixels {
            get {
                if (!_headerHeightPixels.HasValue) {
                    _headerHeightPixels = Resources.DisplayMetrics.DpsToPixels(28);
                }

                return _headerHeightPixels.Value;
            }
        }

        private int DayWidthPixels {
            get {
                if (!_dayWidthPixels.HasValue) {
                    _dayWidthPixels = (int)Math.Round(Resources.DisplayMetrics.WidthPixels / 7d);
                }

                return _dayWidthPixels.Value;
            }
        }

        private int DayHeightPixels {
            get {
                if (!_dayHeightPixels.HasValue) {
                    _dayHeightPixels = Resources.DisplayMetrics.DpsToPixels(DayHeight);
                }

                return _dayHeightPixels.Value;
            }
        }

        private readonly Paint _outlinePaint;
        private readonly Paint _daySeparatorPaint;

        public new CalendarMonthModel DataContext {
            get { return (CalendarMonthModel) base.DataContext; }
        }

        public CalendarMonthLayout(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext) : base(context, layoutInflater, dataContext, 0) {
            var subtleColor = ContextCompatEx.GetColor(context, Resource.Color.subtle);

            _outlinePaint = new Paint {
                StrokeWidth = OutlineWidth,
                Color = subtleColor
            };

            _daySeparatorPaint = new Paint {
                StrokeWidth = DaySeparatorWidth,
                Color = subtleColor
            };

            SetBackgroundColor(Color.White);

            OnDataContextSet();
        }

        private IDisposable _subscription;

        private void OnDataContextSet() {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            if (DataContext != null) {
                _subscription = DataContext.WeakSubscribe(DataContextOnPropertyChanged);

                if (DataContext.Days != null) {
                    OnItemsSourceSet();
                }
            }
        }

        private void OnItemsSourceSet() {
            RemoveAllViews();

            for (int i = 0; i < 7; i++) {
                AddView(
                    new TextView(new ContextThemeWrapper(Context, Resource.Style.CalendarWeekHeading)) {
                        Text = Globalization.DateTimeFormat.GetShortDayName(Globalization.L10n.GetDayFromIndex(i))
                    },
                    new LayoutParams(DayWidthPixels, HeaderHeightPixels) {
                        LeftMargin =  i * DayWidthPixels
                    }
                );
            }

            var timetableMonth = DataContext;

            if (timetableMonth == null || timetableMonth.Days == null) {
                return;
            }

            for (int i = 0; i < timetableMonth.Days.Count; i++) {
                var v = new CalendarMonthDayLayout(Context, AndroidBindingContext.LayoutInflaterHolder, timetableMonth.Days[i]);

                v.Click += DayOnClick;

                AddView(
                    v,
                    new LayoutParams(DayWidthPixels - 1, DayHeightPixels - 1) {
                        TopMargin = HeaderHeightPixels + ((int)Math.Floor(i / 7f) * DayHeightPixels),
                        LeftMargin = (i % 7) * DayWidthPixels
                    }
                );
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (this.IsInMonoLimbo()) {
                return;
            }

            if (e.PropertyName == "Days") {
                OnItemsSourceSet();
            }
        }

        private void DayOnClick(object sender, EventArgs e) {
            if (DataContext == null) {
                return;
            }

            DataContext.SelectedDay = ((CalendarMonthDayLayout) sender).DataContext;
        }

        protected override void DispatchDraw(Canvas canvas) {
            // Draw lines over the top of the children
            base.DispatchDraw(canvas);

            var headerHeight = HeaderHeightPixels;
            var dayHeight = DayHeightPixels;

            var contentWidth = canvas.Width;
            var contentHeight = canvas.Height;

            var dayWidth = (int)Math.Round(contentWidth / 7f);

            for (int d = 0; d < 7; d++) {
                var x = d * dayWidth;
                var y = headerHeight + (d * dayHeight);

                canvas.DrawLine(x, headerHeight, x, contentHeight, _daySeparatorPaint);
                canvas.DrawLine(0, y, contentWidth, y, (d % 6) == 0 ? _outlinePaint : _daySeparatorPaint);
            }
        }
    }

    public sealed class CalendarMonthDayLayout : ViewGroup, IMvxBindingContextOwner {
        // Majority of ideas come from mvxBaseListItemView

        private readonly IMvxAndroidBindingContext _bindingContext;

        public IMvxAndroidBindingContext AndroidBindingContext {
            get { return _bindingContext; }
        }

        public IMvxBindingContext BindingContext {
            get { return _bindingContext; }
            set { throw new NotImplementedException("BindingContext is readonly in the list item"); }
        }

        public CalendarMonthModel.DayAndEntries DataContext {
            get { return (CalendarMonthModel.DayAndEntries) _bindingContext.DataContext; }
            set {
                if (_isAttachedToWindow) {
                    _bindingContext.DataContext = value;
                }
                else {
                    _cachedDataContext = value;

                    if (_bindingContext.DataContext != null) {
                        _bindingContext.DataContext = null;
                    }
                }
            }
        }

        private int? _entrySizePixels;

        public int EntrySizePixels {
            get {
                if (!_entrySizePixels.HasValue) {
                    _entrySizePixels = Resources.DisplayMetrics.DpsToPixels(8);
                }

                return _entrySizePixels.Value;
            }
        }

        public CalendarMonthDayLayout(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext) : base(context) {
            _bindingContext = new MvxAndroidBindingContext(context, layoutInflater, dataContext);

            var padding = Resources.DisplayMetrics.DpsToPixels(4);

            SetPadding(padding, padding, padding, padding);

            OnDataContextSet();
        }

        private IDisposable _subscription;

        private void OnDataContextSet() {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            if (DataContext != null) {
                _subscription = DataContext.WeakSubscribe(DataContextOnPropertyChanged);

                if (DataContext.Entries != null) {
                    OnItemsSourceSet();
                }
            }
        }

        private void OnItemsSourceSet() {
            RemoveAllViews();

            if (DataContext == null || DataContext.Entries == null) {
                return;
            }

            foreach (var entry in DataContext.Entries) {
                AddView(new CalendarMonthDayEntry(Context, entry));
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (this.IsInMonoLimbo()) {
                return;
            }

            if (e.PropertyName == "Entries") {
                OnItemsSourceSet();
            }
            else if (e.PropertyName.In("Day", "IsOtherMonth", "IsSelected")) {
                Invalidate();
            }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int measuredWidth = MeasureSpec.GetSize(widthMeasureSpec),
                measuredHeight = MeasureSpec.GetSize(heightMeasureSpec);

            SetMeasuredDimension(ResolveSize(measuredWidth, widthMeasureSpec), ResolveSize(measuredHeight, heightMeasureSpec));

            for (int i = 0; i < ChildCount; i++) {
                var child = GetChildAt(i);

                if (child.Visibility == ViewStates.Gone) {
                    continue;
                }

                var spec = MeasureSpec.MakeMeasureSpec(EntrySizePixels, MeasureSpecMode.Exactly);

                child.Measure(spec, spec);
            }
        }

        protected override void OnLayout(bool changed, int left, int top, int right, int bottom) {
            var childCount = ChildCount;

            if (childCount == 0) {
                return;
            }

            var padding = PaddingLeft;
            var spacing = Resources.DisplayMetrics.DpsToPixels(2);

            var entrySize = EntrySizePixels;

            var contentWidth = MeasuredWidth - (padding * 2);
            var contentHeight = MeasuredHeight - (padding * 2);

            var entriesPerRow = (int)Math.Floor((float)contentWidth / (entrySize + spacing));
            var rowCount = (int)Math.Ceiling((float)ChildCount / entriesPerRow);

            int childLeft = padding;
            int childTop = (contentHeight - (rowCount * (entrySize + spacing))) + spacing + padding;

            for (int i = 0; i < ChildCount; i++) {
                var child = GetChildAt(i);

                if (child.Visibility == ViewStates.Gone) {
                    continue;
                }

                child.Layout(
                    childLeft,
                    childTop,
                    childLeft + child.MeasuredWidth,
                    childTop + child.MeasuredHeight
                );

                if (((i + 1) % entriesPerRow) == 0) {
                    childLeft = padding;
                    childTop += (entrySize + spacing);
                }
                else {
                    childLeft += (entrySize + spacing);
                }
            }
        }

        protected override void DispatchDraw(Canvas canvas) {
            var day = DataContext;

            string text = day.Day.Day.ToString(CultureInfo.InvariantCulture);

            int textColorResId;

            if (day.IsCurrent) {
                textColorResId = Resource.Color.accent;
            }
            else if (day.IsOtherMonth) {
                textColorResId = Resource.Color.subtle_text;
            }
            else {
                textColorResId = Resource.Color.foreground;
            }

            var paint = new Paint(PaintFlags.AntiAlias | PaintFlags.SubpixelText) {
                TextAlign = Paint.Align.Left,
                TextSize = Resources.GetDimension(Resource.Dimension.text_calmonth_day),
                Color = ContextCompatEx.GetColor(Context, textColorResId)
            };
            paint.SetTypeface(RobotoWidgetHelper.GetRobotoTypeface(Context, RobotoTypeface.Regular));

            var textBounds = new Rect();
            paint.GetTextBounds(text, 0, text.Length, textBounds);

            if (day.IsSelected) {
                canvas.DrawColor(ContextCompatEx.GetColor(Context, Resource.Color.accent_light));

                paint.Color = Color.White;
            }
            else if (day.IsHoliday) {
                canvas.DrawColor(ContextCompatEx.GetColor(Context, Resource.Color.subtle_super));
            }

            // TODO: Optimise this so it isn't called for every day
            if (day.IsHoliday) {
                var glyphSizePx = 16 * Resources.DisplayMetrics.ScaledDensity;

                var holPaint = new Paint(PaintFlags.AntiAlias | PaintFlags.SubpixelText) {
                    Color = DataContext.IsSelected ? Color.White : ContextCompatEx.GetColor(Context, Resource.Color.subtle_text),
                    TextAlign = Paint.Align.Center,
                    TextSize = glyphSizePx
                };
                holPaint.SetTypeface(Typeface.CreateFromAsset(Context.Assets, "fonts/MslIcons.ttf"));

                string holGlyph = ((char)Glyph.Holiday).ToString();

                var holIconBounds = new Rect();
                holPaint.GetTextBounds(holGlyph, 0, holGlyph.Length, holIconBounds);

                canvas.DrawText(
                    holGlyph,
                    canvas.Width / 2f,
                    (PaddingTop + textBounds.Bottom + (canvas.Height / 2f)) - holIconBounds.ExactCenterY(),
                    holPaint
                );
            }

            canvas.DrawText(
                text,
                -textBounds.Left + PaddingLeft,
                -textBounds.Top + PaddingTop,
                paint
            );

            // If extra perf needed in the future we could just draw the
            // entries too (not sure how much faster that would be though).

            base.DispatchDraw(canvas);
        }

        #region Lifecycle Handling

        private CalendarMonthModel.DayAndEntries _cachedDataContext;
        private bool _isAttachedToWindow;

        protected override void OnAttachedToWindow() {
            base.OnAttachedToWindow();
            _isAttachedToWindow = true;
            if (_cachedDataContext != null
                && DataContext == null) {
                DataContext = _cachedDataContext;
            }
        }

        protected override void OnDetachedFromWindow() {
            _cachedDataContext = DataContext;
            DataContext = null;
            base.OnDetachedFromWindow();
            _isAttachedToWindow = false;
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                this.ClearAllBindings();
                _cachedDataContext = null;
            }

            base.Dispose(disposing);
        }

        #endregion
    }

    public sealed class CalendarMonthDayEntry : View {
        private readonly int _cornerRadius;

        private readonly Paint _backgroundPaint;
        private readonly Paint _strokePaint;

        public CalendarMonthDayEntry(Context context, AgendaEntry entry) : base(context) {
            _cornerRadius = context.Resources.GetDimensionPixelSize(Resource.Dimension.corner_radius);

            _backgroundPaint = new Paint(PaintFlags.AntiAlias) {
                Color = entry.ColorValue.ToNativeColor(),
                StrokeWidth = Resources.DisplayMetrics.Density
            };
            _backgroundPaint.SetStyle(Paint.Style.Fill);

            _strokePaint = new Paint(PaintFlags.AntiAlias) {
                Color = Color.White,
                StrokeWidth = Resources.DisplayMetrics.Density
            };
            _strokePaint.SetStyle(Paint.Style.Stroke);
        }

        public override void Draw(Canvas canvas) {
            base.Draw(canvas);

            var rect = new RectF(0, 0, canvas.Width, canvas.Height);

            canvas.DrawRoundRect(rect, _cornerRadius, _cornerRadius, _backgroundPaint);
            canvas.DrawRoundRect(rect, _cornerRadius, _cornerRadius, _strokePaint);
        }
    }
}