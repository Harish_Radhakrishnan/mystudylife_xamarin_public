using Android.Runtime;
using Android.Views;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.ExamsFragment")]
    public class ExamsFragment : EntityListFragment<Exam, ExamsViewModel> {
        public override int LayoutResourceId => Resource.Layout.View_Exams;

        public override int? MenuResourceId => Resource.Menu.Exams;

        protected override EntityListFilterDialogFragment<Exam> GetFilterDialogFragment() => ExamListFilterDialogFragment.NewInstance(this.ViewModel);

        public override void OnPrepareOptionsMenu(IMenu menu) {
            base.OnPrepareOptionsMenu(menu);

            menu.FindItem(Resource.Id.ActionToggleFilter)?.SetTitle(
                this.ViewModel.Filter == ExamFilterOption.Current
                    ? Resource.String.ShowPastExams
                    : Resource.String.ShowCurrentExams
            );
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.ActionToggleFilter:
                    this.ViewModel.ToggleFilterCommand.Execute(null);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override MvxRecyclerAdapter GetAdapter(IMvxAndroidBindingContext bindingContext) {
            return new GroupedExamsRecyclerAdapter(bindingContext);
        }
    }
}