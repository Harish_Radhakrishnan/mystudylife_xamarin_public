using System;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using AndroidX.AppCompat.Widget;
using Java.Interop;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views.Fragments;
using MvvmCross.ViewModels;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Base;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.FragmentRootLayout")]
    public class FragmentRootLayout : LinearLayout {
        protected FragmentRootLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}

        public FragmentRootLayout(Context context) : base(context) {}

        public FragmentRootLayout(Context context, IAttributeSet attrs) : base(context, attrs) {}

        public FragmentRootLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {}

        [Export("getXFraction")]
        public float GetXFraction() {
            return (Width == 0) ? 0 : GetX()/ Width;
        }

        [Export("setXFraction")]
        public void SetXFraction(float xFraction) {
            SetX((Width > 0) ? (xFraction * Width) : 0);
        }
    }

    [Register("mystudylife.droid.fragments.FragmentRootFrameLayout")]
    public class FragmentRootFrameLayout : FrameLayout {
        protected FragmentRootFrameLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public FragmentRootFrameLayout(Context context) : base(context) { }

        public FragmentRootFrameLayout(Context context, IAttributeSet attrs) : base(context, attrs) { }

        public FragmentRootFrameLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) { }

        [Export("getXFraction")]
        public float GetXFraction() {
            return (Width == 0) ? 0 : GetX() / Width;
        }

        [Export("setXFraction")]
        public void SetXFraction(float xFraction) {
            SetX((Width > 0) ? (xFraction * Width) : 0);
        }
    }

    [Register("mystudylife.droid.fragments.FragmentRootScrollableLayout")]
    public class FragmentRootScrollableLayout : ScrollView {
        protected FragmentRootScrollableLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}

        public FragmentRootScrollableLayout(Context context) : base(context) {}

        public FragmentRootScrollableLayout(Context context, IAttributeSet attrs) : base(context, attrs) {}

        public FragmentRootScrollableLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {}

        [Export("getXFraction")]
        public float GetXFraction() {
            return (Width == 0) ? 0 : GetX() / Width;
        }

        [Export("setXFraction")]
        public void SetXFraction(float xFraction) {
            SetX((Width > 0) ? (xFraction * Width) : 0);
        }
    }

	public abstract class BaseFragment<TViewModel> : BaseFragment
		where TViewModel : BaseViewModel {

		public new TViewModel ViewModel {
			get => (TViewModel) base.ViewModel;
            set => base.ViewModel = value;
        }
	}

    public abstract class BaseFragment :MvxFragment<BaseViewModel> {

        private MvxPropertyChangedListener _vmListener;

	    public virtual ScreenOrientation RequestedOrientation => ScreenOrientation.User;

        public abstract int LayoutResourceId {
			get;
		}

		/// <summary>
		///		The resId of the menu to show
		///		for this fragment.
		/// </summary>
		public virtual int? MenuResourceId => null;

        public new BaseViewModel ViewModel {
            get => (BaseViewModel) base.ViewModel;
            set => base.ViewModel = value;
        }

        public MvxPropertyChangedListener ViewModelListener {
            get {
                if (_vmListener == null) {
                    throw new Exception("Cannot get ViewModelListener before ViewModel is set.");
                }

                return _vmListener;
            }
        }

	    protected LayoutInflater LayoutInflater => Activity.LayoutInflater;

        protected BaseFragment() {
            HasOptionsMenu = true;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			base.OnCreateView(inflater, container, savedInstanceState);

			return ((IMvxAndroidBindingContext)BindingContext).BindingInflate(LayoutResourceId, container, false);
		}

	    public override void OnResume() {
	        base.OnResume();

	        this.Activity.RequestedOrientation = RequestedOrientation;

            // Reset action bar
            var ab = ((AppCompatActivity)this.Activity).SupportActionBar;

	        ab.Title = null;
            ab.Subtitle = null;
            //ab.NavigationMode = ActionBarNavigationMode.Standard;
            //ab.CustomView = null;

            ab.SetDisplayShowTitleEnabled(true);
            ab.SetDisplayShowCustomEnabled(false);

            OnSetActionBar(ab);
	    }

	    protected virtual void OnSetActionBar(ActionBar ab) { }

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            _vmListener?.Dispose();
            _vmListener = null;

            var vm = ViewModel;
            if (vm == null) {
                return;
            }

            _vmListener = new MvxPropertyChangedListener(vm);

            if (vm.IsLoaded) {
                ViewModelOnLoadedCore();
            }
            else {
                _vmListener.Listen(nameof(ILoaded.IsLoaded), () => {
                    if (vm.IsLoaded) {
                        ViewModelOnLoadedCore();
                    }
                });
            }
        }

        private void ViewModelOnLoadedCore() => ViewModelOnLoaded();

        /// <summary>
        ///     Called when the view model is loaded if it implements <see cref="ILoaded"/>,
        ///     otherwise called when set.
        /// </summary>
        protected virtual void ViewModelOnLoaded() { }

        public virtual void OnActionModeStarted(ActionMode mode) {

        }

        public virtual void OnActionModeFinished(ActionMode mode) {

        }

	    public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater) {
	        if (MenuResourceId.HasValue) {
                Activity?.FindViewById<Toolbar>(Resource.Id.Toolbar)?.InflateMenu(MenuResourceId.Value);
	        }

	        base.OnCreateOptionsMenu(menu, inflater);
        }

        public void InvalidateOptionsMenu() {
            this.SafeGet(x => x.Activity)?.InvalidateOptionsMenu();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _vmListener?.Dispose();
                _vmListener = null;
            }

            base.Dispose(disposing);
        }
	}
}