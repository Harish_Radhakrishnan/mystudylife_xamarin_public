using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels.Calendar;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using PopupMenu = AndroidX.AppCompat.Widget.PopupMenu;

namespace MyStudyLife.Droid.Fragments {
	public abstract class CalendarFragment<TViewModel> : BaseFragment<TViewModel>
		where TViewModel : CalendarViewModel {

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener.Listen(() => this.ViewModel.IsSelectedPeriodCurrent, InvalidateOptionsMenu);
        }

		protected override void OnSetActionBar(ActionBar ab) {
            base.OnSetActionBar(ab);

            ab.SetDisplayShowTitleEnabled(false);

            var v = GetActionBarView();

            v.Click += ActionBarTitleOnClick;

            ab.SetCustomView(v, new ActionBar.LayoutParams((int)(GravityFlags.Left | GravityFlags.CenterVertical)));
            ab.SetDisplayShowCustomEnabled(true);
		}

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            var popup = new MslPopupMenu(this.Activity, (View) sender);

            popup.MenuInflater.Inflate(Resource.Menu.CalendarViewPopup, popup.Menu);

            popup.MenuItemClick += PopupOnMenuItemClick;

            popup.Show();
        }

	    private void PopupOnMenuItemClick(object sender, PopupMenu.MenuItemClickEventArgs e) {
            if (e.Item.ItemId == Resource.Id.ActionMonth && this is CalendarWeekFragment) {
                this.ViewModel.SwitchViewCommand.Execute(null);
            }
            else if (e.Item.ItemId == Resource.Id.ActionWeek && this is CalendarMonthFragment) {
                this.ViewModel.SwitchViewCommand.Execute(null);
            }
	    }

	    protected virtual View GetActionBarView() {
            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);

            var titleView = v.FindViewById<TextView>(Resource.Id.ActionBarTitle);
            titleView.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.spinner_dropdown_arrow_white, 0);
            titleView.CompoundDrawablePadding = Resources.GetDimensionPixelSize(Resource.Dimension.margin_xs);

            v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle).Visibility = ViewStates.Gone;

            var set = this.CreateBindingSet<CalendarFragment<TViewModel>, TViewModel>();

            set.Bind(titleView)
                .For(x => x.Text)
                .To(x => x.PageTitle);

            set.Apply();

	        return v;
	    }

	    public override bool OnOptionsItemSelected(IMenuItem item) {
			switch (item.ItemId) {
                case Resource.Id.ActionCalendarShowCurrent:
					this.ViewModel.CurrentPeriodCommand.Execute(null);
					return true;
			}
			
			return base.OnOptionsItemSelected(item);
		}

	    public override void OnPrepareOptionsMenu(IMenu menu) {
	        var item = menu.FindItem(Resource.Id.ActionCalendarShowCurrent);
            var viewModel = this.ViewModel;

            if (item != null && viewModel != null) {
                var isSelectedPeriodCurrent = viewModel.IsSelectedPeriodCurrent;

                item.SetEnabled(!isSelectedPeriodCurrent);
                item.Icon.Mutate().SetAlpha(isSelectedPeriodCurrent ? 130 : 255);
            }

	        base.OnPrepareOptionsMenu(menu);
	    }
	}

    [Register("mystudylife.droid.fragments.CalendarViewPager")]
	public class CalendarViewPager : MvxViewPager {
        protected CalendarViewPager(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CalendarViewPager(Context context) : base(context) { }
        public CalendarViewPager(Context context, IAttributeSet attrs) : base(context, attrs) { }

	    protected override void SetSelectedItem(object item) {
            // Prevents animation (and therefore stuttering) when SelectedItem
            // is first set.
	        this.SetCurrentItem(Adapter.GetPosition(item), this.CurrentItem != 0);
	    }
	}
}