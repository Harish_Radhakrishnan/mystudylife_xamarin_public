using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.UI.Data;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.TasksFragment")]
    public class TasksFragment : EntityListFragment<Task, TasksViewModel> {
        private RichTextView _lonelySubTitleView;

        public override int LayoutResourceId => Resource.Layout.View_Tasks;

        public override int? MenuResourceId => Resource.Menu.Tasks;

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener.Listen(nameof(ViewModel.IsPastFilter), SetLonleySubTitle);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            _lonelySubTitleView = view.FindViewById<RichTextView>(Resource.Id.LonelySubTitle);

            _lonelySubTitleView.LinkClick += LonelySubTitleViewOnLinkClick;

            SetLonleySubTitle();

            return view;
        }

        private void LonelySubTitleViewOnLinkClick(object sender, LocalLinkEventArgs e) {
            var link = e.ClickedSpans[0] as URLSpan;

            if (link == null) return;

            switch (link.URL) {
                case "FILTER_CURRENT":
                case "FILTER_PAST":
                    this.ViewModel.ToggleFilterCommand.Execute(null);
                    break;
            }

            e.Handled = true;
        }

        private void SetLonleySubTitle() {
            // Fixes bug when changing filter after rotation
            if (!this.IsInMonoLimbo() && this.IsAdded) {
                var tv = _lonelySubTitleView.SafeGet();

                int resId = (this.ViewModel != null && this.ViewModel.IsPastFilter) ? Resource.String.SearchCurrentTasksPrompt : Resource.String.SearchPastTasksPrompt;

                tv.SetText(Html.FromHtml(Resources.GetString(resId)), TextView.BufferType.Spannable);
            }
        }

        protected override EntityListFilterDialogFragment<Task> GetFilterDialogFragment() => TaskListFilterDialogFragment.NewInstance(this.ViewModel);

        protected override void OnPrepareSearchView(AndroidX.AppCompat.Widget.SearchView view) {
            base.OnPrepareSearchView(view);
            // Hack as it seems impossible to apply text color styles pre API 21 (JC: I tried everything I could find)
            if (!OS.CheckSystemVersion(BuildVersionCodes.Lollipop)) {
                var srcText = view.FindViewById<EditText>(Resource.Id.search_src_text);

                // TODO: Refactor use to variables
                srcText.SetTextColor(Android.Graphics.Color.White);
                srcText.SetHintTextColor(new Android.Graphics.Color(255, 255, 255, 127)); // 50% white alpha
            }

            view.QueryHint = R.Filter_tasks;
        }

        public override void OnPrepareOptionsMenu(IMenu menu) {
            base.OnPrepareOptionsMenu(menu);

            menu.FindItem(Resource.Id.ActionToggleFilter)?.SetTitle(
                this.ViewModel.Filter == TaskFilterOption.Current
                    ? Resource.String.ShowPastTasks
                    : Resource.String.ShowCurrentTasks
            );
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.ActionToggleFilter:
                    this.ViewModel.ToggleFilterCommand.Execute(null);
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected override MvxRecyclerAdapter GetAdapter(IMvxAndroidBindingContext bindingContext) {
            return new GroupedRecyclerAdapter(bindingContext);
        }

        class GroupedRecyclerAdapter : GroupedTasksRecyclerAdapter {
            public GroupedRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

            protected override TaskGrouping GetGrouping(int position) {
                return ((TasksViewModel)BindingContext.DataContext).GetGrouping(position);
            }
        }
    }
}