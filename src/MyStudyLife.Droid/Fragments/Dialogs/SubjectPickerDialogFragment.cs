using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using AFollestad.MaterialDialogs;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using MvvmCross;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Extensions;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Utility;
using Object = Java.Lang.Object;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    public sealed class SubjectPickerDialogFragment : DialogFragment {
        #region IOnSubjectSetListener

        // In keeping with android style...

        public interface IOnSubjectSetListener {
            void OnSubjectSet(Subject subject);
        }

        #endregion
        
        private readonly Context _context;
        private readonly IOnSubjectSetListener _listener;
        private readonly ObservableCollection<Subject> _subjects;

        [UsedImplicitly]
        private readonly MvxNotifyCollectionChangedEventSubscription _subjectsSubscription;

        public Subject SelectedSubject { get; private set; }

        /// <value>
        ///     True to show the new button in the dialog.
        /// </value>
        public bool ShowNew { get; set; }

        /// <value>
        ///     True to show the "None" option.
        /// </value>
        public bool ShowNone { get; set; }

        private int _titleResId = Resource.String.Subject_SelectSubject;

        public int TitleResId {
            set {
                _titleResId = value;

                this.SafeGet(x => x.Dialog)?.SetTitle(value);
            }
        }

        // REQUIRED, PUBLIC, DO NOT REMOVE
        public SubjectPickerDialogFragment() {
            // Dismiss dialog on restore (lazy) as it won't reattach to the same listener
            DismissAllowingStateLoss();
        }
        
        public SubjectPickerDialogFragment(Context context, IOnSubjectSetListener listener, ObservableCollection<Subject> subjects, Subject subject) {
			_context = context;
			_listener = listener;

            if (subjects != null) {
                _subjects = subjects;

                _subjectsSubscription = _subjects.WeakSubscribe(OnSubjectsCollectionChanged);
            }
            else {
                _subjects = new ObservableCollection<Subject>();
            }

            SelectedSubject = subject;
            ShowNew = true;

            // Prevents recreation on rotate
            RetainInstance = true;
        }
        
        public override Android.App.Dialog OnCreateDialog(Bundle savedInstanceState) {
            var view = ((LayoutInflater) _context.GetSystemService(Context.LayoutInflaterService)).Inflate(Resource.Layout.Dialog_SubjectPicker, null);

            var listView = view.FindViewById<ListView>(Resource.Id.SubjectPickerListView);

            listView.Adapter = new SubjectPickerAdapter(_context, _subjects, SelectedSubject, ShowNone);
            listView.ItemClick += ListViewOnItemClick;
            listView.ChoiceMode = ChoiceMode.Single;

            var checkedPos = SelectedSubject != null ? _subjects.FirstIndexOf(x => x.Guid == SelectedSubject.Guid) : -1;

            if (checkedPos > -1) {
                listView.SetItemChecked(checkedPos, true);
            }

            view.FindViewById<View>(Resource.Id.LonelyView).Visibility = _subjects.Any()
                ? ViewStates.Gone
                : ViewStates.Visible;

            var builder = new MaterialDialog.Builder(_context)
                .Title(_titleResId)
                .CustomView(view, false);

            if (ShowNew && Mvx.IoCProvider.Resolve<IFeatureService>().CanUseFeatureAsync(Feature.WriteSubjects).GetResult())  {
                builder
                   .PositiveText(Resource.String.Common_New)
                   .Callback(new DialogHelper.ButtonCallbackShim(
                        async () => await Mvx.IoCProvider.Resolve<INavigationService>().Navigate<SubjectInputViewModel>()
                   ));

                view.FindViewById<LonelyView>(Resource.Id.LonelyView).SetText(Resource.String.Subject_PickerLonelyWithNew);
            }

            return builder.Build();
        }

        private void ListViewOnItemClick(object sender, AdapterView.ItemClickEventArgs e) {
            Subject subject;

            if (ShowNone) {
                subject = e.Position == 0 ? null : _subjects[e.Position - 1];
            }
            else {
                subject = _subjects[e.Position];
            }

            _listener.OnSubjectSet(subject);

            Dismiss();
        }

        private void OnSubjectsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (Dialog != null) {
                ((SubjectPickerAdapter)Dialog.FindViewById<ListView>(Resource.Id.SubjectPickerListView).Adapter).NotifyDataSetChanged();

                Dialog.FindViewById<View>(Resource.Id.LonelyView).Visibility = _subjects.Any()
                    ? ViewStates.Gone
                    : ViewStates.Visible;
            }
        }

        class SubjectPickerAdapter : BaseAdapter {
            private readonly LayoutInflater _layoutInflater;
            private readonly IList<Subject> _subjects;
            private readonly Subject _selectedSubject;
            private readonly bool _showNone;

            public override int Count => _subjects.Count + (_showNone ? 1 : 0);

            public SubjectPickerAdapter(Context context, IList<Subject> subjects, Subject selectedSubject, bool showNone) {
                _layoutInflater = (LayoutInflater) context.GetSystemService(Context.LayoutInflaterService);
                _subjects = subjects;
                _selectedSubject = selectedSubject;
                _showNone = showNone;
            }

            public override Object GetItem(int position) {
                return null;
            }

            public override long GetItemId(int position) {
                return position;
            }

            public override View GetView(int position, View convertView, ViewGroup parent) {
                var view = convertView ?? _layoutInflater.Inflate(Resource.Layout.ListItem_SubjectPicker, parent, false);
                var colorView = view.FindViewById<View>(Resource.Id.SubjectPickerItem_Color);
                var textView = view.FindViewById<TextView>(Resource.Id.SubjectPickerItem_Name);

                if (position == 0 && _showNone) {
                    colorView.Visibility = ViewStates.Gone;

                    textView.SetText(Resource.String.None);
                    textView.Selected = _selectedSubject == null;
                }
                else {
                    var subject = _subjects[position - (_showNone ? 1 : 0)];

                    colorView.Visibility = ViewStates.Visible;
                    // TODO: remove call to UserColors.GetColor
                    var canUseExtendedColors = Mvx.IoCProvider.Resolve<IFeatureService>().CanUseFeatureAsync(Feature.ExtendedColors).ConfigureAwait(false).GetAwaiter().GetResult();
                    colorView.Background.Mutate().SetColorFilter(UserColors.GetColor(subject.Color, canUseExtendedColors).Color.ToNativeColor(), PorterDuff.Mode.Multiply);
                    
                    textView.Text = subject.Name;
                    textView.Selected = _selectedSubject != null && _selectedSubject.Guid == subject.Guid;
                }
                
                return view;
            }
        }
    }
}