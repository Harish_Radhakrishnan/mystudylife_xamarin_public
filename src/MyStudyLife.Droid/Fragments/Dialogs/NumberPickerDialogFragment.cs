using AFollestad.MaterialDialogs;
using Android.App;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Widget;
using MyStudyLife.Droid.Common;
using DialogFragment = AndroidX.Fragment.App.DialogFragment;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    public sealed class NumberPickerDialogFragment : DialogFragment, NumberPicker.IOnValueChangeListener {
        private readonly int _titleResId, _min, _max, _current;
        private readonly NumberPicker.IOnValueChangeListener _listener;
        private readonly string _pickerTag;

        private int _newValue;

        // REQUIRED, PUBLIC, DO NOT REMOVE
        public NumberPickerDialogFragment() {
            // Dismiss dialog on restore (lazy) as it won't reattach to the same listener
            this.DismissAllowingStateLoss();
        }

        public NumberPickerDialogFragment(int titleResId, int min, int max, int current, NumberPicker.IOnValueChangeListener listener) {
            _titleResId = titleResId;
            _min = min;
            _max = max;
            _current = _newValue = current;
            _listener = listener;

            // Prevents recreation on rotate
            this.RetainInstance = true;
        }

        public NumberPickerDialogFragment(int titleResId, int min, int max, int current,
            NumberPicker.IOnValueChangeListener listener, string pickerTag)
            : this(titleResId, min, max, current, listener) {
            _pickerTag = pickerTag;
        }

        public override Dialog OnCreateDialog(Bundle savedState) {
            var numberPicker = new NumberPicker(this.Activity) {
                MaxValue = _max,
                MinValue = _min,
                Value = _current,
                Tag = _pickerTag
            };

            numberPicker.SetOnValueChangedListener(this);

            try {
                var @class = Java.Lang.Class.FromType(typeof(NumberPicker));

                var divider = @class.GetDeclaredField("mSelectionDivider");

                divider.Accessible = true;
                divider.Set(numberPicker, new ColorDrawable(ContextCompatEx.GetColor(Context, Resource.Color.accent)));
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return new MaterialDialog.Builder(this.Activity)
                .Title(_titleResId)
                .CustomView(numberPicker, false)
                .NegativeText(Resource.String.Common_Cancel)
                .PositiveText(Resource.String.Common_Ok)
                .Callback(new DialogHelper.ButtonCallbackShim(
                    onPositive: () => {
                        if (_newValue != _current) {
                            _listener.OnValueChange(numberPicker, _current, _newValue);
                        }
                    }
                ))
                .Build();
        }

        public void OnValueChange(NumberPicker picker, int oldVal, int newVal) {
            _newValue = newVal;
        }
    }
}