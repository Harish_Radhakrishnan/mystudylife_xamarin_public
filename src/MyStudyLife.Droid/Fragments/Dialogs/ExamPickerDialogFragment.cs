using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using AFollestad.MaterialDialogs;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI.Annotations;
using Object = Java.Lang.Object;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    public sealed class ExamPickerDialogFragment : DialogFragment {
        #region IOnExamSetListener

        // In keeping with android style...

        public interface IOnExamSetListener {
            void OnExamSet(Exam exam);
        }

        #endregion
        
        private readonly IOnExamSetListener _listener;
        private readonly ObservableCollection<Exam> _exams;

        private Adapter _adapter;

        [UsedImplicitly]
        private readonly IDisposable _examsSubscription;

        public Exam SelectedExam { get; private set; }

        // REQUIRED, PUBLIC, DO NOT REMOVE
        public ExamPickerDialogFragment() {
            // Dismiss dialog on restore (lazy) as it won't reattach to the same listener
            DismissAllowingStateLoss();
        }

        public ExamPickerDialogFragment(IOnExamSetListener listener, ObservableCollection<Exam> exams, Exam exam) {
			_listener = listener;

            if (exams != null) {
                _exams = exams;

                _examsSubscription = _exams.WeakSubscribe(OnExamsCollectionChanged);
            }
            else {
                _exams = new ObservableCollection<Exam>();
            }

            SelectedExam = exam;

            // Prevents recreation on rotate
            RetainInstance = true;
        }
        
        public override Android.App.Dialog OnCreateDialog(Bundle savedInstanceState) {
            var view = ((LayoutInflater)Activity.GetSystemService(Context.LayoutInflaterService)).Inflate(Resource.Layout.Dialog_ExamPicker, null);

            var listView = view.FindViewById<ListView>(Resource.Id.ListView);

            listView.Adapter = _adapter = new Adapter(Activity, _exams, SelectedExam);
            listView.ItemClick += ListViewOnItemClick;
            listView.ChoiceMode = ChoiceMode.Single;

            bool anyExams = _exams.Any();

            listView.SetVisible(anyExams);
            view.FindViewById<View>(Resource.Id.LonelyView).SetVisible(!anyExams);

            return new MaterialDialog.Builder(Activity)
                .Title(Resource.String.SelectExam)
                .CustomView(view, false)
                .Build();
        }

        private void ListViewOnItemClick(object sender, AdapterView.ItemClickEventArgs e) {
            _listener.OnExamSet(e.Position == 0 ? null : _exams[e.Position - 1]);

            Dismiss();
        }

        private void OnExamsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (_adapter != null) {
                _adapter.NotifyDataSetChanged();
            }

            var dialog = Dialog;

            if (dialog != null) {
                dialog.FindViewById<View>(Resource.Id.LonelyView).Visibility = _exams.Any()
                    ? ViewStates.Gone
                    : ViewStates.Visible;
            }
        }

        class Adapter : BaseAdapter {
            private readonly Context _context;
            private readonly LayoutInflater _layoutInflater;
            private readonly IList<Exam> _exams;
            private readonly Exam _selectedExam;

            public override int Count {
                get { return _exams.Count + 1; }
            }

            public Adapter(Context context, IList<Exam> subjects, Exam selectedExam) {
                _context = context;
                _layoutInflater = (LayoutInflater) context.GetSystemService(Context.LayoutInflaterService);
                _exams = subjects;
                _selectedExam = selectedExam;
            }

            public override Object GetItem(int position) {
                return null;
            }

            public override long GetItemId(int position) {
                return position;
            }

            public override View GetView(int position, View convertView, ViewGroup parent) {
                //if (position == 0) {
                //    var noneView = convertView as TextView;

                //    if (noneView == null) {
                //        noneView = new RobotoTextView(_context) {
                //            LayoutParameters = new ListView.LayoutParams(
                //                ListView.LayoutParams.MatchParent,
                //                _context.Resources.DisplayMetrics.DpsToPixels(56)
                //            ),
                //            Gravity = GravityFlags.CenterVertical
                //        };

                //        var padding = _context.Resources.GetDimensionPixelSize(Resource.Dimension.dialog_margin);

                //        noneView.SetPadding(padding, 0, padding, 0);
                //        noneView.SetTextColor(_context.Resources.GetColorStateList(Resource.Color.foreground_selectable));
                //        noneView.SetTextSize(ComplexUnitType.Px, _context.Resources.GetDimension(Resource.Dimension.text_subhead));
                //        noneView.SetText(Resource.String.None);
                //    }

                //    noneView.Selected = _selectedExam == null;

                //    return noneView;
                //}
                
                var view = convertView as ViewGroup ?? _layoutInflater.Inflate(Resource.Layout.ListItem_ExamPicker, parent, false);
                var titleTextView = view.FindViewById<TextView>(Resource.Id.ListItemTitle);
                var subTitleTextView = view.FindViewById<TextView>(Resource.Id.ListItemSubTitle);

                bool isSelected;

                if (position == 0) {
                    isSelected = _selectedExam == null;

                    titleTextView.SetText(Resource.String.None);
                    subTitleTextView.Text = null;
                    subTitleTextView.Visibility = ViewStates.Gone;
                }
                else {
                    var exam = _exams[position - 1];

                    isSelected = _selectedExam != null && _selectedExam.Guid == exam.Guid;

                    titleTextView.Text = exam.Title;
                    subTitleTextView.Text = exam.Date.ToShortTimeStringEx() + " " + exam.Date.ToString("D");
                    subTitleTextView.Visibility = ViewStates.Visible;
                }

                titleTextView.Selected = isSelected;
                subTitleTextView.Selected = isSelected;
                
                return view;
            }
        }
    }
}