using System;
using AFollestad.MaterialDialogs;
using Android.Content.Res;
using Android.OS;
using Android.Util;
using Android.Views;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.Base;
using DialogFragment = AndroidX.Fragment.App.DialogFragment;
using Android.Runtime;
using MyStudyLife.UI.ViewModels;
using AndroidX.AppCompat.Widget;
using Android.App;
using Android.Widget;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    // Android can't reinstatiate generic C# classes
    public abstract class EntityListFilterDialogFragment<T> : DialogFragment, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener, SubjectPickerDialogFragment.IOnSubjectSetListener
        where T : SubjectDependentEntity, new() {

        private View _dialogView;
        private BaseEntitiesViewModel<T> _viewModel;

        public BaseEntitiesViewModel<T> ViewModel {
            get { return _viewModel; }
            set {
                _viewModel = value;
                Invalidate();
            }
        }

        protected EntityListFilterDialogFragment() { }
        protected EntityListFilterDialogFragment(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        
        public override Dialog OnCreateDialog(Bundle savedInstanceState) {
            var dialog = new MaterialDialog.Builder(this.Activity)
                .Title(Resource.String.Common_Filter)
                .CustomView(Resource.Layout.Dialog_ListFilter, false)
                .Build();

            _dialogView = dialog.View;

            var schedulePickerView = _dialogView.FindViewById<AppCompatButton>(Resource.Id.SchedulePicker);
            var subjectPickerView = _dialogView.FindViewById<AppCompatButton>(Resource.Id.SubjectPicker);

            schedulePickerView.Click += SchedulePickerOnClick;
            subjectPickerView.Click += SubjectPickerOnClick;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop) {
                // For some reason this doesn't happen on Lollipop - support lib bug?

                var tv = new TypedValue();

                Activity.Theme.ResolveAttribute(Android.Resource.Attribute.ColorControlNormal, tv, true);
                var colorControlNormal = tv.Data;

                Activity.Theme.ResolveAttribute(Android.Resource.Attribute.ColorControlActivated, tv, true);
                var colorControlActivated = tv.Data;

                var tintList = new ColorStateList(
                    new [] {
                        new []{ Android.Resource.Attribute.StatePressed },
                        new int[0]
                    },
                    new [] {
                        colorControlActivated,
                        colorControlNormal
                    }
                );

                schedulePickerView.SupportBackgroundTintList = tintList;
                subjectPickerView.SupportBackgroundTintList = tintList;
            }

            this.Invalidate();

            return dialog;
        }

        private void SchedulePickerOnClick(object sender, EventArgs e) {
            new AcademicSchedulePickerDialogFragment(this.Activity, this, this.ViewModel.AcademicYears, this.ViewModel.SelectedSchedule, this.ViewModel.ScheduleSelectionHelpText) {
                TitleResId = Resource.String.FilterByYearOrTerm,
                ShowNew = false
            }.Show(this.Activity.SupportFragmentManager, "schedule-picker"); 
        }

        private void SubjectPickerOnClick(object sender, EventArgs e) {
            new SubjectPickerDialogFragment(this.Activity, this, this.ViewModel.Subjects, this.ViewModel.SelectedSubject) {
                TitleResId = Resource.String.FilterBySubject,
                ShowNew = false,
                ShowNone = true
            }.Show(this.Activity.SupportFragmentManager, "subject-picker"); 
        }

        private void Invalidate() {
            if (_dialogView == null) {
                return;
            }

            var schedulePicker = _dialogView.FindViewById<TextView>(Resource.Id.SchedulePicker);
            var subjectPicker = _dialogView.FindViewById<TextView>(Resource.Id.SubjectPicker);

            var vm = this.ViewModel;

            schedulePicker.Text = vm?.SelectedSchedule != null
                ? vm.SelectedScheduleText
                : Resources.GetString(Resource.String.None);
            subjectPicker.Text = vm?.SelectedSubject?.Name != null
                ? vm.SelectedSubject.Name
                : Resources.GetString(Resource.String.None);
        }

        public void OnScheduleSet(IAcademicSchedule schedule) {
            var vm = this.ViewModel;

            if (vm != null) {
                vm.SelectedSchedule = schedule;
                this.Invalidate();
            }
        }

        public void OnSubjectSet(Subject subject) {
            var vm = this.ViewModel;

            if (vm != null) {
                vm.SelectedSubject = subject;
                this.Invalidate();
            }
        }
    }

    [Register("mystudylife.droid.fragments.dialogs.TaskListFilterDialogFragment")]
    public class TaskListFilterDialogFragment : EntityListFilterDialogFragment<Task> {
        public TaskListFilterDialogFragment() { }
        protected TaskListFilterDialogFragment(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public static TaskListFilterDialogFragment NewInstance(TasksViewModel viewModel) => new TaskListFilterDialogFragment {
            ViewModel = viewModel
        };
    }

    [Register("mystudylife.droid.fragments.dialogs.ExamList")]
    public class ExamListFilterDialogFragment : EntityListFilterDialogFragment<Exam> {
        public ExamListFilterDialogFragment() { }
        protected ExamListFilterDialogFragment(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public static ExamListFilterDialogFragment NewInstance(ExamsViewModel viewModel) => new ExamListFilterDialogFragment {
            ViewModel = viewModel
        };
    }
}