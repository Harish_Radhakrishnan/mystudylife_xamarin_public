using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.Fragment.App;
using MvvmCross;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Extensions;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Utility;
using DialogFragment = AndroidX.Fragment.App.DialogFragment;
using Object = Java.Lang.Object;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    public class AcademicSchedulePickerDialogFragment : DialogFragment, ExpandableListView.IOnGroupClickListener, ExpandableListView.IOnChildClickListener {
        #region IOnScheduleSetListener

        public interface IOnScheduleSetListener {
            void OnScheduleSet(IAcademicSchedule schedule);
        }

        #endregion

        private readonly Context _context;
        private IOnScheduleSetListener _listener;
        private readonly ObservableCollection<AcademicYear> _years;

        [UsedImplicitly] private readonly IDisposable _yearsSubscription;

        private readonly string _helpText;
        private ExpandableListView _acScheduleSelector;

        public IAcademicSchedule SelectedSchedule { get; private set; }

        /// <value>
        ///     True to show the new button in the dialog.
        /// </value>
        public bool ShowNew { get; set; }

        /// <value>
        ///     True to allow the user to select a null schedule.
        /// </value>
        public bool AllowNone { get; set; }

        private int _titleResId = Resource.String.AcademicSchedule_SelectSchedule;

        public int TitleResId {
            set {
                _titleResId = value;

                this.SafeGet(x => x.Dialog)?.SetTitle(value);
            }
        }

        public AcademicSchedulePickerDialogFragment() {
            // Empty constructor required for dialog fragment
        }

        public AcademicSchedulePickerDialogFragment(Context context, IOnScheduleSetListener listener, ObservableCollection<AcademicYear> years, IAcademicSchedule selectedSchedule, string helpText) {
            _context = context;
            _listener = listener;
            _helpText = helpText;

            if (years != null) {
                _years = years;

                _yearsSubscription = _years.WeakSubscribe(OnYearsCollectionChanged);
            }
            else {
                _years = new ObservableCollection<AcademicYear>();
            }

            this.SelectedSchedule = selectedSchedule;
            this.ShowNew = true;
            this.AllowNone = true;

            // Prevents recreation on rotate
            this.RetainInstance = true;
        }

        public void SetOnScheduleSetListener(IOnScheduleSetListener listener) {
            this._listener = listener;
        }

        public override Dialog OnCreateDialog(Bundle savedInstanceState) {
            var li = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);

            var view = li.Inflate(Resource.Layout.Dialog_AcademicSchedulePicker, null);

            view.FindViewById<TextView>(Resource.Id.AcSchedulePickerHelpText).Text = _helpText;
            _acScheduleSelector = view.FindViewById<ExpandableListView>(Resource.Id.AcSchedulePickerListView);

            _acScheduleSelector.SetAdapter(new AcademicYearAdapter(this, _acScheduleSelector, _years, this.SelectedSchedule));
            _acScheduleSelector.SetOnGroupClickListener(this);
            _acScheduleSelector.SetOnChildClickListener(this);
            _acScheduleSelector.ChoiceMode = ChoiceMode.Single;

            var builder = new MaterialDialog.Builder(_context)
                .Title(_titleResId)
                .CustomView(view, false);

            var hasYears = this._years.Any();

            if (!hasYears || !this.AllowNone) {
                var setNoneView = view.FindViewById(Resource.Id.AcSchedulePickerSetNone);

                setNoneView.Selected = this.SelectedSchedule == null;
                setNoneView.Visibility = ViewStates.Gone;
            }

            if (!this._years.Any()) {
                view.FindViewById<View>(Resource.Id.LonelyView).Visibility = ViewStates.Visible;
                view.FindViewById<View>(Resource.Id.AcSchedulePickerHelpText).Visibility = ViewStates.Gone;
            }
            else {
                view.FindViewById<TextView>(Resource.Id.AcSchedulePickerSetNone).Click += SetNoneOnClick;
                view.FindViewById<View>(Resource.Id.LonelyView).Visibility = ViewStates.Gone;

                if (this.SelectedSchedule != null && !this.SelectedSchedule.Guid.IsEmpty()) {
                    int checkedPos;

                    var term = this.SelectedSchedule as AcademicTerm;

                    if (term != null) {
                        // ReSharper disable once PossibleInvalidOperationException
                        var yearPos = _years.FirstIndexOf(x => x.Guid == term.YearGuid);

                        if (yearPos >= 0) {
                            _acScheduleSelector.ExpandGroup(yearPos, false);
                        }

                        checkedPos = _acScheduleSelector.GetFlatListPosition(
                            ExpandableListView.GetPackedPositionForChild(
                                yearPos,
                                term.Year.Terms.FirstIndexOf(x => x.Guid == term.Guid)
                            )
                        );
                    }
                    else {
                        checkedPos = _acScheduleSelector.GetFlatListPosition(ExpandableListView.GetPackedPositionForGroup(_years.FirstIndexOf(x => x.Guid == this.SelectedSchedule.Guid)));
                    }

                    if (checkedPos > -1) {
                        _acScheduleSelector.SetItemChecked(checkedPos, true);
                    }
                }

                builder.NeutralText(Resource.String.AcademicSchedule_SetCurrent);
            }

            if (this.ShowNew && Mvx.IoCProvider.Resolve<IFeatureService>().CanUseFeatureAsync(Feature.WriteAcademicYears).GetResult()) {
                builder.PositiveText(Resource.String.Common_New);

                view.FindViewById<LonelyView>(Resource.Id.LonelyView).SetText(Resource.String.AcademicSchedule_PickerLonelyWithNew);
            }

            builder.Callback(new DialogHelper.ButtonCallbackShim(
                onNeutral: async () => {
                    this._listener.OnScheduleSet(await Mvx.IoCProvider.Resolve<IAcademicYearRepository>().GetCurrentScheduleAsync());

                    this.Dismiss();
                },
                onPositive: async () => {
                    if (ShowNew) {
                        await Mvx.IoCProvider.Resolve<INavigationService>().Navigate<AcademicYearInputViewModel>();
                    }
                }
            ));

            return builder.Build();
        }

        private void SetNoneOnClick(object sender, EventArgs e) {
            this._listener.OnScheduleSet(null);

            this.Dismiss();
        }

        public bool OnGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long id) {
            this._listener.OnScheduleSet(this._years[groupPosition]);

            this.Dismiss();

            return true; // true prevents expanding
        }

        public bool OnChildClick(ExpandableListView parent, View clickedView, int groupPosition, int childPosition, long id) {
            this._listener.OnScheduleSet(this._years[groupPosition].Terms[childPosition]);

            this.Dismiss();

            return true;
        }

        private void OnYearsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            var dialog = this.SafeGet(x => x.Dialog);

            if (dialog != null) {
                ((AcademicYearAdapter)dialog.FindViewById<ExpandableListView>(Resource.Id.AcSchedulePickerListView).ExpandableListAdapter).NotifyDataSetChanged();

                dialog.FindViewById<View>(Resource.Id.AcSchedulePickerHelpText).Visibility = this._years.Any()
                    ? ViewStates.Visible
                    : ViewStates.Gone;

                dialog.FindViewById<View>(Resource.Id.LonelyView).Visibility = this._years.Any()
                    ? ViewStates.Gone
                    : ViewStates.Visible;
            }
        }

        class AcademicYearAdapter : BaseExpandableListAdapter {
            private readonly AcademicSchedulePickerDialogFragment _dialog;
            private readonly ExpandableListView _listView;
            private readonly LayoutInflater _layoutInflater;
            private readonly IList<AcademicYear> _years;
            private readonly IAcademicSchedule _selectedSchedule;

            private readonly string _termsFormat;

            public override int GroupCount => this._years.Count;

            public override bool HasStableIds => true;

            public AcademicYearAdapter(AcademicSchedulePickerDialogFragment dialog, ExpandableListView listView, IList<AcademicYear> years, IAcademicSchedule selectedSchedule) {
                this._dialog = dialog;
                this._listView = listView;
                this._layoutInflater = (LayoutInflater)dialog._context.GetSystemService(Context.LayoutInflaterService);
                this._selectedSchedule = selectedSchedule;
                this._years = years;

                this._termsFormat = "{0:" + dialog._context.GetString(Resource.String.Common_Term) + ";" + dialog._context.GetString(Resource.String.Common_Terms) + "}";
            }

            public override Object GetGroup(int groupPosition) {
                return null;
            }

            public override long GetGroupId(int groupPosition) {
                return groupPosition;
            }

            public override Object GetChild(int groupPosition, int childPosition) {
                return null; // this._years[groupPosition].Terms[childPosition];
            }

            public override long GetChildId(int groupPosition, int childPosition) {
                return childPosition;
            }

            public override int GetChildrenCount(int groupPosition) {
                return this._years[groupPosition].Terms.Count;
            }

            public override bool IsChildSelectable(int groupPosition, int childPosition) {
                return true;
            }

            public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent) {

                // Ignore convertView as this list is never likely to be large, plus it is difficult
                // to ensure that termsView only has one click event handler (with the correct position)
                var view = _layoutInflater.Inflate(Resource.Layout.ListItem_AcademicYearPicker, null);

                var year = this._years[groupPosition];
                var isSelected = this._selectedSchedule != null && this._selectedSchedule.Guid == year.Guid;

                // For some reason the click event doesn't propagate...
                view.FindViewById(Resource.Id.AcademicYearPickerItem).Click += (s, e) => {
                    _dialog.OnGroupClick(_listView, view, groupPosition, GetGroupId(groupPosition));
                };

                var titleView = view.FindViewById<TextView>(Resource.Id.AcademicYearPickerItem_Title);
                var datesView = view.FindViewById<TextView>(Resource.Id.AcademicYearPickerItem_Dates);

                titleView.Text = year.ToString();
                datesView.Text = year.Dates;

                titleView.Selected = isSelected;
                datesView.Selected = isSelected;

                var termsView = view.FindViewById<TextView>(Resource.Id.AcademicYearPickerItem_Terms);

                termsView.Text = String.Format(new PluralFormatProvider(true), _termsFormat, year.Terms.Count);
                termsView.Click += (s, e) => {
                    if (_listView.IsGroupExpanded(groupPosition)) {
                        _listView.CollapseGroup(groupPosition);
                    }
                    else {
                        _listView.ExpandGroup(groupPosition, true);
                    }
                };

                return view;
            }

            public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent) {
                var view = convertView ?? _layoutInflater.Inflate(Resource.Layout.ListItem_AcademicTermPicker, null);

                var term = this._years[groupPosition].Terms[childPosition];
                var isSelected = this._selectedSchedule != null && this._selectedSchedule.Guid == term.Guid;

                var titleView = view.FindViewById<TextView>(Resource.Id.AcademicTermPickerItem_Title);
                var datesView = view.FindViewById<TextView>(Resource.Id.AcademicTermPickerItem_Dates);

                titleView.Text = term.Name;
                datesView.Text = term.Dates;

                titleView.Selected = isSelected;
                datesView.Selected = isSelected;

                return view;
            }
        }
    }
}