using System;
using System.Collections.Generic;
using System.Linq;
using AFollestad.MaterialDialogs;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using AndroidX.Fragment.App;
using MvvmCross.Plugin.Color.Platforms.Android;
using MyStudyLife.Data.UI;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Fragments.Dialogs {
    
    public sealed class ColorPickerDialogFragment : DialogFragment {
        #region IOnColorSetListener

        // In keeping with android style...

        public interface IOnColorSetListener {
            void OnColorSet(UserColor color);
        }

        #endregion
        
        private readonly IOnColorSetListener _listener;
        private readonly IList<UserColor> _colors;

        public UserColor SelectedColor { get; private set; }

        public bool ShowInheritFromSubject { get; set; }

        // REQUIRED, PUBLIC, DO NOT REMOVE
        public ColorPickerDialogFragment() {
            // Dismiss dialog on restore (lazy) as it won't reattach to the same listener
            this.DismissAllowingStateLoss();
        }
        
        public ColorPickerDialogFragment(IOnColorSetListener listener, IEnumerable<UserColor> colors, UserColor color) {
			_listener = listener;

            if (colors == null) {
                this._colors = new List<UserColor>();
            }
            else {
                var list = colors as IList<UserColor>;

                this._colors = list ?? colors.ToList();
            }

            this.SelectedColor = color;

            // Prevents recreation on rotate
            this.RetainInstance = true;
        }
        
        public override Android.App.Dialog OnCreateDialog(Bundle savedInstanceState) {
            var view = Activity.LayoutInflater.Inflate(Resource.Layout.Dialog_ColorPicker, null, false);
            var colorsView = view.FindViewById<ColorsLayout>(Resource.Id.ColorsLayout);

            var colorsCount = this._colors.Count;

            for (int i = 0; i < colorsCount; i++) {
                var color = this._colors[i];
                var colorView = Activity.LayoutInflater.Inflate(Resource.Layout.GridItem_Color, colorsView, false);

                colorView.FindViewById(Resource.Id.ColorIndicator).Background.Mutate().SetColorFilter(color.Color.ToNativeColor(), PorterDuff.Mode.Multiply);
                colorView.FindViewById(Resource.Id.SelectedIndicator).SetVisible(
                    this.SelectedColor != null && color.Identifier == this.SelectedColor.Identifier,
                    ViewStates.Invisible
                );

                colorView.Click += ColorViewOnClick;

                colorView.Tag = new Java.Lang.String(color.Identifier);

                colorsView.AddView(colorView);
            }
            
            var builder = new MaterialDialog.Builder(Activity)
                .Title(Resource.String.Color)
                .CustomView(view, false);

            if (ShowInheritFromSubject) {
                builder.NeutralText(Resource.String.UseSubjectColor);
                builder.Callback(new DialogHelper.ButtonCallbackShim(
                    null,
                    onNeutral: () => {
                        this._listener.OnColorSet(null);

                        this.Dismiss();
                    }
                ));
            }

            return builder.Build();
        }

        private void ColorViewOnClick(object sender, EventArgs e) {
            var cssClassName = ((View) sender).Tag.ToString();
            var color = this._colors.Single(x => x.Identifier == cssClassName);

            this._listener.OnColorSet(color);

            this.Dismiss();
        }
    }

    [Register("mystudylife.droid.fragments.dialogs.ColorsLayout")]
    class ColorsLayout : ViewGroup {
        private readonly int _minGap, _childSize;

        protected ColorsLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public ColorsLayout(Context context) : this(context, null) { }
        public ColorsLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public ColorsLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            _minGap = context.Resources.DisplayMetrics.DpsToPixels(8);
            _childSize = context.Resources.DisplayMetrics.DpsToPixels(48);
        }

        private int _columnCount, _rowCount, _gapCount, _gapSize;

        private void UpdateLayoutMeasurements(int width) {
            var childCount = this.ChildCount;

            _columnCount = (int)Math.Floor(width / ((double)_childSize + _minGap));
            _gapCount = _columnCount - 1;
            _gapSize = (int)Math.Round((width - (_childSize * _columnCount)) / (double)_gapCount);
            _rowCount = (int)Math.Ceiling(childCount / (double) _columnCount);
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            var width = MeasureSpec.GetSize(widthMeasureSpec);

            UpdateLayoutMeasurements(width);

            SetMeasuredDimension(
                width,
                (_rowCount * _childSize) + ((_rowCount - 1) * _gapSize)
            );

            var childMeasureSpec = MeasureSpec.MakeMeasureSpec(_childSize, MeasureSpecMode.Exactly);

            for (int i = 0; i < this.ChildCount; i++) {
                this.GetChildAt(i).Measure(childMeasureSpec, childMeasureSpec);
            }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b) {
            UpdateLayoutMeasurements((r - l));

            var childCount = this.ChildCount;
            var lastColumnIndex = _columnCount - 1;

            int parentLeft = 0;
            int parentRight = r - l;
            int parentTop = 0;
            int parentBottom = b - t;

            int childTop = parentTop;

            for (int i = 0; i < childCount; i++) {
                var child = this.GetChildAt(i);
                var column = i % _columnCount;

                int childLeft, childRight;

                if (column == 0) {
                    childLeft = parentLeft;
                    childRight = parentLeft + _childSize;
                }
                else if (column == lastColumnIndex) {
                    childRight = parentRight;
                    childLeft = parentRight - _childSize;
                }
                else {
                    childLeft = parentLeft + (column * (_childSize + _gapSize));
                    childRight = childLeft + _childSize;
                }

                child.Layout(childLeft, childTop, childRight, childTop + _childSize);

                if (column == lastColumnIndex) {
                    childTop += _childSize + _gapSize;
                }
            }
        }
    }
}