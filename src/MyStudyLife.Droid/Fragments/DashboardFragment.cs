using System;
using System.ComponentModel;
using Android.Content;
using Android.OS;
using Android.Runtime;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Scheduling;
using MyStudyLife.UI.ViewModels;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using MyStudyLife.Droid.Common;
using Android.Widget;
using MikePhil.Charting.Charts;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.UI.Data;
using System.Linq;
using MyStudyLife.Data.UI;
using Android.Graphics.Drawables;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Data;
using MyStudyLife.UI.Models;
using Android.Util;
using Android.App;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.WeakSubscription;
using MyStudyLife.UI;
using AndroidX.Core.Content;
using AndroidX.CardView.Widget;
using AndroidX.RecyclerView.Widget;
using AndroidX.ViewPager.Widget;
using AndroidX.AppCompat.Widget;
using Android.Views;
using Google.Android.Material.Tabs;
using Google.Android.Material.FloatingActionButton;
using MyStudyLife.Droid.Common.TimeHop.StickyHeadersRecyclerView;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.DashboardFragment")]
    public class DashboardFragment : BaseFragment<DashboardViewModel> {
        private MvxFixedPagerAdapter _pagerAdapter;
        /// <summary>
        ///     The padding that the Pie chart library adds internally
        ///     that we can't change :-(
        /// </summary>
        private const int PIE_CHART_PADDING = 18;

        public override int LayoutResourceId => Resource.Layout.View_Dashboard;

        // Should prevent minutesremaining binding issue
        public override void OnResume() {
            base.OnResume();
            ViewModel?.StartUIUpdateTimer();
        }

        public override void OnPause() {
            base.OnPause();
            ViewModel?.StopUIUpdateTimer();
        }

        protected override void OnSetActionBar(ActionBar ab) => ab.SetTitle(Resource.String.Views_Dashboard);

        protected override void ViewModelOnLoaded() {
            base.ViewModelOnLoaded();
            SetExamsPageState();
        }

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener.Listen(nameof(ViewModel.ShowExamsTab), SetExamsPageState);

            SetExamsPageState();
        }

        private void SetExamsPageState() {
            if (_pagerAdapter == null) {
                return;
            }

            var vm = ViewModel;
            bool visible = vm == null || vm.ShowExamsTab;

            var examsPage = _pagerAdapter.Pages.OfType<ExamsPage>().FirstOrDefault();

            if (visible && examsPage == null) {
                _pagerAdapter.AddPage(new ExamsPage(this));
            }
            // Check for loaded prevents it being removed prematurely
            else if (!visible && examsPage != null) {
                _pagerAdapter.RemovePage(examsPage);
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            var viewPager = view.FindViewById<ViewPager>(Resource.Id.Pager);
            var tabLayout = view.FindViewById<TabLayout>(Resource.Id.Tabs);
            _pagerAdapter = new MvxFixedPagerAdapter(
                Context,
                (IMvxAndroidBindingContext)BindingContext,
                // Add all pages since this is default behaviour
                new TodayPage(this), new TasksPage(this), new ExamsPage(this)
            );
            // Allows all 3 tabs to be instantiated at once, and stops the TodayPage
            // being rebound (causing dials to animate) after showing the ExamsPage.
            // Does sort of solve #MSL-111, so if that ever needs reproducing again,
            // navigate to another screen from the hamburger menu and open a modal,
            // then navigate back.
            viewPager.OffscreenPageLimit = 2;
            viewPager.Adapter = _pagerAdapter;

            tabLayout.SetupWithViewPager(viewPager, _pagerAdapter);

            var fab = view.FindViewById<FloatingActionButton>(Resource.Id.NewTaskFab);

            fab.SetImageDrawable(AppCompatDrawableManager.Get().GetDrawable(Context, Resource.Drawable.task_icon));
            fab.SetScaleType(ImageView.ScaleType.Center);

            SetExamsPageState();

            return view;
        }

        private static void ApplyChartStyles(PieChart chart) {
            chart.HoleRadius = 90f;
            chart.TransparentCircleRadius = 0f;
            chart.SetHoleColor(0x00);
            chart.SetTouchEnabled(false);
            chart.Legend.Enabled = false;
            chart.SetCenterTextColor(ContextCompat.GetColor(chart.Context, Resource.Color.foreground));

            // These two are required to remove default text,
            // NoDataText shows up before the data has loaded
            // in to the dials and Descriptions is part of the
            // legend.
            chart.SetNoDataText(null);
            chart.Description = null;
        }

        private abstract class DashboardPageBase : MvxFixedPagerAdapter.Page {
            private readonly Glyph _tabIcon;

            protected DashboardFragment ParentFragment { get; }

            public DashboardPageBase(DashboardFragment parentFragment, Glyph tabIcon, int titleResId, int layoutResId)
                : base(parentFragment.Context, titleResId, layoutResId) {
                ParentFragment = parentFragment;
                _tabIcon = tabIcon;
            }

            public override TabLayout.Tab InstantiateTab(TabLayout tabLayout, IMvxAndroidBindingContext bindingContext) {
                var tab = base.InstantiateTab(tabLayout, bindingContext);

                var tabView = bindingContext.BindingInflate(Resource.Layout.Component_DashboardTab, null);
                tabView.FindViewById<Widgets.Icon>(Resource.Id.Icon).Glyph = _tabIcon;
                var chart = tabView.FindViewById<PieChart>(Resource.Id.TabDial);

                int size = DisplayHelper.DpsToPixels(30) + (PIE_CHART_PADDING * 2);
                var layoutParameters = (FrameLayout.LayoutParams)chart.LayoutParameters;
                layoutParameters.Height = size;
                layoutParameters.Width = size;
                // have to set the margins in code, else the dial is off-center in the parent
                layoutParameters.SetMargins(-PIE_CHART_PADDING, -PIE_CHART_PADDING, -PIE_CHART_PADDING, -PIE_CHART_PADDING);

                ApplyChartStyles(chart);

                tab.SetCustomView(tabView);

                return tab;
            }
        }

        private class TodayPage : DashboardPageBase {
            /// <summary>
            ///     The maxium width of the next day dials in dips.
            /// </summary>
            private const float NEXT_DAY_DIAL_MAX_WIDTH = 90;

            /// <summary>
            ///     The spacing between the next day dials in dips.
            /// </summary>
            private const float NEXT_DAY_DIAL_SPACING = 10;

            private ScrollView _scrollView;

            public TodayPage(DashboardFragment parentFragment)
                : base(parentFragment, Glyph.Today, Resource.String.Today, Resource.Layout.View_Dashboard_Today) { }

            public override View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                var v = base.InstantiateView(container, bindingContext);

                _scrollView = v.FindViewById<ScrollView>(Resource.Id.RootScroll);

                ParentFragment?.ViewModelListener?
                    .Listen(nameof(ParentFragment.ViewModel.ShowTodayLonely), SetShouldFillViewport);

                SetShouldFillViewport();

                var agendaList = v.FindViewById<StaticListView>(Resource.Id.AgendaList);
                agendaList.OnItemClick += AgendaListOnItemClick;

                ApplyChartStyles(v.FindViewById<PieChart>(Resource.Id.NextDayClasses));
                ApplyChartStyles(v.FindViewById<PieChart>(Resource.Id.NextDayTasks));
                ApplyChartStyles(v.FindViewById<PieChart>(Resource.Id.NextDayExams));

                var nextDayDialsView = v.FindViewById<LinearLayout>(Resource.Id.NextDayDials);
                var metrics = DisplayHelper.GetDisplayMetrics();

                var parentWidth = DisplayHelper.PixelsToDps(metrics.WidthPixels) - (16 * 2); // 16dp padding
                var calculatedDialWidth = (parentWidth - (NEXT_DAY_DIAL_SPACING * 2)) / 3f;
                var dialWidth = Math.Min(NEXT_DAY_DIAL_MAX_WIDTH, calculatedDialWidth);

                var dialWidthPx = DisplayHelper.DpsToPixels((float)dialWidth);

                v.FindViewById(Resource.Id.NextDayClassesLayout).LayoutParameters.Width = dialWidthPx;
                v.FindViewById(Resource.Id.NextDayTasksLayout).LayoutParameters.Width = dialWidthPx;
                v.FindViewById(Resource.Id.NextDayExamsLayout).LayoutParameters.Width = dialWidthPx;

                nextDayDialsView.LayoutParameters.Height = dialWidthPx;

                // Gap between the right edge of the last dial and the edge of the screen
                var nextDayDialRightSpace = parentWidth - (dialWidth * 3) + (NEXT_DAY_DIAL_SPACING * 2);
                var FABWidth = 56 + (16 * 2); // 56dp FAB width, 16dp padding
                if (nextDayDialRightSpace < FABWidth) {
                    ((LinearLayout.LayoutParams)nextDayDialsView.LayoutParameters).BottomMargin = DisplayHelper.DpsToPixels(FABWidth / 2f);
                }

                return v;
            }

            private void SetShouldFillViewport() {
                // Fill the viewport (thereby displaying the next day content at the bottom)
                // if we have no entries for the current day. Displaying this when we do have
                // entries creates a confusing experience for the user.
                _scrollView.FillViewport = (ParentFragment.ViewModel?.ShowTodayLonely ?? true);
            }

            private void AgendaListOnItemClick(object sender, StaticListView.ItemClickEventArgs e) {
                var viewHolder = (StaticListView)sender;

                Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntry((AgendaEntry)e.DataContext, e.View);
            }

            public override TabLayout.Tab InstantiateTab(TabLayout tabLayout, IMvxAndroidBindingContext bindingContext) {
                var tab = base.InstantiateTab(tabLayout, bindingContext);

                var set = ParentFragment.CreateBindingSet<DashboardFragment, DashboardViewModel>();
                var dial = tab.CustomView.FindViewById<PieChart>(Resource.Id.TabDial);

                set.Bind(dial)
                    .For("DialData")
                    .To(x => x.TodayDial);
                set.Bind(dial)
                    .For(d => d.CenterText)
                    .To(x => x.TodayDial.Sum)
                    .WithFallback("0");

                set.Apply();

                return tab;
            }
        }
        private class TasksPage : DashboardPageBase {
            public TasksPage(DashboardFragment parentFragment)
                : base(parentFragment, Glyph.Task, Resource.String.Today, Resource.Layout.View_Dashboard_Tasks) { }

            public override View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                var v = base.InstantiateView(container, bindingContext);

                var recycler = v.FindViewById<MvxRecyclerView>(Resource.Id.Recycler);
                recycler.SetLayoutManager(new FABLinearLayoutManager(Context));

                var adapter = new TasksRecyclerAdapter(bindingContext);
                adapter.OnItemClick += Adapter_OnItemClick;

                var headersDecor = new StickyRecyclerHeadersDecoration(adapter);
                recycler.AddItemDecoration(headersDecor);

                // #MSL-111 Use .Post to fix issue where headers not drawn correctly when ItemsSource is already populated on adapter set
                adapter.DataSetChanged += (s, e) => recycler.Post(() => {
                    headersDecor.InvalidateHeaders();
                    recycler.InvalidateItemDecorations();
                });

                recycler.Adapter = adapter;

                recycler.AddItemDecoration(new SpacerDecoration(Context));

                return v;
            }

            private void Adapter_OnItemClick(object sender, EventArgs e) {
                var viewHolder = (MvxRecyclerViewHolder)sender;

                Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntity((SubjectDependentEntity)viewHolder.DataContext, viewHolder.ItemView);
            }

            public override TabLayout.Tab InstantiateTab(TabLayout tabLayout, IMvxAndroidBindingContext bindingContext) {
                var tab = base.InstantiateTab(tabLayout, bindingContext);

                var set = ParentFragment.CreateBindingSet<DashboardFragment, DashboardViewModel>();
                var dial = tab.CustomView.FindViewById<PieChart>(Resource.Id.TabDial);

                set.Bind(dial)
                    .For("DialData")
                    .To(x => x.TasksDial);
                set.Bind(dial)
                    .For(d => d.CenterText)
                    .To(x => x.TasksDial.Sum)
                    .WithFallback("0");

                set.Apply();

                return tab;
            }

            class TasksRecyclerAdapter : GroupedTasksRecyclerAdapter {
                public TasksRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

                protected override TaskGrouping GetGrouping(int position) {
                    return ((DashboardViewModel)BindingContext.DataContext).GetTaskGrouping(position);
                }

                public override RecyclerView.ViewHolder OnCreateHeaderViewHolder(ViewGroup parent) {
                    var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Component_DashboardHeader, parent, false);

                    return new HeaderViewHolder(view);
                }

                public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                    var viewHolder = base.OnCreateViewHolder(parent, viewType);
                    var view = (CardView)viewHolder.ItemView;
                    var layoutParams = (RecyclerView.LayoutParams)view.LayoutParameters;

                    // Remove any added padding for the shadow so we align with the
                    // screen edge and can control spacing ourselves
                    layoutParams.LeftMargin = -view.PaddingLeft;
                    layoutParams.TopMargin = -view.PaddingTop;
                    layoutParams.RightMargin = -view.PaddingRight;
                    layoutParams.BottomMargin = -view.PaddingBottom;

                    return viewHolder;
                }
            }
        }
        private class ExamsPage : DashboardPageBase {
            public ExamsPage(DashboardFragment parentFragment)
                : base(parentFragment, Glyph.Exam, Resource.String.Exams, Resource.Layout.View_Dashboard_Exams) { }

            public override View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                var v = base.InstantiateView(container, bindingContext);

                var recycler = v.FindViewById<MvxRecyclerView>(Resource.Id.Recycler);
                recycler.SetLayoutManager(new FABLinearLayoutManager(Context));

                var adapter = new ExamsRecyclerAdapter(bindingContext);
                adapter.OnItemClick += Adapter_OnItemClick;

                var headersDecor = new StickyRecyclerHeadersDecoration(adapter);
                recycler.AddItemDecoration(headersDecor);

                // #MSL-111 Use .Post to fix issue where headers not drawn correctly when ItemsSource is already populated on adapter set
                adapter.DataSetChanged += (s, e) => recycler.Post(() => {
                    headersDecor.InvalidateHeaders();
                    recycler.InvalidateItemDecorations();
                });

                recycler.Adapter = adapter;

                recycler.AddItemDecoration(new SpacerDecoration(Context));

                return v;
            }

            private void Adapter_OnItemClick(object sender, EventArgs e) {
                var viewHolder = (MvxRecyclerViewHolder)sender;

                Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntity((SubjectDependentEntity)((ExamModel)viewHolder.DataContext).Base, viewHolder.ItemView);
            }

            public override TabLayout.Tab InstantiateTab(TabLayout tabLayout, IMvxAndroidBindingContext bindingContext) {
                var tab = base.InstantiateTab(tabLayout, bindingContext);

                var set = ParentFragment.CreateBindingSet<DashboardFragment, DashboardViewModel>();
                var dial = tab.CustomView.FindViewById<PieChart>(Resource.Id.TabDial);

                set.Bind(dial)
                    .For("DialData")
                    .To(x => x.ExamsDial);
                set.Bind(dial)
                    .For(d => d.CenterText)
                    .To(x => x.ExamsDial.Sum)
                    .WithFallback("0");

                set.Apply();

                return tab;
            }

            class ExamsRecyclerAdapter : GroupedExamsRecyclerAdapter {
                public ExamsRecyclerAdapter(IMvxAndroidBindingContext bindingContext) : base(bindingContext) { }

                protected override ExamGrouping GetGrouping(int position) {
                    return ((DashboardViewModel)BindingContext.DataContext).GetExamGrouping(position);
                }

                public override RecyclerView.ViewHolder OnCreateHeaderViewHolder(ViewGroup parent) {
                    var view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.Component_DashboardHeader, parent, false);

                    return new HeaderViewHolder(view);
                }

                public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType) {
                    var viewHolder = base.OnCreateViewHolder(parent, viewType);
                    var view = (CardView)viewHolder.ItemView;
                    var layoutParams = (RecyclerView.LayoutParams)view.LayoutParameters;

                    // Remove any added padding for the shadow so we align with the
                    // screen edge and can control spacing ourselves
                    layoutParams.LeftMargin = -view.PaddingLeft;
                    layoutParams.TopMargin = -view.PaddingTop;
                    layoutParams.RightMargin = -view.PaddingRight;
                    layoutParams.BottomMargin = -view.PaddingBottom;

                    return viewHolder;
                }
            }
        }
    }

    [Register("mystudylife.droid.fragments.AgendaListView")]
    public class AgendaListView : StaticListView {
        protected AgendaListView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public AgendaListView(Context context, IAttributeSet attrs) : this(context, attrs, new AgendaEntryAdapter(context)) { }
        public AgendaListView(Context context, IAttributeSet attrs, AgendaEntryAdapter adapter) : base(context, attrs, adapter) {
            ChildrenDrawingOrderEnabled = true;
        }

        protected override int GetChildDrawingOrder(int childCount, int i) => childCount - i - 1;

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            var width = MeasureSpec.GetSize(widthMeasureSpec);

            int widthUsed = 0;
            int heightUsed = 0;

            View prevView = null;

            for (int i = 0; i < ChildCount; i++) {
                var currentView = GetChildAt(i);

                if (prevView != null) {
                    var prev = (AgendaEntryListItemView)prevView.Tag;
                    var current = (AgendaEntryListItemView)prevView.Tag;

                    heightUsed += Math.Max(prev.MinimumSpacing, current.MinimumSpacing);
                }

                var layoutParams = (MarginLayoutParams)currentView.LayoutParameters;

                MeasureChildWithMargins(currentView, widthMeasureSpec, widthUsed, heightMeasureSpec, heightUsed);

                heightUsed += currentView.MeasuredHeight + layoutParams.TopMargin + layoutParams.BottomMargin;

                prevView = currentView;
            }

            int height = heightUsed + PaddingTop + PaddingBottom;

            SetMeasuredDimension(width, height);
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b) {
            int currentTop = PaddingTop;

            View prevView = null;

            for (int i = 0; i < ChildCount; i++) {
                var currentView = GetChildAt(i);

                if (prevView != null) {
                    var prev = (AgendaEntryListItemView)prevView.Tag;
                    var current = (AgendaEntryListItemView)currentView.Tag;

                    currentTop += Math.Max(prev.MinimumSpacing, current.MinimumSpacing);
                }

                var layoutParams = (MarginLayoutParams)currentView.LayoutParameters;

                int left = layoutParams.LeftMargin;
                int top = currentTop + layoutParams.TopMargin;
                int right = left + currentView.MeasuredWidth;
                int bottom = top + currentView.MeasuredHeight;

                currentView.Layout(left, top, right, bottom);

                currentTop = bottom + layoutParams.BottomMargin;

                prevView = currentView;
            }
        }
    }

    public class AgendaEntryAdapter : MvxAdapterWithChangedEvent {
        public AgendaEntryAdapter(Context context) : base(context) { }

        public AgendaEntryAdapter(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer) { }

        protected override IMvxListItemView CreateBindableView(object dataContext, ViewGroup parent, int templateId)
            => new AgendaEntryListItemView(Context, BindingContext.LayoutInflaterHolder, (AgendaEntry)dataContext, parent);
    }

    [Register("mystudylife.droid.fragments.AgendaEntryListItemView")]
    public class AgendaEntryListItemView : Java.Lang.Object, IMvxListItemView, IMvxBindingContextOwner, View.IOnAttachStateChangeListener {
        private static int _currentEntryHeight, _futureEntryHeight;
        private static int _pastEntrySpacing, _currentEntrySpacing, _futureEntrySpacing;
        private static int _pastEntryElevation, _currentEntryElevation, _futureEntryElevation;

        static AgendaEntryListItemView() {
            var resources = Application.Context.Resources;

            _currentEntryHeight = resources.GetDimensionPixelSize(Resource.Dimension.ae_current_height);
            _futureEntryHeight = resources.GetDimensionPixelSize(Resource.Dimension.ae_future_height);

            _pastEntrySpacing = resources.GetDimensionPixelSize(Resource.Dimension.ae_past_spacing);
            _currentEntrySpacing = resources.GetDimensionPixelSize(Resource.Dimension.ae_current_spacing);
            _futureEntrySpacing = resources.GetDimensionPixelSize(Resource.Dimension.ae_future_spacing);

            _currentEntryElevation = resources.DisplayMetrics.DpsToPixels(4);
            _futureEntryElevation = resources.DisplayMetrics.DpsToPixels(2);
            _pastEntryElevation = resources.DisplayMetrics.DpsToPixels(1);
        }

        static int GetTemplateIdForState(AgendaEntryState state)
            => state == AgendaEntryState.Past ? Resource.Layout.ListItem_AgendaEntryPast : Resource.Layout.ListItem_AgendaEntry;

        private IDisposable _listener;
        private readonly IMvxAndroidBindingContext _bindingContext;
        private ViewGroup _parent;
        private View _content;
        private object _cachedDataContext;
        private bool _isAttachedToWindow;

        private AgendaEntryState? _currentState;

        private int _minimumSpacing;

        /// <summary>
        ///     The minimum spacing between this and other views.
        /// </summary>
        /// <remarks>
        ///     Should probably be in custom layout params!
        /// </remarks>
        public int MinimumSpacing {
            get => _minimumSpacing;
            set {
                if (!Equals(_minimumSpacing, value)) {
                    _minimumSpacing = value;

                    Content.RequestLayout();
                }
            }
        }

        public AgendaEntryListItemView(Context context, IMvxLayoutInflaterHolder layoutInflater, AgendaEntry dataContext, ViewGroup parent) {
            _bindingContext = new MvxAndroidBindingContext(context, layoutInflater, dataContext);
            _parent = parent;

            ListenForStateChange();
            ApplyStylesForCurrentState();

            _bindingContext.DataContextChanged += BindingContextOnDataContextChanged;
        }

        public void OnViewAttachedToWindow(View attachedView) {
            _isAttachedToWindow = true;

            if (_cachedDataContext != null && DataContext == null)
                DataContext = _cachedDataContext;
        }

        public void OnViewDetachedFromWindow(View detachedView) {
            _cachedDataContext = DataContext;
            DataContext = null;
            _isAttachedToWindow = false;
        }

        public IMvxBindingContext BindingContext {
            get => _bindingContext;
            set => throw new NotImplementedException("BindingContext is readonly in the list item");
        }

        public View Content {
            get => _content;
            set {
                _content = value;
                _content.AddOnAttachStateChangeListener(this);
            }
        }

        public virtual object DataContext {
            get => _bindingContext.DataContext;
            set {
                if (_isAttachedToWindow) {
                    _bindingContext.DataContext = value;
                }
                else {
                    _cachedDataContext = value;
                    _bindingContext.DataContext = null;
                }
            }
        }

        public int TemplateId { get; private set; }

        private void ListenForStateChange() {
            var ae = (AgendaEntry)DataContext;

            _listener?.Dispose();
            _listener = ae?.WeakSubscribe(DataContextOnPropertyChanged);
        }

        private void BindingContextOnDataContextChanged(object sender, EventArgs e) {
            ListenForStateChange();

            if (DataContext != null) {
                ApplyStylesForCurrentState();
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(AgendaEntry.State)) {
                ApplyStylesForCurrentState();
            }
            else if (e.PropertyName.In(nameof(AgendaEntry.TasksOverdue), nameof(AgendaEntry.TasksIncomplete), nameof(AgendaEntry.TasksComplete))) {
                UpdateTasksPill();
            }
        }

        private void ApplyStylesForCurrentState() {
            var entry = (AgendaEntry)DataContext;

            if (entry == null || _parent == null) {
                return;
            }

            if (_currentState.HasValue && _currentState.Value == entry.State) {
                return;
            }

            _currentState = entry.State;

            TemplateId = GetTemplateIdForState(entry.State);

            BindingContext.ClearAllBindings();

            if (Content != null) {
                _parent.RemoveView(Content);
            }

            Content = _bindingContext.BindingInflate(TemplateId, _parent, false);

            var card = Content.FindViewById<CardView>(Resource.Id.Card);
            var cardInner = card.FindViewById(Resource.Id.CardInner);
            var timesView = card.FindViewById(Resource.Id.AgendaEntryTime);

            var layoutParams = (LinearLayout.LayoutParams)Content.LayoutParameters;
            var cardInnerLayoutParams = cardInner.LayoutParameters;

            if (entry.IsCurrent) {
                card.CardElevation = _currentEntryElevation;

                if (cardInnerLayoutParams.Height != _currentEntryHeight) {
                    cardInnerLayoutParams.Height = _currentEntryHeight;
                    cardInner.RequestLayout();
                }
            }
            else if (entry.IsFuture) {
                card.CardElevation = _futureEntryElevation;

                if (cardInnerLayoutParams.Height != _futureEntryHeight) {
                    cardInnerLayoutParams.Height = _futureEntryHeight;
                    cardInner.RequestLayout();
                }
            }
            else {
                // MSL-255 - Although we can set the card elevation in XML (as past entries
                // have a separate template), doing so causes a radius must be > 0 exception
                // to be thrown on API <= 21. Setting from code works for some reason!
                card.CardElevation = _pastEntryElevation;
            }

            // Normalize margin based on the card's elevation, this also allows
            // views to overlap since clipping doesn't seem to play nice with CardView
            if (layoutParams != null &&
                layoutParams.LeftMargin != -card.PaddingLeft) {
                layoutParams.LeftMargin = -card.PaddingLeft;
                layoutParams.TopMargin = -card.PaddingTop;
                layoutParams.RightMargin = -card.PaddingRight;
                layoutParams.BottomMargin = -card.PaddingBottom;
                Content.RequestLayout();
            }

            MinimumSpacing = entry.State switch {
                AgendaEntryState.Past => _pastEntrySpacing,
                AgendaEntryState.Current => _currentEntrySpacing,
                _ => _futureEntrySpacing
            };

            // Fade times when entry is current so timeline looks
            // less obstructive as it moves through the entry
            timesView.Alpha = entry.IsCurrent ? .3f : 1f;

            UpdateTasksPill();
        }

        private void UpdateTasksPill() {
            var agendaEntry = (AgendaEntry)DataContext;
            var tasksView = Content.FindViewById<TextView>(Resource.Id.AgendaEntryTasks);

            if (agendaEntry == null || tasksView == null) {
                return;
            }

            var color = UserColors.Okay;
            if (agendaEntry.TasksOverdue > 0) {
                color = UserColors.Attention;
            }
            else if (agendaEntry.TasksIncomplete > 0) {
                color = UserColors.Warning;
            }

            tasksView.Text = $"{agendaEntry.TasksDue} TASK{(agendaEntry.TasksDue == 1 ? "" : "S")} DUE";
            ((GradientDrawable)tasksView.Background.Mutate()).SetColor(color.ToNativeColor());
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _listener?.Dispose();
                _listener = null;
            }

            base.Dispose(disposing);
        }
    }
}