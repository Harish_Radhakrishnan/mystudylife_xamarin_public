using System;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Data;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.Droid.Common;
using String = System.String;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Droid.Platform;
using AndroidX.ViewPager.Widget;
using Google.Android.Material.Tabs;
using Google.Android.Material.FloatingActionButton;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.ScheduleFragment")]
    public sealed class ScheduleFragment : BaseFragment<ScheduleViewModel>, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener {
        public override int LayoutResourceId => Resource.Layout.View_Schedule;
        public override int? MenuResourceId => Resource.Menu.ScheduleView;

        public override void OnViewModelSet() {
            base.OnViewModelSet();

            this.ViewModelListener
                .Listen(() => this.ViewModel.IsLoaded, this.InvalidateOptionsMenu)
                .Listen(() => this.ViewModel.SelectedYear, this.InvalidateOptionsMenu)
                .Listen(() => this.ViewModel.ShowLonely, this.InvalidateOptionsMenu);
        }

        protected override void OnSetActionBar(ActionBar ab) {
            base.OnSetActionBar(ab);

            ab.SetDisplayShowTitleEnabled(false);

            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);
            var subtitleView = v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle);
            
            v.Click += ActionBarTitleOnClick;

            v.FindViewById<TextView>(Resource.Id.ActionBarTitle).SetText(Resource.String.Schedule);

            var set = this.CreateBindingSet<ScheduleFragment, ScheduleViewModel>();

            set.Bind(subtitleView)
                .For(x => x.Text)
                .To(x => x.SelectedScheduleText);
            set.Bind(subtitleView)
                .For(x => x.Visibility)
                .To(x => x.SelectedScheduleText)
                .WithConversion("Visibility");

            set.Apply();

            ab.SetCustomView(v, new ActionBar.LayoutParams((int) (GravityFlags.Left | GravityFlags.CenterVertical)));
            ab.SetDisplayShowCustomEnabled(true);
        }

        public override void OnPrepareOptionsMenu(IMenu menu) {
            base.OnPrepareOptionsMenu(menu);

            var vm = this.ViewModel;

            var editItem = menu.FindItem(Resource.Id.ActionEdit);

            if (editItem != null) {
                editItem.SetTitle(
                    Resources.GetString(Resource.String.Edit) + (
                        vm != null && vm.SelectedYear != null
                            ? " " + vm.SelectedYear
                            : String.Empty
                    )
                );
                editItem.SetVisible(
                    vm != null && vm.EditAcademicYearCommand.CanExecute(null)
                );
            }

            menu.FindItem(Resource.Id.ActionNew)?.SetVisible(
                vm != null && vm.CanAddYears && !vm.ShowLonely
            );
            menu.FindItem(Resource.Id.ActionSchool)?.SetVisible(
                vm?.User != null &&
                vm.User.IsStudentWithUnmanagedSchool &&
                vm.CanAddYears &&
                vm.SelectedYear != null &&
                vm.SelectedYear.IsFromSchool
            );
            menu.FindItem(Resource.Id.ActionManageSubjects)?.SetVisible(
                vm != null && vm.ShowManageSubjectsCommand.CanExecute(null)
            );
            menu.FindItem(Resource.Id.ActionJoinClasses)?.SetVisible(
                vm != null && vm.ShowJoinClassesCommand.CanExecute(null)
            );
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.ActionSchool:
                    Mvx.IoCProvider.Resolve<IDialogService>().ShowNotification(null, R.AcademicScheduleFromSchoolError);
                    return true;
                case Resource.Id.ActionJoinClasses:
                    this.ViewModel.ShowJoinClassesCommand.Execute(null);
                    return true;
                case Resource.Id.ActionNew:
                    this.ViewModel.NewAcademicYearCommand.Execute(null);
                    return true;
                case Resource.Id.ActionEdit:
                    this.ViewModel.EditAcademicYearCommand.Execute(null);
                    return true;
                case Resource.Id.ActionManageSubjects:
                    this.ViewModel.ShowManageSubjectsCommand.Execute(null);
                    return true;
            }
            
            return base.OnOptionsItemSelected(item);
        }

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            new AcademicSchedulePickerDialogFragment(
                this.Activity,
                this,
                this.ViewModel.AcademicYears,
                this.ViewModel.SelectedSchedule, R.GetScheduleViewFilterHelp(this.ViewModel.HasClassesWithoutSchedule, false)
            ) {
                TitleResId = Resource.String.FilterByYearOrTerm,
                AllowNone = this.ViewModel.HasClassesWithoutSchedule,
                ShowNew = false
            }.Show(this.Activity.SupportFragmentManager, "schedule-selector");
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            if (OS.HasLollipops) {
                view.FindViewById(Resource.Id.ToolbarShadow).SetVisible(false);
            }

            var viewPager = view.FindViewById<ViewPager>(Resource.Id.Pager);

            viewPager.Adapter = new MvxFixedPagerAdapter(
                this.Activity,
                (IMvxAndroidBindingContext) this.BindingContext,
                new ClassesPage(this.Activity),
                new HolidaysPage(this.Activity)
            );

            var slidingTabs = view.FindViewById<TabLayout>(Resource.Id.Tabs);
            slidingTabs.SetupWithViewPager(viewPager);

            return view;
        }

        public void OnScheduleSet(IAcademicSchedule schedule) {
            if (this.ViewModel != null) {
                this.ViewModel.SelectedSchedule = schedule;
            }
        }
        
        private class ClassesPage : MvxFixedPagerAdapter.Page {
            public ClassesPage(Context context)
                : base(context, Resource.String.Classes, Resource.Layout.View_Schedule_Classes) { }

            public override View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                var v = base.InstantiateView(container, bindingContext);

                var recyclerView = v.FindViewById<MvxRecyclerView>(Resource.Id.Recycler);

                recyclerView.SetLayoutManager(new FABLinearLayoutManager(Context, v.FindViewById<FloatingActionButton>(Resource.Id.FloatingActionButton)));

                var adapter = new ClassAdapter(bindingContext);
                adapter.OnItemClick += ClassListOnItemClick;
                recyclerView.Adapter = adapter;

                recyclerView.AddItemDecoration(new DividerDecoration(Context));

                return v;
            }

            private void ClassListOnItemClick(object sender, EventArgs e) {
                var viewHolder = (MvxRecyclerViewHolder)sender;

                if (viewHolder.DataContext != null) {
                    Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntity((Class)viewHolder.DataContext, viewHolder.ItemView);
                }
            }
        }

        private class HolidaysPage : MvxFixedPagerAdapter.Page {
            public HolidaysPage(Context context)
                : base(context, Resource.String.Holidays, Resource.Layout.View_Schedule_Holidays) { }

            public override View InstantiateView(ViewGroup container, IMvxAndroidBindingContext bindingContext) {
                var v = base.InstantiateView(container, bindingContext);

                var recyclerView = v.FindViewById<MvxRecyclerView>(Resource.Id.Recycler);

                recyclerView.SetLayoutManager(new FABLinearLayoutManager(Context, v.FindViewById<FloatingActionButton>(Resource.Id.FloatingActionButton)));
                recyclerView.AddItemDecoration(new DividerDecoration(Context));

                return v;
            }
        }
    }
}