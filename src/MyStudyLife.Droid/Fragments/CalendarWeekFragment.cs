using System;
using System.ComponentModel;
using System.Threading;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings.Target.Construction;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Droid.Widgets.ObservableScrollView;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using MyStudyLife.UI.ViewModels.Calendar;
using Math = System.Math;
using JavaObject = Java.Lang.Object;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.WeakSubscription;
using MyStudyLife.Droid.Platform;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.Platforms.Android.Binding.Target;
using Microsoft.Extensions.Logging;
using AndroidX.Core.App;

namespace MyStudyLife.Droid.Fragments {
    [Register("mystudylife.droid.fragments.CalendarWeekFragment")]
    public class CalendarWeekFragment : CalendarFragment<CalendarWeekViewModel> {

        private Timer _minuteTimer;

        private MvxViewPager _pager;

        public override int LayoutResourceId {
            get { return Resource.Layout.View_CalendarWeek; }
        }

        public override int? MenuResourceId {
            get { return Resource.Menu.CalendarWeekView; }
        }

        public int CurrentScrollOffset;

        public Toast HolidayToast;

        protected override View GetActionBarView() {
            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);

            var titleView = v.FindViewById<TextView>(Resource.Id.ActionBarTitle);
            var subtitle = v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle);

            titleView.SetCompoundDrawablesWithIntrinsicBounds(0, 0, Resource.Drawable.spinner_dropdown_arrow_white, 0);
            titleView.CompoundDrawablePadding = Resources.GetDimensionPixelSize(Resource.Dimension.margin_xs);

            var set = this.CreateBindingSet<CalendarWeekFragment, CalendarWeekViewModel>();

            set.Bind(v.FindViewById<TextView>(Resource.Id.ActionBarTitle))
                .For(x => x.Text)
                .To(x => x.PageTitleWithoutWeek);
            set.Bind(subtitle)
                .For(x => x.Text)
                .To(x => x.RotationWeekText);
            set.Bind(subtitle)
                .For(x => x.Visibility)
                .To(x => x.RotationWeekText)
                .WithConversion("Visibility");

            set.Apply();

            return v;
        }

        public override void OnResume() {
            base.OnResume();

            this._minuteTimer = new Timer(MinuteTimerCallback, null, TimeSpan.FromSeconds(60d - DateTimeEx.Now.Second), TimeSpan.FromMinutes(1d));
        }

        public override void OnPause() {
            base.OnPause();

            if (this._minuteTimer != null) {
                this._minuteTimer.Dispose();
                this._minuteTimer = null;
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            _pager = view.FindViewById<MvxViewPager>(Resource.Id.CalendarWeekPager);

            _pager.Adapter = new CalendarWeekPagerAdapter(this, (IMvxAndroidBindingContext)this.BindingContext);
            _pager.ItemTemplateId = Resource.Layout.Component_CalendarWeek;

            return view;
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig) {
            base.OnConfigurationChanged(newConfig);

            // TODO(sometime) make calendar week view full screen when landscape

            //#warning Not 100% working on KitKat and above :(
            // http://stackoverflow.com/questions/26890972/android-fullscreen-immersive-when-landscape
            //var decorView = Activity.Window.DecorView;
            //var contentView = Activity.FindViewById(Resource.Id.Content);

            //bool landscape = newConfig.Orientation == Orientation.Landscape;

            //var newUiOptions = (SystemUiFlags) decorView.SystemUiVisibility;

            //if (landscape) {
            //    newUiOptions |= SystemUiFlags.HideNavigation | SystemUiFlags.Fullscreen | SystemUiFlags.ImmersiveSticky;
            //}
            //else {
            //    newUiOptions &= ~(SystemUiFlags.HideNavigation | SystemUiFlags.Fullscreen | SystemUiFlags.ImmersiveSticky);
            //}

            //contentView.SetFitsSystemWindows(!landscape);

            //decorView.SystemUiVisibility = (StatusBarVisibility)newUiOptions;
        }

        private void MinuteTimerCallback(object state) {
            this.SafeGet(x => x.View)?.Post(() => (this._pager?.SafeGet()?.GetCurrentItemView() as CalendarWeekPagerItem)?.SetTimelineOffset());
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                this._pager = null;

                if (this._minuteTimer != null) {
                    this._minuteTimer.Dispose();
                    this._minuteTimer = null;
                }
            }

            base.Dispose(disposing);
        }
    }

    public class CalendarWeekPagerAdapter : MvxDynamicPagerAdapter {
        private readonly CalendarWeekFragment _fragment;

        protected CalendarWeekPagerAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CalendarWeekPagerAdapter(CalendarWeekFragment fragment) : base(fragment.Activity) {
            this._fragment = fragment;
        }
        public CalendarWeekPagerAdapter(CalendarWeekFragment fragment, IMvxAndroidBindingContext bindingContext)
            : base(fragment.Activity, bindingContext) {
            this._fragment = fragment;
        }

        protected override MvxViewPagerItem GetView(int position, object item) {
            return new CalendarWeekPagerItem(this._fragment, this.BindingContext.LayoutInflaterHolder, (CalendarWeekModel)item, this.ItemTemplateId);
        }

        public override JavaObject GetItemId(object item) {
            return ((CalendarWeekModel)item).WeekStart.ToString("yyyy-MM-dd");
        }
    }

    public sealed class CalendarWeekPagerItem : MvxViewPagerItem {
        private readonly CalendarWeekFragment _fragment;

        private bool _userScrolled;

        private IDisposable _subscription;

        private readonly ObservableScrollView _scrollView;
        private readonly View _timelineView;

        public new CalendarWeekModel DataContext => (CalendarWeekModel)base.DataContext;

        public CalendarWeekPagerItem(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public CalendarWeekPagerItem(CalendarWeekFragment fragment, IMvxLayoutInflaterHolder layoutInflater, CalendarWeekModel dataContext, int templateId)
            : base(fragment.Activity, layoutInflater, dataContext, templateId) {

            this._fragment = fragment;

            this._scrollView = this.FindViewById<ObservableScrollView>(Resource.Id.CalendarWeekScroll);
            this._timelineView = this.FindViewById<View>(Resource.Id.CalendarWeekTimeline);

            var entriesLayout = this.FindViewById<CalendarWeekLayout>(Resource.Id.CalendarWeekEntriesLayout);
            var holidaysLayout = this.FindViewById<CalendarWeekHolidaysLayout>(Resource.Id.CalendarWeekHolidays);

            var set = this.CreateBindingSet<CalendarWeekPagerItem, CalendarWeekModel>();

            set.Bind(entriesLayout).For(x => x.WeekStart).To(x => x.WeekStart);

            set.Apply();

            // This fixes #267 - IntPtr.Zero bug, to do with anonymous methods - possibly just in ctor
            // as we don't need it elsewhere
            var item = this;
            this._scrollView.Post(() => item._scrollView.ScrollTo(0, item._fragment.CurrentScrollOffset));

            this._scrollView.ScrollChanged += OnScrollChanged;

            if (dataContext != null) {
                if (dataContext.InitialTime.HasValue) {
                    DoFirstScroll();
                }
                else {
                    _subscription = dataContext.WeakSubscribe(DataContextOnPropertyChanged);
                }
            }

            for (int i = 0; i < holidaysLayout.ChildCount; i++) {
                int d = i;

                holidaysLayout.GetChildAt(i).Click += (s, e) => {
                    var day = this.DataContext.Days[d];

                    if (day.IsHoliday) {
                        _fragment.HolidayToast?.Cancel();

                        var toast = _fragment.HolidayToast = Toast.MakeText(
                            _fragment.Activity, day.HolidayName, ToastLength.Long
                        );
                        toast.Show();
                    }
                };
            }
        }

        private void OnScrollChanged(object sender, ScrollChangedEventArgs e) {
            _userScrolled = true;

            _fragment.CurrentScrollOffset = e.ScrollY;
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(CalendarWeekModel.InitialTime)) {
                _subscription.Dispose();
                _subscription = null;

                DoFirstScroll();
            }
        }

        private void DoFirstScroll() {
            try {
                var dataContext = this.DataContext; // Prevent 2 casts!

                if (dataContext == null) {
                    return;
                }

                var initialTime = dataContext.InitialTime;

                if (this._scrollView != null && !this._userScrolled && initialTime.HasValue && _fragment.CurrentScrollOffset <= 0) {
                    var scrollOffset = DisplayHelper.DpsToPixels(((float)initialTime.Value.TotalHours) * CalendarWeekLayout.HourHeight);

                    // Not sure whether we need item = this here, but better safe than sorry! #267
                    var item = this;
                    this._scrollView.Post(() => item._scrollView.SmoothScrollTo(0, scrollOffset));

                    _fragment.CurrentScrollOffset = scrollOffset;
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogError("CalendarWeekPagerItem: An error occurred during first scroll\n" + ex.ToLongString());
            }
        }

        public override void OnSelected() {
            if (_timelineView != null) {
                var dataContext = this.DataContext; // Prevent 2 casts!

                if (dataContext != null && dataContext.IsCurrentWeek) {
                    this.SetTimelineOffset();
                    // Making the timeline visible after the height has been set prevents the 
                    // timeline temporarily appearing at the top of the view on slower devices
                    _timelineView.Visibility = ViewStates.Visible;
                }
                else {
                    _timelineView.Visibility = ViewStates.Gone;
                }
            }

            _scrollView.Post(() => this._scrollView.ScrollTo(0, _fragment.CurrentScrollOffset));
        }

        public void SetTimelineOffset() {
            _timelineView.SafeGet()?.SetY(Resources.DisplayMetrics.DpsToPixels(64f * (float)DateTimeEx.Now.TimeOfDay.TotalHours));
        }
    }

    [Register("mystudylife.droid.fragments.CalendarWeekHolidaysLayout")]
    public class CalendarWeekHolidaysLayout : ViewGroup {
        private const int DaySeparatorWidth = 1;

        private readonly Paint _outlinePaint;
        private readonly Paint _daySeparatorPaint;

        private int? _outlineWidth;
        private int? _hourColumnWidth;
        private int? _dayWidth;

        private int OutlineWidth {
            get {
                if (!_outlineWidth.HasValue) {
                    _outlineWidth = (int)Math.Round(Resources.DisplayMetrics.Density);
                }

                return _outlineWidth.Value;
            }
        }

        private int HourColumnWidth {
            get {
                if (!_hourColumnWidth.HasValue) {
                    _hourColumnWidth = Resources.DisplayMetrics.DpsToPixels(CalendarWeekLayout.HourColumnWidth);
                }

                return _hourColumnWidth.Value;
            }
        }

        private int DayWidth {
            get {
                if (!_dayWidth.HasValue) {
                    _dayWidth = (int)Math.Round((this.MeasuredWidth - this.HourColumnWidth) / 7f);
                }

                return _dayWidth.Value;
            }
        }

        #region ctor(..)

        protected CalendarWeekHolidaysLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CalendarWeekHolidaysLayout(Context context) : this(context, null) { }
        public CalendarWeekHolidaysLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public CalendarWeekHolidaysLayout(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle) {
            var subtleColor = ContextCompatEx.GetColor(context, Resource.Color.subtle);

            _outlinePaint = new Paint {
                StrokeWidth = OutlineWidth,
                Color = subtleColor
            };

            _daySeparatorPaint = new Paint {
                StrokeWidth = DaySeparatorWidth,
                Color = subtleColor
            };
        }

        #endregion

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int measuredWidth = MeasureSpec.GetSize(widthMeasureSpec),
                measuredHeight = MeasureSpec.GetSize(heightMeasureSpec);

            SetMeasuredDimension(ResolveSize(measuredWidth, widthMeasureSpec), ResolveSize(measuredHeight, heightMeasureSpec));

            var dayWidth = this.DayWidth;

            var widthSpec = MeasureSpec.MakeMeasureSpec(
                dayWidth,
                MeasureSpecMode.Exactly
            );
            var heightSpec = MeasureSpec.MakeMeasureSpec(measuredHeight - (int)Resources.DisplayMetrics.Density, MeasureSpecMode.Exactly);

            for (int i = 0; i < this.ChildCount; i++) {
                var child = this.GetChildAt(i);

                child.Measure(widthSpec, heightSpec);
            }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b) {
            int childLeft = this.HourColumnWidth;

            for (int i = 0; i < this.ChildCount; i++) {
                var child = this.GetChildAt(i);

                child.Layout(
                    childLeft,
                    0,
                    childLeft + child.MeasuredWidth,
                    child.MeasuredHeight
                );

                childLeft += child.MeasuredWidth;
            }
        }

        protected override void DispatchDraw(Canvas canvas) {
            // Draw lines over the top of the children
            base.DispatchDraw(canvas);

            var outlineWidth = OutlineWidth;
            var hourColumnWidth = this.HourColumnWidth;
            var dayWidth = this.DayWidth;

            for (int d = 0; d <= 7; d++) {
                var x = hourColumnWidth + (d * dayWidth);

                canvas.DrawLine(x, 0, x, canvas.Height, _daySeparatorPaint);
            }

            canvas.DrawLine(0, canvas.Height - outlineWidth, canvas.Width, canvas.Height - outlineWidth, _outlinePaint);
        }
    }

    [Register("mystudylife.droid.fragments.CalendarWeekLayout")]
    public class CalendarWeekLayout : MvxFrameLayout {
        public const float HourColumnWidth = 20;
        public const float HourHeight = 64;
        public const int LineThickness = 1;

        private readonly Paint _hourTextPaint;
        private readonly Paint _majorLinePaint;
        private readonly Paint _minorLinePaint;

        private DateTime? _weekStart;

        public DateTime WeekStart {
            get { return _weekStart.GetValueOrDefault(); }
            set {
                if (_weekStart != value) {
                    _weekStart = value;

                    Invalidate();
                }
            }
        }

        protected CalendarWeekLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public CalendarWeekLayout(Context context, IAttributeSet attrs) : this(context, attrs, new CalendarEntryAdapter(context)) { }
        private CalendarWeekLayout(Context context, IAttributeSet attrs, IMvxAdapterWithChangedEvent adapter)
            : base(context, attrs, adapter) {

            _hourTextPaint = new Paint {
                TextAlign = Paint.Align.Right,
                TextSize = Resources.GetDimension(Resource.Dimension.text_calweek_hour),
                SubpixelText = true,
                Color = ContextCompatEx.GetColor(context, Resource.Color.subtle_text),
                AntiAlias = true
            };
            _hourTextPaint.SetTypeface(RobotoWidgetHelper.GetRobotoTypeface(this.Context, RobotoTypeface.Regular));

            _majorLinePaint = new Paint {
                StrokeWidth = LineThickness,
                Color = ContextCompatEx.GetColor(context, Resource.Color.subtle)
            };
            _minorLinePaint = new Paint {
                StrokeWidth = LineThickness,
                Color = ContextCompatEx.GetColor(context, Resource.Color.subtle_super)
            };
        }

        protected override void DispatchDraw(Canvas canvas) {
            var oneDisplayPixel = Resources.DisplayMetrics.Density;
            var hourLabelWidth = oneDisplayPixel * (HourColumnWidth - 4); // 4dp padding
            var hourColumnWidth = oneDisplayPixel * HourColumnWidth;
            var halfHourDisplayPixels = oneDisplayPixel * 32;
            var hourDisplayPixels = halfHourDisplayPixels * 2;

            var contentWidth = (canvas.Width - hourColumnWidth);
            var contentHeight = canvas.Height;

            var dayWidth = (int)Math.Round(contentWidth / 7f);

            var hours = DateTimeFormat.Hours;

            for (int d = 0; d < 8; d++) {
                var x = hourColumnWidth + (d * dayWidth);

                canvas.DrawLine(x, 0, x, contentHeight, _majorLinePaint);
            }

            float textVOffset = ((_hourTextPaint.Descent() + _hourTextPaint.Ascent()) / 2f);

            for (int h = 0; h < 24; h++) {
                var yFull = h * hourDisplayPixels;
                var yHalf = yFull + halfHourDisplayPixels;

                canvas.DrawLine(hourColumnWidth, yFull, canvas.Width, yFull, _majorLinePaint);
                canvas.DrawLine(hourColumnWidth, yHalf, canvas.Width, yHalf, _minorLinePaint);

                if (0 < h) {
                    // Right aligning text means the x value has to be the right, not left
                    canvas.DrawText(hours[h], hourLabelWidth, yFull - textVOffset, _hourTextPaint);
                }
            }

            if (_weekStart.HasValue) {
                var index = (int)(DateTime.Today - _weekStart.Value).TotalDays;

                if (0 <= index && index <= 6) {
                    var paint = new Paint {
                        Color = ContextCompatEx.GetColor(Context, Resource.Color.accent),
                        Alpha = 10
                    };
                    paint.SetStyle(Paint.Style.Fill);

                    canvas.DrawRect(
                        hourColumnWidth + (dayWidth * index),
                        0,
                        hourColumnWidth + (dayWidth * (index + 1)),
                        canvas.Height,
                        paint
                    );
                }
            }

            base.DispatchDraw(canvas);
        }
    }

    public class CalendarEntryAdapter : MvxAdapterWithChangedEvent {
        private DisplayMetrics _displayMetrics;
        private int? _dayWidth;
        private int? _hourColumnWidth;
        private int? _hourHeight;
        private int? _entryMarign;

        private DisplayMetrics DisplayMetrics {
            get { return _displayMetrics ??= DisplayHelper.GetDisplayMetrics(); }
        }

        private int DayWidth {
            get {
                if (!_dayWidth.HasValue) {
                    _dayWidth = (int)Math.Round((DisplayMetrics.WidthPixels - HourColumnWidth) / 7d);
                }

                return _dayWidth.Value;
            }
        }

        private int HourColumnWidth {
            get {
                if (!_hourColumnWidth.HasValue) {
                    _hourColumnWidth = ToDisplayPixels(CalendarWeekLayout.HourColumnWidth);
                }

                return _hourColumnWidth.Value;
            }
        }

        public int HourHeight {
            get {
                if (!_hourHeight.HasValue) {
                    _hourHeight = ToDisplayPixels(CalendarWeekLayout.HourHeight);
                }

                return _hourHeight.Value;
            }
        }

        public int EntryMargin {
            get {
                if (!_entryMarign.HasValue) {
                    _entryMarign = ToDisplayPixels(2);
                }

                return _entryMarign.Value;
            }
        }

        public CalendarEntryAdapter(Context context) : base(context) { }

        #region Overrides of MvxAdapter

        protected override View GetBindableView(View convertView, object dataContext, ViewGroup parent, int templateId) {
            var v = base.GetBindableView(convertView, dataContext, parent, templateId);

            var ae = (AgendaEntry)dataContext;

            var entryWidth = (int)((DayWidth / (float)(ae.ConflictCount + 1)) - EntryMargin);

            v.LayoutParameters = new FrameLayout.LayoutParams(entryWidth, (int)(ae.Duration.TotalHours * HourHeight)) {
                TopMargin = (int)(ae.StartTime.TimeOfDay.TotalHours * HourHeight),
                LeftMargin = (
                    HourColumnWidth + EntryMargin + (DayWidth * L10n.GetDayIndex((int)ae.StartTime.DayOfWeek) +
                        (ae.ConflictIndex > 0 ? (ae.ConflictIndex * (entryWidth + EntryMargin)) : 0)
                    )
                )
            };

            if (ae.IsPast) {
                v.Alpha = 0.5f;
            }

            var title = ae.Title ?? string.Empty;

            var str = v.FindViewById<TextView>(Resource.Id.CalWeekEntryText);

            str.SetText(title + " " + ae.Location, TextView.BufferType.Spannable);

            if (title.Length > 0) {
                ((SpannableString)str.TextFormatted).SetSpan(new StyleSpan(TypefaceStyle.Bold), 0, title.Length, SpanTypes.ExclusiveExclusive);
            }

            v.SetOnClickListener(new ClickListener(ae));

            return v;
        }

        private int ToDisplayPixels(float pixels) => DisplayMetrics.DpsToPixels(pixels);

        class ClickListener : JavaObject, View.IOnClickListener {
            private readonly AgendaEntry _dataContext;

            public ClickListener(AgendaEntry dataContext) {
                _dataContext = dataContext;
            }

            public void OnClick(View view) {
                var activity = (Activity)view.Context;

                Mvx.IoCProvider.Resolve<IDroidNavigationService>().ViewEntry(
                    _dataContext,
                        ActivityOptionsCompat.MakeSceneTransitionAnimation(
                        activity,
                        view,
                        activity.GetString(Resource.String.transition_flexible_space)
                    )
                );
            }
        }

        #endregion
    }

    public class CalendarHeadingTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TextView>("CalHeadingDate", view => new CalendarHeadingTargetBinding(view));
        }

        private static Color? _accentColor, _foregroundColor, _subtleTextColor;

        private static Color AccentColor {
            get {
                if (_accentColor == null) {
                    _accentColor = ContextCompatEx.GetColor(Application.Context, Resource.Color.accent);
                }

                return _accentColor.Value;
            }
        }

        private static Color ForegroundColor {
            get {
                if (_foregroundColor == null) {
                    _foregroundColor = ContextCompatEx.GetColor(Application.Context, Resource.Color.foreground);
                }

                return _foregroundColor.Value;
            }
        }

        private static Color SubtleTextColor {
            get {
                if (_subtleTextColor == null) {
                    _subtleTextColor = ContextCompatEx.GetColor(Application.Context, Resource.Color.subtle_text);
                }

                return _subtleTextColor.Value;
            }
        }

        public override Type TargetType {
            get { return typeof(DateTime); }
        }

        public CalendarHeadingTargetBinding(TextView target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var textView = target as TextView;
            var date = (DateTime)value;

            if (textView == null) {
                return;
            }

            if (date.IsToday()) {
                textView.SetTextColor(AccentColor);
                textView.SetRobotoTypeface(RobotoTypeface.Medium);
            }
            else {
                textView.SetTextColor(date.IsPast() ? SubtleTextColor : ForegroundColor);
                textView.SetRobotoTypeface(RobotoTypeface.Regular);
            }
        }
    }
}