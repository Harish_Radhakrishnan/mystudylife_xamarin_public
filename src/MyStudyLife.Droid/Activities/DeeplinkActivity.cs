using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using MyStudyLife.Diagnostics;
using MyStudyLife.UI.Presentation.DeepLinking;

namespace MyStudyLife.Droid.Activities {
    [Activity(Theme = "@style/MyStudyLife.Theme.NoDisplay", Exported = true)]
    [IntentFilter(new[] {
        Intent.ActionView
    }, Categories = new[] {
        Intent.CategoryBrowsable,
        Intent.CategoryDefault
    }, DataScheme = "msl")]
    public class DeepLinkActivity : Activity {
        private ILogger _log;
        public ILogger Log => _log ??= Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(DeepLinkActivity));

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            var setup = MvxAndroidSetupSingleton.EnsureSingletonAvailable(this);
            setup.EnsureInitialized();

            if (Intent != null) {
                Dispatch(Intent.Data);
            }
            else {
                Log.LogWarning($"{nameof(DeepLinkActivity)} created without an intent");
            }

            Finish();
        }

        public async void Dispatch(Android.Net.Uri uri) {
            try {
                if (uri != null) {
                    if (Mvx.IoCProvider.TryResolve(out IDeepLinkDispatcher dispatcher)) {
                        await dispatcher.Dispatch(new Uri(uri.ToString()));
                    }
                    else {
                        Log.LogError($"Could not resolve {nameof(IDeepLinkDispatcher)}, unable handle deeplink");
                    }
                }
                else {
                    Log.LogWarning($"{nameof(DeepLinkActivity)} created with an intent that does not contain a URI");
                }
            }
            catch (Exception ex) {
                Log.LogError($"Could not resolve {nameof(IDeepLinkDispatcher)}, unable handle deeplink");
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, new List<string> { nameof(DeepLinkActivity) });
            }
        }
    }
}