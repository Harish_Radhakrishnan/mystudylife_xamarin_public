using System;
using System.Collections.Specialized;
using System.Windows.Input;
using Android.App;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.IoC;

namespace MyStudyLife.Droid {
    // This class is never actually executed, but when Xamarin linking is enabled it does how to ensure types and properties
    // are preserved in the deployed app
    public class LinkerPleaseInclude {
        // Fixes #228
        // - https://bugzilla.xamarin.com/show_bug.cgi?id=31228
        // - https://forums.xamarin.com/discussion/42899/datetime-tostring-throws-argumentoutofrangeexception-in-thai-locale
        public void Include(System.Globalization.ThaiBuddhistCalendar cal) {
            cal = new System.Globalization.ThaiBuddhistCalendar();
        }
        public void Include(System.Globalization.PersianCalendar cal) {
            cal = new System.Globalization.PersianCalendar();
        }
        public void Include(System.Globalization.UmAlQuraCalendar cal) {
            cal = new System.Globalization.UmAlQuraCalendar();
        }

        public void Include(Button button) {
            button.Click += (s, e) => button.Text = button.Text + "";
        }

        public void Include(ImageButton button) {
            button.Click += (s, e) => button.ImageAlpha = button.ImageAlpha + 1;
        }

        public void Include(CheckBox checkBox) {
            checkBox.CheckedChange += (sender, args) => checkBox.Checked = !checkBox.Checked;
        }

        public void Include(Switch sw) {
            sw.CheckedChange += (sender, args) => sw.Checked = !sw.Checked;
        }

        public void Include(View view) {
            view.Click += (s, e) => view.ContentDescription = view.ContentDescription + "";
        }

        public void Include(TextView text) {
            text.TextChanged += (sender, args) => text.Text = "" + text.Text;
        }

        public void Include(CheckedTextView text) {
            text.TextChanged += (sender, args) => text.Text = "" + text.Text;
            text.Hint = "" + text.Hint;
        }

        public void Include(CompoundButton cb) {
            cb.CheckedChange += (sender, args) => cb.Checked = !cb.Checked;
        }

        public void IncludeVisibility(View widget) {
            widget.Visibility = widget.Visibility + 1;
        }

        public void Include(SeekBar sb) {
            sb.ProgressChanged += (sender, args) => sb.Progress = sb.Progress + 1;
        }

        public void Include(INotifyCollectionChanged changed) {
            changed.CollectionChanged += (s, e) => { var test =
                $"{e.Action}{e.NewItems}{e.NewStartingIndex}{e.OldItems}{e.OldStartingIndex}"; };
        }

        public void Include(ICommand command) {
            command.CanExecuteChanged += (s, e) => { if (command.CanExecute(null)) command.Execute(null); };
        }

        public void Include(MvxPropertyInjector injector) {
            injector = new MvxPropertyInjector();
        }

        public void Include(System.ComponentModel.INotifyPropertyChanged changed) {
            changed.PropertyChanged += (sender, e) => {
                var test = e.PropertyName;
            };
        }
        
        public void Include(ConsoleColor color)
        {
            Console.Write("");
            Console.WriteLine("");
            color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.ForegroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkGray;
        }

        public void Include(RadioGroup radioGroup)
        {
            radioGroup.CheckedChange += (sender, args) => radioGroup.Check(args.CheckedId);
        }

        public void Include(RadioButton radioButton)
        {
            radioButton.CheckedChange += (sender, args) => radioButton.Checked = args.IsChecked;
        }

        public void Include(RatingBar ratingBar)
        {
            ratingBar.RatingBarChange += (sender, args) => ratingBar.Rating = 0 + ratingBar.Rating;
        }

        public void Include(Activity act)
        {
            act.Title = act.Title + "";
        }

        public void Include(MvxTaskBasedBindingContext context)
        {
            context.Dispose();
            var context2 = new MvxTaskBasedBindingContext();
            context2.Dispose();
        }
    }
}
