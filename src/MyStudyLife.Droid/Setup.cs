using System;
using System.Collections.Generic;
using System.Reflection;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Converters;
using MvvmCross.IoC;
using MvvmCross.Presenters;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;
using MyStudyLife.Droid.Binding;
using MyStudyLife.Droid.Fragments;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Droid.Versioning;
using MyStudyLife.Droid.Views;
using MyStudyLife.Droid.Views.Input;
using MyStudyLife.Droid.Views.Schools;
using MyStudyLife.Droid.Views.Settings;
using MyStudyLife.Droid.Views.Wizards;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Security.Crypto;
using MyStudyLife.Services;
using MyStudyLife.Sync;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.UI.ViewModels.Schools;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.Utility;
using MyStudyLife.Versioning;
using MvvmCross.Platforms.Android.Core;
using Microsoft.Extensions.Logging;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Platforms.Android.Presenters;
using MvvmCross.Platforms.Android;
using MvvmCross.Platforms.Android.Binding.Binders;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.Platforms.Android.Binding;
using Serilog;
using Serilog.Extensions.Logging;
using Log = Serilog.Log;
using Google.Android.Material.FloatingActionButton;
using AndroidX.AppCompat.Widget;
using AndroidX.CardView.Widget;
using Google.Android.Material.Snackbar;
using MvvmCross.Plugin.File.Platforms.Android;

namespace MyStudyLife.Droid {
    public class Setup : MvxAndroidSetup<MslApp>
    {
        protected override IEnumerable<Assembly> AndroidViewAssemblies => new List<Assembly>(base.AndroidViewAssemblies)
        {
            typeof(SwitchCompat).Assembly,
            typeof(CardView).Assembly,
            typeof(AppCompatImageView).Assembly,
            typeof(FloatingActionButton).Assembly,
            typeof(AnderWeb.DiscreteSeekBar.DiscreteSeekBar).Assembly,
            typeof(MikePhil.Charting.Charts.PieChart).Assembly
        };

        protected override IEnumerable<string> ViewNamespaces => new List<string>(base.ViewNamespaces)
        {
            "MyStudyLife.Droid.Widgets"
        };

        protected override IDictionary<string, string> ViewNamespaceAbbreviations {
            get {
                var toReturn = base.ViewNamespaceAbbreviations;
                toReturn.Add("Msl", "MyStudyLife.Droid.Widgets");
                return toReturn;
            }
        }

        protected override IMvxApplication CreateApp(IMvxIoCProvider iocProvider)
        {
            return new MslApp();

        }

        protected override IMvxAndroidViewPresenter CreateViewPresenter() {
            var customPresenter = new DroidPresenter(AndroidViewAssemblies);
            Mvx.IoCProvider.RegisterSingleton<IMvxViewPresenter>(customPresenter);
            Mvx.IoCProvider.RegisterSingleton<IMslDroidPresenter>(customPresenter);
            return customPresenter;
        }

        #region Overrides of MvxSetup

        protected override void InitializeLastChance(IMvxIoCProvider iocProvider)
        {
            base.InitializeLastChance(iocProvider);
            RegisterUpgrades(Mvx.IoCProvider.Resolve<IUpgradeService>());

            var syncService = Mvx.IoCProvider.Resolve<ISyncService>();

            syncService.StatusChanged += SyncServiceOnStatusChanged;
            syncService.Completed += SyncServiceOnCompleted;
            syncService.Error += SyncServiceOnError;
        }

        protected override void InitializeFirstChance(IMvxIoCProvider iocProvider)
        {
            base.InitializeFirstChance(iocProvider);
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDroidNavigationService, IMvxViewModelLoader, IMvxAndroidViewModelRequestTranslator>(
               (loader, translator) => new DroidNavigationService( loader, Mvx.IoCProvider.Resolve<IMvxViewDispatcher>(), iocProvider, translator));
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INavigationService, IDroidNavigationService>((navigationService) => navigationService);
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDateTimeProvider, DroidDateTimeProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IBugReporter, DroidBugReporter>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IMslConfig, DroidConfig>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INetworkProvider, DroidNetworkProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IStorageProvider, DroidStorageProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ICryptoProvider, DroidCryptoProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDeviceInfoProvider, DroidDeviceInfoProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IHttpClientFactory, DroidHttpClientFactory>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDialogService, DroidDialogService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IReminderScheduler, DroidReminderScheduler>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IPushNotificationService, DroidPushNotificationService>();
            Mvx.IoCProvider.RegisterType<ITaskProvider, DroidTaskProvider>();
            Mvx.IoCProvider.RegisterType<ITimer, DroidTimer>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAnalyticsService, DroidAnalyticsService>();

            // Setup File Plugin... 
            var filePlugin = new FilePluginSetupAndroid();
            filePlugin.Load();
        }

        private void RegisterUpgrades(IUpgradeService upgradeService) {
            upgradeService.RegisterUpgrade<V2ToV3>();
            upgradeService.RegisterUpgrade<V3ToV4>();
        }

        #region Sync Notifications

        private Snackbar _syncErrorSnackbar;

        private void SyncServiceOnStatusChanged(object sender, SyncStatusChangedEventArgs e) {
            bool isSyncing = e.NewStatus == SyncStatus.Syncing;

           Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() =>
            {
                if (isSyncing)
                {
                    CancelSyncSnackbar();
                }
            });
        }

        private void SyncServiceOnCompleted(object sender, SyncCompletedEventArgs e) {
            Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(CancelSyncSnackbar);
        }

        private void SyncServiceOnError(object sender, SyncErrorEventArgs e) {
            if (e.Exception is HttpApiUnauthorizedException) return;

            Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                CancelSyncSnackbar();

                var currentActivity = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity;

                if (currentActivity == null) {
                    return;
                }

                _syncErrorSnackbar = Snackbar.Make(
                    currentActivity.FindViewById(Android.Resource.Id.Content),
                    ((ISyncService)sender).Status == SyncStatus.Offline
                        ? Resource.String.Sync_failed_offline
                        : Resource.String.Sync_failed_unknown_error,
                    Snackbar.LengthLong
                );

                _syncErrorSnackbar.Show();
            });
        }

        private void CancelSyncSnackbar() {
            Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                if (_syncErrorSnackbar != null) {
                    _syncErrorSnackbar.Dismiss();
                    _syncErrorSnackbar.Dispose();
                    _syncErrorSnackbar = null;
                }
            });
        }

        #endregion

        #endregion

        #region Overrides of MvxAndroidSetup

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<View>("Enabled", (view) => new MvxViewEnabledTargetBinding(view));

            registry.RegisterCustomBindingFactory<TextView>(ErrorTargetBinding.Name, (view) => new ErrorTargetBinding(view));

            registry.RegisterCustomBindingFactory<MvxViewPager>(MvxViewPagerSelecteditemBinding.Name, (view) => new MvxViewPagerSelecteditemBinding(view));
            registry.RegisterCustomBindingFactory<MvxGridView>(GridViewSelectedItemBinding.Name, (view) => new GridViewSelectedItemBinding(view));

            registry.RegisterCustomBindingFactory<Widgets.DaysPicker>(DaysPickerValueBinding.Name, (picker) => new DaysPickerValueBinding(picker));

            ListPickerItemsSourceBinding.Register<AcademicScheduleSelector>(registry);
            ListPickerItemsSourceBinding.Register<SubjectPicker>(registry);
            ListPickerItemsSourceBinding.Register<ColorPicker>(registry);

            ListPickerSelectedItemBinding.Register<AcademicScheduleSelector>(registry);
            ListPickerSelectedItemBinding.Register<SubjectPicker>(registry);
            ListPickerSelectedItemBinding.Register<ColorPicker>(registry);

            TimePickerAccentColorBinding.Register(registry);
            DatePickerAccentColorBinding.Register(registry);

            Widgets.DatePicker.ValueBinding.Register(registry);
            Widgets.DatePicker.MinDateBinding.Register(registry);
            Widgets.DatePicker.MaxDateBinding.Register(registry);
            ColorPicker.SelectedValueBinding.Register(registry);
            MvxRadioGroupSelectedIndexBinding.Register(registry);
            TaskStateIndicatorTargetBinding.Register(registry);
            AutoLinkerTargetBinding.Register(registry);
            CardViewBackgroundColorTargetBinding.Register(registry);
            CheckedChangeTargetBinding.Register(registry);
            EnumPicker.TargetBinding.Register(registry);
            ErrorLabelLayout.TargetBinding.Register(registry);
            LonelyView.TextTargetBinding.Register(registry);
            LonelyView.SubTextTargetBinding.Register(registry);
            LonelyView.GlyphTargetBinding.Register(registry);
            DiscreteSeekBarTargetBinding.Register(registry);
            BackgroundColorFilterTargetBinding.Register(registry);
            ImageViewColorFilterTargetBinding.Register(registry);
            CalendarHeadingTargetBinding.Register(registry);
            ClassTimeOccurrenceTextView.Binding.Register(registry);
            StrikeThruTargetBinding.Register(registry);
            ViewInvisibleTargetBinding.Register(registry);
            MultiLengthTextView.Binding.Register(registry);
            PieChartTargetBinding.Register(registry);
            TimelineOffsetTargetBinding.Register(registry);
            TextColorWithFallbackTargetBinding.Register(registry);

            base.FillTargetFactories(registry);
        }

        protected override IDictionary<Type, Type> InitializeLookupDictionary(IMvxIoCProvider iocProvider)
        {
            return new Dictionary<Type, Type>
            {
                [typeof(SignInViewModel)] = typeof(SignInView),
                [typeof(WelcomeViewModel)] = typeof(WelcomeView),
                [typeof(SchoolWelcomeViewModel)] = typeof(SchoolWelcomeView),
                [typeof(UpgradeViewModel)] = typeof(UpgradeView),
                [typeof(JoinClassesViewModel)] = typeof(JoinClassesView),
                [typeof(DashboardViewModel)] = typeof(DashboardFragment),
                [typeof(CalendarWeekViewModel)] = typeof(CalendarWeekFragment),
                [typeof(CalendarMonthViewModel)] = typeof(CalendarMonthFragment),
                [typeof(TasksViewModel)] = typeof(TasksFragment),
                [typeof(ExamsViewModel)] = typeof(ExamsFragment),
                [typeof(ScheduleViewModel)] = typeof(ScheduleFragment),
                [typeof(SettingsViewModel)] = typeof(SettingsFragment),
                [typeof(ClassViewModel)] = typeof(ClassView),
                [typeof(TaskViewModel)] = typeof(TaskView),
                [typeof(ExamViewModel)] = typeof(ExamView),
                [typeof(AcademicYearInputViewModel)] = typeof(AcademicYearInputView),
                [typeof(AcademicTermInputViewModel)] = typeof(AcademicTermInputView),
                [typeof(SubjectInputViewModel)] = typeof(SubjectInputView),
                [typeof(HolidayInputViewModel)] = typeof(HolidayInputView),
                [typeof(ClassInputViewModel)] = typeof(ClassInputView),
                [typeof(ClassTimeInputViewModel)] = typeof(ClassTimeInputView),
                [typeof(TaskInputViewModel)] = typeof(TaskInputView),
                [typeof(ExamInputViewModel)] = typeof(ExamInputView),
                [typeof(SubjectsViewModel)] = typeof(SubjectsView),
                [typeof(GeneralSettingsViewModel)] = typeof(GeneralSettingsView),
                [typeof(ReminderSettingsViewModel)] = typeof(ReminderSettingsView),
                [typeof(DashboardSettingsViewModel)] = typeof(DashboardSettingsView),
                [typeof(TimetableSettingsViewModel)] = typeof(TimetableSettingsView),
                [typeof(SyncSettingsViewModel)] = typeof(SyncSettingsView)
            };
        }

        protected override void FillValueConverters(IMvxValueConverterRegistry registry) {
            // PERF: Doing this manually (not calling base) saves 40-50ms
            registry.AddOrOverwrite("Boolean", new BooleanConverter());
            registry.AddOrOverwrite("InvertedBoolean", new InvertedBooleanConverter());
            registry.AddOrOverwrite("KeyToValue", new KeyToValueConverter());
            registry.AddOrOverwrite("Coalesce", new CoalesceConverter());
            registry.AddOrOverwrite("StringFormat", new StringFormatConverter());
            registry.AddOrOverwrite("StringFormatUppercase", new StringFormatUppercaseConverter());
            registry.AddOrOverwrite("StringToTitleCase", new StringToTitleCaseConverter());
            registry.AddOrOverwrite("StringToSentenceCase", new StringToSentenceCaseConverter());
            registry.AddOrOverwrite("RelativeDate", new RelativeDateConverter());
            registry.AddOrOverwrite("NumericRelativeDate", new NumericRelativeDateConverter());
            registry.AddOrOverwrite("AbbrLongDateFormat", new AbbrLongDateFormatConverter());
            registry.AddOrOverwrite("TimeSpanToShortString", new TimeSpanToShortStringConverter());
            registry.AddOrOverwrite("ClassOccurrence", new ClassOccurrenceConverter());
            registry.AddOrOverwrite("SchedulingModeText", new Views.Input.SchedulingModeTextConverter());
            registry.AddOrOverwrite("TimetableModeVisibility", new TimetableModeVisibilityConverter());
            registry.AddOrOverwrite("TaskRevisionTypeVisibility", new TaskRevisionTypeVisibilityConverter());
            registry.AddOrOverwrite("PluralFormat", new PluralFormatConverter());
            registry.AddOrOverwrite("PluralFormatWithValue", new PluralFormatWithValueConverter());
            registry.AddOrOverwrite("ContextualDate", new ContextualDateConverter());

            // TODO: Phase out and introduce System.Drawing.Color in list item view models...
            var scvc = Mvx.IoCProvider.IoCConstruct<SubjectColorToValueConverter>();

            registry.AddOrOverwrite("SubjectColor", scvc);
            registry.AddOrOverwrite("SubjectColorTo", scvc);

            // Not sure if used
            registry.AddOrOverwrite("EnumToTitle", new EnumToTitleConverter());
        }

        #endregion

        public class AppCompatViewFactory : MvxAndroidViewFactory {
            public override View CreateView(View parent, string name, Context context, IAttributeSet attrs) {
                View view;

                // Robotofy all the things
                if (name == "TextView") {
                    view = new RobotoTextView(context, attrs);
                }
                else {
                    view = base.CreateView(parent, name, context, attrs);
                }

                // This enables us to prevent views being created when they are filling in for
                // missing functionality in earlier OS versions. Compat version is the version in
                // which the functionality was added, so this is the first version that the view
                // will not be created on.
                using (var a = context.ObtainStyledAttributes(attrs, Resource.Styleable.Compat)) {
                    var comaptVersion = a.GetInt(Resource.Styleable.Compat_compat_version, -1);

                    if (-1 < comaptVersion && comaptVersion <= (int)Build.VERSION.SdkInt) {
                        // Hide, don't return null as null means Android will create the view
                        view?.SetVisible(false);
                    }

                    a.Recycle();
                }

                return view;
            }

//            public override View CreateView(string name, Context context, IAttributeSet attrs) {


//                if (Build.VERSION.SdkInt < BuildVersionCodes.Lollipop) {

//                    // If we're running pre-L, we need to 'inject' our tint aware Views in place of the
//                    // standard framework versions
//                    switch (name) {
//#warning TintEditText commented out as it ignores padding
//                        // https://code.google.com/p/android/issues/detail?id=77982
//                        //case "EditText":
//                        //    return new TintEditText(context, attrs);
//                        case "Spinner":
//                            return new AppCompatSpinner(context, attrs);
//                        case "CheckBox":
//                            return new AppCompatCheckBox(context, attrs);
//                        case "RadioButton":
//                            return new AppCompatRadioButton(context, attrs);
//                        case "CheckedTextView":
//                            return new AppCompatCheckedTextView(context, attrs);
//                    }
//                }
//                return base.CreateView(name, context, attrs);
//            }
        }

        public class BindingBuilder : MvxAndroidBindingBuilder {
            protected override IMvxAndroidViewFactory CreateAndroidViewFactory() => new AppCompatViewFactory();
        }

        protected override MvxBindingBuilder CreateBindingBuilder() => new BindingBuilder();

        protected override ILoggerProvider CreateLogProvider()
        {
            return new SerilogLoggerProvider();
        }

        protected override ILoggerFactory CreateLogFactory()
        {
            // serilog configuration
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                // add more sinks here
                .CreateLogger();

            return new SerilogLoggerFactory();
        }
    }
}