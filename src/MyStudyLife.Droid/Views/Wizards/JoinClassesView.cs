using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.Animations;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Wizards;
using Object = Java.Lang.Object;
using MvvmCross.Platforms.Android.Views;
using Android.Widget;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using Android.App;

namespace MyStudyLife.Droid.Views.Wizards {
    [Activity(Label = "Join Classes")]
    public class JoinClassesView : MvxActivity<JoinClassesViewModel>, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        public int StepCount => 5;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_JoinClasses);

            this.SetSupportActionBar(this.FindViewById<Toolbar>(Resource.Id.Toolbar));

            this.FindViewById(Resource.Id.Step1Content).Visibility = ViewStates.Visible;
            this.FindViewById(Resource.Id.Step2Content).Visibility = ViewStates.Gone;
            this.FindViewById(Resource.Id.Step3Content).Visibility = ViewStates.Gone;
            this.FindViewById(Resource.Id.Step4Content).Visibility = ViewStates.Gone;
            this.FindViewById(Resource.Id.Step5Content).Visibility = ViewStates.Gone;

            this.FindViewById(Resource.Id.FindModeTeacher).Click += (s, e) => this.ViewModel.FindModeCommand.Execute(JoinClassesFindMode.Teacher);
            this.FindViewById(Resource.Id.FindModeSubject).Click += (s, e) => this.ViewModel.FindModeCommand.Execute(JoinClassesFindMode.Subject);

            this.FindViewById<ListView>(Resource.Id.TeachersListView).ItemClick += GetMultiSelectClickHandler(() => this.ViewModel.Teachers, this.ViewModel.SelectedTeachers);
            this.FindViewById<ListView>(Resource.Id.SubjectsListView).ItemClick += GetMultiSelectClickHandler(() => this.ViewModel.Subjects, this.ViewModel.SelectedSubjects);
            this.FindViewById<ListView>(Resource.Id.ClassesListView).ItemClick += GroupedClassOnClick;

            var classesAdapter = new GroupedAdapter<Class>(this, (IMvxAndroidBindingContext)this.BindingContext) {
                HeaderTemplateId = Resource.Layout.ListItem_GroupHeader,
                LonelyTemplateId = Resource.Layout.ListItem_GroupLonely,
                ItemTemplateId = Resource.Layout.ListItem_JoinClasses_Class,
                ResolveLonelyViewDataContext = (group) => String.Format(
                    "There are no classes for this {0} in {1}.",
                    this.ViewModel.FindModeAsString,
                    this.ViewModel.SelectedScheduleText
                )
            };

            var classTimesAdapter = new GroupedAdapter<ClassTime>(this, (IMvxAndroidBindingContext)this.BindingContext) {
                HeaderTemplateId = Resource.Layout.ListItem_GroupHeader,
                LonelyTemplateId = Resource.Layout.ListItem_GroupLonely,
                ItemTemplateId = Resource.Layout.ListItem_JoinClasses_ClassTime,
                ResolveLonelyViewDataContext = (group) => "There are no times for this class. Your teacher may add some soon."
            };

            this.FindViewById<ListView>(Resource.Id.ClassesListView).Adapter = classesAdapter;
            this.FindViewById<ListView>(Resource.Id.ClassTimesListView).Adapter = classTimesAdapter;

            var bindingSet = this.CreateBindingSet<JoinClassesView, JoinClassesViewModel>();

            bindingSet.Bind(classesAdapter)
                      .For(x => x.GroupedItems)
                      .To(x => x.GroupedClasses)
                      .OneWay();
            bindingSet.Bind(classTimesAdapter)
                      .For(x => x.GroupedItems)
                      .To(x => x.GroupedClassTimes)
                      .OneWay();

            bindingSet.Apply();

            SetState();
        }

        protected override void OnResume() {
            base.OnResume();

            var ab = this.SupportActionBar;

            ab.SetDisplayShowTitleEnabled(false);

            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);

            v.Click += ActionBarTitleOnClick;

            v.FindViewById<TextView>(Resource.Id.ActionBarTitle).Text = this.Title;

            var set = this.CreateBindingSet<JoinClassesView, JoinClassesViewModel>();

            set.Bind(v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle))
                .For(x => x.Text)
                .To(x => x.SelectedScheduleText);

            set.Apply();

            ab.SetCustomView(v, new ActionBar.LayoutParams((int)(GravityFlags.Left | GravityFlags.CenterVertical)));
            ab.SetDisplayShowCustomEnabled(true);

            this.SupportActionBar.SetHomeButtonEnabled(true);
            this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            new AcademicSchedulePickerDialogFragment(this, this, this.ViewModel.AcademicYears, this.ViewModel.SelectedSchedule, R.ClassInput.JoinWizardAcademicScheduleHelp) {
                ShowNew = false,
                AllowNone = false
            }.Show(this.SupportFragmentManager, "schedule-picker");
        }

        public void OnScheduleSet(IAcademicSchedule schedule) {
            this.ViewModel.SelectedSchedule = schedule;
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (item.ItemId == Android.Resource.Id.Home) {
                if (this.ViewModel != null && this.ViewModel.CanGoBack) {
                    this.ViewModel.PreviousStepCommand.Execute(null);
                }
                else {
                    this.Finish();
                }

                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private EventHandler<AdapterView.ItemClickEventArgs> GetMultiSelectClickHandler<T>(Func<IEnumerable<T>> getItemsSource, ObservableCollection<object> selectedItems) {
            return (s, e) => {
                var itemsSource = getItemsSource();
                var itemsSourceList = itemsSource as IList<T> ?? itemsSource.ToList();

                var itemPositions = ((ListView)s).CheckedItemPositions;

                var loopCount = itemPositions.Size();

                if (loopCount > itemsSourceList.Count) {
                    loopCount = itemsSourceList.Count;
                }

                for (int i = 0; i < loopCount; i++) {
                    var item = itemsSourceList[itemPositions.KeyAt(i)];

                    if (itemPositions.ValueAt(i)) {
                        if (!selectedItems.Contains(item)) {
                            selectedItems.Add(item);
                        }
                    }
                    else {
                        selectedItems.Remove(item);
                    }
                }
            };
        }

        private void GroupedClassOnClick(object sender, AdapterView.ItemClickEventArgs e) {
            var items = this.ViewModel.Classes.ToList();
            var selectedItems = this.ViewModel.SelectedClasses;

            var itemPositions = ((ListView)sender).CheckedItemPositions;
            var adapter = (GroupedAdapter<Class>)((ListView)sender).Adapter;

            for (int i = 0; i < itemPositions.Size(); i++) {
                var itemPos = adapter.ItemPositionToFlatPosition(itemPositions.KeyAt(i));

                if (itemPos >= items.Count) {
                    continue;
                }
                var item = items[itemPos];

                if (itemPositions.ValueAt(i)) {
                    if (!selectedItems.Contains(item)) {
                        selectedItems.Add(item);
                    }
                }
                else {
                    selectedItems.Remove(item);
                }
            }
        }

        public override void OnBackPressed() {
            if (this.ViewModel != null && this.ViewModel.CanGoBack) {
                this.ViewModel.PreviousStepCommand.Execute();
            }
            else {
                base.OnBackPressed();
            }
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.Step, SetState)
                .Listen(() => this.ViewModel.IsBusy, () =>
                    RunOnUiThread(() => TryHideOrShowProgressDialog(this.ViewModel.IsBusy))
                );

            this.ViewModel.StepChanged += OnStepChanged;
        }

        private void OnStepChanged(object sender, WizardViewModel.StepChangedEventArgs e) {
            if (e.IsBack) {
                return;
            }

            switch (e.NewStep) {
                case 1: {
                        var listView = this.FindViewById<ListView>(Resource.Id.TeachersListView);

                        for (int i = 0; i < listView.Count; i++) {
                            listView.SetItemChecked(i, false);
                        }

                        listView = this.FindViewById<ListView>(Resource.Id.SubjectsListView);

                        for (int i = 0; i < listView.Count; i++) {
                            listView.SetItemChecked(i, false);
                        }
                    }
                    break;
                case 2: {
                        var listView = this.FindViewById<ListView>(Resource.Id.ClassesListView);

                        for (int i = 0; i < listView.Count; i++) {
                            listView.SetItemChecked(i, false);
                        }
                    }
                    break;
                case 4: {
                        this.FindViewById<TextView>(Resource.Id.JoinClassesSuccessText).Text = String.Format(
                            new PluralFormatProvider(true),
                            Resources.GetString(Resource.String.JoinClasses_Success),
                            this.ViewModel.SelectedClasses.Count
                        );
                    }
                    break;
            }
        }

        #region Progress Dialog

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

        private void TryHideOrShowProgressDialog(bool isBusy) {
            DialogHelper.ShowOrHideProgressDialog(
                this,
                _progressDialogReference,
                isBusy,
                this.ViewModel.Step == 3 ? R.Common.UpdatingWithEllipsis : R.Common.LoadingWithEllipsis
            );
        }

        #endregion

        #region State

        private int _previousStep;

        private void SetState() {
            int step = this.ViewModel.Step;

            if (this.SupportActionBar.CustomView != null) {
                this.SupportActionBar.CustomView.Enabled = step == 0;
            }

            var backButton = this.FindViewById<Button>(Resource.Id.BackButton);

            var maxStepIndex = (StepCount - 1);

            this.FindViewById(Resource.Id.ButtonBar).Visibility = step == 0 ? ViewStates.Gone : ViewStates.Visible;
            this.FindViewById<Button>(Resource.Id.NextButton).Visibility = step < maxStepIndex ? ViewStates.Visible : ViewStates.Gone;
            this.FindViewById<Button>(Resource.Id.DoneButton).Visibility = step == maxStepIndex ? ViewStates.Visible : ViewStates.Gone;

            backButton.Enabled = true;
            backButton.Visibility = (step > 0 && step < maxStepIndex) ? ViewStates.Visible : ViewStates.Gone;

            this.FindViewById(Resource.Id.ButtonSeparator).Visibility = step < maxStepIndex ? ViewStates.Visible : ViewStates.Gone;

            View oldStep = _previousStep == 0 && step == 0 ? null : GetViewForStep(_previousStep);
            View newStep = GetViewForStep(step);

            bool isBack = step < _previousStep;

            if (oldStep != null) {
                var animOut = AnimationUtils.LoadAnimation(this, isBack ? Resource.Animation.push_right_out : Resource.Animation.push_left_out);

                animOut.AnimationEnd += (s, e) => {
                    oldStep.Visibility = ViewStates.Gone;
                };

                oldStep.StartAnimation(animOut);
            }

            var animIn = AnimationUtils.LoadAnimation(this, isBack ? Resource.Animation.push_right_in : Resource.Animation.push_left_in);

            animIn.AnimationStart += (s, e) => {
                newStep.Visibility = ViewStates.Visible;
            };

            newStep.StartAnimation(animIn);

            _previousStep = step;
        }

        private View GetViewForStep(int step) {
            switch (step) {
                case 1:
                    return this.FindViewById(Resource.Id.Step2Content);
                case 2:
                    return this.FindViewById(Resource.Id.Step3Content);
                case 3:
                    return this.FindViewById(Resource.Id.Step4Content);
                case 4:
                    return this.FindViewById(Resource.Id.Step5Content);
            }

            return this.FindViewById(Resource.Id.Step1Content);
        }

        #endregion
    }

    public class GroupedAdapter<T> : BaseAdapter<T>
        where T : class {

        enum ItemViewType {
            Header,
            Item,
            Lonely
        }

        class ItemView {
            public ItemViewType Type { get; private set; }

            public object DataContext { get; private set; }

            public ItemView(ItemViewType type, object dataContext) {
                this.Type = type;
                this.DataContext = dataContext;
            }
        }

        private readonly Context _context;
        private readonly IMvxAndroidBindingContext _bindingContext;
        private IList<ItemView> _items;
        private IEnumerable<IGrouping<string, T>> _groupedItems;
        private readonly HashSet<int> _groupHeaderPositions;
        private readonly HashSet<int> _emptyGroupPositions;

        public IEnumerable<IGrouping<string, T>> GroupedItems {
            get => _groupedItems;
            set {
                if (value != null) {
                    this._items = new List<ItemView>();

                    int i = 0;

                    foreach (var group in value) {
                        this._items.Add(new ItemView(ItemViewType.Header, group.Key));

                        this._groupHeaderPositions.Add(i);

                        foreach (var item in group) {
                            this._items.Add(new ItemView(ItemViewType.Item, item));

                            i++;
                        }

                        if (!group.Any()) {
                            this._items.Add(new ItemView(ItemViewType.Lonely, ResolveLonelyViewDataContext != null ? ResolveLonelyViewDataContext(group.Key) : group.Key));

                            this._emptyGroupPositions.Add(i);
                        }

                        i++;
                    }
                }

                _groupedItems = value;

                this.NotifyDataSetInvalidated();
            }
        }

        public Func<string, object> ResolveLonelyViewDataContext { get; set; }

        public int HeaderTemplateId { get; set; }

        public int LonelyTemplateId { get; set; }

        public int ItemTemplateId { get; set; }

        public int[] GroupHeaderPositions => _groupHeaderPositions.ToArray();

        /// <summary>
        ///     Positions where <see cref="LonelyTemplateId"/> is
        ///     used to display an empty group.
        /// </summary>
        public int[] EmptyGroupPositions => _emptyGroupPositions.ToArray();

        public GroupedAdapter(Context context, IMvxAndroidBindingContext bindingContext) {
            this._context = context;
            this._bindingContext = bindingContext;
            this._groupHeaderPositions = new HashSet<int>();
            this._emptyGroupPositions = new HashSet<int>();
        }

        /// <summary>
        ///     Gets the position of the given item excluding any group headers.
        /// </summary>
        public int GetFlatPositionOf(T item) {
            int itemPos = 0;

            for (int i = 0; i < this.Count; i++) {
                if (GroupHeaderPositions.Contains(i) || EmptyGroupPositions.Contains(i)) {
                    continue;
                }

                if (this[i] == item) {
                    return itemPos;
                }

                itemPos++;
            }

            return -1;
        }

        public int ItemPositionToFlatPosition(int i) {
            return i - (GroupHeaderPositions.Count(x => x < i) + EmptyGroupPositions.Count(x => x < i));
        }

        public override Object GetItem(int position) {
            return null;
        }

        public override T this[int position] => this._items[position].DataContext as T;

        public override long GetItemId(int position) {
            return position;
        }

        public override int ViewTypeCount => 3;

        public override int GetItemViewType(int position) {
            return (int)this._items[position].Type;
        }

        public override View GetView(int position, View convertView, ViewGroup parent) {
            var itemView = _items[position];

            return itemView.Type switch {
                ItemViewType.Header => GetHeaderView((string)itemView.DataContext, convertView, parent),
                ItemViewType.Item => GetItemView(position, (T)itemView.DataContext, convertView, parent),
                ItemViewType.Lonely => GetLonelyView((string)itemView.DataContext, convertView, parent),
                _ => null
            };
        }

        protected virtual View GetHeaderView(string header, View convertView, ViewGroup parent) {
            var view = convertView?.Tag as IMvxListItemView;

            if (view != null && view.TemplateId != HeaderTemplateId) {
                view = null;
            }

            if (view == null) {
                // Clickable = true means that the click event won't propagate to the list view preventing
                // any selection
                view = new MvxListItemView(_context, _bindingContext.LayoutInflaterHolder, header, parent, HeaderTemplateId);
            }
            else {
                view.DataContext = header;
            }

            view.Content.Clickable = true;

            return view.Content;
        }

        protected virtual View GetLonelyView(string lonelyText, View convertView, ViewGroup parent) {
            var view = convertView?.Tag as IMvxListItemView;

            if (view != null && view.TemplateId != HeaderTemplateId) {
                view = null;
            }

            if (view == null) {
                view = new MvxListItemView(_context, _bindingContext.LayoutInflaterHolder, lonelyText, parent, LonelyTemplateId);
            }
            else {
                view.DataContext = lonelyText;
            }

            view.Content.Clickable = true;

            return view.Content;
        }

        protected virtual View GetItemView(int position, T item, View convertView, ViewGroup parent) {
            var view = convertView?.Tag as IMvxListItemView;

            if (view != null && view.TemplateId != ItemTemplateId) {
                view = null;
            }

            if (view == null) {
                view = new MvxListItemView(_context, _bindingContext.LayoutInflaterHolder, item, parent, ItemTemplateId);
            }
            else {
                view.DataContext = item;
            }

            return view.Content;
        }

        public override int Count => _items?.Count ?? 0;
    }
}