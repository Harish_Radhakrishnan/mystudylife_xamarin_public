using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Common;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using FFImageLoading;
using FFImageLoading.Cross;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Settings;
using Fragment = AndroidX.Fragment.App.Fragment;
using MyStudyLife.UI.ViewModels.Components;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Views;
using ActionBarDrawerStateChangeEventArgs = MyStudyLife.Droid.Common.ActionBarDrawerStateChangeEventArgs;
using AndroidX.DrawerLayout.Widget;
using Microsoft.Extensions.Logging;
using Android.Widget;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;

namespace MyStudyLife.Droid.Views {
    [Activity(
        Label = "",
        ScreenOrientation = ScreenOrientation.User
    )]
    public class CoreView : MvxActivity {
        private MvxPropertyChangedListener _vmListener;

        private Color _statusBarColor;
        private Color _colorPrimary;

        private Toolbar _toolbar;

        private DrawerLayout _drawerLayout;
        private View _drawer;
        private ActionBarDrawerToggleWrapper _drawerToggle;
        private DrawerNavigationItemsView _drawerNavigationItemsView;
        private MvxCachedImageView _userPictureView;
        private ImageView _userPicturePlaceholderView;

        public new NavigationDrawerViewModel ViewModel {
            get { return base.ViewModel as NavigationDrawerViewModel; }
            set { base.ViewModel = value; }
        }

        public Color StatusBarColor {
            get { return _statusBarColor; }
            set {
                _statusBarColor = value;

                this.FindViewById(Resource.Id.Content)?.SetBackgroundColor(value);
            }
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _vmListener?.Dispose();
            _vmListener = null;

            var vm = this.ViewModel;
            if (vm != null) {
                _vmListener = new MvxPropertyChangedListener(vm)
                    .Listen(nameof(vm.User), SetUserPicture);

                if (vm.User != null) {
                    SetUserPicture();
                }
            }
        }

        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);

            Mvx.IoCProvider.Resolve<IMslDroidPresenter>().RegisterCoreView(this);

            var a = Theme.ObtainStyledAttributes(new[] { Resource.Attribute.colorPrimary });

            _colorPrimary = _statusBarColor = a.GetColor(0, Color.Transparent);

            a.Recycle();

            SetContentView(Resource.Layout.View_Core);

            if (OS.HasLollipops) {
                Window.SetStatusBarColor(Color.ParseColor("#32000000"));
                this.FindViewById(Resource.Id.Content).SetPadding(0, Resources.DisplayMetrics.DpsToPixels(24), 0, 0);
            }

            this._toolbar = this.FindViewById<Toolbar>(Resource.Id.Toolbar);

            this.SetSupportActionBar(_toolbar);

            // Workaround for all screens but CoreView having dark text on
            // the toolbar despite manually setting theme / same resource
            this._toolbar.SetTitleTextColor(Color.White);
            ToolbarExtensions.EnsureConsistentStyle(_toolbar);
            this.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            this.SupportActionBar.SetHomeButtonEnabled(true);

            this.SetupDrawer();

            if (savedInstanceState == null) {
                ShowFragmentFromIntent(Intent);
            }

            if (GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this) != ConnectionResult.Success) {
                GoogleApiAvailability.Instance.MakeGooglePlayServicesAvailable(this);
            }
        }

        // TODO: In the future should we use this for external fragment requests?
        // Handles being shown from a reminder
        protected override void OnNewIntent(Intent intent) => ShowFragmentFromIntent(intent);

        private void ShowFragmentFromIntent(Intent intent) {
            var serializedRequest = intent.GetStringExtra(DroidPresenter.ViewModelRequestBundleKey);
            var serializer = Mvx.IoCProvider.Resolve<IMvxNavigationSerializer>().Serializer;
            var request = serializer.DeserializeObject<MvxViewModelRequest>(serializedRequest);

            var bundle = new Bundle();
            bundle.PutString(DroidPresenter.ViewModelRequestBundleKey, serializedRequest);

            this.Show(request, bundle);
        }

        private void SetupDrawer() {
            this._drawerLayout = this.FindViewById<DrawerLayout>(Resource.Id.DrawerLayout);
            this._drawer = this.FindViewById(Resource.Id.Drawer);

            var standardIncrement = Resources.DisplayMetrics.DpsToPixels(DisplayHelper.IsTablet ? 64 : 56);

            // Spec says standardIncrement * 5, but that looks crap
            var drawerWidth = Math.Min((Resources.DisplayMetrics.WidthPixels - standardIncrement), standardIncrement * 6);

            _drawer.LayoutParameters.Width = drawerWidth;

            var statusBarHeight = Resources.DisplayMetrics.DpsToPixels(25);

            // 16:9 ratio INCLUDING status bar (even when not transparent)
            var drawerHeaderHeight = ((9 * drawerWidth) / 16) - (OS.HasLollipops ? 0 : statusBarHeight);

            var drawerHeader = _drawer.FindViewById(Resource.Id.DrawerHeader);
            this._userPictureView = drawerHeader.FindViewById<MvxCachedImageView>(Resource.Id.DrawerUserPicture);
            this._userPictureView.Transformations = new List<ITransformation> {
                new CircleTransformation()
            };
            this._userPictureView.Reload();
            this._userPicturePlaceholderView = drawerHeader.FindViewById<ImageView>(Resource.Id.DrawerUserPicturePlaceholder);

            this.SetUserPicture();

            drawerHeader.LayoutParameters.Height = drawerHeaderHeight;

            if (OS.HasLollipops) {
                var marginLayoutParams = (ViewGroup.MarginLayoutParams)this._drawer.FindViewById(Resource.Id.StandardSchoolLogo).LayoutParameters;

                marginLayoutParams.TopMargin += statusBarHeight;
            }

            this._drawerNavigationItemsView = _drawer.FindViewById<DrawerNavigationItemsView>(Resource.Id.DrawerNavigationItems);

            var currentFragment = SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame) as BaseFragment;

            if (currentFragment != null) {
                this._drawerNavigationItemsView.SetSelectedIndexFromViewModel(currentFragment.FindAssociatedViewModelTypeOrNull());
            }

            this._drawerNavigationItemsView.SelectionChanged += NavigationItemsViewOnSelectionChanged;
            this._drawerNavigationItemsView.SelectedItemClick += NavigationItemsViewOnSelectedItemClick;

            this._drawerToggle = new ActionBarDrawerToggleWrapper(
                this,
                _drawerLayout,
                _toolbar,
                Resource.String.Common_Open,
                Resource.String.Common_Close
            );
            this._drawerLayout.AddDrawerListener(this._drawerToggle);
            this._drawerLayout.DrawerClosed += DrawerOnClosed;
            this._drawerLayout.DrawerOpened += DrawerOnOpened;

            var drawerSyncButton = this.FindViewById(Resource.Id.DrawerSyncButton);

            drawerSyncButton.LongClick += DrawerSyncButtonOnLongClick;
            drawerSyncButton.FindViewById<ProgressBar>(Resource.Id.SyncingIndicator).IndeterminateDrawable.SetColorFilter(
                new PorterDuffColorFilter(ContextCompatEx.GetColor(this, Resource.Color.accent), PorterDuff.Mode.SrcAtop)
            );
        }

        private void DrawerOnClosed(object sender, DrawerLayout.DrawerClosedEventArgs e) {
            var vm = ViewModel;
            if (vm != null) {
                vm.IsOpen = false;
            }
        }

        private void DrawerOnOpened(object sender, DrawerLayout.DrawerOpenedEventArgs e) {
            var vm = ViewModel;
            if (vm != null) {
                vm.IsOpen = true;
            }
        }

        private void SetUserPicture() {
            if (_userPictureView?.IsInMonoLimbo() ?? true) {
                return;
            }

            var user = ViewModel?.User;

            var imagePath = "android.resource/" + (user != null && user.IsTeacher
                ? Resource.Drawable.teacher_placeholder
                : Resource.Drawable.student_placeholder);

            _userPictureView.ImagePath = ViewModel?.User?.Picture?.ToString() ?? imagePath;
            _userPictureView.ErrorPlaceholderImagePath = imagePath;
            _userPictureView.LoadingPlaceholderImagePath = imagePath;
            _userPictureView.Reload();

            // Gravatar returns a transparent image, so we have to set the placeholder on the view behind too
            ImageService.Instance
                .LoadCompiledResource(imagePath)
                .Transform(new CircleTransformation())
                .Into(_userPicturePlaceholderView);
        }

        private async void DrawerSyncButtonOnLongClick(object sender, EventArgs e) {
            await Mvx.IoCProvider.Resolve<INavigationService>().Navigate<SyncSettingsViewModel>();
        }

        private void NavigationItemsViewOnSelectedItemClick(object sender, EventArgs e) {
            _drawerLayout.CloseDrawer(_drawer);
        }

        private void NavigationItemsViewOnSelectionChanged(object sender, DrawerNavigationItemsView.SelectionChangedEventArgs e) {
            var currentFragment = SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame);

            if (currentFragment?.GetType().GetProperty(nameof(IMvxFragmentView.ViewModel), e.SelectedItem.ViewModelType) == null) {
                return;
            }

            var navigationService = Mvx.IoCProvider.Resolve<INavigationService>();

            async void NavigateOnIdleHandler(object a, ActionBarDrawerStateChangeEventArgs b) {
                if (b.NewState != DrawerLayout.StateIdle) return;

                _drawerToggle.DrawerStateChanged -= NavigateOnIdleHandler;

                await navigationService.Navigate(e.SelectedItem.ViewModelType);
            }

            _drawerToggle.DrawerStateChanged += NavigateOnIdleHandler;

            _drawerLayout.CloseDrawer(_drawer);
        }

        protected override void OnPostCreate(Bundle savedInstanceState) {
            base.OnPostCreate(savedInstanceState);

            _drawerToggle.SyncState();

            if (savedInstanceState == null) return;

            RestoreViewModelsFromBundle(savedInstanceState);
        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig) {
            base.OnConfigurationChanged(newConfig);

            _drawerToggle.OnConfigurationChanged(newConfig);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (_drawerToggle.OnOptionsItemSelected(item))
                return true;

            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed() {
            if (SupportFragmentManager.BackStackEntryCount == 1) {
                this.Finish();
            }
            else {
                InvalidateOptionsMenu();

                base.OnBackPressed();

                Mvx.IoCProvider.Resolve<INavigationService>().BackStack.Pop();

                var currentFragmentViewModelType = GetCurrentFragment()?.FindAssociatedViewModelTypeOrNull();

                if (currentFragmentViewModelType != null) {
                    _drawerNavigationItemsView?.SetSelectedIndexFromViewModel(currentFragmentViewModelType);
                }
            }
        }

        public override void OnActionModeStarted(ActionMode mode) {
            base.OnActionModeStarted(mode);

            var currentFragment = SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame) as BaseFragment;
            currentFragment?.OnActionModeStarted(mode);
        }

        public override void OnActionModeFinished(ActionMode mode) {
            base.OnActionModeFinished(mode);

            var currentFragment = SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame) as BaseFragment;
            currentFragment?.OnActionModeFinished(mode);
        }

        private const string SavedFragmentStateKey = "__mvxSavedFragmentState_";
        private const string SavedFragmentTypeKey = "__mvxSavedFragmentType_";
        private const string SavedCurrentFragmentKey = "__mvxSavedCurrentFragment";

        protected override void OnSaveInstanceState(Bundle outState) {
            var currentFragment = GetCurrentFragment();

            if (currentFragment != null) {
                if (Mvx.IoCProvider.TryResolve(out IMvxSavedStateConverter savedStateConverter)) {
                    var mvxBundle = currentFragment.CreateSaveStateBundle();
                    var bundle = new Bundle();
                    savedStateConverter.Write(bundle, mvxBundle);
                    outState.PutBundle(SavedFragmentStateKey + currentFragment.Tag, bundle);
                }

                outState.PutString(SavedFragmentTypeKey + currentFragment.Tag, currentFragment.FindAssociatedViewModelTypeOrNull().AssemblyQualifiedName);
                outState.PutString(SavedCurrentFragmentKey, currentFragment.Tag);
            }

            base.OnSaveInstanceState(outState);
        }

        private static void RestoreViewModelsFromBundle(Bundle savedInstanceState) {
            IMvxSavedStateConverter savedStateConverter;
            IMvxMultipleViewModelCache viewModelCache;
            IMvxViewModelLoader viewModelLoader;

            if (!Mvx.IoCProvider.TryResolve(out savedStateConverter)) {
                Mvx.IoCProvider.Resolve<ILogger<CoreView>>().LogTrace("Could not resolve IMvxSavedStateConverter, won't be able to convert saved state");
                return;
            }

            if (!Mvx.IoCProvider.TryResolve(out viewModelCache)) {
                Mvx.IoCProvider.Resolve<ILogger<CoreView>>().LogTrace("Could not resolve IMvxMultipleViewModelCache, won't be able to convert saved state");
                return;
            }

            if (!Mvx.IoCProvider.TryResolve(out viewModelLoader)) {
                Mvx.IoCProvider.Resolve<ILogger<CoreView>>().LogTrace("Could not resolve IMvxViewModelLoader, won't be able to load ViewModel for caching");
                return;
            }

            // Harder ressurection, just in case we were killed to death.
            var currentFragmentTag = savedInstanceState.GetString(SavedCurrentFragmentKey);

            var bundle = savedInstanceState.GetBundle(SavedFragmentStateKey + currentFragmentTag);
            if (bundle.IsEmpty) return;

            var viewModelType = Type.GetType(savedInstanceState.GetString(SavedFragmentTypeKey + currentFragmentTag));

            var mvxBundle = savedStateConverter.Read(bundle);
            var request = MvxViewModelRequest.GetDefaultRequest(viewModelType);

            // repopulate the ViewModel with the SavedState and cache it.
            var vm = viewModelLoader.LoadViewModel(request, mvxBundle);
            viewModelCache.Cache(vm);
        }

        // TODO: savedInstanceState like MvxCachingFragmentActivity
        public bool Show(MvxViewModelRequest request, Bundle bundle) {
            var viewModelType = request.ViewModelType;

            string tag = viewModelType.Name.Substring(0, viewModelType.Name.IndexOf("ViewModel", StringComparison.InvariantCulture));
            string javaName = "mystudylife.droid.fragments." + tag + "Fragment";

            var currentFragment = SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame);
            var currentFragmentTag = currentFragment?.Tag;

            if (currentFragmentTag == tag) {
                return true;
            }

            var fragment = Fragment.Instantiate(this, javaName, bundle);
            var ft = SupportFragmentManager.BeginTransaction();

            ft.Replace(Resource.Id.ContentFrame, fragment, tag);
            ft.AddToBackStack(tag);

            // Maybe the calendar fragments should be hosted inside a calendar fragment?
            if (tag.StartsWith("Calendar") && currentFragmentTag != null && currentFragmentTag.StartsWith("Calendar")) {
                SupportFragmentManager.PopBackStack();
            }

            ft.CommitAllowingStateLoss();

            InvalidateOptionsMenu();

            SupportFragmentManager.ExecutePendingTransactions();

            _drawerNavigationItemsView?.SetSelectedIndexFromViewModel(viewModelType);

            return true;
        }

        public override void InvalidateOptionsMenu() {
            base.InvalidateOptionsMenu();

            // Reset color for views who set it (eg. tasks subject filter)
            if (_toolbar != null) {
                _toolbar.SetBackgroundColor(_colorPrimary);
                StatusBarColor = _colorPrimary;
            }
        }

        private BaseFragment GetCurrentFragment() => SupportFragmentManager.FindFragmentById(Resource.Id.ContentFrame) as BaseFragment;

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _vmListener?.Dispose();
                _vmListener = null;
            }

            base.Dispose(disposing);
        }
    }

    [Register("mystudylife.droid.views.DrawerNavigationItemsView")]
    class DrawerNavigationItemsView : LinearLayout {
        private static readonly NavigationItem[] NavigationItems = {
            NavigationItem.ForViewModel<DashboardViewModel>(Resource.String.Views_Dashboard, Glyph.Dashboard),
            NavigationItem.ForViewModel<CalendarWeekViewModel>(Resource.String.Views_Calendar, Glyph.Calendar),
            NavigationItem.ForViewModel<TasksViewModel>(Resource.String.Views_Tasks, Glyph.Task),
            NavigationItem.ForViewModel<ExamsViewModel>(Resource.String.Views_Exams, Glyph.Exam),
            NavigationItem.ForViewModel<ScheduleViewModel>(Resource.String.Schedule, Glyph.Schedule),
            NavigationItem.ForViewModel<SettingsViewModel>(Resource.String.Views_Settings, Glyph.Settings),
        };

        public event EventHandler SelectedItemClick;
        public event EventHandler<SelectionChangedEventArgs> SelectionChanged;

        private readonly Color _itemColor;
        private readonly Color _itemColorSelected;

        private int _selectedIndex;

        public int SelectedIndex {
            get { return _selectedIndex; }
            set {
                if (_selectedIndex != value) {
                    _selectedIndex = value;
                    UpdateSelectedState();
                }
            }
        }

        protected DrawerNavigationItemsView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }
        public DrawerNavigationItemsView(Context context) : this(context, null) { }
        public DrawerNavigationItemsView(Context context, IAttributeSet attrs) : this(context, attrs, 0) { }
        public DrawerNavigationItemsView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            _itemColor = ContextCompatEx.GetColor(context, Resource.Color.foreground_mid);

            var outValue = new TypedValue();
            context.Theme.ResolveAttribute(Resource.Attribute.colorPrimary, outValue, true);
            _itemColorSelected = new Color(outValue.Data);

            var layoutInflater = LayoutInflater.FromContext(context);

            for (int i = 0; i < NavigationItems.Length; i++) {
                var navItem = NavigationItems[i];

                var view = layoutInflater.Inflate(Resource.Layout.ListItem_NavigationDrawer, this, false);

                var iconView = view.FindViewById<Icon>(Resource.Id.DrawerItemIcon);
                var textView = view.FindViewById<RobotoTextView>(Resource.Id.DrawerItemText);

                iconView.Glyph = navItem.Glyph;
                textView.SetText(navItem.TextResourceId);

                UpdateSelectedState(iconView, textView, i == SelectedIndex);

                int pos = i;
                view.Click += (s, e) => SetSelectedIndex(pos);

                this.AddView(view);
            }
        }

        public void SetSelectedIndexFromViewModel(Type viewModelType) {
            // This is horrid!
            if (viewModelType == typeof(CalendarMonthViewModel)) {
                viewModelType = typeof(CalendarWeekViewModel);
            }

            var index = NavigationItems.FirstIndexOf(x => x.ViewModelType == viewModelType);

            if (index == -1) {
                throw new NotSupportedException($"View model type '{viewModelType.FullName}' not found in drawer navigation items.");
            }

            this.SelectedIndex = index;
        }

        private void SetSelectedIndex(int index) {
            if (_selectedIndex == index) {
                SelectedItemClick?.Invoke(this, EventArgs.Empty);
            }
            else {
                // Updates state
                this.SelectedIndex = index;

                SelectionChanged?.Invoke(this, new SelectionChangedEventArgs(index, NavigationItems[index]));
            }
        }

        private void UpdateSelectedState() {
            for (int i = 0; i < this.ChildCount; i++) {
                var view = this.GetChildAt(i);

                var iconView = view.FindViewById<Icon>(Resource.Id.DrawerItemIcon);
                var textView = view.FindViewById<RobotoTextView>(Resource.Id.DrawerItemText);

                UpdateSelectedState(iconView, textView, i == SelectedIndex);
            }
        }

        private void UpdateSelectedState(Icon iconView, RobotoTextView textView, bool selected) {
            Color textColor = selected ? _itemColorSelected : _itemColor;

            iconView.SetTextColor(textColor);
            textView.SetTextColor(textColor);
            textView.RobotoTypeface = selected ? RobotoTypeface.Medium : RobotoTypeface.Regular;
        }

        public class SelectionChangedEventArgs : EventArgs {
            public int SelectedIndex { get; private set; }

            public NavigationItem SelectedItem { get; private set; }

            public SelectionChangedEventArgs(int selectedIndex, NavigationItem selectedItem) {
                this.SelectedIndex = selectedIndex;
                this.SelectedItem = selectedItem;
            }
        }
    }

    class NavigationItem {
        public int TextResourceId { get; private set; }

        public Glyph Glyph { get; private set; }

        public Type ViewModelType { get; private set; }

        private NavigationItem(int textResId, Glyph glyph, Type viewModelType) {
            this.TextResourceId = textResId;
            this.Glyph = glyph;
            this.ViewModelType = viewModelType;
        }

        public static NavigationItem ForViewModel<TViewModel>(int textResId, Glyph glyph) {
            return new NavigationItem(textResId, glyph, typeof(TViewModel));
        }
    }
}