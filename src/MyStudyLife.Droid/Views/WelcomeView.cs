using System;
using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using TimePickerDialog = Wdullaer.MaterialDateTimePicker.Time.TimePickerDialog;

namespace MyStudyLife.Droid.Views {
    [Activity(Label = "Hello")]
    public class WelcomeView : MvxActivity, TimePickerDialog.IOnTimeSetListener, NumberPicker.IOnValueChangeListener {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        public new WelcomeViewModel ViewModel => (WelcomeViewModel)base.ViewModel;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_Welcome);

            FindViewById(Resource.Id.DefaultStartTimePicker).Click += DefaultStartTimePickerOnClick;
            FindViewById(Resource.Id.DefaultDurationPicker).Click += DefaultDurationPickerOnClick;
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(() => ViewModel.IsSaving, () =>
                    // Posting to the UI thread should hopefully fix the Java.Lang.RuntimeException bug
                    // https://github.com/jamie94bc/mystudylife-native/issues/37
                    RunOnUiThread(() => TryHideOrShowProgressDialog(ViewModel.IsSaving))
                );
        }

        #region Progress Dialog

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

        private void TryHideOrShowProgressDialog(bool isBusy) {
            DialogHelper.ShowOrHideProgressDialog(this, _progressDialogReference, isBusy, R.Common.UpdatingWithEllipsis);
        }

        #endregion

        #region Picker Event Handlers

        private const string DefaultStartTimePickerFragmentTag = "default_start_time_picker";

        protected override void OnResume() {
            base.OnResume();

            var picker = (TimePickerDialog)SupportFragmentManager.FindFragmentByTag(DefaultStartTimePickerFragmentTag);

            if (picker != null) {
                picker.OnTimeSetListener = this;
            }
        }

        private void DefaultStartTimePickerOnClick(object sender, EventArgs e) {
            var defaultStartTime = ViewModel.Settings.DefaultStartTime;

            var picker = TimePickerDialog.NewInstance(this, defaultStartTime.Hours, defaultStartTime.Minutes, DateTimeFormat.Is24Hour);

            picker.Show(SupportFragmentManager, DefaultStartTimePickerFragmentTag);
        }

        private void DefaultDurationPickerOnClick(object sender, EventArgs e) {
            new NumberPickerDialogFragment(
                Resource.String.Settings_DefaultDuration,
                Data.Data.MinDuration,
                Data.Data.MaxDuration,
                ViewModel.Settings.DefaultDuration,
                this
            ).Show(SupportFragmentManager, "duration");
        }

        #endregion

        #region Implementation of IOnTimeSetListener, IOnValueChangeListener

        public void OnValueChange(NumberPicker picker, int oldVal, int newVal) {
            ViewModel.Settings.DefaultDuration = newVal;
        }

        public void OnTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            ViewModel.Settings.DefaultStartTime = new TimeSpan(hourOfDay, minute, 0);
        }

        #endregion
    }
}