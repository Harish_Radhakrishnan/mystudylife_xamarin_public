using System;
using Android.App;
using Android.Graphics;
using Android.OS;
using Google.Android.Material.AppBar;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Schools;

namespace MyStudyLife.Droid.Views.Schools {
    [Activity(Label = "Hello")]
    public class SchoolWelcomeView : MvxActivity<SchoolWelcomeViewModel> {
        [UsedImplicitly]
        private MvxPropertyChangedListener _listener;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            
            SetContentView(Resource.Layout.View_SchoolWelcome);

            var collapsingToolbar = FindViewById<CollapsingToolbarLayout>(Resource.Id.CollapsingToolbar);
            collapsingToolbar.SetCollapsedTitleTextColor(Color.White);
            collapsingToolbar.SetExpandedTitleColor(Color.White);
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.IsSaving), () =>
                    // Posting to the UI thread should hopefully fix the Java.Lang.RuntimeException bug
                    // https://github.com/mystudylife/mystudylife-native/issues/37
                    RunOnUiThread(() => TryHideOrShowProgressDialog(this.ViewModel.IsSaving))
                );
        }

        #region Progress Dialog

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

        private void TryHideOrShowProgressDialog(bool isBusy) {
            DialogHelper.ShowOrHideProgressDialog(this, _progressDialogReference, isBusy, R.Common.UpdatingWithEllipsis);
        }

        #endregion
    }
}