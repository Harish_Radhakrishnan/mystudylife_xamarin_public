using System;
using Android.App;
using Android.Content.Res;
using Android.OS;
using Android.Text;
using Android.Text.Style;
using Android.Views.Animations;
using Android.Widget;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels.View;
using AnderWeb.DiscreteSeekBar;

namespace MyStudyLife.Droid.Views {
	[Activity]
	public class TaskView : EntityView<Task, TaskViewModel> {
        private DiscreteSeekBar _seekBar;
        private TextView _progressTextView;
        private RichTextView _completedAtTextView;

        private RobotoTextView _dueDateCaptionTextView;

        private TextView[] _textViewsWithLinks;

        public override int LayoutResourceId => Resource.Layout.View_Task;

        protected override void OnCreate(Bundle bundle) {
            //var explode = new Android.Transitions.Explode();

            //explode.SetDuration(300);

            //Window.EnterTransition = explode;
            //Window.ExitTransition = explode;

            base.OnCreate(bundle);

	        _progressTextView = this.FindViewById<TextView>(Resource.Id.ProgressText);
	        _seekBar = this.FindViewById<DiscreteSeekBar>(Resource.Id.SeekBar);
            _seekBar.StartTrackingTouch += SeekBarOnStartTrackingTouch;
            _seekBar.StopTrackingTouch += SeekBarOnStopTrackingTouch;

	        _completedAtTextView = this.FindViewById<RichTextView>(Resource.Id.CompletedAtText);

            _dueDateCaptionTextView = this.FindViewById<RobotoTextView>(Resource.Id.DueDateCaptionText);

            var detailTextLonelyView = this.FindViewById<RichTextView>(Resource.Id.DetailTextLonely);

            detailTextLonelyView.SetText(Html.FromHtml(Resources.GetString(Resource.String.Task_DetailLonely)), TextView.BufferType.Spannable);

            detailTextLonelyView.LinkClick += LinkClick;
	        _completedAtTextView.LinkClick += LinkClick;


	        _textViewsWithLinks = new[] {
	            _completedAtTextView,
                this.FindViewById<TextView>(Resource.Id.DetailText),
                detailTextLonelyView
	        };

            this.SetCompletedRelativeText();
            this.ViewModelOnStateChange();
        }

	    protected override void SetColor(MaterialPalette palette) {
	        base.SetColor(palette);

            var seekBar = this._seekBar.SafeGet();

	        if (seekBar != null) {
                seekBar.SetThumbColor(palette.PrimaryColor, palette.PrimaryColor);
                seekBar.SetScrubberColor(palette.PrimaryColor);
                seekBar.SetTrackColor(palette.PrimaryColor);
            }

            if (_textViewsWithLinks != null) {
	            var states = new ColorStateList(
	                new [] {
	                    new [] { Android.Resource.Attribute.StateEnabled },
	                    new [] { Android.Resource.Attribute.StatePressed },
	                    new [] { Android.Resource.Attribute.StateFocused }
	                },
                    new int[] {
                        palette.PrimaryColor,
                        palette.PrimaryColorLight,
                        palette.PrimaryColorLight
                    }
	            );

	            foreach (var tv in _textViewsWithLinks) {
                    tv.SafeGet()?.SetLinkTextColor(states);
	            }
	        }
	    }

	    protected override void OnViewModelSet() {
	        base.OnViewModelSet();

            ViewModelListener
                .Listen(nameof(this.ViewModel.State), ViewModelOnStateChange)
                .Listen(nameof(this.ViewModel.CompletedRelativeText), SetCompletedRelativeText);
        }

        private void ViewModelOnStateChange() {
            var vm = this.ViewModel;
            var sub = this.SafeGet(x => x._dueDateCaptionTextView);

            if (vm != null && sub != null) {
                if (vm.State == TaskState.Overdue || vm.State == TaskState.IncompleteAndDueSoon) {
                    sub.SetRobotoTypeface(RobotoTypeface.Medium);
                }
                else {
                    sub.SetRobotoTypeface(RobotoTypeface.Regular);
                }
            }
        }

	    private void SetCompletedRelativeText() {
	        var text = this.ViewModel?.CompletedRelativeText;
            var textView = _completedAtTextView.SafeGet();

            if (textView != null) {
                if (String.IsNullOrWhiteSpace(text)) {
                    textView.Text = null;
                    return;
                }

                var format = Resources.GetString(Resource.String.Task_CompletedAtFormat);
                var formatted = String.Format(format, text);

                textView.SetText(Html.FromHtml(formatted), TextView.BufferType.Spannable);
	        }
	    }

	    private void LinkClick(object sender, LocalLinkEventArgs e) {
	        var link = e.ClickedSpans[0] as URLSpan;

	        if (link == null) return;

	        switch (link.URL) {
	            case "EDIT_TASK":
	                this.ViewModel.EditCommand.Execute(null);
	                break;
	            case "SET_INCOMPLETE":
	                this.ViewModel.SetIncompleteCommand.Execute(null);
	                break;
	        }

	        e.Handled = true;
	    }

        private void SeekBarOnStartTrackingTouch(object sender, DiscreteSeekBar.StartTrackingTouchEventArgs e) {
            _progressTextView.ClearAnimation();
            _progressTextView.StartAnimation(GetProgressTextAnimation(1, 0));
        }

        private void SeekBarOnStopTrackingTouch(object sender, DiscreteSeekBar.StopTrackingTouchEventArgs e) {
            _progressTextView.ClearAnimation();
            _progressTextView.StartAnimation(GetProgressTextAnimation(0, 1));

            // null checking required to prevent edge case null reference (MSL-269)
            var item = this.ViewModel?.Item;
            if (item != null) {
                item.Progress = e.SeekBar.Progress;
            }
        }

        private Animation GetProgressTextAnimation(float fromAlpha, float toAlpha) {
            return new AlphaAnimation(fromAlpha, toAlpha) {
                Duration = AnimationHelper.DurationMedium,
                Interpolator = AnimationHelper.GetFastOutSlowInInterpolator(this),
                FillAfter = true
            };
	    }
    }
}