using Android.Views;
using Android.Widget;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Base;
using System;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;
using Android.OS;
using MvvmCross.ViewModels;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;

namespace MyStudyLife.Droid.Views {
    public abstract class BaseNonCoreView<TViewModel> : MvxActivity<TViewModel> where TViewModel : BaseViewModel {

        [UsedImplicitly]
        private MvxPropertyChangedListener _listener;

        private bool _contentViewSet;
        private Toolbar _toolbar;

        protected MvxPropertyChangedListener ViewModelListener {
            get {
                if (_listener == null) {
                    throw new InvalidOperationException(
                        "ViewModelListener cannot be used before OnViewModelSet() is called. " +
                        "Did you call base.OnViewModelSet() before accessing this property?"
                    );
                }

                return _listener;
            }
        }

        protected virtual bool ContentViewSet => _contentViewSet || FindViewById(Resource.Id.ContentFrame) != null;

        protected Toolbar Toolbar {
            get { return _toolbar ??= _contentViewSet ? FindViewById<Toolbar>(Resource.Id.Toolbar) : null; }
        }

        public override void SetContentView(int layoutResId) {
            base.SetContentView(Resource.Layout.View_Base);

            var contentFrame = FindViewById<FrameLayout>(Resource.Id.ContentFrame);
            this.BindingInflate(layoutResId, contentFrame, true);

            SetupToolbar();

            _contentViewSet = true;
        }

        protected void SetContentViewNoWrap(int layoutResId) {
            base.SetContentView(layoutResId);

            SetupToolbar();

            _contentViewSet = true;
        }

        private void SetupToolbar() {
            _toolbar = FindViewById<Toolbar>(Resource.Id.Toolbar);

            if (_toolbar != null) {
                // Workaround for all screens but CoreView having dark text on
                // the toolbar despite manually setting theme / same resource
                _toolbar.SetTitleTextColor(Android.Graphics.Color.White);

                SetSupportActionBar(_toolbar);

                _toolbar.EnsureConsistentStyle();

                OnSetActionBar(SupportActionBar);
            }

        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            if (item.ItemId == Android.Resource.Id.Home) {
                ViewModel.GoBack();

                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        protected virtual void OnSetActionBar(ActionBar ab) {
            ab.SetHomeButtonEnabled(true);
            ab.SetDisplayHomeAsUpEnabled(true);
            ab.SetHomeAsUpIndicator(Resource.Drawable.ic_action_previous);
            ab.SetDisplayShowTitleEnabled(true);
        }

        public override void OnBackPressed() {
            Mvx.IoCProvider.Resolve<INavigationService>().Close(ViewModel);
        }

        private bool _onPostedCreateCalled;
        private bool _callViewModelOnLoadedOnPostCreate;

        protected override void OnPostCreate(Bundle savedInstanceState) {
            base.OnPostCreate(savedInstanceState);

            _onPostedCreateCalled = true;

            // Candidate fix for MSL-273, ViewModel should always not be null and IsLoaded
            // should always be true if _callViewModelOnLoadedOnPostCreate is, but doesn't
            // always seem to be the case
            if (_callViewModelOnLoadedOnPostCreate && (ViewModel?.IsLoaded ?? false)) {
                ViewModelOnLoaded();
            }
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(ViewModel);

            var loadedVm = ViewModel as ILoaded;

            if (loadedVm != null) {
                if (loadedVm.IsLoaded) {
                    ViewModelOnLoadedCore();
                }
                else {
                    _listener.Listen(nameof(ILoaded.IsLoaded), () => {
                        if (loadedVm.IsLoaded) {
                            ViewModelOnLoadedCore();
                        }
                    });
                }
            }
            else {
                ViewModelOnLoadedCore();
            }
        }

        private void ViewModelOnLoadedCore() {
            if (_onPostedCreateCalled) {
                ViewModelOnLoaded();
            }
            else {
                _callViewModelOnLoadedOnPostCreate = true;
            }
        }

        /// <summary>
        ///     Called when the view model is loaded if it implements <see cref="ILoaded"/>,
        ///     otherwise called when set.
        /// </summary>
        protected virtual void ViewModelOnLoaded() { }
    }
}