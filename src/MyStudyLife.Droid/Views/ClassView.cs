using System;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.View;
using System.Collections.Generic;
using Android.Widget;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Droid.Extensions;
using MyStudyLife.Droid.Widgets.ObservableScrollView;
using AndroidX.ViewPager.Widget;
using MvvmCross.Platforms.Android.Views.Fragments;
using Google.Android.Material.Tabs;

namespace MyStudyLife.Droid.Views {
    [Activity(Label = "Class")]
    public class ClassView : EntityView<Class, ClassViewModel> {
        public override int LayoutResourceId => Resource.Layout.View_Class;

        private WeakReference<IMenu> _menuReference;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            //this.FindViewById(Resource.Id.ClassViewStudentsButton).Click += OnClick;

            this.Invalidate();
        }

        private void OnClick(object sender, EventArgs e) {
            // TODO: Add loading indicator in StudentsDialogFragment
            if (this.ViewModel.StudentsIsBusy) {
                return;
            }

            if (this.ViewModel.StudentsHasErrored) {
                this.ViewModel.SetStudentsCommand.Execute(null);
            }
            else {
                new StudentsDialogFragment(this).Show(SupportFragmentManager, "class-students");
            }
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener
                .Listen(() => this.ViewModel.IsReadOnly, Invalidate)
                .Listen(() => this.ViewModel.CanLeave, Invalidate)
                .Listen(() => this.ViewModel.IsLeaving, OnIsLeavingChanged);
        }

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

        private void OnIsLeavingChanged() {
            var isBusy = this.ViewModel.IsLeaving;

            RunOnUiThread(() => TryHideOrShowProgressDialog(isBusy));
        }

        private void TryHideOrShowProgressDialog(bool isBusy) {
            DialogHelper.ShowOrHideProgressDialog(this, _progressDialogReference, isBusy, R.ClassInput.LeavingWithEllipsis);
        }

        protected override void ViewModelOnLoaded(bool overrideAnimation) {
            if (this.ViewModel.Item == null) {
                throw new InvalidOperationException($"{nameof(ViewModelOnLoaded)} should not be called when ViewModel.Item is null");
            }

            // Hide the tabs when Item is not yet loaded
            this.TabsVisible = this.ViewModel.IsScheduled && !this.ViewModel.Item.IsOneOff;

            var viewPager = this.FindViewById<ViewPager>(Resource.Id.Pager);

            var pages = new List<MvxFixedPagerAdapter.Page>(2);
            bool addSchedulePage = false;

            if (this.ViewModel.IsScheduled) {
                pages.Add(new MvxFixedPagerAdapter.Page(this, Resource.String.Current, Resource.Layout.View_Class_Current));

                if (!this.ViewModel.Item.IsOneOff) {
                    addSchedulePage = true;
                }
            }
            else {
                addSchedulePage = true;
            }

            if (addSchedulePage) {
                pages.Add(new MvxFixedPagerAdapter.Page(this, Resource.String.Schedule, Resource.Layout.View_Class_Schedule));
            }

            var adapter = new MvxFixedPagerAdapter(
                this,
                (IMvxAndroidBindingContext)this.BindingContext,
                pages.ToArray()
            );
            adapter.ItemInstantiated += (s, e) => {
                AttachScrollView(e.View.FindViewById<ObservableScrollView>(Resource.Id.RootScroll));
                FlexibleSpaceOnHeightChanged(FlexibleSpaceHeight);
            };

            viewPager.Adapter = adapter;

            var slidingTabs = this.FindViewById<TabLayout>(Resource.Id.Tabs);
            slidingTabs.SetupWithViewPager(viewPager);

            viewPager.PageSelected += ViewPagerOnPageSelected;

            Invalidate();

            base.ViewModelOnLoaded(overrideAnimation);
        }

        private void ViewPagerOnPageSelected(object sender, ViewPager.PageSelectedEventArgs e) {
            UpdateFlexibleSpace(((ViewPager)sender).GetChildAt(e.Position).ScrollY);
        }

        protected override void FlexibleSpaceOnHeightChanged(int newHeight) {
            var viewPager = this.FindViewById<ViewPager>(Resource.Id.Pager);

            for (int i = 0; i < viewPager.ChildCount; i++) {
                var childView = viewPager.GetChildAt(i);

                ((ScrollView)childView).GetChildAt(0).SetPaddingTop(newHeight);
            }
        }

        private void Invalidate() {
            IMenu menu;
            if (_menuReference != null && _menuReference.TryGetTarget(out menu) && this.ViewModel != null && this.ViewModel.IsLoaded) {
                menu.FindItem(Resource.Id.ActionEdit).SetVisible(!this.ViewModel.IsReadOnly);
                menu.FindItem(Resource.Id.ActionLeave).SetVisible(this.ViewModel.CanLeave);
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.ClassView, menu);

            _menuReference = new WeakReference<IMenu>(menu);

            this.Invalidate();

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.ActionLeave:
                    this.ViewModel.LeaveWithConfirmationCommand.Execute(null);
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public class StudentsDialogFragment : MvxDialogFragment {
            private readonly Context _context;

            public StudentsDialogFragment(ClassView view) {
                _context = view;

                this.DataContext = view.ViewModel;
            }

            public override Dialog OnCreateDialog(Bundle savedInstanceState) {
                this.EnsureBindingContextIsSet(LayoutInflater);

                var view = this.BindingInflate(Resource.Layout.Dialog_ClassStudents, null);

                return new MaterialDialog.Builder(_context)
                    .Title(Resource.String.Students)
                    .CustomView(view, false)
                    .Build();
            }
        }
    }
}