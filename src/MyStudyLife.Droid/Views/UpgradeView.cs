using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Text;
using Android.Text.Style;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.Droid.Views {
	[Activity(
        Label = "Data Upgrade",
        ScreenOrientation = ScreenOrientation.Portrait,
        NoHistory = true
    )]
	public class UpgradeView : MvxActivity<UpgradeViewModel> {
	    [UsedImplicitly] private MvxPropertyChangedListener _listener;
        
		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_Upgrade);

            var errorMessageTextView = this.FindViewById<RichTextView>(Resource.Id.ErrorMessageText);
            errorMessageTextView.SetText(Html.FromHtml(Resources.GetString(Resource.String.DataUpgrade_ErrorMessage)), TextView.BufferType.Spannable);

            errorMessageTextView.LinkClick += (s, e) => {
                if ((e.ClickedSpans[0] as URLSpan)?.URL == "WEB_APP") {
                    this.ViewModel.OpenWebAppCommand.Execute(null);

                    e.Handled = true;
                }
            };
        }

	    protected override void OnStart() {
	        base.OnStart();

            TryHideOrShowProgressDialog(this.ViewModel?.IsUpgrading ?? false);
        }

	    protected override void OnViewModelSet() {
			base.OnViewModelSet();

	        _listener = new MvxPropertyChangedListener(this.ViewModel)
	            .Listen(nameof(this.ViewModel.IsUpgrading), OnUpgradingChanged);

            TryHideOrShowProgressDialog(this.ViewModel.IsUpgrading);
        }

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);
        
	    private void OnUpgradingChanged() {
	        RunOnUiThread(() => TryHideOrShowProgressDialog(this.ViewModel.IsUpgrading));
	    }

        private void TryHideOrShowProgressDialog(bool isBusy) {
            DialogHelper.ShowOrHideProgressDialog(this, _progressDialogReference, isBusy, R.Common.UpdatingWithEllipsis);
        }
	}
}