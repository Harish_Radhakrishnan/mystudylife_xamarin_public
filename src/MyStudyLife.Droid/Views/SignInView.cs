using System;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Text;
using Android.Text.Style;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.ViewModels;
using MvvmCross;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using static AFollestad.MaterialDialogs.MaterialDialog;
using Keycode = Android.Views.Keycode;
using Microsoft.Extensions.Logging;
using MvvmCross.Platforms.Android.Views;
using Google.Android.Material.TextField;

namespace MyStudyLife.Droid.Views {
    [Activity(
        Label = "Sign In",
        Theme = "@style/MyStudyLife.Theme.SignIn",
        ScreenOrientation = ScreenOrientation.Portrait,
        WindowSoftInputMode = SoftInput.StateHidden | SoftInput.AdjustResize,
        LaunchMode = LaunchMode.SingleTop
    )]
	public class SignInView : MvxActivity<SignInViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        private ScrollView _rootView;
        private View _oAuthOptionsView;
        private View _credentialsSignInView;
        private RichTextView _findOutMoreView;
        private RichTextView _legalTextView;
        private View _extrasView;
        private TextInputEditText _emailInput;
        private InputMethodManager _imm;

        protected override void OnNewIntent(Intent intent) {
            base.OnNewIntent(intent);

            var extraData = intent.Extras?.GetString("MvxLaunchData");
            if (extraData == null) {
                return;
            }

            var converter = Mvx.IoCProvider.Resolve<IMvxNavigationSerializer>();
            var viewModelRequest = converter.Serializer.DeserializeObject<MvxViewModelRequest>(extraData);

            Mvx.IoCProvider.Resolve<IMvxViewModelLoader>().ReloadViewModel(ViewModel, viewModelRequest, null);
        }

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            _imm = ((InputMethodManager)GetSystemService(InputMethodService));

            SetContentView(Resource.Layout.View_SignIn);

            _credentialsSignInView = FindViewById<LinearLayout>(Resource.Id.CredentialsSignIn);
            _emailInput = _credentialsSignInView.FindViewById<TextInputEditText>(Resource.Id.Email);

            var passwordInput = _credentialsSignInView.FindViewById<TextInputEditText>(Resource.Id.Password);
            var passwordContainer = (TextInputLayout)passwordInput.Parent.Parent;

            passwordContainer.Typeface = Typeface.CreateFromAsset(Assets, "fonts/Roboto-Regular.ttf");

            passwordInput.SetTypeface(Typeface.Default, TypefaceStyle.Normal);
            passwordInput.SetImeActionLabel(Resources.GetString(Resource.String.SignIn), ImeAction.Done);
            passwordInput.EditorAction += PasswordEditTextOnEditorAction;
            
            FindViewById(Resource.Id.SignInLogoLayout).Click += HideKeyboardOnClick;

            _legalTextView = FindViewById<RichTextView>(Resource.Id.LegalText);
            _legalTextView.LinkClick += OnLinkClick;
            _legalTextView.SetText(Html.FromHtml(Resources.GetString(Resource.String.SignIn_Legal)), TextView.BufferType.Spannable);

            _findOutMoreView = FindViewById<RichTextView>(Resource.Id.FindOutMore);
            _findOutMoreView.LinkClick += OnLinkClick;
            _findOutMoreView.SetText(Html.FromHtml(Resources.GetString(Resource.String.SignIn_FindOutMore)), TextView.BufferType.Spannable);

            var forgottenPasswordTextView = FindViewById<TextView>(Resource.Id.ForgottenPassword);
            forgottenPasswordTextView.PaintFlags |= PaintFlags.UnderlineText;

            _rootView = FindViewById<ScrollView>(Resource.Id.RootScroll);
            _oAuthOptionsView = FindViewById<LinearLayout>(Resource.Id.OAuthOptions);
            _extrasView = FindViewById<View>(Resource.Id.ExtrasView);

            _rootView.ViewTreeObserver.GlobalLayout += ViewTreeObserverOnGlobalLayout;
            _rootView.LayoutChange += ContainerViewOnLayoutChange;
        }

        private void ViewTreeObserverOnGlobalLayout(object sender, EventArgs e) {
            Rect r = new Rect();
            _rootView.GetWindowVisibleDisplayFrame(r);

            int rootViewTop = r.Top;
            int rootViewDisplayedHeight = (r.Bottom - rootViewTop);
            int screenHeight = _rootView.RootView.Height;
            int heightDifference = screenHeight - rootViewDisplayedHeight;

            // We're assuming keyboards will always take up > 1/4 of the screen
            if (heightDifference > (screenHeight / 4f) && _emailInput.IsFocused) {
                // We want the entire credentials block to be visible (input fields and sign in button)
                int credentialsSignInBottom = (_credentialsSignInView.Bottom - _credentialsSignInView.Top) + _credentialsSignInView.GetTopTo(_rootView);
                int emailInputTop = _emailInput.GetTopTo(_rootView);

                int scrollY = credentialsSignInBottom - _rootView.Height;

                // If it won't all fit in the screen, ensure email input is fully visible
                if (emailInputTop < scrollY) {
                    scrollY = emailInputTop;
                }

                _rootView.SmoothScrollTo(0, Math.Max(0, scrollY));
            }
        }

        private void ContainerViewOnLayoutChange(object sender, View.LayoutChangeEventArgs e) {
            if (ViewModel?.ShowEmailSignIn ?? false) {
                _oAuthOptionsView.SetX(-_rootView.Width);
                _credentialsSignInView.SetX(0);
            }
            else {
                _oAuthOptionsView.SetX(0);
                _credentialsSignInView.SetX(_rootView.Width);
            }

            _rootView.LayoutChange -= ContainerViewOnLayoutChange;
        }

        protected override void OnPostCreate(Bundle savedInstanceState) {
            base.OnPostCreate(savedInstanceState);

            OnSigningInChanged();
            OnErrorMessageChanged();
        }

        protected override void OnViewModelSet() {
			base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(nameof(ViewModel.SigningIn), OnSigningInChanged)
                .Listen(nameof(ViewModel.ErrorMessage), OnErrorMessageChanged)
                .Listen(nameof(ViewModel.ShowEmailSignIn), OnShowEmailSignInChanged);
        }

        private readonly WeakReference<Dialog> _progressDialogReference = new WeakReference<Dialog>(null);

		private void OnSigningInChanged() {
            if (this.IsInMonoLimbo()) {
                return;
            }

            DialogHelper.ShowOrHideProgressDialog(
                this, _progressDialogReference,
                ViewModel.SigningIn,
                Resource.String.SigningInWithEllipsis
            );
		}

        private MaterialDialog _dialog;

        private void OnErrorMessageChanged() {
            if (this.IsInMonoLimbo()) {
                return;
            }

            try {
                _dialog?.Cancel();

                if (ViewModel.IsOnline && !String.IsNullOrEmpty(ViewModel.ErrorMessage)) {
                    _dialog = new Builder(this)
                        .Title(Resource.String.SignInError)
                        .Content(ViewModel.ErrorMessage)
                        .PositiveText(Resource.String.Common_Ok)
                        .Show();
                }
            }
            // Occurs when the dialog is shown before the activity is visible, caused by a property change.
            // This should be handled by calling this method in OnCreate, so we've swallowed the exception.
            catch (DialogException ex) when (ex.Message.Contains("Bad window token", StringComparison.OrdinalIgnoreCase)) {                
                Mvx.IoCProvider.Resolve<ILogger<SignInView>>().LogWarning("SignInView: Swallowed bad window token exception during OnErrorMessageChanged");
            }
        }

        private void HideKeyboardOnClick(object sender, EventArgs e) {
            var currentFocus = CurrentFocus;
            if (currentFocus != null) {
                InputMethodManager.FromContext(this).HideSoftInputFromWindow(
                    currentFocus.WindowToken,
                    HideSoftInputFlags.None
                );
            }
        }

        private void PasswordEditTextOnEditorAction(object sender, TextView.EditorActionEventArgs e) {
            var cmd = ViewModel.MslSignInCommand;

            if ((e.ActionId == ImeAction.Done || (e.Event != null && e.Event.KeyCode == Keycode.Enter && e.Event.Action == KeyEventActions.Down)) && cmd.CanExecute(null)) {
                cmd.Execute(null);

                e.Handled = true;
            }
        }

        public override void OnBackPressed() {
            if (ViewModel?.ShowEmailSignIn == true) {
                ViewModel.ShowEmailSignIn = false;
            }
            else {
                base.OnBackPressed();
            }
        }

        private void OnLinkClick(object sender, LocalLinkEventArgs e) {
            var link = e.ClickedSpans[0] as URLSpan;

            switch (link?.URL) {
                case "FIND_OUT_MORE":
                    ViewModel.FindOutMoreCommand.Execute(null);
                    e.Handled = true;
                    break;
                case "PRIVACY_POLICY":
                    ViewModel.PrivacyPolicyCommand.Execute(null);
                    e.Handled = true;
                    break;
                case "TERMS_OF_SERVICE":
                    ViewModel.TermsOfServiceCommand.Execute(null);
                    e.Handled = true;
                    break;
            }
        }

        private void OnShowEmailSignInChanged() {
            var showEmailSignIn = ViewModel?.ShowEmailSignIn ?? false;

            var distance = showEmailSignIn ? -_rootView.Width : _rootView.Width;
            var duration = AnimationHelper.DurationMedium;

            _extrasView.Animate()
                       .Alpha(showEmailSignIn ? 0 : 1)
                       .SetDuration(duration);

            _oAuthOptionsView.Animate()
                 .TranslationXBy(distance)
                 .SetDuration(duration)
                 .SetInterpolator(AnimationHelper.GetFastOutSlowInInterpolator(this));
            _credentialsSignInView.Animate()
                  .TranslationXBy(distance)
                  .SetDuration(duration)
                  .SetInterpolator(AnimationHelper.GetFastOutSlowInInterpolator(this));

            if (showEmailSignIn) {
                _emailInput.RequestFocus();
                _imm.ToggleSoftInputFromWindow(_rootView.WindowToken, 0, 0);
            }
            else {
                _emailInput.ClearFocus();
                _imm.HideSoftInputFromWindow(_rootView.WindowToken, 0);
            }
        }
    }
}