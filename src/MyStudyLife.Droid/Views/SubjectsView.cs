using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.Base;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Data;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using ActionBar = AndroidX.AppCompat.App.ActionBar;

namespace MyStudyLife.Droid.Views {
    [Activity(Label = "Subjects")]
    public class SubjectsView : BaseNonCoreView<SubjectsViewModel>, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener {
        private Toast _errorToast;

        protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_Subjects);
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            this.ViewModel.EditErrorInteraction.Requested += EditErrorInteractionOnRequested;
        }

        private void EditErrorInteractionOnRequested(object sender, MvxValueEventArgs<string> e) {
            _errorToast?.Cancel();
            _errorToast = Toast.MakeText(this, e.Value, ToastLength.Long);
            _errorToast.Show();
        }

	    protected override void OnSetActionBar(ActionBar ab) {
			base.OnSetActionBar(ab);

            ab.SetDisplayShowTitleEnabled(false);

            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);
            var subtitleView = v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle);

            v.Click += ActionBarTitleOnClick;

            v.FindViewById<TextView>(Resource.Id.ActionBarTitle).SetText(Resource.String.Views_Subjects);

            var set = this.CreateBindingSet<SubjectsView, SubjectsViewModel>();

            set.Bind(subtitleView)
                .For(x => x.Text)
                .To(x => x.SelectedScheduleText);
            set.Bind(subtitleView)
                .For(x => x.Visibility)
                .To(x => x.SelectedScheduleText)
                .WithConversion("Visibility");

            set.Apply();

            ab.SetCustomView(v, new ActionBar.LayoutParams((int)(GravityFlags.Left | GravityFlags.CenterVertical)));
            ab.SetDisplayShowCustomEnabled(true);
		}

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            new AcademicSchedulePickerDialogFragment(
                this,
                this,
                this.ViewModel.AcademicYears,
                this.ViewModel.SelectedSchedule,
                R.SubjectInput.AcademicScheduleListHelp
            ) {
                TitleResId = Resource.String.FilterByYearOrTerm,
                ShowNew = false
            }.Show(this.SupportFragmentManager, "schedule-selector");
        }

	    public void OnScheduleSet(IAcademicSchedule schedule) {
	        if (this.ViewModel != null) {
	            this.ViewModel.SelectedSchedule = schedule;
	        }
	    }
	}
}