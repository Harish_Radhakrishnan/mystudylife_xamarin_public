using System;
using Android.App;
using Android.OS;
using Android.Widget;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
    [Activity(Label = "Exam Input")]
    public class ExamInputView : SubjectDependentInputView<Exam, ExamInputViewModel>, NumberPicker.IOnValueChangeListener {
        private MslCheckBox _resitCheckBox;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            this.SetContentView(Resource.Layout.View_ExamInput);

            this.FindViewById(Resource.Id.ExamDurationText).Click += ExamDurationTextOnClick;

            this.FindViewById(Resource.Id.ResitLabel).Click += ResitLabelOnClick;

            _resitCheckBox = this.FindViewById<MslCheckBox>(Resource.Id.ResitCheckBox);
        }

        private void ExamDurationTextOnClick(object sender, EventArgs e) {
            new NumberPickerDialogFragment(
                Resource.String.Common_Duration,
                Data.Data.MinDuration,
                Data.Data.MaxDuration,
                this.ViewModel.InputItem.Duration,
                this
            ).Show(this.SupportFragmentManager, "duration");
        }

        private void ResitLabelOnClick(object sender, EventArgs e) {
            this.ViewModel.InputItem.Resit = !this.ViewModel.InputItem.Resit;
        }

        protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewExam : R.EditExam;
        }

        protected override void SetColor(MaterialPalette palette) {
            base.SetColor(palette);

            if (_resitCheckBox != null) {
                _resitCheckBox.CheckedTintColor = palette.PrimaryColor;
            }
        }

        #region Implementation of IOnValueChangeListener

        public void OnValueChange(NumberPicker picker, int oldVal, int newVal) {
            this.ViewModel.InputItem.Duration = newVal;
        }

        #endregion
    }
}