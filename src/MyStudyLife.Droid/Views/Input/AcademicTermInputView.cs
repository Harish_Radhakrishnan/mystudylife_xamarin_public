using Android.App;
using Android.OS;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
    [Activity(Label = "Academic Term Input")]
    public class AcademicTermInputView : InputView<AcademicTermInputViewModel> {
		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_AcademicTermInput);
        }

	    protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewAcademicTerm : R.EditAcademicTerm;
        }
	}
}