using System;
using System.ComponentModel;
using System.Windows.Input;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Binding.Views;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.WeakSubscription;
using AndroidX.Core.Graphics.Drawable;
using AndroidX.Core.Content;

namespace MyStudyLife.Droid.Views.Input {
	[Activity(Label = "Class Input")]
    public class ClassInputView : SubjectDependentInputView<Class, ClassInputViewModel>, ColorPickerDialogFragment.IOnColorSetListener {
        private View _colorIndicator;
	    private Button _colorButton;
	    private Button _newTimeButton;
	    private MslSwitch _startEndDatesSwitch;

		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_ClassInput);

            // For some reason the XML attributes don't work
		    int margin = Resources.GetDimensionPixelSize(Resource.Dimension.margin);

            FindViewById<EditText>(Resource.Id.ModuleTextField).SetPadding(margin, 0, margin, 0);
            
            using (new MvxBindingContextStackRegistration<IMvxAndroidBindingContext>((IMvxAndroidBindingContext)BindingContext))
            {
                FindViewById<StaticListView>(Resource.Id.ClassInputTimeListView).Adapter = new ClassTimeInputAdapter(this) {
                    DeleteCommand = ViewModel.DeleteTimeCommand
                };
            }

		    _colorIndicator = FindViewById(Resource.Id.ColorIndicator);
		    _colorButton = FindViewById<Button>(Resource.Id.ColorButton);

            _colorButton.Click += ColorButtonOnClick;

            _colorButton.SetPadding(0, 0, 0, 0);

		    _newTimeButton = FindViewById<Button>(Resource.Id.NewTimeButton);
		    _startEndDatesSwitch = FindViewById<MslSwitch>(Resource.Id.StartEndDatesSwitch);

		    Invalidate();
		}

	    private void ColorButtonOnClick(object sender, EventArgs e) {
	        new ColorPickerDialogFragment(this, ViewModel.Colors, ViewModel.SelectedColor) {
	            ShowInheritFromSubject = true
	        }.Show(SupportFragmentManager, "color-picker");
	    }

	    protected override void ViewModelOnLoaded() {
	        base.ViewModelOnLoaded();

	        if (ContentViewSet) {
	            Invalidate();
	        }
	    }

	    protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewClass : R.EditClass;
        }

        protected override AcademicSchedulePickerDialogFragment GetAcademicSchedulePicker() {
            return new AcademicSchedulePickerDialogFragment(this, this, ViewModel.AcademicYears, ViewModel.SelectedSchedule, R.ClassInput.AcademicScheduleHelp);
        }

	    private void Invalidate() {
	        if (ViewModel == null || ViewModel.User == null) {
	            return;
	        }

	        bool isTeacher = ViewModel.User.IsTeacher;

            FindViewById<TextView>(Resource.Id.ModuleTextField).Hint = isTeacher
                ? R.ClassInput.InputModuleTeacher
                : R.ClassInput.InputModuleStudent;

	        if (ViewModel.InputItem == null) {
	            return;
	        }

	        if (isTeacher) {
	            UpdateColorIndicator();
	        }
	    }

	    protected override void SetColor(MaterialPalette palette) {
	        base.SetColor(palette);

	        if (_newTimeButton != null && _startEndDatesSwitch != null) {
	            _newTimeButton.SetTextColor(palette.PrimaryColor);
	            _startEndDatesSwitch.CheckedTintColor = palette.PrimaryColor;
	        }
	    }

	    private void UpdateColorIndicator() {
            if (_colorIndicator == null) {
	            return;
	        }

            _colorIndicator.Background.Mutate().SetColorFilter(ViewModel.Color.ToNativeColor(), PorterDuff.Mode.Multiply);
            _colorButton.SetText(
                ViewModel.SelectedColor != null
                    ? Resource.String.OverridesSubjectColor
                    : Resource.String.UsesSubjectColor
            );
	    }

	    public void OnColorSet(UserColor color) {
	        ViewModel.SelectedColor = color;

	        UpdateColorIndicator();
	    }
	}

    public class ClassTimeInputAdapter : MvxAdapterWithChangedEvent {
        public ICommand DeleteCommand { get; set; }

        public ClassTimeInputAdapter(Context context) : base(context) {}
        protected ClassTimeInputAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}

        protected override IMvxListItemView CreateBindableView(object dataContext, ViewGroup parent, int templateId) {
            return new ClassTimeListItemView(Context, BindingContext.LayoutInflaterHolder, dataContext, parent, templateId) {
                DeleteCommand = DeleteCommand
            };
        }
        
        class ClassTimeListItemView : MvxListItemView {
            private IDisposable _subscription;

            private readonly ClassTimeOccurrenceTextView _occurrenceTextView;

            public ICommand DeleteCommand { get; set; }

            public ClassTimeListItemView(Context context, IMvxLayoutInflaterHolder layoutInflater, object dataContext, ViewGroup parent, int templateId)
                : base(context, layoutInflater, dataContext, parent, templateId) {
                _occurrenceTextView = Content.FindViewById<ClassTimeOccurrenceTextView>(Resource.Id.ClassTimeOccurrenceText);

                BindingContext.DataContextChanged += (s, e) => OnDataContextChanged();

                if (DataContext != null) {
                    OnDataContextChanged();
                }

                var drawable = DrawableCompat.Wrap(
                    ContextCompat.GetDrawable(Content.Context, Resource.Drawable.ic_action_delete)
                );
                var tintList = ContextCompat.GetColorStateList(Content.Context, Resource.Color.subtle_to_danger);

                DrawableCompat.SetTintList(drawable, tintList);

                var actionDelete = Content.FindViewById<ImageButton>(Resource.Id.ActionDelete);
                actionDelete.SetImageDrawable(drawable);
                actionDelete.Click += (s, e) => DeleteCommand?.Execute(DataContext);
            }

            private void OnDataContextChanged() {
                var ct = (ClassTime)BindingContext.DataContext;

                _subscription?.Dispose();
                _subscription = ct?.WeakSubscribe(DataContextOnPropertyChanged);
            }

            private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
                if (e.PropertyName.In("RotationWeek", "Days", "RotationDays")) {
                    _occurrenceTextView?.NotifyValueChanged();
                }
            }

            protected override void Dispose(bool disposing) {
                if (disposing) {
                    if (_subscription != null) {
                        _subscription.Dispose();
                        _subscription = null;
                    }
                }
                base.Dispose(disposing);
            }
        }
    }
}