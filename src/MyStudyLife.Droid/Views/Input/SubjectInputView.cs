using Android.App;
using Android.OS;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
	[Activity(Label = "Subject Input")]
    public class SubjectInputView : InputView<SubjectInputViewModel> {
		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_SubjectInput);
		}

        protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewSubject : R.EditSubject;
        }
	}
}