using System;
using System.Linq;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.OS;
using Android.Widget;
using Java.Lang;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using ActionBar = AndroidX.AppCompat.App.ActionBar;
using String = System.String;
using JInteger = Java.Lang.Integer;

namespace MyStudyLife.Droid.Views.Input {
    [Activity(Label = "Class Time Input")]
    public class ClassTimeInputView : InputView<ClassTimeInputViewModel>, MaterialDialog.IListCallbackMultiChoice {
        [UsedImplicitly] private MvxPropertyChangedListener _itemListener;

        private Button _rotationDaysPicker;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            this.SetContentView(Resource.Layout.View_ClassTimeInput);

            this.FindViewById<Button>(Resource.Id.RotationWeekPicker).Click += RotationWeekPickerOnClick;

            _rotationDaysPicker = this.FindViewById<Button>(Resource.Id.RotationDaysPicker);
            _rotationDaysPicker.Click += RotationDaysPickerOnClick;

            this.SetDaysText();
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener
                .Listen(nameof(this.ViewModel.Color), SetColor)
                .Listen(nameof(this.ViewModel.RotationDays), SetDaysText);

            SetColor();
        }

        protected override void ViewModelOnLoaded() {
            _itemListener = new MvxPropertyChangedListener(this.ViewModel.InputItem)
                .Listen(nameof(this.ViewModel.InputItem.RotationDays), SetDaysText);

            this.SetViewTitle();

            if (this.ContentViewSet) {
                this.SetDaysText();
            }
        }

        protected override void OnSetActionBar(ActionBar ab) {
            base.OnSetActionBar(ab);

            ab.SetDisplayShowHomeEnabled(false);

            SetColor();
        }

        protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewClassTime : R.EditClassTime;
        }

        private void SetColor() {
            var toolbar = this.SafeGet(x => x.Toolbar);
            if (toolbar != null && this.ViewModel.Color != null) {
                var palette = new MaterialPalette(this.ViewModel.Color.ToNativeColor());

                toolbar.SetBackgroundColor(palette.PrimaryColor);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop) {
                    Window.SetStatusBarColor(palette.PrimaryColorDark);
                }
            }
        }

        #region Rotation Week Spinner

        private void RotationWeekPickerOnClick(object sender, EventArgs e) {
            new MaterialDialog.Builder(this)
                .Items(this.ViewModel.RotationWeeks.Select(x => x.Item2).ToArray())
                .ItemsCallbackSingleChoice(this.ViewModel.InputItem.RotationWeek.GetValueOrDefault(), (dialog, view, position, text) => {
                    dialog.Dismiss();

                    if (ViewModel?.InputItem != null) {
                        ViewModel.InputItem.RotationWeek = position;
                    }

                    return true;
                })
                .Show();
        }

        #endregion

        #region Rotation Day Picker

        private void RotationDaysPickerOnClick(object sender, EventArgs e) {
            var rotationDays = this.ViewModel.RotationDays;
            var selectedDays = this.ViewModel.InputItem.RotationDays.GetValueOrDefault();

            MaterialDialog dialog = null;

            var builder = new MaterialDialog.Builder(this)
                .Title(Resource.String.RotationDays)
                .Items(rotationDays.Select(x => x.Item2).ToArray())
                .AlwaysCallMultiChoiceCallback()
                .ItemsCallbackMultiChoice(
                    rotationDays.Where(x => selectedDays.HasFlag((RotationDays)x.Item1)).Select(x => new JInteger(x.Item3)).ToArray(),
                    this
                )
                .Callback(new DialogHelper.ButtonCallbackShim(
                    onPositive: () => {
                        // ReSharper disable AccessToModifiedClosure
                        if (dialog == null) {
                            return;
                        }

                        var daysOfWeek = dialog.GetSelectedIndices().Select(x => 1 << x.IntValue());

                        if (ViewModel.InputItem != null) {
                            ViewModel.InputItem.RotationDays = (RotationDays)daysOfWeek.Aggregate((x, y) => x | y);
                            // ReSharper restore AccessToModifiedClosure
                        }
                    }
                ))
                .PositiveText(Resource.String.Common_Ok)
                .NegativeText(Resource.String.Common_Cancel);

            dialog = builder.Build();

            dialog.GetActionButton(DialogAction.Positive).Enabled = 1 <= dialog.GetSelectedIndices().Length;

            dialog.Show();
        }

        public bool OnSelection(MaterialDialog dialog, JInteger[] positions, ICharSequence[] items) {
            dialog.GetActionButton(DialogAction.Positive).Enabled = 1 <= positions.Length;

            return true;
        }

        private void SetDaysText() {
            if (this.ViewModel?.InputItem == null || this.ViewModel.RotationDays == null) {
                return;
            }

            var ct = this.ViewModel.InputItem;

            string text = null;

            if (ct.RotationDays.HasValue && ct.RotationDays.Value > 0) {
                text = String.Join(", ",
                    from rDay in this.ViewModel.RotationDays
                    where ct.RotationDays.Value.HasFlag((RotationDays)rDay.Item1)
                    select rDay.Item2
                );
            }

            _rotationDaysPicker.Text = text;
        }

        #endregion
    }
}