using System;
using Android.OS;
using Android.Views;
using Android.Widget;
using AndroidX.AppCompat.App;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Android;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
    public abstract class SubjectDependentInputView<TEntity, TViewModel> : InputView<TViewModel>, AcademicSchedulePickerDialogFragment.IOnScheduleSetListener
        where TEntity : SubjectDependentEntity
        where TViewModel : SubjectDependentInputViewModel<TEntity> {

        private const string FragmentTag = "schedule_picker";

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener.Listen(() => ViewModel.Color, SetColor);
        }

        protected override void OnPostCreate(Bundle savedInstanceState) {
            base.OnPostCreate(savedInstanceState);

            SetColor();
        }

        protected override void OnSetActionBar(ActionBar ab) {
            base.OnSetActionBar(ab);

            ab.SetDisplayShowTitleEnabled(false);

            var v = LayoutInflater.Inflate(Resource.Layout.Toolbar_TitleButton, null);

            v.Click += ActionBarTitleOnClick;
            v.FindViewById<TextView>(Resource.Id.ActionBarTitle).Text = GetViewTitle(ViewModel.IsNew);
            v.FindViewById<TextView>(Resource.Id.ActionBarSubtitle).Bind(this, "Text SelectedScheduleText");

            ab.SetCustomView(v, new ActionBar.LayoutParams((int)(GravityFlags.Left | GravityFlags.Top)));
            ab.SetDisplayShowCustomEnabled(true);

            SetColor();
        }

        protected override void SetViewTitle() {
            var actionBar = this.SafeGet(x => x.SupportActionBar);

            if (actionBar?.CustomView != null && ViewModel != null) {
                actionBar.CustomView.FindViewById<TextView>(Resource.Id.ActionBarTitle).Text = GetViewTitle(ViewModel.IsNew);
            }
        }

        protected override void OnResume() {
            base.OnResume();

            var picker = (AcademicSchedulePickerDialogFragment) SupportFragmentManager.FindFragmentByTag(FragmentTag);

            picker?.SetOnScheduleSetListener(this);
        }

        private void ActionBarTitleOnClick(object sender, EventArgs e) {
            GetAcademicSchedulePicker().Show(SupportFragmentManager, FragmentTag);
        }

        protected virtual AcademicSchedulePickerDialogFragment GetAcademicSchedulePicker() {
            return new AcademicSchedulePickerDialogFragment(this, this, ViewModel.AcademicYears, ViewModel.SelectedSchedule, R.AcademicScheduleTaskExamInputHelp);
        }

        public void OnScheduleSet(IAcademicSchedule schedule) {
            if (ViewModel != null) {
                ViewModel.SelectedSchedule = schedule;
            }
        }

        private void SetColor() {
            if (!this.IsInMonoLimbo() && Toolbar != null && ViewModel.ColorOrNull != null) {
                SetColor(new MaterialPalette(ViewModel.ColorOrNull.Value.ToNativeColor()));
            }
        }

        protected virtual void SetColor(MaterialPalette palette) {
            Toolbar.SetBackgroundColor(palette.PrimaryColor);

            if (OS.HasLollipops) {
                Window.SetStatusBarColor(palette.PrimaryColorDark);
            }
        }
    }
}