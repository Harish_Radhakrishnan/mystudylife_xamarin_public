using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyStudyLife.Data;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Views.Input {
	[Activity(Label = "Task Input")]
    public class TaskInputView : SubjectDependentInputView<Task, TaskInputViewModel>, ExamPickerDialogFragment.IOnExamSetListener {
        private View _examPickerView;
	    private View _selectedExamView;
        private TextView _selectedExamTitleView;
        private TextView _selectedExamSubTitleView;
        private View _selectedExamPlaceholderView;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            this.SetContentView(Resource.Layout.View_TaskInput);

            _examPickerView = this.FindViewById(Resource.Id.ExamPicker);
            _examPickerView.Click += ExamPickerViewOnClick;

            _selectedExamView = _examPickerView.FindViewById(Resource.Id.SelectedExam);
            _selectedExamTitleView = _examPickerView.FindViewById<TextView>(Resource.Id.SelectedExamTitle);
            _selectedExamSubTitleView = _examPickerView.FindViewById<TextView>(Resource.Id.SelectedExamSubTitle);
            _selectedExamPlaceholderView = _examPickerView.FindViewById(Resource.Id.SelectedExamPlaceholder);

            SetExamPicker();
        }

	    protected override void OnViewModelSet() {
	        base.OnViewModelSet();

	        ViewModelListener.Listen(() => this.ViewModel.SelectedExam, SetExamPicker);

	        SetExamPicker();
	    }

	    private void SetExamPicker() {
	        var vm = this.ViewModel;

	        if (!this.IsInMonoLimbo() && vm != null && _examPickerView != null) {
	            var exam = vm.SelectedExam;

                if (exam != null && !exam.Guid.IsEmpty()) {
	                _selectedExamView.Visibility = ViewStates.Visible;
	                _selectedExamPlaceholderView.Visibility = ViewStates.Gone;

                    _selectedExamTitleView.Text = exam.Title;
                    _selectedExamSubTitleView.Text = exam.Date.ToShortTimeStringEx() + " " + exam.Date.ToString("D");
	            }
                else {
                    _selectedExamView.Visibility = ViewStates.Gone;
                    _selectedExamPlaceholderView.Visibility = ViewStates.Visible;
	            }
	        }
	    }

	    private void ExamPickerViewOnClick(object sender, EventArgs e) {
            new ExamPickerDialogFragment(this, this.ViewModel.Exams, this.ViewModel.SelectedExam).Show(
                this.SupportFragmentManager, "exam-picker"
            );
	    }

	    protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewTask : R.EditTask;
        }

	    public void OnExamSet(Exam exam) {
	        this.ViewModel.SelectedExam = exam;
	    }
	}
}