using System;
using Android.Views;
using Android.Views.Animations;
using Android.Views.InputMethods;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Input;
using ActionBar = AndroidX.AppCompat.App.ActionBar;

namespace MyStudyLife.Droid.Views.Input {
    public interface IInputView { }

	public abstract class InputView<TViewModel> : BaseNonCoreView<TViewModel>, IInputView
        where TViewModel : BaseViewModel, IInputViewModel {

	    public override void SetContentView(int layoutResId) {
            //var enterTransition = new Fade(FadingMode.In);
            //{
            //   // enterTransition.AddTransition(new Explode());

            //    //var fadeTransition = new Fade();
            //    enterTransition.ExcludeTarget(Android.Resource.Id.NavigationBarBackground, true);
            //    enterTransition.ExcludeTarget(Android.Resource.Id.StatusBarBackground, true);
            //    //enterTransition.AddTransition(fadeTransition);
            //}
            //Window.EnterTransition = enterTransition;

            //var exitTransition = new Fade(FadingMode.Out);
            //{
            //    exitTransition.ExcludeTarget(Android.Resource.Id.NavigationBarBackground, true);
            //    exitTransition.ExcludeTarget(Android.Resource.Id.StatusBarBackground, true);

            //    //enterTransition.AddTransition(new Explode());

            //    //var fadeTransition = new Fade(FadingMode.Out);
            //    //fadeTransition.ExcludeTarget(Android.Resource.Id.NavigationBarBackground, true);
            //    //fadeTransition.ExcludeTarget(Android.Resource.Id.StatusBarBackground, true);
            //    //fadeTransition.AddTarget(Resource.Id.ActionBarTitleButton);
            //    //exitTransition.AddTransition(fadeTransition);
            //};
            //Window.ExitTransition = exitTransition;

            SetContentViewNoWrap(Resource.Layout.view_input);

	        var contentView = this.BindingInflate(layoutResId, null);

            FindViewById<ViewGroup>(Resource.Id.InputContent).AddView(contentView);

            contentView.SetPadding(
                contentView.PaddingLeft,
                contentView.PaddingTop,
                contentView.PaddingRight,
                Math.Max(contentView.PaddingBottom, Resources.GetDimensionPixelSize(Resource.Dimension.margin_s))
            );

            var inputErrorSummary = FindViewById(Resource.Id.InputErrorSummary);

            inputErrorSummary.LayoutChange += (s, e) => InputErrorSummaryOnLayout();

            FindViewById(Resource.Id.ActionSave).Click += SaveOnClick;
	    }

	    private void SaveOnClick(object sender, EventArgs e) {
            if (CurrentFocus != null) {
                InputMethodManager.FromContext(this).HideSoftInputFromWindow(
                    CurrentFocus.WindowToken,
                    HideSoftInputFlags.None
                );
            }
            ClearFocus();

            ViewModel.SaveCommand.Execute(null);
        }

	    protected override void OnSetActionBar(ActionBar ab) {
	        base.OnSetActionBar(ab);

	        ab.SetDisplayHomeAsUpEnabled(true);
	        ab.SetHomeAsUpIndicator(Resource.Drawable.ic_action_close);

            // Sometimes the action bar is not yet set when we try to set the title
	        SetViewTitle();
	    }

        private void InputErrorSummaryOnLayout() {
            var errorSummaryView = FindViewById(Resource.Id.InputErrorSummary);
	        var inputContentView = FindViewById(Resource.Id.InputContent);
	        var inputContentViewLayoutParams = (ViewGroup.MarginLayoutParams) inputContentView.LayoutParameters;

	        int offset = errorSummaryView.MeasuredHeight;

// ReSharper disable once CompareOfFloatsByEqualityOperator
            if (offset != inputContentViewLayoutParams.TopMargin) {
                var animation = new TranslateAnimation(0, 0, 0, offset - inputContentViewLayoutParams.TopMargin) {
                    Duration = AnimationHelper.DurationShort,
                    Interpolator = AnimationHelper.GetFastOutSlowInInterpolator(this)
                };

                animation.AnimationEnd += (s, e) => {
                    inputContentView.ClearAnimation();

                    inputContentViewLayoutParams.TopMargin = offset;

                    inputContentView.RequestLayout();
                };

                inputContentView.StartAnimation(animation);
	        }
	    }

	    protected override void ViewModelOnLoaded() {
            base.ViewModelOnLoaded();

	        SetViewTitle();
	    }

        protected virtual void SetViewTitle() {
            var actionBar = this.SafeGet(x => x.SupportActionBar);

            if (actionBar != null) {
                actionBar.Title = ViewModel.IsLoaded ? GetViewTitle(ViewModel.IsNew) : String.Empty;
            }
        }

        protected virtual string GetViewTitle(bool isNew) {
	        return isNew ? "New Item" : "Edit Item";
	    }

        public sealed override bool OnCreateOptionsMenu(IMenu menu) {
            return true;
        }

        public sealed override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Android.Resource.Id.Home:
                    ViewModel.CancelInputCommand.Execute(null);
                    return true;
                default:
                    return false;
            }
        }

        private void ClearFocus(ViewGroup viewGroup = null) {
            if (viewGroup == null) {
                viewGroup = FindViewById<ViewGroup>(Resource.Id.InputContent);
                viewGroup.Focusable = true;
                viewGroup.FocusableInTouchMode = true;
            }

            for (int i = 0; i < viewGroup.ChildCount; i++) {
                var childView = viewGroup.GetChildAt(i);

                childView.ClearFocus();

                if (childView is ViewGroup childViewGroup) {
                    ClearFocus(childViewGroup);
	            }
	        }
	    }
	}
}