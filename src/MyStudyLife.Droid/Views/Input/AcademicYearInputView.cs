using System;
using System.Globalization;
using System.Linq;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using MvvmCross.Converters;
using MyStudyLife.Data;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
	[Activity(Label = "Academic Year Input", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AcademicYearInputView : InputView<AcademicYearInputViewModel> {
	    private Picker _schedulingModePicker;
        
	    protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_AcademicYearInput);

            _schedulingModePicker = FindViewById<Picker>(Resource.Id.SchedulingModePicker);
            _schedulingModePicker.Click += SchedulingModePickerOnClick;

            FindViewById<Picker>(Resource.Id.SchedulingWeekCountPicker).Click += SchedulingWeekCountPickerOnClick;
            FindViewById<Picker>(Resource.Id.SchedulingStartWeekPicker).Click += SchedulingStartWeekPickerOnClick;

            FindViewById<Picker>(Resource.Id.SchedulingDayCountPicker).Click += SchedulingDayCountPickerOnClick;
            FindViewById<Picker>(Resource.Id.SchedulingStartDayPicker).Click += SchedulingStartDayPickerOnClick;
	    }

	    private void SchedulingModePickerOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

	        var options = new[] {
	            Resource.String.The_same_day_every_week,
	            Resource.String.The_same_day_every_x_weeks,
	            Resource.String.A_numbered_or_lettered_day
	        }.Select(x => Resources.GetString(x)).ToList();

            foreach (var option in options) {
                popup.Menu.Add(option);
            }

            popup.MenuItemClick += (s, e) => {
                ViewModel.InputItem.Scheduling.Mode = (SchedulingMode)options.IndexOf(e.Item.TitleFormatted.ToString());
            };

            popup.Show();
        }

        private void SchedulingWeekCountPickerOnClick(object sender, EventArgs args) {
            // TODO: Localize
            new MslPopupMenu.Builder(this, (View)sender)
                .Items(ViewModel.WeekCountOptions.Select(x => x + " weeks"))
                .MenuItemClick((pos) => {
                    ViewModel.InputItem.Scheduling.WeekCount = ViewModel.WeekCountOptions[pos];
                })
                .Show();
        }

        private void SchedulingStartWeekPickerOnClick(object sender, EventArgs args) {
            new MslPopupMenu.Builder(this, (View)sender)
                .Items(ViewModel.AvailableWeeks.Select(x => x.Value))
                .MenuItemClick((pos) => {
                    ViewModel.StartWeekValue = ViewModel.AvailableWeeks[pos];
                })
                .Show();
        }


        private void SchedulingDayCountPickerOnClick(object sender, EventArgs args) {
            // TODO: Localize
            new MslPopupMenu.Builder(this, (View) sender)
                .Items(ViewModel.DayCountOptions.Select(x => x + " days"))
                .MenuItemClick((pos) => {
                    ViewModel.InputItem.Scheduling.DayCount = ViewModel.DayCountOptions[pos];
                })
                .Show();
        }

        private void SchedulingStartDayPickerOnClick(object sender, EventArgs args) {
            new MslPopupMenu.Builder(this, (View)sender)
                .Items(ViewModel.AvailableDays.Select(x => x.Value))
                .MenuItemClick((pos) => {
                    ViewModel.StartDayValue = ViewModel.AvailableDays[pos];
                })
                .Show();
        }
        
	    protected override string GetViewTitle(bool isNew) {
	        return isNew ? R.NewAcademicYear : R.EditAcademicYear;
	    }
	}

    public class SchedulingModeTextConverter : MvxValueConverter<SchedulingMode, string> {
        private readonly string[] _modes;

        public SchedulingModeTextConverter() {
            _modes = new[] {
	            Resource.String.The_same_day_every_week,
	            Resource.String.The_same_day_every_x_weeks,
	            Resource.String.A_numbered_or_lettered_day
	        }.Select(x => Application.Context.Resources.GetString(x)).ToArray();
        }

        protected override string Convert(SchedulingMode value, Type targetType, object parameter, CultureInfo culture) {
            return _modes[(int)value];
        }
    }
}