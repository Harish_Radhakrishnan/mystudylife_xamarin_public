using System;
using Android.App;
using Android.OS;
using Android.Views;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.Droid.Views.Input {
	[Activity(Label = "Holiday Input")]
    public class HolidayInputView : InputView<HolidayInputViewModel> {
        protected override void ViewModelOnLoaded() {
            base.ViewModelOnLoaded();

            SetDeleteMenuItemVisibility();
        }

        private void SetDeleteMenuItemVisibility() {
            //if (_optionsMenu != null) {
            //    _optionsMenu.FindItem(Resource.Id.ActionOverflow).SetVisible(this.ViewModel.CanDelete);
            //}
        }

		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_HolidayInput);

            this.FindViewById<View>(Resource.Id.PushesScheduleCheckBoxLabel).Click += OnClick;
        }

	    private void OnClick(object sender, EventArgs e) {
	        this.ViewModel.InputItem.PushesSchedule = !this.ViewModel.InputItem.PushesSchedule;
	    }

	    protected override string GetViewTitle(bool isNew) {
            return isNew ? R.NewHoliday : R.EditHoliday;
        }

        //public override bool OnOptionsItemSelected(IMenuItem item) {
        //    if (item.ItemId == Resource.Id.ActionDelete) {
        //        this.ViewModel.DeleteWithConfirmationCommand.Execute(null);
        //    }

        //    return base.OnOptionsItemSelected(item);
        //}
	}
}