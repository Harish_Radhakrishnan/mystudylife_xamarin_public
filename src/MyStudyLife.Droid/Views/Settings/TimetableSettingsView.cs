using System;
using System.Linq;
using AFollestad.MaterialDialogs;
using Android.App;
using Android.OS;
using Android.Text;
using Android.Text.Method;
using Android.Views;
using Android.Widget;
using Java.Lang;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Settings;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Settings;
using String = System.String;
using Enum = System.Enum;
using JInteger = Java.Lang.Integer;

namespace MyStudyLife.Droid.Views.Settings {
	[Activity(Label = "Timetable Settings")]
	public class TimetableSettingsView : SettingsItemView<TimetableSettingsViewModel>, MaterialDialog.IListCallbackMultiChoice {
	    [UsedImplicitly] private MvxPropertyChangedListener _listener;

		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);

			SetContentView(Resource.Layout.View_Settings_Timetable);

			this.FindViewById<LinearLayout>(Resource.Id.TimetableMode).Click += TimetableModeOnClick;

			this.FindViewById<LinearLayout>(Resource.Id.TimetableWeekCount).Click += TimetableWeekCountOnClick;
			this.FindViewById<LinearLayout>(Resource.Id.TimetableSetWeek).Click += TimetableSetWeekOnClick;

			this.FindViewById<LinearLayout>(Resource.Id.TimetableDayCount).Click += TimetableDayCountOnClick;
			this.FindViewById<LinearLayout>(Resource.Id.TimetableSetDay).Click += TimetableSetDayOnClick;
            this.FindViewById<LinearLayout>(Resource.Id.TimetableDays).Click += TimetableDaysOnClick;

            this.SetIsReadOnly();
		}

	    protected override void OnViewModelSet() {
	        base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.User, SetIsReadOnly);
	    }

	    private void SetIsReadOnly() {
            var user = this.ViewModel?.User;

            if (user != null) {
                var warningMessage = this.FindViewById<TextView>(Resource.Id.TimetableSettingsWarningMessage);
                var settingsLayout = this.FindViewById<LinearLayout>(Resource.Id.SettingsLayout);

                var isStudentWithUnmanagedSchool = user.IsStudentWithUnmanagedSchool;

                if (isStudentWithUnmanagedSchool || user.IsTeacher) {
                    warningMessage.Text = isStudentWithUnmanagedSchool
                        ? R.TimetableSettings.StudentWithSchoolMessage
                        : R.TimetableSettings.TeacherWithSchoolMessage;
                    
                    RecursiveSetIsEnabled(settingsLayout, !isStudentWithUnmanagedSchool);

                    settingsLayout.Alpha = settingsLayout.Enabled ? 1 : 0.5f;
                }
                else {
                    warningMessage.MovementMethod = LinkMovementMethod.Instance;

                    warningMessage.SetText(Html.FromHtml(
                        "We've added a brand new way to setup rotating schedules - you can add holidays that push your schedule too. " +
                        "To get started, add an academic year and take a look at our <a href=\"" + this.ViewModel.UpgradeGuideUri + "\">upgrade guide</a>." +
                        "<br>" +
                        "<br>" +
                        "These settings will be removed in the future but will automatically disappear when you no longer need them."
                    ), TextView.BufferType.Normal);
                }
            }
	    }

	    private void RecursiveSetIsEnabled(View view, bool enabled) {
	        var viewGroup = view as ViewGroup;

	        if (viewGroup != null) {
                for (int i = 0; i < viewGroup.ChildCount; i++) {
                    RecursiveSetIsEnabled(viewGroup.GetChildAt(i), enabled);
                }
	        }

	        view.Enabled = enabled;
	    }

	    private void TimetableModeOnClick(object sender, EventArgs eventArgs) {
            var popup = new MslPopupMenu(this, (View)sender);
                
			var modes = Enum.GetValues(typeof (TimetableMode)).Cast<TimetableMode>();
	        var modeTitles = modes.Select(x => ((Enum) x).GetTitle()).ToList();

	        foreach (var mode in modeTitles) {
	            popup.Menu.Add(mode);
	        }
            
            popup.MenuItemClick += (s, e) => {
				this.ViewModel.Settings.Mode = (TimetableMode) modeTitles.IndexOf(e.Item.TitleFormatted.ToString());
            };

            popup.Show();
		}

        private void TimetableWeekCountOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

            foreach (var i in this.ViewModel.RotationWeekCountOptions) {
                popup.Menu.Add(i + " Weeks");
            }

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.Settings.WeekCount = Int32.Parse(e.Item.TitleFormatted.ToString().Replace("Weeks", String.Empty).Trim());
            };

            popup.Show();
		}

		private void TimetableSetWeekOnClick(object sender, EventArgs eventArgs) {
            var popup = new MslPopupMenu(this, (View)sender);

		    var rotationWeeks = this.ViewModel.RotationWeeks.Select(x => "Week " + x.Value).ToList();

            foreach (var rWeek in rotationWeeks) {
                popup.Menu.Add(rWeek);
            }

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.SetWeekValue = this.ViewModel.RotationWeeks[rotationWeeks.IndexOf(e.Item.TitleFormatted.ToString())];
            };

            popup.Show();
		}

        private void TimetableDayCountOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

            foreach (var i in this.ViewModel.RotationDayCountOptions) {
                popup.Menu.Add(i + " Days");
            }

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.Settings.DayCount = Int32.Parse(e.Item.TitleFormatted.ToString().Replace("Days", String.Empty).Trim());
            };

            popup.Show();
		}

		private void TimetableSetDayOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

            var rotationDays = this.ViewModel.RotationWeeks.Select(x => "Day " + x.Value).ToList();

            foreach (var rDay in rotationDays) {
                popup.Menu.Add(rDay);
            }

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.SetDayValue = this.ViewModel.RotationDays[rotationDays.IndexOf(e.Item.TitleFormatted.ToString())];
            };

            popup.Show();
        }

        #region Timetable Days

        private void TimetableDaysOnClick(object sender, EventArgs args) {
            var days = DateTimeFormat.Days.ToList();
            var timetableDays = this.ViewModel.TimetableDays;

            MaterialDialog dialog = null;

            var builder = new MaterialDialog.Builder(this)
                .Title(Resource.String.Days)
                .Items(days.Select(x => x.Name).ToArray())
                .AlwaysCallMultiChoiceCallback()
                .ItemsCallbackMultiChoice(
                    days.Where(x => timetableDays.HasFlag(x.DayOfWeek.ToFlag())).Select(x => new JInteger((int)x.DayOfWeek)).ToArray(),
                    this
                )
                .Callback(new DialogHelper.ButtonCallbackShim(
                    onPositive: () => {
// ReSharper disable AccessToModifiedClosure
                        if (dialog == null) {
                            return;
                        }

                        var daysOfWeek = dialog.GetSelectedIndices().Select(x => ((DayOfWeek)(x.IntValue())).ToFlag());

                        this.ViewModel.TimetableDays = daysOfWeek.Aggregate((x, y) => x | y);
// ReSharper restore AccessToModifiedClosure
                    }
                ))
                .PositiveText(Resource.String.Common_Ok)
                .NegativeText(Resource.String.Common_Cancel);

            dialog = builder.Show();
		}

        public bool OnSelection(MaterialDialog dialog, JInteger[] positions, ICharSequence[] items) {
            dialog.GetActionButton(DialogAction.Positive).Enabled = 4 <= positions.Length;

            return true;
        }

		#endregion
	}
}