using System;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.ViewModels;
using MyStudyLife.Droid.Fragments.Dialogs;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.Globalization;
using MyStudyLife.UI.ViewModels.Settings;
using TimePickerDialog = Wdullaer.MaterialDateTimePicker.Time.TimePickerDialog;

namespace MyStudyLife.Droid.Views.Settings {
    [Activity(Label = "General Settings")]
    public class GeneralSettingsView : SettingsItemView<GeneralSettingsViewModel>, TimePickerDialog.IOnTimeSetListener, NumberPicker.IOnValueChangeListener {
        // ReSharper disable once NotAccessedField.Local
        private MvxPropertyChangedListener _settingsListener;

        private TextView _rotationScheduleLetteredTextView;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_Settings_General);

            _rotationScheduleLetteredTextView = this.FindViewById<TextView>(Resource.Id.RotationScheduleLetteredText);

            this.FindViewById(Resource.Id.FirstDayOfWeekPicker).Click += FirstDayOfWeekOnClick;
            this.FindViewById(Resource.Id.RotationScheduleLetteredPicker).Click += RotationScheduleLetteredOnClick;
            this.FindViewById(Resource.Id.DefaultStartTimePicker).Click += DefaultStartTimeOnClick;
            this.FindViewById(Resource.Id.DefaultDurationPicker).Click += DefaultDurationOnClick;

            SetRotationScheduleLetteredText();
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            Action setupSettingsListener = () => {
                _settingsListener = new MvxPropertyChangedListener(this.ViewModel.Settings)
                    .Listen(() => this.ViewModel.Settings.IsRotationScheduleLettered, SetRotationScheduleLetteredText);

                SetRotationScheduleLetteredText();
            };

            if (this.ViewModel.Settings != null) {
                setupSettingsListener();
            }
            else {
                new MvxPropertyChangedListener(this.ViewModel)
                    .Listen(() => this.ViewModel.Settings, () => {
                        if (this.ViewModel.Settings != null) {
                            setupSettingsListener();
                        }
                    });
            }
        }

        private void SetRotationScheduleLetteredText() {
            var vm = this.ViewModel;

            if (_rotationScheduleLetteredTextView != null && vm != null && vm.Settings != null) {
                _rotationScheduleLetteredTextView.SetText(vm.Settings.IsRotationScheduleLettered ? Resource.String.Letters : Resource.String.Numbers);
            }
        }

        private void FirstDayOfWeekOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

            var days = this.ViewModel.Days.Select(x => x.Name).ToList();

            foreach (var mode in days) {
                popup.Menu.Add(mode);
            }

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.FirstDayOfWeekValue = this.ViewModel.Days.ElementAt(days.IndexOf(e.Item.TitleFormatted.ToString()));
            };

            popup.Show();
        }

        private void RotationScheduleLetteredOnClick(object sender, EventArgs args) {
            var popup = new MslPopupMenu(this, (View)sender);

            popup.MenuInflater.Inflate(Resource.Menu.NumbersOrLetters, popup.Menu);

            popup.MenuItemClick += (s, e) => {
                this.ViewModel.Settings.IsRotationScheduleLettered = e.Item.ItemId == Resource.Id.ActionLetters;
            };

            popup.Show();
        }

        private void DefaultStartTimeOnClick(object sender, EventArgs args) {
            var time = this.ViewModel.Settings.DefaultStartTime;

            var picker = TimePickerDialog.NewInstance(this, time.Hours, time.Minutes, DateTimeFormat.Is24Hour);

            picker.Show(this.SupportFragmentManager, "time");
        }

        private void DefaultDurationOnClick(object sender, EventArgs args) {
            new NumberPickerDialogFragment(
                Resource.String.Settings_DefaultDuration,
                Data.Data.MinDuration,
                Data.Data.MaxDuration,
                this.ViewModel.Settings.DefaultDuration,
                this
            ).Show(this.SupportFragmentManager, "duration");
        }

        #region Implementation of IOnTimeSetListener

        public void OnTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            this.ViewModel.Settings.DefaultStartTime = new TimeSpan(hourOfDay, minute, 0);
        }

        #endregion

        #region Implementation of IOnValueChangeListener

        public void OnValueChange(NumberPicker picker, int oldVal, int newVal) {
            this.ViewModel.Settings.DefaultDuration = newVal;
        }

        #endregion
    }
}