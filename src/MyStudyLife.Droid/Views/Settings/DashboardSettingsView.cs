using System;
using Android.App;
using Android.OS;
using Android.Views;
using MyStudyLife.Data.Settings;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.Droid.Views.Settings {
    [Activity(Label = "Dashboard")]
    public class DashboardSettingsView : SettingsItemView<DashboardSettingsViewModel> {
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_Settings_Dashboard);

            this.FindViewById(Resource.Id.DashboardTasksTimescale).Click += TasksTimescaleOnClick;
            this.FindViewById(Resource.Id.DashboardExamsTimescale).Click += ExamsTimescaleOnClick;
        }

        private void TasksTimescaleOnClick(object sender, EventArgs e) {
            var popup = new MslPopupMenu(this, (View)sender);

            this.ViewModel.TasksTimescaleOptions.Apply(x => popup.Menu.Add(Menu.None, (int)x.Key, Menu.None, x.Value));
            
            popup.MenuItemClick += (s, ev) => {
                this.ViewModel.TasksTimescale = (DashboardTasksTimescale)ev.Item.ItemId;
            };

            popup.Show();
        }

        private void ExamsTimescaleOnClick(object sender, EventArgs e) {
            var popup = new MslPopupMenu(this, (View)sender);

            this.ViewModel.ExamsTimescaleOptions.Apply(x => popup.Menu.Add(Menu.None, (int)x.Key, Menu.None, x.Value));

            popup.MenuItemClick += (s, ev) => {
                this.ViewModel.ExamsTimescale = (DashboardExamsTimescale)ev.Item.ItemId;
            };

            popup.Show();
        }
    }
}