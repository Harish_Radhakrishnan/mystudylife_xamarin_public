using System;
using Android.App;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Widget;
using MyStudyLife.Data.Settings;
using MyStudyLife.Globalization;
using MyStudyLife.UI.ViewModels.Settings;
using PopupMenu = AndroidX.AppCompat.Widget.PopupMenu;
using TimePickerDialog = Wdullaer.MaterialDateTimePicker.Time.TimePickerDialog;
using MyStudyLife.Droid.Common;
using MvvmCross.ViewModels;
using AndroidX.Core.Graphics.Drawable;
using AndroidX.AppCompat.Widget;

namespace MyStudyLife.Droid.Views.Settings {
    [Activity(Label = "Reminders")]
    public class ReminderSettingsView : SettingsItemView<ReminderSettingsViewModel>, TimePickerDialog.IOnTimeSetListener {
        // ReSharper disable once NotAccessedField.Local
        private MvxPropertyChangedListener _settingsListener;

        private TextView _remindersEnabledTextView;
        private TextView _classRemindersTextView;
        private TextView _examRemindersTextView;
        private TextView _taskRemindersEnabledTextView;
        private TextView _taskRemindersTextView;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.View_Settings_Reminder);

            _remindersEnabledTextView = this.FindViewById<TextView>(Resource.Id.RemindersEnabledText);
            _classRemindersTextView = this.FindViewById<TextView>(Resource.Id.ClassRemindersText);
            _examRemindersTextView = this.FindViewById<TextView>(Resource.Id.ExamRemindersText);
            _taskRemindersEnabledTextView = this.FindViewById<TextView>(Resource.Id.TaskRemindersEnabledText);
            _taskRemindersTextView = this.FindViewById<TextView>(Resource.Id.TaskRemindersText);

            this.FindViewById(Resource.Id.ClassRemindersPicker).Click += RemindersPickerOnClick(
                enabled => this.ViewModel.Settings.ClassRemindersEnabled = enabled,
                time => this.ViewModel.Settings.ClassReminderTime = time
            );
            this.FindViewById(Resource.Id.ExamRemindersPicker).Click += RemindersPickerOnClick(
                enabled => this.ViewModel.Settings.ExamRemindersEnabled = enabled,
                time => this.ViewModel.Settings.ExamReminderTime = time
            );

            this.FindViewById(Resource.Id.TaskRemindersPicker).Click += TaskRemindersPickerOnClick;

            SetEnabledText();
            SetClassRemindersText();
            SetExamRemindersText();
            SetTaskRemindersText();

            var remindersEnabledSwitch = this.FindViewById<SwitchCompat>(Resource.Id.RemindersEnabledSwitch);

            var whiteAlpha75 = Color.White;
            whiteAlpha75.A = 191; // 75%

            DrawableCompat.SetTintList(remindersEnabledSwitch.TrackDrawable, new ColorStateList(
                new[] {
                    new[] { Android.Resource.Attribute.StateChecked },
                    new int[0]
                },
                new int[] {
                    whiteAlpha75,
                    ContextCompatEx.GetColor(this, Resource.Color.accent_light)
                }
            ));

            DrawableCompat.SetTintList(remindersEnabledSwitch.ThumbDrawable, new ColorStateList(
                new[] {
                    new[] { Android.Resource.Attribute.StateChecked },
                    new int[0]
                },
                new int[] {
                    Color.White,
                    ContextCompatEx.GetColor(this, Resource.Color.accent)
                }
            ));
        }

        private EventHandler RemindersPickerOnClick(Action<bool> setEnabled, Action<ReminderTime> setTime) {
            return (sender, args) => {
                var popup = new PopupMenu(this, (View)sender, (int)GravityFlags.CenterVertical, Resource.Attribute.actionOverflowMenuStyle, Resource.Style.MyStudyLife_Widget_PopupMenu);

                popup.MenuInflater.Inflate(Resource.Menu.RemindersPopup, popup.Menu);

                popup.MenuItemClick += (s, e) => {
                    if (e.Item.ItemId == Resource.Id.ActionRemindersOff) {
                        setEnabled(false);
                    }
                    else {
                        setEnabled(true);

                        ReminderTime time;

                        switch (e.Item.ItemId) {
                            case Resource.Id.ActionRemindersMinutes5:
                                time = ReminderTime.Minutes5;
                                break;
                            case Resource.Id.ActionRemindersMinutes15:
                                time = ReminderTime.Minutes15;
                                break;
                            case Resource.Id.ActionRemindersMinutes30:
                                time = ReminderTime.Minutes30;
                                break;
                            default:
                                throw new NotSupportedException("Unknown reminder time selection");
                        }

                        setTime(time);
                    }
                };

                popup.Show();
            };
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            Action setupSettingsListener = () => {
                _settingsListener = new MvxPropertyChangedListener(this.ViewModel.Settings)
                    .Listen(() => this.ViewModel.Settings.RemindersEnabled, SetEnabledText)
                    .Listen(() => this.ViewModel.Settings.ClassRemindersEnabled, SetClassRemindersText)
                    .Listen(() => this.ViewModel.Settings.ClassReminderTime, SetClassRemindersText)
                    .Listen(() => this.ViewModel.Settings.ExamRemindersEnabled, SetExamRemindersText)
                    .Listen(() => this.ViewModel.Settings.ExamReminderTime, SetExamRemindersText)
                    .Listen(() => this.ViewModel.Settings.TaskRemindersEnabled, SetTaskRemindersText)
                    .Listen(() => this.ViewModel.Settings.TaskReminderTime, SetTaskRemindersText);

                SetEnabledText();
                SetClassRemindersText();
                SetExamRemindersText();
                SetTaskRemindersText();
            };

            if (this.ViewModel.Settings != null) {
                setupSettingsListener();
            }
            else {
                new MvxPropertyChangedListener(this.ViewModel)
                    .Listen(() => this.ViewModel.Settings, () => {
                        if (this.ViewModel.Settings != null) {
                            setupSettingsListener();
                        }
                    });
            }
        }

        private void SetEnabledText() {
            var vm = this.ViewModel;
            var tv = _remindersEnabledTextView.SafeGet();

            if (tv != null && vm != null && vm.Settings != null) {
                tv.SetText(vm.Settings.RemindersEnabled ? Resource.String.On : Resource.String.Off);
            }
        }

        private void SetClassRemindersText() {
            var vm = this.ViewModel;
            var tv = _classRemindersTextView.SafeGet();

            if (tv != null && vm != null && vm.Settings != null) {
                tv.SetText(GetRemindersText(vm.Settings.ClassRemindersEnabled, vm.Settings.ClassReminderTime));
            }
        }

        private void SetExamRemindersText() {
            var vm = this.ViewModel;
            var tv = _examRemindersTextView.SafeGet();

            if (tv != null && vm != null && vm.Settings != null) {
                tv.SetText(GetRemindersText(vm.Settings.ExamRemindersEnabled, vm.Settings.ExamReminderTime));
            }
        }

        private void SetTaskRemindersText() {
            var vm = this.ViewModel;
            var tv = _taskRemindersTextView.SafeGet();
            var enabledTv = _taskRemindersEnabledTextView.SafeGet();

            if (enabledTv != null && tv != null && vm != null && vm.Settings != null) {
                enabledTv.SetText(vm.Settings.TaskRemindersEnabled ? Resource.String.On : Resource.String.Off);

                tv.Text =
                    String.Format(Resources.GetString(Resource.String.AtTimeTheDayBeforeFormat),
                        vm.Settings.TaskReminderTime.ToShortTimeStringEx());

            }
        }

        private int GetRemindersText(bool remindersEnabled, ReminderTime? time) {
            int resId = Resource.String.RemindersOff;

            if (remindersEnabled) {
                switch (time) {
                    case ReminderTime.Minutes5:
                        resId = Resource.String.RemindersMinutes5;
                        break;
                    case ReminderTime.Minutes15:
                        resId = Resource.String.RemindersMinutes15;
                        break;
                    case ReminderTime.Minutes30:
                        resId = Resource.String.RemindersMinutes30;
                        break;
                }
            }

            return resId;
        }

        private void TaskRemindersPickerOnClick(object sender, EventArgs e) {
            var time = this.ViewModel.Settings.TaskReminderTime;

            var picker = TimePickerDialog.NewInstance(this, time.Hours, time.Minutes, DateTimeFormat.Is24Hour);

            picker.Show((this).SupportFragmentManager, "task-reminders-time");
        }

        #region Implementation of IOnTimeSetListener

        public void OnTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
            this.ViewModel.Settings.TaskReminderTime = new TimeSpan(hourOfDay, minute, 0);
        }

        #endregion
    }
}