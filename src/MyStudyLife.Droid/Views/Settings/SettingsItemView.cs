using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.Droid.Views.Settings {
	public abstract class SettingsItemView<TViewModel> : BaseNonCoreView<TViewModel>
        where TViewModel : BaseViewModel, ISettingsViewModel {

        protected override void OnStop() {
            this.ViewModel.SaveChanges();

	        base.OnStop();
	    }
	}
}