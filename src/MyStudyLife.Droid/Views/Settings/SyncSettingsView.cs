using Android.App;
using Android.OS;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.Droid.Views.Settings {
	[Activity(Label = "Sync Settings")]
    public class SyncSettingsView : BaseNonCoreView<SyncSettingsViewModel> {
		protected override void OnCreate(Bundle bundle) {
			base.OnCreate(bundle);
            
			SetContentView(Resource.Layout.View_Settings_Sync);
		}
	}
}