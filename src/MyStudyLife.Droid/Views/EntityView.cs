using System;
using Android.Animation;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Droid.Widgets.ObservableScrollView;
using MyStudyLife.UI.ViewModels.View;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Android;
using MyStudyLife.Droid.Extensions;
using Math = System.Math;
using Google.Android.Material.Tabs;

namespace MyStudyLife.Droid.Views {
    public interface IEntityView { }

    // Inspired by samples from https://github.com/ksoichiro/Android-ObservableScrollView
    public abstract class EntityView<TEntity, TViewModel> : BaseNonCoreView<TViewModel>, IEntityView
        where TEntity : SubjectDependentEntity
        where TViewModel : EntityViewModel<TEntity> {

        private const int CONTENT_IN_ANIMATION_DURATION = 500;

        private ObservableScrollView _scrollView;

        private int _toolbarHeight;

        private View _flexibleSpaceView;
        private Toolbar _toolbar;
        private TabLayout _tabLayout;
        private View _tabShadow;
        private View _titleLayoutView;
        private View _titleInnerLayout;
        private View _toolbarTitleLayoutView;
        private LinearLayout _toolbarTitleInnerLayoutView;
        private FrameLayout _contentLayout;

        private ValueAnimator _flexibleSpaceViewResizeAnimation;
        private ViewPropertyAnimator[] _contentInAnimators;
        private bool _contentAnimatedIn;

        public abstract int LayoutResourceId { get; }

        private bool _tabsVisible;
        public bool TabsVisible {
            get { return _tabsVisible; }
            set {
                if (_tabsVisible != value) {
                    _tabsVisible = value;

                    if (ContentViewSet) {
                        UpdateTabLayout();
                    }
                }
            }
        }

        protected int FlexibleSpaceHeight => _flexibleSpaceView?.Height ?? 0;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            this.SetContentViewNoWrap(Resource.Layout.view_tec);

            _contentLayout = this.FindViewById<FrameLayout>(Resource.Id.Content);
            _contentLayout.AddView(this.BindingInflate(LayoutResourceId, null), 0);

            _flexibleSpaceView = this.FindViewById(Resource.Id.FlexibleSpace);
            _toolbar = this.FindViewById<Toolbar>(Resource.Id.Toolbar);
            _tabLayout = this.FindViewById<TabLayout>(Resource.Id.Tabs);
            _tabShadow = this.FindViewById(Resource.Id.TabsShadow);
            _titleLayoutView = this.FindViewById(Resource.Id.TitleLayout);
            _titleInnerLayout = this.FindViewById(Resource.Id.TitleInnerLayout); // Using inner as outer is animated by the scroll thing
            _toolbarTitleLayoutView = this.FindViewById(Resource.Id.ToolbarTitleLayout);
            _toolbarTitleInnerLayoutView = this.FindViewById<LinearLayout>(Resource.Id.ToolbarTitleInnerLayout);

            _toolbar.Alpha = 0;
            _tabLayout.Alpha = 0;
            _tabShadow.Alpha = 0;
            _titleInnerLayout.Alpha = 0;
            _contentLayout.Alpha = 0;

            //if (OS.HasLollipops) {
            //    _toolbar.Alpha = 0;

            //    bool stop = false;

            //    _contentLayout.ViewTreeObserver.PreDraw += (s, e) => {
            //        if (stop) return;
            //        stop = true;

            //        const int delay = 500;
            //        const int duration = 500;

            //    };
            //}

            this.Title = null;

            _scrollView = this.FindViewById<ObservableScrollView>(Resource.Id.RootScroll);

            var tv = new TypedValue();
            if (Theme.ResolveAttribute(Resource.Attribute.actionBarSize, tv, true)) {
                _toolbarHeight = TypedValue.ComplexToDimensionPixelSize(tv.Data, Resources.DisplayMetrics);
            }

            FlexibleSpaceOnHeightChanged(_flexibleSpaceView.LayoutParameters.Height);

            UpdateTabLayout();

            AttachScrollView(_scrollView);

            _titleLayoutView.LayoutChange += TitleLayoutViewOnLayoutChange;

            _toolbar.ChildViewAdded += ToolbarChildViewAdded;
        }

        protected override void OnPostCreate(Bundle savedInstanceState) {
            base.OnPostCreate(savedInstanceState);

            SetColor();
        }

        protected sealed override void ViewModelOnLoaded() {
            base.ViewModelOnLoaded();
            this.ViewModelOnLoaded(false);
        }

        protected virtual void ViewModelOnLoaded(bool overrideAnimation) {
            if (overrideAnimation) {
                return;
            }

            if (_flexibleSpaceViewResizeAnimation != null) {
                _flexibleSpaceViewResizeAnimation.AnimationEnd += (s, e) => AnimateContentIn(false);
            }
            else {
                AnimateContentIn();
            }
        }

        private void AnimateContentIn(bool withDelay = true) {
            _contentInAnimators?.Apply(x => x.Cancel());

            int delay = withDelay ? 300 : 0;

            _contentInAnimators = new[] {
                _toolbar.Animate()
                    .SetStartDelay(delay)
                    .SetDuration(CONTENT_IN_ANIMATION_DURATION)
                    .Alpha(1),
                _tabLayout.Animate()
                    .SetStartDelay(delay)
                    .SetDuration(CONTENT_IN_ANIMATION_DURATION)
                    .Alpha(1)
                    .WithEndActionCompat(() => {
                        _contentAnimatedIn = true;
                    }),
                _tabShadow.Animate()
                    .SetStartDelay(delay)
                    .SetDuration(CONTENT_IN_ANIMATION_DURATION)
                    .Alpha(1),
                _titleInnerLayout.Animate()
                    .SetStartDelay(delay)
                    .SetDuration(CONTENT_IN_ANIMATION_DURATION)
                    .Alpha(1),
                _contentLayout.Animate()
                    .SetStartDelay(delay)
                    .SetDuration(CONTENT_IN_ANIMATION_DURATION)
                    .Alpha(1)
            };

            _contentInAnimators.Apply(x => x.Start());
        }

        protected void AttachScrollView(ObservableScrollView scrollView) {
            scrollView.OverScrollMode = OverScrollMode.Never;
            scrollView.ScrollChanged += ScrollViewOnScrollChanged;
        }

        private void ToolbarChildViewAdded(object sender, EventArgs e) {
            // If this was proper C# we'd use `child as ActionMenuView`
            // but we have to use JavaCast which doesn't support JavaTryCast
            // and writing a generic one using Java.Lang.Class.FromType() doesn't work either.

            for (int i = 0; i < (_toolbar.ChildCount - 1); i++) {
                var child = _toolbar.GetChildAt(i);

                child.LayoutChange -= ActionMenuViewOnLayoutChange;
            }

            if (_toolbar.ChildCount > 1) {
                var actionMenuView = _toolbar.GetChildAt(_toolbar.ChildCount - 1);

                UpdateToolbarTitleLayout(actionMenuView);

                actionMenuView.LayoutChange += ActionMenuViewOnLayoutChange;
            }
        }

        private void ActionMenuViewOnLayoutChange(object sender, View.LayoutChangeEventArgs e) {
            UpdateToolbarTitleLayout((View) sender);
        }

        private void UpdateToolbarTitleLayout(View actionMenuView) {
            // This may seem odd but it's the most reliable way (sometimes the parent view
            // doesn't cause the child view to update, even with .ForceLayout())
            for (int i = 0; i < _toolbarTitleInnerLayoutView.ChildCount; i++) {
                var child = _toolbarTitleInnerLayoutView.GetChildAt(i);
                child.SetPadding(0, 0, actionMenuView.Width, 0);
            }
        }

        private void TitleLayoutViewOnLayoutChange(object sender, View.LayoutChangeEventArgs args) {
            var height = _flexibleSpaceView.Height;
            var targetHeight = _titleLayoutView.Height;

            if (height != targetHeight) {
                _flexibleSpaceViewResizeAnimation?.Cancel();

                if (!_contentAnimatedIn) {
                    _contentInAnimators?.Apply(x => x.Cancel());

                    var animator = _flexibleSpaceViewResizeAnimation = ValueAnimator.OfInt(height, targetHeight);
                    animator.SetDuration(AnimationHelper.DurationMedium);
                    animator.SetInterpolator(AnimationHelper.GetFastOutSlowInInterpolator(this));
                    animator.Update += (s, e) => {
                        var val = (int)animator.AnimatedValue;

                        _flexibleSpaceView.LayoutParameters.Height = val;
                        _flexibleSpaceView.RequestLayout();
                    };
                    animator.AnimationEnd += (s, e) => {
                        _flexibleSpaceViewResizeAnimation = null;
                        AnimateContentIn(false);
                    };
                    animator.Start();
                }
                else {
                    _flexibleSpaceView.LayoutParameters.Height = targetHeight;
                    _flexibleSpaceView.RequestLayout();
                }
            }

            UpdateFlexibleSpace();

            FlexibleSpaceOnHeightChanged(targetHeight);
        }

        private void ScrollViewOnScrollChanged(object sender, ScrollChangedEventArgs e) {
            UpdateFlexibleSpace(e.ScrollY);
        }

        private void UpdateTabLayout() {
            _titleLayoutView.SetPaddingBottom(TabsVisible ? (_toolbarHeight - Resources.GetDimensionPixelSize(Resource.Dimension.margin_s)) : 0);
            _tabLayout.SetVisible(TabsVisible);
            _tabShadow.SetVisible(TabsVisible);
            UpdateFlexibleSpace();
        }

        protected void UpdateFlexibleSpace(int? scrollY = null) {
            const float parallaxDelta = .8f;

            int adjustedScrollY = scrollY.GetValueOrDefault(_scrollView.CurrentScrollY);
            int tabsHeight = TabsVisible ? _toolbarHeight : 0;
            int flexibleSpaceHeight = Math.Max(0, _titleLayoutView.Height - (_toolbarHeight + tabsHeight));

            if (adjustedScrollY < 0) {
                adjustedScrollY = 0;
            }
            else if (flexibleSpaceHeight < adjustedScrollY) {
                adjustedScrollY = flexibleSpaceHeight;
            }

            _flexibleSpaceView.TranslationY = -adjustedScrollY;
            _tabLayout.TranslationY = (_titleLayoutView.Height - _tabLayout.Height) - adjustedScrollY;
            // Import we're using the _titleLayoutView's height, it's the actual view
            // whereas the _flexibleSpaceView is just set from _titleLayoutView. This
            // can cause inconsistencies if _flexibleSpaceView's values are used between
            // layouts
            _tabShadow.TranslationY = _titleLayoutView.Height - adjustedScrollY;

            float toolbarHalfHeight = _toolbarHeight * .5f;

            // The point at which the large title should be hidden and the toolbar title begins to show
            float tippingPoint = flexibleSpaceHeight - toolbarHalfHeight;

            _titleLayoutView.Alpha = 1 - (adjustedScrollY / tippingPoint);
            _titleLayoutView.TranslationY = -adjustedScrollY * parallaxDelta;

            float delta = ((adjustedScrollY - tippingPoint) / toolbarHalfHeight);

            _toolbarTitleLayoutView.Alpha = adjustedScrollY > tippingPoint ? delta : 0;
            _toolbarTitleLayoutView.TranslationY = toolbarHalfHeight * parallaxDelta * (1 - delta);
        }

        protected virtual void FlexibleSpaceOnHeightChanged(int newHeight) {
            var contentView = this.FindViewById(Resource.Id.Content);

            contentView.SetPaddingTop(newHeight);
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            ViewModelListener
                .Listen(nameof(this.ViewModel.Color), SetColor);

            SetColor();
        }

        private void SetColor() {
            if (!this.IsInMonoLimbo() && _toolbar != null && this.ViewModel.ColorOrNull != null) {
                SetColor(new MaterialPalette(this.ViewModel.ColorOrNull.Value.ToNativeColor()));
            }
        }

        protected virtual void SetColor(MaterialPalette palette) {
            _toolbar.SetBackgroundColor(Android.Graphics.Color.Transparent);//palette.PrimaryColor);

            if (OS.HasLollipops) {
                Window.SetStatusBarColor(palette.PrimaryColorDark);
            }
        }

        #region Options Menu

        public override bool OnCreateOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.TecDetailView, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Android.Resource.Id.Home:
                    this.ViewModel.GoBack();
                    return true;
                case Resource.Id.ActionEdit:
                    this.ViewModel.EditCommand.Execute(null);
                    return true;
                default:
                    return false;
            }
        }

        #endregion

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data) {
            if (requestCode == DroidPresenter.CustomRequestCode && resultCode == Result.Ok && data.GetBooleanExtra(DroidPresenter.EntityDeletionIntentExtra, false)) {
                Finish();
            }
            else {
                base.OnActivityResult(requestCode, resultCode, data);
            }
        }
    }

    // Unfortunately this isn't something that we can do with the OnLayoutChange event
    [Register("mystudylife.droid.views.EntityViewContentLayout")]
    class EntityViewContentLayout : FrameLayout {
        private readonly int _toolbarHeight;

        protected EntityViewContentLayout(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {}
        public EntityViewContentLayout(Context context) : this(context, null) { }
        public EntityViewContentLayout(Context context, IAttributeSet attrs) : this(context, attrs, 0) {}
        public EntityViewContentLayout(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr) {
            var tv = new TypedValue();
            if (context.Theme.ResolveAttribute(Resource.Attribute.actionBarSize, tv, true)) {
                _toolbarHeight = TypedValue.ComplexToDimensionPixelSize(tv.Data, Resources.DisplayMetrics);
            }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

            var parent = (View) this.Parent;

            var contentHeight = this.MeasuredHeight - this.PaddingTop;
            var scrollThreshold = parent.MeasuredHeight - this.PaddingTop;
            var collapseTitleScrollThreshold = parent.MeasuredHeight - _toolbarHeight;

            if (scrollThreshold < contentHeight && contentHeight < collapseTitleScrollThreshold) {
                SetMeasuredDimension(
                    MeasuredWidth,
                    collapseTitleScrollThreshold + this.PaddingTop
                );
            }
        }
    }
}