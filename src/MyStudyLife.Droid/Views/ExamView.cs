using Android.App;
using Android.OS;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI.ViewModels.View;

namespace MyStudyLife.Droid.Views {
    [Activity(Label = "Exam")]
    public class ExamView : EntityView<Exam, ExamViewModel> {
        private MvxPropertyChangedListener _listener;
        private RobotoTextView _dateCaptionTextView;

        public override int LayoutResourceId => Resource.Layout.View_Exam;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);

            _dateCaptionTextView = FindViewById<RobotoTextView>(Resource.Id.DateCaptionText);
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.State), ViewModelOnStateChange);
        }

        private void ViewModelOnStateChange() {
            var vm = this.ViewModel;
            var sub = _dateCaptionTextView;

            if (vm != null && sub != null) {
                if (vm.State == ExamState.InTheNext3Days || vm.State == ExamState.InTheNext7Days) {
                    sub.SetRobotoTypeface(RobotoTypeface.Medium);
                }
                else {
                    sub.SetRobotoTypeface(RobotoTypeface.Regular);
                }
            }
        }
    }
}