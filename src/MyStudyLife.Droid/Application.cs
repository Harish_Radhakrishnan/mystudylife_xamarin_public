using System;
using System.Collections.Generic;
using Android.App;
using Android.Media;
using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.App;
using MvvmCross.Platforms.Android.Views;
using MyStudyLife.Droid.Services;
using MyStudyLife.UI;

#if !DEBUG
using MyStudyLife.Droid.Platform;
#endif

namespace MyStudyLife.Droid {
    [Application]
    public class MslDroidApp : MvxAndroidApplication<Setup, MslApp>
    {
        public MslDroidApp(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public override void OnCreate() {
#if DEBUG
            StrictMode.SetThreadPolicy(new StrictMode.ThreadPolicy.Builder().DetectAll().PenaltyLog().Build());
            StrictMode.SetVmPolicy(new StrictMode.VmPolicy.Builder().DetectLeakedSqlLiteObjects().DetectLeakedClosableObjects().PenaltyLog().Build());
#endif

            base.OnCreate();

#if !DEBUG
            DroidBugReporter.Attach();
#endif
            
            AppCompatDelegate.CompatVectorFromResourcesEnabled = true;
            
            // Notification channels required in Oreo and above
            if (Build.VERSION.SdkInt >= BuildVersionCodes.O) {
                CreateNotificationChannels();
            }
            
            ScheduledTaskHelper.EnsureScheduledTask(Context);
        }

        private void CreateNotificationChannels() {
        
            var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            
            notificationManager.CreateNotificationChannels(new List<NotificationChannel> {
                CreateNotificationChannel(Resource.String.ClassRemindersNotificationChannelId, Resource.String.ClassRemindersNotificationChannelName),
                CreateNotificationChannel(Resource.String.ExamRemindersNotificationChannelId, Resource.String.ExamRemindersNotificationChannelName),
                CreateNotificationChannel(Resource.String.TaskRemindersNotificationChannelId, Resource.String.TaskRemindersNotificationChannelName),
                CreateNotificationChannel(Resource.String.DefaultNotificationChannelId, Resource.String.DefaultNotificationChannelName)
            });
        }
        
        private NotificationChannel CreateNotificationChannel(int idRes, int nameRes, NotificationImportance importance = NotificationImportance.High) {
        
            var defaultSoundUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
            var audioAttributes = new AudioAttributes.Builder()
                .SetContentType(AudioContentType.Music)
                .Build();
            
            var channel = new NotificationChannel(GetString(idRes), GetString(nameRes), importance);
            channel.EnableLights(true);
            channel.EnableVibration(true);
            channel.SetVibrationPattern(new long[] {0, 100, 200, 300});
            channel.SetSound(defaultSoundUri, audioAttributes);
            
            return channel;
        }
    }
}