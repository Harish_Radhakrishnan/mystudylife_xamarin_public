using System;
using System.Globalization;
using System.Threading;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using AndroidX.AppCompat.App;
using Java.Lang.Reflect;
using MvvmCross.Platforms.Android.Views;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using DateFormat = Android.Text.Format.DateFormat;

namespace MyStudyLife.Droid {
	[Activity(
		Label = "My Study Life",
		MainLauncher = true,
		Theme = "@style/Theme.Splash",
		NoHistory = true,
		ScreenOrientation = ScreenOrientation.User)]
	public class SplashScreen : MvxSplashScreenActivity
	{
		private bool _wasResumed;

		public SplashScreen() : base(Resource.Layout.SplashScreen) { }

		protected override void OnCreate(Bundle bundle) {
			AppCompatDelegate.DefaultNightMode = AppCompatDelegate.ModeNightNo;
			base.OnCreate(bundle);

            // From http://stackoverflow.com/a/11438245/491468
            // Prevents menu button being used for overflow menu
            // in action bar.
		    try {
		        ViewConfiguration config = ViewConfiguration.Get(this);
		        Field menuKeyField = Java.Lang.Class.FromType(typeof (ViewConfiguration)).GetDeclaredField("sHasPermanentMenuKey");
		        if (menuKeyField != null) {
		            menuKeyField.Accessible = true;
		            menuKeyField.SetBoolean(config, false);
		        }
		    }
// ReSharper disable once EmptyGeneralCatchClause
		    catch {}

            // Workaround for https://bugzilla.xamarin.com/show_bug.cgi?id=23544
            // and https://bugzilla.xamarin.com/show_bug.cgi?id=27725
		    try {
		        var shouldBe24Hour = DateFormat.Is24HourFormat(this);
		        var monoIs24Hour = DateTimeFormat.Is24Hour;

		        if (shouldBe24Hour != monoIs24Hour ||
		            String.IsNullOrEmpty(CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0])) {

		            var newCultureInfo = new CultureInfo(CultureInfo.CurrentCulture.Name);
		            var newUiCultureInfo = new CultureInfo(CultureInfo.CurrentUICulture.Name);

		            DateTimeFormat.EnsureDayNames(newCultureInfo.DateTimeFormat);
		            DateTimeFormat.EnsureDayNames(newUiCultureInfo.DateTimeFormat);

		            if (shouldBe24Hour && !monoIs24Hour) {
		                DateTimeFormat.Ensure24Hours(newCultureInfo.DateTimeFormat);
		                DateTimeFormat.Ensure24Hours(newUiCultureInfo.DateTimeFormat);
		            }
		            else if (!shouldBe24Hour && monoIs24Hour) {
		                DateTimeFormat.Ensure12Hours(newCultureInfo.DateTimeFormat);
		                DateTimeFormat.Ensure12Hours(newUiCultureInfo.DateTimeFormat);
		            }

                    // As per Material Design guidelines http://www.google.com/design/spec/patterns/data-formats.html#data-formats-date-time
		            if (!shouldBe24Hour) {
                        newCultureInfo.DateTimeFormat.AMDesignator = newCultureInfo.DateTimeFormat.AMDesignator.ToUpper();
                        newCultureInfo.DateTimeFormat.PMDesignator = newCultureInfo.DateTimeFormat.PMDesignator.ToUpper();

                        newUiCultureInfo.DateTimeFormat.AMDesignator = newCultureInfo.DateTimeFormat.AMDesignator.ToUpper();
                        newUiCultureInfo.DateTimeFormat.PMDesignator = newCultureInfo.DateTimeFormat.PMDesignator.ToUpper();
		            }

		            Thread.CurrentThread.CurrentCulture = newCultureInfo;
		            Thread.CurrentThread.CurrentUICulture = newUiCultureInfo;
		            CultureInfo.DefaultThreadCurrentCulture = newCultureInfo;
		            CultureInfo.DefaultThreadCurrentUICulture = newUiCultureInfo;
		        }
		    }
// ReSharper disable once EmptyGeneralCatchClause
		    catch {}
		}
        
		protected override void OnRestart() {
			base.OnRestart();

			_wasResumed = true;
		}

        protected override object GetAppStartHint(object hint = null) =>
            new AppStartHint(_wasResumed ? AppStartMode.Resume : AppStartMode.New);
	}
}