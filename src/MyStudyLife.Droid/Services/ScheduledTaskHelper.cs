using System;
using Android.App;
using Android.Content;
using Android.OS;
using MyStudyLife.Droid.Platform;

namespace MyStudyLife.Droid.Services {
    public static class ScheduledTaskHelper {
        public static void EnsureScheduledTask(Context context) {
            var alarmManager = (AlarmManager)context.GetSystemService(Context.AlarmService);

            var scheduledTaskIntent = new Intent(context, typeof(ScheduledTaskReceiver));

            var alarmType = AlarmType.RtcWakeup;// This will "wake" the phone up if it's asleep
            var triggerAtMilliSec = DateTime.UtcNow.AddSeconds(30d).GetTimeInMillis();
            var taskIntervalAtMilliSec = (long)DroidConfig.ScheduledTaskInterval.TotalMilliseconds;
            PendingIntent pendingIntent;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.S) {
                // Starting with Android 12 (Build.VERSION_CODES.S), it will be required to explicitly specify the mutability of PendingIntents on creation
                pendingIntent = PendingIntent.GetBroadcast(context, 0, scheduledTaskIntent, PendingIntentFlags.Immutable);
            }
            else {
                // If this intent already exists, it will be updated
                pendingIntent = PendingIntent.GetBroadcast(context, 0, scheduledTaskIntent, PendingIntentFlags.UpdateCurrent);
            }

            alarmManager.SetRepeating(
            alarmType,
            triggerAtMilliSec,
            taskIntervalAtMilliSec,
            pendingIntent
            );

        }
    }
}