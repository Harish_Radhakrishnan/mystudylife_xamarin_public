using Android.App;
using Android.Content;

namespace MyStudyLife.Droid.Services {
    [BroadcastReceiver(Exported =false)]
    [IntentFilter(new[] { "android.intent.action.BOOT_COMPLETED" })]
    public class OnBootReceiver : BroadcastReceiver {
        public override void OnReceive(Context context, Intent intent) {
            ScheduledTaskHelper.EnsureScheduledTask(context);
        }
    }
}