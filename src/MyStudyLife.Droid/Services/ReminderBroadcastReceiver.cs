using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Util;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Presenters;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using MyStudyLife.Data.UI;
using MyStudyLife.Reminders;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.View;
using Newtonsoft.Json;
using TaskStackBuilder = AndroidX.Core.App.TaskStackBuilder;

namespace MyStudyLife.Droid.Services {
    // Shows notifications
    [BroadcastReceiver]
    public class ReminderBroadcastReceiver : BroadcastReceiver {
        public override void OnReceive(Context context, Intent intent) {
            var reminderJson = intent.GetStringExtra("REMINDER");

            if (String.IsNullOrEmpty(reminderJson)) {
                Log.Error("ReminderBroadcastReceiver", "Intent received without REMINDER extra, ignoring");
                return;
            }

            var reminder = JsonConvert.DeserializeObject<Reminder>(reminderJson);

            Log.Debug("ReminderBroadcastReceiver", $"Displaying reminder type={reminder.Type}");

            var notificationManager = (NotificationManager)context.GetSystemService(Context.NotificationService);
            var defaultSoundUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);

            var notificationChannelIdRes = reminder.Type switch {
                ReminderType.Class => Resource.String.ClassRemindersNotificationChannelId,
                ReminderType.Exam => Resource.String.ExamRemindersNotificationChannelId,
                ReminderType.Task => Resource.String.TaskRemindersNotificationChannelId,
                _ => Resource.String.DefaultNotificationChannelId
            };

            var builder = Build.VERSION.SdkInt >= BuildVersionCodes.O
                ? new Notification.Builder(context, context.GetString(notificationChannelIdRes))
#pragma warning disable 618
                : new Notification.Builder(context);
#pragma warning restore 618

            builder
                .SetTicker(reminder.Title)
                .SetContentTitle(reminder.Title)
                .SetAutoCancel(true)
#pragma warning disable 618
                .SetSound(defaultSoundUri);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop) {
                var color = reminder.Color;

                if (color != null) {
                    // TODO (low priority): check user is eligable for extended colors
                    builder.SetColor(UserColors.GetColor(color, true).Argb);
                }
            }

            if (reminder.IsMultiline && Build.VERSION.SdkInt >= BuildVersionCodes.JellyBean) {
                var bigStyle = new Notification.InboxStyle();

                bigStyle.SetBigContentTitle(reminder.Title);

                var lineCount = reminder.Lines.Count();
                var displayLineCount = lineCount;
                bool displayMore = false;

                if (lineCount > 5) {
                    displayLineCount = 4;
                    displayMore = true;
                }

                for (int i = 0; i < displayLineCount; i++) {
                    bigStyle.AddLine(reminder.Lines.ElementAt(i));
                }

                if (displayMore) {
                    bigStyle.AddLine("+ " + (lineCount - displayLineCount) + " more");
                }

                builder.SetStyle(bigStyle);
                builder.SetContentText(reminder.Lines.First() + (lineCount > 1 ? (" + " + (lineCount - 1) + " more") : String.Empty));
            }
            else {
                builder.SetContentText(reminder.Message);
            }

            builder.SetSmallIcon(reminder.Type switch {
                ReminderType.Class => Resource.Drawable.ic_stat_class_icon,
                ReminderType.Exam => Resource.Drawable.ic_stat_exam_icon,
                ReminderType.Task => Resource.Drawable.ic_stat_task_icon,
                _ => Resource.Drawable.ic_stat_icon
            });

            var setup = MvxAndroidSetupSingleton.EnsureSingletonAvailable(context);
            setup.EnsureInitialized();

            if (reminder.Type != ReminderType.Task && reminder.Guid.HasValue) {
                var request = MvxViewModelRequest.GetDefaultRequest(
                    reminder.Type == ReminderType.Class ? typeof(ClassViewModel) : typeof(ExamViewModel)
                );

                request.ParameterValues = new Dictionary<string, string> {
                    {"Guid", reminder.Guid.Value.ToString() }
                };

                if (reminder.Type == ReminderType.Class) {
                    request.ParameterValues.Add("Date", DateTimeEx.Now.ToString("s"));

                    if (reminder.Extras != null && reminder.Extras.TryGetValue("TimeGuid", out var timeGuid)) {
                        request.ParameterValues.Add("TimeGuid", timeGuid);
                    }
                }

                // TODO: AndroidViewModelRequestTranslator should work for nested fragments...
                var translator = Mvx.IoCProvider.Resolve<IMvxAndroidViewModelRequestTranslator>();

                var stackBuilder = TaskStackBuilder.Create(context);

                stackBuilder.AddNextIntent(GetCoreViewIntent<DashboardViewModel>(context));
                stackBuilder.AddNextIntent(translator.GetIntentFor(request));

                builder.SetContentIntent(stackBuilder.GetPendingIntent(0, (int)PendingIntentFlags.CancelCurrent | (int)PendingIntentFlags.Mutable));
            }
            else if (reminder.Type == ReminderType.Task) {
                // This doesn't work well with `context.PackageManager.GetLaunchIntentForPackage("com.virblue.mystudylife")`
                // as it launches the splash screen (actually resuming it) which calls `TriggerFirstNavigate` which tries
                // to commit a fragment on `CoreView`, but it fails because it has been paused as the splash screen is now
                // the top activity.
                builder.SetContentIntent(PendingIntent.GetActivity(context, 0, GetCoreViewIntent<TasksViewModel>(context), PendingIntentFlags.CancelCurrent | PendingIntentFlags.Mutable));
            }
            else {
                Mvx.IoCProvider.Resolve<ILogger<ReminderBroadcastReceiver>>().LogError("Unexpected reminder payload received by ReminderBroadcastReceiver");

                builder.SetContentIntent(PendingIntent.GetActivity(context, 0, GetCoreViewIntent<DashboardViewModel>(context), PendingIntentFlags.CancelCurrent | PendingIntentFlags.Mutable));
            }

            var notification = builder.Build();
            notification.LedARGB = 0x1c8d76;

            notificationManager.Notify(reminder.Id, notification);
        }

        private Intent GetCoreViewIntent<TViewModel>(Context context) where TViewModel : IMvxViewModel {
            var request = Mvx.IoCProvider.Resolve<IMvxNavigationSerializer>().Serializer.SerializeObject(MvxViewModelRequest<TViewModel>.GetDefaultRequest());
            var intent = new Intent(context, typeof(Views.CoreView));
            intent.AddFlags(ActivityFlags.SingleTop);
            intent.PutExtra(MvxAndroidViewPresenter.ViewModelRequestBundleKey, request);

            return intent;
        }
    }
}