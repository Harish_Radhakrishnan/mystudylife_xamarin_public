using System.Threading;
using Android.App;
using Android.Media;
using Firebase.Messaging;
using Java.Lang;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.Authorization;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Services;
using Exception = System.Exception;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Services
{
    [Service(Exported = false)]
    [IntentFilter(new[] {"com.google.firebase.MESSAGING_EVENT"})]
    public class MslFirebaseMessagingService : FirebaseMessagingService {
        public override void OnMessageReceived(RemoteMessage remoteMessage) {
            if (remoteMessage.Data != null && remoteMessage.Data.Count > 0) {
                if (!DroidBackgroundTaskHelper.EnsureInitialized(this)) return;

                Mvx.IoCProvider.Resolve<IPushNotificationService>().HandleRawNotification(remoteMessage.Data);
            }
            else {
                var remoteNotification = remoteMessage.GetNotification();

                var defaultSoundUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);

                var notificationChannelId = remoteNotification.ChannelId ?? GetString(Resource.String.DefaultNotificationChannelId);
                
                var notification = new Notification.Builder(this, notificationChannelId)
                    .SetSmallIcon(Resource.Drawable.ic_stat_icon)
                    .SetColor(GetColor(Resource.Color.accent))
                    .SetContentTitle(remoteNotification.Title)
                    .SetContentText(remoteNotification.Body)
                    .SetAutoCancel(true)
#pragma warning disable 618
                    .SetSound(defaultSoundUri)
#pragma warning restore 618
                    .Build();
                
                var notificationManager = (NotificationManager)GetSystemService(NotificationService);
                
                notificationManager.Notify((int)JavaSystem.CurrentTimeMillis(), notification);
            }
        }

        public override void OnNewToken(string token) => Task.Run(async () => await RegisterTokenAsync(token));
        
        private async Task RegisterTokenAsync(string token, CancellationToken ct = default) {
            // Don't call EnsureInitialized here as it causes a deadlock on first application launch.
            // The PNS token is handled by EnsureNotificationChannelAsync after sign in
            if (
                !Mvx.IoCProvider.TryResolve<ILogger<MslFirebaseMessagingService>>(out var logger) ||
                !Mvx.IoCProvider.TryResolve<IPushNotificationService>(out var pns) ||
                !Mvx.IoCProvider.TryResolve<IAuthorizationService>(out var authService)) {

                return;
            }

            try {
                if (!authService.IsAuthorized) {
                    return;
                }
                
                logger.LogInformation("Firebase registering new token");
                
                await pns.RegisterTokenAsync(token);
            }
            catch (Exception ex) {
                logger.LogWarning("Failed to register new Firebase token", ex);
            }
        }
    }
}