using Android.Content;
using Android.OS;
using Android.Runtime;
using Mindscape.Raygun4Net;
using MyStudyLife.Data;
using MyStudyLife.Droid.Platform;
using MyStudyLife.Reminders;
using System;
using Android.Util;
using System.Collections.Generic;
using MvvmCross;
using MvvmCross.Exceptions;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Services {
    /// <summary>
    ///     A task that should be run every x hours.
    /// </summary>
    [BroadcastReceiver]
    public class ScheduledTaskReceiver : BroadcastReceiver {
        private PowerManager.WakeLock _wakeLock;

        public ScheduledTaskReceiver() { }
        protected ScheduledTaskReceiver(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) { }

        public override void OnReceive(Context context, Intent intent) {
            var pm = (PowerManager)context.GetSystemService(Context.PowerService);

            // Prevents CPU sleeping (but allows display to turn off)
            _wakeLock = pm.NewWakeLock(WakeLockFlags.Partial, "MSL_SCHEDULED_TASK");

            _wakeLock.Acquire();
            
            var pendingResult = GoAsync();

            Task.Run(async () => await OnReceiveAsync(context)).ContinueWith(task => {
                pendingResult.Finish();
                _wakeLock.Release();
            });
        }

        private async Task OnReceiveAsync(Context context) {
            try {
                if (!DroidBackgroundTaskHelper.EnsureInitialized(context)) return;

                // Unsure whether this will work in the background and
                // difficult to test hence the try-catch.
                try {
                    if (RaygunClient.Current == null) {
                        var user = Mvx.IoCProvider.Resolve<IUserStore>().GetCurrentUser();

                        DroidBugReporter.Attach(user?.Id.ToString());
                    }
                }
// ReSharper disable once EmptyGeneralCatchClause
                catch {}

                await Mvx.IoCProvider.Resolve<IReminderService>().RunAsync();
            }
            catch (Exception ex) {
                Log.Error("ScheduledTaskReceiver", "Caught exception whilst running scheduled task: " + ex.ToLongString());
                new DroidBugReporter().Send(ex, true, new List<string> { "ScheduledTaskReceiver", "Background" });
            }
        }
    }
}