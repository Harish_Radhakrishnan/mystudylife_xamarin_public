using Android.Views;

namespace MyStudyLife.Droid.Extensions {
    public static class ViewExtensions {
        public static void SetPaddingLeft(this View view, int value) => SetPadding(view, left: value);
        public static void SetPaddingTop(this View view, int value) => SetPadding(view, top: value);
        public static void SetPaddingRight(this View view, int value) => SetPadding(view, right: value);
        public static void SetPaddingBottom(this View view, int value) => SetPadding(view, bottom: value);

        public static void SetPadding(this View view, int? left = null, int? top = null, int? right = null, int? bottom = null) {
            view.SetPadding(
                left.GetValueOrDefault(view.PaddingLeft),
                top.GetValueOrDefault(view.PaddingTop),
                right.GetValueOrDefault(view.PaddingRight),
                bottom.GetValueOrDefault(view.PaddingBottom)
            );
        }
    }
}