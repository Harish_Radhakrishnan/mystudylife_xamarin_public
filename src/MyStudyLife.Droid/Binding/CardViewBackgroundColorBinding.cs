using System;
using Android.Graphics;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.Target;
using Microsoft.Extensions.Logging;
using AndroidX.CardView.Widget;

namespace MyStudyLife.Droid.Binding {
    class CardViewBackgroundColorTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<CardView>("CardBackgroundColor", icon => new CardViewBackgroundColorTargetBinding(icon));
        }

        public new CardView Target => (CardView)base.Target;

        public override Type TargetType => typeof(object);

        public CardViewBackgroundColorTargetBinding(object target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var cardView = target as CardView;

            if (cardView == null) {
                return;
            }

            if (value == null) {
                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogWarning("Tried to set card background color to null, falling back to transparent.");
            }

            cardView.SetCardBackgroundColor((Color?) value ?? Color.Transparent);
        }
    }
}