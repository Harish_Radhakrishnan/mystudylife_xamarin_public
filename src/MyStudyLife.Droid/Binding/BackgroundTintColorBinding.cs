using System;
using Android.Graphics;
using Android.Views;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class BackgroundColorFilterTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<View>("BackgroundColorFilter", view => new BackgroundColorFilterTargetBinding(view));
        }

        public override Type TargetType => typeof(Color);

        public BackgroundColorFilterTargetBinding(View target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var view = target as View;

            if (view == null || value == null) {
                return;
            }

            view.Background?.Mutate().SetColorFilter((Color)value, PorterDuff.Mode.SrcIn);
        }
    }
}