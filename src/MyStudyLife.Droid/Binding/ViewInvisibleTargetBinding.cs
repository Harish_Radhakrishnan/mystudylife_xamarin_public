using Android.Views;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Binding.Extensions;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class ViewInvisibleTargetBinding : MvxBaseViewVisibleBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<View>("Invisible", view => new ViewInvisibleTargetBinding(view));
        }

        public ViewInvisibleTargetBinding(View target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            ((View)target).Visibility = value.ConvertToBoolean() ? ViewStates.Invisible : ViewStates.Visible;
        }
    }
}