﻿using System;
using System.Linq;
using System.Text;
using Android.Text;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target.Construction;
using MyStudyLife.UI.Helpers;
using Android.Text.Method;
using Android.Text.Style;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Droid.Common;

namespace MyStudyLife.Droid.Binding {
    public class AutoLinkerTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TextView>("AutoLinkText", textView => new AutoLinkerTargetBinding(textView));
        }

        public AutoLinkerTargetBinding(TextView target) : base(target) {
            target.MovementMethod = LinkMovementMethod.Instance;
        }

        public override Type TargetType => typeof (string);

        protected override void SetValueImpl(object target, object value) {
            var textView = target as TextView;

            if (textView == null) {
                return;
            }

            var text = value as string;

            if (String.IsNullOrEmpty(text)) {
                textView.Text = String.Empty;
                return;
            }

            var matches = AutoLinker.GetMatches(text).ToList();

            if (matches.Count == 0) {
                textView.Text = text;
                return;
            }

            var sb = new StringBuilder();

            int lastMatchEndIndex = 0;

            for (int i = 0; i < matches.Count; i++) {
                var match = matches[i];

                if (match.StartIndex > 0) {
                    sb.Append(text.Substring(lastMatchEndIndex, match.StartIndex - lastMatchEndIndex));
                }

                sb.Append("<a href=\"").Append(match.Uri).Append("\">").Append(match.Text).Append("</a>");

                lastMatchEndIndex = match.EndIndex;

                if (i == (matches.Count - 1)) {
                    if (lastMatchEndIndex < (text.Length - 1)) {
                        sb.Append(text.Substring(lastMatchEndIndex, text.Length - lastMatchEndIndex));
                    }
                }
            }

            // replacing "\n" with "<br />" is required as newlines are not preserved, instead
            // they get replaced with spaces, causing the user's data to not display as it
            // would be assumed to.
            var spannedText = new SpannableString(Html.FromHtml(sb.ToString().Replace("\n", "<br />")));
            var spans = spannedText.GetSpans(0, sb.ToString().Length, Java.Lang.Class.FromType(typeof(URLSpan)));

            foreach (URLSpan span in spans) {
                var start = spannedText.GetSpanStart(span);
                var end = spannedText.GetSpanEnd(span);

                var url = span.URL;
                spannedText.RemoveSpan(span);
                spannedText.SetSpan(new MSLURLSpan(url), start, end, SpanTypes.ExclusiveExclusive);
            }

            textView.SetText(spannedText, TextView.BufferType.Spannable);
        }
    }
}
