using System;
using Android.Widget;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding;
using MvvmCross.Binding.Extensions;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Platforms.Android.Binding.Views;

namespace MyStudyLife.Droid.Binding {
    public class GridViewSelectedItemBinding : MvxAndroidTargetBinding {
        public const string Name = "SelectedItem";

        private object _currentValue;

        private new MvxGridView Target => (MvxGridView)base.Target;

        public GridViewSelectedItemBinding(object target) : base(target) { }

        public override Type TargetType => typeof(object);

        public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

        public override void SubscribeToEvents() {
            base.SubscribeToEvents();

            ((GridView)this.Target).ItemClick += TargetOnItemClick;
        }

        private void TargetOnItemClick(object sender, AdapterView.ItemClickEventArgs e) {
            SetSelectedItem(Target, e.Position);

            var newValue = Target.Adapter.ItemsSource.ElementAt(e.Position);

            if (!newValue.Equals(_currentValue)) {
                _currentValue = newValue;
                FireValueChanged(newValue);
            }
        }

        protected override void SetValueImpl(object target, object value) {
            var view = (GridView)target;

            if (value != null && value != _currentValue) {
                var index = ((IMvxAdapter)view.Adapter).GetPosition(value);
                if (index < 0) {
                    Mvx.IoCProvider.Resolve<ILogger<GridViewSelectedItemBinding>>().LogTrace("Value not found for spinner {0}", value.ToString());
                    return;
                }
                _currentValue = value;

                view.Post(() => SetSelectedItem(view, index));
            }
        }

        private void SetSelectedItem(GridView target, int index) {
            for (int i = 0; i < target.ChildCount; i++) {
                target.GetChildAt(i).Activated = i == index;
            }
        }
    }
}