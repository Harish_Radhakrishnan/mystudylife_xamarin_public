using System;
using Android.App;
using Android.Views;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.Droid.Binding {
    /// <summary>
    ///     Binding to dynamically set the timeline offset.
    /// </summary>
    /// <remarks>
    ///     For this to work correctly, the entries list view must have a top offset (padding/margin)
    ///     of half the timeline's height.
    /// </remarks>
    public class TimelineOffsetTargetBinding : MvxAndroidTargetBinding {
        private static float _timelineHeight;
        private static float _pastEntryHeight, _currentEntryHeight, _futureEntryHeight;
        private static float _pastEntrySpacing, _currentEntrySpacing, _futureEntrySpacing;

        static TimelineOffsetTargetBinding() {
            var resources = Application.Context.Resources;

            _timelineHeight = resources.GetDimension(Resource.Dimension.agenda_timeline_height);

            _pastEntryHeight = resources.GetDimension(Resource.Dimension.ae_past_height);
            _currentEntryHeight = resources.GetDimension(Resource.Dimension.ae_current_height);
            _futureEntryHeight = resources.GetDimension(Resource.Dimension.ae_future_height);

            _pastEntrySpacing = resources.GetDimension(Resource.Dimension.ae_past_spacing);
            _currentEntrySpacing = resources.GetDimension(Resource.Dimension.ae_current_spacing);
            _futureEntrySpacing = resources.GetDimension(Resource.Dimension.ae_future_spacing);
        }

        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<View>("TimelineOffset", timeline => new TimelineOffsetTargetBinding(timeline));
        }
        
        private ViewPropertyAnimator _currentAnimator;

        public new View Target => (View)base.Target;

        public override Type TargetType => typeof(TimelineOffset);

        public TimelineOffsetTargetBinding(object target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            if (target == null || value == null) {
                return;
            }

            var timeline = (View)target;
            var offset = (TimelineOffset)value;

            bool hasPastEntries = offset.PastEntries > 0;
            // In most scenarios, there will only ever be a "part" current entry, that is, an entry
            // with an offset based on the minutes remaining of the current entry
            float wholeCurrentEntries = (float)Math.Floor(offset.CurrentEntriesPosition);
            float fractionalCurrentEntries = offset.CurrentEntries - wholeCurrentEntries;

            float offsetY =
                // Offset by each past entry and spacing AFTER the past entries,
                // except for the last one
                (offset.PastEntries * _pastEntryHeight) +
                (Math.Max(0f, offset.PastEntries - 1) * _pastEntrySpacing) +
                // Where there are multiple, offset by any current entries
                (wholeCurrentEntries * _currentEntryHeight) +
                // Offset for spacing BEFORE fractional entry, if we have past entries (since we only pad the bottom)
                (hasPastEntries ? (offset.CurrentEntries * _currentEntrySpacing) : 0) +
                // Offset by position within current entry
                ((offset.CurrentEntriesPosition - wholeCurrentEntries) * _currentEntryHeight) +
                // Offset to align with top of future entry if any (and no current or past entries since we only pad the bottom)
                ((offset.CurrentEntries == 0 && hasPastEntries && offset.FutureEntries > 0) ? _futureEntrySpacing : 0);

            float offsetYPx = (int)Math.Round(offsetY);

            timeline.Post(() => {
                _currentAnimator?.Cancel();

                _currentAnimator = timeline.Animate()
                    .TranslationY(offsetYPx)
                    .SetDuration(300)
                    .SetDefaultInterpolator(timeline.Context);

                _currentAnimator.Start();
            });
        }
    }
}