using System;
using System.Collections.Generic;
using MvvmCross.Binding.Bindings.Target.Construction;
using MikePhil.Charting.Charts;
using MikePhil.Charting.Data;
using MikePhil.Charting.Animation;
using MyStudyLife.UI.Data;
using Android.Graphics;
using MyStudyLife.Data.UI;
using System.Linq;
using MvvmCross.Platforms.Android.Binding.Target;
using JavaInt = Java.Lang.Integer;

namespace MyStudyLife.Droid.Binding {
    public class PieChartTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<PieChart>("DialData", pieChart => new PieChartTargetBinding(pieChart));
        }

        public PieChartTargetBinding(PieChart target) : base(target) {

        }

        public override Type TargetType => typeof(List<DialDataset>);

        protected override void SetValueImpl(object target, object value) { 

            var pieChart = target as PieChart;

            if (pieChart == null) {
                return;
            }

            pieChart.Clear();

            var dataset = (value as IList<DialDataset>) ?? ((value as IEnumerable<DialDataset>) ?? Enumerable.Empty<DialDataset>())?.ToList();

            var length = Math.Max(dataset.Count, 1);
            var entries = new PieEntry[length];
            var colors = new JavaInt[length];
            var count = 0;

            for (var i = 0; i < dataset.Count; i++) {
                var e = dataset[i];

                count += e.Count;
                entries[i] = new PieEntry(e.Value, String.Empty);
                colors[i] = (JavaInt)e.Color.ToArgb();
            }
            if (dataset.Count == 0) {
                entries[0] = new PieEntry(1, String.Empty);
                colors[0] = (JavaInt)UserColors.Empty.ToArgb();
                pieChart.AnimateY(0);
            }
            else {
                pieChart.AnimateY(600, Easing.EaseInOutCubic);
            }

            var dataSet = new PieDataSet(entries, String.Empty) {
                SliceSpace = length > 1 ? 2f : 0,
                Colors = colors
            };

            var data = new PieData(dataSet);
            data.SetValueTextColor(Color.Transparent.ToArgb());
            pieChart.Data = data;
        }
    }
}