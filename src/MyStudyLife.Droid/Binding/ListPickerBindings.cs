using System;
using System.Collections;
using Android.Views;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Droid.Widgets;

namespace MyStudyLife.Droid.Binding {
	public class ListPickerSelectedItemBinding : MvxAndroidTargetBinding {
		public const string Name = "SelectedItem";
        public static void Register<TView>(IMvxTargetBindingFactoryRegistry registry) where TView : View, IListPicker {
            registry.RegisterCustomBindingFactory<TView>("SelectedItem", target => new ListPickerSelectedItemBinding(target));
        }

		public new IListPicker Target => (IListPicker)base.Target;

        public ListPickerSelectedItemBinding(IListPicker target) : base(target) {}

        public override void SubscribeToEvents() {
            this.Target.SelectedItemChanged += TargetOnValueChanged;
	    }

	    protected override void SetValueImpl(object target, object value) {
			((IListPicker)target).SelectedItem = value;
		}

		public override Type TargetType => typeof(object);

        public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

        protected override void Dispose(bool isDisposing) {
			if (isDisposing) {
                this.Target.SelectedItemChanged -= TargetOnValueChanged;
			}

			base.Dispose(isDisposing);
		}

		private void TargetOnValueChanged(object sender, EventArgs e) {
			if (this.Target == null) return;

			this.FireValueChanged(this.Target.SelectedItem);
		}
	}

	public class ListPickerItemsSourceBinding : MvxAndroidTargetBinding {
		public const string Name = "ItemsSource";
	    public static void Register<TView>(IMvxTargetBindingFactoryRegistry registry) where TView : View, IListPicker {
            registry.RegisterCustomBindingFactory<TView>("ItemsSource", target => new ListPickerItemsSourceBinding(target));
	    }

		public new IListPicker Target => (IListPicker) base.Target;

        public ListPickerItemsSourceBinding(IListPicker target)
			: base(target) { }

		protected override void SetValueImpl(object target, object value) {
            ((IListPicker)target).ItemsSource = (IEnumerable) value;
		}

		public override Type TargetType => typeof(IEnumerable);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;
    }
}