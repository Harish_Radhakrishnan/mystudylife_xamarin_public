using System;
using Android.Graphics;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.Plugin.Color.Platforms.Android;

namespace MyStudyLife.Droid.Binding {
    public class TextColorWithFallbackTargetBinding : MvxAndroidTargetBinding {
        private readonly Color _originalColor;

        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TextView>("TextColorWithFallback", textView => new TextColorWithFallbackTargetBinding(textView));
        }

        public TextColorWithFallbackTargetBinding(TextView target) : base(target) {
            _originalColor = new Color(target.CurrentTextColor);
        }

        public override Type TargetType => typeof(System.Drawing.Color);

        protected override void SetValueImpl(object target, object value) {
            (target as TextView)?.SetTextColor(((System.Drawing.Color?)value)?.ToNativeColor() ?? _originalColor);
        }
    }
}