using System;
using AnderWeb.DiscreteSeekBar;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class DiscreteSeekBarTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<DiscreteSeekBar>("Progress", target => new DiscreteSeekBarTargetBinding(target));
        }

        public new DiscreteSeekBar Target => (DiscreteSeekBar) base.Target;

        public DiscreteSeekBarTargetBinding(DiscreteSeekBar target) : base(target) { }

        public override void SubscribeToEvents() {
            this.Target.ProgressChanged += TargetOnProgressChanged;
        }

        protected override void SetValueImpl(object target, object value) {
            ((DiscreteSeekBar)target).Progress = (int)value;
        }

        public override Type TargetType => typeof(int);

        public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                this.Target.ProgressChanged -= TargetOnProgressChanged;
            }

            base.Dispose(isDisposing);
        }

        private void TargetOnProgressChanged(object sender, DiscreteSeekBar.ProgressChangedEventArgs e) {
            this.FireValueChanged(((DiscreteSeekBar)sender).Progress);
        }
    }
}