using System;
using Android.Views;
using Android.Widget;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class MvxRadioGroupSelectedIndexBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<RadioGroup>("SelectedIndex", radioGroup => new MvxRadioGroupSelectedIndexBinding(radioGroup));
        }

        private bool _checkChangedPrevented;
        private int _selectedIndex = -1;

        public new RadioGroup Target => (RadioGroup)base.Target;

        public override Type TargetType => typeof(object);

        public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

        public int SelectedIndex {
            get => _selectedIndex;
            set {
                if (value != _selectedIndex) {
                    _selectedIndex = value;

                    FireValueChanged(value);
                }
            }
        }

        public MvxRadioGroupSelectedIndexBinding(RadioGroup radioGroup) : base(radioGroup) { }

        public override void SubscribeToEvents() {
            this.Target.CheckedChange += TargetOnCheckedChange;
            this.Target.ChildViewAdded += TargetOnChildViewAdded;
        }

        private void TargetOnChildViewAdded(object sender, ViewGroup.ChildViewAddedEventArgs e) {
            var radioGroup = Target;

            if (_selectedIndex == radioGroup.ChildCount - 1) {
                _checkChangedPrevented = true;
                radioGroup.Check(radioGroup.GetChildAt(_selectedIndex).Id);
                _checkChangedPrevented = false;
            }
        }

        private void TargetOnCheckedChange(object sender, RadioGroup.CheckedChangeEventArgs e) {
            if (_checkChangedPrevented) {
                return;
            }

            var radioGroup = Target;
            var checkedId = e.CheckedId;

            if (checkedId == View.NoId) {
                SelectedIndex = -1;
                return;
            }

            for (var i = radioGroup.ChildCount - 1; i >= 0; i--) {
                if (checkedId == radioGroup.GetChildAt(i).Id) {
                    SelectedIndex = i;
                    return;
                }
            }

            SelectedIndex = -1;

            Mvx.IoCProvider.Resolve<ILogger<MvxRadioGroupSelectedIndexBinding>>().LogError("RadioGroup id not found: {0}", checkedId);
        }

        protected override void SetValueImpl(object target, object value) {
            var radioGroup = target as RadioGroup;

            if (radioGroup == null) {
                return;
            }

            try {
                _checkChangedPrevented = true;

                _selectedIndex = (int)value;

                if (_selectedIndex < 0 || _selectedIndex >= radioGroup.ChildCount) {
                    radioGroup.ClearCheck();
                }
                else {
                    radioGroup.Check(radioGroup.GetChildAt(_selectedIndex).Id);
                }
            }
            finally {
                _checkChangedPrevented = false;
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                var radioGroup = Target;

                if (radioGroup != null) {
                    radioGroup.CheckedChange -= TargetOnCheckedChange;
                    radioGroup.ChildViewAdded -= TargetOnChildViewAdded;
                }
            }

            base.Dispose(disposing);
        }
    }
}