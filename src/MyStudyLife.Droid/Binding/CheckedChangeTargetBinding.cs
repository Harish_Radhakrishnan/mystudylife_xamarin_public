using System;
using System.Windows.Input;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MvvmCross.WeakSubscription;

namespace MyStudyLife.Droid.Binding {
    public class CheckedChangeTargetBinding : MvxAndroidTargetBinding {
        public const string Name = "CheckedChange";

        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<CompoundButton>(Name, target => new CheckedChangeTargetBinding(target));
        }

        private ICommand _command;
        private IDisposable _canExecuteSubscription;
        private readonly EventHandler<EventArgs> _canExecuteEventHandler;

        public CompoundButton Checkable => (CompoundButton)base.Target;

        public override Type TargetType => typeof(ICommand);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public CheckedChangeTargetBinding(CompoundButton target) : base(target) {
            _canExecuteEventHandler = OnCanExecuteChanged;
            target.CheckedChange += OnCheckedChange;
        }

        private void OnCheckedChange(object sender, EventArgs args) {
            if (_command == null)
                return;

            if (!_command.CanExecute(Checkable.Checked))
                return;

            _command.Execute(Checkable.Checked);
        }

        protected override void SetValueImpl(object target, object value) {
            if (_canExecuteSubscription != null) {
                _canExecuteSubscription.Dispose();
                _canExecuteSubscription = null;
            }

            _command = value as ICommand;
            if (_command != null) {
                _canExecuteSubscription = _command.WeakSubscribe(_canExecuteEventHandler);
            }

            RefreshEnabledState();
        }

        private void RefreshEnabledState() {
            var checkable = Checkable;
            if (checkable == null)
                return;

            var shouldBeEnabled = false;
            if (_command != null) {
                shouldBeEnabled = _command.CanExecute(null);
            }

            checkable.Enabled = shouldBeEnabled;
        }

        private void OnCanExecuteChanged(object sender, EventArgs e) {
            RefreshEnabledState();
        }

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                var checkable = Checkable;
                if (checkable != null) {
                    checkable.CheckedChange -= OnCheckedChange;
                }
                if (_canExecuteSubscription != null) {
                    _canExecuteSubscription.Dispose();
                    _canExecuteSubscription = null;
                }
            }

            base.Dispose(isDisposing);
        }
    }
}