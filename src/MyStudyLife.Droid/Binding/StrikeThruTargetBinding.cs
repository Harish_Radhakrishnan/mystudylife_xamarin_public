using System;
using Android.Widget;
using Android.Graphics;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class StrikeThruTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<TextView>("StrikeThru", target => new StrikeThruTargetBinding(target));
        }

        public override Type TargetType => typeof(bool);

        public StrikeThruTargetBinding(TextView target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var tv = (TextView)target;

            tv.PaintFlags = ((bool)value) ? (tv.PaintFlags | PaintFlags.StrikeThruText) : (tv.PaintFlags & ~PaintFlags.StrikeThruText);
        }
    }
}