using System;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
	/// <summary>
	///		A binding used for input
	///		errors which both sets the text and
	///		visibility of a TextView.
	/// </summary>
	public class ErrorTargetBinding : MvxAndroidTargetBinding {
		public const string Name = "Error";

		public new TextView Target => (TextView) base.Target;

        public override Type TargetType => typeof(TextView);

        public override MvxBindingMode DefaultMode => MvxBindingMode.OneWay;

        public ErrorTargetBinding(TextView target) : base(target) {}
        
	    #region Overrides of MvxConvertingTargetBinding

	    protected override void SetValueImpl(object target, object value) {
	        var t = ((TextView) target);

            t.SetText((string)value, TextView.BufferType.Normal);

            t.Visibility = value == null ? ViewStates.Gone : ViewStates.Visible;
	    }

	    #endregion
	}
}