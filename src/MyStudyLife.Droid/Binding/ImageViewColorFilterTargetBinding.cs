using System;
using Android.Graphics;
using Android.Widget;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding {
    public class ImageViewColorFilterTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<ImageView>("ColorFilter", view => new ImageViewColorFilterTargetBinding(view));
        }

        public override Type TargetType => typeof(Color);

        public ImageViewColorFilterTargetBinding(ImageView target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var view = target as ImageView;

            if (view == null || value == null) {
                return;
            }

            view.Drawable?.Mutate().SetColorFilter((Color)value, PorterDuff.Mode.SrcAtop);
        }
    }
}