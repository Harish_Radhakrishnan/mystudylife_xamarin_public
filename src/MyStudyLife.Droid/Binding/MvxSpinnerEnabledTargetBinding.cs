using System;
using Android.Views;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Platforms.Android.Binding.Target;

namespace MyStudyLife.Droid.Binding
{
    public class MvxViewEnabledTargetBinding : MvxAndroidTargetBinding {
		public MvxViewEnabledTargetBinding(View target) : base(target) { }

		public override Type TargetType => typeof (bool);

        protected override void SetValueImpl(object target, object value) {
			var view = (View) target;

			if (view == null) return;

			if (!(value is bool)) {
				Mvx.IoCProvider.Resolve<ILogger<MvxViewEnabledTargetBinding>>().LogWarning("Value must be a non null bool.");

				return;
			}

			view.Enabled = (bool)value;
		}
	}
}