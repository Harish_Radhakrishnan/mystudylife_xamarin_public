using System;
using Android.App;
using Android.Views;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Platforms.Android.Binding.Target;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Widgets;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Binding {
    // TODO: Can we remove this?
    class TaskStateIndicatorTargetBinding : MvxAndroidTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<Icon>("TaskState", icon => new TaskStateIndicatorTargetBinding(icon));
        }

        public new Icon Target => (Icon)base.Target;

        public override Type TargetType => typeof(object);

        public TaskStateIndicatorTargetBinding(object target) : base(target) { }

        protected override void SetValueImpl(object target, object value) {
            var icon = target as Icon;
            var task = value as Task;

            if (icon == null) {
                return;
            }

            if (task == null) {
                icon.Visibility = ViewStates.Gone;
                return;
            }

            var context = Application.Context;

            if (task.IsOverdue || task.IsIncompleteAndDueSoon) {
                icon.Glyph = Glyph.Attention;
                icon.SetTextColor(ContextCompatEx.GetColor(context, task.IsOverdue ? Resource.Color.attention_foreground : Resource.Color.warning_foreground));
                icon.Visibility = ViewStates.Visible;
            }
            else if (task.IsComplete) {
                icon.Glyph = Glyph.Tick;
                icon.SetTextColor(ContextCompatEx.GetColor(context, Resource.Color.okay_foreground));
                icon.Visibility = ViewStates.Visible;
            }
            else {
                icon.Visibility = ViewStates.Gone;
            }
        }
    }
}