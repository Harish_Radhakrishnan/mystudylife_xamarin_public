## Support Library ##
-keep public class androidx.appCompat.** { *; }
-keep public class androidx.core.** { *; }
-keep public class androidx.recyclerview.widget.** { *; }

-keep public class * extends android.support.v4.view.ActionProvider {
    public <init>(android.content.Context);
}

## Support Design Library ##
-dontwarn android.support.design.**
-keep class android.support.design.** { *; }
-keep interface android.support.design.** { *; }
-keep public class android.support.design.R$* { *; }

## Google Analytics ##
-keep class com.google.analytics.** { *; }

## Google Play Services ##
# https://developer.android.com/google/play-services/setup.html#Proguard
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

## MPAndroidChart ##
-keep class com.github.mikephil.charting.** { *; }

## MaterialDateTimePicker ##
-keep class wdullaer.materialdatetimepicker.** { *; }

## Xamarin SDK - fixes xamarin.android.net.OldAndroidSSLSocketFactory ##
-keep class xamarin.android.** { *; <init>(...); }

-keep class ffimageloading.cross.** { *; }
-keep class FFImageLoading.** { *; }