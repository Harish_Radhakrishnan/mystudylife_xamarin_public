using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using MyStudyLife.Data;
using MyStudyLife.Data.Serialization;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Platform {
	public class DroidStorageProvider : IStorageProvider {
        #region Files

        private readonly static ConcurrentDictionary<string, object> _fileLocks = new ConcurrentDictionary<string, object>();

        private Lazy<string> _appDataDirectoryPath = new Lazy<string>(() =>
            Environment.GetFolderPath(Environment.SpecialFolder.Personal)
        );

        private string AppDataDirectoryPath => _appDataDirectoryPath.Value;

        private string GetFullFilePath(string filePath) {
            Check.NotNullOrEmpty(ref filePath, nameof(filePath));

            return Path.Combine(AppDataDirectoryPath, filePath);
        }

        private object GetFileLock(string fileName) => _fileLocks.GetOrAdd(fileName, new object());

        /// <summary>
        /// Loads the contents from a file with the given name.
        ///
        /// Creates the file if it does not already exist, returning
        /// an empty string.
        /// </summary>
        /// <param name="filePath">The path of the file to load.</param>
        /// <returns>The file contents.</returns>
        public Task<string> LoadFileContentsAsync(string filePath) => Task.Run(() => {
            string fullFilePath = GetFullFilePath(filePath);

            lock (GetFileLock(fullFilePath)) {
                if (new FileInfo(fullFilePath).Exists) {
                    return File.ReadAllText(fullFilePath);
                }
            }

            return null;
        });

        public Task<Stream> FileOpenReadAsync(string filePath) {

            return Task.FromResult<Stream>(FileOpenRead(filePath));
        }

        public Stream FileOpenRead(string filePath) {
            string fullFilePath = GetFullFilePath(filePath);

            return File.Exists(fullFilePath) ? File.OpenRead(fullFilePath) : null;
        }

        /// <summary>
        /// Writes the given <paramref name="contents"/> to a
        /// file with the given <paramref name="fileName"/>.
        ///
        /// Creates the file if it does not already exist.
        /// </summary>
        /// <param name="filePath">The name of the file.</param>
        /// <param name="contents">The file contents eg. text, json xml.</param>
        public Task WriteFileContentsAsync(string filePath, string contents) => Task.Run(() => {
            string fullFileName = GetFullFilePath(filePath);

            lock (GetFileLock(fullFileName)) {
                File.WriteAllText(fullFileName, contents);
            }
        });

		/// <summary>
		/// Removes all files from the local store.
		/// </summary>
		public Task RemoveFilesAsync() => Task.Run(() => {
            DirectoryInfo di = new DirectoryInfo(AppDataDirectoryPath);

            var files = di.GetFiles("*.json").ToList();

            foreach (var f in files) {
                lock (GetFileLock(f.FullName)) {
                    f.Delete();
                }
            }
        });

        public DateTimeOffset GetLastModified(string path) => new FileInfo(Path.Combine(AppDataDirectoryPath, path)).LastWriteTime;

        #endregion

        #region Settings

        private const string IsolatedStorageSettings = "IsolatedStorageSettings";

        /// <summary>
        /// Gets the value for the stored application
        /// setting with the given <paramref name="settingName"/>.
        /// </summary>
        /// <typeparam name="T">The type stored in the setting.</typeparam>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The setting's value.</returns>
        public T GetSetting<T>(string settingName) {
			var prefs = Application.Context.GetSharedPreferences(IsolatedStorageSettings, FileCreationMode.MultiProcess);

			if (prefs.Contains(settingName)) {
				string val = prefs.GetString(settingName, null);

				if (val != null) {
                    T obj = default(T);

                    var tType = typeof(T);

                    if (tType == typeof(string)) {
                        obj = (T)(object)val?.Trim('"'); // In case it was previously stored using JSON
                    }
                    else if (!String.IsNullOrWhiteSpace(val)) {
                        if (typeof(IDeserializeJson).IsAssignableFrom(tType)) {
                            obj = (T)Activator.CreateInstance(tType);

                            ((IDeserializeJson)obj).SetValuesFromJson(val);
                        }
                        else {
                            obj = JsonConvert.DeserializeObject<T>(val);
                        }
                    }

                    return obj;
                }
			}

			return default(T);
		}

		/// <summary>
		/// Adds or updates the application setting
		/// with the given <paramref name="settingName"/>
		/// and <paramref name="settingValue."/>
		/// </summary>
		/// <typeparam name="T">The type to store in the setting.</typeparam>
		/// <param name="settingName">The name of the setting.</param>
		/// <param name="settingValue">The setting's value.</param>
		public void AddOrUpdateSetting<T>(string settingName, T settingValue) {
			var prefs = Application.Context.GetSharedPreferences(IsolatedStorageSettings, FileCreationMode.MultiProcess);

			var editor = prefs.Edit();

			editor.PutString(settingName, JsonConvert.SerializeObject(settingValue));

			editor.Commit();
		}

		/// <summary>
		/// Removes the application setting
		/// with the given <paramref name="settingName"/>
		/// </summary>
		/// <param name="settingName">The name of the setting.</param>
		public void RemoveSetting(string settingName) {
			var prefs = Application.Context.GetSharedPreferences(IsolatedStorageSettings, FileCreationMode.MultiProcess);

			var editor = prefs.Edit();

			editor.Remove(settingName);

			editor.Commit();
		}

        #endregion
    }
}