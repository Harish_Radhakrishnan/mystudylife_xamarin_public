using System;
using System.Collections;
using System.Collections.Generic;
using Mindscape.Raygun4Net;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.Droid.Platform {
    public class DroidBugReporter : BugReporter {
        /// <summary>
        ///     Checks if the exception is caused by https://code.google.com/p/android/issues/detail?id=63929,
        ///     in KitKat 4.4.2 and below.
        /// </summary>
        public static bool IsARTWeakReferenceBug(Exception ex) => ex != null && ex.Message.Contains("'jobject' must not be IntPtr.Zero");

        protected override void SetUserImpl(string uid) {
            // Checking for null is required in a unit test setting
            if (RaygunClient.Current != null) {
                RaygunClient.Current.User = uid;
            }
        }

        protected override void SendImpl(Exception exception, IList<string> tags, IDictionary customUserData)
            => RaygunClient.Current?.Send(exception, tags, customUserData);

        /// <summary>
        ///     Listens to and sends all unhandled exceptions and unobserved task exceptions.
        /// </summary>
        /// <param name="userId">
        ///     The current user's identifier.
        /// </param>
        public static void Attach(string userId = null) {
            if (RaygunClient.Current == null) {
                RaygunClient.Attach(DroidConfig.RaygunApiKey, userId);
            }
        }
    }
}