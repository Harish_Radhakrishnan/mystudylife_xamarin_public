using MvvmCross.ViewModels;

namespace MyStudyLife.Droid.Platform {
	public interface IFragmentHost {
		bool Show(MvxViewModelRequest request);
	}
}