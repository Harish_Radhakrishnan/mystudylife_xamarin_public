using System;
using System.Threading.Tasks;
using AFollestad.MaterialDialogs;
using Android.Widget;
using MyStudyLife.Droid.Common;
using MyStudyLife.UI.Services;
using MvvmCross;
using MvvmCross.Platforms.Android;

namespace MyStudyLife.Droid.Platform {
	public class DroidDialogService : DialogServiceBase, IDialogService {
	    #region Implementation of IDialogService
    
	    protected override void RequestConfirmationImpl(string title, string message, ConfirmationType confirmationType, Action<bool> onActioned) {
            int positiveTextRes = Resource.String.Common_Yes;
            int negativeTextRes = Resource.String.Common_No;

	        if (confirmationType != ConfirmationType.YesNo) {
	            negativeTextRes = Resource.String.Common_Cancel;

	            switch (confirmationType) {
	                case ConfirmationType.Discard:
                        positiveTextRes = Resource.String.Discard;
	                    break;
                    case ConfirmationType.Delete:
                        positiveTextRes = Resource.String.Common_Delete;
                        break;
	            }
	        }

	        var builder = new MaterialDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity)
	            .PositiveText(positiveTextRes)
	            .NegativeText(negativeTextRes)
	            .Callback(new DialogHelper.ButtonCallbackShim(() => onActioned(true), () => onActioned(false)))
	            .AutoDismiss(false);

	        if (String.IsNullOrEmpty(message)) {
	            builder.Content(title);
	        }
            else {
	            builder.Title(title);
	            builder.Content(message);
	        }

            builder.Show();
	    }

	    protected override void RequestConfirmationImpl(string title, string message, string cancelText, string confirmText, Action<bool> onActioned) {
	        throw new Exception("We have overridden RequestConfirmationImpl so this should not be called.");
	    }

	    /// <summary>
		///     Shows a blocking dismissible message box.
		/// </summary>
        public void ShowDismissible(string title, string message) {
            new MaterialDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity)
                .Title(title)
                .Content(message)
                .PositiveText(Resource.String.Common_Ok)
                .Show();
		}

		/// <summary>
		///     Shows a non-blocking notification - for example
		///     a toast notification in Windows 8 / Phone.
		/// </summary>
		public void ShowNotification(string title, string message) {
			Toast.MakeText(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity, message, ToastLength.Long).Show();
		}

		public void ShowCustom(string title, string message, CustomDialogAction leftAction, CustomDialogAction rightAction) {
		    var builder = new MaterialDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity)
		        .Title(title)
		        .Content(message)
		        .AutoDismiss(false);
            
			if (leftAction != null && rightAction != null) {
				if (leftAction.IsDefault || (!leftAction.IsDefault && !rightAction.IsDefault)) {
					builder.PositiveText(leftAction.Content)
					       .NegativeText(rightAction.Content)
                           .Callback(new DialogHelper.ButtonCallbackShim(
                               leftAction.TryTriggerOnChosen,
                               rightAction.TryTriggerOnChosen
                           ));
				}
				else {
                    builder.PositiveText(rightAction.Content)
                           .NegativeText(leftAction.Content)
                           .Callback(new DialogHelper.ButtonCallbackShim(
                               rightAction.TryTriggerOnChosen,
                               leftAction.TryTriggerOnChosen
                           ));
				}
			}
			else {
				CustomDialogAction action = leftAction ?? rightAction;

				builder.NeutralText(action.Content)
                       .Callback(new DialogHelper.ButtonCallbackShim(null, onNeutral: action.TryTriggerOnChosen));
			}

			builder.Show();
		}

	    public Task<CustomDialogResult> ShowCustomAsync(string title, string message, string positiveAction, string negativeAction) {
            var tcs = new TaskCompletionSource<CustomDialogResult>();

            new MaterialDialog.Builder(Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity)
                .Title(title)
                .Content(message)
                .PositiveText(positiveAction)
                .NegativeText(negativeAction)
                .Callback(new DialogHelper.ButtonCallbackShim(
                    () => tcs.TrySetResult(new CustomDialogResult(true)),
                    () => tcs.TrySetResult(new CustomDialogResult(false))
                ))
                .AutoDismiss(false)
                .Show();

            return tcs.Task;
	    }

	    #endregion
	}
}