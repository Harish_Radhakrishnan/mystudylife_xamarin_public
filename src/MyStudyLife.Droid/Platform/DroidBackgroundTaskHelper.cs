using Android.Content;
using Mindscape.Raygun4Net;
using MvvmCross;
using MvvmCross.Platforms.Android.Core;
using MyStudyLife.Data;
using MyStudyLife.Versioning;

namespace MyStudyLife.Droid.Platform {
    public static class DroidBackgroundTaskHelper {
        /// <summary>
        ///     Ensures the IoC is initialized and checks whether there
        ///     are any pending upgrades to the app which should be performed
        ///     in the foreground.
        /// </summary>
        /// <returns>
        ///     False if there are pending upgrades.
        /// </returns>
        public static bool EnsureInitialized(Context context) {
            var setupSingleton = MvxAndroidSetupSingleton.EnsureSingletonAvailable(context);
            setupSingleton.EnsureInitialized();
            
            try {
                if (RaygunClient.Current == null) {
                    var user = Mvx.IoCProvider.Resolve<IUserStore>().GetCurrentUser();

                    DroidBugReporter.Attach(user?.Id.ToString());
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return !Mvx.IoCProvider.Resolve<IUpgradeService>().RequiresUpgrade;
        }
    }
}