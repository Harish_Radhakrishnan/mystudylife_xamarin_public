using System;
using Java.Util;

namespace MyStudyLife.Droid.Platform {
    public sealed class DroidDateTimeProvider : IDateTimeProvider {
        // https://bugzilla.xamarin.com/show_bug.cgi?id=22648
        public DateTime Now {
            get {
                var c = Calendar.Instance;

                return new DateTime(
                    c.Get(CalendarField.Year),
                    c.Get(CalendarField.Month) + 1,
                    c.Get(CalendarField.DayOfMonth),
                    c.Get(CalendarField.HourOfDay),
                    c.Get(CalendarField.Minute),
                    c.Get(CalendarField.Second),
                    c.Get(CalendarField.Millisecond),
                    DateTimeKind.Local
                );
            }
        }
    }
}