using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using MyStudyLife.Net;

namespace MyStudyLife.Droid.Platform {
	public class DroidNetworkProvider : INetworkProvider, IDisposable {
		private ConnectivityManager _connectivityManager;
		private readonly NetworkStatusBroadcastReceiver _broadcastReceiver;

		private ConnectivityManager ConnectivityManager {
			get {
				return _connectivityManager ??= (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
			}
		}

		#region Implementation of INetworkProvider

		public event NetworkConnectivityChangedDelegate ConnectivityChanged;

		/// <summary>
		/// Indicates whether the device
		/// is currently connected to the internet.
		/// </summary>
		public bool IsOnline { get; private set; }

		/// <summary>
		/// Indicates whether the current connection
		/// has been disabled by the user's settings.
		/// </summary>
		public bool IsConnectionDisabled => false;

        public DroidNetworkProvider() {
			_broadcastReceiver = new NetworkStatusBroadcastReceiver();
			_broadcastReceiver.ConnectionStatusChanged += BroadcastReceiverOnConnectionStatusChanged;

			Application.Context.RegisterReceiver(_broadcastReceiver, new IntentFilter(ConnectivityManager.ConnectivityAction));

			Invalidate();
		}

		private async void BroadcastReceiverOnConnectionStatusChanged(object sender, EventArgs e) {
			await this.GetCanUseConnectionAsync();
		}

		private async void Invalidate() {
			await this.GetCanUseConnectionAsync();
		}

		/// <summary>
		/// Gets a value which indicates whether
		/// the current connection can be used
		/// taking into account the network status
		/// and user preferences.
		/// </summary>
		public Task<bool> GetCanUseConnectionAsync() {
			return Task.Run(() => {
				var activeConnection = ConnectivityManager.ActiveNetworkInfo;

				bool wasOnline = this.IsOnline;
				this.IsOnline = activeConnection != null && activeConnection.IsConnected;

				// If the connection has changed.
				if (this.IsOnline != wasOnline) {
					ConnectivityChanged?.Invoke(this, new NetworkConnectivityChangedEventArgs(wasOnline, false, this.IsOnline, false));
				}

				return IsOnline && !IsConnectionDisabled;
			});
		}

		#endregion

		#region Implementation of IDisposable

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose() {
			Application.Context.UnregisterReceiver(_broadcastReceiver);
		}

		#endregion
	}

	[BroadcastReceiver]
	public class NetworkStatusBroadcastReceiver : BroadcastReceiver {
		public event EventHandler ConnectionStatusChanged;

		#region Overrides of BroadcastReceiver

		public override void OnReceive(Context context, Intent intent) {
            ConnectionStatusChanged?.Invoke(this, EventArgs.Empty);
        }

		#endregion
	}
}