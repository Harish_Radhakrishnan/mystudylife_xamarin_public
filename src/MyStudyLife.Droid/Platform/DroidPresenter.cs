using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Core;
using MvvmCross.Platforms.Android.Presenters;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MyStudyLife.Authorization;
using MyStudyLife.Droid.Common;
using MyStudyLife.Droid.Views;
using MyStudyLife.Droid.Views.Input;
using MyStudyLife.UI;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.UI.ViewModels.Schools;
using MyStudyLife.UI.ViewModels.Settings;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Platform
{
    public interface IMslDroidPresenter {
        void RegisterCoreView(CoreView coreView);
    }

    public class DroidPresenter : MvxAndroidViewPresenter, IMslDroidPresenter {
        public const int CustomRequestCode = 7;
        public const string EntityDeletionIntentExtra = "ENTITY_DELETED";

        private INavigationService NavigationService => Mvx.IoCProvider.Resolve<INavigationService>();
        private IAuthorizationService AuthService => Mvx.IoCProvider.Resolve<IAuthorizationService>();
        private IAppStateManager AppStateManager => Mvx.IoCProvider.Resolve<IAppStateManager>();

        public DroidPresenter(IEnumerable<Assembly> androidViewAssemblies) : base(androidViewAssemblies) {
            AddPresentationHintHandler<EntityDeletedPresentationHint>(hint => {
                var intent = new Intent();
                intent.PutExtra(EntityDeletionIntentExtra, true);
                
                CurrentActivity.SetResult(Result.Ok, intent);

                if (OS.HasLollipops) {
                    CurrentActivity.FinishAfterTransition();
                }
                else {
                    CurrentActivity.Finish();
                }

                return Task.FromResult(true);
            });
            AddPresentationHintHandler<WelcomeWizardCompletedPresentationHint>(hint => {
                Show(MvxViewModelRequest.GetDefaultRequest(hint.AddClasses ? typeof(ScheduleViewModel) : typeof(DashboardViewModel)));

                // Closing welcome view is handled in Show
                return Task.FromResult(false);
            });
            AddPresentationHintHandler<AuthorizationChangedPresentationHint>(hint => {
                if (hint.Cause == Authorization.AuthStatusChangeCause.SignIn) return Task.FromResult(false);

                var activity = CurrentActivity;

                // Removes back stack
                var intent = new Intent(Application.Context, typeof(SignInView));
                intent.AddFlags(ActivityFlags.ClearTop);
                ShowIntent(intent);

                if (OS.CheckSystemVersion(BuildVersionCodes.JellyBean) && activity != null) {
                    activity.FinishAffinity();
                }
                else {
                    GetCoreView()?.Finish();
                }

                return Task.FromResult(true);
            });
            AddPresentationHintHandler<OAuthWebFlowResultPresentationHint>(async hint => {
                var state = await AppStateManager.GetStateAsync();
                
                if (AuthService.IsAuthorized && state.User != null) {
                    return await NavigationService.NavigateFirstViewAuthorized(state.User);
                }
                
                // This will often be the DeepLinkActivity, so we show a new SignInView
                // which has SingleTop+ClearTop flags applied, which brings the existing 
                // activity to the front
                return await Show(new MvxViewModelRequest {
                    ViewModelType = typeof(SignInViewModel),
                    ParameterValues = SignInViewModel.NavObject.ForOAuthResult(hint.Result).ToSimplePropertyDictionary(),
                    PresentationValues = new Dictionary<string, string> {
                        ["ExternalRequest"] = Boolean.TrueString
                    }
                });
            });
        }

        private readonly Type[] _coreViewViewModelTypes = {
            typeof (DashboardViewModel),
            typeof (CalendarWeekViewModel),
            typeof (CalendarMonthViewModel),
            typeof (TasksViewModel),
            typeof (ExamsViewModel),
            typeof (ScheduleViewModel),
            typeof (SettingsViewModel)
        };

        private readonly WeakReference<CoreView> _coreViewReference = new WeakReference<CoreView>(null, true);

        public void RegisterCoreView(CoreView coreView) {
            _coreViewReference.SetTarget(coreView);
        }

        private CoreView GetCoreView() {
            if (!_coreViewReference.TryGetTarget(out var coreView)) {
                return null;
            }

            if (coreView.IsInMonoLimbo() || coreView.IsFinishing || coreView.IsDestroyed) {
                return null;
            }

            return coreView;
        }

        public override Task<bool> Show(MvxViewModelRequest request) {
            var currentActivity = CurrentActivity;

            // SignInView here ensures SplashScreen is killed
            // Potentially fixes #308
            bool killPrevious = request.ViewModelType == typeof(UpgradeViewModel) || request.ViewModelType == typeof(WelcomeViewModel) || request.ViewModelType == typeof(SchoolWelcomeViewModel);

            if (request.ViewModelType == typeof(SignInViewModel)) {
                var intent = this.CreateIntentForRequest(request);
                // Clear top ensures that when we're signing in, we return to the
                // existing SignInView whilst finishing Chrome custom tabs.
                intent.AddFlags(ActivityFlags.ClearTop | ActivityFlags.SingleTop);

                ShowIntent(intent);

                killPrevious = !(currentActivity is SignInView);
            }
            else if (_coreViewViewModelTypes.Contains(request.ViewModelType)) {
                CoreView coreView = GetCoreView();

                var serializedRequest = NavigationSerializer.Serializer.SerializeObject(request);

                if (coreView != null) {
                    var bundle = new Bundle();
                    bundle.PutString(ViewModelRequestBundleKey, serializedRequest);

                    coreView.Show(request, bundle);
                }
                else {
                    var intent = new Intent(Application.Context, typeof (CoreView));
                    intent.AddFlags(ActivityFlags.ClearTop);
                    intent.PutExtra(ViewModelRequestBundleKey, serializedRequest);

                    ShowIntent(intent);

                    killPrevious = true;
                }
            }
            else if (typeof(IInputViewModel).IsAssignableFrom(request.ViewModelType)) {
                var intent = this.CreateIntentForRequest(request);
                intent.SetFlags(intent.Flags & ~ActivityFlags.NewTask);

                var activity = CurrentActivity;

                if (activity == null) {
                    Mvx.IoCProvider.Resolve<ILogger<DroidPresenter>>().LogWarning("Cannot Resolve current top activity");
                    return Task.FromResult(false);
                }

                // Don't animate between entity views and input views (eg. TaskView -> TaskInputView)
                // or input views and other input views (eg. ClassInputView -> ClassTimeInputView)
                if (activity is IEntityView || activity is IInputView) {
                    intent.AddFlags(ActivityFlags.NoAnimation);
                }

                // No animation bundle
                activity.StartActivityForResult(intent, CustomRequestCode);
            }
            else if (request.ViewModelType == typeof(SubjectsViewModel)) {
                ShowIntent(CreateIntentForRequest(request), Bundle.Empty); // No animation
            }
            else {
                ShowIntent(CreateIntentForRequest(request));
            }

            NavigationService.BackStack.Push(request.ViewModelType);

            // SingleTop or ClearTop just doesn't seem to work without NewTask
            // which creates a horrible transition
            if (killPrevious && currentActivity != null) {
                if (OS.CheckSystemVersion(BuildVersionCodes.JellyBean)) {
                    currentActivity.FinishAffinity();
                }
                else {
                    currentActivity.Finish();
                }
            }

            return Task.FromResult(true);
        }
        
        private void ShowIntent(Intent intent) {
            if (OS.HasLollipops && CurrentActivity != null) {
                ShowIntent(intent, ActivityOptions.MakeSceneTransitionAnimation(CurrentActivity).ToBundle());
            }
            else {
                base.ShowIntent(intent, Bundle.Empty);
            }
        }

        public override Task<bool> Close(IMvxViewModel viewModel) {
            if (viewModel is WelcomeViewModel) {
                Show(MvxViewModelRequest<DashboardViewModel>.GetDefaultRequest());
            }

            var activity = CurrentActivity;

            var currentView = activity as IMvxView;

            if (currentView == null) {
                Mvx.IoCProvider.Resolve<ILogger<DroidPresenter>>().LogWarning("Ignoring close for viewmodel - rootframe has no current page");
                return Task.FromResult(false);
            }

            if (currentView.ViewModel != viewModel) {
                Mvx.IoCProvider.Resolve<ILogger<DroidPresenter>>().LogWarning("Ignoring close for viewmodel - rootframe's current page is not the view for the requested viewmodel");
                return Task.FromResult(false);
            }

            if (OS.HasLollipops) {
                activity.FinishAfterTransition();
            }
            else {
                activity.Finish();
            }

            if (viewModel is IInputViewModel) {
                activity.OverridePendingTransition(0, 0);
            }

            NavigationService.BackStack.Pop(viewModel.GetType());
            
            return Task.FromResult(true);
        }
    }
}