using Android.App;
using Android.Gms.Analytics;
using MyStudyLife.Configuration;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.Extensions;

namespace MyStudyLife.Droid.Platform {
    public class DroidAnalyticsService : AnalyticsServiceBase {
        private readonly Tracker _tracker;

        private bool _isNewSession;

        public DroidAnalyticsService(IMslConfig config, INavigationService navigationService) : base(navigationService) {
            var analytics = GoogleAnalytics.GetInstance(Application.Context);

            analytics.SetLocalDispatchPeriod((int)DispatchInterval.TotalSeconds);

            _tracker = analytics.NewTracker(PropertyId);
            _tracker.SetAppName("My Study Life for Android");
            _tracker.SetAppId("mystudylife.droid");
            _tracker.SetAppVersion("droid-" + config.AppVersion.ToHumanString());

            _tracker.EnableAdvertisingIdCollection(true);
            _tracker.EnableAutoActivityTracking(false);
            _tracker.EnableExceptionReporting(false);
        }

        protected override void SetUserInfoImpl(string userId, string userType, string schoolId, string schoolType) {
            _tracker.Set("&cd1", userType);
            _tracker.Set("&cd2", schoolId);
            _tracker.Set("&cd3", schoolType);
            _tracker.Set("&uid", userId);
        }

        protected override void TrackScreenImpl(string screenName) {
            _tracker.SetScreenName(screenName);

            Send(new HitBuilders.ScreenViewBuilder());
        }

        protected override void TrackEventImpl(string category, string action) {
            Send(new HitBuilders.EventBuilder(category, action));
        }
        protected override void TrackEventImpl(string category, string action, string label) {
            Send(new HitBuilders.EventBuilder(category, action).SetLabel(label));
        }

        protected override void NotifySessionEndImpl() {
            _isNewSession = true;
        }

        private void Send(HitBuilders.HitBuilder builder) {
            if (_isNewSession) {
                builder.SetNewSession();
                _isNewSession = false;
            }

            _tracker.Send(builder.Build());
        }
    }
}