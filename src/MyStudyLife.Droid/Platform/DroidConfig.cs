using System;
using Android.App;
using MyStudyLife.Configuration;

namespace MyStudyLife.Droid.Platform {
    public class DroidConfig : MslConfig<DroidConfig> {
        public static TimeSpan ScheduledTaskInterval => TimeSpan.FromHours(12.0);

        internal const string RaygunApiKey = "u8+qSp/3hVgA5Hb70BlxUA==";

        internal readonly string[] FirebaseTopics = {"global"};

        private static readonly Lazy<string> _appVersionName = new Lazy<string>(() => {
            var context = Application.Context;
            return context.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
        });

        private static readonly Lazy<Version> _appVersion =
            new Lazy<Version>(() => Version.Parse(_appVersionName.Value.Split("-")[0]));

        public override Version AppVersion => _appVersion.Value;

        public override string AppVersionName => _appVersionName.Value;

        protected override string ApiClientId => "7V6fohdR6e3BRlynXoYZkaLPyg4E3pRv";
    }
}