using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Microsoft.Extensions.Logging;
using MyStudyLife.Droid.Services;
using MyStudyLife.Reminders;
using Newtonsoft.Json;

namespace MyStudyLife.Droid.Platform {

    public class DroidReminderScheduler : IReminderScheduler {
        private readonly ILogger<DroidReminderScheduler> _logger;
        private readonly Context _context;
        private readonly AlarmManager _alarmManager;

        // Getter as GetSharedPreferences is an IO call
        private ISharedPreferences Preferences =>
        _context.GetSharedPreferences("MSL_REMINDERS", FileCreationMode.Private);

        public DroidReminderScheduler(ILogger<DroidReminderScheduler> logger) {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _context = Application.Context;
            _alarmManager = (AlarmManager)_context.GetSystemService(Context.AlarmService);
        }

        /// <summary>
        ///     Schedules a reminder to be displayed to a user at a future point in time.
        /// </summary>
        /// <returns>
        ///     True if successful. Can be false if reminders are disabled by the device.
        /// </returns>
        public bool ScheduleReminder(Reminder reminder) {
            var intent = GetIntentForReminderWithId(reminder.GetUniqueName());

            intent.PutExtra("REMINDER", JsonConvert.SerializeObject(reminder));

            var timeMillis = reminder.Time.GetTimeInMillis();
            PendingIntent pendingIntent;

            if (Build.VERSION.SdkInt >= BuildVersionCodes.S) {
                if (_alarmManager.CanScheduleExactAlarms()) {
                    pendingIntent = PendingIntent.GetBroadcast(_context, 0, intent, PendingIntentFlags.Immutable);
                    _alarmManager.SetExactAndAllowWhileIdle(AlarmType.RtcWakeup, timeMillis, pendingIntent);
                }
            }
            else {
                pendingIntent = PendingIntent.GetBroadcast(_context, 0, intent, PendingIntentFlags.OneShot);

                if (Build.VERSION.SdkInt >= BuildVersionCodes.M) {
                    _alarmManager.SetExactAndAllowWhileIdle(AlarmType.RtcWakeup, timeMillis, pendingIntent);
                }
                else if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat) {
                    _alarmManager.SetExact(AlarmType.RtcWakeup, timeMillis, pendingIntent);
                }
                else {
                    _alarmManager.Set(AlarmType.RtcWakeup, timeMillis, pendingIntent);
                }
            }

            var persistedReminderIds = Preferences.GetStringSet("MSL_REMINDERS", new List<string>()).ToList();

            persistedReminderIds.Add(reminder.GetUniqueName());

            var editor = Preferences.Edit();

            editor.PutStringSet("MSL_REMINDERS", persistedReminderIds);

            return editor.Commit();
        }

        public void ClearReminders() {
            var existingReminderIds = Preferences.GetStringSet("MSL_REMINDERS", new List<string>()).ToList();
            var reminderIdsToPersist = new List<string>();

            foreach (var id in existingReminderIds) {
                try {
                    PendingIntent pendingIntent;
                    if (Build.VERSION.SdkInt >= BuildVersionCodes.S) {
                        // Starting with Android 12 (Build.VERSION_CODES.S), it will be required to explicitly specify the mutability of PendingIntents on creation
                        pendingIntent = PendingIntent.GetBroadcast(_context, 0, GetIntentForReminderWithId(id), PendingIntentFlags.Immutable);
                    }
                    else {
                        // Set UpdateCurrent becuase we can modify if requried 
                        pendingIntent = PendingIntent.GetBroadcast(_context, 0, GetIntentForReminderWithId(id), PendingIntentFlags.UpdateCurrent);
                    }

                    pendingIntent.Cancel();
                    _alarmManager.Cancel(pendingIntent);
                }
                catch {
                    _logger.LogWarning($"Failed to remove reminder with id '{id}'");

                    reminderIdsToPersist.Add(id);
                }
            }

            var editor = Preferences.Edit();

            editor.PutStringSet("MSL_REMINDERS", reminderIdsToPersist);

            editor.Commit();
        }

        private Intent GetIntentForReminderWithId(string reminderId) {
            var intent = new Intent(_context, typeof(ReminderBroadcastReceiver));

            intent.SetAction(reminderId);

            return intent;
        }
    }
}