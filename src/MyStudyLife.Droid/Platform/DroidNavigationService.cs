using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Views;
using AndroidX.Core.App;
using AndroidX.Core.Content;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Platforms.Android;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MyStudyLife.Data;
using MyStudyLife.Droid.Common;
using MyStudyLife.Scheduling;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Wizards;
using Serilog;

namespace MyStudyLife.Droid.Platform {
    public class DroidNavigationService : NavigationService, IDroidNavigationService {
        private IMvxAndroidViewModelRequestTranslator _requestTranslator;

        private IMvxAndroidViewModelRequestTranslator RequestTranslator => _requestTranslator ??=
            Mvx.IoCProvider.Resolve<IMvxAndroidViewModelRequestTranslator>();

        public DroidNavigationService(
            IMvxViewModelLoader viewModelLoader,
            IMvxViewDispatcher mvxViewDispatcher,
            IMvxIoCProvider mvxIoCProvider,
            IMvxAndroidViewModelRequestTranslator requestTranslator
        ) : base(viewModelLoader, mvxViewDispatcher, mvxIoCProvider) {
            _requestTranslator = requestTranslator;
        }

        public void ViewEntry(AgendaEntry entry, View transitionFromView) {
            ShowViewModel(GetRequest(entry), transitionFromView);
        }

        public void ViewEntry(AgendaEntry entry, ActivityOptionsCompat options) {
            ShowViewModel(GetRequest(entry), options);
        }

        public void ViewEntity<T>(T entity, View transitionFromView) where T : SubjectDependentEntity {
            ShowViewModel(GetRequest(entity), transitionFromView);
        }

        private void ShowViewModel(MvxViewModelRequest request, View transitionFromView) {
            ActivityOptionsCompat options = null;
            var pos = new Rect();

            if (OS.HasLollipops && transitionFromView.GetGlobalVisibleRect(pos)) {
                options = ActivityOptionsCompat.MakeScaleUpAnimation(
                    transitionFromView,
                    pos.Left,
                    pos.Top,
                    pos.Width(),
                    pos.Height()
                );
            }

            ShowViewModel(request, options);

            BackStack.Push(request.ViewModelType);
        }

        private void ShowViewModel(MvxViewModelRequest request, ActivityOptionsCompat options) {
            var intent = RequestTranslator.GetIntentFor(request);
            var bundle = options?.ToBundle();

            var activity = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity;
            if (activity == null) {
                Log.Warning("Cannot Resolve current top activity. Creating new activity from Application Context");
                intent.AddFlags(ActivityFlags.NewTask);
                ContextCompat.StartActivity(Application.Context, intent, bundle);
                return;
            }

            ContextCompat.StartActivity(activity, intent, bundle);

            BackStack.Push(request.ViewModelType);
        }

        public override Type GetFirstViewAuthorized(User user) {
            var viewModelType = base.GetFirstViewAuthorized(user);

#pragma warning disable 618
            if (viewModelType == typeof(WelcomeWizardViewModel)) {
#pragma warning restore 618
                return typeof(WelcomeViewModel);
            }
            return viewModelType;
        }
    }
}