using System;
using System.Threading;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using MyStudyLife.Data;
using MyStudyLife.Utility;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Platform {
    public class DroidDeviceInfoProvider : IDeviceInfoProvider {
        private const string PREF_UNIQUE_ID = "PREF_UNIQUE_ID";

        private static readonly Lazy<string> _hardwareId = new Lazy<string>(GetHardwareId, LazyThreadSafetyMode.ExecutionAndPublication);

        /// <summary>
        ///        Gets the unique identifier for the 
        ///        current hardware.
        /// </summary>
        /// <remarks>Run as async as GetSharedPreferences is an IO call.</remarks>
        /// <returns>The hardware's unique id.</returns>
        public Task<Device> GetDeviceInformationAsync() => Task.Run(() => new Device {
            Manufacturer = Build.Manufacturer,
            Model = Build.Model,
            HardwareId = _hardwareId.Value,
            OSVersion = Build.VERSION.Release
        });

        private static string GetHardwareId() {
            var context = Application.Context;
            var androidId = Android.Provider.Settings.Secure.GetString(context.ContentResolver, Android.Provider.Settings.Secure.AndroidId);

            if (!String.IsNullOrEmpty(androidId) && "9774d56d682e549c" != androidId) {
                return androidId;
            }

            var sharedPrefs = context.GetSharedPreferences(PREF_UNIQUE_ID, FileCreationMode.Private);
            var uniqueId = sharedPrefs.GetString(PREF_UNIQUE_ID, null);

            if (uniqueId == null) {
                uniqueId = Guid.NewGuid().ToString("N");
                var editor = sharedPrefs.Edit();
                editor.PutString(PREF_UNIQUE_ID, uniqueId);
                editor.Commit();
            }

            return uniqueId;
        }
    }
}