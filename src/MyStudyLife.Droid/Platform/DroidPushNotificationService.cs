using System;
using System.Collections.Generic;
using Android.App;
using Android.Gms.Common;
using Android.Gms.Extensions;
using Android.Gms.Tasks;
using Firebase;
using Firebase.Installations;
using Firebase.Messaging;
using Microsoft.Extensions.Logging;
using MyStudyLife.Net;
using MyStudyLife.Services;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.Droid.Platform
{
    public class DroidPushNotificationService : PushNotificationServiceBase, IPushNotificationService
    {
        private const string Tag = "PNS";

        private readonly IHttpApiClient _httpApiClient;
        private readonly ILogger<PushNotificationServiceBase> _logger;

        public DroidPushNotificationService(IHttpApiClient httpApiClient, ILogger<PushNotificationServiceBase> logger) : base(logger)
        {
            _httpApiClient = httpApiClient ?? throw new ArgumentNullException(nameof(httpApiClient));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task EnsureNotificationChannelAsync()
        {
            try
            {
                if (!IsGooglePlayServicesApiAvailable())
                {
                    return;
                }

                _logger.LogInformation("Requesting Firebase token");
                var result = await FirebaseMessaging.Instance.GetToken();
               
                await RegisterTokenAsync(result.ToString());

                foreach (var topic in DroidConfig.Current.FirebaseTopics)
                {
                    await FirebaseMessaging.Instance.SubscribeToTopic(topic);
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Failed to register Firebase token", ex);
            }
        }

        public async Task RegisterTokenAsync(string token)
        {
            var content = new Dictionary<string, string> {
                { "registration_id", token }
            };

            var jsonContent = JsonConvert.SerializeObject(content);

            await _httpApiClient.PostJsonAsync("push/fcm", jsonContent);

            _logger.LogInformation("Firebase token registered");
        }

        public Task CloseNotificationChannelAsync() {
            try {
                if (IsGooglePlayServicesApiAvailable()) {
                    FirebaseInstallations.GetInstance(FirebaseApp.Instance).Delete();                  
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning("Failed to delete Firebase instance id", ex);
            }

            return Task.CompletedTask;
        }

        private bool IsGooglePlayServicesApiAvailable()
        {
            if (GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(Application.Context) == ConnectionResult.Success)
            {
                return true;
            }

            _logger.LogWarning("Google Play Services not available - push notifications unavailable");
            return false;

        }
    }
}