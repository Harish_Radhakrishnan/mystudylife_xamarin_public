using System;
using System.Reflection;
using Android.App;
using Android.Content;
using Android.OS;
using AndroidX.Browser.CustomTabs;
using AndroidX.Core.Content;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Platforms.Android;
using MvvmCross.Plugin.Color.Platforms.Android;
using MvvmCross.Views;
using MyStudyLife.Configuration;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.Droid.Platform
{
    public class DroidTaskProvider : ITaskProvider {
        private readonly IMvxAndroidCurrentTopActivity _topActivity;

        public DroidTaskProvider(IMvxAndroidCurrentTopActivity topActivity) {
            _topActivity = topActivity;
        }

        public void ShowEmailComposer(string subject, string body, string to) {

            var intent = new Intent(Intent.ActionSend);

            string[] recipients = { to };

            intent.PutExtra(Intent.ExtraEmail, recipients);
            intent.PutExtra(Intent.ExtraSubject, subject);
            intent.PutExtra(Intent.ExtraText, body);

            intent.SetType("text/html");

            var chooser = Intent.CreateChooser(intent, "Send Email");

            chooser.AddFlags(ActivityFlags.NewTask | ActivityFlags.NoHistory | ActivityFlags.ClearWhenTaskReset);

            Application.Context.StartActivity(chooser);
        }

        #region Review App

        public void ReviewApp() {
            var packageManager = Application.Context.PackageManager.GetInstallerPackageName(Application.Context.PackageName);

            try {

                // Fall back to Google Play.
                bool amazonOrGoogle = false;

                try {
                    if (packageManager == null && Build.Manufacturer.Equals("Samsung", StringComparison.InvariantCultureIgnoreCase)) {
                        ShowAppWithIntentUri("samsungapps://ProductDetail/");

                        amazonOrGoogle = true;
                    }
                    else if (packageManager != null && packageManager.StartsWith("com.amazon")) {
                        ShowAppWithIntentUri("amzn://apps/android?p=");

                        amazonOrGoogle = true;
                    }
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch { }

                if (!amazonOrGoogle) {
                    try {
                        ShowAppWithIntentUri("market://details?id=");
                    }
                    catch (ActivityNotFoundException) {
                        ShowAppWithIntentUri("http://play.google.com/store/apps/details?id=");
                    }
                }
            }
            catch {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(
                    "Unable to find app store",
                    "Sorry, we couldn't find which app store you used to install My Study Life. " +
                    "We'd be grateful if you could navigate to the app store and rate My Study Life manually :)"
                );
            }
        }

        #endregion

        private void ShowAppWithIntentUri(string intentUri) {
            intentUri += Application.Context.PackageName;

            Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(intentUri));

            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop | ActivityFlags.IncludeStoppedPackages);

            Application.Context.StartActivity(intent);
        }

        public bool TryShowNativeUri(Uri uri) {
            try {
                ShowNativeUri(uri);

                return true;
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<DroidTaskProvider>>().LogWarning("Masked exception during DroidTaskProvider.TryShowNativeUri: " + ex.ToString());

                return false;
            }
        }

        private void ShowNativeUri(Uri uri) {
            var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(uri.ToString()));

            intent.AddFlags(ActivityFlags.NewTask | ActivityFlags.ClearTop | ActivityFlags.IncludeStoppedPackages);

            Application.Context.StartActivity(intent);
        }

        public void ShowWebUri(Uri uri) {
            int? color = null;

            try {
                var mvxView = Mvx.IoCProvider.Resolve<IMvxAndroidCurrentTopActivity>().Activity as IMvxView;
                var tViewModel = mvxView?.ViewModel?.GetType();
                var colorProp = tViewModel?.GetProperty("Color", BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);

                if (colorProp != null) {
                    color = (colorProp.GetValue(mvxView.ViewModel) as System.Drawing.Color?)?.ToNativeColor();
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<DroidTaskProvider>>().LogTrace("Masked exception whilst retrieving current color during ShowWebUri: " + ex.ToLongString());
            }

            var customTabIntent = new CustomTabsIntent.Builder()
                .SetToolbarColor(color.GetValueOrDefault(ContextCompat.GetColor(Application.Context, Resource.Color.accent)))
                .Build();
            customTabIntent.LaunchUrl(_topActivity.Activity, Android.Net.Uri.Parse(uri.ToString()));
        }

        public void ShowFacebookPage() {
            // As of version 3002850, the Facebook app also accepts fb://facewebmodal/f?href=
            // deeplinks, which many sources online say is the correct way, but fb://page
            // still works, and works better as it opens the page, not the page's post stream.

            if (!TryShowNativeUri(new Uri(MslConfig.Current.FacebookPageDeeplink))) {
                ShowWebUri(new Uri(MslConfig.Current.FacebookPageUri));
            }
        }
    }
}