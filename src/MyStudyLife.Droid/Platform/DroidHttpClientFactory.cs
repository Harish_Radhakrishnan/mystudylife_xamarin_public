using System.Net;
using System.Net.Http;
using MyStudyLife.Net;
using Xamarin.Android.Net;

namespace MyStudyLife.Droid.Platform {
    public class DroidHttpClientFactory : IHttpClientFactory {
        public HttpClient Get() {
            var handler = new AndroidClientHandler();

            if (handler.SupportsAutomaticDecompression) {
                handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            }

            return new HttpClient(handler);
        }
    }
}