using Android.Views;
using AndroidX.Core.App;
using MyStudyLife.Data;
using MyStudyLife.Scheduling;
using MyStudyLife.UI;

namespace MyStudyLife.Droid.Platform {
    public interface IDroidNavigationService : INavigationService {
        void ViewEntry(AgendaEntry entry, ActivityOptionsCompat options);
        void ViewEntry(AgendaEntry entry, View transitionFromView);

        void ViewEntity<T>(T entity, View transitionFromView) where T : SubjectDependentEntity;
    }
}