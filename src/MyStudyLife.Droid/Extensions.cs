using System;
using Android.Views;
using Java.Util;

namespace MyStudyLife.Droid {
    /// <summary>
    ///     Android specific extensions.
    /// </summary>
    public static class DroidExtensions {
// ReSharper disable once InconsistentNaming
        private static readonly DateTime Jan1st1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        ///     Equivalent to Calender.getTimeInMillis in java but
        ///     for DateTime.
        /// </summary>
        public static long GetTimeInMillis(this DateTime d) {
            return (long) (d.ToUniversalTimeEx() - Jan1st1970).TotalMilliseconds;
        }

        public static Calendar ToCalendar(this DateTime d) {
            var cal = Calendar.Instance;

            cal.Set(CalendarField.Year, d.Year);
            cal.Set(CalendarField.Month, d.Month - 1);
            cal.Set(CalendarField.Date, d.Day);
            cal.Set(CalendarField.HourOfDay, d.Hour);
            cal.Set(CalendarField.Minute, d.Minute);
            cal.Set(CalendarField.Second, d.Second);
            cal.Set(CalendarField.Millisecond, d.Millisecond);

            return cal;
        }

        public static void SetVisible(this View view, bool visible, ViewStates notVisibleState = ViewStates.Gone) {
            view.Visibility = visible ? ViewStates.Visible : notVisibleState;
        }

        /// <summary>
        ///     Gets the top of the view, relative to the given parent.
        /// </summary>
        /// <param name="view">
        ///     The view.
        /// </param>
        /// <param name="parentView">
        ///     The parent view. Must be in the hierachy above <paramref name="view"/>.
        /// </param>
        public static int GetTopTo(this View view, View parentView) {
            const int MAX_DEPTH = 25;

            int depth = 0;
            int y = 0;

            while (view != parentView) {
                depth++;

                y += view.Top;

                view = view.Parent as View;

                if (depth > MAX_DEPTH) {
                    throw new Exception($"Could not find parent view within {MAX_DEPTH} steps from view. Is provided view a parent?");
                }
            }

            return y;
        }
    }
}