﻿using System;
using System.Linq;
using System.Reflection;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.UI;
using MyStudyLife.W8.Common;
using MyStudyLife.W8.Controls;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8 {
    public sealed partial class AppShell {
        private readonly Rect _splashImageRect;

        private AppBar _topAppBar, _bottomAppBar;
        private bool _isShowingModal;

        public static AppShell Current { get; private set; }

        public bool IsShowingModal {
            get { return _isShowingModal; }
            set {
                _isShowingModal = value;

                if (this.Modal != null) {
                    this.Modal.Visibility = (_isShowingModal ? Visibility.Visible : Visibility.Collapsed);
                }
            }
        }
        
        public AppShell(IActivatedEventArgs args)  {
            Current = this;

            this.InitializeComponent();

            if (args.SplashScreen != null) {
                _splashImageRect = args.SplashScreen.ImageLocation;

                Window.Current.SizeChanged += WindowOnSizeChanged;

                this.PositionImage();

                this.ViewFrame.Navigating += DismissSplashScreenOnFirstNavigate;
            }
            else {
                this.Children.Remove(this.SplashScreen);
            }

            this.ViewFrame.Navigating += ViewFrameOnNavigating;
        }

        #region Splash Screen

        private void DismissSplashScreenOnFirstNavigate(object sender, NavigatingCancelEventArgs e) {
            this.ViewFrame.Navigating -= DismissSplashScreenOnFirstNavigate;

            this.Children.Remove(this.SplashScreen);
        }
        
        private void PositionImage() {
            this.SplashScreenImage.SetValue(Canvas.LeftProperty, _splashImageRect.X);
            this.SplashScreenImage.SetValue(Canvas.TopProperty, _splashImageRect.Y);
            this.SplashScreenImage.Height = _splashImageRect.Height;
            this.SplashScreenImage.Width = _splashImageRect.Width;

            this.SplashScreenImageTransform.CenterY = _splashImageRect.Height / 2;
            this.SplashScreenImageTransform.CenterX = _splashImageRect.Width / 2;
        }

        private void WindowOnSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            this.PositionImage();
        }

        #endregion

        /// <summary>
        ///     Things to do after MvvmCross is initialized.
        /// </summary>
        public void Initialize() {
            this.Children.Add(new ActivityIndicator());
        }

        private void ViewFrameOnNavigating(object sender, NavigatingCancelEventArgs e) {
            _topAppBar = null;
            _bottomAppBar = null;

            CloseModal();
        }

        private void ModalOverlayOnTapped(object sender, TappedRoutedEventArgs e) {
            if ((ModalBackdrop)this.ModalOverlay.Tag == ModalBackdrop.Dismissible) {
                CloseModal(this.ModalHost.Children.LastOrDefault(x => x is IModal) as FrameworkElement);
            }
        }

        public void ShowModal(Type modalViewType, MvxViewModelRequest request) {
            var modalView = (UserControl) Activator.CreateInstance(modalViewType);

            ((IModal)modalView).ViewModel = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(request, null);

            var modalAttribute = modalViewType.GetTypeInfo().GetCustomAttribute<ModalAttribute>(true);

            if (modalAttribute != null) {
                if (modalAttribute.OverridesHorizontalAlignment) {
                    modalView.HorizontalAlignment = modalAttribute.HorizontalAlignment;
                }
                
                if (modalAttribute.OverridesVerticalAlignment) {
                    modalView.VerticalAlignment = modalAttribute.VerticalAlignment;
                }
            }
            
            this.ModalHost.Children.Add(modalView);

            this.SetOverlayStyle(modalAttribute);
            this.SetOverlayIndex();

            this.IsShowingModal = true;

            // AppBar handling

            var view = this.ViewFrame.Content as LayoutAwarePage;

            if (view != null) {
                // Check for null to handle multiple modals
                // on a single page.
                //
                // IsOpen = false ensures it won't be opened
                // when reinstated, also has to be set when
                // still bound to the view.
                if (view.TopAppBar != null) {
                    view.TopAppBar.IsOpen = false;
                    _topAppBar = view.TopAppBar;
                    view.TopAppBar = null;
                }
                if (view.BottomAppBar != null) {
                    view.BottomAppBar.IsOpen = false;
                    _bottomAppBar = view.BottomAppBar;
                    view.BottomAppBar = null;
                }
            }
        }

        public void CloseModal() {
            if (this.ModalHost.Children.Count > 1) {
                var children = this.ModalHost.Children.ToList();
                // Don't remove the overlay
                children.Remove(this.ModalOverlay);

                foreach (var child in children) {
                    var closeHandler = child as IHandleClose;

                    if (closeHandler != null) {
                        closeHandler.HandleClose(null);
                    }

                    this.ModalHost.Children.Remove(child);
                }

                var transaction = Mvx.Resolve<INavigationService>().BackStack.CurrentTransaction;

                if (transaction != null) {
                    transaction.Pop(children.Count);
                }
            }

            this.IsShowingModal = false;
            this.ModalOverlay.Visibility = Visibility.Collapsed;

            ReinstateAppBars();
        }

        public void CloseModal(IMvxViewModel viewModel) {
            if (viewModel == null) {
                throw new ArgumentNullException("viewModel");
            }

            CloseModal(
                (FrameworkElement) this.ModalHost.Children.OfType<IModal>().SingleOrDefault(x => x.ViewModel == viewModel)
            );
        }

        public void CloseModal(Type modalViewType) {
            if (modalViewType == null) {
                throw new ArgumentNullException("modalViewType");
            }

#if DEBUG
            if (!typeof(IModal).GetTypeInfo().IsAssignableFrom(modalViewType.GetTypeInfo())) {
                throw new ArgumentException("Type not supported. Must inherit from IModal.", "modalViewType");
            }
#endif

            CloseModal(
                (FrameworkElement)this.ModalHost.Children.LastOrDefault(x => x.GetType() == modalViewType)
            );
        }

        private void CloseModal(FrameworkElement modalView) {

            if (modalView != null) {
                var closeHandler = modalView as IHandleClose;

                if (closeHandler != null) {
                    closeHandler.HandleClose(null);
                }

                this.ModalHost.Children.Remove(modalView);
            }

            this.IsShowingModal = this.ModalHost.Children.Count > 1;

            if (!this.IsShowingModal) {
                this.ModalOverlay.Visibility = Visibility.Collapsed;

                ReinstateAppBars();
            }
            else {
                this.SetOverlayIndex();

                var modalAttribute = this.ModalHost.Children.Last().GetType().GetTypeInfo().GetCustomAttribute<ModalAttribute>(true);

                this.SetOverlayStyle(modalAttribute);
            }
        }

        private void SetOverlayStyle(ModalAttribute modalAttribute) {
            var backdrop = ModalBackdrop.Dismissible;

            if (modalAttribute != null) {
                backdrop = modalAttribute.Backdrop;
            }

            this.ModalOverlay.Tag = backdrop;

            if (backdrop == ModalBackdrop.None) {
                this.ModalOverlay.Visibility = Visibility.Collapsed;
            }
            else {
                this.ModalOverlay.Visibility = Visibility.Visible;

                switch (backdrop) {
                    case ModalBackdrop.Static:
                        this.ModalOverlay.Background = new SolidColorBrush(Colors.Black) {
                            Opacity = 0.3
                        };
                        break;
                    case ModalBackdrop.StaticInvisible:
                        this.ModalOverlay.Background = new SolidColorBrush(Colors.Transparent);
                        break;
                    case ModalBackdrop.Dismissible:
                        this.ModalOverlay.Background = new SolidColorBrush(Colors.Black) {
                            Opacity = 0.3
                        };
                        break;
                }
            }
        }

        private void SetOverlayIndex() {
            this.ModalHost.Children.Move(
                (uint)this.ModalHost.Children.IndexOf(this.ModalOverlay),
                (uint)this.ModalHost.Children.Count - 2
            );
        }

        private void ReinstateAppBars() {
            var view = this.ViewFrame.Content as LayoutAwarePage;

            if (view != null) {
                if (view.TopAppBar == null) {
                    view.TopAppBar = _topAppBar;
                }
                if (view.BottomAppBar == null) {
                    view.BottomAppBar = _bottomAppBar;
                }
            }
        }
    }
}
