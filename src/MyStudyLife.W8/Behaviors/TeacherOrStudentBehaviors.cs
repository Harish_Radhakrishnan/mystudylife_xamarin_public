﻿using System;
using System.Diagnostics;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using MyStudyLife.Data;

namespace MyStudyLife.W8.Behaviors {
    public class TeacherOrStudentTextBehavior : Behavior<TextBlock> {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof(User), typeof(TeacherOrStudentTextBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public User User {
            get { return (User)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public static readonly DependencyProperty TextIfTeacherProperty = DependencyProperty.Register(
            "TextIfTeacher", typeof(string), typeof(TeacherOrStudentTextBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public string TextIfTeacher {
            get { return (string)GetValue(TextIfTeacherProperty); }
            set { SetValue(TextIfTeacherProperty, value); }
        }

        public static readonly DependencyProperty TextIfStudentProperty = DependencyProperty.Register(
            "TextIfStudent", typeof(string), typeof(TeacherOrStudentTextBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public string TextIfStudent {
            get { return (string)GetValue(TextIfStudentProperty); }
            set { SetValue(TextIfStudentProperty, value); }
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((TeacherOrStudentTextBehavior)d).Invalidate();
        }

        protected override void OnAttached() {
            this.Invalidate();
        }

        protected override void OnDetached() { }

        private void Invalidate() {
            if (this.AssociatedObject == null) {
                return;
            }

            bool isTeacher = this.User != null && this.User.IsTeacher;

            this.AssociatedObject.Text = (isTeacher ? this.TextIfTeacher : this.TextIfStudent) ?? String.Empty;
        }
    }

    public abstract class TeacherOrStudentColorBehavior : Behavior<FrameworkElement> {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof(User), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public User User {
            get { return (User) GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public static readonly DependencyProperty ColorIfTeacherProperty = DependencyProperty.Register(
            "ColorIfTeacher", typeof(Brush), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public Brush ColorIfTeacher {
            get { return (Brush) GetValue(ColorIfTeacherProperty); }
            set { SetValue(ColorIfTeacherProperty, value); }
        }

        public static readonly DependencyProperty ColorIfStudentProperty = DependencyProperty.Register(
            "ColorIfStudent", typeof(Brush), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public Brush ColorIfStudent {
            get { return (Brush) GetValue(ColorIfStudentProperty); }
            set { SetValue(ColorIfStudentProperty, value); }
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((TeacherOrStudentColorBehavior)d).Invalidate();
        }

        protected override void OnAttached() {
            this.Invalidate();
        }

        protected override void OnDetached() {}

        private void Invalidate() {
            if (this.ColorIfStudent == null && this.ColorIfTeacher == null) {
                return;
            }

            bool isTeacher = this.User != null && this.User.IsTeacher;

            this.SetColor(isTeacher
                ? ColorIfTeacher ?? ColorIfStudent
                : ColorIfStudent
            );
        }

        protected abstract void SetColor(Brush brush);
    }

    public class TeacherOrStudentFillBehavior : TeacherOrStudentColorBehavior {
        protected override void SetColor(Brush brush) {
            ((Rectangle) this.AssociatedObject).Fill = brush;
        }
    }

    public class TeacherOrStudentBackgroundBehavior : TeacherOrStudentColorBehavior {
        private Action<Brush> _setColorAction;

        protected override void OnAttached() {
            base.OnAttached();

            if (this.AssociatedObject is Border) {
                _setColorAction = brush => {
                    ((Border) (this.AssociatedObject)).Background = brush;
                };
            }
            else if (this.AssociatedObject is Panel) {
                _setColorAction = brush => {
                    ((Panel) (this.AssociatedObject)).Background = brush;
                };
            }
            else {
                Debug.WriteLine("TeacherOrStudentBackgroundBehavior: Unknown type '{0}'", this.AssociatedObject.GetType());
            }
        }

        protected override void SetColor(Brush brush) {
            if (_setColorAction != null) {
                _setColorAction(brush);
            }
        }
    }

    public class TeacherOrStudentBorderBrushBehavior : TeacherOrStudentColorBehavior {
        protected override void SetColor(Brush brush) {
            ((Border)this.AssociatedObject).BorderBrush = brush;
        }
    }
}
