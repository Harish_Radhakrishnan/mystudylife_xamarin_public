﻿using System.Diagnostics;
using System.Runtime.InteropServices;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace MyStudyLife.W8.Behaviors {
    public class StoryboardOnStateBehavior : Behavior<FrameworkElement> {
        protected override void OnAttached() {
            AssociatedObject.Loaded += AssociatedObject_Loaded;
        }
        protected override void OnDetached() {
            AssociatedObject.Loaded -= AssociatedObject_Loaded;

            Window.Current.SizeChanged -= OnSizeChanged;
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs e) {

            SetStoryboard();

            Window.Current.SizeChanged += OnSizeChanged;
        }

        private void OnSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            SetStoryboard();
        }

        private void SetStoryboard() {
            var width = Window.Current.Bounds.Width;

            if (width <= 320 && TryGoToState("Below320")) return;
            if (width <= 500 && TryGoToState("Below500")) return;
            if (width <= 768 && TryGoToState("Below768")) return;
            if (width <= 1024 && TryGoToState("Below1024")) return;

            TryGoToState("Default");
        }

        private bool TryGoToState(string sbName) {
            if (AssociatedObject.Resources.ContainsKey(sbName)) {
                ((Storyboard)AssociatedObject.Resources[sbName]).Begin();

                return true;
            }

            return false;
        }
    }
}
