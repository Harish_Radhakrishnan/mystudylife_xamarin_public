﻿using System.Runtime.CompilerServices;
using Windows.UI.Xaml;

namespace MyStudyLife.W8.Behaviors {
	public class StyleWhenFlagBehavior : Behavior<FrameworkElement> {
		public int Flags {
			get { return (int)GetValue(FlagsProperty); }
			set { SetValue(FlagsProperty, value); }
		}

		public static readonly DependencyProperty FlagsProperty = DependencyProperty.Register(
			"Flags",
			typeof(int),
			typeof(StyleWhenFlagBehavior),
			new PropertyMetadata(default(int), OnFlagsChanged)
		);

		public int Flag {
			get { return (int)GetValue(FlagProperty); }
			set { SetValue(FlagProperty, value); }
		}

		public static readonly DependencyProperty FlagProperty = DependencyProperty.Register(
			"Flag",
			typeof(int),
			typeof(StyleWhenFlagBehavior),
			new PropertyMetadata(default(int), OnFlagsChanged)
		);

		public static readonly DependencyProperty StyleWhenFlagProperty = DependencyProperty.Register(
			"StyleWhenFlag",
			typeof(Style),
			typeof(StyleWhenFlagBehavior),
			new PropertyMetadata(null)
		);

		public Style StyleWhenFlag {
			get { return (Style)GetValue(StyleWhenFlagProperty); }
			set { SetValue(StyleWhenFlagProperty, value); }
		}

		private static readonly ConditionalWeakTable<FrameworkElement, Style> OriginalStyleMapping = new ConditionalWeakTable<FrameworkElement, Style>();

		protected override void OnAttached() {
			OriginalStyleMapping.Add(this.AssociatedObject, this.AssociatedObject.Style);
		}

		protected override void OnDetached() {
			OriginalStyleMapping.Remove(this.AssociatedObject);
		}

		private static void OnFlagsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			var behavior = ((StyleWhenFlagBehavior)sender);

			if (behavior.AssociatedObject == null) return;

			int flag = (int) behavior.GetValue(FlagProperty);
			int flags = (int) behavior.GetValue(FlagsProperty);

			bool hasFlag = (flags & flag) == flag;
						   
			Style origStyle;

			if (hasFlag) {
				// Cache the existing style
				if (!OriginalStyleMapping.TryGetValue(behavior.AssociatedObject, out origStyle)) {
					OriginalStyleMapping.Add(behavior.AssociatedObject, behavior.AssociatedObject.Style);
				}

				behavior.AssociatedObject.Style = (Style) behavior.GetValue(StyleWhenFlagProperty);
			}
			else {
				// Try and get the old style
				OriginalStyleMapping.TryGetValue(behavior.AssociatedObject, out origStyle);

				behavior.AssociatedObject.Style = origStyle;
			}
		}
	}
}
