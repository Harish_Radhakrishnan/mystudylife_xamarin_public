﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;

namespace MyStudyLife.W8.Behaviors {
    public class PointerAnimationBehavior : Behavior<FrameworkElement> {
        protected override void OnAttached() {
            var targetName = AssociatedObject.Name;

            if (String.IsNullOrWhiteSpace(targetName)) {
                throw new NotSupportedException("AssociatedObject must have an x:Name in order to use this behavior.");
            }

            var pressedSb = new Storyboard();
            pressedSb.Children.Add(new PointerDownThemeAnimation {
                TargetName = targetName
            });
                
            this.AssociatedObject.Resources.Add("PointerPressedStoryboard", pressedSb);
            
            var releasedSb = new Storyboard();
            releasedSb.Children.Add(new PointerUpThemeAnimation {
                TargetName = targetName
            });

            this.AssociatedObject.Resources.Add("PointerReleasedStoryboard", releasedSb);

            this.AssociatedObject.PointerPressed += OnPointerPressed;
            this.AssociatedObject.PointerReleased += OnPointerReleased;
            this.AssociatedObject.PointerCanceled += OnPointerReleased;
            this.AssociatedObject.PointerCaptureLost += OnPointerReleased;
            this.AssociatedObject.PointerExited += OnPointerReleased;
        }

        protected override void OnDetached() {
            this.AssociatedObject.PointerPressed -= OnPointerPressed;
            this.AssociatedObject.PointerReleased -= OnPointerReleased;
            this.AssociatedObject.PointerCanceled -= OnPointerReleased;
            this.AssociatedObject.PointerCaptureLost -= OnPointerReleased;
            this.AssociatedObject.PointerExited -= OnPointerReleased;
        }

        private void OnPointerReleased(object sender, PointerRoutedEventArgs e) {
            ((Storyboard)((FrameworkElement)sender).Resources["PointerPressedStoryboard"]).Begin();
        }

        private void OnPointerPressed(object sender, PointerRoutedEventArgs e) {
            ((Storyboard)((FrameworkElement)sender).Resources["PointerReleasedStoryboard"]).Begin();
        }
    }
}
