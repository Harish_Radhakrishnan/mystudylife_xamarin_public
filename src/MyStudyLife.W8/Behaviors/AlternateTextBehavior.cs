﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Behaviors {
    public class AlternateTextBehavior : Behavior<TextBlock> {
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(AlternateTextBehavior), new PropertyMetadata(default(string), InvalidateOnPropertyChanged));

        public static readonly DependencyProperty AltTextProperty =
            DependencyProperty.Register("AltText", typeof(string), typeof(AlternateTextBehavior), new PropertyMetadata(default(string), InvalidateOnPropertyChanged));
        
        public static readonly DependencyProperty AltWhenWidthBelowProperty =
            DependencyProperty.Register("AltWhenWidthBelow", typeof (double), typeof (AlternateTextBehavior), new PropertyMetadata(default(double)));

        private static void InvalidateOnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((AlternateTextBehavior)d).Invalidate();
        }

        public double AltWhenWidthBelow {
            get { return (double) GetValue(AltWhenWidthBelowProperty); }
            set { SetValue(AltWhenWidthBelowProperty, value); }
        }

        public string Text {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string AltText {
            get { return (string)GetValue(AltTextProperty); }
            set { SetValue(AltTextProperty, value); }
        }

        protected override void OnAttached() {
            Invalidate();

            Window.Current.SizeChanged += WindowOnSizeChanged;
        }

        protected override void OnDetached() {
            Window.Current.SizeChanged -= WindowOnSizeChanged;
        }

        private void WindowOnSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            this.Invalidate();
        }

        private void Invalidate() {
            if (this.AssociatedObject == null) return;

            this.AssociatedObject.Text = Window.Current.Bounds.Width <= AltWhenWidthBelow ? AltText : Text;
        }
    }
}
