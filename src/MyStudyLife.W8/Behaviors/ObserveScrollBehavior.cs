﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Behaviors {
    public class ObserveScrollBehavior : Behavior<ScrollViewer> {
        #region Properties

        public static readonly DependencyProperty VerticalOffsetProperty =
            DependencyProperty.Register("VerticalOffset", typeof(double), typeof(ObserveScrollBehavior), new PropertyMetadata(default(double), VerticalOffset_Changed));


        public double VerticalOffset {
            get { return (double)GetValue(VerticalOffsetProperty); }
            set { SetValue(VerticalOffsetProperty, value); }
        }

        public static readonly DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.Register("HorizontalOffset", typeof(double), typeof(ObserveScrollBehavior), new PropertyMetadata(default(double), HorizontalOffset_Changed));

        public double HorizontalOffset {
            get { return (double)GetValue(HorizontalOffsetProperty); }
            set { SetValue(HorizontalOffsetProperty, value); }
        }

        #endregion

        protected override void OnAttached() {
            AssociatedObject.ViewChanged += AssociatedObject_ViewChanged;
            AssociatedObject.Loaded += AssociatedObject_Loaded;
        }

        protected override void OnDetached() {
            AssociatedObject.ViewChanged -= AssociatedObject_ViewChanged;
            AssociatedObject.Loaded -= AssociatedObject_Loaded;
        }

        private void AssociatedObject_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e) {
            this.VerticalOffset = AssociatedObject.VerticalOffset;
            this.HorizontalOffset = AssociatedObject.HorizontalOffset;
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            AssociatedObject.ChangeView(this.HorizontalOffset, this.VerticalOffset, null);
        }

        private static void VerticalOffset_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = d as ObserveScrollBehavior;

            if (behavior != null && behavior.AssociatedObject != null) {
                behavior.AssociatedObject.ChangeView(null, (double) e.NewValue, null);
            }
        }

        private static void HorizontalOffset_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = d as ObserveScrollBehavior;

            if (behavior != null && behavior.AssociatedObject != null) {
                behavior.AssociatedObject.ChangeView((double)e.NewValue, null, null);
            }
        }
    }
}
