﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Behaviors {
	public class CheckBoxFlagsBehavior : Behavior<CheckBox>  {

		public int Flags {
			get { return (int) GetValue(FlagsProperty); }
			set { SetValue(FlagsProperty, value); }
		}

		public static readonly DependencyProperty FlagsProperty = DependencyProperty.Register(
			"Flags",
			typeof (int),
			typeof (CheckBoxFlagsBehavior),
			new PropertyMetadata(default(int), FlagsOnChanged)
		);

		public int CheckedIfFlag {
			get { return (int)GetValue(CheckedIfFlagProperty); }
			set { SetValue(CheckedIfFlagProperty, value); }
		}

		public static readonly DependencyProperty CheckedIfFlagProperty = DependencyProperty.Register(
			"CheckedIfFlag",
			typeof(int),
			typeof(CheckBoxFlagsBehavior),
			new PropertyMetadata(default(int), FlagsOnChanged)
		);

		private static void FlagsOnChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
			var behavior = (CheckBoxFlagsBehavior)sender;

			if (behavior.AssociatedObject != null) {
				SetChecked(behavior);
			}
		}

		protected override void OnAttached() {
			this.AssociatedObject.Checked += AssociatedObjectOnChecked;
            this.AssociatedObject.Unchecked += AssociatedObjectOnUnchecked;
		}

		protected override void OnDetached() {
			this.AssociatedObject.Checked -= AssociatedObjectOnChecked;
			this.AssociatedObject.Unchecked -= AssociatedObjectOnUnchecked;
		}

		private static void SetChecked(CheckBoxFlagsBehavior behavior) {
			behavior.AssociatedObject.IsChecked = (behavior.Flags & behavior.CheckedIfFlag) == behavior.CheckedIfFlag;
		}

		private void AssociatedObjectOnChecked(object sender, RoutedEventArgs e) {
			Flags |= CheckedIfFlag;
		}

		private void AssociatedObjectOnUnchecked(object sender, RoutedEventArgs e) {
			Flags &= ~CheckedIfFlag;
		}
	}
}
