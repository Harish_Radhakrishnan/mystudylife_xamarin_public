﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Microsoft.Xaml.Interactivity;

namespace MyStudyLife.W8.Behaviors {
    public abstract class Behavior<T> : DependencyObject, IBehavior
        where T : DependencyObject {

        public T AssociatedObject { get; private set; }

        protected abstract void OnAttached();

        protected abstract void OnDetached();

        DependencyObject IBehavior.AssociatedObject {
            get { return this.AssociatedObject; }
        }
        void IBehavior.Attach(DependencyObject associatedObject) {
            if (associatedObject != null &&
                associatedObject.GetType() != typeof(T) &&
               !associatedObject.GetType().GetTypeInfo().IsSubclassOf(typeof(T))) {

                throw new Exception("Invalid target type");
            }

            this.AssociatedObject = associatedObject as T;
            this.OnAttached();
        }

        void IBehavior.Detach() {
            this.OnDetached();
            this.AssociatedObject = null;
        }
    }
}
