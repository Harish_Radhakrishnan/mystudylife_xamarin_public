using System;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.BindingEx.WindowsShared;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsCommon.Platform;
using Windows.UI.Xaml.Controls;
using Cirrious.MvvmCross.WindowsCommon.Views;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Store;
using MyStudyLife.Diagnostics;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Security.Crypto;
using MyStudyLife.Services;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.Versioning;
using MyStudyLife.W8.Controls;
using MyStudyLife.W8.Controls.Settings;
using MyStudyLife.W8.Platform;
using MyStudyLife.W8.Services;
using MyStudyLife.W8.Versioning;
using MyStudyLife.W8.Views;
using MyStudyLife.W8.Views.Input;

namespace MyStudyLife.W8 {
    public class W8Setup : MvxWindowsSetup {
        private readonly AppShell _appShell;

        public W8Setup(AppShell appShell)
            : base(appShell.ViewFrame) {

            _appShell = appShell;

            this.StateChanged += OnStateChanged;

            #if DEBUG

            //#warning Testing stuff

            //var sp = new W8StorageProvider();
            
            //sp.AddOrUpdateSetting(UpgradeService.CurrentAppVersionKey, "3.0.8");

            //sp.AddOrUpdateSetting("MSL_AT", new AccessToken {
            //    Expires = DateTime.UtcNow.AddMonths(1),
            //    Granted = DateTime.UtcNow.AddDays(-1),
            //    Token = "4mbi05n78z38162g4btmo"
            //});

            #endif

            DataStore.TraceEnabled = false;
        }

        private void OnStateChanged(object sender, MvxSetupStateEventArgs e) {
            if (e.SetupState == MvxSetupState.Initialized) {
                this.StateChanged -= OnStateChanged;
                
                _appShell.Initialize();
            }
        }

        protected override IMvxApplication CreateApp() {
            return new UI.App();
        }

        protected override IMvxTrace CreateDebugTrace() {
            return new MslDebugTrace();
        }

        #region Overrides of MvxSetup

        protected override void InitializeFirstChance() {
            base.InitializeFirstChance();

			Mvx.LazyConstructAndRegisterSingleton<IBugReporter, W8BugReporter>();

            Mvx.LazyConstructAndRegisterSingleton<IMslConfig, W8Config>();
            Mvx.LazyConstructAndRegisterSingleton<INetworkProvider, W8NetworkProvider>();
            Mvx.LazyConstructAndRegisterSingleton<IStorageProvider, W8StorageProvider>();
            Mvx.LazyConstructAndRegisterSingleton<IAuthorizationProvider, W8AuthorizationProvider>();
			Mvx.LazyConstructAndRegisterSingleton<ICryptoProvider, W8CryptoProvider>();
			Mvx.LazyConstructAndRegisterSingleton<IReminderScheduler, W8ReminderScheduler>();

            Mvx.LazyConstructAndRegisterSingleton<IDialogService, W8DialogService>();

            Mvx.LazyConstructAndRegisterSingleton<IPushNotificationService, W8PushNotificationService>();

            Mvx.RegisterType<IBootstrapAction, W8BootstrapAction>();

            Mvx.RegisterType<ITaskProvider, W8TaskProvider>();

            Mvx.RegisterType<ITimer, W8Timer>();

            Mvx.LazyConstructAndRegisterSingleton<IAnalyticsService, W8AnalyticsService>();
        }

        protected override void InitializeLastChance() {
            base.InitializeLastChance();

            RegisterFragments(Mvx.GetSingleton<ICustomPresenter>());

            var upgradeService = new W8UpgradeService(Mvx.Resolve<IMslConfig>(), Mvx.Resolve<IStorageProvider>());

            Mvx.RegisterSingleton(typeof(IUpgradeService), upgradeService);

            RegisterUpgrades(upgradeService);

// ReSharper disable once RedundantNameQualifier
			Cirrious.MvvmCross.Plugins.Messenger.PluginLoader.Instance.EnsureLoaded();
            
			new MvxWindowsBindingBuilder().DoRegistration();
        }

        protected override IMvxWindowsViewPresenter CreateViewPresenter(IMvxWindowsFrame rootFrame) {
            var customPresenter = new W8Presenter(rootFrame);
            Mvx.RegisterSingleton<ICustomPresenter>(customPresenter);
            return customPresenter;
        }

        private void RegisterUpgrades(IUpgradeService upgradeService) {
            upgradeService.RegisterUpgrade<V1ToV2>();
            upgradeService.RegisterUpgrade<V2ToV201>();
            upgradeService.RegisterUpgrade<V201ToV3>();
            upgradeService.RegisterUpgrade<V3ToV4>();
            upgradeService.RegisterUpgrade<V405ToV406>();
        }

        #endregion

        private void RegisterFragments(ICustomPresenter presenter) {
            presenter.MapSplitView<Task, TaskViewModel, TasksView>();
            presenter.MapSplitView<Exam, ExamViewModel, ExamsView>();

            presenter.AddModal<ClassViewModel, ViewClass>();
            presenter.AddModal<TaskViewModel, ViewTask>();
            presenter.AddModal<ExamViewModel, ViewExam>();

            presenter.AddModal<SubjectsViewModel, SubjectsFlyout>();
            presenter.AddModal<SubjectInputViewModel, SubjectInputFlyout>();
            presenter.AddModal<AcademicYearInputViewModel, AcademicYearInputFlyout>();
            presenter.AddModal<AcademicTermInputViewModel, AcademicTermInputFlyout>();
            presenter.AddModal<HolidayInputViewModel, HolidayInputFlyout>();
            
            presenter.AddModal<JoinClassesViewModel, JoinClassesWizard>();
            presenter.AddModal<WelcomeWizardViewModel, WelcomeWizard>();

            presenter.AddHandler<ClassTimeInputViewModel, ClassInputView>(ClassInputView.GetInstance);
            presenter.AddCloseHandler<AccountViewModel>(AccountPane.GetInstance);
        }
    }
}