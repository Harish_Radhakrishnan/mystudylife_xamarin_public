﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.W8.Controls;
using MyStudyLife.W8.Converters;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Views {
    public abstract class InternalExamsView : BaseEntitiesView<Exam, ExamsViewModel> { }

    public sealed partial class ExamsView : ISplitView<Exam> {
        public ModalHost ModalHost {
            get { return this.SplitViewModalHolder; }
        }

        #region Overrides of EntityPage<TasksPageViewModel,Task>

        protected override void Initialize() {
            this.InitializeComponent();
        }

        #endregion
        
        private void ShowFilter(object sender, TappedRoutedEventArgs e) {
            var menu = new MenuFlyout();

            var currentMenuItem = new ToggleMenuFlyoutItem {
                Text = "Current Exams",
                CommandParameter = TaskFilterOption.Current
            };
            var pastMenuItem = new ToggleMenuFlyoutItem {
                Text = "Past Exams",
                CommandParameter = TaskFilterOption.Past
            };

            var commandBinding = new Binding {
                Path = new PropertyPath("SetFilterCommand")
            };

            currentMenuItem.SetBinding(MenuFlyoutItem.CommandProperty, commandBinding);
            pastMenuItem.SetBinding(MenuFlyoutItem.CommandProperty, commandBinding);

            currentMenuItem.SetBinding(ToggleMenuFlyoutItem.IsCheckedProperty, new Binding {
                Path = new PropertyPath("Filter"),
                Converter = (ParameterToBooleanConverter)App.Current.Resources["ParameterToBooleanConverter"],
                ConverterParameter = ExamFilterOption.Current.ToString()
            });
            pastMenuItem.SetBinding(ToggleMenuFlyoutItem.IsCheckedProperty, new Binding {
                Path = new PropertyPath("Filter"),
                Converter = (ParameterToBooleanConverter)App.Current.Resources["ParameterToBooleanConverter"],
                ConverterParameter = ExamFilterOption.Past.ToString()
            });

            currentMenuItem.Tapped += DismissAppBarOnTapped;
            pastMenuItem.Tapped += DismissAppBarOnTapped;

// ReSharper disable once PossibleNullReferenceException
            menu.Items.Add(currentMenuItem);
            menu.Items.Add(pastMenuItem);

            menu.ShowAt(sender as FrameworkElement);   
        }
    }
}