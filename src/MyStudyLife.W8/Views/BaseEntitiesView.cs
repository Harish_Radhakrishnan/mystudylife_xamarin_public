﻿using System;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.W8.Common;
using MyStudyLife.W8.Controls;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Views {
    public abstract class BaseEntitiesView<TEntity, TViewModel> : LayoutAwarePage
        where TEntity : SubjectDependentEntity, new()
        where TViewModel : BaseEntitiesViewModel<TEntity> {

        public new TViewModel ViewModel {
            get { return (TViewModel) base.ViewModel; }
        }

		#region Controls

        private ListView _itemsListViewControl;
		private Grid _viewItemLonelyControl;

        public ListView ItemsListViewControl {
            get { return _itemsListViewControl ?? (_itemsListViewControl = (ListView) this.FindName("ItemsListView")); }
        }

		public Grid ViewItemLonelyControl {
			get { return _viewItemLonelyControl ?? (_viewItemLonelyControl = (Grid)this.FindName("ViewItemLonelyGrid")); }
		}

		#endregion

        protected BaseEntitiesView() {
            // For some reason we can't call InitializeComponent here...
            // so we just make a method that has to be implemented so 
            // it's not forgotten.

// ReSharper disable once DoNotCallOverridableMethodsInConstructor
            this.Initialize();


            ((ModalHost)this.FindName("SplitViewModalHolder")).ModalChanged += ModalHostOnModalChanged;

            this.ItemsListViewControl.SelectionChanged += ItemsListViewControlOnSelectionChanged;
        }

        private void ModalHostOnModalChanged(object sender, EventArgs e) {
            if (((ModalHost)this.FindName("SplitViewModalHolder")).Modal == null) {
                if (this.ItemsListViewControl.SelectedItems.Count == 1) {
                    this.ItemsListViewControl.SelectedItem = null;
                    this.InvalidateVisualState();
                }
            }
        }

        private void ItemsListViewControlOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (this.ItemsListViewControl.SelectedItems.Count == 0 || this.ItemsListViewControl.SelectedItems.Count > 1) {
                ((ModalHost)this.FindName("SplitViewModalHolder")).Modal = null;
            }
            else if (this.ItemsListViewControl.SelectedItems.Count == 1) {
                this.ViewModel.ViewEntityCommand.Execute(this.ItemsListViewControl.SelectedItem);
            }

            // Invalidate the view state when logical page navigation is in effect, as a change
            // in selection may cause a corresponding change in the current logical page.  When
            // an item is selected this has the effect of changing from displaying the item list
            // to showing the selected item's details.  When the selection is cleared this has the
            // opposite effect.
            if (this.UsingLogicalPageNavigation()) this.InvalidateVisualState();
        }

		protected void ShowAcScheduleSelector(object sender, RoutedEventArgs e) {
		    string pluralTypeName = "items";

		    if (typeof (TEntity) == typeof (Class)) {
		        pluralTypeName = R.Classes;
		    }
            else if (typeof(TEntity) == typeof(Task)) {
                pluralTypeName = R.Tasks;
		    }
            else if (typeof(TEntity) == typeof(Exam)) {
                pluralTypeName = R.Exams;
            }

		    AcademicScheduleSelector.GetInstanceAndShow(
                (Panel)FindName("Root"),
                sender as FrameworkElement,
                String.Format(R.AcademicScheduleTaskExamListHelp, pluralTypeName)
            );
		}

        protected abstract void Initialize();

        #region Logical page navigation

        private bool UsingLogicalPageNavigation() {
            return Window.Current.Bounds.Width <= 768;
        }

        /// <summary>
        /// Invoked when the page's back button is pressed.
        /// </summary>
        /// <param name="sender">The back button instance.</param>
        /// <param name="e">Event data that describes how the back button was clicked.</param>
        internal override void GoBack(object sender, RoutedEventArgs e) {
            if (this.UsingLogicalPageNavigation() && this.ItemsListViewControl.SelectedItem != null) {
                // When logical page navigation is in effect and there's a selected item that
                // item's details are currently displayed.  Clearing the selection will return to
                // the item list.  From the user's point of view this is a logical backward
                // navigation.
                ((ModalHost)this.FindName("SplitViewModalHolder")).Modal = null;
                this.ItemsListViewControl.SelectedItems.Clear();
            }
            else {
                // When logical page navigation is not in effect, or when there is no selected
                // item, use the default back button behavior.
                base.GoBack(sender, e);
            }
        }
        
        protected override void ActivateStates(Control ctrl) {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            var width = Window.Current.Bounds.Width;

            const bool useTransitions = false;

            bool detailActive = this.UsingLogicalPageNavigation() && this.ItemsListViewControl.SelectedItem != null;

            VisualStateManager.GoToState(ctrl, width <= 1024 ? "Below1024Active" : "Below1024Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 768 ? (detailActive ? "Below768DetailActive" : "Below768Active") : "Below768Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 500 ? "Below500Active" : "Below500Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 320 ? "Below320Active" : "Below320Inactive", useTransitions);
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        #endregion
    }
}
