﻿using System;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI.ViewModels;
using Windows.UI.Xaml.Controls;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.CrossCore;
using MyStudyLife.UI.ViewModels.Components;

namespace MyStudyLife.W8.Views {
    public sealed partial class DashboardView {

        public new DashboardViewModel ViewModel {
            get { return (DashboardViewModel) base.ViewModel; }
            private set { base.ViewModel = value; }
        }

        private static WeakReference<DashboardView> _instanceReference;

        public static DashboardView GetInstance() {
            DashboardView instance = null;

            if (_instanceReference != null) {
                _instanceReference.TryGetTarget(out instance);
            }

            return instance;
        }
        
        #region Instance
        
        public DashboardView() {
            _instanceReference = new WeakReference<DashboardView>(this);

            this.InitializeComponent();
            
            this.Loaded += (s, e) => UpdateNowBoxLayout();
            this.SizeChanged += (s, e) => UpdateNowBoxLayout();

            this.HeaderGrid.DataContext = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<NavigationDrawerViewModel>.GetDefaultRequest(), null);
        }

        #endregion

        private void UserThumbnailOnTapped(object sender, TappedRoutedEventArgs e) {
            App.Current.ShowAccountPane();
        }

        // #243 - just a quick hack for Windows 10
        private void UpdateNowBoxLayout() {
            double nowBoxHeight = ((Control) this.NowBoxWrapPanel.Children[0]).ActualHeight;

            if (((nowBoxHeight * this.NowBoxWrapPanel.Children.Count) + this.NowBoxWrapPanel.Margin.Bottom + this.NowBoxWrapPanel.Margin.Top) >= this.Row2.ActualHeight) {
                this.NowBoxWrapPanel.MaxHeight = nowBoxHeight * 2;
            }
            else {
                this.NowBoxWrapPanel.MaxHeight = Double.PositiveInfinity;
            }
        }
    }
}
