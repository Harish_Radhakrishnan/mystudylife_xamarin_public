﻿using System.Diagnostics;
using Windows.UI.Xaml;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Views.Input {
    public sealed partial class ClassTimeInputView : IHandleRequest, IHandleClose {

        #region Dependency Properties

        public static readonly DependencyProperty PropertyTypeProperty = DependencyProperty.Register(
            "ClassTime", typeof(ClassTime), typeof(ClassTimeInputView), new PropertyMetadata(null, OnClassTimeChanged));

        public ClassTime ClassTime {
            get { return (ClassTime) GetValue(PropertyTypeProperty); }
            set { SetValue(PropertyTypeProperty, value); }
        }

        private static void OnClassTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var ctrl = (ClassTimeInputView) d;

            ctrl.ViewCtrl.DataContext = e.NewValue;

            if (e.NewValue != null) {
                ctrl.NewCtrl.Visibility = Visibility.Collapsed;
                ctrl.ViewCtrl.Visibility = Visibility.Visible;
                ctrl.InputCtrl.Visibility = Visibility.Collapsed;
            }
        }

        public static readonly DependencyProperty IsInputActiveProperty = DependencyProperty.Register(
            "IsInputActive", typeof (bool), typeof (ClassTimeInputView), new PropertyMetadata(false));

        public bool IsInputActive {
            get { return (bool) GetValue(IsInputActiveProperty); }
            private set { SetValue(IsInputActiveProperty, value); }
        }

        #endregion

        public bool IsNew {
            get { return this.ClassTime == null; }
        }

        public ClassTimeInputView() {
            this.InitializeComponent();

            this.NewCtrl.Visibility = Visibility.Visible;
            this.ViewCtrl.Visibility = Visibility.Collapsed;
            this.InputCtrl.Visibility = Visibility.Collapsed;

#if DEBUG
            this.Loaded += (s, e) => {
                if (!(this.DataContext is ClassInputViewModel)) {
                    Debug.WriteLine("WARNING: DataContext for ClassTimeInputView is not of type ClassInputViewModel");
                }
            };
#endif
        }

        public void HandleRequest(MvxViewModelRequest request) {
            var viewModel = (ClassTimeInputViewModel) Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(request, null);

            this.IsInputActive = true;

            this.InputCtrl.DataContext = viewModel;

            this.NewCtrl.Visibility = Visibility.Collapsed;
            this.ViewCtrl.Visibility = Visibility.Collapsed;
            this.InputCtrl.Visibility = Visibility.Visible;
        }

        public void HandleClose(IMvxViewModel viewModel) {
            if (this.IsNew) {
                this.NewCtrl.Visibility = Visibility.Visible;
                this.ViewCtrl.Visibility = Visibility.Collapsed;
            }
            else {
                this.NewCtrl.Visibility = Visibility.Collapsed;
                this.ViewCtrl.Visibility = Visibility.Visible;
            }

            this.InputCtrl.Visibility = Visibility.Collapsed;

            this.InputCtrl.DataContext = null;
            this.IsInputActive = false;
        }


        public class NewTimeModel { }
    }
}
