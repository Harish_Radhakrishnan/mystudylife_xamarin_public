﻿<common:LayoutAwarePage
    x:Name="ViewRoot"
    x:Class="MyStudyLife.W8.Views.Input.TaskInputView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:common="using:MyStudyLife.W8.Common"
    xmlns:vms="using:MyStudyLife.UI.ViewModels.Input"
    xmlns:data="using:MyStudyLife.Data"
    mc:Ignorable="d"
    d:DataContext="{d:DesignInstance vms:TaskInputViewModel}">

    <common:LayoutAwarePage.Resources>
        <ResourceDictionary Source="/Common/InputViewStyles.xaml" />
    </common:LayoutAwarePage.Resources>

    <ScrollViewer x:Name="RootScrollViewer" Style="{StaticResource InputRootScrollViewerStyle}">
        <ScrollViewer.Resources>
            <common:ActualSizePropertyProxy x:Name="ViewRootSizeProxy" Element="{Binding ElementName=ViewRoot}" />
        </ScrollViewer.Resources>
        
        <Grid x:Name="Root" MinHeight="{Binding ElementName=ViewRootSizeProxy, Path=ActualHeightValue}" Style="{StaticResource InputRootGridStyle}">
            <Grid.ColumnDefinitions>
                <ColumnDefinition x:Name="SidebarColumn" Width="520*" MaxWidth="520" />
                <ColumnDefinition Width="856*" />
            </Grid.ColumnDefinitions>

            <Grid.RowDefinitions>
                <RowDefinition Height="Auto" />
                <RowDefinition Height="*" />
            </Grid.RowDefinitions>

            <Border x:Name="Sidebar" Style="{StaticResource InputSidebarFormStyle}" Grid.Column="0" Grid.Row="0" Grid.RowSpan="2">
                <StackPanel x:Name="SidebarInner" Style="{StaticResource InputSidebarFormInnerStackStyle}">
                    <Grid x:Name="SidebarHeader" Style="{StaticResource InputSidebarHeaderGridStyle}">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>

                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>

                        <Button x:Name="BackButton" Foreground="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}"
                                Style="{StaticResource InputBackButtonStyle}" Command="{Binding CancelInputCommand}" Grid.Column="0" Grid.Row="1" />
                        <Button x:Name="SColSaveButton" Foreground="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}"
                                Style="{StaticResource InputSColSaveButtonStyle}" Command="{Binding SaveCommand}" Grid.Column="1" Grid.Row="0" />

                        <Button x:Name="HeaderSelectorButton" Style="{StaticResource InputSidebarSelectorButtonStyle}" Tapped="AcademicScheduleSelectorOnTapped"
                                Grid.Row="1" Grid.Column="1">
                            <Grid>
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition Width="Auto" />
                                    <ColumnDefinition />
                                </Grid.ColumnDefinitions>

                                <Border x:Name="SelectorIconBorder" Background="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}"
                                        Style="{StaticResource InputSidebarSelectorIconBorderStyle}"  Grid.Column="0">
                                    <TextBlock x:Name="SelectorIcon" Text="{StaticResource TaskGlyph}" Style="{StaticResource InputSidebarSelectorIconStyle}" />
                                </Border>

                                <StackPanel HorizontalAlignment="Left" VerticalAlignment="Center" Grid.Column="1">
                                    <TextBlock x:Name="SelectorTitleText" Style="{StaticResource InputSidebarSelectorTitleTextStyle}">
                                        <Run Text="{Binding InputType}" />
                                        <Run Text="Task" />
                                    </TextBlock>
                                    <TextBlock Text="{Binding SelectedScheduleText}" Style="{StaticResource InputSidebarSelectorScheduleTextStyle}" />
                                </StackPanel>

                                <TextBlock Style="{StaticResource InputSidebarSelectorArrowStyle}" Grid.Column="2" />
                            </Grid>
                        </Button>
                    </Grid>

                    <StackPanel Style="{StaticResource InputSidebarFormRowStackStyle}">
                        <TextBlock Text="Subject" Style="{StaticResource LabelTextStyle}" />
                        <Grid>
                            <Grid.ColumnDefinitions>
                                <ColumnDefinition Width="*" />
                                <ColumnDefinition Width="Auto" />
                            </Grid.ColumnDefinitions>

                            <ComboBox ItemsSource="{Binding Subjects}" SelectedItem="{Binding InputItem.Subject, Mode=TwoWay}"
                                      ItemTemplate="{StaticResource SubjectComboBoxItemTemplate}" Grid.Column="0" />

                            <Button Command="{Binding NewSubjectCommand}" Style="{StaticResource InputNewSubjectButtonStyle}" Grid.Column="1"
                                    Foreground="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}" />
                        </Grid>
                        <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="Subject" Style="{StaticResource InputFieldErrorTextStyle}" />
                    </StackPanel>

                    <StackPanel Style="{StaticResource InputSidebarFormRowStackStyle}">
                        <TextBlock Text="Type" Style="{StaticResource LabelTextStyle}" />
                        <ComboBox ItemsSource="{Binding InputItem.Type, Converter={StaticResource EnumToIEnumerableConverter}}" SelectedItem="{Binding InputItem.Type, Mode=TwoWay}" />
                        <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="Type" Style="{StaticResource InputFieldErrorTextStyle}" />
                    </StackPanel>

                    <StackPanel Style="{StaticResource InputSidebarFormRowStackStyle}"
                                Visibility="{Binding InputItem.Type, Converter={StaticResource ParameterToVisibilityConverter}, ConverterParameter=Revision}">
                        <TextBlock Text="Exam" Style="{StaticResource LabelTextStyle}" />
                        <ComboBox ItemsSource="{Binding ExamsWithNull}" SelectedItem="{Binding SelectedExam, Mode=TwoWay}">
                            <ComboBox.ItemTemplateSelector>
                                <common:ComboBoxItemTemplateSelector>
                                    <common:ComboBoxItemTemplateSelector.NullTemplate>
                                        <DataTemplate>
                                            <TextBlock Text="No Exam" />
                                        </DataTemplate>
                                    </common:ComboBoxItemTemplateSelector.NullTemplate>
                                    <common:ComboBoxItemTemplateSelector.SelectedTemplate>
                                        <DataTemplate>
                                            <TextBlock d:DataContext="{d:DesignInstance data:Exam}">
                                                <Run Text="{Binding Title, Converter={StaticResource StringToTitleCaseConverter}}" /><Run Text="," />
                                                <Run Text="{Binding Date, Converter={StaticResource StringFormatConverter}, ConverterParameter='{}{0:D}'}" />
                                                <Run Text="{Binding Time, Converter={StaticResource TimeSpanToShortStringConverter}}" />
                                            </TextBlock>
                                        </DataTemplate>
                                    </common:ComboBoxItemTemplateSelector.SelectedTemplate>
                                    <common:ComboBoxItemTemplateSelector.DropDownTemplate>
                                        <DataTemplate>
                                            <StackPanel d:DataContext="{d:DesignInstance data:Exam}">
                                                <TextBlock Text="{Binding Title, Converter={StaticResource StringToTitleCaseConverter}}"
                                                            Style="{StaticResource MediumTextStyle}" />
                                                <TextBlock Style="{StaticResource SmallTextStyle}">
                                                    <Run Text="{Binding Date, Converter={StaticResource StringFormatConverter}, ConverterParameter='{}{0:D}'}" />
                                                    <Run Text="{Binding Time}" />
                                                </TextBlock>
                                            </StackPanel>
                                        </DataTemplate>
                                    </common:ComboBoxItemTemplateSelector.DropDownTemplate>
                                </common:ComboBoxItemTemplateSelector>
                            </ComboBox.ItemTemplateSelector>
                        </ComboBox>
                        <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="ExamGuid" Style="{StaticResource InputFieldErrorTextStyle}" />
                    </StackPanel>
                    
                    <StackPanel Style="{StaticResource InputSidebarFormRowStackStyle}">
                        <TextBlock Text="Due Date" Style="{StaticResource LabelTextStyle}" />
                        <DatePicker Date="{Binding InputItem.DueDate, Converter={StaticResource DateTimeToDateTimeOffset}, Mode=TwoWay}" />
                        <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="DueDate" Style="{StaticResource InputFieldErrorTextStyle}" />
                    </StackPanel>

                    <StackPanel x:Name="SColTitleFromRow" Visibility="Collapsed" Style="{StaticResource InputSidebarFormRowStackStyle}">
                        <TextBlock Text="Title" Style="{StaticResource LabelTextStyle}" />
                        <TextBox Text="{Binding InputItem.Title, Mode=TwoWay}" />
                        <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="Title" Style="{StaticResource InputFieldErrorTextStyle}" />
                    </StackPanel>
                </StackPanel>
            </Border>

            <Grid x:Name="Content" Style="{StaticResource InputContentGridStyle}" VerticalAlignment="Stretch" Grid.Column="1" Grid.Row="0" Grid.RowSpan="2">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="*" />
                </Grid.RowDefinitions>

                <Border x:Name="ContentHeader" BorderBrush="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}" Style="{StaticResource InputContentHeaderBorderStyle}">
                    <TextBox PlaceholderText="Add a title" Text="{Binding InputItem.Title, Mode=TwoWay}" Style="{StaticResource InputContentHeaderTextStyle}" />
                </Border>

                <Button x:Name="SaveButton" Foreground="{Binding InputItem.Subject.Color, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}"
                        Command="{Binding SaveCommand}" Style="{StaticResource InputSaveButtonStyle}" Grid.Row="0" />

                <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="Title" Style="{StaticResource InputFieldErrorTextStyle}" Grid.Row="1" />

                <TextBox x:Name="DetailTextBox" Margin="0,24,-60,24" Padding="0,0,60,0" PlaceholderText="Add some detail" Text="{Binding InputItem.Detail, Mode=TwoWay}" BorderThickness="0" HorizontalAlignment="Stretch"
                         VerticalAlignment="Stretch" TextWrapping="Wrap" AcceptsReturn="True" ScrollViewer.VerticalScrollBarVisibility="Auto"  Grid.Row="2" />
            </Grid>
        </Grid>

        <VisualStateManager.VisualStateGroups>
            <VisualStateGroup x:Name="Below1024Group">
                <VisualState x:Name="Below1024Inactive" />
                <VisualState x:Name="Below1024Active">
                    <Storyboard>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="RootScrollViewer" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputRootScrollViewerSnappedStyle}" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Sidebar" Storyboard.TargetProperty="(Grid.ColumnSpan)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="2" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Sidebar" Storyboard.TargetProperty="(Grid.RowSpan)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="1" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Sidebar" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarFormBelow1024Style}" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SidebarHeader" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarHeaderGridSnappedStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="HeaderSelectorButton" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarSelectorButtonSnappedStyle}" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Content" Storyboard.TargetProperty="(Grid.Column)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="0" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Content" Storyboard.TargetProperty="(Grid.ColumnSpan)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="2" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Content" Storyboard.TargetProperty="(Grid.Row)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="1" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Content" Storyboard.TargetProperty="(Grid.RowSpan)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="1" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="ContentHeader" Storyboard.TargetProperty="Visibility">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="Collapsed" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="BackButton" Storyboard.TargetProperty="(Grid.Row)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="0" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SaveButton" Storyboard.TargetProperty="Visibility">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="Collapsed" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SColSaveButton" Storyboard.TargetProperty="Visibility">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="Visible" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="HeaderSelectorButton" Storyboard.TargetProperty="(Grid.Column)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="0" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="HeaderSelectorButton" Storyboard.TargetProperty="(Grid.ColumnSpan)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="2" />
                        </ObjectAnimationUsingKeyFrames>

                        <!-- Task Specific -->
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SColTitleFromRow" Storyboard.TargetProperty="Visibility">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="Visible" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="DetailTextBox" Storyboard.TargetProperty="Margin">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="0,24" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="DetailTextBox" Storyboard.TargetProperty="Padding">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="0" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="DetailTextBox" Storyboard.TargetProperty="(ScrollViewer.VerticalScrollBarVisibility)">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="Disabled" />
                        </ObjectAnimationUsingKeyFrames>
                    </Storyboard>
                </VisualState>
            </VisualStateGroup>
            
            <VisualStateGroup x:Name="Below768Group">
                <VisualState x:Name="Below768Inactive" />
                <VisualState x:Name="Below768Active">
                    <Storyboard>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SidebarInner" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarFormInnerStackFilledStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                    </Storyboard>
                </VisualState>
            </VisualStateGroup>

            <VisualStateGroup x:Name="Below500Group">
                <VisualState x:Name="Below500Inactive" />
                <VisualState x:Name="Below500Active">
                    <Storyboard>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SidebarInner" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarFormInnerStackSnappedStyle}" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SelectorIconBorder" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarSelectorIconBorderNarrowStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SelectorIcon" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarSelectorIconNarrowStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SelectorTitleText" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSidebarSelectorTitleTextNarrowStyle}" />
                        </ObjectAnimationUsingKeyFrames>

                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="Content" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputContentGridSnappedStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                        
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="BackButton" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSnappedBackButtonStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="SColSaveButton" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource InputSnappedSaveButtonStyle}" />
                        </ObjectAnimationUsingKeyFrames>
                    </Storyboard>
                </VisualState>
            </VisualStateGroup>
            
            <VisualStateGroup x:Name="Below320Group">
                <VisualState x:Name="Below320Inactive" />
                <VisualState x:Name="Below320Active" />
            </VisualStateGroup>
        </VisualStateManager.VisualStateGroups>
    </ScrollViewer>
</common:LayoutAwarePage>
