﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Controls;

namespace MyStudyLife.W8.Views.Input {
    public sealed partial class TaskInputView {
        #region Instance

        private static TaskInputView _current;

        public static TaskInputView GetInstance() {
            return _current;
        }

        #endregion

        private new TaskInputViewModel ViewModel {
            get { return base.ViewModel as TaskInputViewModel; }
        }

        public TaskInputView() {
            _current = this;
            
            this.InitializeComponent();
        }

        #region Academic Schedule Selector

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {
            var f = new Flyout {
                Placement = FlyoutPlacementMode.Bottom
            };

            var selector = new AcademicScheduleSelector(f) {
                HelpText = {
                    Text = R.AcademicScheduleTaskExamInputHelp
                }
            };

            selector.SetBinding(AcademicScheduleSelector.AcademicYearsProperty, new Binding {
                Path = new PropertyPath("AcademicYears")
            });
            selector.SetBinding(AcademicScheduleSelector.SelectedScheduleProperty, new Binding {
                Path = new PropertyPath("SelectedSchedule"),
                Mode = BindingMode.TwoWay
			});

            FlyoutBase.SetAttachedFlyout(sender as FrameworkElement, f);
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        #endregion
    }
}
