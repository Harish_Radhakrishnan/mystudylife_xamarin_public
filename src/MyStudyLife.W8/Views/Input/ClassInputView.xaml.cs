﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Cirrious.CrossCore.WeakSubscription;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Controls;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Views.Input {
    public sealed partial class ClassInputView : IHandleRequest, IHandleClose {
        #region Instance

        private static ClassInputView _current;

        public static ClassInputView GetInstance() {
            return _current;
        }

        #endregion

        private new ClassInputViewModel ViewModel {
            get { return base.ViewModel as ClassInputViewModel; }
        }

        public ClassInputView() {
            _current = this;

            this.InitializeComponent();
        }

        #region Academic Schedule Selector

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {

            var f = new Flyout {
                Placement = FlyoutPlacementMode.Bottom
            };

            var selector = new AcademicScheduleSelector(f) {
                HelpText = {
                    Text = R.ClassInput.AcademicScheduleHelp
                },
                ShowNew = true
            };

            selector.SetBinding(AcademicScheduleSelector.AcademicYearsProperty, new Binding {
                Path = new PropertyPath("AcademicYears")
            });
            selector.SetBinding(AcademicScheduleSelector.SelectedScheduleProperty, new Binding {
                Path = new PropertyPath("SelectedSchedule"),
                Mode = BindingMode.TwoWay
            });

            FlyoutBase.SetAttachedFlyout(sender as FrameworkElement, f);
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        #endregion

        #region Class Type

        private void ClassTypeButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            var menu = new MenuFlyout {
                Placement = FlyoutPlacementMode.Bottom,
            };

            FlyoutBase.SetAttachedFlyout(sender as FrameworkElement, menu);

            Action<ClassType, string> createMenuItem = (type, title) => {
                var menuItem = new MenuFlyoutItem {
                    Text = title,
                    Tag = type
                };

                menuItem.Tapped += MenuItemOnTapped;

// ReSharper disable once PossibleNullReferenceException
                menu.Items.Add(menuItem);
            };

            createMenuItem(ClassType.OneOff, "One-off");
            createMenuItem(ClassType.Recurring, "Recurring");

            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        private void MenuItemOnTapped(object sender, TappedRoutedEventArgs e) {
            var type = (ClassType) ((MenuFlyoutItem) sender).Tag;

            this.ViewModel.InputClassType = type;
        }

        #endregion

        public void HandleRequest(MvxViewModelRequest request) {
            if (request.ViewModelType == typeof(ClassTimeInputViewModel)) {
                string guidStr;
                Guid guid;

                ClassTimeInputView view;

                if (request.ParameterValues != null && request.ParameterValues.TryGetValue("TimeGuid", out guidStr) && Guid.TryParse(guidStr, out guid) && !guid.IsEmpty()) {
                    view = this.TimesItemsControl.ItemsViews.SingleOrDefault(x => x.ClassTime != null && x.ClassTime.Guid == guid);
                }
                else {
                    view = this.TimesItemsControl.ItemsViews.SingleOrDefault(x => x.ClassTime == null);
                }

                view.HandleRequest(request);

                this.TimesItemsControl.ItemsViews.Where(x => x != view).Apply(x => x.IsEnabled = false);

                this.SaveButton.IsEnabled = false;
            }
        }

        public void HandleClose(IMvxViewModel viewModel) {
            if (viewModel is ClassTimeInputViewModel) {
                var view = this.TimesItemsControl.ItemsViews.SingleOrDefault(x => x.IsInputActive);

                if (view != null) {
                    view.HandleClose(viewModel);
                }

                this.TimesItemsControl.ItemsViews.Apply(x => x.IsEnabled = true);

                this.SaveButton.IsEnabled = true;
            }
        }
    }

    #region ClassTimeItemsControl

    public sealed class ClassTimeItemsControl : ItemsControl {

        #region Dependency Properties

        public static readonly DependencyProperty TimesProperty = DependencyProperty.Register(
            "Times", typeof (ObservableCollection<ClassTime>), typeof (ClassTimeItemsControl), new PropertyMetadata(null, OnTimesPropertyChanged));

        public ObservableCollection<ClassTime> Times {
            get { return (ObservableCollection<ClassTime>) GetValue(TimesProperty); }
            set { SetValue(TimesProperty, value); }
        }

        private static void OnTimesPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var ctrl = (ClassTimeItemsControl) d;

            if (ctrl.Items != null) {
                ctrl.Items.Clear();
            }

            if (ctrl.Times != null) {
                ctrl.Times.WeakSubscribe(ctrl.TimesOnCollectionChanged);

                if (ctrl.Items != null) {
                    ctrl.Items.AddRange(ctrl.Times);
                }
            }

            if (ctrl.Items != null) {
                ctrl.Items.Add(new ClassTimeInputView.NewTimeModel());
            }
        }

        private void TimesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (this.Items == null) return;

            switch (e.Action) {
                case NotifyCollectionChangedAction.Reset:
                    this.Items.Clear();
                    this.Items.Add(new ClassTimeInputView.NewTimeModel());
                    break;
                case NotifyCollectionChangedAction.Add: {
                    int i = e.NewStartingIndex;

                    foreach (var item in e.NewItems) {
                        this.Items.Insert(i, item);

                        i++;
                    }
                }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems) {
                        this.Items.Remove(item);
                    }
                    break;
            }
        }

        #endregion

        public IEnumerable<ClassTimeInputView> ItemsViews {
            get {
                if (this.ItemsPanelRoot != null) {
                    return this.ItemsPanelRoot.Children.OfType<ClassTimeInputView>();
                }

                return Enumerable.Empty<ClassTimeInputView>();
            }
        }

        protected override bool IsItemItsOwnContainerOverride(object item) {
            return (item is ClassTimeInputView);
        }

        protected override DependencyObject GetContainerForItemOverride() {
            return new ClassTimeInputView();
        }

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item) {
            var view = (ClassTimeInputView) element;

            view.DataContext = this.DataContext;
            view.ClassTime = item as ClassTime;
        }
    }

    #endregion
}
