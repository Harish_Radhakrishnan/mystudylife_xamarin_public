﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Controls;

namespace MyStudyLife.W8.Views.Input {
    public sealed partial class ExamInputView {
        #region Instance

        private static ExamInputView _current;

        public static ExamInputView GetInstance() {
            return _current;
        }

        #endregion

        public new ExamInputViewModel ViewModel {
            get { return base.ViewModel as ExamInputViewModel; }
        }

        public ExamInputView() {
            _current = this;

            this.InitializeComponent();
        }

        #region Academic Schedule Selector

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {
            var f = new Flyout {
                Placement = FlyoutPlacementMode.Bottom
            };

            var selector = new AcademicScheduleSelector(f) {
                HelpText = {
                    Text = R.AcademicScheduleTaskExamInputHelp
                }
            };

            selector.SetBinding(AcademicScheduleSelector.AcademicYearsProperty, new Binding {
                Path = new PropertyPath("AcademicYears")
            });
            selector.SetBinding(AcademicScheduleSelector.SelectedScheduleProperty, new Binding {
                Path = new PropertyPath("SelectedSchedule"),
                Mode = BindingMode.TwoWay
            });

            FlyoutBase.SetAttachedFlyout(sender as FrameworkElement, f);
            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        #endregion

        private void ResitButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            var menu = new MenuFlyout {
                Placement = FlyoutPlacementMode.Bottom,
            };

            FlyoutBase.SetAttachedFlyout(sender as FrameworkElement, menu);

            Action<bool, string> createMenuItem = (isResit, title) => {
                var menuItem = new MenuFlyoutItem {
                    Text = title,
                    Tag = isResit
                };

                menuItem.Tapped += MenuItemOnTapped;

// ReSharper disable once PossibleNullReferenceException
                menu.Items.Add(menuItem);
            };

            createMenuItem(false, "Not A Resit");
            createMenuItem(true, "Resit");

            FlyoutBase.ShowAttachedFlyout(sender as FrameworkElement);
        }

        private void MenuItemOnTapped(object sender, TappedRoutedEventArgs e) {
            this.ViewModel.InputItem.Resit = (bool)((MenuFlyoutItem)sender).Tag;
        }
    }
}
