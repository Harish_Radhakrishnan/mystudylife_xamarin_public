﻿<UserControl
    x:Name="ViewRoot"
    x:Class="MyStudyLife.W8.Views.Input.ClassTimeInputView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:data="using:MyStudyLife.Data"
    xmlns:common="using:MyStudyLife.W8.Common"
    xmlns:converters="using:MyStudyLife.W8.Converters"
    xmlns:vms="using:MyStudyLife.UI.ViewModels.Input"
    mc:Ignorable="d"
    d:DesignWidth="440" d:DesignHeight="300" d:DataContext="{d:DesignInstance vms:ClassInputViewModel}">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Common/InputViewStyles.xaml" />
            </ResourceDictionary.MergedDictionaries>

            <converters:ClassDayTypeToBooleanConverter x:Key="ClassDayTypeToBooleanConverter" />
            <converters:ClassTypeToIntConverter x:Key="ClassTypeToIntConverter" />
            <converters:DaysOfWeekToIntConverter x:Key="DaysOfWeekToIntConverter" />
            <converters:RotationDaysToIntConverter x:Key="RotationDaysToIntConverter" />

            <Style x:Key="TimeButtonStyle" TargetType="Button">
                <Setter Property="Background" Value="White"/>
                <Setter Property="BorderBrush" Value="{StaticResource BrushBorder}"/>
                <Setter Property="BorderThickness" Value="1"/>
                <Setter Property="Padding" Value="12"/>
                <Setter Property="HorizontalAlignment" Value="Stretch"/>
                <Setter Property="HorizontalContentAlignment" Value="Stretch"/>
                <Setter Property="VerticalAlignment" Value="Stretch"/>
                <Setter Property="VerticalContentAlignment" Value="Stretch"/>
                <Setter Property="FontWeight" Value="SemiLight"/>
                <Setter Property="FontSize" Value="{StaticResource MyStudyLifeMediumFontSize}"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="Button">
                            <Grid>
                                <VisualStateManager.VisualStateGroups>
                                    <VisualStateGroup x:Name="CommonStates">
                                        <VisualState x:Name="Normal"/>
                                        <VisualState x:Name="PointerOver">
                                            <Storyboard>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="Border">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource BrushBorder}"/>
                                                </ObjectAnimationUsingKeyFrames>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Pressed">
                                            <Storyboard>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="Border">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource BrushBorder}"/>
                                                </ObjectAnimationUsingKeyFrames>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="(ContentPresenter.Foreground)" Storyboard.TargetName="ContentPresenter">
                                                    <DiscreteObjectKeyFrame KeyTime="0">
                                                        <DiscreteObjectKeyFrame.Value>
                                                            <SolidColorBrush Color="White"/>
                                                        </DiscreteObjectKeyFrame.Value>
                                                    </DiscreteObjectKeyFrame>
                                                </ObjectAnimationUsingKeyFrames>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Disabled" />
                                    </VisualStateGroup>
                                    <VisualStateGroup x:Name="FocusStates">
                                        <VisualState x:Name="Focused">
                                            <Storyboard>
                                                <DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualWhite"/>
                                                <DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualBlack"/>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Unfocused"/>
                                        <VisualState x:Name="PointerFocused"/>
                                    </VisualStateGroup>
                                </VisualStateManager.VisualStateGroups>
                                <Border x:Name="Border" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}" Background="{TemplateBinding Background}">
                                    <ContentPresenter x:Name="ContentPresenter" ContentTemplate="{TemplateBinding ContentTemplate}" ContentTransitions="{TemplateBinding ContentTransitions}"
                                                          Content="{TemplateBinding Content}" HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" Margin="{TemplateBinding Padding}"
                                                          VerticalAlignment="{TemplateBinding VerticalContentAlignment}" Foreground="{TemplateBinding Foreground}"/>
                                </Border>
                                <Rectangle x:Name="FocusVisualWhite" IsHitTestVisible="False" Opacity="0" StrokeDashOffset="1.5" StrokeEndLineCap="Square" Stroke="{StaticResource FocusVisualWhiteStrokeThemeBrush}" StrokeDashArray="1,1"/>
                                <Rectangle x:Name="FocusVisualBlack" IsHitTestVisible="False" Opacity="0" StrokeDashOffset="0.5" StrokeEndLineCap="Square" Stroke="{StaticResource FocusVisualBlackStrokeThemeBrush}" StrokeDashArray="1,1"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>

            <Style x:Key="DeleteTimeButtonStyle" BasedOn="{StaticResource InputDeleteButtonStyle}" TargetType="Button">
                <Setter Property="Margin" Value="0,-6,-6,0" />
            </Style>

            <Style x:Key="CancelInputButtonStyle" TargetType="Button" BasedOn="{StaticResource DeleteTimeButtonStyle}">
                <Setter Property="Content" Value="&#xE10A;" />
                <Setter Property="FontFamily" Value="Segoe UI Symbol" />
                <Setter Property="FontSize" Value="16"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="Button">
                            <Grid>
                                <VisualStateManager.VisualStateGroups>
                                    <VisualStateGroup x:Name="CommonStates">
                                        <VisualState x:Name="Normal">
                                            <Storyboard>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="Border">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="White"/>
                                                </ObjectAnimationUsingKeyFrames>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="PointerOver">
                                            <Storyboard>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="Border">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource BrushSubtle}"/>
                                                </ObjectAnimationUsingKeyFrames>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="White"/>
                                                </ObjectAnimationUsingKeyFrames>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Pressed">
                                            <Storyboard>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Background" Storyboard.TargetName="Border">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource BrushSubtle}"/>
                                                </ObjectAnimationUsingKeyFrames>
                                                <ObjectAnimationUsingKeyFrames Storyboard.TargetProperty="Foreground" Storyboard.TargetName="ContentPresenter">
                                                    <DiscreteObjectKeyFrame KeyTime="0" Value="White"/>
                                                </ObjectAnimationUsingKeyFrames>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Disabled" />
                                    </VisualStateGroup>
                                    <VisualStateGroup x:Name="FocusStates">
                                        <VisualState x:Name="Focused">
                                            <Storyboard>
                                                <DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualWhite"/>
                                                <DoubleAnimation Duration="0" To="1" Storyboard.TargetProperty="Opacity" Storyboard.TargetName="FocusVisualBlack"/>
                                            </Storyboard>
                                        </VisualState>
                                        <VisualState x:Name="Unfocused"/>
                                        <VisualState x:Name="PointerFocused"/>
                                    </VisualStateGroup>
                                </VisualStateManager.VisualStateGroups>
                                <Border x:Name="Border" BorderBrush="{TemplateBinding BorderBrush}" BorderThickness="{TemplateBinding BorderThickness}" Background="{TemplateBinding Background}">
                                    <ContentPresenter x:Name="ContentPresenter" ContentTemplate="{TemplateBinding ContentTemplate}" ContentTransitions="{TemplateBinding ContentTransitions}"
                                                          Content="{TemplateBinding Content}" HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" Margin="{TemplateBinding Padding}"
                                                          VerticalAlignment="{TemplateBinding VerticalContentAlignment}" Foreground="{TemplateBinding Foreground}"/>
                                </Border>
                                <Rectangle x:Name="FocusVisualWhite" IsHitTestVisible="False" Opacity="0" StrokeDashOffset="1.5" StrokeEndLineCap="Square" Stroke="{StaticResource FocusVisualWhiteStrokeThemeBrush}" StrokeDashArray="1,1"/>
                                <Rectangle x:Name="FocusVisualBlack" IsHitTestVisible="False" Opacity="0" StrokeDashOffset="0.5" StrokeEndLineCap="Square" Stroke="{StaticResource FocusVisualBlackStrokeThemeBrush}" StrokeDashArray="1,1"/>
                            </Grid>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
            </Style>
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid x:Name="TimeRoot" Margin="12" VerticalAlignment="Top">
        <Button x:Name="NewCtrl" Width="224" Height="124" Style="{StaticResource TimeButtonStyle}"
                IsEnabled="{Binding ElementName=ViewRoot, Path=IsEnabled}"
                Command="{Binding ElementName=ViewRoot, Path=DataContext.NewTimeCommand}">
            <TextBlock Text="New Time" Foreground="{Binding DataContext.InputItem.Subject.Color, ElementName=ViewRoot, Converter={StaticResource SubjectColorToValueConverter}, FallbackValue={StaticResource BrushAccent}}"
                       FontWeight="Normal" Style="{StaticResource LargeTextStyle}" HorizontalAlignment="Center" VerticalAlignment="Center" />
        </Button>

        <Button x:Name="ViewCtrl" Width="224" Height="124" Style="{StaticResource TimeButtonStyle}"
                IsEnabled="{Binding ElementName=ViewRoot, Path=IsEnabled}"
                Command="{Binding ElementName=ViewRoot, Path=DataContext.EditTimeCommand}" CommandParameter="{Binding}"
                d:DataContext="{d:DesignInstance data:ClassTime}" >

            <Grid HorizontalAlignment="Stretch" VerticalAlignment="Stretch">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="*" />
                    <ColumnDefinition Width="Auto" />
                </Grid.ColumnDefinitions>

                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <TextBlock Text="{Binding Time}" Style="{StaticResource MediumTextStyle}" Grid.Row="0" />
                <TextBlock Text="{Binding Self, Converter={StaticResource ClassOccurrenceConverter}}" TextWrapping="WrapWholeWords"
                           Foreground="{StaticResource BrushSubtleText}" Style="{StaticResource SmallTextStyle}"
                           Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="1" />

                <Button Style="{StaticResource DeleteTimeButtonStyle}"
                        Command="{Binding ElementName=ViewRoot, Path=DataContext.DeleteTimeCommand}" CommandParameter="{Binding}"
                        Grid.Row="0" Grid.RowSpan="2" Grid.Column="1" />
            </Grid>
        </Button>

        <Border x:Name="InputCtrl" Padding="12" BorderThickness="1" BorderBrush="{StaticResource BrushBorder}" Background="White"
                d:DataContext="{d:DesignInstance vms:ClassTimeInputViewModel}">
            <Grid>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition />
                    <ColumnDefinition />
                </Grid.ColumnDefinitions>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <Grid Margin="0,0,0,12" Style="{StaticResource InputContentFormRowStyle}" Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="1"
                      Visibility="{Binding IsDayRotationEnabled, Converter={StaticResource Visibility}}">

                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="{StaticResource InputContentFormInputWidth}" />
                    </Grid.ColumnDefinitions>

                    <TextBlock
                        Text="Day Type"
                        Margin="0,0,12,0"
                        VerticalAlignment="Center"
                        Style="{StaticResource LabelTextStyle}"
                        Grid.Column="0" />
                    <StackPanel Grid.Column="1">
                        <RadioButton
                            x:Name="WeekdayDayTypeRadio"
                            Content="Weekday" GroupName="DayType"
                            IsChecked="{Binding IsDayRotation, Converter={StaticResource InvertedBoolean}, Mode=OneWay}"
                            Command="{Binding IsDayRotationCommand}"
                            CommandParameter="{Binding ElementName=WeekdayDayTypeRadio, Converter={StaticResource InvertedBoolean}, Path=IsChecked, Mode=OneWay}" />
                        <RadioButton
                            x:Name="RotationDayDayTypeRadio"
                            Content="Rotation Day" GroupName="DayType"
                            IsChecked="{Binding IsDayRotation, Mode=OneWay}"
                            Command="{Binding IsDayRotationCommand}"
                            CommandParameter="{Binding ElementName=RotationDayDayTypeRadio, Path=IsChecked, Mode=OneWay}" />
                    </StackPanel>
                </Grid>

                <StackPanel Margin="0,0,0,12" Style="{StaticResource InputContentFormRowStyle}" Grid.Column="0" Grid.Row="1"
                            Visibility="{Binding IsWeekRotationEnabled, Converter={StaticResource Visibility}}">
                    
                    <TextBlock Text="Repeats" Style="{StaticResource LabelTextStyle}" />
                    <ComboBox ItemsSource="{Binding RotationWeeks}" DisplayMemberPath="Item2" SelectedValuePath="Item1"
                              SelectedValue="{Binding InputItem.RotationWeek, Mode=TwoWay}" />
                    <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="RotationWeek"
                               Style="{StaticResource InputFieldErrorTextStyle}" />
                    
                </StackPanel>

                <StackPanel Style="{StaticResource InputContentFormRowStyle}" Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="2">
                    <StackPanel x:Name="RecurringDayPicker" Background="{StaticResource BrushBorder}">
                        <ItemsControl ItemsSource="{Binding Days}"
                                      Visibility="{Binding IsDayRotation, Converter={StaticResource InvertedVisibility}}">
                            <ItemsControl.ItemsPanel>
                                <ItemsPanelTemplate>
                                    <Grid Margin="0,1,1,0" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" 
                                          common:GridHelpers.ColumnCount="{Binding Days.Count}" />
                                </ItemsPanelTemplate>
                            </ItemsControl.ItemsPanel>
                            <ItemsControl.ItemTemplate>
                                <DataTemplate>
                                    <CheckBox Content="{Binding Item2}" Style="{StaticResource SelectorCheckBoxStyle}"
                                              common:ItemsGridLayout.GridColumn="{Binding Item3}"
                                              common:FlagsCheckBox.CheckedIfFlag="{Binding Item1}"
                                              common:FlagsCheckBox.Flags="{Binding
                                                  ElementName=InputCtrl, Path=DataContext.InputItem.Days, Mode=TwoWay,
                                                  Converter={StaticResource DaysOfWeekToIntConverter}
                                              }" />
                                </DataTemplate>
                            </ItemsControl.ItemTemplate>
                        </ItemsControl>

                        <ItemsControl ItemsSource="{Binding RotationDays}"
                                      Visibility="{Binding IsDayRotation, Converter={StaticResource Visibility}}">
                            <ItemsControl.ItemsPanel>
                                <ItemsPanelTemplate>
                                    <Grid Margin="0,1,1,0" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" 
                                          common:GridHelpers.ColumnCount="{Binding RotationDays.Count}" />
                                </ItemsPanelTemplate>
                            </ItemsControl.ItemsPanel>
                            <ItemsControl.ItemTemplate>
                                <DataTemplate>
                                    <CheckBox Content="{Binding Item2}" Style="{StaticResource SelectorCheckBoxStyle}"
                                              common:ItemsGridLayout.GridColumn="{Binding Item3}"
                                              common:FlagsCheckBox.CheckedIfFlag="{Binding Item1}"
                                              common:FlagsCheckBox.Flags="{Binding
                                                  ElementName=InputCtrl, Path=DataContext.InputItem.RotationDays, Mode=TwoWay,
                                                  Converter={StaticResource RotationDaysToIntConverter}
                                              }" />
                                </DataTemplate>
                            </ItemsControl.ItemTemplate>
                        </ItemsControl>
                    </StackPanel>

                    <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="Days" Style="{StaticResource InputFieldErrorTextStyle}" />
                    <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="RotationDays" Style="{StaticResource InputFieldErrorTextStyle}" />
                </StackPanel>

                <StackPanel Margin="0,12,12,12" Style="{StaticResource InputContentFormRowStyle}" Grid.Column="0" Grid.Row="3">
                    <TextBlock Text="Start Time" Style="{StaticResource LabelTextStyle}" />
                    <TimePicker Time="{Binding InputItem.StartTime, Mode=TwoWay}" />
                    <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="StartTime"
                               Style="{StaticResource InputFieldErrorTextStyle}" />
                </StackPanel>

                <StackPanel Margin="12,12,0,12" Style="{StaticResource InputContentFormRowStyle}" Grid.Column="1" Grid.Row="3">
                    <TextBlock Text="End Time" Style="{StaticResource LabelTextStyle}" Grid.Column="0" />
                    <TimePicker Time="{Binding InputItem.EndTime, Mode=TwoWay}" />
                    <TextBlock common:ErrorHelper.Errors="{Binding Errors}" common:ErrorHelper.FieldName="EndTime"
                               Style="{StaticResource InputFieldErrorTextStyle}" />
                </StackPanel>

                <TextBlock
                    common:ErrorHelper.Errors="{Binding Errors}"
                    common:ErrorHelper.FieldName="Time"
                    Margin="0,-6,0,12"
                    Style="{StaticResource InputFieldErrorTextStyle}" 
                    Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="4"/>

                <Button Content="Save" Command="{Binding SaveCommand}" Margin="0,12,0,0"
                        HorizontalAlignment="Stretch" Style="{StaticResource MyStudyLifeButton1Style}"
                        Grid.Column="0" Grid.ColumnSpan="2" Grid.Row="5" />

                <Button Style="{StaticResource CancelInputButtonStyle}"
                        Command="{Binding CancelInputCommand}"
                        Grid.Row="0" Grid.RowSpan="2" Grid.Column="1" />
            </Grid>
        </Border>
    </Grid>
</UserControl>
