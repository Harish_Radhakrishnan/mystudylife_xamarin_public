﻿using System;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.Messenger;
using Windows.UI.Xaml.Controls;
using MyStudyLife.Data;
using MyStudyLife.Messaging;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.View;

namespace MyStudyLife.W8.Views {
    /// <summary>
    ///     The page used to search all entities if the user
    ///     is not on the classes, tasks or exams pages.
    /// </summary>
    public sealed partial class SearchView {
        public SearchView() {
            this.InitializeComponent();
        }

        private void ListViewOnItemClick(object sender, ItemClickEventArgs e) {
            var navigationService = Mvx.Resolve<INavigationService>();

            var cls = e.ClickedItem as Class;

            if (cls != null) {
                navigationService.NavigateTo<ClassViewModel>(new ClassViewModel.NavObject {
                    Guid = cls.Guid
                });

                return;
            }

            var task = e.ClickedItem as Task;
            
            if (task != null) {
                navigationService.NavigateTo<TaskViewModel>(new TaskViewModel.NavObject {
                    Guid = task.Guid
                });

                return;
            }

            var exam = e.ClickedItem as Exam;
            
            if (exam != null) {
                navigationService.NavigateTo<ExamViewModel>(new ExamViewModel.NavObject {
                    Guid = exam.Guid
                });

                return;
            }

            throw new Exception("Unknown clicked item.");
        }
    }
}
