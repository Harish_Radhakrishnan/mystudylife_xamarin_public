﻿using System;
using System.ComponentModel;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Cirrious.CrossCore;
using Cirrious.CrossCore.WeakSubscription;
using MyStudyLife.Scheduling;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Calendar;

namespace MyStudyLife.W8.Views.Calendar {
    public sealed partial class CalendarMonthDayControl {
        private static SolidColorBrush _brushAccent, _brushAccentLight, _brushForegroundLight, _brushSubtleText, _brushSubtleSuper;

        private static SolidColorBrush BrushAccent {
            get { return _brushAccent ?? (_brushAccent = (SolidColorBrush) App.Current.Resources["BrushAccent"]); }
        }
        private static SolidColorBrush BrushAccentLight {
            get { return _brushAccentLight ?? (_brushAccentLight = (SolidColorBrush)App.Current.Resources["BrushAccentLight"]); }
        }
        private static SolidColorBrush BrushForegroundLight {
            get { return _brushForegroundLight ?? (_brushForegroundLight = (SolidColorBrush)App.Current.Resources["BrushForegroundLight"]); }
        }
        private static SolidColorBrush BrushSubtleText {
            get { return _brushSubtleText ?? (_brushSubtleText = (SolidColorBrush)App.Current.Resources["BrushSubtleText"]); }
        }
        private static SolidColorBrush BrushSubtleSuper {
            get { return _brushSubtleSuper ?? (_brushSubtleSuper = (SolidColorBrush) App.Current.Resources["BrushSubtleSuper"]);}
        }

        public new CalendarMonthModel.DayAndEntries DataContext {
            get { return (CalendarMonthModel.DayAndEntries) base.DataContext; }
        }


        public static readonly DependencyProperty IsSnappedProperty = DependencyProperty.Register(
            "IsSnapped", typeof(bool), typeof(CalendarMonthDayControl), new PropertyMetadata(false, IsSnappedOnChanged));

        private static void IsSnappedOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var ctrl = (CalendarMonthDayControl)d;

            ctrl.SetStyles();
        }

        public bool IsSnapped {
            get { return (bool)GetValue(IsSnappedProperty); }
            set { SetValue(IsSnappedProperty, value); }
        }

        private IDisposable _subscription;

        public CalendarMonthDayControl() {
            this.InitializeComponent();

            this.SetStyles();

            OnDataContextChanged(this, null);

            this.DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs e) {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            if (this.DataContext != null) {
                _subscription = this.DataContext.WeakSubscribe(DataContextOnPropertyChanged);

                this.SetState();
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In("IsSelected", "IsOtherMonth", "IsHoliday")) {
                this.SetState();
            }
        }

        private void SetStyles() {
            string suffix = (this.IsSnapped ? "Snapped" : String.Empty) + "Style";

            this.ItemsControl.Style = (Style)this.Resources["ItemsControl" + suffix];
            this.DayLabel.Style = (Style)this.Resources["DayLabel" + suffix];
            this.HolidayGrid.Style = (Style) this.Resources["HolidayGrid" + suffix];
            this.HolidayGlyph.Style = (Style) this.Resources["HolidayGlyph" + suffix];
            this.HolidayNameText.Style = (Style) this.Resources["HolidayNameText" + suffix];
        }

        private void SetState() {
            // Could probably have used visual state manager instead?

            var day = this.DataContext;

            if (day == null) {
                return;
            }

            if (day.IsCurrent) {
                this.DayLabel.Foreground = BrushAccent;
            }
            else if (day.IsOtherMonth) {
                this.DayLabel.Foreground = BrushForegroundLight;
            }
            else {
                this.DayLabel.Foreground = BrushSubtleText;
            }

            if (day.IsSelected) {
                this.RootGrid.Background = BrushAccentLight;
                this.DayLabel.Foreground = new SolidColorBrush(Colors.White);
                this.HolidayGlyph.Foreground = new SolidColorBrush(Colors.White);
                this.HolidayNameText.Foreground = new SolidColorBrush(Colors.White);
            }
            else if (day.IsHoliday) {
                this.RootGrid.Background = BrushSubtleSuper;
                this.HolidayGlyph.Foreground = BrushSubtleText;
                this.HolidayNameText.Foreground = BrushSubtleText;
            }
            else {
                this.RootGrid.Background = new SolidColorBrush(Colors.White);
            }
        }

        private void AgendaEntry_OnTapped(object sender, TappedRoutedEventArgs e) {
            e.Handled = true;

            Mvx.Resolve<INavigationService>().ViewEntry((AgendaEntry)((FrameworkElement)sender).DataContext);
        }
    }
}
