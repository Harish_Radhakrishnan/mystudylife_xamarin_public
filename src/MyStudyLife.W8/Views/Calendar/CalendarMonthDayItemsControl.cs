﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyStudyLife.Scheduling;

namespace MyStudyLife.W8.Views.Calendar {
    public class CalendarMonthDayItemsControl : ItemsControl {
        protected override void PrepareContainerForItemOverride(DependencyObject element, object item) {
            var contentControl = (FrameworkElement)element;

            var agendaEntry = item as AgendaEntry;

            if (agendaEntry != null && agendaEntry.IsPast) {
                contentControl.SetValue(OpacityProperty, 0.5);
            }

            base.PrepareContainerForItemOverride(element, item);
        }
    }
}
