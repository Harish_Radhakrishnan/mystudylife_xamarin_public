﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Views.Calendar {
    public sealed partial class CalendarWeekHolidayControl {
        public static readonly DependencyProperty ShowHolidayNameProperty = DependencyProperty.Register(
            "ShowHolidayName", typeof(bool), typeof(CalendarWeekHolidayControl), new PropertyMetadata(true, ShowHolidayNameChanged));

        private static void ShowHolidayNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((CalendarWeekHolidayControl)d).SetStyles();
        }

        public bool ShowHolidayName {
            get { return (bool) GetValue(ShowHolidayNameProperty); }
            set { SetValue(ShowHolidayNameProperty, value); }
        }

        public CalendarWeekHolidayControl() {
            this.InitializeComponent();

            this.DataContextChanged += (s, e) => this.SetStyles();

            this.SetStyles();
        }

        private void SetStyles() {
            if (this.ShowHolidayName) {
                this.Glyph.HorizontalAlignment = HorizontalAlignment.Left;
                Grid.SetColumnSpan(this.Glyph, 1);
                this.Label.Visibility = Visibility.Visible;
            }
            else {
                this.Glyph.HorizontalAlignment = HorizontalAlignment.Center;
                Grid.SetColumnSpan(this.Glyph, 2);
                this.Label.Visibility = Visibility.Collapsed;
            }
        }
    }
}
