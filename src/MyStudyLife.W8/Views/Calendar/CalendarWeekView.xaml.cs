﻿using System;
using System.ComponentModel;
using System.Linq;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using Cirrious.CrossCore.WeakSubscription;
using MyStudyLife.Globalization;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.W8.Common;
using MyStudyLife.W8.Converters;

namespace MyStudyLife.W8.Views.Calendar {
    public sealed partial class CalendarWeekView {
        private static WeakReference<CalendarWeekView> _instanceRef;

        public static CalendarWeekView Instance {
            get {
                CalendarWeekView instance;

                if (_instanceRef != null && _instanceRef.TryGetTarget(out instance)) {
                    return instance;
                }

                return null;
            }
        }

        public double CurrentScrollOffset;

        public const double HourHeight = 64d;

        private GridLength? _hourRowHeight;
        public GridLength HourRowHeight {
            get {
                if (!_hourRowHeight.HasValue) {
                    _hourRowHeight = new GridLength(HourHeight);
                }

                return _hourRowHeight.Value;
            }
        }

        public new CalendarWeekViewModel ViewModel {
            get { return (CalendarWeekViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private readonly DispatcherTimer _timer;

        public CalendarWeekView() {
            _instanceRef = new WeakReference<CalendarWeekView>(this);

            this.InitializeComponent();

            this.SizeChanged += (s, e) => this.SetCurrentTimelineOffset();
            this.WeekFlipView.SelectionChanged += WeekFlipViewOnSelectionChanged;

            var n = DateTime.Now;

            _timer = new DispatcherTimer {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                Interval = TimeSpan.FromSeconds(60 - n.Second)
            };

            _timer.Tick += TimerOnTick;

            _timer.Start();
        }

        private void WeekFlipViewOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (e.AddedItems.Count == 0) {
                return;
            }

            var selectedItem = this.WeekFlipView.ItemContainerGenerator.ContainerFromItem(e.AddedItems[0]);

            if (selectedItem != null) {
                selectedItem.FindVisualChildren<ScrollViewer>().First().ScrollToVerticalOffset(CurrentScrollOffset);
                selectedItem.FindVisualChildren<CalendarWeekLayout>().First().SetTimelineOffset();
            }
        }

        private void TimerOnTick(object sender, object o) {
            if (_timer.Interval.TotalMinutes < 1.0) {
                _timer.Interval = TimeSpan.FromMinutes(1);
            }

            this.SetCurrentTimelineOffset();
        }

        private void SetCurrentTimelineOffset() {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (this.WeekFlipView != null && this.WeekFlipView.SelectedItem != null) {
                var selectedItem = this.WeekFlipView.ItemContainerGenerator.ContainerFromItem(this.WeekFlipView.SelectedItem);

                if (selectedItem != null) {
                    selectedItem.FindVisualChildren<CalendarWeekLayout>().First().SetTimelineOffset();
                }
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            var calendarMonthView = Frame.BackStack.LastOrDefault(entry => entry.SourcePageType == typeof (CalendarMonthView));

            if (calendarMonthView != null) {
                Frame.BackStack.Remove(calendarMonthView);
            }
        }

        // For some reason the command doesn't bind on the button.
        // Might be because we're creating the items control in C#.
        private void AgendaEntryOnTapped(object sender, TappedRoutedEventArgs e) {
            this.ViewModel.ViewEntryCommand.Execute(((FrameworkElement)sender).DataContext);
        }
    }

    public class CalendarWeekLayout : Grid {
        public const double HourHeight = 64d;

        protected virtual double HourColumnWidth {
            get { return 50d; }
        }

        protected virtual DataTemplate EntryItemTemplate {
            get { return (DataTemplate) CalendarWeekView.Instance.Resources["EntryItemTemplate"]; }
        }

        #region Resources

        private SolidColorBrush _brushSubtle;
        private SolidColorBrush _brushSubtleSuper;
        private SolidColorBrush _brushSubtleText;
        private Style _hourLabelStyle;

        private SolidColorBrush BrushSubtle {
            get { return _brushSubtle ?? (_brushSubtle = (SolidColorBrush)App.Current.Resources["BrushSubtle"]); }
        }
        private SolidColorBrush BrushSubtleSuper {
            get { return _brushSubtleSuper ?? (_brushSubtleSuper = (SolidColorBrush)App.Current.Resources["BrushSubtleSuper"]); }
        }
        private SolidColorBrush BrushSubtleText {
            get { return _brushSubtleText ?? (_brushSubtleText = (SolidColorBrush)App.Current.Resources["BrushSubtleText"]); }
        }
        private Style HourLabelStyle {
            get { return _hourLabelStyle ?? (_hourLabelStyle = (Style)CalendarWeekView.Instance.Resources["TimetableTimeTextStyle"]);}
        }

        #endregion

        private bool _userScrolled;
        private IDisposable _subscription;

        private Line _timeline;
        private Border _currentDayHighlight;

        public new CalendarWeekModel DataContext {
            get { return (CalendarWeekModel)base.DataContext; }
        }

        public CalendarWeekLayout() {
            this.Initialize();
        }

        private void Initialize() {
            var grid = this;

            grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(HourColumnWidth)
            });

            for (int i = 0; i < 7; i++) {
                grid.ColumnDefinitions.Add(new ColumnDefinition());

                var l = CreateStretchableLine(Orientation.Vertical);

                l.HorizontalAlignment = HorizontalAlignment.Left;

                Grid.SetColumn(l, i + 1);
                Grid.SetRow(l, 0);
                Grid.SetRowSpan(l, 24);

                grid.Children.Add(l);
            }

            for (int i = 0; i <= 24; i++) {
                var l = CreateStretchableLine(Orientation.Horizontal);

                l.VerticalAlignment = i == 24 ? VerticalAlignment.Bottom : VerticalAlignment.Top;

                Grid.SetColumn(l, 1);
                Grid.SetColumnSpan(l, 7);
                Grid.SetRow(l, i == 24 ? 23 : i);

                grid.Children.Add(l);

                if (i < 24) {
                    grid.RowDefinitions.Add(new RowDefinition {
                        Height = new GridLength(HourHeight)
                    });

                    var timeLabel = new TextBlock {
                        Text = DateTimeFormat.Hours[i],
                        Style = HourLabelStyle
                    };

                    Grid.SetRow(timeLabel, i);

                    grid.Children.Add(timeLabel);

                    var lHalf = CreateStretchableLine(Orientation.Horizontal);

                    lHalf.Stroke = BrushSubtleSuper;
                    lHalf.VerticalAlignment = VerticalAlignment.Center;

                    Grid.SetColumn(lHalf, 1);
                    Grid.SetColumnSpan(lHalf, 7);
                    Grid.SetRow(lHalf, i);

                    grid.Children.Add(lHalf);
                }
            }

            _currentDayHighlight = new Border {
                Background = new SolidColorBrush((Color)App.Current.Resources["ColorAccent"]) {
                    Opacity = 0.1
                }
            };

            _currentDayHighlight.SetBinding(
                VisibilityProperty,
                new Binding {
                    Path = new PropertyPath("IsCurrentWeek"),
                    Converter = new VisibilityConverter()
                }
            );
            _currentDayHighlight.SetBinding(
                Grid.ColumnProperty,
                new Binding {
                    Path = new PropertyPath("CurrentDayIndex"),
                    Converter = new CurrentDayIndexConverter()
                }
            );

            Grid.SetRowSpan(_currentDayHighlight, 24);

            grid.Children.Add(_currentDayHighlight);

            var itemsControl = new CalendarWeekItemsControl {
                DayWidth = (CalendarWeekView.Instance.ActualWidth - HourColumnWidth) / 7f,
                ItemTemplate = EntryItemTemplate,
                Style = (Style)CalendarWeekView.Instance.Resources["TimetableItemsControlStyle"],
            };

            itemsControl.SetBinding(
                ItemsControl.ItemsSourceProperty,
                new Binding {
                    Path = new PropertyPath("Entries")
                }
            );

            Grid.SetColumn(itemsControl, 1);
            Grid.SetColumnSpan(itemsControl, 7);
            Grid.SetRowSpan(itemsControl, 24);

            grid.Children.Add(itemsControl);

            _timeline = new Line {
                X1 = 0,
                X2 = Window.Current.Bounds.Width - HourColumnWidth,
                Y1 = 0,
                Y2 = 0,
                Stroke = (SolidColorBrush) App.Current.Resources["BrushAccent"],
                StrokeThickness = 2.0,
                Transitions = new TransitionCollection {
                    new RepositionThemeTransition()
                }
            };

            _timeline.SetBinding(
                VisibilityProperty,
                new Binding {
                    Path = new PropertyPath("IsCurrentWeek"),
                    Converter = new VisibilityConverter()
                }
            );

            this.SetTimelineOffset();

            Grid.SetColumn(_timeline, 1);
            Grid.SetColumnSpan(_timeline, 7);
            Grid.SetRowSpan(_timeline, 24);

            grid.Children.Add(_timeline);

            this.Loaded += OnLoaded;
            this.SizeChanged += OnSizeChanged;
        }

        /// <summary>
        ///     Creates a line that can be stetched to fit
        ///     a <see cref="Grid"/> for example. For vertical
        ///     stretching <c>Y2 = 1</c> must be set, for
        ///     horizontal <c>X2 = 1</c> must be set.
        /// </summary>
        private Line CreateStretchableLine(Orientation orientation) {
            var l = new Line {
                Stretch = Stretch.Fill,
                Stroke = BrushSubtle,
                StrokeThickness = 1.0,
                StrokeStartLineCap = PenLineCap.Square,
                StrokeEndLineCap = PenLineCap.Square
            };

            if (orientation == Orientation.Horizontal) {
                l.X2 = 1;
                l.HorizontalAlignment = HorizontalAlignment.Stretch;
            }
            else {
                l.Y2 = 1;
                l.VerticalAlignment = VerticalAlignment.Stretch;
            }

            return l;
        }


        public void SetTimelineOffset() {
            if (_timeline != null) {
                double vOffset = DateTime.Now.TimeOfDay.TotalHours * HourHeight;

                _timeline.X2 = Window.Current.Bounds.Width - HourColumnWidth;
                _timeline.Y1 = vOffset;
                _timeline.Y2 = vOffset;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            var scrollViewer = (ScrollViewer)this.Parent;

            scrollViewer.ChangeView(null, CalendarWeekView.Instance.CurrentScrollOffset, null, true);

            scrollViewer.ViewChanged += ScrollViewerOnScroll;

            if (this.DataContext != null) {
                if (this.DataContext.InitialTime.HasValue) {
                    DoFirstScroll();
                }
                else {
                    _subscription = this.DataContext.WeakSubscribe(DataContextOnPropertyChanged);
                }
            }
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e) {
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (e.NewSize.Width == e.PreviousSize.Width) {
                return;
            }

            var gridWidth = (e.NewSize.Width - HourColumnWidth);

            this.Children.OfType<CalendarWeekItemsControl>().First().DayWidth = gridWidth / 7f;
        }

        private void ScrollViewerOnScroll(object sender, ScrollViewerViewChangedEventArgs e) {
            _userScrolled = true;

            CalendarWeekView.Instance.CurrentScrollOffset = ((ScrollViewer)sender).VerticalOffset;
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "InitialTime") {
                if (_subscription != null) {
                    _subscription.Dispose();
                    _subscription = null;
                }

                DoFirstScroll();
            }
        }

        private void DoFirstScroll() {
            try {
                var dataContext = this.DataContext; // Prevent 2 casts!

                if (dataContext == null) {
                    return;
                }

                var initialTime = this.DataContext.InitialTime;

                if (!this._userScrolled && initialTime.HasValue && CalendarWeekView.Instance.CurrentScrollOffset <= 0) {
                    var scrollOffset = initialTime.Value.TotalHours * HourHeight;

                    ((ScrollViewer)this.Parent).ChangeView(null, scrollOffset, null);

                    CalendarWeekView.Instance.CurrentScrollOffset = scrollOffset;
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause, RedundantCatchClause
            catch {
#if DEBUG
                throw;
#endif
            }
        }

        class CurrentDayIndexConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, string language) {
                return ((int) value) + 1;
            }

            public object ConvertBack(object value, Type targetType, object parameter, string language) {
                throw new NotSupportedException();
            }
        }
    }

    public class CalendarWeekSnappedLayout : CalendarWeekLayout {
        protected override double HourColumnWidth {
            get { return 30d; }
        }

        protected override DataTemplate EntryItemTemplate {
            get { return (DataTemplate)CalendarWeekView.Instance.Resources["EntrySnappedItemTemplate"]; }
        }
    }
}
