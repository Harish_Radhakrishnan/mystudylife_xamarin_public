﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.Calendar;

namespace MyStudyLife.W8.Views.Calendar {
    public sealed partial class CalendarMonthView {
        public CalendarMonthView() {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            var calendarWeekView = Frame.BackStack.LastOrDefault(entry => entry.SourcePageType == typeof(CalendarWeekView));

            if (calendarWeekView != null) {
                Frame.BackStack.Remove(calendarWeekView);
            }
        }
    }

    public class CalendarMonthLayout : Grid {
        public static readonly DependencyProperty IsSnappedProperty = DependencyProperty.Register(
            "IsSnapped", typeof(bool), typeof(CalendarMonthDayControl), new PropertyMetadata(false, IsSnappedOnChanged));

        private static void IsSnappedOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var ctrl = (CalendarMonthLayout) d;

            ctrl.Children.OfType<CalendarMonthDayControl>().Apply(x => x.IsSnapped = (bool) e.NewValue);
        }

        public bool IsSnapped {
            get { return (bool)GetValue(IsSnappedProperty); }
            set { SetValue(IsSnappedProperty, value); }
        }

        public CalendarMonthLayout() {
            int i = 0;

            for (int w = 0; w < 6; w++) {
                this.RowDefinitions.Add(new RowDefinition());

                for (int d = 0; d < 7; d++) {
                    if (w == 0) {
                        this.ColumnDefinitions.Add(new ColumnDefinition());
                    }

                    var dayControl = new CalendarMonthDayControl {
                        IsSnapped = this.IsSnapped
                    };

                    Grid.SetRow(dayControl, w);
                    Grid.SetColumn(dayControl, d);

                    dayControl.SetBinding(DataContextProperty, new Binding {
                        Path = new PropertyPath(String.Concat("Days[", i, "]"))
                    });

                    dayControl.Tapped += DayControlOnTapped;

                    this.Children.Add(dayControl);

                    i++;
                }
            }
        }

        private void DayControlOnTapped(object sender, TappedRoutedEventArgs e) {
            ((CalendarMonthModel)this.DataContext).SelectedDay = ((CalendarMonthDayControl)sender).DataContext;
        }
    }
}
