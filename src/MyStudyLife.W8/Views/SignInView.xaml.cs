﻿using System;
using System.ComponentModel;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Views {
    public sealed partial class SignInView {
        public new SignInViewModel ViewModel {
            get { return this.DataContext as SignInViewModel; }
        }

        public SignInView() {
            this.InitializeComponent();
            
            this.Loaded += OnLoaded;

            this.MslEmailText.KeyDown += MslEmailTextOnKeyDown;
            this.MslPasswordText.KeyDown += MslPasswordTextOnKeyDown;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            this.ViewModel.PropertyChanged += ViewModelOnPropertyChanged;   
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In("IsBusy", "MslIsBusy")) {
                this.CredentialsStack.Visibility = (!this.ViewModel.IsBusy || this.ViewModel.MslIsBusy).ToVisibility();
                this.Distraction.Focus(FocusState.Programmatic);
            }
        }

        private void MslEmailTextOnKeyDown(object sender, KeyRoutedEventArgs e) {
            if (e.Key.In(VirtualKey.Enter, VirtualKey.Execute)) {
                this.MslPasswordText.Focus(FocusState.Keyboard);
            }
        }

        private void MslPasswordTextOnKeyDown(object sender, KeyRoutedEventArgs e) {
            if (e.Key.In(VirtualKey.Enter, VirtualKey.Execute)) {
                this.Distraction.Focus(FocusState.Programmatic);
                this.ViewModel.MslSignInCommand.Execute(null);
            }
        }
    }
}
