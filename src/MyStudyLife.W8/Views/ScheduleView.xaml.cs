﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.W8.Behaviors;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Views {
    public sealed partial class ScheduleView {
        private static readonly WeakReference<ScheduleView> _instanceReference = new WeakReference<ScheduleView>(null);

        public static ScheduleView Instance {
            get {
                ScheduleView instance;

                _instanceReference.TryGetTarget(out instance);

                return instance;
            }
        }

        public new ScheduleViewModel ViewModel => (ScheduleViewModel)base.ViewModel;

        public ScheduleView() {
            _instanceReference.SetTarget(this);

            this.InitializeComponent();
        }

        private void HandleRightTap(object sender, RightTappedRoutedEventArgs e) {
            // Prevents being able to deselect elements
            e.Handled = true;
        }

        protected override void ActivateStates(Control ctrl) {
            base.ActivateStates(ctrl);

            var width = Window.Current.Bounds.Width;

            // Special state for the schedule view
            VisualStateManager.GoToState(ctrl, width < 672 ? "Below672Active" : "Below672Inactive", false);
        }
    }

    /// <summary>
    ///     A toggle button that can only be checked, not unchecked.
    /// </summary>
    public class UncheckableToggleButton : ToggleButton {
        protected override void OnToggle() {
            if (!this.IsChecked.GetValueOrDefault()) {
                base.OnToggle();
            }
        }
    }

    public class CheckedWhenSelectedItemBehavior : Behavior<ToggleButton> {
        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            "SelectedItem", typeof(object), typeof(CheckedWhenSelectedItemBehavior), new PropertyMetadata(null, PropertyChanged));

        public static readonly DependencyProperty ItemProperty = DependencyProperty.Register(
            "Item", typeof(object), typeof(CheckedWhenSelectedItemBehavior), new PropertyMetadata(null, PropertyChanged));

        public object SelectedItem {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public object Item {
            get { return GetValue(ItemProperty); }
            set { SetValue(ItemProperty, value); }
        }

        private static void PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((CheckedWhenSelectedItemBehavior)d).SetChecked();
        }

        protected override void OnAttached() {
            this.SetChecked();
        }

        protected override void OnDetached() {}

        private void SetChecked() {
            if (this.AssociatedObject == null) {
                return;
            }

            this.AssociatedObject.IsChecked = (
                (this.SelectedItem == null && this.Item == null) ||
                (this.SelectedItem != null && this.Item != null && this.SelectedItem.Equals(this.Item))
            );
        }
    }

    public class SelectedAcademicYearVisibilityBehavior : Behavior<FrameworkElement> {
        public static readonly DependencyProperty SelectedAcademicYearProperty = DependencyProperty.Register(
            "SelectedAcademicYear", typeof(AcademicYear), typeof(SelectedAcademicYearVisibilityBehavior), new PropertyMetadata(null, AcademicYearChanged));

        public static readonly DependencyProperty AcademicYearProperty = DependencyProperty.Register(
            "AcademicYear", typeof(AcademicYear), typeof(SelectedAcademicYearVisibilityBehavior), new PropertyMetadata(null, AcademicYearChanged));

        public AcademicYear SelectedAcademicYear {
            get { return (AcademicYear)GetValue(SelectedAcademicYearProperty); }
            set { SetValue(SelectedAcademicYearProperty, value); }
        }

        public AcademicYear AcademicYear {
            get { return (AcademicYear)GetValue(AcademicYearProperty); }
            set { SetValue(AcademicYearProperty, value); }
        }

        private static void AcademicYearChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((SelectedAcademicYearVisibilityBehavior)d).SetVisibility();
        }

        protected override void OnAttached() {
            this.SetVisibility();
        }

        protected override void OnDetached() {}

        private void SetVisibility() {
            if (this.AssociatedObject == null) {
                return;
            }

            if (this.AcademicYear == null || this.SelectedAcademicYear == null) {
                this.AssociatedObject.Visibility = Visibility.Collapsed;
            }
            else {
                this.AssociatedObject.Visibility = (this.AcademicYear.Guid == this.SelectedAcademicYear.Guid).ToVisibility();
            }
        }
    }

    public class SchoolVisibilityConverter : DependencyObject, IValueConverter {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof (User), typeof (SchoolVisibilityConverter), new PropertyMetadata(null));

        public User User {
            get { return (User) GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, string language) {
            if (value == null || this.User == null) {
                return Visibility.Collapsed;
            }

            return (((IBelongToSchoolOrUser) value).SchoolId.HasValue && this.User.IsStudentWithSchool).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }

    public class HolidayPushesScheduleVisibilityConverter : DependencyObject, IValueConverter {
        public static readonly DependencyProperty AcademicYearProperty = DependencyProperty.Register(
            "AcademicYear", typeof(AcademicYear), typeof(HolidayPushesScheduleVisibilityConverter), new PropertyMetadata(null));

        public AcademicYear AcademicYear {
            get { return (AcademicYear)GetValue(AcademicYearProperty); }
            set { SetValue(AcademicYearProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, string language) {
            if (value == null || this.AcademicYear == null) {
                return Visibility.Collapsed;
            }

            return ((Holiday)value).EffectivePushesSchedule(this.AcademicYear).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}