﻿using System;
using System.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters {
    /// <summary>
    /// Value converter that translates to <paramref name="parameter"/> if <paramref name="value"/> is greater than 0
    /// and to the opposite of <paramref name="parameter"/> if null.
    /// <paramref name="parameter"/> should be a <see cref="Visibility"/> or <see cref="String"/> equivalent.
    /// 
    /// If parameter is not supplied <see cref="Visibility.Visible"/> is return if the count is 0
    /// and <see cref="Visibility.Collapsed"/> greater than 0.
    /// </summary>
    public sealed class CountToVisibilityConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            Visibility vParameter;

            if (parameter == null)
                vParameter = Visibility.Visible;

            else if (!(parameter is Visibility)) {
                try {
                    vParameter = (Visibility)Enum.Parse(typeof(Visibility), parameter.ToString(), true);
                }
                catch (Exception) {
                    return value;
                }
            }
            else
                vParameter = (Visibility)parameter;

            int? count = null;

            if (value is int) {
                count = (int)value;
            }
            else {
                IList list = value as IList;

                if (list != null) {
                    count = list.Count;
                }
                else {
                    IEnumerable enumerable = value as IEnumerable;

                    if (enumerable != null) {
                        count = 0;

                        var enumerator = enumerable.GetEnumerator();

                        while (enumerator.MoveNext()) {
                            count++;
                        }
                    }
                }
            }

            return count.GetValueOrDefault() > 0 ? vParameter : vParameter == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}