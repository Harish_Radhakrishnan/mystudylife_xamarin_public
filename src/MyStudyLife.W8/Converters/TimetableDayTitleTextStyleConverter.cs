﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using MyStudyLife.W8.Views.Calendar;

namespace MyStudyLife.W8.Converters {
    [System.Diagnostics.DebuggerStepThrough]
    public class TimetableDayTitleTextStyleConverter : IValueConverter {
        static Style _currentDayStyle, _standardDayStyle;

        public object Convert(object value, Type targetType, object parameter, string language) {
            var weekView = CalendarWeekView.Instance;

            if (weekView == null) {
                return null;
            }

            if (_standardDayStyle == null || _currentDayStyle == null) {
                _standardDayStyle = (Style) weekView.Resources["TimetableDayTitleTextStyle"];
                _currentDayStyle = (Style) weekView.Resources["TimetableCurrentDayTitleTextStyle"];
            }

            return DateTime.Today == ((DateTime)value).Date ? _currentDayStyle : _standardDayStyle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}
