﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters
{
    /// <summary>
    /// Value converter that translates a percentage to a width from the maximum width given by the <paramref name="parameter"/>.
    /// </summary>
    //[System.Diagnostics.DebuggerStepThrough]
    public sealed class PercentageToWidthConverter : IValueConverter
    {    
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double dValue, dParameter;

            if (!Double.TryParse(value.ToString(), out dValue) || !Double.TryParse(parameter.ToString(), out dParameter))
                return value;

            return (dValue / 100d) * dParameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}