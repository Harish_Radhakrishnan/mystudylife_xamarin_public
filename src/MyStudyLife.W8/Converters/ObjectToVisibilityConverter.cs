﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters {
    /// <summary>
    /// Value converter that translates to <paramref name="parameter"/> if <paramref name="value"/> is not null
    /// and to the opposite of <paramref name="parameter"/> if null.
    /// 
    /// If the value is an empty string it is treated as null.
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public sealed class ObjectToVisibilityConverter : IValueConverter {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param> 
        /// <param name="parameter"/> should be a <see cref="Visibility"/> or <see cref="String"/> equivalent.
        /// <param name="language"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, string language) {
            Visibility vParameter;

            if (parameter == null)
                vParameter = Visibility.Visible;
            else if (!(parameter is Visibility)) {
                if (!Enum.TryParse<Visibility>(parameter.ToString(), out vParameter))
                    return value;
            }
            else
                vParameter = (Visibility)parameter;

            return (value is string ? !String.IsNullOrWhiteSpace(value.ToString()) : value != null) ? vParameter : vParameter == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}
