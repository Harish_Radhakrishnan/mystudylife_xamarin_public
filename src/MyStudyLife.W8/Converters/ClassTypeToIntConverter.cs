﻿using System;
using Windows.UI.Xaml.Data;
using MyStudyLife.Data;

namespace MyStudyLife.W8.Converters {
    public class ClassTypeToIntConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            return (int) ((ClassType?) value).GetValueOrDefault();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            return (ClassType)(int)value;
        }
    }
}
