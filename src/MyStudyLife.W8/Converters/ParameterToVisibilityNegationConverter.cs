﻿using System;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters
{
    public class ParameterToVisibilityNegationConverter : IValueConverter
    {
        /// <summary>
        /// Returns Visible if the value does not equal the parameter, collapsed otherwise.
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.DebuggerStepThrough]
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value.ToString() != parameter.ToString() ? Windows.UI.Xaml.Visibility.Visible : Windows.UI.Xaml.Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
