﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters {
	/// <summary>
	///		Value converter that translates true to parameter and false to
	///		opposite of the parameter.
	/// </summary>
	public sealed class BooleanToVisibilityConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, string language) {
			Visibility vParameter = Visibility.Visible;

			if (parameter != null) {
				if (!(parameter is Visibility)) {
					if (!Enum.TryParse(parameter.ToString(), out vParameter))
						return value;
				}
				else if (!String.IsNullOrWhiteSpace(parameter.ToString()))
					vParameter = (Visibility)parameter;
			}

			return (value is bool && (bool)value) ? vParameter : vParameter == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language) {
			return value is Visibility && (Visibility)value == Visibility.Visible;
		}
	}
}
