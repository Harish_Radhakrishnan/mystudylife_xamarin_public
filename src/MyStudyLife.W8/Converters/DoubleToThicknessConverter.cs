﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using MyStudyLife.Extensions;

namespace MyStudyLife.W8.Converters {
    public class DoubleToThicknessConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            if (!(value is double)) {
                throw new ArgumentException("Must be of type double", "value");
            }

            if (parameter != null) {
                if (!parameter.ToString().In("Left", "Top", "Right", "Bottom")) {
                    throw new ArgumentException("Must be Left, Top, Right or Bottom", "parameter");
                }
            }
            else {
                parameter = "Top";
            }

            double left = 0, top = 0, right = 0, bottom = 0;

            switch (parameter.ToString()) {
                case "Left":
                    left = (double) value;
                    break;
                case "Top":
                    top = (double) value;
                    break;
                case "Right":
                    right = (double) value;
                    break;
                case "Bottom":
                    bottom = (double) value;
                    break;
            }

            return new Thickness(left, top, right, bottom);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}
