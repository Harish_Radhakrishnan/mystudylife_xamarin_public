﻿using System;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters {
	/// <summary>
	/// Value converter that translates to <paramref name="parameter"/> if <paramref name="value"/> is not null.
	/// </summary>
	[System.Diagnostics.DebuggerStepThrough]
	public sealed class ObjectToValueConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, string language) {
			return value != null ? parameter : null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language) {
			throw new NotSupportedException();
		}
	}
}
