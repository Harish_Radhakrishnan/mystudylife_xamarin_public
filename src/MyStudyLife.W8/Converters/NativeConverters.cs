﻿using Cirrious.CrossCore.WindowsCommon.Converters;

namespace MyStudyLife.W8.Converters {
    #region Plugins

    public class VisibilityConverter : MvxNativeValueConverter<Cirrious.MvvmCross.Plugins.Visibility.MvxVisibilityValueConverter> { }
    public class InvertedVisibilityConverter : MvxNativeValueConverter<Cirrious.MvvmCross.Plugins.Visibility.MvxInvertedVisibilityValueConverter> { }

    public class NativeColorConverter : MvxNativeValueConverter<Cirrious.MvvmCross.Plugins.Color.MvxNativeColorValueConverter> { }

    #endregion

    public class BooleanConverter : MvxNativeValueConverter<UI.Converters.BooleanConverter> { }
    public class InvertedBooleanConverter : MvxNativeValueConverter<UI.Converters.InvertedBooleanConverter> { }
    public class BooleanToIntConverter : MvxNativeValueConverter<UI.Converters.BooleanToIntConverter> { }

	public class BooleanNegationConverter : MvxNativeValueConverter<UI.Converters.BooleanNegationConverter> { }
	public class CountToBooleanConverter : MvxNativeValueConverter<UI.Converters.CountToBooleanConverter> { }
	public class CountToBooleanNegationConverter : MvxNativeValueConverter<UI.Converters.CountToBooleanNegationConverter> { }
	public class ObjectToBooleanConverter : MvxNativeValueConverter<UI.Converters.ObjectToBooleanConverter> { }
    
	public class CoalesceConverter : MvxNativeValueConverter<UI.Converters.CoalesceConverter> { }

    public class LocalDateTimeFormatConverter : MvxNativeValueConverter<UI.Converters.LocalDateTimeFormatConverter> { }
    public class AbbrLongDateFormatConverter : MvxNativeValueConverter<UI.Converters.AbbrLongDateFormatConverter> { }
    public class DateTimeToDateTimeOffsetConveter : MvxNativeValueConverter<UI.Converters.DateTimeToDateTimeOffsetConverter> { }

	public class EnumToBooleanConverter : MvxNativeValueConverter<UI.Converters.EnumToBooleanConverter> { }
	public class EnumToIEnumerableConverter : MvxNativeValueConverter<UI.Converters.EnumToIEnumerableConverter> { }
	public class EnumToTitleConverter : MvxNativeValueConverter<UI.Converters.EnumToTitleConverter> { }

	#region Custom Enums
	public class ClassTypeToBooleanConverter : MvxNativeValueConverter<UI.Converters.ClassTypeToBooleanConverter> { }
	public class ClassDayTypeToBooleanConverter : MvxNativeValueConverter<UI.Converters.ClassDayTypeToBooleanConverter> { }
	public class DaysOfWeekToIntConverter : MvxNativeValueConverter<UI.Converters.DaysOfWeekToIntConverter> { }
    public class RotationDaysToIntConverter : MvxNativeValueConverter<UI.Converters.RotationDaysToIntConverter> { }
	#endregion

    public class IntToFlagConverter : MvxNativeValueConverter<UI.Converters.IntToFlagConverter> { }

    public class ParameterToBooleanConverter : MvxNativeValueConverter<UI.Converters.ParameterToBooleanConverter> { }
    public class ParameterToBooleanNegationConverter : MvxNativeValueConverter<UI.Converters.ParameterToBooleanNegationConverter> { }

    public class PluralFormatConverter : MvxNativeValueConverter<UI.Converters.PluralFormatConverter> { }

    public class PluralFormatWithValueConverter : MvxNativeValueConverter<UI.Converters.PluralFormatWithValueConverter> { }

	public class StringToLowercaseConverter : MvxNativeValueConverter<UI.Converters.StringToLowercaseConverter> { }
	public class StringToTitleCaseConverter : MvxNativeValueConverter<UI.Converters.StringToTitleCaseConverter> { }
	public class StringToUppercaseConverter : MvxNativeValueConverter<UI.Converters.StringToUppercaseConverter> { }

	public class StringFormatConverter : MvxNativeValueConverter<UI.Converters.StringFormatConverter> { }
	public class StringFormatUppercaseConverter : MvxNativeValueConverter<UI.Converters.StringFormatUppercaseConverter> { }
    public class SubstringConverter : MvxNativeValueConverter<UI.Converters.SubstringConverter> { }

	public class TimeSpanToShortStringConverter : MvxNativeValueConverter<UI.Converters.TimeSpanToShortStringConverter> { }
	public class TimeSpanToLengthConverter : MvxNativeValueConverter<UI.Converters.TimeSpanToLengthConverter> { }
	public class TimeSpanToDateTimeConverter : MvxNativeValueConverter<UI.Converters.TimeSpanToDateTimeConverter> { }

    public class RelativeTimeConverter : MvxNativeValueConverter<UI.Converters.RelativeTimeConverter> { }
    public class RelativeDateConverter : MvxNativeValueConverter<UI.Converters.RelativeDateConverter> { }

	public class ClassOccurrenceConverter : MvxNativeValueConverter<UI.Converters.ClassOccurrenceConverter> { }

	public class SubjectColorToColorObjectConverter : MvxNativeValueConverter<UI.Converters.SubjectColorToColorObjectConverter> { }
    public class SubjectColorToValueConverter : MvxNativeValueConverter<UI.Converters.SubjectColorToValueConverter> { }

    public class SetFlagCountVisibilityConverter : MvxNativeValueConverter<UI.Converters.SetFlagCountVisibilityConverter> { }
}
