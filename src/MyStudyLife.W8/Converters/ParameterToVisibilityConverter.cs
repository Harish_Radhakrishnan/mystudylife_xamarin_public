﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Converters {
    public class ParameterToVisibilityConverter : IValueConverter {
        /// <summary>
        /// Returns Visible if the value equals the parameter, collapsed otherwise.
        /// </summary>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, string language) {
            return value == null ? Visibility.Collapsed : (value.ToString() == parameter.ToString()).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}
