﻿using System;
using Windows.UI.Xaml.Data;

namespace MyStudyLife.W8.Converters {
	/// <summary>
	///		Value converter an <see cref="Enum"/> to an <see cref="Int32"/> and visa versa..
	/// </summary>
	public sealed class EnumToIntConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, string language) {
			return (int)value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, string language) {
			return Enum.Parse(targetType, value.ToString(), true);
		}
	}
}