﻿using MyStudyLife.Versioning.Shared;
using System;

namespace MyStudyLife.W8.Versioning {
    public class V405ToV406 : SchoolLogoUpgrade {
        public override Version FromVersion => new Version(4, 0, 5);

        public override Version ToVersion => new Version(4, 0, 6);
    }
}
