﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using MyStudyLife.Data;
using MyStudyLife.Data.Store;
using MyStudyLife.Versioning;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.W8.Versioning {
    public class V2ToV201 : IUpgrade{
        public Version FromVersion {
            get {
                return new Version(2,0);
            }
        }

        public Version ToVersion {
            get {
                return new Version(2, 0, 1);
            }
        }

        /// <summary>
        ///		We didn't implement foreign key "actions" in V2.0.0
        ///     of the app (Windows Phone update wasn't simultaneous).
        /// 
        ///     Because SQLite is unable to alter the structure
        ///     of the tables it has to be recreated like below.
        /// </summary>
        public Task<bool> DoUpgradeAsync() {
            var dataStore = Mvx.Resolve<IDataStore>();

			var conn = dataStore.GetConnection();
            return System.Threading.Tasks.Task.Run(() => {
                using (conn.Lock()) {
                    conn.BeginTransaction();

                    SafeMakeOldTable(conn, "AcademicSchedules");
                    SafeMakeOldTable(conn, "Subjects");
                    SafeMakeOldTable(conn, "Classes");
                    SafeMakeOldTable(conn, "Tasks");
                    SafeMakeOldTable(conn, "Exams");
                    
                    conn.CreateTable<AcademicSchedule>();
                    conn.CreateTable<Subject>();
                    conn.CreateTable<JsonToSQLite.V4Class>();
                    conn.CreateTable<Exam>();
                    conn.CreateTable<Data.Task>();

                    int schedulesMoved = TransferDataFromOldTable(conn, "AcademicSchedules");
                    int subjectsMoved = TransferDataFromOldTable(conn, "Subjects");
                    int classesMoved = TransferDataFromOldTable(conn, "Classes");
                    int examsMoved = TransferDataFromOldTable(conn, "Exams");
                    int tasksMoved = TransferDataFromOldTable(conn, "Tasks");

                    Debug.WriteLine(
                        "{0} schedules, {1} subjects, {2} classes, {3} tasks and {4} exams updated during SQLite structure change. Dropping old tables.",
                        schedulesMoved, subjectsMoved, classesMoved, tasksMoved, examsMoved);

                    conn.Execute("DROP TABLE [Tasks_Old]");
                    conn.Execute("DROP TABLE [Exams_Old]");
                    conn.Execute("DROP TABLE [Classes_Old]");
                    conn.Execute("DROP TABLE [Subjects_Old]");
                    conn.Execute("DROP TABLE [AcademicSchedules_Old]");

                    conn.Commit();
                }

                return true;
            });
        }

        private void SafeMakeOldTable(ISQLiteConnection conn, string tableName) {
            conn.Execute("DROP TABLE IF EXISTS [" + tableName + "_Old]");
            conn.Execute("ALTER TABLE [" + tableName + "] RENAME TO [" + tableName + "_Old]");
        }

        private int TransferDataFromOldTable(ISQLiteConnection conn, string tableName) {
            var oldTableInfo = conn.GetTableInfo(tableName + "_Old");
            var newTableInfo = conn.GetTableInfo(tableName);

            var sharedColumns = newTableInfo.Where(x => oldTableInfo.Any(y => y.Name == x.Name));

            var sqlColumnText = String.Join(", ", sharedColumns.Select(x => String.Concat("[", x.Name, "]")));

            return conn.Execute(
                "INSERT INTO [" + tableName + "] (" + sqlColumnText + ") SELECT " + sqlColumnText + " FROM [" + tableName + "_Old]" 
            );
        }

        public class AcademicSchedule {
            public Guid Guid { get; set; }
            
            public long UserId { get; set; }

            public DateTime Created { get; set; }

            public DateTime? Updated { get; set; }

            public DateTime? Deleted { get; set; }

            [Column("_needsSyncing")]
            public bool NeedsSyncing { get; set; }

            public string Revision { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            [Column("ParentGuid"), ForeignKey(typeof(AcademicSchedule), OnDeleteAction = OnDeleteAction.Cascade)]
            public Guid? YearGuid { get; set; }

            [MaxLength(50)]
            public string Name { get; set; }
        }
    }
}
