﻿using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.W8.Versioning {
    public sealed class V201ToV3 : TeacherSharingUpgrade {
        public override Version FromVersion {
            get { return new Version(2, 0, 1); }
        }

        public override Version ToVersion {
            get { return new Version(3, 0); }
        }
    }
}
