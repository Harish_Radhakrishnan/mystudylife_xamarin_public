﻿using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.W8.Versioning {
    public class V1ToV2 : JsonToSQLite {
        public override Version FromVersion {
            get {
                return new Version(1,0);
            }
        }

        public override Version ToVersion {
            get {
                return new Version(2,0);
            }
        }
    }
}
