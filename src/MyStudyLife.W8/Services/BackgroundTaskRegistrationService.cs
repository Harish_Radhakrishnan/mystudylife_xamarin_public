﻿using System;
using System.Diagnostics;
using System.Linq;
using Windows.ApplicationModel.Background;

namespace MyStudyLife.W8.Services {
    public static class BackgroundTaskRegistrationService {
        #region Constants

        /// <summary>
        /// The interval in which the live tile task should be triggered
        /// </summary>
        private const uint LIVE_TILE_TASK_RUN_INTERVAL = 30;

        /// <summary>
        /// The interval in which the reminders task should be triggered.
        /// Set to a quater of a day but schedules notifications for the whole day.
        /// This allows the task to fail etc.
        /// </summary>
        private const uint REMINDERS_TASK_RUN_INTERVAL = 360;

        #endregion

        #region Properties

        public static string LiveTileTaskName {
            get { return "Live Tile Task"; }
        }

        public static string RemindersTaskName {
            get { return "Reminders Task"; }
        }

        #endregion

        private static volatile bool _liveTileTaskChecked = false, _remindersTaskChecked = false;

        /// <summary>
        /// Ensures the live tile task is registered.
        /// Registers it if it is not, does nothing otherwise.
        /// </summary>
        public static async System.Threading.Tasks.Task EnsureLiveTileTaskRegistered() {
            if (_liveTileTaskChecked)
                return;

            try {
                BackgroundAccessStatus newStatus = await BackgroundExecutionManager.RequestAccessAsync();

                if (newStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity) {
                    bool isRegistered = BackgroundTaskRegistration.AllTasks.Any(t => t.Value.Name == LiveTileTaskName);

                    if (!isRegistered) {
                        BackgroundTaskBuilder builder = new BackgroundTaskBuilder {
                            Name = LiveTileTaskName,
                            TaskEntryPoint =
                                "MyStudyLife.W8.BackgroundTasks.LiveTileBackgroundTask"
                        };

                        builder.SetTrigger(new TimeTrigger(LIVE_TILE_TASK_RUN_INTERVAL, false));

                        builder.Register();
                    }
                }
            }
            catch // Required due to a bug in WinRT APIs
            {
                Debug.WriteLine("Exception caught whilst ensuring live tile task is registered.");
            }
            finally {
                _liveTileTaskChecked = true;
            }
        }

        /// <summary>
        /// Ensures the reminders task is registered.
        /// Registers it if it is not, does nothing otherwise.
        /// </summary>
        public static async System.Threading.Tasks.Task EnsureRemindersTaskRegistered() {
            if (_remindersTaskChecked)
                return;

            try {
                BackgroundAccessStatus newStatus = await BackgroundExecutionManager.RequestAccessAsync();

                if (newStatus == BackgroundAccessStatus.AllowedMayUseActiveRealTimeConnectivity) {
                    bool isRegistered = BackgroundTaskRegistration.AllTasks.Any(t => t.Value.Name == RemindersTaskName);

                    if (!isRegistered) {
                        BackgroundTaskBuilder builder = new BackgroundTaskBuilder {
                            Name = RemindersTaskName,
                            TaskEntryPoint =
                                "MyStudyLife.W8.BackgroundTasks.RemindersBackgroundTask"
                        };

                        builder.SetTrigger(new TimeTrigger(REMINDERS_TASK_RUN_INTERVAL, false));

                        builder.Register();
                    }
                }
            }
            catch // Required due to a bug in WinRT APIs
            {
                Debug.WriteLine("Exception caught whilst ensuring reminders task is registered.");
            }
            finally {
                _remindersTaskChecked = true;
            }
        }

        /// <summary>
        /// Ensures the background task with the given name is not registered.
        /// Unregisters if it is not, does nothing otherwise.
        /// </summary>
        /// <param name="taskName">The name of the task.</param>
        public static void EnsureNotRegistered(string taskName) {
            try {
                var possibleTasks = BackgroundTaskRegistration.AllTasks.Where(t => t.Value.Name == taskName).ToList();

                if (possibleTasks.Any()) {
                    possibleTasks.First().Value.Unregister(false);
                }
            }
            catch { } // Required due to a bug in WinRT APIs
        }
    }
}
