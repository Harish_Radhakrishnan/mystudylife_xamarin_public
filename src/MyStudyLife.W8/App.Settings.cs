﻿using System;
using System.Diagnostics;
using Windows.System;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsCommon.Views;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.W8.Controls.Settings;
using Windows.ApplicationModel.Activation;
using Windows.UI.ApplicationSettings;
using Windows.ApplicationModel.Search;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using MslSettingsPane = MyStudyLife.W8.Controls.Settings.SettingsPane;
using SettingsPane = Windows.UI.ApplicationSettings.SettingsPane;

namespace MyStudyLife.W8 {
    public sealed partial class App {
        private enum SettingsFlyoutWidth {
            Wide = 646,
            Narrow = 323
        }

        private const SettingsFlyoutWidth SettingsPopupWidth = SettingsFlyoutWidth.Wide;
        private const SettingsFlyoutWidth AccountsPopupWidth = SettingsFlyoutWidth.Narrow;
        private const SettingsFlyoutWidth AboutPopupWidth = SettingsFlyoutWidth.Wide;

        private void RegisterAppSettings() {
            SettingsPane.GetForCurrentView().CommandsRequested += App_CommandsRequested;
        }

        #region Settings

        private void App_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs e) {

			// Guids below to fix http://connect.microsoft.com/VisualStudio/feedback/details/797900/invalidcastexception-when-creating-new-settingscommand-instance-in-vs-2013

            var authService = Mvx.Resolve<IAuthorizationService>();

            e.Request.ApplicationCommands.Add(new SettingsCommand(new Guid("864B8862-78F8-4BDE-ACE9-0CDC8A949F54"), "Settings", (x) => {
                if (!authService.IsAuthorized)
                    return;

                Mvx.Resolve<INavigationService>().NavigateTo<SettingsViewModel>();
            }));

			e.Request.ApplicationCommands.Add(new SettingsCommand(new Guid("4FDB5ADE-98E5-44E4-BA99-1BB5203C0C07"), "Account", (x) => {
                if (!authService.IsAuthorized)
                    return;

                CreateFlyout(AccountsPopupWidth, "Account", new AccountPane());
            }));

            e.Request.ApplicationCommands.Add(new SettingsCommand(new Guid("6346733B-CD52-4234-B9E6-21BB1A7D5B08"), "Help + Feedback", async (x) =>
                await Launcher.LaunchUriAsync(new Uri(Mvx.Resolve<IMslConfig>().FeedbackUri))
            ));

            e.Request.ApplicationCommands.Add(new SettingsCommand(new Guid("ED99B01E-B2E4-4502-94E8-3C69D0E5A7E1"), "About", (x) =>
                CreateFlyout(AboutPopupWidth, x.Label, new AboutPane()))
            );
        }

        private SettingsFlyout CreateFlyout(SettingsFlyoutWidth width, string defaultTitle, UserControl content, bool showIndependent = false) {
            var bmp = new BitmapImage(new Uri("ms-appx:///Assets/SmallLogo.png"));

            var flyout = new SettingsFlyout {
                Width = (double)width > Window.Current.Bounds.Width ? Window.Current.Bounds.Width : (double)width,
                HeaderBackground = (SolidColorBrush)Current.Resources["BrushAccent"],
                Title = defaultTitle,
                Content = content,
                IconSource = bmp,
                HorizontalContentAlignment = HorizontalAlignment.Stretch,
                VerticalContentAlignment = VerticalAlignment.Stretch
            };

            if (showIndependent) {
                flyout.ShowIndependent();
            }
            else {
                flyout.Show();
            }

            // This may not be pretty - but then again none of the settings stuff in W8 is!
            // Essentially we know the controls will have a view model at this point as
            // they're passed in the ctor, rather than waiting to close when they may be null #232.
            var viewModelType = content.DataContext.GetType();

            ((Popup)flyout.Parent).Closed += (s, o) => {
                var backStack = Mvx.Resolve<INavigationService>().BackStack;

                backStack.Pop(viewModelType);
            };

            return flyout;
        }

        private void AttachClosedListener(Popup popup) {

        }

        internal void HandleSettingsRequest(MvxViewModelRequest request) {
            var viewModel = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(request, null);

            var settingsViewModel = viewModel as SettingsViewModel;

            if (settingsViewModel != null) {
                CreateFlyout(
                    SettingsFlyoutWidth.Narrow,
                    "Settings",
                    new MslSettingsPane(settingsViewModel)
                );

                return;
            }

            SettingsFlyout flyout = null;

            var reminderViewModel = viewModel as ReminderSettingsViewModel;

            if (reminderViewModel != null) {
                flyout = CreateFlyout(
                    SettingsFlyoutWidth.Wide,
                    "Reminders",
                    new ReminderSettingsPane(reminderViewModel), true
                );
            }
            else {
                var syncViewModel = viewModel as SyncSettingsViewModel;

                if (syncViewModel != null) {
                    flyout = CreateFlyout(
                        SettingsFlyoutWidth.Wide,
                        "Sync",
                        new SyncSettingsPane(syncViewModel), true
                    );
                }
                else {
                    var timetableViewModel = viewModel as TimetableSettingsViewModel;

                    if (timetableViewModel != null) {
                        flyout = CreateFlyout(
                            SettingsFlyoutWidth.Wide,
                            "Timetable",
                            new TimetableSettingsPane(timetableViewModel), true
                        );
                    }
                    else {
                        var generalViewModel = viewModel as GeneralSettingsViewModel;

                        if (generalViewModel != null) {
                            flyout = CreateFlyout(
                                SettingsFlyoutWidth.Wide,
                                "General",
                                new GeneralSettingsPane(generalViewModel), true
                            );
                        }
                    }
                }
            }

            if (flyout == null) {
                throw new NotImplementedException("Unknown settings viewmodel '" + request.ViewModelType.Name + "'");
            }

            flyout.BackClick += (s, e) => Mvx.Resolve<INavigationService>().NavigateTo<SettingsViewModel>();
        }
        
        public void ShowAccountPane() {
            CreateFlyout(AccountsPopupWidth, "Account", new AccountPane(), true);
        }

        #endregion

        #region Search

        protected override void OnSearchActivated(SearchActivatedEventArgs e) {
            EnsureMainPageActivatedAsync(e);
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs e) {
            try {
                SearchPane.GetForCurrentView().QuerySubmitted += OnQuerySubmitted;
            }
            catch (Exception ex) {
                Debug.WriteLine("Failed to register search event: " + ex);
#if RELEASE
                new MyStudyLife.W8.Platform.W8BugReporter().Send(ex);
#endif
            }
        }

        private void OnQuerySubmitted(object sender, SearchPaneQuerySubmittedEventArgs e) {
            string queryText = e.QueryText;

            if (String.IsNullOrWhiteSpace(queryText)) return;

            var page = ((MvxWindowsPage)AppShell.Current.ViewFrame.Content);

            if (page != null && page.DataContext is ISearchable) {
                ((ISearchable)page.DataContext).ProcessQueryText(queryText);
            }
            else {
                Mvx.Resolve<INavigationService>().NavigateTo<SearchViewModel>(new {
                    queryText
                });
            }
        }

        #endregion
    }
}
