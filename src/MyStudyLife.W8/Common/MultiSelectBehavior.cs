﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using MyStudyLife.W8.Behaviors;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common {
    //[System.Diagnostics.DebuggerNonUserCode]
    public class MultiSelectBehavior : Behavior<ListViewBase> {

        #region SelectedItems Attached Property

        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.Register(
            "SelectedItems",
            typeof(object),
            typeof(MultiSelectBehavior),
            new PropertyMetadata(new ObservableCollection<object>(), PropertyChangedCallback));

        #endregion

        private volatile bool _selectionChangedInProgress; // Flag to avoid infinite loop if same viewmodel is shared by multiple controls

        public MultiSelectBehavior() {
            SelectedItems = new ObservableCollection<object>();
        }

        public ObservableCollection<object> SelectedItems {
            get { return (ObservableCollection<object>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        protected override void OnAttached() {
            AssociatedObject.SelectionChanged += OnSelectionChanged;
        }

        protected override void OnDetached() {
            AssociatedObject.SelectionChanged -= OnSelectionChanged;
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var b = (MultiSelectBehavior) d;

            if (e.OldValue is ObservableCollection<object>) {
                (e.OldValue as ObservableCollection<object>).CollectionChanged -= b.SelectedItemsChanged;
            }

            if (e.NewValue is ObservableCollection<object>) {
                (e.NewValue as ObservableCollection<object>).CollectionChanged += b.SelectedItemsChanged;
            }
        }

        private void SelectedItemsChanged(object sender, NotifyCollectionChangedEventArgs e) {
            var listViewBase = this.AssociatedObject;

            var listSelectedItems = listViewBase.SelectedItems;
            
            if (e.Action == NotifyCollectionChangedAction.Reset) {
                listSelectedItems.Clear();
            }
            else {
                if (e.OldItems != null) {
                    foreach (var item in e.OldItems) {
                        listSelectedItems.Remove(item);
                    }
                }

                if (e.NewItems != null) {
                    foreach (var item in e.NewItems) {
                        if (!listSelectedItems.Contains(item)) {
                            listSelectedItems.Add(item);
                        }
                    }
                }
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (_selectionChangedInProgress)
                return;

            _selectionChangedInProgress = true;

            foreach (var item in e.RemovedItems.Where(item => SelectedItems.Contains(item))) {
                SelectedItems.Remove(item);
            }

            foreach (var item in e.AddedItems.Where(item => !SelectedItems.Contains(item))) {
                SelectedItems.Add(item);
            }
            _selectionChangedInProgress = false;
        }
    }

}
