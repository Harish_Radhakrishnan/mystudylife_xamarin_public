﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Cirrious.CrossCore.WeakSubscription;
using MyStudyLife.UI;
using MyStudyLife.W8.Behaviors;

namespace MyStudyLife.W8.Common {
    public class ItemsSourceWithNullBehavior : Behavior<ItemsControl> {
        #region DependencyProperties

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            "ItemsSource", typeof(IEnumerable), typeof(ItemsSourceWithNullBehavior), new PropertyMetadata(null, ItemsSourceOnChanged));

        public static readonly DependencyProperty WithNullProperty = DependencyProperty.Register(
            "WithNull", typeof(bool), typeof(ItemsSourceWithNullBehavior), new PropertyMetadata(true, WithNullOnChanged));

        public IEnumerable ItemsSource {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public bool WithNull {
            get { return (bool)GetValue(WithNullProperty); }
            set { SetValue(WithNullProperty, value); }
        }

        private static void ItemsSourceOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((ItemsSourceWithNullBehavior)d).ItemsSourceOnChanged();
        }

        private static void WithNullOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((ItemsSourceWithNullBehavior)d).WithNullOnChanged();
        }

        #endregion

        private IDisposable _subscription;

        protected override void OnAttached() {
            this.SetItemsSource();
        }

        protected override void OnDetached() {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }
        }

        private void ItemsSourceOnChanged() {
            if (_subscription != null) {
                _subscription.Dispose();
                _subscription = null;
            }

            var itemsSource = this.ItemsSource;

            if (itemsSource == null) {
                if (this.AssociatedObject != null) {
                    this.AssociatedObject.ItemsSource = null;
                }

                return;
            }

            var observableCollection = itemsSource as INotifyCollectionChanged;

            if (observableCollection != null) {
                _subscription = observableCollection.WeakSubscribe(ItemsSourceOnCollectionChanged);
            }

            this.SetItemsSource();
        }

        private void WithNullOnChanged() {
            if (this.AssociatedObject == null) {
                return;
            }

            var existingItemsSource = this.AssociatedObject.ItemsSource as IList;

            if (existingItemsSource == null || (existingItemsSource.Count > 0 && existingItemsSource[0] is ListItemsSourcePlaceholder)) {
                return;
            }

            existingItemsSource.Insert(0, ListItemsSourcePlaceholder.Null);
        }

        private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            var existingItemsSource = this.AssociatedObject.ItemsSource as IList;

            if (existingItemsSource == null || e.Action == NotifyCollectionChangedAction.Reset) {
                this.SetItemsSource();
                return;
            }

            if (e.Action == NotifyCollectionChangedAction.Replace || e.Action == NotifyCollectionChangedAction.Remove) {
                foreach (var item in e.OldItems) {
                    existingItemsSource.Remove(item);
                }
            }

            if (e.Action == NotifyCollectionChangedAction.Remove) {
                return;
            }

            var adjustedNewStartingIndex = e.NewStartingIndex + (this.WithNull ? 1 : 0);

            for (int i = 0; i < e.NewItems.Count; i++) {
                existingItemsSource.Insert(adjustedNewStartingIndex + i, e.NewItems[i]);
            }
        }

        private void SetItemsSource() {
            if (this.AssociatedObject == null) {
                return;
            }

            if (this.ItemsSource == null || !this.WithNull) {
                this.AssociatedObject.ItemsSource = this.ItemsSource;
            }
            else {
                var nItemsSource = this.ItemsSource.Cast<object>().ToObservableCollection();

                nItemsSource.Insert(0, ListItemsSourcePlaceholder.Null);

                this.AssociatedObject.ItemsSource = nItemsSource;
            }
        }
    }

    public class NullSelectedItemConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            return value == null ? ListItemsSourcePlaceholder.Null : null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            return value is ListItemsSourcePlaceholder ? null : value;
        }
    }

    public class ListItemsSourcePlaceholder {
        private static ListItemsSourcePlaceholder _null;

        public static ListItemsSourcePlaceholder Null {
            get { return _null ?? (_null = new ListItemsSourcePlaceholder()); }
        }

        private ListItemsSourcePlaceholder() { }
    }
}
