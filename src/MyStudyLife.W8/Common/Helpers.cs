﻿using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace MyStudyLife.W8.Common {
    public static class Helpers {
        public static LayoutAwarePage GetPage(this DependencyObject control) {
            var tmp = VisualTreeHelper.GetParent(control);

            while (!(tmp is Page))
                tmp = VisualTreeHelper.GetParent(tmp);

            LayoutAwarePage page = tmp as LayoutAwarePage;

            if (page == null)
                throw new NotSupportedException("This user control must be a child of a page which derives from LayoutAwarePage");

            return page;
        }

        public static T GetVisualParent<T>(this DependencyObject child) where T : UIElement {
            while ((child != null) && !(child is T)) {
                child = VisualTreeHelper.GetParent(child);
            }
            return child as T;
        }

        public static T GetVisualChildByName<T>(this DependencyObject parent, string name) where T : DependencyObject {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++) {
                var child = VisualTreeHelper.GetChild(parent, i);
                string controlName = child.GetValue(FrameworkElement.NameProperty) as string;
                if (controlName == name) {
                    return child as T;
                }
                T result = GetVisualChildByName<T>(child, name);
                if (result != null)
                    return result;
            }
            return null;
        }

        public static IEnumerable<T> FindVisualChildren<T>(this DependencyObject depObj) where T : DependencyObject {
            if (depObj != null) {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++) {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T) {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child)) {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        ///     Converts the given <paramref name="condition"/> to
        ///     <code>Visibility.Visible</code> if true, <code>Visibility.Collapsed</code>
        ///     if not.
        /// </summary>
        public static Visibility ToVisibility(this bool condition) {
            return (condition) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
