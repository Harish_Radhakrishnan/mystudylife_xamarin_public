﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyStudyLife.Scheduling;
using MyStudyLife.W8.Views;

namespace MyStudyLife.W8.Common {
    public sealed class AgendaEntryTemplateSelector : DataTemplateSelector {

        #region Overrides of DataTemplateSelector

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container) {
            DashboardView dashboard = DashboardView.GetInstance();

            if (dashboard == null) {
                throw new InvalidOperationException("AgendaEntryTemplateSelector can only be used on the dashboard view.");
            }

            if (item != null) {
                var ae = (AgendaEntry)item;

                if (ae.IsPast) {
                    return (DataTemplate) dashboard.Resources["AgendaEntryPastItemTemplate"];
                }

                if (ae.IsCurrent) {
                    return (DataTemplate) dashboard.Resources["AgendaEntryCurrentItemTemplate"];
                }
            }

            return (DataTemplate)dashboard.Resources["AgendaEntryNextItemTemplate"];
        }

        #endregion
    }
}
