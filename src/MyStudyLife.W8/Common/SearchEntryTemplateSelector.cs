﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyStudyLife.Data;

namespace MyStudyLife.W8.Common {
    public class SearchEntryTemplateSelector : DataTemplateSelector {
        static DataTemplate _classTemplate, _taskTemplate, _examTemplate;

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container) {
            _classTemplate = App.Current.Resources["ItemsListViewClassTemplate"] as DataTemplate;
            _taskTemplate = App.Current.Resources["ItemsListViewTaskTemplate"] as DataTemplate;
            _examTemplate = App.Current.Resources["ItemsListViewExamTemplate"] as DataTemplate;

            Type itemType = item.GetType();

            if (itemType == typeof(Class))
                return _classTemplate;
            if (itemType == typeof(Task))
                return _taskTemplate;
            if (itemType == typeof(Exam))
                return _examTemplate;

            throw new Exception("itemType must be either Class, Task or Exam");
        }
    }
}
