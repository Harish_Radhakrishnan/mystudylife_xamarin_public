﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common {
    public static class FlagsCheckBox {
        public static readonly DependencyProperty CheckedIfFlagProperty = DependencyProperty.RegisterAttached(
            "CheckedIfFlag",
            typeof(int),
            typeof(FlagsCheckBox),
            new PropertyMetadata(false, CheckedIfFlagOnChange)
        );

        public static int GetCheckedIfFlag(DependencyObject d) {
            object val = d.GetValue(CheckedIfFlagProperty);

            // Fixes strange bug where it's sometimes a boolean?
            return val is int ? (int) val : 0;
        }

        public static void SetCheckedIfFlag(DependencyObject d, int checkedIfFlag) {
            d.SetValue(CheckedIfFlagProperty, checkedIfFlag);
        }

        public static readonly DependencyProperty FlagsProperty = DependencyProperty.RegisterAttached(
            "Flags",
            typeof(int),
            typeof(FlagsCheckBox),
            new PropertyMetadata(false, FlagsOnChange)
        );

        public static int GetFlags(DependencyObject d) {
            object val = d.GetValue(FlagsProperty);

            // Fixes strange bug where it's sometimes a boolean?
            return val is int ? (int)val : 0;
        }

        public static void SetFlags(DependencyObject d, int flags) {
            d.SetValue(FlagsProperty, flags);
        }

        // Should only be set once
        private static void CheckedIfFlagOnChange(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var chk = ((CheckBox) d);

            chk.Checked += ChkOnChecked;
            chk.Unchecked += ChkOnUnchecked;

            int flags = GetFlags(d);
            int flag = GetCheckedIfFlag(d);

            chk.IsChecked = flags > 0 && flag > 0 && (flags & flag) == flag;
        }

        private static void FlagsOnChange(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var chk = ((CheckBox) d);

            int flags = GetFlags(d);
            int flag = GetCheckedIfFlag(d);

            chk.IsChecked = flags > 0 && flag > 0 && (flags & flag) == flag;
        }

        private static void ChkOnChecked(object sender, RoutedEventArgs e) {
            DependencyObject d = sender as DependencyObject;

            SetFlags(d, GetFlags(d) | GetCheckedIfFlag(d));
        }
        private static void ChkOnUnchecked(object sender, RoutedEventArgs e) {
            DependencyObject d = sender as DependencyObject;

            SetFlags(d, GetFlags(d) & ~GetCheckedIfFlag(d));
        }
    }
}
