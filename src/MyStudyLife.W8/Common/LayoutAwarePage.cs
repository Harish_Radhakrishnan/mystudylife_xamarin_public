﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.ApplicationModel;
using Windows.Foundation.Metadata;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media.Animation;
using Cirrious.MvvmCross.WindowsCommon.Views;

namespace MyStudyLife.W8.Common {
    /// <summary>
    /// Typical implementation of Page that provides several important conveniences:
    /// <list type="bullet">
    /// <item>
    /// <description>Application view state to visual state mapping</description>
    /// </item>
    /// <item>
    /// <description>GoBack, GoForward, and GoHome event handlers</description>
    /// </item>
    /// <item>
    /// <description>Mouse and keyboard shortcuts for navigation</description>
    /// </item>
    /// <item>
    /// <description>State management for navigation and process lifetime management</description>
    /// </item>
    /// <item>
    /// <description>A default view model</description>
    /// </item>
    /// </list>
    /// </summary>
    [WebHostHidden]
    public class LayoutAwarePage : MvxWindowsPage {
        private double _previousWidth = 0;

        private List<Control> _layoutAwareControls;

        /// <summary>
        /// Initializes a new instance of the <see cref="LayoutAwarePage"/> class.
        /// </summary>
        public LayoutAwarePage() {
            //this.NavigationCacheMode = NavigationCacheMode.Enabled;

            if (DesignMode.DesignModeEnabled) return;

            // When this page is part of the visual tree make two changes:
            // 1) Map application view state to visual state for the page
            // 2) Handle keyboard and mouse navigation requests
            this.Loaded += (sender, e) => {
                this.StartLayoutUpdates(sender, e);

                // Keyboard and mouse navigation only apply when occupying the entire window
                if (this.ActualHeight == Window.Current.Bounds.Height &&
                    this.ActualWidth == Window.Current.Bounds.Width) {
                    // Listen to the window directly so focus isn't required
                    Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated +=
                        CoreDispatcher_AcceleratorKeyActivated;
                    Window.Current.CoreWindow.PointerPressed +=
                        this.CoreWindow_PointerPressed;
                }
            };

            // Undo the same changes when the page is no longer visible as best practice to avoid 
            // prolonging object lifetime or leaking
            this.Unloaded += (sender, e) => {
                this.StopLayoutUpdates(sender, e);
                Window.Current.CoreWindow.Dispatcher.AcceleratorKeyActivated -=
                    CoreDispatcher_AcceleratorKeyActivated;
                Window.Current.CoreWindow.PointerPressed -=
                    this.CoreWindow_PointerPressed;
            };
        }

        #region Navigation support

        /// <summary>
        /// Invoked as an event handler to navigate backward in the page's associated
        /// <see cref="Frame"/> until it reaches the top of the navigation stack.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="e">Event data describing the conditions that led to the event.</param>
        internal virtual void GoHome(object sender, RoutedEventArgs e) {
            // Use the navigation frame to return to the topmost page
            if (Frame != null) {
                while (Frame.CanGoBack) Frame.GoBack();
            }
        }

        /// <summary>
        /// Invoked as an event handler to navigate backward in the navigation stack
        /// associated with this page's <see cref="Frame"/>.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="e">Event data describing the conditions that led to the
        /// event.</param>
        internal virtual void GoBack(object sender, RoutedEventArgs e) {
            // Use the navigation frame to return to the previous page
            if (Frame != null && Frame.CanGoBack) Frame.GoBack();
        }

        /// <summary>
        /// Invoked as an event handler to navigate forward in the navigation stack
        /// associated with this page's <see cref="Frame"/>.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="e">Event data describing the conditions that led to the
        /// event.</param>
        internal virtual void GoForward(object sender, RoutedEventArgs e) {
            // Use the navigation frame to move to the next page
            if (Frame != null && Frame.CanGoForward) Frame.GoForward();
        }

        /// <summary>
        /// Invoked on every keystroke, including system keys such as Alt key combinations, when
        /// this page is active and occupies the entire window.  Used to detect keyboard navigation
        /// between pages even when the page itself doesn't have focus.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="args">Event data describing the conditions that led to the event.</param>
        private void CoreDispatcher_AcceleratorKeyActivated(CoreDispatcher sender,
            AcceleratorKeyEventArgs args) {
            var virtualKey = args.VirtualKey;

            // Only investigate further when Left, Right, or the dedicated Previous or Next keys
            // are pressed
            if ((args.EventType == CoreAcceleratorKeyEventType.SystemKeyDown ||
                args.EventType == CoreAcceleratorKeyEventType.KeyDown) &&
                (virtualKey == VirtualKey.Left || virtualKey == VirtualKey.Right ||
                (int)virtualKey == 166 || (int)virtualKey == 167)) {
                var coreWindow = Window.Current.CoreWindow;
                var downState = CoreVirtualKeyStates.Down;
                bool menuKey = (coreWindow.GetKeyState(VirtualKey.Menu) & downState) == downState;
                bool controlKey = (coreWindow.GetKeyState(VirtualKey.Control) & downState) == downState;
                bool shiftKey = (coreWindow.GetKeyState(VirtualKey.Shift) & downState) == downState;
                bool noModifiers = !menuKey && !controlKey && !shiftKey;
                bool onlyAlt = menuKey && !controlKey && !shiftKey;

                if (((int)virtualKey == 166 && noModifiers) ||
                    (virtualKey == VirtualKey.Left && onlyAlt)) {
                    // When the previous key or Alt+Left are pressed navigate back
                    args.Handled = true;
                    this.GoBack(this, new RoutedEventArgs());
                }
                else if (((int)virtualKey == 167 && noModifiers) ||
                    (virtualKey == VirtualKey.Right && onlyAlt)) {
                    // When the next key or Alt+Right are pressed navigate forward
                    args.Handled = true;
                    this.GoForward(this, new RoutedEventArgs());
                }
            }
        }

        /// <summary>
        /// Invoked on every mouse click, touch screen tap, or equivalent interaction when this
        /// page is active and occupies the entire window.  Used to detect browser-style next and
        /// previous mouse button clicks to navigate between pages.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="args">Event data describing the conditions that led to the event.</param>
        private void CoreWindow_PointerPressed(CoreWindow sender,
            PointerEventArgs args) {
            var properties = args.CurrentPoint.Properties;

            // Ignore button chords with the left, right, and middle buttons
            if (properties.IsLeftButtonPressed || properties.IsRightButtonPressed ||
                properties.IsMiddleButtonPressed) return;

            // If back or foward are pressed (but not both) navigate appropriately
            bool backPressed = properties.IsXButton1Pressed;
            bool forwardPressed = properties.IsXButton2Pressed;
            if (backPressed ^ forwardPressed) {
                args.Handled = true;
                if (backPressed) this.GoBack(this, new RoutedEventArgs());
                if (forwardPressed) this.GoForward(this, new RoutedEventArgs());
            }
        }

        #endregion

        #region Visual state switching

        /// <summary>
        /// Invoked as an event handler, typically on the <see cref="FrameworkElement.Loaded"/>
        /// event of a <see cref="Control"/> within the page, to indicate that the sender should
        /// start receiving visual state management changes that correspond to application view
        /// state changes.
        /// </summary>
        /// <param name="sender">Instance of <see cref="Control"/> that supports visual state
        /// management corresponding to view states.</param>
        /// <param name="e">Event data that describes how the request was made.</param>
        /// <remarks>The current view state will immediately be used to set the corresponding
        /// visual state when layout updates are requested.  A corresponding
        /// <see cref="FrameworkElement.Unloaded"/> event handler connected to
        /// <see cref="StopLayoutUpdates"/> is strongly encouraged.  Instances of
        /// <see cref="LayoutAwarePage"/> automatically invoke these handlers in their Loaded and
        /// Unloaded EventAggregator.</remarks>
        /// <seealso cref="DetermineVisualState"/>
        /// <seealso cref="InvalidateVisualState"/>
        public void StartLayoutUpdates(object sender, RoutedEventArgs e) {
            var control = sender as Control;
            if (control == null) return;
            if (_layoutAwareControls == null) {
                // Start listening to view state changes when there are controls interested in updates
                Window.Current.SizeChanged += WindowSizeChanged;
                _layoutAwareControls = new List<Control>();
            }
            _layoutAwareControls.Add(control);

            // Set the initial visual state of the control
            ActivateStates(control);
        }

        private void WindowSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            InvalidateVisualState();
        }

        /// <summary>
        /// Invoked as an event handler, typically on the <see cref="FrameworkElement.Unloaded"/>
        /// event of a <see cref="Control"/>, to indicate that the sender should start receiving
        /// visual state management changes that correspond to application view state changes.
        /// </summary>
        /// <param name="sender">Instance of <see cref="Control"/> that supports visual state
        /// management corresponding to view states.</param>
        /// <param name="e">Event data that describes how the request was made.</param>
        /// <remarks>The current view state will immediately be used to set the corresponding
        /// visual state when layout updates are requested.</remarks>
        /// <seealso cref="StartLayoutUpdates"/>
        public void StopLayoutUpdates(object sender, RoutedEventArgs e) {
            var control = sender as Control;
            if (control == null || _layoutAwareControls == null) return;
            _layoutAwareControls.Remove(control);
            if (_layoutAwareControls.Count == 0) {
                // Stop listening to view state changes when no controls are interested in updates
                _layoutAwareControls = null;
                Window.Current.SizeChanged -= WindowSizeChanged;
            }
        }

        /// <summary>
        /// Updates all controls that are listening for visual state changes with the correct
        /// visual state.
        /// </summary>
        public void InvalidateVisualState() {
            if (_layoutAwareControls != null) {
                _layoutAwareControls.Apply(ActivateStates);
            }
        }

        protected virtual void ActivateStates(Control ctrl) {
// ReSharper disable CompareOfFloatsByEqualityOperator
            var width = Window.Current.Bounds.Width;
            
            const bool useTransitions = false;

            // Fixes a bug where when resized to be larger, the appropriate state
            // was not applied because it had already been applied...
            //
            // The below code will only reset the state when appropriate - ie. if
            // we move from 500 to 768 it won't bother resetting 500.
            if (_previousWidth > 0 && width > _previousWidth) {
                if (_previousWidth <= 320) {
                    VisualStateManager.GoToState(ctrl, "Below500Inactive", useTransitions);
                }

                if (_previousWidth <= 500) {
                    VisualStateManager.GoToState(ctrl, "Below768Inactive", useTransitions);
                }

                if (_previousWidth <= 768) {
                    VisualStateManager.GoToState(ctrl, "Below1024Inactive", useTransitions);
                }
            }

            _previousWidth = width;

            VisualStateManager.GoToState(ctrl, width <= 1024 ? "Below1024Active" : "Below1024Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 768 ? "Below768Active" : "Below768Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 500 ? "Below500Active" : "Below500Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 320 ? "Below320Active" : "Below320Inactive", useTransitions);
// ReSharper restore CompareOfFloatsByEqualityOperator
        }

        #endregion

        #region App Bar

        protected void StartAppBarLayoutUpdates(object sender, RoutedEventArgs e) {
            Panel root = ((AppBar)sender).Content as Panel;
            if (root != null) {
                foreach (var child in root.Children) {
                    Panel panel = child as Panel;

                    if (panel != null) {
                        foreach (UIElement pChild in panel.Children) {
                            StartLayoutUpdates(pChild, new RoutedEventArgs());
                        }
                    }
                    else {
                        StartLayoutUpdates(child, new RoutedEventArgs());
                    }
                }
            }
        }

        protected void StopAppBarLayoutUpdates(object sender, RoutedEventArgs e) {
            Panel root = ((AppBar)sender).Content as Panel;
            if (root != null) {
                foreach (var child in root.Children) {
                    Panel panel = child as Panel;

                    if (panel != null) {
                        foreach (UIElement pChild in panel.Children) {
                            StopLayoutUpdates(pChild, new RoutedEventArgs());
                        }
                    }
                    else {
                        StopLayoutUpdates(child, new RoutedEventArgs());
                    }
                }
            }
        }

        protected void ShowBottomAppBarOnClick(object sender, RoutedEventArgs e) {
            Debug.Assert(BottomAppBar != null);

            if (BottomAppBar != null) {
                BottomAppBar.IsOpen = true;
            }
        }

        protected void DismissAppBarOnTapped(object sender, TappedRoutedEventArgs e) {
            if (TopAppBar != null) {
                TopAppBar.IsOpen = false;
            }

            if (BottomAppBar != null) {
                BottomAppBar.IsOpen = false;
            }
        }

        #endregion
        
        protected virtual void FrameworkElement_Loaded(object sender, RoutedEventArgs e) {
            ((Storyboard)((FrameworkElement)sender).Resources["LoadedStoryboard"]).Begin();
        }
    }
}
