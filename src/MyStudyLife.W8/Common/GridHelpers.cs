﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common {
	public static class GridHelpers {
		#region Row Count

		public static readonly DependencyProperty RowCountProperty =
			DependencyProperty.RegisterAttached("RowCount", typeof (int), typeof (GridHelpers), new PropertyMetadata(default(int), OnRowCountChanged));

		public static int GetRowCount(DependencyObject obj) {
			return (int)obj.GetValue(RowCountProperty);
		}

		public static void SetRowCount(DependencyObject obj, int count) {
			obj.SetValue(RowCountProperty, count);
		}

		private static void OnRowCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e) {
			var rowCount = (int)e.NewValue;
			var rowDefinitions = ((Grid)obj).ColumnDefinitions;

			if (rowCount == rowDefinitions.Count) return;

			if (rowCount > rowDefinitions.Count) {
				for (int i = rowDefinitions.Count; i < rowCount; i++) {
					rowDefinitions.Add(new ColumnDefinition());
				}
			}
			else {
				for (int i = rowDefinitions.Count; i > rowCount; i--) {
                    rowDefinitions.RemoveAt(i - 1);
				}
			}
		}

		#endregion

		#region Column Count

		public static readonly DependencyProperty ColumnCountProperty =
			DependencyProperty.RegisterAttached("ColumnCount", typeof(int), typeof(GridHelpers), new PropertyMetadata(default(int), OnColumnCountChanged));

		public static int GetColumnCount(DependencyObject obj) {
			return (int)obj.GetValue(RowCountProperty);
		}

		public static void SetColumnCount(DependencyObject obj, int count) {
			obj.SetValue(RowCountProperty, count);
		}

		private static void OnColumnCountChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e) {
			var colCount = (int) e.NewValue;
			var colDefinitions = ((Grid)obj).ColumnDefinitions;

			if (colCount == colDefinitions.Count) return;

			if (colCount > colDefinitions.Count) {
				for (int i = colDefinitions.Count; i < colCount; i++) {
					colDefinitions.Add(new ColumnDefinition());
				}
			}
			else {
				for (int i = colDefinitions.Count; i > colCount; i--) {
                    colDefinitions.RemoveAt(i - 1);
				}
			}
		}

		#endregion
	}
}
