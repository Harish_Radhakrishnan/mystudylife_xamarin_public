﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common
{
    public class SelectedItemTemplateSelector : DataTemplateSelector
    {
        public static DataTemplate NormalTemplate { get; set; }
        public static DataTemplate SelectedTemplate { get; set; }

        public SelectedItemTemplateSelector()
        {
             
        }
        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (NormalTemplate == null)
                NormalTemplate = App.Current.Resources["MyStudyLifeItemsListViewTaskTemplate"] as DataTemplate;

            if (SelectedTemplate == null)
                SelectedTemplate = App.Current.Resources["MyStudyLifeItemsListViewTaskSelectedTemplate"] as DataTemplate;  

            ListBoxItem listBoxItem = container.GetVisualParent<ListBoxItem>();

            return listBoxItem.IsSelected ? SelectedTemplate : NormalTemplate;
        }
    }
}
