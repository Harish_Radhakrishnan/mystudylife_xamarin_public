﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyStudyLife.Data;

namespace MyStudyLife.W8.Common {
    public class TaskStateIndicator {
        public static void SetForTask(DependencyObject element, Task value) {
            element.SetValue(ForTaskProperty, value);
        }

        public static Task GetForTask(DependencyObject element) {
            return (Task)element.GetValue(ForTaskProperty);
        }

        public static readonly DependencyProperty ForTaskProperty = DependencyProperty.RegisterAttached(
            "ForTask", typeof(Task), typeof(TaskStateIndicator), new PropertyMetadata(null, OnTaskChanged));

        private static void OnTaskChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var textBlock = (TextBlock)d;
            var task = (Task)e.NewValue;

            if (textBlock == null) {
                return;
            }

            if (task == null) {
                textBlock.Visibility = Visibility.Collapsed;
                return;
            }

            if (task.IsOverdue || task.IsIncompleteAndDueSoon) {
                textBlock.Text = App.Current.MslResources.GlyphAttention;
                textBlock.Foreground = task.IsOverdue ? App.Current.MslResources.BrushAttention : App.Current.MslResources.BrushWarning;
                textBlock.Visibility = Visibility.Visible;
            }
            else if (task.IsComplete) {
                textBlock.Text = App.Current.MslResources.GlyphTick;
                textBlock.Foreground = App.Current.MslResources.BrushOkay;
                textBlock.Visibility = Visibility.Visible;
            }
            else {
                textBlock.Visibility = Visibility.Collapsed;
            }
        }
    }
}
