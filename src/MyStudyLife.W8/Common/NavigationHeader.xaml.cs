﻿using System;
using System.Reflection;
using Cirrious.CrossCore;
using MyStudyLife.UI;
using MyStudyLife.W8.Views;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace MyStudyLife.W8.Common {
    public sealed partial class NavigationHeader : UserControl {
        public static readonly DependencyProperty PageTitleProperty =
            DependencyProperty.Register("PageTitle", typeof(string), typeof(NavigationHeader), new PropertyMetadata(String.Empty, OnPageTitleChanged));
            
        public string PageTitle {
            get { return (string)GetValue(PageTitleProperty); }
            set { SetValue(PageTitleProperty, value); }
        }

        private static void OnPageTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            NavigationHeader ctrl = (NavigationHeader)d;

            ctrl.PageTitleTextBlock.Text = (string) (e.NewValue ?? String.Empty);
        }

        #region Selector

        public static readonly DependencyProperty SelectorItemTemplateProperty =
            DependencyProperty.Register("SelectorItemTemplate", typeof(DataTemplate), typeof(NavigationHeader), new PropertyMetadata(null));

        public DataTemplate SelectorItemTemplate {
            get { return (DataTemplate)GetValue(SelectorItemTemplateProperty); }
            set { SetValue(SelectorItemTemplateProperty, value); }
        }

        public static readonly DependencyProperty SelectorItemsSourceProperty =
            DependencyProperty.Register("SelectorItemsSource", typeof(object), typeof(NavigationHeader), new PropertyMetadata(null, OnSelectorItemsSourceChanged));

        private static void OnSelectorItemsSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            ((NavigationHeader) sender).Selector.Visibility =
                e.NewValue == null ? Visibility.Collapsed : Visibility.Visible;
        }

        public object SelectorItemsSource {
            get { return GetValue(SelectorItemsSourceProperty); }
            set { SetValue(SelectorItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty SelectorSelectedItemProperty =
            DependencyProperty.Register("SelectorSelectedItem", typeof(object), typeof(NavigationHeader), new PropertyMetadata(null));

        public object SelectorSelectedItem {
            get { return GetValue(SelectorSelectedItemProperty); }
            set { SetValue(SelectorSelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectorItemPropertyProperty =
            DependencyProperty.Register("SelectorItemProperty", typeof(string), typeof(NavigationHeader), new PropertyMetadata(null));

        /// <summary>
        ///     The name of the property to display / check on
        ///     the selected item.
        /// </summary>
        public string SelectorItemProperty {
            get { return (string)GetValue(SelectorItemPropertyProperty); }
            set { SetValue(SelectorItemPropertyProperty, value); }
        }

        #endregion

        public NavigationHeader() {
            this.InitializeComponent();

            this.Root.DataContext = this;

            this.Loaded += (s, e) => {
                this.Selector.Visibility = SelectorItemsSource == null ? Visibility.Collapsed : Visibility.Visible;
            };

            this.Selector.SelectionChanged += SelectorOnSelectionChanged;
        }

        private void SelectorOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var selectedItem = this.Selector.SelectedItem;

            if (selectedItem != null) {
                var prop = selectedItem.GetType().GetRuntimeProperty(SelectorItemProperty);

                var value = prop.GetValue(selectedItem);

                this.PageTitleTextBlock.Text = value == null ? this.PageTitle : String.Concat(value.ToString().ToTitleCase(), " ", this.PageTitle);
            }
        }

        private void GridOnTapped(object sender, TappedRoutedEventArgs e) {
            if (this.SelectorItemsSource == null) return;

            this.Selector.IsDropDownOpen = !this.Selector.IsDropDownOpen;
        }

        private void BackButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            AppShell.Current.ViewFrame.GoBack();
        }

        private void GridShowHighlight(object sender, PointerRoutedEventArgs e) {
            if (this.SelectorItemsSource == null) return;

            this.Highlight.Opacity = 1;
        }

        private void GridHideHighlight(object sender, PointerRoutedEventArgs e) {
            if (this.SelectorItemsSource == null) return;

            this.Highlight.Opacity = 0;
        }
    }
}
