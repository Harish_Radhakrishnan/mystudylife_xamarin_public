﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common {
    /// <summary>
    ///     A content control which changes it's <see cref="ReactiveItemContentControl.ContentTemplate"/>
    ///     using a <see cref="ReactiveItemContentControl.ContentTemplateSelector"/> when the
    ///     <see cref="ReactiveItemContentControl.WhenChanged"/> binding is changed.
    /// </summary>
    public sealed class ReactiveItemContentControl : ContentControl {
        public static readonly DependencyProperty WhenChangedProperty = DependencyProperty.Register(
            "WhenChanged", typeof(object), typeof(ReactiveItemContentControl), new PropertyMetadata(-1, WhenChangedOnChanged));

        public object WhenChanged {
            get { return GetValue(WhenChangedProperty); }
            set { SetValue(WhenChangedProperty, value); }
        }

        public ReactiveItemContentControl() {
            this.SetTemplate();

            this.DataContextChanged += OnDataContextChanged;
        }

        private static void WhenChangedOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((ReactiveItemContentControl)d).SetTemplate();
        }

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs e) {
            this.SetTemplate();
        }

        private void SetTemplate() {
            if (this.ContentTemplateSelector != null) {
                this.ContentTemplate = this.ContentTemplateSelector.SelectTemplate(this.DataContext, this);
            }
        }
    }
}
