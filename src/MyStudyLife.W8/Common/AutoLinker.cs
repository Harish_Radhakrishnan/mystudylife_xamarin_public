﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Documents;

namespace MyStudyLife.W8.Common {
    public class AutoLinker {
        public static string GetText(DependencyObject d) {
            return (string)d.GetValue(TextProperty);
        }

        public static void SetText(DependencyObject d, string text) {
            d.SetValue(TextProperty, text);
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.RegisterAttached(
            "Text", typeof(string), typeof(AutoLinker), new PropertyMetadata(null, OnTextChanged));

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var textBlock = d as TextBlock;

            if (textBlock == null) {
                return;
            }

            var text = e.NewValue as string;
            textBlock.Inlines.Clear();

            if (String.IsNullOrEmpty(text)) {
                return;
            }

            var matches = UI.Helpers.AutoLinker.GetMatches(text).ToList();

            if (matches.Count == 0) {
                textBlock.Inlines.Add(new Run {
                    Text = text
                });
                return;
            }

            int lastMatchEndIndex = 0;

            for (int i = 0; i < matches.Count; i++) {
                var match = matches[i];

                if (match.StartIndex > 0) {
                    textBlock.Inlines.Add(new Run {
                        Text = text.Substring(lastMatchEndIndex, match.StartIndex - lastMatchEndIndex)
                    });
                }

                var hyperlink = new Hyperlink {
                    NavigateUri = match.Uri
                };

                hyperlink.Inlines.Add(new Run {
                    Text = match.Text
                });

                textBlock.Inlines.Add(hyperlink);

                lastMatchEndIndex = match.EndIndex;

                if (i == (matches.Count - 1)) {
                    if (lastMatchEndIndex < (text.Length - 1)) {
                        textBlock.Inlines.Add(new Run {
                            Text = text.Substring(lastMatchEndIndex, text.Length - lastMatchEndIndex)
                        });
                    }
                }
            }
        }
    }
}
