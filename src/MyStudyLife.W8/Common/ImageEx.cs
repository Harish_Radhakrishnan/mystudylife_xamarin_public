﻿using System;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

namespace MyStudyLife.W8.Common {
    public static class ImageEx {
        public static DependencyProperty LocalStorageUriSource = DependencyProperty.RegisterAttached(
            "LocalStorageUriSource", typeof(string), typeof(ImageEx), new PropertyMetadata(null, OnPathChanged));

        public static string GetLocalStorageUriSource(DependencyObject d) => (string)d.GetValue(LocalStorageUriSource);
        public static void SetLocalStorageUriSource(DependencyObject d, string text) => d.SetValue(LocalStorageUriSource, text);

        private static async void OnPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var image = d as BitmapImage;

            if (image == null) {
                return;
            }

            var path = (string)e.NewValue;

            if (path == null) {
                image.UriSource = null;
            }
            else {
                var file = await StorageFile.GetFileFromPathAsync(path);

                FileRandomAccessStream stream = (FileRandomAccessStream)await file.OpenAsync(FileAccessMode.Read);

                image.SetSource(stream);
            }
        }
    }
}
