﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace MyStudyLife.W8.Common {
    public class ComboBoxItemTemplateSelector : DataTemplateSelector {
        public DataTemplate NullTemplate { get; set; }
        public DataTemplate SelectedTemplate { get; set; }
        public DataTemplate DropDownTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container) {
            if (item == null) {
                return NullTemplate;
            }

            var comboBoxItem = container.GetVisualParent<ComboBoxItem>();
            if (comboBoxItem == null) {
                return SelectedTemplate;
            }
            return DropDownTemplate;
        }
    }
}
