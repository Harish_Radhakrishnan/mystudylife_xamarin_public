﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;

namespace MyStudyLife.W8.Controls {
    public sealed partial class ConfirmationFlyout {

        public event TappedEventHandler OnConfirm;

        public static readonly DependencyProperty ConfirmationTextProperty =
            DependencyProperty.Register("ConfirmationText", typeof(string), typeof(ConfirmationFlyout), new PropertyMetadata(String.Empty));

        public string ConfirmationText {
            get { return (string)GetValue(ConfirmationTextProperty); }
            set { SetValue(ConfirmationTextProperty, value); }
        }

        public static readonly DependencyProperty ConfirmationActionProperty =
            DependencyProperty.Register("ConfirmationAction", typeof(string), typeof(ConfirmationFlyout), new PropertyMetadata("Delete"));

        public string ConfirmationAction {
            get { return (string)GetValue(ConfirmationActionProperty); }
            set { SetValue(ConfirmationActionProperty, value); }
        }

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(ConfirmationFlyout), new PropertyMetadata(null));

        public ICommand Command {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly DependencyProperty CommandParameterProperty =
            DependencyProperty.Register("CommandParameter", typeof(object), typeof(ConfirmationFlyout), new PropertyMetadata(null));

        public object CommandParameter {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public ConfirmationFlyout() {
            this.InitializeComponent();
        }

        private void ConfirmationButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            if (OnConfirm != null) {
                OnConfirm(this, e);
            }   
        }
    }
}
