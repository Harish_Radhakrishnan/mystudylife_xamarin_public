﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public class ModalHost : UserControl {

        public event EventHandler ModalChanged;

        public static readonly DependencyProperty ModalProperty = DependencyProperty.Register(
            "Modal", typeof (IModal), typeof (ModalHost), new PropertyMetadata(null, PropertyChangedCallback));
            
        public IModal Modal {
            get { return (IModal) GetValue(ModalProperty); }
            set { SetValue(ModalProperty, value); }
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var modalHost = (ModalHost) d;

            if (modalHost.Content != e.NewValue) {
                modalHost.Content.Content = e.NewValue;

                var handler = modalHost.ModalChanged;

                if (handler != null) {
                    handler(d, EventArgs.Empty);
                }
            }
        }

        public new ScrollViewer Content {
            get { return (ScrollViewer) base.Content; }
            set { base.Content = value; }
        }

        public ModalHost() {
            this.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            this.VerticalContentAlignment = VerticalAlignment.Top;
            this.Content = new ScrollViewer {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalScrollMode = ScrollMode.Disabled,
                VerticalScrollMode = ScrollMode.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,
                ZoomMode = ZoomMode.Disabled
            };
        }
    }
}
