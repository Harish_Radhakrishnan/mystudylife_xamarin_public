﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Cirrious.CrossCore;
using MyStudyLife.UI;

namespace MyStudyLife.W8.Controls {
    public sealed partial class AppBarNavigationButton : ICommandBarElement {

        public static readonly DependencyProperty IconProperty =
            DependencyProperty.Register("Icon", typeof(string), typeof(AppBarNavigationButton), new PropertyMetadata(null));

        public string Icon {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty TypeNameProperty =
            DependencyProperty.Register("TypeName", typeof (string), typeof (AppBarNavigationButton), new PropertyMetadata(null));

        public string TypeName {
            get { return (string)GetValue(TypeNameProperty); }
            set { SetValue(TypeNameProperty, value); }
        }

        private Type _viewModelType;

        private Type ViewModelType {
            get {
                return _viewModelType ??
                    (_viewModelType = Type.GetType(String.Format("MyStudyLife.UI.ViewModels.{0}ViewModel, MyStudyLife.UI.Core", TypeName), true));
            }
        }

        public AppBarNavigationButton() {
            this.InitializeComponent();

            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs) {
            if (String.IsNullOrWhiteSpace(TypeName)) return;

            var iconResourceName = (String.IsNullOrWhiteSpace(Icon) 
                ? (TypeName.EndsWith("es") ? TypeName.Substring(0, TypeName.Length - 2) : TypeName.TrimEnd('s'))
                : Icon) + "Glyph";

            this.NavigationIcon.Text = (String) App.Current.Resources[iconResourceName];
            this.NavigationText.Text = String.IsNullOrWhiteSpace(Icon) ? TypeName : Icon;
        }

        private void OnTapped(object sender, TappedRoutedEventArgs e) {
            Mvx.Resolve<INavigationService>().NavigateTo(ViewModelType);
        }

        public bool IsCompact { get; set; }
    }
}
