﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Animation;
using Cirrious.CrossCore;
using MyStudyLife.UI;
using MyStudyLife.UI.Data.Statistics;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Controls {
    public sealed partial class NowBox {

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(NowBox), new PropertyMetadata("Today"));

        public string Title {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty StatisticsProperty =
            DependencyProperty.Register("Statistics", typeof (StatisticGroup), typeof (NowBox), new PropertyMetadata(null, OnStatisticsChanged));

        public StatisticGroup Statistics {
            get { return (StatisticGroup)GetValue(StatisticsProperty); }
            set { SetValue(StatisticsProperty, value); }
        }

        public static readonly DependencyProperty NavigationTypeNameProperty =
            DependencyProperty.Register("NavigationTypeName", typeof(string), typeof(NowBox), new PropertyMetadata(null));

        public string NavigationTypeName {
            get { return (string)GetValue(NavigationTypeNameProperty); }
            set { SetValue(NavigationTypeNameProperty, value); }
        }

        private static void OnStatisticsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((NowBox)d).UpdateStatistics();

            if (e.NewValue != null) {
                ((StatisticGroup)e.NewValue).Statistics.CollectionChanged += (a, b) => ((NowBox)d).UpdateStatistics();
            }
        }

        private void UpdateStatistics() {
            StatisticGroup statGroup = this.Statistics;

            if (statGroup != null) {

                if (statGroup.Statistics.Any()) {

                    this.GOneNOneText.SetBinding(TextBlock.TextProperty, new Binding {
                        ElementName = this.CtrlRoot.Name,
                        Path = new PropertyPath("Statistics.Statistics[0].One")
                    });
                    this.GOneNTwoText.SetBinding(TextBlock.TextProperty, new Binding {
                        ElementName = this.CtrlRoot.Name,
                        Path = new PropertyPath("Statistics.Statistics[0].Two")
                    });

                    if (statGroup.Statistics.Count == 2) {
                        this.GTwoNOneText.SetBinding(TextBlock.TextProperty, new Binding {
                            ElementName = this.CtrlRoot.Name,
                            Path = new PropertyPath("Statistics.Statistics[1].One")
                        });
                        this.GTwoNTwoText.SetBinding(TextBlock.TextProperty, new Binding {
                            ElementName = this.CtrlRoot.Name,
                            Path = new PropertyPath("Statistics.Statistics[1].Two")
                        });
                    }
                }

                this.StartAnimations();
            }
        }

        public NowBox() {
            this.InitializeComponent();
        }

        private void StartAnimations() {
            if (this.EntranceStoryboard.GetCurrentState() != ClockState.Stopped) {
                this.EntranceStoryboard.Stop();
            }

            if (this.RotateTwoStoryboard.GetCurrentState() != ClockState.Stopped) {
                this.RotateTwoStoryboard.Stop();
            }

            if (this.Statistics != null && this.Statistics.Statistics.Count == 2) {
                this.EntranceStoryboard.Completed += EntranceStoryboardOnCompleted;
            }

            this.EntranceStoryboard.Begin();
        }

        private void EntranceStoryboardOnCompleted(object sender, object o) {
            this.EntranceStoryboard.Completed -= EntranceStoryboardOnCompleted;

            this.RotateTwoStoryboard.Begin();
        }


        #region Navigation

        private Type _viewModelType;

        private Type ViewModelType {
            get {
                return _viewModelType ??
                    (_viewModelType = Type.GetType(String.Format("MyStudyLife.UI.ViewModels.{0}ViewModel, MyStudyLife.UI.Core", NavigationTypeName), true));
            }
        }

        private void RootOnTapped(object sender, RoutedEventArgs e) {
            Mvx.Resolve<INavigationService>().NavigateTo(ViewModelType);
        }

        #endregion
    }
}
