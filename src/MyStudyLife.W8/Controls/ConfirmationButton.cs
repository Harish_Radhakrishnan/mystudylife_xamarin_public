﻿using System;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;

namespace MyStudyLife.W8.Controls {
    public class ConfirmationButton : Button {
        public static readonly DependencyProperty ConfirmationTextProperty =
            DependencyProperty.Register("ConfirmationText", typeof(string), typeof(ConfirmationFlyout), new PropertyMetadata(String.Empty));

        public string ConfirmationText {
            get { return (string)GetValue(ConfirmationTextProperty); }
            set { SetValue(ConfirmationTextProperty, value); }
        }

        public static readonly DependencyProperty ConfirmationActionProperty =
            DependencyProperty.Register("ConfirmationAction", typeof(string), typeof(ConfirmationButton), new PropertyMetadata("Delete"));

        public string ConfirmationAction {
            get { return (string)GetValue(ConfirmationActionProperty); }
            set { SetValue(ConfirmationActionProperty, value); }
        }

        public static readonly DependencyProperty ConfirmationCommandProperty =
            DependencyProperty.Register("ConfirmationCommand", typeof(ICommand), typeof(ConfirmationButton), new PropertyMetadata(null));

        public ICommand ConfirmationCommand {
            get { return (ICommand)GetValue(ConfirmationCommandProperty); }
            set { SetValue(ConfirmationCommandProperty, value); }
        }

        public static readonly DependencyProperty ConfirmationCommandParameterProperty =
            DependencyProperty.Register("ConfirmationCommandParameter", typeof(object), typeof(ConfirmationButton), new PropertyMetadata(null));

        public object ConfirmationCommandParameter {
            get { return GetValue(ConfirmationCommandParameterProperty); }
            set { SetValue(ConfirmationCommandParameterProperty, value); }
        }

        public static readonly DependencyProperty ConfirmationPlacementProperty =
            DependencyProperty.Register("ConfirmationPlacement", typeof(FlyoutPlacementMode), typeof(ConfirmationButton), new PropertyMetadata(FlyoutPlacementMode.Top));

        public FlyoutPlacementMode ConfirmationPlacement {
            get { return (FlyoutPlacementMode)GetValue(ConfirmationPlacementProperty); }
            set { SetValue(ConfirmationPlacementProperty, value); }
        }

        #region Overrides of Control

        protected override void OnTapped(TappedRoutedEventArgs e) {
            var confFlyout = new ConfirmationFlyout {
                MaxWidth = 340,
                MaxHeight = 190,
                ConfirmationText = this.ConfirmationText,
                ConfirmationAction = this.ConfirmationAction,
                Command = this.ConfirmationCommand,
                CommandParameter = this.ConfirmationCommandParameter
            };

            var f = new Flyout {
                Content = confFlyout,
                Placement = ConfirmationPlacement
            };

            confFlyout.OnConfirm += (a, b) => f.Hide();

            FlyoutBase.SetAttachedFlyout(this, f);
            FlyoutBase.ShowAttachedFlyout(this);
        }

        #endregion
    }
}
