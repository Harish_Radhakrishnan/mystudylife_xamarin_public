﻿using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public sealed partial class HolidayInputFlyout : IModal<HolidayInputViewModel> {

        public HolidayInputViewModel ViewModel {
            get { return (HolidayInputViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        public HolidayInputFlyout() {
            this.InitializeComponent();
        }
    }
}
