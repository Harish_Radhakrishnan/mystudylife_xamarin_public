﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public abstract class InternalViewClass : ViewEntityControl<Class> {}

    [Modal(HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Center)]
    public sealed partial class ViewClass : IModal<ClassViewModel> {
        IMvxViewModel IModal.ViewModel  {
            get { return base.ViewModel; }
            set { base.ViewModel = value as EntityViewModel<Class>; }
        } 

        public new ClassViewModel ViewModel {
            get { return (ClassViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        protected override void Initialize() {
            this.InitializeComponent();

            this.Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e) {
            if (this.ViewModel != null) {
                this.ViewModel.CancelPendingRequests();
            }
        }

        private void ItemsWrapGridOnLayoutUpdated(object sender, object e) {
            var tic = (ItemsWrapGrid)sender;
            
            if (tic == null) return;

            var width = tic.ActualWidth / tic.MaximumRowsOrColumns;

            if (tic.Children.Any(x => ((FrameworkElement) x).ActualWidth > width)) {
                width = tic.ActualWidth / (tic.MaximumRowsOrColumns - 1);
            }

            tic.ItemWidth = width;
        }

        private void ShowFront(object sender, TappedRoutedEventArgs e) {
            VisualStateManager.GoToState(this, "Front", true);
        }

        private void ShowBack(object sender, TappedRoutedEventArgs e) {
            if (this.ViewModel.StudentsHasErrored) {
                this.ViewModel.SetStudentsCommand.Execute(null);
            }
            else {
                VisualStateManager.GoToState(this, "Back", true);
            }
        }

        private void LeaveOnTapped(object sender, TappedRoutedEventArgs e) {
            var confFlyout = new ConfirmationFlyout {
                MaxWidth = 340,
                MaxHeight = 190,
                ConfirmationText = R.ClassInput.LeaveConfirmation,
                ConfirmationAction = R.Leave,
                Command = this.ViewModel.LeaveCommand
            };

            var f = new Flyout {
                Content = confFlyout,
                Placement = FlyoutPlacementMode.Bottom
            };

            confFlyout.OnConfirm += (a, b) => f.Hide();

            FlyoutBase.SetAttachedFlyout(this.MoreButton, f);
            FlyoutBase.ShowAttachedFlyout(this.MoreButton);
        }
    }
}
