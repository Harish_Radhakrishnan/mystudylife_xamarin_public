﻿using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public sealed partial class AcademicTermInputFlyout : IModal<AcademicTermInputViewModel> {
        
        public AcademicTermInputViewModel ViewModel {
            get { return (AcademicTermInputViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        public AcademicTermInputFlyout() {
            this.InitializeComponent();
        }
    }
}
