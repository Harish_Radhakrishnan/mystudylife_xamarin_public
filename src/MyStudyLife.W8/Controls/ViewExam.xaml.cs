﻿using Windows.UI.Xaml;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public abstract class InternalViewExam : ViewEntityControl<Exam> { }

    [Modal(HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Center)]
    public sealed partial class ViewExam : IModal<ExamViewModel> {
        IMvxViewModel IModal.ViewModel {
            get { return base.ViewModel; }
            set { base.ViewModel = value as EntityViewModel<Exam>; }
        }

        public new ExamViewModel ViewModel {
            get { return (ExamViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        protected override void Initialize() {
            this.InitializeComponent();
        }
    }
}
