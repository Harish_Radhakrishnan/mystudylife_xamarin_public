﻿using System.ComponentModel;
using Windows.ApplicationModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using Cirrious.CrossCore.WeakSubscription;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    [Modal(Backdrop = ModalBackdrop.Static)]
    public sealed partial class JoinClassesWizard : IModal<JoinClassesViewModel> {
// ReSharper disable once NotAccessedField.Local
        private MvxNotifyPropertyChangedEventSubscription _viewModelPropertyChangedEventSubscription;

        IMvxViewModel IModal.ViewModel  {
            get { return this.ViewModel; }
            set { this.ViewModel = (JoinClassesViewModel)value; }
        } 

        public JoinClassesViewModel ViewModel {
            get { return (JoinClassesViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        public JoinClassesWizard() {
            this.InitializeComponent();

            VisualStateManager.GoToState(this, "Step0", true);

            this.DataContextChanged += OnDataContextChanged;
        }

        private void OnDataContextChanged(FrameworkElement sender, DataContextChangedEventArgs e) {
            if (this.ViewModel != null) {
                _viewModelPropertyChangedEventSubscription = this.ViewModel.WeakSubscribe(ViewModelOnPropertyChanged);
            }
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "Step") {
                // Then we're passed finished
                if (this.ViewModel.Step >= 5) {
                    return;
                }

                this.WrapStack.Margin = new Thickness(
                    -(double)App.Current.Resources["HorizontalPopupContentWidth"] * this.ViewModel.Step,
                    this.WrapStack.Margin.Top, this.WrapStack.Margin.Right, this.WrapStack.Margin.Bottom
                );

                VisualStateManager.GoToState(this, "Step" + this.ViewModel.Step, true);
            }
        }

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this.RootContent,
                sender as FrameworkElement,
                "The selected year/term will determine which classes you are able to join in the next steps."
            );
        }
    }
}
