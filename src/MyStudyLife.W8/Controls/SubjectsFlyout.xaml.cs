﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public sealed partial class SubjectsFlyout : IModal<SubjectsViewModel> {
    
        public SubjectsViewModel ViewModel {
            get { return (SubjectsViewModel) this.DataContext; }
            set { this.DataContext = value; }
        }
        
        public SubjectsFlyout() {
            this.InitializeComponent();
        }

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(this.RootContent, sender as FrameworkElement, R.SubjectsScheduleFilterHelp);
        }

        private void SubjectOnTapped(object sender, TappedRoutedEventArgs e) {
            var subject = (Subject) ((FrameworkElement) sender).DataContext;
            
            if (this.ViewModel == null) {
                return;
            }

            if (subject.IsFromSchool && !this.ViewModel.User.IsTeacher) {

                var f = new Flyout {
                    Content = new TextBlock {
                        MaxWidth = 340,
                        Text = R.SubjectFromSchoolError,
                        TextWrapping = TextWrapping.WrapWholeWords,
                        Style = (Style) App.Current.Resources["MediumTextStyle"]
                    },
                    Placement = FlyoutPlacementMode.Top
                };

                FlyoutBase.SetAttachedFlyout((FrameworkElement)sender, f);
                FlyoutBase.ShowAttachedFlyout((FrameworkElement)sender);
            }
            else {
                this.ViewModel.EditCommand.Execute(subject);
            }
        }
    }
}
