﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Cirrious.CrossCore;
using MyStudyLife.Data;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Controls {
    public sealed partial class AcademicScheduleSelector {

        private Flyout _flyout;

        #region Academic Years

        public static readonly DependencyProperty AcademicYearsProperty =
            DependencyProperty.Register("AcademicYears", typeof(ObservableCollection<AcademicYear>), typeof(AcademicScheduleSelector), new PropertyMetadata(null, OnAcademicYearsChanged));

        private static void OnAcademicYearsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var selector = (AcademicScheduleSelector)d;

            var selectedSchedule = selector.SelectedSchedule;

            selector._preventSelectionChangedEvent = true;

            selector.YearComboBox.ItemsSource = GetYearsItemSource((ObservableCollection<AcademicYear>)e.NewValue);

            selector._preventSelectionChangedEvent = false;

            selector.SelectedSchedule = selectedSchedule;

            selector.SetPickersFromSelectedSchedule();
            selector.SetCurrentButtonDisabled();
        }

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return (ObservableCollection<AcademicYear>)GetValue(AcademicYearsProperty); }
            set { SetValue(AcademicYearsProperty, value); }
        }

        #endregion

        #region Selected Schedule

        public static readonly DependencyProperty SelectedScheduleProperty =
            DependencyProperty.Register("SelectedSchedule", typeof(IAcademicSchedule), typeof(AcademicScheduleSelector), new PropertyMetadata(null, OnSelectedScheduleChanged));

        private static void OnSelectedScheduleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var selector = (AcademicScheduleSelector)d;

            selector.SetPickersFromSelectedSchedule();
        }

        public IAcademicSchedule SelectedSchedule {
            get { return (IAcademicSchedule)GetValue(SelectedScheduleProperty); }
            set { SetValue(SelectedScheduleProperty, value); }
        }

        private bool _preventSelectionChangedEvent;

        private void SetPickersFromSelectedSchedule() {
            var selector = this;

            _preventSelectionChangedEvent = true;

            if (this.SelectedSchedule == null || this.SelectedSchedule.Guid.IsEmpty()) {
                selector.YearComboBox.SelectedValue = Guid.Empty;
                selector.TermComboBox.ItemsSource = GetTermsItemSource(null);
            }
            else {
                var schedule = this.SelectedSchedule;
                var term = schedule as AcademicTerm;

                if (term != null) {
                    var year = AcademicYears.Single(x => x.Guid == term.YearGuid);

                    selector.YearComboBox.SelectedValue = year.Guid;
                    selector.TermComboBox.ItemsSource = GetTermsItemSource(year.Terms);
                    selector.TermComboBox.SelectedValue = schedule.Guid;
                }
                else {
					var year = AcademicYears.Single(x => x.Guid == schedule.Guid);
                    
                    selector.YearComboBox.SelectedValue = year.Guid;
                    selector.TermComboBox.ItemsSource = GetTermsItemSource(year.Terms);
                    selector.TermComboBox.SelectedValue = Guid.Empty;
                }
            }

            selector.SetCurrentButtonDisabled();

            _preventSelectionChangedEvent = false;
        }

        // ReSharper disable ReturnTypeCanBeEnumerable.Local
        private static object GetTermsItemSource(IEnumerable<AcademicTerm> terms) {
            var nCollection = terms != null ? terms.ToList() : new List<AcademicTerm>();

            nCollection.Insert(0, new AcademicTerm());

            return nCollection;
        }
        private static object GetYearsItemSource(IEnumerable<AcademicYear> years) {
            var nCollection = years != null ? years.ToList() : new List<AcademicYear>();

            nCollection.Insert(0, new AcademicYear());

            return nCollection;
        }
        // ReSharper restore ReturnTypeCanBeEnumerable.Local

        #endregion

        #region Show New

        public static readonly DependencyProperty ShowNewProperty = DependencyProperty.Register(
            "ShowNew", typeof(bool), typeof(AcademicScheduleSelector), new PropertyMetadata(false));

        public bool ShowNew {
            get { return (bool)GetValue(ShowNewProperty); }
            set { SetValue(ShowNewProperty, value); }
        }

        #endregion

        public AcademicScheduleSelector(Flyout flyout) {
            _flyout = flyout;

            _flyout.Content = this;

            this.InitializeComponent();

            this.Loaded += OnLoaded;

            this.CurrentScheduleButton.Tapped += CurrentScheduleButtonOnTapped;
            this.NewScheduleButton.Tapped += NewScheduleButtonOnTapped;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            this.SetCurrentButtonDisabled();

            this.YearComboBox.SelectionChanged += YearComboBoxOnSelectionChanged;
            this.TermComboBox.SelectionChanged += TermComboBoxOnSelectionChanged;
        }

        private void YearComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (_preventSelectionChangedEvent) return;

            var s = this.YearComboBox.SelectedItem as AcademicYear;

            if (this.SelectedSchedule != null && s != null) {
                if (this.SelectedSchedule.Equals(s) || (this.SelectedSchedule is AcademicTerm && ((AcademicTerm)this.SelectedSchedule).Year.Equals(s))) {
                    return;
                }
            }

            this.SelectedSchedule = s == null || s.Guid.IsEmpty() ? null : s;
        }

        private void TermComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (_preventSelectionChangedEvent) return;

            var s = this.TermComboBox.SelectedItem as IAcademicSchedule ?? this.YearComboBox.SelectedItem as IAcademicSchedule;

            this.SelectedSchedule = s == null || s.Guid.IsEmpty() ? (IAcademicSchedule)this.YearComboBox.SelectedItem : s;
        }

        private async void CurrentScheduleButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            using (var acYearRepo = new AcademicYearRepository()) {
                this.SelectedSchedule = await acYearRepo.GetCurrentAsync();
            }
        }

        private void NewScheduleButtonOnTapped(object sender, TappedRoutedEventArgs e) {
            if (_flyout != null) {
                _flyout.Hide();
            }
            
            Mvx.Resolve<INavigationService>().NavigateTo<AcademicYearInputViewModel>();
        }

        private async void SetCurrentButtonDisabled() {
            // There's no schedules, obviously disabled.
            if (this.YearComboBox.Items == null || this.YearComboBox.Items.Count == 0) {
                this.CurrentScheduleButton.IsEnabled = false;

                return;
            }
            var s = (AcademicYear)this.YearComboBox.SelectedItem;

            if (s == null || s.Guid.IsEmpty()) {
                this.TermStack.Visibility = Visibility.Collapsed;
            }
            else {
                this.TermStack.Visibility = Visibility.Visible;
            }

            using (var acScheduleRepo = new AcademicYearRepository()) {
                var currentSchedule = await acScheduleRepo.GetCurrentAsync();

                this.CurrentScheduleButton.IsEnabled = (this.SelectedSchedule == null && currentSchedule != null) ||
                                                       (this.SelectedSchedule != null && currentSchedule != null && this.SelectedSchedule.Guid != currentSchedule.Guid);
            }
        }

        public static AcademicScheduleSelector GetInstanceAndShow(Panel parent, FrameworkElement placementTarget, string helpText, bool showNew = false) {
            var f = new Flyout {
                Placement = FlyoutPlacementMode.Top
            };
            
            var selector = new AcademicScheduleSelector(f) {
                DataContext = parent.DataContext,
                HelpText = {
                    Text = helpText
                }
            };

            selector.SetBinding(AcademicYearsProperty, new Binding {
                Path = new PropertyPath("AcademicYears")
            });
            selector.SetBinding(SelectedScheduleProperty, new Binding {
                Path = new PropertyPath("SelectedSchedule"),
                Mode = BindingMode.TwoWay
            });

            FlyoutBase.SetAttachedFlyout(placementTarget, f);
            FlyoutBase.ShowAttachedFlyout(placementTarget);

            return selector;
        }
    }

    public class AcademicScheduleSelectorItemConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, string language) {
            if (value as IAcademicSchedule == null || ((IAcademicSchedule)value).Guid.IsEmpty()) {
                return "No " + parameter;
            }

            var term = value as AcademicTerm;

            if (term != null) {
                return term.Name;
            }

            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language) {
            throw new NotSupportedException();
        }
    }
}
