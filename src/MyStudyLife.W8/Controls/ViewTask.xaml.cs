﻿using System.Diagnostics;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Documents;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {

    public abstract class InternalViewTask : ViewEntityControl<Task> {}

    [Modal(HorizontalAlignment = HorizontalAlignment.Stretch, VerticalAlignment = VerticalAlignment.Center)]
    public sealed partial class ViewTask : IModal<TaskViewModel> {
        IMvxViewModel IModal.ViewModel {
            get { return base.ViewModel; }
            set { base.ViewModel = value as EntityViewModel<Task>; }
        } 

        public new TaskViewModel ViewModel {
            get { return (TaskViewModel) base.ViewModel; }
            set { base.ViewModel = value; }
        }

        protected override void Initialize() {
            this.InitializeComponent();
        }

        private void SetIncompleteOnClick(Hyperlink sender, HyperlinkClickEventArgs e) {
            this.ViewModel.SetIncompleteCommand.Execute(null);
        }

        private void AddDetailOnClick(Hyperlink sender, HyperlinkClickEventArgs e) {
            this.ViewModel.EditCommand.Execute(null);
        }
    }
}
