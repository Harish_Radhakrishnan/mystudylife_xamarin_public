﻿using System;
using Windows.ApplicationModel;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8.Controls {

    public abstract class ViewEntityControl<TEntity> : UserControl
        where TEntity : SubjectDependentEntity {

        private bool _isHostedInSplitView;
        private Storyboard _animationInStoryboard;

        protected bool IsHostedInSplitView {
            get { return _isHostedInSplitView; }
        }
        
        private FrameworkElement Root {
            get { return FindName("Root") as FrameworkElement; }
        }

        public EntityViewModel<TEntity> ViewModel {
            get { return this.Root.DataContext as EntityViewModel<TEntity>; }
            set { this.Root.DataContext = value; }
        }

        protected ViewEntityControl() {
            // For some reason we can't call InitializeComponent here...
            // so we just make a method that has to be implemented so 
            // it's not forgotten.

// ReSharper disable once DoNotCallOverridableMethodsInConstructor
            this.Initialize();

            if (DesignMode.DesignModeEnabled) return;

            Window.Current.SizeChanged += WindowSizeChanged;

            this.Loaded += OnLoaded;
        }

        protected abstract void Initialize();

        private void OnLoaded(object sender, RoutedEventArgs e) {
            _isHostedInSplitView = this.GetVisualParent<Page>() != null;

            if (!IsHostedInSplitView) {
                this.MaxWidth = 630;
            }

            ActivateStates();

            _animationInStoryboard = new Storyboard();

            _animationInStoryboard.Children.Add(new PopInThemeAnimation {
                TargetName = "ViewRoot",
                FromHorizontalOffset = _isHostedInSplitView ? 50 : 0,
                FromVerticalOffset = _isHostedInSplitView ? 0 : 50
            });

            // Has to be added to resources before playing
            this.Resources["AnimationIn"] = _animationInStoryboard;

            _animationInStoryboard.Begin();
        }

        #region Visual State Handling

        private double _previousWidth;

        private void WindowSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            ActivateStates();
        }

        private void ActivateStates() {
            var ctrl = this;

            var width = Window.Current.Bounds.Width;

            const bool useTransitions = false;

            // Fixes a bug where when resized to be larger, the appropriate state
            // was not applied because it had already been applied...
            //
            // The below code will only reset the state when appropriate - ie. if
            // we move from 500 to 768 it won't bother resetting 500.
            if (_previousWidth > 0 && width > _previousWidth) {
                if (_previousWidth <= 320) {
                    VisualStateManager.GoToState(ctrl, "Below500Inactive", useTransitions);
                }

                if (_previousWidth <= 500) {
                    VisualStateManager.GoToState(ctrl, "Below768Inactive", useTransitions);
                }

                if (_previousWidth <= 768) {
                    VisualStateManager.GoToState(ctrl, "Below1024Inactive", useTransitions);
                }
            }

            _previousWidth = width;

            string splitView = _isHostedInSplitView ? "SplitView" : String.Empty;

            VisualStateManager.GoToState(ctrl, width <= 1024 ? "Below1024" + splitView + "Active" : "Below1024Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 768 ? "Below768" + splitView + "Active" : "Below768Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 500 ? "Below500" + splitView + "Active" : "Below500Inactive", useTransitions);
            VisualStateManager.GoToState(ctrl, width <= 320 ? "Below320" + splitView + "Active" : "Below320Inactive", useTransitions);
        }

        #endregion
    }
}
