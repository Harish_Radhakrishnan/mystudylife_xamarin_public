﻿using System;
using System.Threading;
using Windows.ApplicationModel;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Cirrious.CrossCore;
using MyStudyLife.Sync;

namespace MyStudyLife.W8.Controls {
    public sealed partial class ActivityIndicator {
        private ISyncService _syncService;
        private ISyncService SyncService {
            get { return _syncService ?? (_syncService = Mvx.Resolve<ISyncService>()); }
        }

        public ActivityIndicator() {
            this.InitializeComponent();

            if (DesignMode.DesignModeEnabled) return;

            this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            SyncService.StatusChanged += SyncServiceOnStatusChanged;
            SyncService.Completed += SyncServiceOnCompleted;
            SyncService.Error += SyncServiceOnError;

            if (SyncService.Status == SyncStatus.Syncing) {
                GoToSyncing();
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs e) {
            SyncService.StatusChanged -= SyncServiceOnStatusChanged;
            SyncService.Error -= SyncServiceOnError;
        }

        private async void SyncServiceOnStatusChanged(object sender, SyncStatusChangedEventArgs e) {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {

                switch (e.NewStatus) {
                    case SyncStatus.Offline:
                    case SyncStatus.Disabled:
                    case SyncStatus.Throttled:
                        this.SyncErrorText.Text = "Sync error: " + e.NewStatus.ToString().ToLower();
                        GoToError();
                        break;
                    case SyncStatus.Syncing:
                        GoToSyncing();
                        break;
                }
            });
        }

        private async void SyncServiceOnCompleted(object sender, SyncCompletedEventArgs e) {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    () => {
                        GoToCompleted();
                        IdleIn(5000);
                    });
        }

        private async void SyncServiceOnError(object sender, SyncErrorEventArgs e) {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                () => {
                    this.SyncErrorText.Text = "Sync error: " + e.Message.ToLower();
                    GoToError();
                    IdleIn(5000);
                });
        }

        #region Utils

        private void GoToIdle() {
            VisualStateManager.GoToState(this, "Idle", true);
        }

        private void GoToSyncing() {
            VisualStateManager.GoToState(this, "Syncing", true);
        }

        private void GoToCompleted() {
            VisualStateManager.GoToState(this, "Completed", true);
        }

        private void GoToError() {
            VisualStateManager.GoToState(this, "Error", true);
        }

        private async void IdleIn(int ms) {
            await System.Threading.Tasks.Task.Run(async () => {
                using (var m = new ManualResetEvent(false)) {
                    m.WaitOne(ms);

                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, GoToIdle);
                }
            });
        }

        #endregion
    }
}
