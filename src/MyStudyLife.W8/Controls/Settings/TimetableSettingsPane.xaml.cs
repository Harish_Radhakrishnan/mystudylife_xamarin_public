﻿using Windows.UI.Xaml;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.W8.Controls.Settings {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TimetableSettingsPane {
        private TimetableSettingsViewModel ViewModel {
            get { return (TimetableSettingsViewModel) DataContext; }
            set { DataContext = value; }
        }

        public TimetableSettingsPane(TimetableSettingsViewModel viewModel) {
            this.ViewModel = viewModel;

            this.InitializeComponent();

            this.Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs) {
            if (this.ViewModel != null) {
                this.ViewModel.SaveChanges();
            }
        }
    }
}
