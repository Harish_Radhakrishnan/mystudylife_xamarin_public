﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.W8.Controls.Settings {
    public sealed partial class SettingsPane {
        private SettingsViewModel ViewModel {
            get { return this.DataContext as SettingsViewModel; }
            set { this.DataContext = value; }
        }

        public SettingsPane(SettingsViewModel viewModel) {
            this.ViewModel = viewModel;

            this.InitializeComponent();
        }

        private void NavigationItem_Tapped(object sender, TappedRoutedEventArgs e) {
            var items = this.NavigationItemsControl.Items;

            if (items != null) {
                int itemIndex = items.IndexOf(((TextBlock)sender).Text);

                string viewModel;

                switch (itemIndex) {
                    case 0:
                        viewModel = "General";
                        break;
                    case 1:
                        viewModel = "Reminder";
                        break;
                    case 2:
                        viewModel = "Sync";
                        break;
                    case 3:
                        viewModel = "Timetable";
                        break;
                    default:
                        throw new NotImplementedException("Extra item found in settings list.");
                }

                this.ViewModel.NavigateCommand.Execute("Settings." + viewModel + "Settings");
            }
        }
    }
}
