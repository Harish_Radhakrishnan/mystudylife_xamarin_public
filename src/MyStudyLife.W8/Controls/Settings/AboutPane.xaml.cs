﻿using System;
using Windows.ApplicationModel;
using Windows.System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace MyStudyLife.W8.Controls.Settings {
    public sealed partial class AboutPane {
		public AboutPane() {
            this.InitializeComponent();

            var appVersion = Package.Current.Id.Version;

            this.VersionRunText.Text = String.Format("{0}.{1}.{2}",
                                                    appVersion.Major, appVersion.Minor,
                                                    appVersion.Build);
            this.YearRunText.Text = DateTime.Now.Year.ToString();
        }

	    private async void SocialButtonOnTapped(object sender, TappedRoutedEventArgs e) {
		    await Launcher.LaunchUriAsync(new Uri((string) ((Button) sender).Tag));
	    }
    }
}
