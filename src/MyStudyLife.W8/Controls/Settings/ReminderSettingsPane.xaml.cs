﻿using Windows.UI.Xaml;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.W8.Controls.Settings {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ReminderSettingsPane {
        private ReminderSettingsViewModel ViewModel {
            get { return (ReminderSettingsViewModel)DataContext; }
            set { DataContext = value; }
        }

        public ReminderSettingsPane(ReminderSettingsViewModel viewModel) {
            this.ViewModel = viewModel;

            this.InitializeComponent();

            this.Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs) {
            if (this.ViewModel != null) {
                this.ViewModel.SaveChanges();
            }
        }
    }
}
