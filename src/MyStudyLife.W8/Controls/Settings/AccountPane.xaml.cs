﻿using Windows.UI.Xaml.Controls;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.W8.Platform;
using Cirrious.CrossCore;

namespace MyStudyLife.W8.Controls.Settings {
    public sealed partial class AccountPane : IHandleClose {
        private static AccountPane _current;

        public SettingsFlyout HostFlyout {
            get { return (SettingsFlyout) this.Parent; }
        }

        private AccountViewModel ViewModel {
            get { return this.DataContext as AccountViewModel; }
            set { this.DataContext = value; }
        }

        public AccountPane() {
            _current = this;

            this.InitializeComponent();

            this.ViewModel = Mvx.IocConstruct<AccountViewModel>();
            this.ViewModel.Start();
        }

        public static AccountPane GetInstance() {
            return _current;
        }

        public static void TryGetInstanceAndHandleClose() {
            if (_current == null || _current.HostFlyout == null) return;

            _current.HostFlyout.Hide();

            _current = null;
        }

        #region Implementation of IHandleClose

        public void HandleClose(IMvxViewModel viewModel) {
            this.HostFlyout.Hide();

            _current = null;
        }

        #endregion
    }
}
