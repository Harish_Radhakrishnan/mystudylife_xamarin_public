﻿using Windows.UI.Xaml;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.W8.Controls.Settings {
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GeneralSettingsPane {
        private GeneralSettingsViewModel ViewModel {
            get { return (GeneralSettingsViewModel)DataContext; }
            set { DataContext = value; }
        }

        public GeneralSettingsPane(GeneralSettingsViewModel viewModel) {
            this.ViewModel = viewModel;

            this.InitializeComponent();

            this.Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e) {
            if (this.ViewModel != null) {
                this.ViewModel.SaveChanges();
            }
        }
    }
}
