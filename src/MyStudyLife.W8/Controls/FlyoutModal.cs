﻿using System;
using System.Diagnostics;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media.Animation;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    [Modal(Backdrop = ModalBackdrop.StaticInvisible, VerticalAlignment = VerticalAlignment.Stretch)]
    public class FlyoutModal : UserControl, IModal {

        IMvxViewModel IModal.ViewModel {
            get { return (IMvxViewModel) this.DataContext; }
            set { this.DataContext = value; }
        }

        public FlyoutModal() {
            this.Transitions = new TransitionCollection {
                new PaneThemeTransition {
                    Edge = EdgeTransitionLocation.Left
                }
            };
        
            this.Loaded += OnLoaded;
            this.Unloaded += OnUnloaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            Window.Current.SizeChanged += WindowOnSizeChanged;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e) {
            Window.Current.SizeChanged -= WindowOnSizeChanged;
        }

        private void WindowOnSizeChanged(object sender, WindowSizeChangedEventArgs e) {
            var rootContent = (FrameworkElement) this.FindName("RootContent");

            if (rootContent == null) {
                Debug.WriteLine("WARNING: Failed to find RootContent element in " + this.GetType().Name);
            }
            else {
                rootContent.Width = e.Size.Width > 520 ? 520 : e.Size.Width;
            }
        }
    }
}
