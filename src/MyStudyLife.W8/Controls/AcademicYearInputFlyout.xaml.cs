﻿using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Common;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public sealed partial class AcademicYearInputFlyout : IModal<AcademicYearInputViewModel> {

        public AcademicYearInputViewModel ViewModel {
            get { return (AcademicYearInputViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        public AcademicYearInputFlyout() {
            this.InitializeComponent();

            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            var tabs = this.Tabs.Children.OfType<ToggleButton>().ToList();

            if (tabs.All(x => !x.IsChecked.GetValueOrDefault())) {
                tabs[0].IsChecked = true;
            }

            tabs.Apply(x => x.Checked += TabOnChecked);

            SetTabContentVisibility(tabs.Single(x => x.IsChecked.GetValueOrDefault()).Tag.ToString());
        }

        private void TabOnChecked(object sender, RoutedEventArgs e) {
            var checkedTab = (ToggleButton) sender;
            var tabs = this.Tabs.Children.OfType<ToggleButton>().ToList();

            foreach (var tab in tabs) {
                if (tab != checkedTab) {
                    tab.IsChecked = false;
                    VisualStateManager.GoToState(tab, "Unchecked", true);
                }
            }

            SetTabContentVisibility(checkedTab.Tag.ToString());
        }

        private void SetTabContentVisibility(string checkedTabName) {
            var tabContent = (FrameworkElement)this.FindName(checkedTabName + "TabContent");

            if (tabContent != null) {
                tabContent.Visibility = Visibility.Visible;
            }

            this.TabsContent.Children.Where(x => x != tabContent).Apply(x => x.Visibility = Visibility.Collapsed);
        }
    }
}
