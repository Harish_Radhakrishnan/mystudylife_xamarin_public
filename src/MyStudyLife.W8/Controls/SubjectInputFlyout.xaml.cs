﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Input;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.W8.Platform;

namespace MyStudyLife.W8.Controls {
    public sealed partial class SubjectInputFlyout : IModal<SubjectInputViewModel> {

        public SubjectInputViewModel ViewModel {
            get { return (SubjectInputViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }
        
        public SubjectInputFlyout() {
            this.InitializeComponent();
        }

        private void AcademicScheduleSelectorOnTapped(object sender, TappedRoutedEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this.RootContent,
                sender as FrameworkElement,
                R.SubjectInput.AcademicScheduleInputHelp,
                true
            );
        }
    }
}
