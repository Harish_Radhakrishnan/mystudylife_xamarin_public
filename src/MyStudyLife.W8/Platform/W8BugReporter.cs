﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mindscape.Raygun4Net;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.W8.Platform {
    public class W8BugReporter : BugReporter {
        protected override void SetUserImpl(string uid) {
            App.Current.RaygunClient.User = uid;
        }

        protected override void SendImpl(Exception exception, IList<string> tags, IDictionary customUserData) {
			RaygunClient.Current.Send(exception, tags, customUserData);
		}
	}
}
