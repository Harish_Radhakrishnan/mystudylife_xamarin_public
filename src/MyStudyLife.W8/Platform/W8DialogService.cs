﻿using System;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.W8.Platform {
	// Idea for ShowAsyncQueue taken from http://stackoverflow.com/questions/14488587/how-to-allow-for-multiple-popups-at-once-in-winrt

	public class W8DialogService : DialogServiceBase, IDialogService {
		private static TaskCompletionSource<MessageDialog> _currentDialogShowRequest;

	    private CoreDispatcher Dispatcher {
	        get { return Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher; }
	    }

		/// <summary>
		/// Begins an asynchronous operation showing a dialog.
		/// If another dialog is already shown using
		/// ShowAsyncQueue or ShowAsyncIfPossible method - it will wait
		/// for that previous dialog to be dismissed before showing the new one.
		/// </summary>
		/// <param name="dialog">The dialog.</param>
		/// <returns></returns>
		/// <exception cref="System.InvalidOperationException">This method can only be invoked from UI thread.</exception>
		private static async Task ShowAsyncQueue(MessageDialog dialog) {
			if (!Window.Current.Dispatcher.HasThreadAccess) {
				throw new InvalidOperationException("This method can only be invoked from UI thread.");
			}

			while (_currentDialogShowRequest != null) {
				await _currentDialogShowRequest.Task;
			}

			var request = _currentDialogShowRequest = new TaskCompletionSource<MessageDialog>();
			await dialog.ShowAsync();
			_currentDialogShowRequest = null;
			request.SetResult(dialog);
		}
        
	    protected override async void RequestConfirmationImpl(string title, string message, string cancelText, string confirmText, Action<bool> onActioned) {
	        await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
	            var dialog = new MessageDialog(message ?? String.Empty, title);

	            dialog.Commands.Add(new UICommand(confirmText, ignored => onActioned(true)));
	            dialog.Commands.Add(new UICommand(cancelText, ignored => onActioned(false)));

	            await ShowAsyncQueue(dialog);
	        });
	    }

	    public async void ShowDismissible(string title, string message) {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
                var dialog = new MessageDialog(message ?? String.Empty, title);

                dialog.Commands.Add(new UICommand(R.Common.Okay));

				await ShowAsyncQueue(dialog);
            });
        }

        public async void ShowNotification(string title, string message) {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
                var notifier = ToastNotificationManager.CreateToastNotifier();

                var template = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);

                var textElements = template.GetElementsByTagName("text");

                textElements[0].InnerText = title;
                textElements[1].InnerText = message;

                notifier.Show(new ToastNotification(template));
            });
        }

		public async void ShowCustom(string title, string message, CustomDialogAction leftAction, CustomDialogAction rightAction) {
			await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
				var dialog = new MessageDialog(message ?? String.Empty, title);

				if (leftAction != null) {
					dialog.Commands.Add(new UICommand(leftAction.Content, command => leftAction.TryTriggerOnChosen()));
				}

				if (rightAction != null) {
					dialog.Commands.Add(new UICommand(rightAction.Content, command => rightAction.TryTriggerOnChosen()));
				}

				await ShowAsyncQueue(dialog);
			});
	    }

	    public async Task<CustomDialogResult> ShowCustomAsync(string title, string message, string positiveAction, string negativeAction) {
            var tcs = new TaskCompletionSource<CustomDialogResult>();

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => {
                var dialog = new MessageDialog(message ?? String.Empty, title);

                dialog.Commands.Add(new UICommand(positiveAction, ignored => tcs.TrySetResult(new CustomDialogResult(true))));
                dialog.Commands.Add(new UICommand(negativeAction, ignored => tcs.TrySetResult(new CustomDialogResult(false))));

                await ShowAsyncQueue(dialog);
            });

            return await tcs.Task;
	    }
    }
}
