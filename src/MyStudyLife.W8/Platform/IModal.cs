﻿using System;
using Windows.UI.Xaml;
using Cirrious.MvvmCross.ViewModels;

namespace MyStudyLife.W8.Platform {
    public interface IModal {
        IMvxViewModel ViewModel { get; set; }
    }

    public interface IModal<TViewModel> : IModal where TViewModel : IMvxViewModel {
        new TViewModel ViewModel { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class ModalAttribute : Attribute {
        private HorizontalAlignment? _horizontalAlignment;
        private VerticalAlignment? _verticalAlignment;

        public ModalBackdrop Backdrop { get; set; }

        public HorizontalAlignment HorizontalAlignment {
            get { return _horizontalAlignment.Value; }
            set { _horizontalAlignment = value; }
        }

        public VerticalAlignment VerticalAlignment {
            get { return _verticalAlignment.Value; }
            set { _verticalAlignment = value; }
        }

        public bool OverridesHorizontalAlignment {
            get { return _horizontalAlignment.HasValue; }
        }

        public bool OverridesVerticalAlignment {
            get { return _horizontalAlignment.HasValue; }
        }

        public ModalAttribute() {
            this.Backdrop = ModalBackdrop.Dismissible;
        }
    }

    public enum ModalBackdrop {
        /// <summary>
        ///     No backdrop will be shown.
        /// </summary>
        None,
        /// <summary>
        ///     A static backdrop that does
        ///     not auto dismiss the modal on
        ///     click will be shown.
        /// </summary>
        Static,
        /// <summary>
        ///     Like <see cref="Static"/> but
        ///     invisible.
        /// </summary>
        StaticInvisible,
        /// <summary>
        ///     A backdrop will be shown that
        ///     dismisses the modal when tapped.
        /// </summary>
        Dismissible
    }
}
