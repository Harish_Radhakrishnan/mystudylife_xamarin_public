﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsCommon.Views;
using GoogleAnalytics.Core;
using Microsoft.VisualBasic.CompilerServices;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.W8.Controls;
using MyStudyLife.W8.Controls.Settings;
using MyStudyLife.W8.Services;
using MyStudyLife.W8.Views;

namespace MyStudyLife.W8.Platform {
    public interface ICustomPresenter {
        void MapSplitView<TEntity, TViewModel, TSplitView>()
            where TEntity : SubjectDependentEntity
            where TViewModel : EntityViewModel<TEntity>
            where TSplitView : MvxWindowsPage, ISplitView<TEntity>;

        void AddModal<TViewModel, TModal>()
            where TViewModel : IMvxViewModel
            where TModal : IModal<TViewModel>;

        void AddHandler<TViewModel, THandler>(Func<THandler> getHandler)
            where TViewModel : IMvxViewModel
            where THandler : IHandleRequest, IHandleClose;

        void AddRequestHandler<TViewModel>(Func<IHandleRequest> getRequestHandler) where TViewModel : IMvxViewModel;

        void AddCloseHandler<TViewModel>(Func<IHandleClose> getCloseHandler) where TViewModel : IMvxViewModel;
    }

    public class W8Presenter : MvxWindowsViewPresenter, ICustomPresenter, IMvxViewPresenterWithDelegate {
	    private readonly Frame _rootFrame;

        #region IMvxViewPresenterWithDelegate

        private readonly MvxViewPresenterDelegate _delegate = new MvxViewPresenterDelegate();

        public MvxViewPresenterDelegate Delegate {
            get { return _delegate; }
        }

        #endregion

        private readonly Dictionary<Type, Type> _modalViewModelMappings = new Dictionary<Type, Type>();
        private readonly Dictionary<Type, Type> _splitViewMappings = new Dictionary<Type, Type>();

        private readonly Dictionary<Type, Func<IHandleRequest>> _requestHandlers = new Dictionary<Type, Func<IHandleRequest>>();
        private readonly Dictionary<Type, Func<IHandleClose>> _closeHandlers = new Dictionary<Type, Func<IHandleClose>>();

        private readonly NavigationBackStack _backStack;

	    public W8Presenter(IMvxWindowsFrame rootFrame) : base(rootFrame) {
		    _rootFrame = (Frame)rootFrame.UnderlyingControl;

            _rootFrame.Navigating += RootFrameOnNavigating;

	        _backStack = Mvx.Resolve<INavigationService>().BackStack;
            
	        #region Unused code

	        // This took 43ms to execute on WHITEBEAST (a powerful PC)
	        // with just 3 registered modals so to improve startup time
	        // we're going to map modals by hand.

	        //_modalViewModelMappings = (
	        //    from t in typeof(IModal<>).GetTypeInfo().Assembly.ExceptionSafeGetTypes()
	        //    where (
	        //        t.GetTypeInfo().ImplementedInterfaces
	        //            .Select(i => i.GetTypeInfo())
	        //            .Any(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IModal<>))
	        //        )
	        //    select new {
	        //        Key = t.GetRuntimeProperties().Single(x =>
	        //            x.Name == "ViewModel" && x.DeclaringType == t
	        //        ).PropertyType,
	        //        Value = t
	        //    }
	        //).ToDictionary(k => k.Key, v => v.Value);

	        #endregion

            this.AddPresentationHintHandler<WelcomeWizardCompletedPresentationHint>(hint => {
                Type modalViewType;

                if (AppShell.Current.IsShowingModal && _modalViewModelMappings.TryGetValue(typeof(WelcomeWizardViewModel), out modalViewType)) {
                    AppShell.Current.CloseModal(modalViewType);
	            }

                var transaction = _backStack.BeginTransaction();

                transaction.Push(typeof(DashboardViewModel));

                if (hint.AddClasses) {
                    this.Show(MvxViewModelRequest<ScheduleViewModel>.GetDefaultRequest());
                }
                else {
                    transaction.Commit();
                }
	        });
            this.AddPresentationHintHandler<SubjectDeletedPresentationHint>(hint => {
                AppShell.Current.CloseModal(typeof(SubjectInputFlyout));
                _backStack.Pop();
            });
            this.AddPresentationHintHandler<UpgradeCompletePresentationHint>(hint => {
                if (_rootFrame.CanGoBack) {
                    _rootFrame.GoBack();
                }

                UpgradeViewModel.AfterPresentationHintHandledAsync(hint);
            });
            this.AddPresentationHintHandler<AuthorizationChangedPresentationHint>(hint => {
                if (hint.ChangeCause == AuthorizationChangeCause.SignIn) {
                    return;
                }

                while (_rootFrame.BackStackDepth > 0 && !(_rootFrame.Content is SignInView)) {
                    _rootFrame.GoBack();
                }

                AccountPane.TryGetInstanceAndHandleClose();
                LiveTileService.ClearTiles();

                _backStack.Clear();

                this.Show(new MvxViewModelRequest {
                    ViewModelType = typeof(SignInViewModel),
                    RequestedBy = new MvxRequestedBy(MvxRequestedByType.UserAction)
                });
            });
	    }
        
        private void RootFrameOnNavigating(object sender, NavigatingCancelEventArgs e) {
            if (e.NavigationMode == NavigationMode.Back && !_navigating) {
                var view = _rootFrame.Content as MvxWindowsPage;

                if (view != null && view.ViewModel != null) {
                    var viewType = view.GetType();
                    var viewModelType = view.ViewModel.GetType();

                    var topBackStackEntry = _backStack.Peek();
                    
                    if (view is ISplitView && topBackStackEntry.ViewModelType != viewModelType) {
                        _splitViewMappings.ContainsValue(view.GetType());

                        if (_splitViewMappings.Any(mapping => mapping.Value == viewType && mapping.Key == topBackStackEntry.ViewModelType)) {
                            _backStack.Pop(2);
                            return;
                        }
                    }
                    else {
                        _backStack.Pop(viewModelType);
                        return;
                    }
                }
                   
                _backStack.Pop();
            }
        }

        public override void Show(MvxViewModelRequest request) {
            Func<IHandleRequest> requestHandler;

            // Make sure the dashboard is loaded behind the welcome wizard modal
	        if (request.ViewModelType == typeof (WelcomeWizardViewModel)) {
                // By calling base, this doesn't get added to the back stack (which does
                // also mean it doesn't get a back stack entry in the view model, but it's not currently
                // used).
	            base.Show(new MvxViewModelRequest(typeof (DashboardViewModel), null, null, null));
	        }

            if (_requestHandlers.TryGetValue(request.ViewModelType, out requestHandler)) {
                requestHandler().HandleRequest(request);

                return;
            }

            Type splitViewType;
            Type modalViewType;
            ISplitView splitView;
            MvxWindowsPage splitViewAsPage;

            if (
                (splitView = _rootFrame.Content as ISplitView) != null &&
                _splitViewMappings.TryGetValue(request.ViewModelType, out splitViewType) &&
                splitViewType == splitView.GetType() &&
                
                // This ensures that only requests that originate from the split view's viewmodel
                // are displayed in the split view.
                (splitViewAsPage = _rootFrame.Content as MvxWindowsPage) != null &&
                splitViewAsPage.DataContext != null &&
                splitViewAsPage.DataContext.GetType().FullName == request.RequestedBy.AdditionalInfo
            ) {

                if (!_modalViewModelMappings.TryGetValue(request.ViewModelType, out modalViewType)) {
                    throw new Exception(String.Format(
                        "Could not find modal for view model type '{0}'.",
                        request.ViewModelType.Name
                    ));
                }

                var modal = (IModal)Activator.CreateInstance(modalViewType);

                _backStack.Push(request.ViewModelType, NavigationBackStackPushMode.ReplaceOnSamePush);

                modal.ViewModel = Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(request, null);

                splitView.ModalHost.Modal = modal;

                return;
            }

            if (_modalViewModelMappings.TryGetValue(request.ViewModelType, out modalViewType)) {
                _backStack.Push(request.ViewModelType);
                AppShell.Current.ShowModal(modalViewType, request);
                return;
            }

            if (request.ViewModelType.Namespace == "MyStudyLife.UI.ViewModels.Settings") {
                var transaction = _backStack.BeginTransaction();
                if (_backStack.Peek().ViewModelType.Namespace == "MyStudyLife.UI.ViewModels.Settings") {
                    transaction.Pop();
                }
                transaction.Push(request.ViewModelType);
                transaction.Commit();
                App.Current.HandleSettingsRequest(request);
                return;
            }

            PushToBackStackInTransaction(() => base.Show(request), request.ViewModelType);
        }

        private void PushToBackStackInTransaction(Action action, Type viewModelType) {
            var transaction = _backStack.CurrentTransaction ?? _backStack.BeginTransaction();
            action();
            transaction.Push(viewModelType);
            transaction.Commit();
        }

        public override void ChangePresentation(MvxPresentationHint hint) {
            if (!Delegate.HandleChangePresentation(hint)) {
                base.ChangePresentation(hint);
            }
        }

        private bool _navigating;

        public override void Close(IMvxViewModel viewModel) {
            var viewModelType = viewModel.GetType();

            Type modalViewType;

            if (AppShell.Current.IsShowingModal && _modalViewModelMappings.TryGetValue(viewModelType, out modalViewType)) {
                AppShell.Current.CloseModal(modalViewType);
                goto popFromBackStack;
            }

            Type splitViewType;
            ISplitView splitView;

            if (
                (splitView = _rootFrame.Content as ISplitView) != null &&
                _splitViewMappings.TryGetValue(viewModelType, out splitViewType) &&
                splitViewType == splitView.GetType()
            ) {

                if (!_modalViewModelMappings.TryGetValue(viewModelType, out modalViewType)) {
                    throw new Exception(String.Format(
                        "Could not find modal for view model type '{0}'.",
                        viewModelType.Name
                    ));
                }

                splitView.ModalHost.Modal = null;

                goto popFromBackStack;
            }

            Func<IHandleClose> getCloseHandler;

            if (_closeHandlers.TryGetValue(viewModel.GetType(), out getCloseHandler)) {
                getCloseHandler().HandleClose(viewModel);

                goto popFromBackStack;
            }

            try {
                _navigating = true;
                base.Close(viewModel);
            }
            finally {
                _navigating = false;
            }

        popFromBackStack:
            _backStack.Pop(viewModelType);
        }

        #region ICustomPresenter

        /// <summary>
        ///     Maps a split view.
        /// </summary>
        /// <typeparam name="TEntity">
        ///     The entity type displayed in the split view.
        /// </typeparam>
        /// <typeparam name="TViewModel">
        ///     The view model used to display a single entity.
        /// </typeparam>
        /// <typeparam name="TSplitView">
        ///     The view used to display the list of and a single
        ///     entity.
        /// </typeparam>
        public void MapSplitView<TEntity, TViewModel, TSplitView>()
            where TEntity : SubjectDependentEntity
            where TViewModel : EntityViewModel<TEntity>
            where TSplitView : MvxWindowsPage, ISplitView<TEntity> {

            _splitViewMappings[typeof (TViewModel)] = typeof (TSplitView);
        }

        /// <summary>
        ///     Adds a modal 
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <typeparam name="TModal"></typeparam>
        public void AddModal<TViewModel, TModal>()
            where TViewModel : IMvxViewModel
            where TModal : IModal<TViewModel> {

            _modalViewModelMappings[typeof (TViewModel)] = typeof (TModal);
        }

        public void AddHandler<TViewModel, THandler>(Func<THandler> getHandler)
            where TViewModel : IMvxViewModel
            where THandler : IHandleRequest, IHandleClose {

            var tViewModel = typeof (TViewModel);

            _requestHandlers.Add(tViewModel, () => getHandler());
            _closeHandlers.Add(tViewModel, () => getHandler());
        }

        public void AddRequestHandler<TViewModel>(Func<IHandleRequest> getRequestHandler)
            where TViewModel : IMvxViewModel {
            
            _requestHandlers.Add(typeof (TViewModel), getRequestHandler);
        }

        public void AddCloseHandler<TViewModel>(Func<IHandleClose> getCloseHandler)
            where TViewModel : IMvxViewModel {

            _closeHandlers.Add(typeof (TViewModel), getCloseHandler);
        }

        #endregion
    }
}
