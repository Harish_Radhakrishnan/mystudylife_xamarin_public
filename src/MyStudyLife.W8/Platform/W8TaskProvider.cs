﻿using System;
using Windows.ApplicationModel;
using Windows.System;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.W8.Platform {
    public class W8TaskProvider : ITaskProvider {
        public async void ShowEmailComposer(string subject, string body, string to) {
            var uri = new Uri(String.Format(
                    "mailto:?to={0}&subject={1}&body={2}",
                    Uri.EscapeDataString(to),
                    Uri.EscapeDataString(subject),
                    Uri.EscapeDataString(body)
                )
            );

            await Launcher.LaunchUriAsync(uri);
        }

	    public async void ReviewApp() {
		    var uri = new Uri(
				String.Concat(
					"ms-windows-store:review?pfn=",
					Package.Current.Id.FamilyName
				)
			);

		    await Launcher.LaunchUriAsync(uri);
	    }

        public bool TryShowNativeUri(Uri uri) {
            return false;
        }

        public async void ShowWebUri(Uri uri) {
            await Launcher.LaunchUriAsync(uri);
        }
    }
}
