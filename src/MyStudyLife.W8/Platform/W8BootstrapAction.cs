﻿using System.Threading.Tasks;
using MyStudyLife.Data.Settings;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.W8.Services;

namespace MyStudyLife.W8.Platform {
    public class W8BootstrapAction : IBootstrapAction {
        public async Task RunAsync() {
            await BackgroundTaskRegistrationService.EnsureLiveTileTaskRegistered();

            var deviceSettings = await new SettingsRepository().GetDeviceSettingsAsync();

            if (deviceSettings.RemindersEnabled) {
                await BackgroundTaskRegistrationService.EnsureRemindersTaskRegistered();
            }
        }
    }
}
