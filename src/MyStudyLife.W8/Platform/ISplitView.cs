﻿using MyStudyLife.W8.Controls;

namespace MyStudyLife.W8.Platform {
    public interface ISplitView {
        ModalHost ModalHost { get; }
    }

    public interface ISplitView<TEntity> : ISplitView { }
}
