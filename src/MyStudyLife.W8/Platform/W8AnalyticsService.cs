﻿using GoogleAnalytics.Core;
using MyStudyLife.Configuration;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;

namespace MyStudyLife.W8.Platform {
    public class W8AnalyticsService : AnalyticsServiceBase {
        private readonly Tracker _tracker;

        public W8AnalyticsService(IMslConfig config, INavigationService navigationService) : base(navigationService) {
            GAServiceManager.Current.DispatchPeriod = DispatchInterval;

            _tracker = GoogleAnalytics.AnalyticsEngine.Current.GetTracker(PropertyId);
            
            _tracker.AppId = "mystudylife.w8";
            _tracker.AppName = "My Study Life for Windows 8";
            _tracker.AppVersion = "w8-" + config.AppVersion.ToString(3); 
        }

        protected override void SetUserIdImpl(string userId) {
            _tracker.UserId = userId;
        }

        protected override void TrackScreenImpl(string screenName) {
            _tracker.SendView(screenName);
        }

        protected override void TrackEventImpl(string category, string action, string label) {
            _tracker.SendEvent(category, action, label, 0); // TODO: What's value used for?
        }

        protected override void NotifySessionEndImpl() {
            _tracker.SetEndSession(true);
        }
    }
}
