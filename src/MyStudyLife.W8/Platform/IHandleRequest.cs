﻿using Cirrious.MvvmCross.ViewModels;

namespace MyStudyLife.W8.Platform {
    public interface IHandleRequest {

        void HandleRequest(MvxViewModelRequest request);
    }
}
