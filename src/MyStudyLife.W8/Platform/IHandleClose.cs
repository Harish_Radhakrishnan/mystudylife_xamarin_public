﻿using Cirrious.MvvmCross.ViewModels;

namespace MyStudyLife.W8.Platform {
	public interface IHandleClose {
		void HandleClose(IMvxViewModel viewModel);
	}
}
