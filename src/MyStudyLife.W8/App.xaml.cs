﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Mindscape.Raygun4Net;
using MyStudyLife.UI;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using MyStudyLife.W8.Common;
using System.Globalization;

namespace MyStudyLife.W8 {
    /// <summary>
    ///		Provides application-specific behavior to
    ///		supplement the default Application class.
    /// </summary>
    sealed partial class App {
        public static new App Current { get; private set; }

		public readonly RaygunClient RaygunClient = new RaygunClient("YNoReoZzOnuhmKM1/LSsUg==");

        public TypedResourceDictionary MslResources { get; private set; }

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App() {
            RaygunClient.Attach("YNoReoZzOnuhmKM1/LSsUg==");
            
            this.InitializeComponent();

			Current = this;

            // TODO: If we translate the app, this would be bad as it also changes CurrentUICulture
            // ...but it's a great quick fix for now!
            https://www.pedrolamas.com/2015/11/02/cultureinfo-changes-in-uwp/
            try {
                Windows.Globalization.ApplicationLanguages.PrimaryLanguageOverride = new Windows.Globalization.DateTimeFormatting.DateTimeFormatter("longdate", new[] { "US" }).ResolvedLanguage;
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }
        }

	    #region Bootsrapper

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args) {
            base.OnLaunched(args);

            EnsureMainPageActivatedAsync(args);
        }

        public void EnsureMainPageActivatedAsync(IActivatedEventArgs args) {
            // Do not repeat app initialization when already running, just ensure that
            // the window is active
            if (args.PreviousExecutionState != ApplicationExecutionState.Running) {
                var appShell = Window.Current.Content as AppShell;

                // Do not repeat app initialization when the Window already has content,
                // just ensure that the window is active
                if (appShell == null) {
                    // Create a Frame to act as the navigation context and navigate to the first page
                    appShell = new AppShell(args);

                    if (args.PreviousExecutionState == ApplicationExecutionState.Terminated) {
                        //TODO: Load state from previously suspended application
                    }

                    // Place the frame in the current Window
                    Window.Current.Content = appShell;
                }

                if (appShell.ViewFrame.Content == null) {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter

                    var setup = new W8Setup(appShell);
                    setup.Initialize();

                    var start = Mvx.Resolve<IMvxAppStart>();

	                var appStartMode = args.PreviousExecutionState.In(
                        ApplicationExecutionState.NotRunning,
		                ApplicationExecutionState.Terminated, ApplicationExecutionState.ClosedByUser
                    ) ? AppStartMode.New : AppStartMode.Resume;

	                start.Start(args.Kind == ActivationKind.Search
		                ? new SearchQueryHint(appStartMode, ((SearchActivatedEventArgs) args).QueryText)
		                : new AppStartHint(appStartMode));
                }
            }

            Window.Current.Activate();

            this.RegisterAppSettings(); // Simply attaches an event handler, not time consuming.

            this.MslResources = new TypedResourceDictionary(Resources);
        }
        
        #endregion
    }
}