﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MyStudyLife.Data;
using Windows.Storage;
using Newtonsoft.Json;
using System.IO;
using Cirrious.CrossCore.Platform;
using Cirrious.CrossCore.Exceptions;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.CrossCore.WindowsCommon.Platform;

namespace MyStudyLife.W8 {
    public class W8StorageProvider : IStorageProvider {
        public void AddOrUpdateSetting<T>(string settingName, T settingValue) {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            // Removes setting if it exists.
            RemoveSetting(settingName);

// ReSharper disable once CompareNonConstrainedGenericWithNull
            localSettings.Values.Add(settingName,
                typeof (T) == typeof (string)
                    ? (settingValue != null ? settingValue.ToString() : null)
                    : JsonConvert.SerializeObject(settingValue)
            );
        }

        public T GetSetting<T>(string settingName) {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            var setting = localSettings.Values.FirstOrDefault(ls => ls.Key == settingName);

            if (setting.Key != null && setting.Value != null) {
                if (typeof(T) == typeof(string)) {
                    return (T) setting.Value;
                }

                if (setting.Value.ToString() != "null") {
                    return JsonConvert.DeserializeObject<T>(setting.Value.ToString());
                }
            }

            return default(T);
        }

        public void RemoveSetting(string settingName) {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            if (localSettings.Values.Any(ls => ls.Key == settingName))
                localSettings.Values.Remove(settingName);
        }

        public async Task<string> LoadFileContentsAsync(string fileName) {
            StorageFile storageFile = await GetFileAsync(fileName);

            return await FileIO.ReadTextAsync(storageFile);
        }

        public async Task<Stream> FileOpenReadAsync(string fileName) {
            StorageFile storageFile = await GetFileAsync(fileName);

            return (await storageFile.OpenReadAsync().AsTask()).AsStreamForRead();
        }

        public async Task<string> LoadResourceFileContentsAsync(string resourceUrl) {
            resourceUrl = String.Concat("ms-appx:///", resourceUrl);

            Debug.WriteLine("Retrieving resource file from path '{0}'", resourceUrl);

            Uri resourceUri = new Uri(resourceUrl, UriKind.Absolute);

            StorageFile storageFile = await StorageFile.GetFileFromApplicationUriAsync(resourceUri);
            
            return await FileIO.ReadTextAsync(storageFile);
        }

        public async System.Threading.Tasks.Task WriteFileContentsAsync(string fileName, string contents) {
            StorageFile storageFile = await GetFileAsync(fileName);

            await FileIO.WriteTextAsync(storageFile, contents);
        }

        public async System.Threading.Tasks.Task RemoveFilesAsync() {
            var toDelete = (await LocalFolder.GetItemsAsync()).Where(x => !x.Name.Contains(".sqlite"));

            foreach (var item in toDelete)
                await item.DeleteAsync(StorageDeleteOption.PermanentDelete);
        }

        #region Privates Methods

        /// <summary>
        /// The folder for My Study Life data storage.
        /// </summary>
        private static StorageFolder LocalFolder => ApplicationData.Current.LocalFolder;

        /// <summary>
        /// Returns the file in isolated storage with the given name, creates it if it does not already exist.
        /// </summary>
        /// <param name="fileName">The name of the file (including extension) to retreive.</param>
        [DebuggerStepThrough]
        private async Task<StorageFile> GetFileAsync(string fileName) {
            return await LocalFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);
        }

        public DateTimeOffset GetLastModified(string path) {
            var file = StorageFile.GetFileFromPathAsync(Path.Combine(LocalFolder.Path, path)).Await();

            return file.GetBasicPropertiesAsync().Await().DateModified;
        }

        #endregion
    }
}
