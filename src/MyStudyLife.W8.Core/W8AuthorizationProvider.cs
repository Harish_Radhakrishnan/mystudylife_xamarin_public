﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Windows.Security.Authentication.Web;
using Windows.System.Profile;
using Windows.Devices.Enumeration.Pnp;
using Cirrious.CrossCore;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.W8 {

    public class W8AuthorizationProvider : IAuthorizationProvider {

        #region Device Information

        public async Task<Device> GetDeviceInformationAsync() {
            var device = new Device();

            // The guid the current device tends to be.
            const string systemGuid = "{00000000-0000-0000-FFFF-FFFFFFFFFFFF}";

            string[] properties = { "System.ItemNameDisplay", "System.Devices.Manufacturer", "System.Devices.ModelName" };

            try {
                var container =
                    (await PnpObject.FindAllAsync(PnpObjectType.DeviceContainer, properties)).FirstOrDefault(
                        p => p.Id == systemGuid);

                if (container != null && container.Properties != null) {
                    object name, manufacturer, model;

                    container.Properties.TryGetValue("System.ItemNameDisplay", out name);
                    container.Properties.TryGetValue("System.Devices.Manufacturer", out manufacturer);
                    container.Properties.TryGetValue("System.Devices.ModelName", out model);

                    if (name != null) {
                        device.Name = name.ToString();
                    }
                    if (manufacturer != null) {
                        device.Manufacturer = manufacturer.ToString();
                    }
                    if (model != null) {
                        device.Model = model.ToString();
                    }
                }

                device.HardwareId = this.GetHardwareId();
            }
            catch (Exception ex) {
                // I think this is the cause of some bugs, added better null checking above
                // which should hopefully fix it... for everything else there's the below
                // (async exception handling is shit).
                Mvx.Resolve<IBugReporter>().Send(ex);

                throw;
            }

            return device;
        }

        private string GetHardwareId() {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(hardwareId);

            byte[] bytes = new byte[hardwareId.Length];
            dataReader.ReadBytes(bytes);

            return BitConverter.ToString(bytes);
        }

        #endregion

        #region OAuth

        public async Task<OAuthAuthorizationResult> PerformOAuthAuthorizationAsync(AuthenticationProvider provider) {
            string redirectUri = "http://localhost";

            var authResult = await WebAuthenticationBroker.AuthenticateAsync(WebAuthenticationOptions.None,
                new Uri(Mvx.Resolve<IMslConfig>().GetOAuthStartUri(provider)),
                new Uri(redirectUri)
            );

            if (authResult.ResponseStatus != WebAuthenticationStatus.Success) {
                return new OAuthAuthorizationResult(
                    authResult.ResponseStatus == WebAuthenticationStatus.UserCancel
                        ? OAuthAuthorizationStatus.UserCancel
                        : OAuthAuthorizationStatus.ErrorHttp, authResult.ResponseErrorDetail.ToString());
            }

            var url = new Uri(authResult.ResponseData, UriKind.Absolute);
            
            return OAuthAuthorizationResult.FromUri(url);
        }

        #endregion
    }
}
