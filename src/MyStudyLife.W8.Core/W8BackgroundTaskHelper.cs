﻿using Cirrious.CrossCore;
using Cirrious.MvvmCross.Community.Plugins.Sqlite.WindowsStore;
using MyStudyLife.Versioning;

namespace MyStudyLife.W8 {
    public static class W8BackgroundTaskHelper {
        public static bool CheckAndInitializeIoC() {
            return BackgroundTaskHelper.CheckAndInitializeIoC<W8Config, MvxStoreSQLiteConnectionFactory, W8StorageProvider, W8ReminderScheduler>(
                Mvx.LazyConstructAndRegisterSingleton<IUpgradeService, W8UpgradeService>
            );
        }
    }
}
