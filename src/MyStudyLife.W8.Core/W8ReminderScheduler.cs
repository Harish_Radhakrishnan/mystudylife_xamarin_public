﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Windows.UI.Notifications;
using MyStudyLife.Reminders;
using MyStudyLife.W8.Common;

namespace MyStudyLife.W8 {
    public sealed class W8ReminderScheduler : IReminderScheduler {
        private ToastNotifier _notifier;

        private ToastNotifier Notifier {
            get { return _notifier ?? (_notifier = ToastNotificationManager.CreateToastNotifier()); }
        }

        public bool ScheduleReminder(Reminder reminder) {
            if (Notifier.Setting != NotificationSetting.Enabled) return false;

            XDocument xToast = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02).ToXDocument();
            XElement xBinding = xToast.Root.Element("visual").Element("binding");

            // TODO: Add icons
            //XElement xImage = xBinding.Element("image");
            //xImage.SetAttributeValue("src", String.Format("ms-appx:///Assets/Icons/{0}.png", entry.BaseType));
            //xImage.SetAttributeValue("alt", entry.BaseType);

            IEnumerable<XElement> xText = xBinding.Elements("text").ToList();

            XElement xTextTitle = xText.ElementAt(0);
            XElement xTextBody = xText.ElementAt(1);

            xTextTitle.SetValue(reminder.Title);
            xTextBody.SetValue(reminder.Message ?? String.Empty);

            Notifier.AddToSchedule(new ScheduledToastNotification(xToast.ToXmlDocument(), reminder.Time));

            return true;
        }

        public void ClearReminders() {
            var notifications = Notifier.GetScheduledToastNotifications().ToList();

            foreach (ScheduledToastNotification notification in notifications) {
                Notifier.RemoveFromSchedule(notification);
            }
        }
    }
}
