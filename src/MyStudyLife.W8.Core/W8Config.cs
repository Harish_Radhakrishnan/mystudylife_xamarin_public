﻿using System;
using Windows.ApplicationModel;
using MyStudyLife.Configuration;

namespace MyStudyLife.W8 {
    public sealed class W8Config : MslConfig {
        private readonly Lazy<Version> _appVersion = new Lazy<Version>(() => {
            var v = Package.Current.Id.Version;

            return new Version(v.Major, v.Minor, v.Build, v.Revision);
        });

        public override Version AppVersion => _appVersion.Value;

        protected override string ApiClientId => "kAInUvhwWb9tZ8J5e2Ef9tO4GMYvsuMz";
    }
}