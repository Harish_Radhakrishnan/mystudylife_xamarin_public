﻿using System;
using System.Collections.Generic;
using MyStudyLife.Net;
using MyStudyLife.Services;
using Newtonsoft.Json;
using Windows.Networking.PushNotifications;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Exceptions;
using Cirrious.CrossCore.Platform;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.W8.Services {
    public class W8PushNotificationService : PushNotificationServiceBase, IPushNotificationService {
        private readonly INetworkProvider _networkProvider;
        private readonly IHttpApiClient _apiClient;

        private Windows.Networking.PushNotifications.PushNotificationChannel _channel;

        public W8PushNotificationService(INetworkProvider networkProvider, IHttpApiClient apiClient) {
            this._networkProvider = networkProvider;
            this._apiClient = apiClient;
		}

        /// <summary>
        /// Ensures there is an active notification channel
        /// for the current user.
        /// </summary>
        public async Task EnsureNotificationChannelAsync() {
            try {
                // We don't care about metered mode.
                if (!_networkProvider.IsOnline) return;

                if (_channel == null) {
                    Trace(MvxTraceLevel.Diagnostic, "Ensuring active notification channel");

                    var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
                    
                    await _apiClient.PostJsonAsync("push/wns/", JsonConvert.SerializeObject(new {
                        uri = channel.Uri
                    }));

                    // Only store channel if it is successfully sent.
                    _channel = channel;
                }

                if (_channel == null) return;

                _channel.PushNotificationReceived += ChannelOnNotificationReceived;
            }
            catch (Exception ex) {
                Mvx.TaggedError("PNS", "Failed to open notification channel: " + ex.ToLongString());
            }
        }

        public Task CloseNotificationChannelAsync() {
            _channel?.Close();
            return TaskEx.NoOp;
        }

        protected void ChannelOnNotificationReceived(object sender, PushNotificationReceivedEventArgs e) {
            if (e.RawNotification != null && e.NotificationType == PushNotificationType.Raw) {
                HandleNotification(e.RawNotification);
            }
        }

        public void HandleNotification(RawNotification notification) {
            var contentBytes = Convert.FromBase64String(notification.Content);
            var content = System.Text.Encoding.UTF8.GetString(contentBytes, 0, contentBytes.Length);

            // WARNING: If we send nested objects in the notification JSON, this won't work!
            HandleRawNotification(JsonConvert.DeserializeObject<Dictionary<string, object>>(content));
        }
    }
}
