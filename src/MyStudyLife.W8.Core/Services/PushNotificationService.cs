﻿using System;
using System.Diagnostics;
using MyStudyLife.Data;
using MyStudyLife.Data.Store;
using MyStudyLife.Net;
using MyStudyLife.Services;
using MyStudyLife.Sync;
using Newtonsoft.Json;
using Windows.Networking.PushNotifications;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.W8.Services {
    public class PushNotificationService : IPushNotificationService {
        #region Instance

        private readonly IStorageProvider _storageProvider;
        private readonly INetworkProvider _networkProvider;
        private readonly IHttpApiClient _apiClient;
        private readonly ISyncService _syncService;

		public PushNotificationService(IStorageProvider storageProvider, INetworkProvider networkProvider, IHttpApiClient apiClient) {
            this._storageProvider = storageProvider;
            this._networkProvider = networkProvider;
            this._apiClient = apiClient;
		}

        public PushNotificationService(
			IStorageProvider storageProvider,
            INetworkProvider networkProvider,
            IHttpApiClient apiClient,
			ISyncService syncService
		) : this(storageProvider, networkProvider, apiClient) {

            this._syncService = syncService;
        }

        #endregion

        #region Channels

        public PushNotificationChannel CurrentChannel { get; private set; }

        /// <summary>
        /// Ensures there is an active notification channel
        /// for the current user.
        /// </summary>
        public async void EnsureNotificationChannelAsync() {
            try {
                // We don't care about metered mode.
                if (!_networkProvider.IsOnline)
                    return;

                if (CurrentChannel == null) {
                    Debug.WriteLine("PushNotificationService: Ensuring an active channel.");

                    var channel =
                        await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

                    await _apiClient.PostJsonAsync("push/wns/", JsonConvert.SerializeObject(new {
                        uri = channel.Uri
                    }));

                    // Only store channel if it is successfully sent.
                    CurrentChannel = channel;
                }

                // Check again incase creating a channel failed.
                if (CurrentChannel != null) {
                    CurrentChannel.PushNotificationReceived += async (sender, args) => {
                            if (args.RawNotification != null)
                                await ProcessRawNotificationAsync(args.RawNotification.Content, this._syncService);
                        };
                }
            }
            catch (Exception ex) {
                // This isn't essential so suppress all errors.
                Debug.WriteLine("PushNotificationService: An {0} occured whilst ensuring an active channel: {1}.", ex, ex.Message);
            }
        }

        /// <summary>
        ///     Closes the current notification channel.
        /// 
        ///     Should be called before the user logs out. When a user signs out
        ///     all channels will automatically be closed from the signout auth method.
        /// </summary>
        public void CloseNotificationChannelAsync() {
            throw new NotImplementedException();
        }

        #endregion

        #region Notification Received

        public async Task ProcessRawNotificationAsync(string rawContent) {
            await ProcessRawNotificationAsync(rawContent, null);
        }

        /// <summary>
        ///     The handler triggered when a raw push
        ///     notification indiciating sync is required is
        ///     received.
        /// </summary>
        /// <param name="rawContent">The raw content returned.</param>
        /// <param name="syncService">
        ///     The current sync service.
        /// 
        ///     This enables us to use the app's sync service if running or create
        ///     a new if it's triggered in a background task.
        /// </param>
#pragma warning disable 1998
        public async Task ProcessRawNotificationAsync(string rawContent, ISyncService syncService) {
#pragma warning restore 1998
            var rawBytes = Convert.FromBase64String(rawContent);

            string jsonContent = System.Text.Encoding.UTF8.GetString(rawBytes, 0, rawBytes.Length);

            Debug.WriteLine("PushNotificationService: {0} received from WNS{1}", jsonContent, syncService == null ? " via background task." : String.Empty);

            // Don't bother checking if we're online, we've just received a push notification so we obviously are.
            if (syncService == null) {
                syncService = new SyncService(_storageProvider, _networkProvider, _apiClient);
            }

            syncService.TrySync(SyncTriggerKind.Programmatic);
        }

        #endregion
    }
}
