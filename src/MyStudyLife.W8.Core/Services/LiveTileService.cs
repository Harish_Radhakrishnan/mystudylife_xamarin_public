﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using MyStudyLife.W8.Common;
using Windows.UI.Notifications;
using MyStudyLife.Data;
using AsyncTask = System.Threading.Tasks.Task;
using AgendaStatistics = MyStudyLife.Scheduling.Statistics.AgendaStatistics;
using Cirrious.CrossCore;

namespace MyStudyLife.W8.Services {
    public static class LiveTileService {
        private static IStorageProvider _storageProvider;
        private static TileUpdater _tileUpdater;

        private static IStorageProvider StorageProvider {
            get { return _storageProvider ?? (_storageProvider = new W8StorageProvider()); }
        }

        private static TileUpdater TileUpdater {
            get { return _tileUpdater ?? (_tileUpdater = TileUpdateManager.CreateTileUpdaterForApplication()); }
        }

        private static DateTime DeliveryTime {
            get { return DateTime.Now.AddSeconds(3d); }
        }

        public static async System.Threading.Tasks.Task RunAsync() {
            // See: http://msdn.microsoft.com/en-us/library/windows/apps/windows.ui.notifications.tiletemplatetype.aspx
            // SETS THE MAIN LIVE TILE.
            // Displays:
            //      "NOW" tile with current entry/minutes remaining if there is a current entry
            // or   "NEXT" tile with the details of the next class (and how many tasks are due)
            // or   "UPCOMING" tile with counts of classes left today, tasks due tmrw and number of incomplete tasks.

            DateTime now = DateTime.Now;

            TileUpdater.Clear();

            if (TileUpdater.Setting != NotificationSetting.Enabled) {
                return;
            }

            TileUpdater.EnableNotificationQueueForSquare310x310(false);
            TileUpdater.EnableNotificationQueueForWide310x150(true);

            // ------------------------
            // |        Image         |
            // ------------------------
            // | IM | BIG TEXT        |
            // |  G | small txt x4 lines|
            // ------------------------
            //
            //<tile>
            //  <visual>
            //    <binding template="TileWidePeekImage05">
            //      <image id="1" src="image1.png" alt="alt text"/>
            //      <text id="1">Text Header Field 1</text>
            //      <text id="2">Text Field 2</text>
            //    </binding>  
            //  </visual>
            //</tile>

            AgendaDay today;

            var scheduler = Mvx.Resolve<IAgendaScheduler>();
            today = await scheduler.GetDayForDateAsync(DateTime.Today);
            

            if (today.IsHoliday) {
                await SetHolidayTileAsync(today.Holiday, DateTime.Today);
            }
            else {
                // We're removing all conflicting entries, this could be done better in future!
                var entriesRemainingToday = today.Entries.Where(ae => ae.EndTime > now && !ae.IsConflicting).OrderBy(ae => ae.StartTime).ToList();

                int tileCount = entriesRemainingToday.Count > 5 ? 5 : entriesRemainingToday.Count;

                // Large tile
                var largeTileTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare310x310BlockAndText01).ToXDocument();

                Debug.WriteLine(largeTileTemplate);

                if (tileCount > 0) {
                    #region [WIDE]

                    var wideTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text09).ToXDocument();
                    var wideTextElements = wideTemplate.Descendants("text").ToList();
                    var wideTextTitle = wideTextElements.ElementAt(0);
                    var wideTextContent = wideTextElements.ElementAt(1);

                    bool haveSetCurrent = false;

                    // We can only queue a maximum of 5 notifications.
                    for (int i = 0; i < tileCount; i++) {
                        var entry = entriesRemainingToday.ElementAt(i);

                        if (entry.StartTime <= now && now < entry.EndTime) {
                            wideTextTitle.SetValue("Now");

                            haveSetCurrent = true;
                        }
                        else if (i == 0 || (haveSetCurrent && i == 1)) {
                            // If it's first then it's the next entry, if the current
                            // has been set and it's second then it's also the next entry.

                            wideTextTitle.SetValue("Next");
                        }
                        else {
                            wideTextTitle.SetValue("Later");
                        }

                        wideTextContent.SetValue(String.Format("{0}\n{1}\n{2}", entry.Title, entry.Time, entry.Location));

                        TileUpdater.AddToSchedule(new ScheduledTileNotification(
                            wideTemplate.ToXmlDocument(),
                            // This is calculated here for a reason! On slower machines if we store the value
                            // it can end up being in the past.
                            DateTime.Now.AddSeconds(3d)
                        ) {
                            ExpirationTime = DateTimeHelper.FromDateAndTime(DateTime.Today, entry.EndTime.TimeOfDay),
                            Tag = "ENTRY_WIDE_" + i
                        });
                    }

                    #endregion

                    #region [LARGE]

                    var largeTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare310x310TextList01).ToXDocument();
                    var largeTextElements = largeTemplate.Descendants("text").ToList();

                    var firstEntry = entriesRemainingToday.ElementAt(0);
                    var secondEntry = entriesRemainingToday.ElementAtOrDefault(1);
                    var thirdEntry = entriesRemainingToday.ElementAtOrDefault(2);

                    largeTextElements.ElementAt(0).SetValue(firstEntry.IsCurrent ? "Now" : "Next");
                    largeTextElements.ElementAt(1).SetValue(firstEntry.Title);
                    largeTextElements.ElementAt(2).SetValue(firstEntry.Time);

                    if (secondEntry != null) {
                        largeTextElements.ElementAt(3).SetValue(firstEntry.IsCurrent ? "Next" : "Later");
                        largeTextElements.ElementAt(4).SetValue(secondEntry.Title);
                        largeTextElements.ElementAt(5).SetValue(secondEntry.Time);
                    }

                    // We want "Now, Next, Later", not "Next, Later, Later"
                    if (thirdEntry != null) {
                        if (firstEntry.IsCurrent) {
                            largeTextElements.ElementAt(6).SetValue("Later");
                            largeTextElements.ElementAt(7).SetValue(thirdEntry.Title);
                            largeTextElements.ElementAt(8).SetValue(thirdEntry.Time);
                        }
                        else {
                            largeTextElements.ElementAt(7).SetValue(
// ReSharper disable once PossibleNullReferenceException
                                String.Format("{0} more classes after {1}", entriesRemainingToday.Skip(2).Count(), secondEntry.Title)
                            );
                        }
                    }
                    else {
                        using (var taskRepo = new TaskRepository()) {
                            var dueToday = (await taskRepo.GetByDueDateAsync(DateTime.Today)).ToList();

                            largeTextElements.ElementAt(7).SetValue(
                                dueToday.Count > 0
                                    ? String.Format("{0} tasks due today ({1} incomplete)", dueToday.Count, dueToday.Count(x => !x.IsComplete))
                                    : "You have no tasks due today"
                            );
                        }
                    }

                    TileUpdater.Update(new TileNotification(
                        largeTemplate.ToXmlDocument()
                    ) {
                        ExpirationTime = DateTimeHelper.FromDateAndTime(DateTime.Today, firstEntry.EndTime.TimeOfDay),
                        Tag = "ENTRIES_LARGE"
                    });
                
                    #endregion
                }
                else {
                    await SetStatisticsTileAsync();
                }
            }
        }

        private static async AsyncTask SetStatisticsTileAsync() {

            // CLASSES TOMORROW
            var stats = await AgendaStatistics.ForDateAsync(
                DateTime.Today.AddDays(1),
                StorageProvider
            );
            
            if (stats.IsHoliday) {
                await SetHolidayTileAsync(stats.Holiday, stats.ForDate);
            }
            else {
                var expiryTime = DateTime.Today.AddDays(2);

                string title = stats.ForDate.IsTomorrow() ? "Tomorrow" : stats.ForDate.ToString("dddd");
                string classes = String.Format(new PluralFormatProvider(true), "{0:class;classes}", stats.Classes.Total);
                string exams = String.Format(new PluralFormatProvider(true), "{0:exam;exams}", stats.Exams.Total);

                // TASKS
                string tasks = String.Format(new PluralFormatProvider(true), "{0:task;tasks} due", stats.Tasks.Total);

                if (stats.Tasks.Incomplete > 0) {
                    if (stats.Tasks.Overdue > 0) {
                        tasks += String.Format(" ({0} incomplete, {1} overdue)", stats.Tasks.Incomplete,
                            stats.Tasks.Overdue);
                    }
                    else {
                        tasks += String.Format(" ({0} incomplete)", stats.Tasks.Incomplete);
                    }
                }
                else if (stats.Tasks.Overdue > 0) {
                    tasks += String.Format(" ({0} overdue)", stats.Tasks.Overdue);
                }

                #region [WIDE]

                var wideTemplate =
                    TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text01).ToXDocument();
                var wideTextElements = wideTemplate.Descendants("text").ToList();

                wideTextElements.ElementAt(0).SetValue(title);
                wideTextElements.ElementAt(1).SetValue(classes);
                wideTextElements.ElementAt(2).SetValue(tasks);
                wideTextElements.ElementAt(3).SetValue(exams);

                TileUpdater.Update(new TileNotification(wideTemplate.ToXmlDocument()) {
                    Tag = "UPCOMING_WIDE",
                    ExpirationTime = expiryTime
                });

                #endregion

                #region [LARGE]

                var largeTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare310x310SmallImagesAndTextList05).ToXDocument();
                var largeImageElements = largeTemplate.Descendants("image").ToList();
                var largeTextElements = largeTemplate.Descendants("text").ToList();

                largeImageElements.ElementAt(0).SetAttributeValue("src", "ms-appx:///Assets/Icons/Class.png");
                largeImageElements.ElementAt(1).SetAttributeValue("src", "ms-appx:///Assets/Icons/Task.png");
                largeImageElements.ElementAt(2).SetAttributeValue("src", "ms-appx:///Assets/Icons/Exam.png");

                string classes2, tasks2, exams2;

                largeTextElements.ElementAt(0).SetValue(title);
                largeTextElements.ElementAt(1).SetValue(Truncate(classes, out classes2));
                largeTextElements.ElementAt(2).SetValue(classes2);
                largeTextElements.ElementAt(3).SetValue(Truncate(tasks, out tasks2));
                largeTextElements.ElementAt(4).SetValue(tasks2);
                largeTextElements.ElementAt(5).SetValue(Truncate(exams, out exams2));
                largeTextElements.ElementAt(6).SetValue(exams2);

                TileUpdater.Update(new TileNotification(largeTemplate.ToXmlDocument()) {
                    Tag = "UPCOMING_LARGE",
                    ExpirationTime = expiryTime
                });

                #endregion
            }
        }

        private static async AsyncTask SetHolidayTileAsync(Holiday holiday, DateTime forDate) {
            string title;

            if (forDate.IsToday()) {
                if (forDate == holiday.EndDate) {
                    title = "Today";
                }
                else if (holiday.EndDate.IsTomorrow()) {
                    title = "Until Tomorrow";
                }
                else {
                    title = String.Concat("Until ", GetRelativeDate(forDate, holiday.EndDate));
                }
            }
            else if (forDate.IsTomorrow()) {
                title = "Tomorrow";

                if (holiday.EndDate > forDate) {
                    title += String.Concat(" Until ", GetRelativeDate(forDate, holiday.EndDate));
                }
            }
            else {
                title = forDate.ToString("dddd");
            }

            string content = String.Format("You're on holiday ({0})", holiday.Name);

            string holidayPrompt = String.Empty;
            bool hasTasksDueSoon = false;

            var taskRepo = Mvx.Resolve<ITaskRepository>();
            var tasksDueSoon = (await taskRepo.GetIncompleteByDueDateAsync(holiday.EndDate, 3, true)).ToList();

            if (tasksDueSoon.Count > 0) {
                hasTasksDueSoon = true;

                int overdueCount = tasksDueSoon.Count(x => x.IsOverdue);
                int dueSoonCount = tasksDueSoon.Count - overdueCount;

                var sb = new StringBuilder("but you have ");

                if (overdueCount > 0) {
                    sb.AppendFormat("{0} overdue ", overdueCount);

                    if (dueSoonCount > 0) {
                        sb.Append("and ");
                    }
                }

                if (dueSoonCount > 0) {
                    sb.AppendFormat("{0} incomplete ", dueSoonCount);
                }

                sb.AppendFormat(new PluralFormatProvider(false), "{0:task;tasks}", tasksDueSoon.Count);

                sb.Append(dueSoonCount > 0 ? " due soon" : " to complete");

                holidayPrompt = sb.ToString();
            }

            var expiryTime = forDate.Date.AddDays(1d);

            #region [WIDE]

            var wideTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWide310x150Text09).ToXDocument();
            var wideTextElements = wideTemplate.Descendants("text").ToList();

            string wideContent = content;

            if (hasTasksDueSoon) {
                wideContent += " " + holidayPrompt;
            }

            wideTextElements.ElementAt(0).SetValue(title);
            wideTextElements.ElementAt(1).SetValue(wideContent);

            TileUpdater.Update(new TileNotification(wideTemplate.ToXmlDocument()) {
                Tag = String.Concat("HOL_WID_", forDate.ToString("ymd")), // 15 chars or less
                ExpirationTime = expiryTime
            });

            #endregion

            #region [LARGE]

            var largeTemplate = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquare310x310SmallImagesAndTextList05).ToXDocument();
            var largeImageElements = largeTemplate.Descendants("image").ToList();
            var largeTextElements = largeTemplate.Descendants("text").ToList();

            largeImageElements.ElementAt(0).SetAttributeValue("src", "ms-appx:///Assets/Icons/Holiday.png");
            largeImageElements.ElementAt(1).SetAttributeValue("src", "ms-appx:///Assets/Icons/Task.png");

            largeTextElements.ElementAt(0).SetValue(title);

            string content2, tasks2;

            largeTextElements.ElementAt(1).SetValue(Truncate(content, out content2));
            largeTextElements.ElementAt(2).SetValue(content2);
            largeTextElements.ElementAt(3).SetValue(Truncate("..." + holidayPrompt, out tasks2));
            largeTextElements.ElementAt(4).SetValue(tasks2);

            TileUpdater.Update(new TileNotification(largeTemplate.ToXmlDocument()) {
                Tag = String.Concat("HOL_LRG_", forDate.ToString("ymd")),
                ExpirationTime = expiryTime
            });

            #endregion
        }

        private static string Truncate(string inStr, out string outStr) {
            if (inStr.Length > 30) {
                int splitLoc = inStr.LastIndexOf(' ');

                outStr = inStr.Substring(splitLoc + 1);

                if (outStr.Length > 30) {
                    outStr = outStr.Substring(outStr.LastIndexOf(' ', 27)) + "...";
                }

                return inStr.Substring(0, splitLoc);
            }

            outStr = String.Empty;

            return inStr;
        }

        private static string GetRelativeDate(DateTime baseDate, DateTime date) {
            var diff = (date - baseDate).TotalDays;

            if (diff < 7) {
                return date.ToString("dddd");
            }

            return date.ToString(DateTimeFormat.AreDayAndMonthTheWrongWayRound ? "MMM d" : "d MMM");
        }

        /// <summary>
        ///		Clears all scheduled tiles.
        /// </summary>
        public static void ClearTiles() {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();

            updater.Clear();
        }
    }
}
