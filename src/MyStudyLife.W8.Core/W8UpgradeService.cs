﻿using System;
using System.IO;
using Windows.Storage;
using MyStudyLife.Data;
using MyStudyLife.Versioning;
using AsyncTask = System.Threading.Tasks.Task;
using MyStudyLife.Configuration;

namespace MyStudyLife.W8 {
    public class W8UpgradeService : UpgradeService {
        public W8UpgradeService(IMslConfig config, IStorageProvider provider) : base(config, provider) {}

        #region Overrides of UpgradeService
        
        /// <summary>
        ///     Allows applications to set the previous version
        ///     property before an attempted upgrade is made.
        /// 
        ///     Only called if <see cref="UpgradeService.PreviousAppVersion"/> is null.
        /// </summary>
        protected override async AsyncTask EnsurePreviousVersionAsync() {
            try {
                await ApplicationData.Current.LocalFolder.GetFileAsync("User.json");

                this.PreviousAppVersion = new Version(1, 0);
            }
            catch (FileNotFoundException) { }
        }

        #endregion
    }
}
