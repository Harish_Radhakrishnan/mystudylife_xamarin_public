﻿using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using MyStudyLife.Security.Crypto;

namespace MyStudyLife.W8 {
	public class W8CryptoProvider : ICryptoProvider {
		#region Implementation of ICryptoProvider

		/// <summary>
		///		Converts the given <paramref name="content"/>
		///		to a HMACSHA1 hash using the given <paramref name="privateKey"/>.
		/// </summary>
		/// <remarks>
		///		There is no HMACSHA1 implementation in the PCL... hence this.
		/// </remarks>
		public byte[] ComputeHMACSHA1(string content, string privateKey) {
			var algorithm = MacAlgorithmProvider.OpenAlgorithm(MacAlgorithmNames.HmacSha1);

			var buffer = CryptographicBuffer.ConvertStringToBinary(content, BinaryStringEncoding.Utf8);
			var keyBuffer = CryptographicBuffer.ConvertStringToBinary(privateKey, BinaryStringEncoding.Utf8);
			var key = algorithm.CreateKey(keyBuffer);

			return CryptographicEngine.Sign(key, buffer).ToArray();
		}

		#endregion
	}
}
