﻿using System.Diagnostics;
using System.Xml.Linq;
using Windows.Data.Xml.Dom;

namespace MyStudyLife.W8.Common
{
    public static class XmlExtensions
    {
        [DebuggerStepThrough]
        public static XmlDocument ToXmlDocument(this XDocument xDocument)
        {
            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.LoadXml(xDocument.ToString(SaveOptions.None));

            return xmlDocument;
        }

        [DebuggerStepThrough]
        public static XDocument ToXDocument(this XmlDocument xmlDocument)
        {
            return XDocument.Parse(xmlDocument.GetXml());
        }
    }
}
