﻿using System;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Net;

namespace MyStudyLife.W8 {
    public class W8NetworkProvider : INetworkProvider {
        #region INetworkProvider

        public event NetworkConnectivityChangedDelegate ConnectivityChanged;

        public bool IsConnectionDisabled { get; private set; }

        public bool IsOnline { get; private set; }

        #endregion

        private readonly IStorageProvider _storageProvider;
        private DeviceSettings _deviceSettings;

        private bool _isConnectionIncreasedCost;
        private NetworkCostType _costType = default(NetworkCostType);

        public W8NetworkProvider(IStorageProvider storageProvider) {
            this._storageProvider = storageProvider;

            ConnectionProfile connectionProfile = NetworkInformation.GetInternetConnectionProfile();

            this.IsOnline = connectionProfile != null && connectionProfile.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;

            Invalidate();
        }

        private async void Invalidate() {
            await GetCanUseConnectionAsync();

            NetworkInformation.NetworkStatusChanged += async (sender) => {
                await GetCanUseConnectionAsync();
            };
        }

        #region INetworkProvider Methods

        public async Task<bool> GetCanUseConnectionAsync() {
            bool wasOnline = this.IsOnline;
            bool wasConnectionDisabled = this.IsConnectionDisabled;
            bool wasConnectionIncreasedCost = _isConnectionIncreasedCost;

            bool newIsOnline;
            bool newIsDisabled = false;

            if (_deviceSettings == null) {
                using (var settingsRepo = new SettingsRepository(_storageProvider)) {
                    _deviceSettings = await settingsRepo.GetDeviceSettingsAsync();
                }
            }

            try {
                ConnectionProfile connectionProfile = NetworkInformation.GetInternetConnectionProfile();

                if (connectionProfile != null) {
                    ConnectionCost connectionCost = connectionProfile.GetConnectionCost();

                    var newConnectivity = connectionProfile.GetNetworkConnectivityLevel();
                    bool newIsConnectionIncreasedCost = connectionCost.OverDataLimit || connectionCost.Roaming;
                    var newCostType = connectionCost.NetworkCostType;

                    newIsOnline = newConnectivity == NetworkConnectivityLevel.InternetAccess;

                    bool isExistingMetered = (_costType == NetworkCostType.Variable);
                    // || CostType == NetworkCostType.Fixed);
                    bool isNewMetered = (newCostType == NetworkCostType.Variable);
                    // || n_costType == NetworkCostType.Fixed);

                    if (newIsOnline) {
                        // If it's gone from unmetered to metered.
                        if (!isExistingMetered && isNewMetered) {
                            newIsDisabled = _deviceSettings.SyncDisabledOverMeteredConnection;

                        } // If it's changed from metered to unmetered.
                        else if (isExistingMetered && !isNewMetered) {
                            // Do nothing, sync service will start to sync if applicable.

                        } // If it's changed normal to increased cost.
                        else if (!_isConnectionIncreasedCost && newIsConnectionIncreasedCost) {
                            newIsDisabled = _deviceSettings.SyncDisabledOverMeteredConnection;
                        }
                        else if (_isConnectionIncreasedCost && !newIsConnectionIncreasedCost) {
                            // Do nothing, sync service will start to sync if applicable.
                        }
                    }

                    _isConnectionIncreasedCost = newIsConnectionIncreasedCost;
                    _costType = newCostType;
                }
                else {
                    newIsOnline = false;
                }
            }
            catch (Exception) {
                _costType = NetworkCostType.Unknown;

                newIsOnline = false;
                newIsDisabled = false;
            }
            
            // Vital these are set before invoking the ConnectivityChanged event
            // for obvious reasons.
            this.IsOnline = newIsOnline;
            this.IsConnectionDisabled = newIsDisabled;

            // If the connection has changed.
            if ((wasOnline != newIsOnline || wasConnectionDisabled != newIsDisabled) && ConnectivityChanged != null) {
                ConnectivityChanged.Invoke(this, new NetworkConnectivityChangedEventArgs(wasOnline, wasConnectionIncreasedCost, newIsOnline, _isConnectionIncreasedCost, _deviceSettings));
            }

            return IsOnline && !IsConnectionDisabled;
        }

        #endregion
    }
}
