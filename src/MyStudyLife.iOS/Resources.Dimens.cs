using MyStudyLife.iOS.Controls;

namespace MyStudyLife.iOS {
    public static partial class Resources {
        public static class Dimens {
            public const float ColorStripSize = 4f;
            public const float ColorIndicatorSize = 15f;

            public static readonly Thickness ScreenMargin = new Thickness(15f);
            public const float DefaultMargin = 10f;
            public const float DefaultHorizontalMargin = 15f;
        }
    }
}