﻿using System;
using System.Globalization;
using MvvmCross.Converters;
using MvvmCross.Plugin.Color.Platforms.Ios;

namespace MyStudyLife.iOS.Converters {
    public class ColorToCGColorValueConverter : MvxValueConverter<System.Drawing.Color?> {
        protected override object Convert(System.Drawing.Color? value, Type targetType, object parameter, CultureInfo culture) {
            return value?.ToNativeColor().CGColor;
        }
    }
}
