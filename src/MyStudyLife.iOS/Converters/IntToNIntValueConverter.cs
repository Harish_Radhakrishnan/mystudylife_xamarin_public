﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.iOS.Converters {
    public class IntToNIntValueConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return default(nint);
            }
            
            return new nint((int) value);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return (int)value;
        }
    }
}
