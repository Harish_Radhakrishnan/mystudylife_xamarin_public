﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.iOS.Converters {
    // For some reason we need to use Convert.ChangeType here rather than explicit casts
    public class IntToNFloatValueConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            //actually value cannot be null if converter is being used by auto converter registry and was
            //registered with proper source/target types for unified (value types which are non nullable)
            //but keep it for sanity in case converter is used outside of auto converters scope
            if (value == null) {
                return default(nfloat);
            }
            
            var floatValue = (float) System.Convert.ChangeType(value, typeof(float), culture);
                
            return new nfloat(floatValue);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            var floatValue = (float) System.Convert.ChangeType(value, typeof(float), culture);

            return (int)floatValue;
        }
    }
}
