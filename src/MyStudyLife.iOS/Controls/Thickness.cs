namespace MyStudyLife.iOS.Controls {
    public struct Thickness {
        private float _left;
        private float _top;
        private float _right;
        private float _bottom;

        public float Left {
            get { return _left; }
            set { _left = value; }
        }

        public float Top {
            get { return _top; }
            set { _top = value; }
        }

        public float Right {
            get { return _right; }
            set { _right = value; }
        }

        public float Bottom {
            get { return _bottom; }
            set { _bottom = value; }
        }

        public Thickness(float uniformThickness) 
            : this(uniformThickness, uniformThickness, uniformThickness, uniformThickness) {}

        public Thickness(float leftAndRight, float topAndBottom) 
           : this(leftAndRight, topAndBottom, leftAndRight, topAndBottom) {}

        public Thickness(float left, float top, float right, float bottom) {
            this._left = left;
            this._top = top;
            this._right = right;
            this._bottom = bottom;
        }
    }
}