using CoreGraphics;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;
using MyStudyLife.iOS.Binding;

namespace MyStudyLife.iOS.Controls {
    public class UIFixedTableView : UITableView {
        public new MvxTableViewSource Source {
            get { return (MvxTableViewSource)base.Source; }
            set { base.Source = value; }
        }

        public override CGSize ContentSize {
            get { return base.ContentSize; }
            set {
                if (base.ContentSize != value) {
                    var oldSize = base.ContentSize;

                    base.ContentSize = value;

                    OnContentSizeChanged(oldSize, value);
                }
            }
        }

        public UIFixedTableView() {
            this.Initialize();
        }

        private void Initialize() {
            this.ScrollEnabled = false;
        }

        private void OnContentSizeChanged(CGSize oldSize, CGSize newSize) {
            var heightConstraint = this.Constraints.FirstOrDefault(x => x.FirstAttribute == NSLayoutAttribute.Height);

            if (heightConstraint != null) {
                heightConstraint.Constant = newSize.Height;

                this.LayoutIfNeeded();
            }
            else {
                this.AddConstraints(this.Height().EqualTo(newSize.Height).WithIdentifier("$UIFixedTableViewHeight$"));
            }
        }
    }

    public class MslFixedTableView : UIFixedTableView {
        public new MslTableViewSource Source {
            get { return (MslTableViewSource)base.Source; }
            set { base.Source = value; }
        }
    }
}