﻿using CoreAnimation;
using CoreGraphics;
using System;
using System.Collections.Generic;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public class MDTextField : MSLTextField {
        private const float FOCUS_ANIMATION_DURATION = .3f;

        private readonly Dictionary<UIControlState, UIColor> _stateColorMap = new Dictionary<UIControlState, UIColor> {
            [UIControlState.Normal] = Resources.Colors.Border,
            [UIControlState.Focused] = Resources.Colors.Accent
        };

        private readonly CATextLayer _labelLayer;
        private readonly CALayer _underlineLayer;

        private string _label;

        public string Label {
            get { return _label; }
            set {
                if (_label != value) {
                    this._labelLayer.String = this._label = value;
                }
            }
        }

        public MDTextField() {
            this.ContentEdgeInsets = new UIEdgeInsets(
                20f,
                0,
                8f,
                0
            );

            this._labelLayer = new CATextLayer {
                AlignmentMode = CATextLayer.AlignmentLeft,
                String = this._label,
                AnchorPoint = CGPoint.Empty,
                ContentsScale = UIScreen.MainScreen.Scale,
                FontSize = this.Font.PointSize
            };
            this._labelLayer.SetFont(Resources.Fonts.DefaultFamilyName);
            this.Layer.AddSublayer(this._labelLayer);

            this._underlineLayer = new CALayer {
                ContentsScale = UIScreen.MainScreen.Scale
            };
            this.Layer.AddSublayer(this._underlineLayer);

            this.EditingDidBegin += (s, e) => UpdateState();
            this.EditingDidEnd += (s, e) => UpdateState();
            this.ValueChanged += (s, e) => UpdateState();

            this.UpdateState(false);
        }

        private void UpdateState(bool animate = true) {
            if (animate) {
                CATransaction.Begin();
                CATransaction.AnimationDuration = FOCUS_ANIMATION_DURATION;
            }

            bool activeState = this.IsEditing || !String.IsNullOrEmpty(this.Text);

            var labelTranslateY = activeState ? -this._labelLayer.Position.Y : 0;
            var labelScale = activeState ? (float)FontSize.Caption / this.Font.PointSize : 1f;
            var foregroundColor = this._stateColorMap[this.IsEditing ? UIControlState.Focused : UIControlState.Normal].CGColor;

            this._labelLayer.Transform = CATransform3D.MakeTranslation(0, labelTranslateY, 0).Scale(labelScale);
            this._labelLayer.ForegroundColor = foregroundColor;

            this._underlineLayer.BackgroundColor = foregroundColor;
            // Double line thickness when focused, can't use 3d scale with y only for some reason
            this._underlineLayer.Frame = RectForUnderlineLayer(this.Layer);

            if (animate) {
                CATransaction.Commit();
            }
        }

        public override void LayoutSublayersOfLayer(CALayer layer) {
            base.LayoutSublayersOfLayer(layer);

            // We don't care about the label breaking
            var size = UIStringDrawing.StringSize(
                this.Label,
                UIFont.FromName(Resources.Fonts.DefaultFamilyName, this._labelLayer.FontSize)
            );

            this._labelLayer.Frame = new CGRect(
                this.ContentEdgeInsets.Left,
                this.ContentEdgeInsets.Top + (((layer.Bounds.Height - (this.ContentEdgeInsets.Top + this.ContentEdgeInsets.Bottom)) - size.Height) / 2f),
                this.Bounds.Width,
                size.Height
            );

            this._underlineLayer.Frame = RectForUnderlineLayer(layer);
        }

        private CGRect RectForUnderlineLayer(CALayer parentLayer) {
            nfloat height = this.IsEditing ? 2f : 1f;

            return new CGRect(
                0,
                parentLayer.Bounds.Height - height,
                parentLayer.Bounds.Width,
                height
            );
        }

        /// <summary>
        ///     Sets the color used for the underline and label foreground
        ///     for the given state.
        /// </summary>
        /// <param name="state">
        ///     The control state.
        /// </param>
        /// <param name="color">
        ///     The color.
        /// </param>
        public void SetLabelAndUnderlineColor(UIControlState state, UIColor color) {
            UIColor existingColor;

            if (!this._stateColorMap.TryGetValue(state, out existingColor) || !color.Equals(existingColor)) {
                this._stateColorMap[state] = color;

                this.UpdateState();
            }
        }
    }
}
