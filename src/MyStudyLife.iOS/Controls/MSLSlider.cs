using System;
using Foundation;
using MaterialControls;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Register("MSLSlider")]
    public sealed class MSLSlider : MDSlider {
        public EventHandler<UIEvent> OnTouchesBegan;
        public EventHandler<UIEvent> OnTouchesEnded;
        public EventHandler<UIEvent> OnTouchesCancelled;

        public override void TouchesBegan(NSSet touches, UIEvent evt) {
            base.TouchesBegan(touches, evt);
            OnTouchesBegan?.Invoke(touches, evt);
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt) {
            base.TouchesEnded(touches, evt);
            OnTouchesEnded?.Invoke(touches, evt);
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt) {
            base.TouchesEnded(touches, evt);
            OnTouchesCancelled?.Invoke(touches, evt);
        }
    }
}