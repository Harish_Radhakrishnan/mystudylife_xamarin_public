using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Foundation.Register("MSLTextField")]
    public class MSLTextField : UITextField {
        // TODO: Implement the rest! https://github.com/samsoffes/sstoolkit/blob/master/SSToolkit/SSTextField.m

        public sealed override UIColor TextColor {
            get { return base.TextColor; }
            set { base.TextColor = value; }
        }

        public sealed override bool TranslatesAutoresizingMaskIntoConstraints {
            get { return base.TranslatesAutoresizingMaskIntoConstraints; }
            set { base.TranslatesAutoresizingMaskIntoConstraints = value; }
        }

        public UIEdgeInsets ContentEdgeInsets { get; set; }

        public UIColor PlaceholderColor { get; set; }

        public UITextAlignment PlaceholderAlignment { get; set; }

        public MSLTextField() {
            this.TextColor = Resources.Colors.Foreground;
            this.PlaceholderColor = Resources.Colors.SubtleText;
            this.TranslatesAutoresizingMaskIntoConstraints = false;
        }

        public override CGRect TextRect(CGRect bounds) {
            var insets = this.ContentEdgeInsets;

            return base.TextRect(insets.InsetRect(bounds));
        }

        public override CGRect EditingRect(CGRect bounds) {
            var insets = this.ContentEdgeInsets;
            
            return base.EditingRect(insets.InsetRect(bounds));
        }

        public override void DrawPlaceholder(CGRect rect) {
            this.PlaceholderColor.SetFill();
            
            var placeholderRect = new CGRect(rect.X, (rect.Height - (float)this.Font.LineHeight) / 2, rect.Width, (float)this.Font.LineHeight);

            new NSString(this.Placeholder).DrawString(placeholderRect, this.Font, UILineBreakMode.WordWrap, PlaceholderAlignment);
        }
    }
}