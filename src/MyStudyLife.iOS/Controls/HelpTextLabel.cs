using System;
using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    internal sealed class HelpTextLabel : MSLLabel {
        private Action<HelpTextLabel> _onSized;

        public override string Text {
            get { return base.Text; }
            set {
                base.Text = value;

                this.SizeToFit();
            }
        }

        public HelpTextLabel(Action<HelpTextLabel> onSized = null) {
            this._onSized = onSized;

            ContentEdgeInsets = new UIEdgeInsets(15, 20, 15, 20);
            TranslatesAutoresizingMaskIntoConstraints = true;
            Lines = 0;
            LineBreakMode = UILineBreakMode.WordWrap;
            Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeSmall);
            TextColor = Resources.Colors.SubtleText;
        }

        public override void SizeToFit() {
            var size = new CGSize(
                this.Frame.Width == 0
                    ? UIScreen.MainScreen.ApplicationFrame.Width
                    : NMath.Min(this.Frame.Width, UIScreen.MainScreen.ApplicationFrame.Width),
                float.MaxValue
            );
            
            this.Frame = new CGRect(
                this.Frame.Location,
                new CGSize(size.Width, SizeThatFits(size).Height)
            );

            if (_onSized != null) {
                _onSized(this);
            }
        }

        //public override SizeF SizeThatFits(SizeF size) {
        //    //var maxSize = new SizeF(
        //    //    size.Width,
        //    //    float.MaxValue
        //    //);

        //    return this.TextRectForBounds(new RectangleF(this.Frame.X, this.Frame.Y, size.Width, size.Height), 0).Size;

        //    // var textSize = new NSString(this.Text).StringSize(this.Font, maxSize, this.LineBreakMode);

        //    // var desiredHeight = textSize.Height + this.ContentEdgeInsets.Top + this.ContentEdgeInsets.Bottom;

        //    // return new SizeF(size.Width, (float) Math.Ceiling(desiredHeight));
        //}
    }

    internal sealed class HelpTextAttributedLabel : MslAttributedLabel {
        public UIEdgeInsets ContentEdgeInsets { get; set; }

        public override void SizeToFit() {
            var size = new CGSize(
                this.Frame.Width == 0
                    ? UIScreen.MainScreen.ApplicationFrame.Width
                    : NMath.Min(this.Frame.Width, UIScreen.MainScreen.ApplicationFrame.Width),
                float.MaxValue
            );

            this.Frame = new CGRect(
                this.Frame.Location,
                new CGSize(size.Width, SizeThatFits(size).Height + this.ContentEdgeInsets.Left + this.ContentEdgeInsets.Right)
            );
        }

        public override CGRect TextRectForBounds(CGRect bounds, nint numberOfLines) {
            var insets = this.ContentEdgeInsets;
            var rect = base.TextRectForBounds(insets.InsetRect(bounds), numberOfLines);

            rect.X -= insets.Left;
            rect.Y -= insets.Top;

            rect.Size = new CGSize(
                rect.Size.Width + (insets.Left + insets.Right),
                rect.Size.Height + (insets.Top + insets.Bottom)
            );

            return rect;
        }

        public override void DrawText(CGRect rect) {
            base.DrawText(ContentEdgeInsets.InsetRect(rect));
        }
    }
}