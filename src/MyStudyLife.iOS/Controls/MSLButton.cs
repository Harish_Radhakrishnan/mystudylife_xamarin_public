using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Register(nameof(MSLButton))]
    public class MSLButton : UIButton {
        /// <summary>
        ///     The "System" alpha when the button is highlighted.
        /// 
        ///     Interpreted from UINavigationBarItem.
        /// </summary>
        public const float SystemHighlightedAlpha = 0.3f;

        private UIColor _backgroundColor;
        private UIColor _highlightedBackgroundColor;

        private nfloat _alpha = 1;
        private nfloat? _highlightedAlpha;

        public override UIColor BackgroundColor {
            get { return _backgroundColor; }
            set {
                _backgroundColor = value;

                if (!this.Highlighted || this.HighlightedBackgroundColor == null) {
                    this.RealBackgroundColor = value;
                }
            }
        }

        public UIColor HighlightedBackgroundColor {
            get { return _highlightedBackgroundColor; }
            set {
                _highlightedBackgroundColor = value;

                if (this.Highlighted) {
                    this.RealBackgroundColor = value ?? this.BackgroundColor;
                }
            }
        }

        private UIColor RealBackgroundColor {
            get { return base.BackgroundColor; }
            set { base.BackgroundColor = value; }
        }

        public override nfloat Alpha {
            get { return _alpha; }
            set {
                _alpha = value;

                if (!this.Highlighted || this.HighlightedAlpha == null) {
                    this.RealAlpha = value;
                }
            }
        }

        public nfloat? HighlightedAlpha {
            get { return _highlightedAlpha; }
            set {
                _highlightedAlpha = value;

                if (this.Highlighted) {
                    this.RealAlpha = value ?? this.Alpha;
                }
            }
        }

        private nfloat RealAlpha {
            get { return base.Alpha; }
            set { base.Alpha = value; }
        }

        public override bool Highlighted {
            get { return base.Highlighted; }
            set {
                base.Highlighted = value;

                if (HighlightedBackgroundColor != null) {
                    this.RealBackgroundColor = value ? HighlightedBackgroundColor : BackgroundColor;
                }

                if (HighlightedAlpha != null) {
                    if (value) {
                        RealAlpha = HighlightedAlpha.Value;
                    }
                    else {
                        Animate(0.3f, () => this.RealAlpha = Alpha);
                    }
                }
            }
        }

        public MSLButton() : base() {
            this.SetDefaults();
        }

        public MSLButton(CGRect frame) : base(frame) {
            this.SetDefaults();
        }

        public MSLButton(IntPtr handle) : base(handle) { }

        private void SetDefaults() {
            this.BackgroundColor = UIColor.White;
            this.HighlightedBackgroundColor = Resources.Colors.AccentLight;
        }
    }
}