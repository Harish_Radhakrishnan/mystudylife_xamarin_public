using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    internal static class BorderedViewHelper {
        public static void DrawBorder(CGRect area, UIColor borderColor, Thickness borderThickness) {

            var ctx = UIGraphics.GetCurrentContext();

            ctx.SetStrokeColor(borderColor.CGColor);

            if (borderThickness.Left > 0) {
                ctx.SetLineWidth(borderThickness.Left * UIScreen.MainScreen.Scale);
                
                ctx.BeginPath();
                ctx.MoveTo(area.GetMinX(), area.GetMinY());
                ctx.MoveTo(area.GetMinX(), area.GetMaxY());
                ctx.StrokePath();
            }

            if (borderThickness.Top > 0) {
                ctx.SetLineWidth(borderThickness.Top * UIScreen.MainScreen.Scale);

                ctx.BeginPath();
                ctx.MoveTo(area.GetMinX(), area.GetMinY());
                ctx.MoveTo(area.GetMaxX(), area.GetMinY());
                ctx.StrokePath();
            }

            if (borderThickness.Right > 0) {
                ctx.SetLineWidth(borderThickness.Right * UIScreen.MainScreen.Scale);

                ctx.BeginPath();
                ctx.MoveTo(area.GetMaxX(), area.GetMinY());
                ctx.MoveTo(area.GetMaxX(), area.GetMaxY());
                ctx.StrokePath();
            }

            if (borderThickness.Bottom > 0) {
                ctx.SetLineWidth(borderThickness.Bottom * UIScreen.MainScreen.Scale);

                ctx.BeginPath();
                ctx.MoveTo(area.GetMinX(), area.GetMaxY());
                ctx.AddLineToPoint(area.GetMaxX(), area.GetMaxY());
                ctx.StrokePath();
            }
        }
    }
}