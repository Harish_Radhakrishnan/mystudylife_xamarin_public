﻿using Cirrious.FluentLayouts.Touch;
using System;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    /// <summary>
    ///     Extensions for <see cref="OAStackView"/>.
    /// </summary>
    public static class OAStackViewEx {
        /// <summary>
        ///     Adds a spacer view with the given height.
        /// </summary>
        /// <param name="stackView">
        ///     The <see cref="OAStackView.OAStackView"/>.
        /// </param>
        /// <param name="height">
        ///     The height of the spacer in pixels.
        /// </param>
        public static void AddSpacer(this OAStackView.OAStackView stackView, nfloat height) {
            var spacer = new UIView { TranslatesAutoresizingMaskIntoConstraints = false };
            spacer.AddConstraints(spacer.Height().EqualTo(height));
            stackView.AddArrangedSubview(spacer);
        }

        /// <summary>
        ///     Adds the given <see cref="UIView"/> to the end of the stack view,
        ///     optionally with an amount of space before and after.
        /// </summary>
        /// <param name="stackView">
        ///     The <see cref="OAStackView.OAStackView"/>.
        /// </param>
        /// <param name="view">
        ///     The view to add to the stack view.
        /// </param>
        /// <param name="spaceBefore">
        ///     The space, in pixels to add before the <paramref name="view"/>.
        /// </param>
        /// <param name="spaceAfter">
        ///     The space, in pixels to add before the <paramref name="view"/>.
        /// </param>
        public static void AddArrangedSubview(this OAStackView.OAStackView stackView, UIView view, nfloat spaceBefore, nfloat spaceAfter) {
            Check.NotNull(view);

            if (0f < spaceBefore) {
                var spacer = new UIView { TranslatesAutoresizingMaskIntoConstraints = false };
                spacer.AddConstraints(spacer.Height().EqualTo(spaceBefore));
                stackView.AddArrangedSubview(spacer);
            }

            stackView.AddArrangedSubview(view);

            if (0f < spaceAfter) {
                var spacer = new UIView { TranslatesAutoresizingMaskIntoConstraints = false };
                spacer.AddConstraints(spacer.Height().EqualTo(spaceAfter));
                stackView.AddArrangedSubview(spacer);
            }
        }
    }
}