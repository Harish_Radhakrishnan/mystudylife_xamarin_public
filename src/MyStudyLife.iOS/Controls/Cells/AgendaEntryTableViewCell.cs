﻿using System;
using System.ComponentModel;
using Cirrious.FluentLayouts.Touch;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Plugin.Color.Platforms.Ios;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data.UI;
using MyStudyLife.iOS.Binding;
using MyStudyLife.Scheduling;
using MyStudyLife.UI;
using UIKit;
using static MyStudyLife.iOS.Resources;

namespace MyStudyLife.iOS.Controls.Cells {
    public abstract class AgendaEntryTableViewCellBase : BorderedTableViewCellBase {
        protected const float TimeLabelLeftOffset = Dimens.DefaultHorizontalMargin;
        public const float PrimaryContentLeftOffset = 70f + Dimens.ColorStripSize;

        private const string StartTimeLabelVOffsetConstraintId = "$StartTimeLabel_V$";
        private const string EndTimeLabelVOffsetConstraintId = "$EndTimeLabel_V$";

        private float _timeLabelCenterOffset = 2f;

        public UILabel StartTimeLabel { get; private set; }
        public UILabel EndTimeLabel { get; private set; }

        public virtual float TimeLabelCenterOffset {
            get { return _timeLabelCenterOffset; }
            set {
                if (!Equals(_timeLabelCenterOffset, value)) {
                    _timeLabelCenterOffset = value;

                    foreach (var c in this.Constraints) {
                        switch (c.GetIdentifier()) {
                            case StartTimeLabelVOffsetConstraintId:
                                c.Constant = -value;
                                break;
                            case EndTimeLabelVOffsetConstraintId:
                                c.Constant = value;
                                break;
                        }
                    }

                    this.SetNeedsLayout();
                }
            }
        }

        public AgendaEntryTableViewCellBase(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            var startTimeLabel = StartTimeLabel = new MSLLabel {
                Font = Fonts.OfSize(FontSize.Caption)
            };
            this.ContentView.Add(startTimeLabel);
            var endTimeLabel = EndTimeLabel = new MSLLabel {
                Font = Fonts.OfSize(FontSize.Caption)
            };
            this.ContentView.Add(endTimeLabel);

            const float timeLabelLeftOffset = Dimens.DefaultHorizontalMargin;

            this.ContentView.AddConstraints(
                startTimeLabel.AtLeftOf(ContentView, timeLabelLeftOffset),
                startTimeLabel
                    .Bottom()
                    .EqualTo()
                    .CenterYOf(ContentView)
                    .Minus(TimeLabelCenterOffset)
                    .WithIdentifier(StartTimeLabelVOffsetConstraintId),

                endTimeLabel.WithSameLeft(startTimeLabel),
                endTimeLabel
                    .Top()
                    .EqualTo()
                    .CenterYOf(ContentView)
                    .Plus(TimeLabelCenterOffset)
                    .WithIdentifier(EndTimeLabelVOffsetConstraintId)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<AgendaEntryTableViewCellBase, AgendaEntry>();

                set.Bind(this.ColorIndicator)
                    .For("LayerBackgroundColor")
                    .To(x => x.ColorValue)
                    .WithConversion(Resources.Converters.NativeColor);

                set.Bind(startTimeLabel).To(x => x.StartTime).WithConversion(Resources.Converters.TimeSpanToShortString);
                set.Bind(endTimeLabel).To(x => x.EndTime).WithConversion(Resources.Converters.TimeSpanToShortString);

                set.Apply();
            });
        }
    }

    [Register(nameof(AgendaEntryTableViewCell))]
    [MslTableViewCell(Height = Height)]
    public class AgendaEntryTableViewCell : AgendaEntryTableViewCellBase {
        public const float Height = 64f + (VERTICAL_SPACING * 2f);

        private IDisposable _listener;

        private MSLLabel _tasksDuePill;
        private UIView _teacherView;
        private UILabel _teacherLabel;

        protected UILabel TitleLabel { get; private set; }
        protected UILabel LocationLabel { get; private set; }
        protected MSLLabel TasksDuePill => _tasksDuePill;
        protected UIView TeacherView => _teacherView;

        public override float DesiredHeight => Height;

        public AgendaEntryTableViewCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            base.InitializeImpl();

            var titleLabel = TitleLabel = new MSLLabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Colors.Foreground,
                Font = Fonts.OfSize(FontSize.Subhead)
            };
            titleLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
            this.ContentView.Add(titleLabel);

            var locationLabel = LocationLabel = new MSLLabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Colors.SubtleText,
                Font = Fonts.OfSize(FontSize.Body)
            };
            locationLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
            this.ContentView.Add(locationLabel);

            this._tasksDuePill = new MSLLabel {
                Font = Fonts.OfSize(FontSize.ExtraSmall, FontWeight.Medium),
                ContentEdgeInsets = new UIEdgeInsets(2.5f, 5f, 2.5f, 5f),
                TextColor = UIColor.White,
                ShadowColor = UIColor.FromWhiteAlpha(0f, .2f),
                Layer = {
                    CornerRadius = 2f
                }
            };
            this.ContentView.Add(this._tasksDuePill);

            _teacherView = new MslView();
            _teacherView.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultHigh, UILayoutConstraintAxis.Horizontal);
            this.ContentView.Add(_teacherView);

            var teacherIcon = new MSLIcon(Glyph.Teacher, .8f) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.SubtleText
            };
            _teacherView.Add(teacherIcon);

            _teacherLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontSmall
            };
            _teacherView.Add(_teacherLabel);

            this.ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.ContentView.AddConstraints(
                titleLabel.AtLeftOf(this.ContentView, PrimaryContentLeftOffset),
                titleLabel.Baseline().EqualTo().CenterYOf(this.ContentView).Minus(2.5f),
                titleLabel.Right().LessThanOrEqualTo().LeftOf(_tasksDuePill).Minus(2.5f),

                locationLabel.WithSameLeft(titleLabel),
                locationLabel.Below(titleLabel, 1f),

                _tasksDuePill.CenterY().EqualTo().CenterYOf(titleLabel).Minus(3f),
                _tasksDuePill.AtRightOf(this.ContentView, Dimens.DefaultHorizontalMargin),

                _teacherView.Left().GreaterThanOrEqualTo().RightOf(locationLabel).Plus(5f),
                _teacherView.WithSameCenterY(locationLabel),
                _teacherView.AtRightOf(this.ContentView, Dimens.DefaultHorizontalMargin),

                teacherIcon.AtTopOf(_teacherView),
                teacherIcon.AtBottomOf(_teacherView),
                teacherIcon.AtRightOf(_teacherView, -1),

                _teacherLabel.AtLeftOf(_teacherView),
                _teacherLabel.ToLeftOf(teacherIcon, 2),
                _teacherLabel.WithSameCenterY(teacherIcon)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<AgendaEntryTableViewCell, AgendaEntry>();

                set.Bind(titleLabel).To(x => x.Title);
                set.Bind(locationLabel).To(x => x.Location).WithConversion(Resources.Converters.Coalesce);
                set.Bind(_teacherView).Visibility().To(x => x.TeacherName);
                set.Bind(_teacherLabel).To(x => x.TeacherName);

                set.Apply();

                this.BindingContext.DataContextChanged += BindingContextOnDataContextChanged;

                this.UpdateTasksPill();
            });
        }

        private void BindingContextOnDataContextChanged(object sender, EventArgs e) {
            if (this.DataContext == null) {
                return;
            }

            this.UpdateTasksPill();

            this.ListenForStateChange();
        }

        private void ListenForStateChange() {
            var ae = (AgendaEntry)this.DataContext;

            _listener?.Dispose();
            _listener = ae?.WeakSubscribe(DataContextOnPropertyChanged);
        }

        protected virtual void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In(nameof(AgendaEntry.TasksOverdue), nameof(AgendaEntry.TasksIncomplete), nameof(AgendaEntry.TasksComplete))) {
                this.UpdateTasksPill();
            }
        }

        private void UpdateTasksPill() {
            var agendaEntry = (AgendaEntry)this.DataContext;

            if (agendaEntry == null) {
                return;
            }

            System.Drawing.Color color = UserColors.Okay;
            if (agendaEntry.TasksOverdue > 0) {
                color = UserColors.Attention;
            }
            else if (agendaEntry.TasksIncomplete > 0) {
                color = UserColors.Warning;
            }

            _tasksDuePill.Hidden = agendaEntry.TasksDue == 0;
            _tasksDuePill.Text = StringEx.PluralFormat(true, "{0:TASK;TASKS} DUE", agendaEntry.TasksDue);
            _tasksDuePill.Layer.BackgroundColor = color.ToNativeColor().CGColor;
        }
    }
}
