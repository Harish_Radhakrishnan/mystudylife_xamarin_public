using System;
using System.ComponentModel;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Foundation;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Extensions;
using UIKit;

namespace MyStudyLife.iOS.Controls.Cells {
    [Register(nameof(TaskTableViewCell)), UsedImplicitly]
    internal class TaskTableViewCell : ListItemTableViewCell {
        private IDisposable _subscription;

        private UIView _progressView;
        private UILabel _progressLabel;
        private MSLIcon _doneIcon;

        public TaskTableViewCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            _progressView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this.ContentView.Add(_progressView);

            var progressIcon = new MSLIcon(Glyph.Progress, .8f) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.SubtleText
            };
            _progressView.Add(progressIcon);

            _progressLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontSmall
            };
            _progressView.Add(_progressLabel);

            _doneIcon = new MSLIcon(Glyph.Tick) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.OkayForeground
            };
            this.ContentView.Add(_doneIcon);

            this.ContentView.AddConstraints(
                _progressView.Left().GreaterThanOrEqualTo().RightOf(SubTitleLabel).Plus(5f),
                _progressView.WithSameCenterY(this.SubTitleLabel),
                _progressView.WithSameRight(this.RightLabel),

                progressIcon.AtTopOf(_progressView),
                progressIcon.AtBottomOf(_progressView),
                progressIcon.AtRightOf(_progressView, -1),

                _progressLabel.AtLeftOf(_progressView),
                _progressLabel.ToLeftOf(progressIcon, 2),
                _progressLabel.WithSameCenterY(progressIcon),

                _doneIcon.WithSameCenterY(this.SubTitleLabel),
                _doneIcon.WithSameRight(this.RightLabel)
            );
        }

        protected override void DoBind() {
            var set = this.CreateBindingSet<TaskTableViewCell, Task>();

            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).SourceDescribed("Subject.Name + ' ' + Type");
            set.Bind(RightLabel).To(x => x.DueDate).WithConversion(Resources.Converters.ContextualDate, ContextualDateFormat.Short);
            set.Bind(ColorIndicator)
                .For("LayerBackgroundColor")
                .To(x => x.Subject.Color)
                .WithConversion(Resources.Converters.SubjectColor);

            set.Bind(_progressView).InvertedVisibility().To(x => x.IsComplete);
            set.Bind(_progressLabel).For(x => x.Text).SourceDescribed("Progress + '%'");
            set.Bind(_doneIcon).Visibility().To(x => x.IsComplete);

            set.Apply();

            BindingContext.DataContextChanged += (s, e) => OnDataContextChanged();

            OnDataContextChanged();
        }

        private void OnDataContextChanged() {
            _subscription?.Dispose();

            if (DataContext is Task task) {
                _subscription = task.WeakSubscribe(DataContextOnPropertyChanged);
                ApplyStyles();
            }
            else {
                _subscription = null;
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In(nameof(Task.IsComplete), nameof(Task.DueDate))) {
                ApplyStyles();
            }
        }

        private void ApplyStyles() {
            if (!(DataContext is Task task)) {
                return;
            }

            var isComplete = task.IsComplete;
            var alpha = isComplete ? 0.5f : 1;

            Subviews.Apply(x => x.Alpha = alpha);

            TitleLabel.Strikethrough(isComplete);
            SubTitleLabel.Strikethrough(isComplete);
            RightLabel.Strikethrough(isComplete);

            UIColor dueDateTextColor = Resources.Colors.ForegroundMid;

            if (!isComplete) {
                if (task.IsIncompleteAndDueSoon) {
                    dueDateTextColor = Resources.Colors.WarningForeground;
                }
                else if (task.IsOverdue) {
                    dueDateTextColor = Resources.Colors.AttentionForeground;
                }
            }

            RightLabel.TextColor = dueDateTextColor;
        }
    }
}