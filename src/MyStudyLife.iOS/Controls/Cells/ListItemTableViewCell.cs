using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;
using MyStudyLife.iOS.Binding;

namespace MyStudyLife.iOS.Controls.Cells {
    [MslTableViewCell(Height = Height)]
    public abstract class ListItemTableViewCell : MvxTableViewCell {
        public const float Height = 60f;

        protected UIView ColorIndicator { get; private set; }
        protected UILabel TitleLabel { get; private set; }
        protected UILabel SubTitleLabel { get; private set; }
        protected UILabel RightLabel { get; private set; }

        public virtual bool ShowColorIndicator => true;

        protected ListItemTableViewCell() {
            this.Initialize();
        }

        protected ListItemTableViewCell(IntPtr handle) : base(handle) {
            this.Initialize();
        }

        protected virtual void Initialize() {
            var view = this.ContentView;

            ColorIndicator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            if (ShowColorIndicator) {
                view.Add(ColorIndicator);
            }

            TitleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeMedium),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            TitleLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
            view.Add(TitleLabel);

            SubTitleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            SubTitleLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
            view.Add(SubTitleLabel);

            RightLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.ForegroundMid,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeSmall),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            RightLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultHigh, UILayoutConstraintAxis.Horizontal);
            view.Add(RightLabel);

            if (ShowColorIndicator) {
                view.AddConstraints(
                    ColorIndicator.WithSameLeft(view),
                    ColorIndicator.FullHeightOf(view),
                    ColorIndicator.Width().EqualTo(Resources.Dimens.ColorStripSize)
                );
            }

            view.AddConstraints(
                TitleLabel.AtLeftOf(view, Resources.Dimens.ScreenMargin.Left),
                TitleLabel.Baseline().EqualTo().CenterYOf(this.ContentView).Minus(3f),

                SubTitleLabel.WithSameLeft(TitleLabel),
                SubTitleLabel.Below(TitleLabel, 1f),

                RightLabel.Left().GreaterThanOrEqualTo().RightOf(TitleLabel).Plus(5),
                RightLabel.WithSameRight(view).Minus(10).SetPriority(UILayoutPriority.Required),
                RightLabel.WithSameCenterY(TitleLabel)
            );

            this.InitializeImpl();

            this.DelayBind(DoBind);
        }

        protected virtual void InitializeImpl() {

        }

        protected abstract void DoBind();
    }
}