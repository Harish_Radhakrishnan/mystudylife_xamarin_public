﻿using System;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;
using static MyStudyLife.iOS.Resources;

namespace MyStudyLife.iOS.Controls.Cells {

    /// <summary>
    ///     Table view cell which renders the content with bottom and top borders.
    ///     
    ///     This allows spacing between cells, whilst maintaining borders.
    /// </summary>
    public abstract class BorderedTableViewCellBase : MvxTableViewCell {
        public const float VERTICAL_SPACING = 2.5f;

        public static UIEdgeInsets DefaultContentEdgeInsets = new UIEdgeInsets(VERTICAL_SPACING, 0, VERTICAL_SPACING, 0);

        private UIBorderedView _contentView;
        private UIEdgeInsets _contentEdgeInsets = DefaultContentEdgeInsets;
        private UIColor _borderColor = Colors.BorderLight;
        private Thickness _borderThickness = new Thickness(0, .5f);

        public abstract float DesiredHeight { get; }

        public sealed override UIView ContentView => _contentView;

        public UIEdgeInsets ContentEdgeInsets {
            get { return _contentEdgeInsets; }
            set {
                if (!Equals(_contentEdgeInsets, value)) {
                    _contentEdgeInsets = value;

                    this.SetNeedsLayout();
                }
            }
        }

        public UIColor BorderColor {
            get { return _borderColor; }
            set {
                if (!Equals(_borderColor, value)) {
                    _contentView.BorderColor = _borderColor = value;
                }
            }
        }

        public Thickness BorderThickness {
            get { return _borderThickness; }
            set {
                if (!Equals(_borderThickness, value)) {
                    _contentView.BorderThickness = _borderThickness = value;
                }
            }
        }

        protected UIView ColorIndicator { get; private set; }

        #region Highlighted

        public override void SetHighlighted(bool highlighted, bool animated) {
            if (highlighted) {
                this.ContentView.BackgroundColor = Resources.Colors.Subtle;
            }
            else {
                Animate(0.3f, () => this.ContentView.BackgroundColor = UIColor.White);
            }
        }

        #endregion

        protected BorderedTableViewCellBase(IntPtr handle) : base(handle) {
            this.Initialize();
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            // We override ContentView hence not using .Subviews[0]
            this.ContentView.Frame = _contentEdgeInsets.InsetRect(new CGRect(0, 0, this.Bounds.Width, DesiredHeight));
        }

        private void Initialize() {
            this.BackgroundColor = UIColor.Clear;

            base.ContentView.Add(this._contentView = new UIBorderedView {
                BorderColor = this.BorderColor,
                BorderThickness = this.BorderThickness
            });

            ColorIndicator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _contentView.Add(ColorIndicator);

            _contentView.AddConstraints(
                ColorIndicator.WithSameLeft(_contentView),
                ColorIndicator.FullHeightOf(_contentView),
                ColorIndicator.Width().EqualTo(Dimens.ColorStripSize)
            );

            this.InitializeImpl();
        }

        protected abstract void InitializeImpl();
    }
}
