using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Data;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Controls.Cells {
    [Foundation.Register("HolidayTableViewCell")]
    internal sealed class HolidayTableViewCell : RelatedTableViewCell {
        private MSLIcon _pushesScheduleIcon;

        public HolidayTableViewCell() { }
        public HolidayTableViewCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            _pushesScheduleIcon = new MSLIcon(Glyph.Pushes) {
                TextColor = Resources.Colors.SubtleText
            };
            this.ContentView.Add(_pushesScheduleIcon);

            this.ContentView.AddConstraints(
                _pushesScheduleIcon.ToRightOf(SubTitleLabel, 4f),
                _pushesScheduleIcon.WithSameTop(SubTitleLabel)
            );
        }

        protected override void DoBind() {
            var set = this.CreateBindingSet<HolidayTableViewCell, Holiday>();

            set.Bind(TitleLabel).To(x => x.Name);
            set.Bind(SubTitleLabel).To(x => x.Dates);

            set.Bind(_pushesScheduleIcon).To(x => x.ActualPushesSchedule).For("Visible");

            set.Apply();
        }
    }
}
