using System;
using System.Globalization;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings;
using CoreGraphics;
using MvvmCross.Converters;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.Utilities;
using UIKit;
using Class = MyStudyLife.Data.Class;

namespace MyStudyLife.iOS.Controls.Cells {
    public abstract class ClassTableViewCell : UITableViewCell, IMvxBindable {
        protected UIView ColorIndicator { get; private set; }
        protected UILabel TitleLabel { get; private set; }
        protected UIView TimesView { get; private set; }

        #region IMvxBindable

        // From MvxTableViewCell... needed because we can't override
        // DataContext to clear times bindings!

        public IMvxBindingContext BindingContext { get; set; }

        public object DataContext {
            get { return BindingContext.DataContext; }
            set {
                BindingContext.ClearBindings("Times");

                BindingContext.DataContext = value;

                OnDataContextChange();
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                BindingContext.ClearAllBindings();
            }

            base.Dispose(disposing);
        }

        #endregion

        protected ClassTableViewCell(IntPtr handle) : base(handle) {
            this.CreateBindingContext();

            Initialize();
        }

        private CGColor _colorBefore;

        // Prevents the color from being overriden when highlighted
        public override void SetHighlighted(bool highlighted, bool animated) {
            if (highlighted) {
                _colorBefore = ColorIndicator.Layer.BackgroundColor;

                base.SetSelected(true, animated);

                ColorIndicator.Layer.BackgroundColor = _colorBefore;
            }
            else {
                base.SetSelected(false, animated);
            }
        }

        private void Initialize() {
            var view = ContentView;

            var colorIndicator = ColorIndicator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(colorIndicator);

            var titleLabel = TitleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeMedium),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            titleLabel.SetContentCompressionResistancePriority((float) UILayoutPriority.DefaultLow,
                UILayoutConstraintAxis.Horizontal);
            view.Add(titleLabel);

            var timesView = TimesView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(timesView);

            InitializeImpl();

            view.AddConstraints(
                colorIndicator.AtLeftOf(view),
                colorIndicator.Width().EqualTo(Resources.Dimens.ColorStripSize),
                colorIndicator.FullHeightOf(view),
                titleLabel.AtLeftOf(view, Resources.Dimens.ScreenMargin.Left),
                titleLabel.AtTopOf(view, Resources.Dimens.DefaultMargin),
                timesView.WithSameLeft(titleLabel),
                timesView.AtRightOf(view, Resources.Dimens.ScreenMargin.Right),
                timesView.Below(titleLabel),
                timesView.WithSameBottom(view)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<ClassTableViewCell, Class>();

                set.Bind(titleLabel).To(x => x.Title);

                DoBind(set);

                set.Apply();
            });
        }

        private void OnDataContextChange() {
            var cls = (Class) DataContext;

            if (cls == null || TimesView == null) {
                return;
            }

            var viewsToRemove = TimesView.Subviews.ToArray();

            foreach (var v in viewsToRemove) {
                v.RemoveFromSuperview();
            }

            var set = this.CreateBindingSet<ClassTableViewCell, Class>();

            if (cls.IsOneOff) {
                var timeLabel = new UILabel {
                    AdjustsFontSizeToFitWidth = false,
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal),
                    TextColor = Resources.Colors.SubtleText,
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                TimesView.AddSubview(timeLabel);

                set.Bind(timeLabel).SourceDescribed(
                    "Time + ' ' + StringFormat(Date, '{0:D}')"
                ).WithClearBindingKey("Times");
            }
            else {
                for (int i = 0; i < cls.Times.Count; i++) {
                    var timeLabel = new MSLMultiLengthLabel {
                        AdjustsFontSizeToFitWidth = false,
                        Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal),
                        TextColor = Resources.Colors.SubtleText,
                        TranslatesAutoresizingMaskIntoConstraints = false
                    };
                    TimesView.AddSubview(timeLabel);

                    set.Bind(timeLabel)
                        .SourceDescribed(new MvxBindingDescription(
                            nameof(timeLabel.MultiLengthText),
                            $"Times[{i}]",
                            ClassOccurrenceMultiLenghtTextValueConverter.Instance,
                            null,
                            null,
                            MvxBindingMode.OneWay
                        ))
                        .WithClearBindingKey("Times");
                }
            }

            set.Apply();

            TimesView.AddConstraints(
                TimesView.VerticalStackPanelConstraints(null, TimesView.Subviews)
            );
        }

        protected abstract void InitializeImpl();

        protected virtual void DoBind(MvxFluentBindingDescriptionSet<ClassTableViewCell, Class> set) { }
    }

    [Foundation.Register(nameof(StudentClassTableViewCell))]
    public sealed class StudentClassTableViewCell : ClassTableViewCell {
        private MSLLabel _rightLabel;
        private MSLIcon _schoolIcon;

        public StudentClassTableViewCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            _rightLabel = new MSLLabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.ForegroundMid,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            ContentView.Add(_rightLabel);

            _schoolIcon = new MSLIcon(Glyph.School) {
                TextColor = Resources.Colors.SubtleText
            };
            ContentView.Add(_schoolIcon);

            ContentView.AddConstraints(
                _rightLabel.Left().GreaterThanOrEqualTo().RightOf(TitleLabel).Plus(5),
                _rightLabel.WithSameRight(ContentView).Minus(10f).SetPriority(UILayoutPriority.Required),
                _rightLabel.WithSameCenterY(TitleLabel),
                _schoolIcon.Left().GreaterThanOrEqualTo().RightOf(TimesView).Plus(10f),
                _schoolIcon.WithSameRight(_rightLabel).SetPriority(UILayoutPriority.Required),
                _schoolIcon.WithSameTop(TimesView)
            );

            BindingContext.DataContextChanged += (s, e) => SetSchoolIconState();
            SetSchoolIconState();
        }

        protected override void DoBind(MvxFluentBindingDescriptionSet<ClassTableViewCell, Class> set) {
            set.Bind(ColorIndicator).For("LayerBackgroundColor").To(x => x.Subject.Color)
                .WithConversion("SubjectColorTo");
            set.Bind(_rightLabel).To(x => x.TeacherName);
        }

        private void SetSchoolIconState() {
            _schoolIcon.Hidden =
                User.Current == null || !User.Current.IsStudentWithUnmanagedSchool ||
                DataContext == null || !((Class) DataContext).IsFromSchool;
        }
    }

    [Foundation.Register(nameof(TeacherClassTableViewCell))]
    public sealed class TeacherClassTableViewCell : ClassTableViewCell {
        public TeacherClassTableViewCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            var schoolIcon = new MSLIcon(Glyph.School) {
                TextColor = Resources.Colors.SubtleText
            };
            ContentView.Add(schoolIcon);

            ContentView.AddConstraints(
                TimesView.WithSameRight(schoolIcon).Minus(10),
                schoolIcon.WithSameRight(ContentView).Minus(10).SetPriority(UILayoutPriority.Required),
                schoolIcon.WithSameCenterY(TitleLabel)
            );
        }

        protected override void DoBind(MvxFluentBindingDescriptionSet<ClassTableViewCell, Class> set) {
            set.Bind(ColorIndicator).For("LayerBackgroundColor")
                .To(x => x.ActualColor)
                .WithConversion("SubjectColorTo");
        }
    }

    class ClassOccurrenceMultiLenghtTextValueConverter : MvxValueConverter<ClassTime, MultiLengthText> {
        private static readonly Lazy<ClassOccurrenceMultiLenghtTextValueConverter> _instance =
            new Lazy<ClassOccurrenceMultiLenghtTextValueConverter>();

        public static ClassOccurrenceMultiLenghtTextValueConverter Instance => _instance.Value;

        private readonly IMvxValueConverter _innerConverter = new ClassOccurrenceConverter();

        protected override MultiLengthText Convert(ClassTime value, Type targetType, object parameter,
            CultureInfo culture) {
            if (value == null) {
                return null;
            }

            return new MultiLengthText(
                $"{value.Time} {_innerConverter.Convert(value, typeof(string), DateComponentNaming.Full, culture)}",
                () =>
                    $"{value.Time} {_innerConverter.Convert(value, typeof(string), DateComponentNaming.Abbreviated, culture)}",
                () =>
                    $"{value.Time} {_innerConverter.Convert(value, typeof(string), DateComponentNaming.Shortest, culture)}"
            );
        }
    }
}