using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;
using MyStudyLife.iOS.Binding;

namespace MyStudyLife.iOS.Controls.Cells {
    [MslTableViewCell(Height = Height)]
    internal abstract class RelatedTableViewCell : MvxTableViewCell {
        public const float Height = 60f;

        protected UILabel TitleLabel { get; private set; }
        protected UILabel SubTitleLabel { get; private set; }
        protected UILabel RightLabel { get; private set; }

        protected RelatedTableViewCell() {
            this.Initialize();
        }

        protected RelatedTableViewCell(IntPtr handle) : base(handle) {
            this.Initialize();
        }

        private void Initialize() {
            var view = this.ContentView;
            
            TitleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeMedium),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            TitleLabel.SetContentCompressionResistancePriority((float)UILayoutPriority.DefaultLow, UILayoutConstraintAxis.Horizontal);
            view.Add(TitleLabel);

            SubTitleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(SubTitleLabel);

            RightLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.ForegroundMid,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(RightLabel);

            view.AddConstraints(
                TitleLabel.AtLeftOf(view, 15f),
                TitleLabel.AtTopOf(view, 10f),

                SubTitleLabel.AtLeftOf(TitleLabel),
                SubTitleLabel.Top().GreaterThanOrEqualTo().BottomOf(TitleLabel),
                SubTitleLabel.AtBottomOf(view, 10f),

                RightLabel.ToRightOf(TitleLabel),
                RightLabel.AtRightOf(view, 15f),
                RightLabel.WithSameCenterY(TitleLabel)
            );

            this.InitializeImpl();

            this.DelayBind(DoBind);
        }

        /// <summary>
        ///     Use this method to add any additional
        ///     elements etc.
        /// </summary>
        protected virtual void InitializeImpl() {
            
        }

        protected abstract void DoBind();
    }
}