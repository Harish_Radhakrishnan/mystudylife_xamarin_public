using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using CoreGraphics;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Controls.Cells {
    [Foundation.Register(Identifier)]
    [MslTableViewCell(Height = Height)]
    internal sealed class SubjectTableViewCell : MvxTableViewCell {
        public const string Identifier = "SubjectTableViewCell";
        public const float Height = 44f;

        private UIView _colorIndicator;
        private MSLIcon _schoolIcon;

        public SubjectTableViewCell() {
            this.Initialize();
        }

        public SubjectTableViewCell(IntPtr handle) : base(handle) {
            this.Initialize();
        }

        private CGColor _colorBefore;

        // Prevents the subject color from being overriden when highlighted
        public override void SetHighlighted(bool highlighted, bool animated) {
            if (highlighted) {
                _colorBefore = _colorIndicator.Layer.BackgroundColor;

                base.SetSelected(true, animated);

                _colorIndicator.Layer.BackgroundColor = _colorBefore;
            }
            else {
                base.SetSelected(false, animated);
            }
        }

        private void Initialize() {
            var view = this.ContentView;

            var circle = _colorIndicator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Layer = {
                    CornerRadius = Resources.Dimens.ColorIndicatorSize/2f,
                    BackgroundColor = Resources.Colors.AccentDark.CGColor
                }
            };
            view.Add(circle);

            var titleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeMedium),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(titleLabel);

            _schoolIcon = new MSLIcon(Glyph.School) {
                TextColor = Resources.Colors.SubtleText
            };
            view.Add(_schoolIcon);

            view.AddConstraints(
                circle.WithSameLeft(view).Plus(Resources.Dimens.ScreenMargin.Left),
                circle.WithSameCenterY(view),
                circle.Width().EqualTo(Resources.Dimens.ColorIndicatorSize),
                circle.Height().EqualTo(Resources.Dimens.ColorIndicatorSize),

                titleLabel.ToRightOf(circle).Plus(15),
                titleLabel.WithSameTop(view).Plus(12.5f),

                _schoolIcon.WithSameCenterY(titleLabel),
                _schoolIcon.AtRightOf(view).Minus(Resources.Dimens.ScreenMargin.Right)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<SubjectTableViewCell, Subject>();

                set.Bind(circle).For("LayerBackgroundColor").To(x => x.Color).WithConversion("SubjectColorTo").WithFallback(Resources.Colors.Accent);
                set.Bind(titleLabel).To(x => x.Name);

                set.Apply();
            });

            BindingContext.DataContextChanged += (s, e) => SetSchoolIconState();
            SetSchoolIconState();
        }

        private void SetSchoolIconState() {
            this._schoolIcon.Hidden =
                User.Current == null || !User.Current.IsStudentWithUnmanagedSchool ||
                this.DataContext == null || !((Subject)this.DataContext).IsFromSchool;
        }
    }
}