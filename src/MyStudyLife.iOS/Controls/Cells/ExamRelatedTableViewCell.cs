using System;

namespace MyStudyLife.iOS.Controls.Cells {
    [Foundation.Register(Identifier)]
    internal sealed class ExamRelatedTableViewCell : ExamTableViewCell {
        public const string Identifier = "ExamRelatedTableViewCell";

        public override bool ShowColorIndicator => false;

        public ExamRelatedTableViewCell(IntPtr handle) : base(handle) {}
    }
}