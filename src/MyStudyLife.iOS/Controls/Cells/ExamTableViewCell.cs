using System;
using MvvmCross.Binding.BindingContext;
using Foundation;
using MyStudyLife.UI.Annotations;
using MyStudyLife.Data;

namespace MyStudyLife.iOS.Controls.Cells {
    [Register(nameof(ExamTableViewCell)), UsedImplicitly]
    public class ExamTableViewCell : ListItemTableViewCell {
        public ExamTableViewCell(IntPtr handle) : base(handle) { }

        protected override void DoBind() {
            var set = this.CreateBindingSet<ExamTableViewCell, Exam>();

            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).SourceDescribed("TimeSpanToShortString(Date) + ' ' + ContextualDate(Date)");
            set.Bind(RightLabel).SourceDescribed("Text Duration + ' min'");
            set.Bind(ColorIndicator)
                .For("LayerBackgroundColor")
                .To(x => x.Subject.Color)
                .WithConversion(Resources.Converters.SubjectColor);

            set.Apply();
        }
    }
}