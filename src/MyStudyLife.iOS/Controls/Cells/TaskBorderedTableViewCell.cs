﻿using System;
using CoreGraphics;
using Foundation;
using MyStudyLife.iOS.Binding;
using UIKit;

namespace MyStudyLife.iOS.Controls.Cells {
    [Register(nameof(TaskBorderedTableViewCell))]
    [MslTableViewCell(Height = Height)]
    internal class TaskBorderedTableViewCell : TaskTableViewCell {
        public new const float Height = 64f + (BorderedTableViewCellBase.VERTICAL_SPACING * 2f);

        private UIView _contentView;

        public override UIView ContentView => _contentView;

        public TaskBorderedTableViewCell(IntPtr handle) : base(handle) { }

        #region Highlighted

        public override void SetHighlighted(bool highlighted, bool animated) {
            if (highlighted) {
                this.ContentView.BackgroundColor = Resources.Colors.Subtle;
            }
            else {
                Animate(0.3f, () => this.ContentView.BackgroundColor = UIColor.White);
            }
        }

        #endregion

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            // We override ContentView hence not using .Subviews[0]
            this.ContentView.Frame = BorderedTableViewCellBase.DefaultContentEdgeInsets.InsetRect(new CGRect(0, 0, this.Bounds.Width, Height));
        }

        protected override void Initialize() {
            this.BackgroundColor = UIColor.Clear;

            base.ContentView.Add(this._contentView = new UIBorderedView {
                BorderColor = Resources.Colors.BorderLight,
                BorderThickness = new Thickness(0, .5f)
            });

            base.Initialize();
        }
    }

}
