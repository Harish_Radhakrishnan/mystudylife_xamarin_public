using System;
using Foundation;

namespace MyStudyLife.iOS.Controls.Cells {
    [Register(Identifier)]
    internal sealed class TaskRelatedTableViewCell : TaskTableViewCell {
        public const string Identifier = "TaskRelatedTableViewCell";

        public override bool ShowColorIndicator => false;

        public TaskRelatedTableViewCell(IntPtr handle) : base(handle) { }
    }
}