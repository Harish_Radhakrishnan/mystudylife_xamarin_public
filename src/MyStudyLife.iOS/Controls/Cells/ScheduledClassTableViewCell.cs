using System;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.Scheduling;

namespace MyStudyLife.iOS.Controls.Cells {
    [Foundation.Register(Identifier)]
    internal sealed class ScheduledClassTableViewCell : RelatedTableViewCell {
        public const string Identifier = "ScheduledClassTableCell";

        public ScheduledClassTableViewCell() { }

        public ScheduledClassTableViewCell(IntPtr handle) : base(handle) { }

        protected override void DoBind() {
            var set = this.CreateBindingSet<ScheduledClassTableViewCell, ScheduledClass>();

            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).To(x => x.Location).WithConversion("Coalesce");
            set.Bind(RightLabel).To(x => x.Time);

            set.Apply();
        }
    }
}