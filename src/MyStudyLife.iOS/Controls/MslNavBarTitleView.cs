using System;
using CoreGraphics;
using System.Windows.Input;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    internal sealed class MslNavBarTitleView : UIView {
        public UILabel TitleLabel { get; private set; }

        public UILabel SubTitleLabel { get; private set; }

        private string _title, _subTitle;
        private bool _showArrow;

        public string Title {
            get { return _title; }
            set {
                if (_title == value) {
                    return;
                }

                _title = value;

                if (TitleLabel != null) {
                    TitleLabel.Text = value;

                    this.SetNeedsLayout();
                    this.LayoutIfNeeded();
                    this.SetNeedsDisplay();
                }
            }
        }

        public string SubTitle {
            get { return _subTitle; }
            set {
                if (_subTitle == value) {
                    return;
                }

                _subTitle = value;

                if (SubTitleLabel != null) {
                    SubTitleLabel.Text = value;

                    this.SetNeedsLayout();
                    this.LayoutIfNeeded();
                    this.SetNeedsDisplay();
                }
            }
        }

        public bool ShowArrow {
            get { return _showArrow; }
            set {
                if (_showArrow != value) {
                    _showArrow = value;

                    this.SetNeedsDisplay();
                }
            }
        }

        public event EventHandler Tapped;

#pragma warning disable 0649
        /// <remarks>
        ///     CanExecute not impl.
        /// </remarks>
        public ICommand TappedCommand;
#pragma warning restore 0649
        public MslNavBarTitleView()
            : base(new CGRect(0, 0, 200, 44)) {
            this.BackgroundColor = UIColor.Clear;
            this.AutosizesSubviews = true;
            this.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin |
                                    UIViewAutoresizing.FlexibleBottomMargin | UIViewAutoresizing.FlexibleTopMargin;

            TitleLabel = new UILabel {
                Text = this.Title,
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.BoldSystemFontOfSize(17)
            };

            SubTitleLabel = new UILabel {
                Text = this.SubTitle,
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.SystemFontOfSize(12)
            };

            this.Add(TitleLabel);
            this.Add(SubTitleLabel);

            this.AddGestureRecognizer(new UITapGestureRecognizer(
                () => {
                    Tapped?.Invoke(this, EventArgs.Empty);
                    TappedCommand?.Execute(null);
                }
            ));
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            if (SubTitleLabel != null && !String.IsNullOrEmpty(SubTitleLabel.Text)) {
                var y = (this.Bounds.Height - 34) / 2f;

                TitleLabel.Frame = new CGRect(0, y, 200, 19);
                SubTitleLabel.Frame = new CGRect(0, 19 + y, 200, 15);
            }
            else {
                TitleLabel.Frame = this.Bounds;
            }
        }

        public override void Draw(CGRect rect) {
            base.Draw(rect);

            if (!ShowArrow || _title == null || TitleLabel == null) {
                return;
            }

            var titleSize = _title.StringSize(TitleLabel.Font);

            bool hasSubTitle = SubTitleLabel != null && !String.IsNullOrEmpty(SubTitleLabel.Text);

            // + 10px spacing
            var triangleStartX = TitleLabel.Bounds.Left + ((TitleLabel.Bounds.Width - titleSize.Width)/2) + titleSize.Width + 5;
            var triangleStartY = ((this.Bounds.Height - (hasSubTitle ? 34 : 19)) / 2f) + (titleSize.Height * 1/3f);

            var ctx = UIGraphics.GetCurrentContext();

            const float triangleWidth = 6f; // 11px
            const float triangleHeight = 5f; // 9px

            // 9px x 11px target
            ctx.BeginPath();
            ctx.MoveTo(triangleStartX, triangleStartY);
            ctx.AddLineToPoint(triangleStartX + triangleWidth, triangleStartY);
            ctx.AddLineToPoint(triangleStartX + (triangleWidth / 2), triangleStartY + triangleHeight);
            ctx.ClosePath();

            ctx.SetFillColor(255,255,255,1);
            ctx.FillPath();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (TitleLabel != null) {
                    TitleLabel.Dispose();
                    TitleLabel = null;
                }

                if (SubTitleLabel != null) {
                    SubTitleLabel.Dispose();
                    SubTitleLabel = null;
                }
            }

            base.Dispose(disposing);
        }

        private bool _touchDown;

        public override void TouchesBegan(NSSet touches, UIEvent evt) {
            base.TouchesBegan(touches, evt);

            if (Tapped != null || TappedCommand != null) {
                this._touchDown = true;
                this.Alpha = 0.3f;
            }
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt) {
            base.TouchesEnded(touches, evt);

            this.OnTouchEnd();
        }

        public override void TouchesCancelled(NSSet touches, UIEvent evt) {
            base.TouchesCancelled(touches, evt);

            this.OnTouchEnd();
        }

        private void OnTouchEnd() {
            if ((Tapped != null || TappedCommand != null) && this._touchDown) {
                Animate(0.3f, () => this.Alpha = 1f);

                this._touchDown = false;
            }
            else {
                this._touchDown = false;
            }
        }
    }
}