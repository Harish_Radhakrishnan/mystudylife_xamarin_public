﻿using Cirrious.FluentLayouts.Touch;
using Foundation;
using MyStudyLife.UI;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Register(nameof(MslLonelyView))]
    internal sealed class MslLonelyView : MslView {
        private readonly MSLIcon _iconView;
        private readonly UILabel _labelView;
        private readonly UILabel _subLabelView;

        private Glyph _glyph;
        private UIColor _textColor = Resources.Colors.ForegroundLight;
        private string _text;
        private string _subText;

        public bool AutoAlignInSuperview { get; set; } = true;

        public UIColor TextColor {
            get => _textColor;
            set {
                _textColor = value;

                if (_labelView != null) {
                    _labelView.TextColor = value;
                }
                if (_subLabelView != null) {
                    _subLabelView.TextColor = value;
                }
            }
        }

        public Glyph Glyph {
            get => _glyph;
            set {
                _glyph = value;

                if (_iconView != null) {
                    _iconView.Glyph = value;
                }
            }
        }

        public string Text {
            get => _text;
            set {
                _text = value;

                if (_labelView != null) {
                    _labelView.Text = value;
                }
            }
        }

        public string SubText {
            get => _subText;
            set {
                _subText = value;

                if (_subLabelView != null) {
                    _subLabelView.Text = value;
                }
            }
        }

        public MslLonelyView(Glyph glyph) {
            this.Glyph = glyph;

            _iconView = new MSLIcon(glyph, 6f) {
                TextColor = Resources.Colors.ForegroundLight,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this.Add(_iconView);

            _labelView = new UILabel {
                Text = this.Text,
                TextAlignment = UITextAlignment.Center,
                TextColor = this.TextColor,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Font = Resources.Fonts.OfSize(FontSize.Subhead),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            this.Add(_labelView);

            _subLabelView = new UILabel {
                Text = this.SubText,
                TextAlignment = UITextAlignment.Center,
                TextColor = this.TextColor,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            this.Add(_subLabelView);

            this.AddConstraints(
                _iconView.AtTopOf(this),
                _iconView.WithSameCenterX(this),

                _labelView.FullWidthOf(this, Resources.Dimens.DefaultHorizontalMargin),
                _labelView.Below(_iconView, 10f),

                _subLabelView.FullWidthOf(this, Resources.Dimens.DefaultHorizontalMargin),
                _subLabelView.Below(_labelView),
                _subLabelView.AtBottomOf(this)
            );
        }

        public override void MovedToSuperview() {
            base.MovedToSuperview();

            if (AutoAlignInSuperview) {
                Superview.AddConstraints(
                    this.AtLeftOf(Superview, 15f),
                    this.AtRightOf(Superview, 15f),
                    this.WithSameCenterY(Superview)
                );
            }
        }
    }
}
