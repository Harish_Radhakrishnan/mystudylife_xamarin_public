﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public class MSLPageViewController : UIPageViewController {
        private int _nextIndex;
        private int _currentIndex;

        public event EventHandler<CurrentIndexChangedEventArgs> CurrentIndexChanged;

        public int CurrentIndex {
            get { return _currentIndex; }
            set {
                if (_currentIndex != value) {
                    _currentIndex = value;

                    CurrentIndexChanged?.Invoke(this, new CurrentIndexChangedEventArgs(value));
                }
            }
        }

        public new IIndexedUIPagerViewControllerDataSource DataSource {
            get { return base.DataSource as IIndexedUIPagerViewControllerDataSource; }
            set { base.DataSource = value; }
        }

        public MSLPageViewController(UIPageViewControllerTransitionStyle style, UIPageViewControllerNavigationOrientation navigationOrientation)
            : base(style, navigationOrientation) { }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.WillTransition += OnWillTransition;
            this.DidFinishAnimating += OnDidFinishAnimating;
        }

        private void OnWillTransition(object sender, UIPageViewControllerTransitionEventArgs e) {
            this._nextIndex = this.DataSource.GetViewControllerIndex(e.PendingViewControllers.First());
        }

        private void OnDidFinishAnimating(object sender, UIPageViewFinishedAnimationEventArgs e) {
            if (e.Completed) {
                this.CurrentIndex = this._nextIndex;
            }

            this._nextIndex = 0;
        }

        public override void SetViewControllers(UIViewController[] viewControllers, UIPageViewControllerNavigationDirection direction, bool animated, UICompletionHandler completionHandler) {
            base.SetViewControllers(viewControllers, direction, animated, finished => {
                completionHandler?.Invoke(finished);

                if (finished) {
                    this.CurrentIndex = this.DataSource.GetViewControllerIndex(viewControllers.First());
                }
            });
        }

        public override async Task<bool> SetViewControllersAsync(UIViewController[] viewControllers, UIPageViewControllerNavigationDirection direction, bool animated) {
            var finished = await base.SetViewControllersAsync(viewControllers, direction, animated);

            if (finished) {
                this.CurrentIndex = this.DataSource.GetViewControllerIndex(viewControllers.First());
            }

            return finished;
        }

        public class CurrentIndexChangedEventArgs : EventArgs {
            public int NewIndex { get; }

            public CurrentIndexChangedEventArgs(int newIndex) {
                this.NewIndex = newIndex;
            }
        }
    }

    public interface IIndexedUIPagerViewControllerDataSource : IUIPageViewControllerDataSource {
        int GetViewControllerIndex(UIViewController viewController);
    }

    public class FixedUIPageViewControllerDataSource : UIPageViewControllerDataSource, IIndexedUIPagerViewControllerDataSource {
        private readonly IList<UIViewController> _viewControllers;

        public IEnumerable<UIViewController> ViewControllers => _viewControllers;

        public FixedUIPageViewControllerDataSource(params UIViewController[] viewControllers) {
            this._viewControllers = viewControllers.ToList();
        }

        public override nint GetPresentationCount(UIPageViewController pageViewController) => this._viewControllers.Count;

        // -1 prevents dots appearing
        public override nint GetPresentationIndex(UIPageViewController pageViewController) => -1;

        public override UIViewController GetPreviousViewController(UIPageViewController pageViewController, UIViewController referenceViewController) {
            int i = this._viewControllers.IndexOf(referenceViewController) - 1;

            if (i < 0) {
                return null;
            }
            
            return this._viewControllers[i];
        }

        public override UIViewController GetNextViewController(UIPageViewController pageViewController, UIViewController referenceViewController) {
            int i = this._viewControllers.IndexOf(referenceViewController) + 1;

            if (_viewControllers.Count <= i) {
                return null;
            }

            return this._viewControllers[i];
        }

        public int GetViewControllerIndex(UIViewController viewController)
            => this._viewControllers.IndexOf(viewController);
        
        public void AddViewController(UIViewController viewController)
            => _viewControllers.Add(viewController);
        public void InsertViewController(int index, UIViewController viewController)
            => _viewControllers.Insert(index, viewController);
        public bool RemoveViewController(UIViewController viewController)
            => _viewControllers.Remove(viewController);
    }
}
