using System;
using CoreGraphics;
using UIKit;
using CoreAnimation;

namespace MyStudyLife.iOS.Controls {
    /// <remarks>
    ///     Doesn't work when used as UITableView header
    /// </remarks>
    [Foundation.Register("MSLLabel")]
    public class MSLLabel : UILabel, IBorderedView {
        public UIEdgeInsets ContentEdgeInsets { get; set; }

        public UIColor BorderColor { get; set; }

        public Thickness BorderThickness { get; set; }

        public bool TextOverflowFade { get; set; }

        public override string Text {
            get { return base.Text; }
            set {
                base.Text = value;

                if (this.TextOverflowFade && this.AttributedText != null && this.AttributedText.Size.Width > this.Bounds.Width) {
                    this.Layer.Mask = new CAGradientLayer {
                        Colors = new[] {
                            UIColor.Black.CGColor,
                            UIColor.Clear.CGColor
                        },
                        StartPoint = new CGPoint(0.8f, 0),
                        EndPoint = new CGPoint(1f, 0)
                    };
                }
                else {
                    this.Layer.Mask = null;
                }
            }
        }

        public override bool Hidden {
            get { return base.Hidden; }
            set {
                base.Hidden = value;

                this.InvalidateIntrinsicContentSize();
            }
        }

        public override CGSize IntrinsicContentSize {
            get { return this.Hidden ? new CGSize(NoIntrinsicMetric, 0f) : base.IntrinsicContentSize; }
        }

        public MSLLabel() {
            this.Initialize();
        }

        public MSLLabel(CGRect frame) : base(frame) {
            this.Initialize();
        }

        private void Initialize() {
            this.TextColor = Resources.Colors.Foreground;
            this.TranslatesAutoresizingMaskIntoConstraints = false;
        }
        
        public override CGRect TextRectForBounds(CGRect bounds, nint numberOfLines) {
            var insets = this.ContentEdgeInsets;
            var rect = base.TextRectForBounds(insets.InsetRect(bounds), numberOfLines);

            rect.X -= insets.Left;
            rect.Y -= insets.Top;

            rect.Size = new CGSize(
                rect.Size.Width + (insets.Left + insets.Right),
                rect.Size.Height + (insets.Top + insets.Bottom)
            );

            return rect;
        }

        public override void DrawText(CGRect rect) {
            base.DrawText(ContentEdgeInsets.InsetRect(rect));
        }

        public override void DrawRect(CGRect area, UIViewPrintFormatter formatter) {
            BorderedViewHelper.DrawBorder(area, this.BorderColor, this.BorderThickness);
            
            base.DrawRect(area, formatter);
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            if (Layer.Mask != null) {
                Layer.Mask.Frame = Layer.Bounds;
            }
        }
    }
}