using Cirrious.FluentLayouts.Touch;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    /// <summary>
    ///     Like a <see cref="UIView"/>, but has zero layout
    ///     using constraints (and automatically disables affected constraints)
    ///     when <c>Hidden == true</c>, as opposed to the default behaviour of not drawing.
    /// </summary>
    public class MslView : UIView {
        public HideMode HideMode { get; private set; } = HideMode.Height;

        public override bool Hidden {
            get => base.Hidden;
            set {
                if (value == base.Hidden) return;
                
                base.Hidden = value;

                const string identifier = "$MSLViewHidden$";

                bool hasHiddenConstraint = false;
                    
                foreach (var constraint in Constraints) {
                    if (constraint.GetIdentifier() == identifier) {
                        hasHiddenConstraint = true;

                        constraint.Active = value;
                    }
                    else {
                        // NB: Doesn't seem to reactivate
                        //constraint.Active = !value;
                    }
                }

                if (!hasHiddenConstraint && value) {
                    this.AddConstraints(
                        (HideMode == HideMode.Height ? this.Height() : this.Width())
                        .EqualTo(0f)
                        .SetPriority(UILayoutPriority.Required)
                        .WithIdentifier(identifier)
                    );
                }
            }
        }
    }

    public enum HideMode {
        Width,
        Height
    }
}