using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public class UIBorderedView : UIView, IBorderedView {
        private CALayer _leftBorderLayer, _topBorderLayer, _rightBorderLayer, _bottomBorderLayer;

        private UIColor _borderColor;
        private Thickness _borderThickness;
        private BorderDrawingMode _borderDrawingMode;

        public UIColor BorderColor {
            get { return _borderColor; }
            set {
                if (!Equals(_borderColor, value)) {
                    _borderColor = value;

                    this.SetBorders();
                }
            }
        }

        public Thickness BorderThickness {
            get { return _borderThickness; }
            set {
                if (!Equals(_borderThickness, value)) {
                    _borderThickness = value;

                    this.SetBorders();
                }
            }
        }

        public BorderDrawingMode BorderDrawingMode {
            get { return _borderDrawingMode; }
            set {
                if (!Equals(_borderDrawingMode, value)) {
                    _borderDrawingMode = value;

                    if (_borderDrawingMode == BorderDrawingMode.Outside) {
                        this.Layer.MasksToBounds = false;
                    }

                    this.SetBorders();
                }
            }
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            this.SetBorders();
        }

        private void SetBorders() {
            var scaledBorderThickness = new Thickness(
                this.BorderThickness.Left * (float)UIScreen.MainScreen.Scale,
                this.BorderThickness.Top * (float)UIScreen.MainScreen.Scale,
                this.BorderThickness.Right * (float)UIScreen.MainScreen.Scale,
                this.BorderThickness.Bottom * (float)UIScreen.MainScreen.Scale
            );
            bool drawOutside = this.BorderDrawingMode == BorderDrawingMode.Outside;

            // TODO: Draw outside is half implemented at the moment, will leave gaps if borders two adjoining sides

            this.SetBorder(ref _leftBorderLayer, new CGRect(
                drawOutside ? -scaledBorderThickness.Left : 0, 0, scaledBorderThickness.Left, this.Bounds.Height
            ));
            this.SetBorder(ref _topBorderLayer, new CGRect(
                0, drawOutside ? -scaledBorderThickness.Top : 0, this.Bounds.Width, scaledBorderThickness.Top
            ));
            this.SetBorder(ref _rightBorderLayer, new CGRect(
                this.Bounds.Right - (drawOutside ? 0 : scaledBorderThickness.Right), 0,
                scaledBorderThickness.Right, this.Bounds.Height
            ));
            this.SetBorder(ref _bottomBorderLayer, new CGRect(
                0, this.Bounds.Bottom + (drawOutside ? 0 : -scaledBorderThickness.Bottom), this.Bounds.Width, scaledBorderThickness.Bottom
            ));
        }

        private void SetBorder(ref CALayer borderLayer, CGRect rect) {
            if (rect.Width > 0 && rect.Height > 0) {
                if (borderLayer == null) {
                    borderLayer = new CALayer();
                    this.Layer.AddSublayer(borderLayer);
                }

                borderLayer.Frame = rect;
                borderLayer.BackgroundColor = this.BorderColor?.CGColor;

                this.SetNeedsDisplayInRect(rect);
            }
            else if (borderLayer != null) {
                var oldRect = borderLayer.Frame;

                borderLayer.RemoveFromSuperLayer();
                borderLayer.Dispose();
                borderLayer = null;

                this.SetNeedsDisplayInRect(oldRect);
            }
        }
    }

    public enum BorderDrawingMode {
        /// <summary>
        ///     Draw the border inside the view's bounds.
        /// </summary>
        Inside,
        /// <summary>
        ///     Draw the border outside the view's bounds.
        /// </summary>
        Outside
    }
}