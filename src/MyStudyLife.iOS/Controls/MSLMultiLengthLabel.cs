﻿using System;
using CoreGraphics;
using Foundation;
using MyStudyLife.UI.Utilities;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Register(nameof(MSLMultiLengthLabel))]
    public class MSLMultiLengthLabel : MSLLabel {

        private MultiLengthText _multiLengthText;

        public MultiLengthText MultiLengthText {
            get { return _multiLengthText; }
            set {
                if (!ReferenceEquals(_multiLengthText, value)) {
                    _multiLengthText = value;
                    this.SetMultiLengthText(this.Bounds.Size);
                }
            }
        }

        public override string Text {
            get { return base.Text; }
            set { throw new NotSupportedException($"Cannot set text directly in {nameof(MultiLengthText)}"); }
        }

        public override CGRect Bounds {
            get { return base.Bounds; }
            set {
                base.Bounds = value;
                SetMultiLengthText(value.Size);
            }
        }

        private void SetMultiLengthText(CGSize size) {
            var mlt = this.MultiLengthText;
            NSString text = NSString.Empty;

            if (mlt != null) {
                for (int i = 0; i < 3; i++) {
                    switch (i) {
                        case 0:
                            text = new NSString(mlt.Full);
                            break;
                        case 1:
                            if (mlt.Abbreviated == null) {
                                text = new NSString(mlt.Abbreviated);
                            }
                            break;
                        case 2:
                            if (mlt.Shortest != null) {
                                text = new NSString(mlt.Shortest);
                            }
                            break;
                        default:
                            throw new Exception();
                    }

                    var textWidth = text.GetSizeUsingAttributes(
                        new UIStringAttributes {
                            Font = this.Font
                        }
                    ).Width;

                    // If it fits, break
                    if (0 < size.Width && textWidth < size.Width) {
                        break;
                    }
                }
            }

            base.Text = text;
        }
    }
}
