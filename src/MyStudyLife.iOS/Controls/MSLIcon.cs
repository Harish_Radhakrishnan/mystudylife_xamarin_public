using System;
using CoreGraphics;
using Foundation;
using MyStudyLife.UI;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    // Our glyphs are based off being 16pts large,
    // wrapper class to deal with that nicely :-)
    [Register(nameof(MSLIcon))]
    internal class MSLIcon : UILabel {
        private Glyph _glyph;

        public sealed override bool TranslatesAutoresizingMaskIntoConstraints {
            get { return base.TranslatesAutoresizingMaskIntoConstraints; }
            set { base.TranslatesAutoresizingMaskIntoConstraints = value; }
        }

        public sealed override bool AdjustsFontSizeToFitWidth {
            get { return base.AdjustsFontSizeToFitWidth; }
            set { base.AdjustsFontSizeToFitWidth = value; }
        }

        public Glyph Glyph {
            get { return _glyph; }
            set {
                _glyph = value;
                this.Text = Char.ConvertFromUtf32((int)value);
            }
        }

        public float Scale {
            get { return (float)this.Font.PointSize/16f; }
            set { this.Font = this.Font.WithSize((nfloat)value * 16f); }
        }

        public override bool Hidden {
            get { return base.Hidden; }
            set {
                base.Hidden = value;

                this.InvalidateIntrinsicContentSize();
            }
        }

        public override CGSize IntrinsicContentSize {
            get { return this.Hidden ? new CGSize(NoIntrinsicMetric, 0f) : base.IntrinsicContentSize; }
        }

        public MSLIcon(Glyph glyph, float scale = 1f) {
            this.Initialize(scale * 16);

            this.Glyph = glyph;
            this.TranslatesAutoresizingMaskIntoConstraints = false;
            this.AdjustsFontSizeToFitWidth = false;
        }

        private void Initialize(float fontSize) {
            this.Font = UIFont.FromName("msl", fontSize);
        }
    }
}