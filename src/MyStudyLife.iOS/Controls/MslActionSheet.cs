using UIKit;

namespace MyStudyLife.iOS.Controls {
    internal sealed class MslActionSheet : UIActionSheet {
        public UIColor ButtonTextColor { get; set; }

        public UIColor DestructiveButtonTextColor { get; set; }

        public MslActionSheet() {
            this.ButtonTextColor = Resources.Colors.Accent;
            this.DestructiveButtonTextColor = Resources.Colors.PaletteARed;
        }

        // This doesn't seem to work in iOS 8 anymore?
        //public override void ShowInView(UIView view) {
        //    base.ShowInView(view);

        //    var btns = this.Subviews.OfType<UIButton>().ToList();

        //    for (int i = 0; i < btns.Count; i++) {
        //        var btn = btns[i];

        //        btn.SetTitleColor(
        //            this.DestructiveButtonIndex == i ? DestructiveButtonTextColor : ButtonTextColor,
        //            UIControlState.Normal
        //        );
        //    }
        //}
    }
}