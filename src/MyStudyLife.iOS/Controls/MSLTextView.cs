using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Foundation.Register("MSLTextView")]
    public sealed class MSLTextView : UITextView {
        // TODO: Implement placeholder... https://github.com/gcamp/GCPlaceholderTextView/blob/master/GCPlaceholderTextView/GCPlaceholderTextView.m

        public MSLTextView() {
            this.TextColor = Resources.Colors.Foreground;
            this.TranslatesAutoresizingMaskIntoConstraints = false;
        }
    }
}