using System;
using UIKit;
using CoreGraphics;

namespace MyStudyLife.iOS.Controls {
    public class UIAlignedImageView : UIImageView {
        private bool _enableScaleUp = true, _enableScaleDown = true;

        private UIImageView _childImageView;

        private UIImageViewAlignment _alignment;

        public UIImageViewAlignment Alignment {
            get { return _alignment; }
            set {
                if (_alignment != value) {
                    _alignment = value;
                    this.SetNeedsLayout();
                }
            }
        }

        public override UIImage Image {
            get { return _childImageView?.Image ?? base.Image; }
            set {
                if (_childImageView != null) {
                    _childImageView.Image = value;

                    this.SetNeedsLayout();
                    this.SetNeedsDisplay();
                    this.LayoutIfNeeded();
                }
            }
        }

        public override UIViewContentMode ContentMode {
            get { return base.ContentMode; }
            set {
                base.ContentMode = value;

                if (_childImageView != null) {
                    _childImageView.ContentMode = value;
                }

                this.SetNeedsLayout();
            }
        }

        public override bool Highlighted {
            get { return base.Highlighted; }
            set {
                base.Highlighted = value;
                this.Layer.Contents = null;
            }
        }

        // LayoutMargins isn't present on iOS7, but since we're
        // doing the layout, we can emulate it.
        private UIEdgeInsets _layoutMargins;

        public override UIEdgeInsets LayoutMargins {
            get {
                if (this.RespondsToSelector(new ObjCRuntime.Selector("getLayoutMargins"))) {
                    return base.LayoutMargins;
                }
                else {
                    return _layoutMargins;
                }
            }
            set {
                if (this.RespondsToSelector(new ObjCRuntime.Selector("setLayoutMargins"))) {
                    base.LayoutMargins = value;
                }
                else if (!_layoutMargins.Equals(value)) {
                    _layoutMargins = value;

                    this.SetNeedsLayout();
                    this.SetNeedsDisplay();
                    this.LayoutIfNeeded();
                }
            }
        }

        public UIAlignedImageView() {
            this.Initialize();
        }

        public UIAlignedImageView(CGRect frame) : base(frame) {
            this.Initialize();
        }

        private void Initialize() {
            this.Add(_childImageView = new UIImageView(this.Bounds) {
                AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight,
                ContentMode = this.ContentMode
            });

            // Move any image we might have from this container to the real image view
            if (base.Image != null) {
                _childImageView.Image = base.Image;
                base.Image = null;
            }
        }

        public override void LayoutSubviews() {
            CGSize actualSize = this.GetActualContentSize();

            // Start centered
            CGRect actualFrame = new CGRect((this.Bounds.Width - actualSize.Width) / 2, (this.Bounds.Height - actualSize.Height) / 2, actualSize.Width, actualSize.Height);

            if ((_alignment & UIImageViewAlignment.Left) != 0) {
                actualFrame.X = this.LayoutMargins.Left;
            }
            else if ((_alignment & UIImageViewAlignment.Right) != 0) {
                actualFrame.X = (this.Bounds.GetMaxX() - this.LayoutMargins.Right) - actualFrame.Width;
            }

            if ((_alignment & UIImageViewAlignment.Top) != 0) {
                actualFrame.Y = this.LayoutMargins.Top;
            }
            else if ((_alignment & UIImageViewAlignment.Bottom) != 0) {
                actualFrame.Y = (this.Bounds.GetMaxY() - this.LayoutMargins.Bottom) - actualFrame.Height;
            }

            _childImageView.Frame = actualFrame;

            // Make sure we clear the contents of this container layer, since it refreshes from the image property once in a while.
            this.Layer.Contents = null;
        }

        private CGSize GetActualContentSize() {
            CGSize size = new CGSize(
                this.Bounds.Width - (this.LayoutMargins.Left + this.LayoutMargins.Right),
                this.Bounds.Height - (this.LayoutMargins.Top + this.LayoutMargins.Bottom)
            );

            if (this.Image == null) {
                return size;
            }

            nfloat scaleX = size.Width / _childImageView.Image.Size.Width;
            nfloat scaleY = size.Height / _childImageView.Image.Size.Height;

            switch (this.ContentMode) {
                case UIViewContentMode.ScaleAspectFit: {
                        nfloat scale = NMath.Min(scaleX, scaleY);

                        if ((scale > 1.0f && !_enableScaleUp) || (scale < 1.0f && !_enableScaleDown)) {
                            scale = 1.0f;
                        }

                        size = new CGSize(_childImageView.Image.Size.Width * scale, _childImageView.Image.Size.Height * scale);
                    }
                    break;
                case UIViewContentMode.ScaleAspectFill: {
                        nfloat scale = NMath.Max(scaleX, scaleY);

                        if ((scale > 1.0f && !_enableScaleUp) || (scale < 1.0f && !_enableScaleDown)) {
                            scale = 1.0f;
                        }

                        size = new CGSize(_childImageView.Image.Size.Width * scale, _childImageView.Image.Size.Height * scale);
                    }
                    break;
                case UIViewContentMode.ScaleToFill: {
                        if ((scaleX > 1.0f && !_enableScaleUp) || (scaleX < 1.0f && !_enableScaleDown)) {
                            scaleX = 1.0f;
                        }

                        if ((scaleY > 1.0f && !_enableScaleUp) || (scaleY < 1.0f && !_enableScaleDown)) {
                            scaleY = 1.0f;
                        }

                        size = new CGSize(_childImageView.Image.Size.Width * scaleX, _childImageView.Image.Size.Height * scaleY);
                    }
                    break;
                default:
                    size = _childImageView.Image.Size;
                    break;
            }

            return size;
        }
    }

    [Flags]
    public enum UIImageViewAlignment {
        Center = 0,

        Left = 1,
        Right = 2,
        Top = 4,
        Bottom = 8,

        BottomLeft = Bottom | Left,
        BottomRight = Bottom | Right,
        TopLeft = Top | Left,
        TopRight = Top | Right
    }
}