using System;
using CoreGraphics;
using System.Linq;
using Foundation;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.iOS.Views.Calendar.Week;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public class PagingCollectionView : MslCollectionView {
        public PagingCollectionView(IntPtr handle) : base(handle) {
            Initialize();
        }

        public PagingCollectionView(CGRect frame, UICollectionViewLayout layout) : base(frame, layout) {
            Initialize();
        }

        private void Initialize() {
            ShowsHorizontalScrollIndicator = false;
            ShowsVerticalScrollIndicator = false;
            PagingEnabled = true;
        }

        public override void SafeAreaInsetsDidChange() {
            base.SafeAreaInsetsDidChange();

            // We're using the SafeAreaInsets from the paging collection as the SafeAreaInsets and SafeAreaInsetsDidChange
            // override on the CalendarWeekCollectionViewCell change during paging between weeks
            foreach (var cell in VisibleCells.Cast<CalendarWeekCollectionViewCell>()) {
                cell.ContentInset = SafeAreaInsets;
            }
        }
    }

    public sealed class PagingCollectionViewLayout : UICollectionViewFlowLayout {
        public PagingCollectionViewLayout() {
            MinimumLineSpacing = 0;
            MinimumInteritemSpacing = 0;
            ScrollDirection = UICollectionViewScrollDirection.Horizontal;
        }
    }

    public class MvxPagingCollectionViewSource : MvxCollectionViewSource {
        #region ctor

        public MvxPagingCollectionViewSource(PagingCollectionView collectionView) : base(collectionView) {
            Initialize();
        }
        public MvxPagingCollectionViewSource(PagingCollectionView collectionView, NSString defaultCellIdentifier)
            : base(collectionView, defaultCellIdentifier) {
            Initialize();
        }

        #endregion

        private void Initialize() {
            SelectedItemChanged += OnSelectedItemChanged;
        }

        private void OnSelectedItemChanged(object sender, EventArgs e) {
            ScrollToItem(SelectedItem, false);
        }

        public void ScrollToItem(object item, bool animated) {
            if (item == null || ItemsSource == null) {
                CollectionView.SetContentOffset(CGPoint.Empty, animated);

                return;
            }

            var indexOfItem = ItemsSource.Cast<object>().ToList().IndexOf(item);

            // Prevents being "stuck" at the beginning of the scroll view
            // when inserting new weeks
            if (indexOfItem == 0) {
                indexOfItem += 1;
            }

            CollectionView.SetContentOffset(new CGPoint(
                indexOfItem * CollectionView.Bounds.Width,
                0
                ),
                animated
            );
        }

        public override void DecelerationEnded(UIScrollView scrollView) {
            var currentPage = NMath.Round(CollectionView.ContentOffset.X / CollectionView.Bounds.Width);

            var item = GetItemAt(NSIndexPath.FromItemSection((nint)currentPage, 0));

            // ReSharper disable once RedundantCheckBeforeAssignment
            if (item != SelectedItem) {
                SelectedItem = item;
            }
        }
    }
}