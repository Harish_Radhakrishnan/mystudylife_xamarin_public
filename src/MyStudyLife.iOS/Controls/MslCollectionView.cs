using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public class MslCollectionView : UICollectionView {
        #region ctor(...)

        public MslCollectionView(NSCoder coder) : base(coder) {}
        public MslCollectionView(NSObjectFlag t) : base(t) {}
        public MslCollectionView(IntPtr handle) : base(handle) {}
        public MslCollectionView(CGRect frame, UICollectionViewLayout layout) : base(frame, layout) {}

        #endregion
    }
}