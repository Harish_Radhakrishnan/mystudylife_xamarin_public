﻿using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    /// <summary>
    ///     A view with material design style shadow.
    /// </summary>
    /// <remarks>
    ///     Shadow courtesy of 
    ///     https://medium.com/@Florian/freebie-google-material-design-shadow-helper-2a0501295a2d
    /// </remarks>
    [Register(nameof(MSLCardView))]
    public class MSLCardView : MslView {
        private CALayer[] _shadowSublayers;

        private int _elevation = 1;

        public int CornerRadius { get; set; } = 2;

        public int Elevation {
            get { return _elevation; }
            set {
                if (!Equals(_elevation, value)) {
                    if (_elevation < 0 || 5 < _elevation) {
                        throw new ArgumentOutOfRangeException("Must be between 0 and 5");
                    }

                    _elevation = value;

                    this.SetNeedsLayout();
                }
            }
        }

        public override UIColor BackgroundColor {
            get { return base.BackgroundColor; }
            set {
                base.BackgroundColor = value;

                _shadowSublayers?.Apply(x => x.BackgroundColor = value?.CGColor);
            }
        }

        public MSLCardView() {
            this.BackgroundColor = UIColor.White;

            _shadowSublayers = new CALayer[2];

            for (int i = 0; i < _shadowSublayers.Length; i++) {
                this.Layer.AddSublayer(_shadowSublayers[i] = new CALayer {
                    MasksToBounds = false,
                    BackgroundColor = this.BackgroundColor.CGColor
                });
            }
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();
            
            var path = UIBezierPath.FromRoundedRect(UIEdgeInsets.Zero.InsetRect(this.Bounds), CornerRadius).CGPath;

            var first = this._shadowSublayers[0];
            var second = this._shadowSublayers[1];

            first.Frame = this.Bounds;
            second.Frame = this.Bounds;

            first.ShadowPath = path;
            second.ShadowPath = path;

            switch (Elevation) {
                case 0:
                    BoxShadow(first, 0, 0, 0, 0);
                    BoxShadow(second, 0, 0, 0, 0);
                    break;
                case 1:
                    BoxShadow(first, 0, 1, 3, .12f);
                    BoxShadow(second, 0, 1, 2, .24f);
                    break;
                case 2:
                    BoxShadow(first, 0, 3, 6, .16f);
                    BoxShadow(second, 0, 3, 6, .23f);
                    break;
                case 3:
                    BoxShadow(first, 0, 10, 20, .19f);
                    BoxShadow(second, 0, 6, 6, .23f);
                    break;
                case 4:
                    BoxShadow(first, 0, 14, 28, .25f);
                    BoxShadow(second, 0, 10, 10, .22f);
                    break;
                case 5:
                    BoxShadow(first, 0, 19, 38, .30f);
                    BoxShadow(second, 0, 15, 12, .22f);
                    break;
            }
        }

        private static void BoxShadow(CALayer layer, float xOffset, float yOffset, float blur, float alpha)
            => BoxShadow(layer, xOffset, yOffset, blur, new CGColor(0, alpha));

        private static void BoxShadow(CALayer layer, float xOffset, float yOffset, float blur, CGColor color) {
            layer.ShadowOpacity = .5f;
            layer.ShadowOffset = new CGSize(xOffset, yOffset);
            layer.ShadowRadius = blur;
            layer.ShadowColor = color;
        }
    }
}