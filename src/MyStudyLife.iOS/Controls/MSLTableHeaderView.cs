﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    [Register(nameof(MSLTableHeaderView))]
    public class MSLTableHeaderView : UIView {
        private readonly nfloat _tableViewWidth;
        private readonly UILabel _label;

        private UIEdgeInsets _contentEdgeInsets = new UIEdgeInsets(
            10f, Resources.Dimens.DefaultHorizontalMargin,
            10f, Resources.Dimens.DefaultHorizontalMargin
        );

        public UIEdgeInsets ContentEdgeInsets {
            get { return _contentEdgeInsets; }
            set {
                _contentEdgeInsets = value;

                this.SetNeedsLayout();
            }
        }

        public UIColor TextColor {
            get { return _label.TextColor; }
            set { _label.TextColor = value; }
        }

        public MSLTableHeaderView(UITableView tableView, string text) {
            this.BackgroundColor = Resources.Colors.Background;
            
            // TODO: Can we do this elsewhere like in SizeThatFits
            this._tableViewWidth = tableView.Bounds.Width;

            this.Add(_label = new UILabel {
                Font = Resources.Fonts.FontNormal,
                Text = text,
                TextColor = Resources.Colors.SubtleText
            });
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            _label.Frame = new CGRect(_contentEdgeInsets.Left, _contentEdgeInsets.Top, this._tableViewWidth, 0);
            _label.SizeToFit();
        }

        public override void SizeToFit() {
            base.SizeToFit();

            this.SetNeedsLayout();
            this.LayoutIfNeeded();

            this.Frame = new CGRect(0, 0, this._tableViewWidth, _label.Frame.Height + _contentEdgeInsets.Top + _contentEdgeInsets.Bottom);
        }
    }
}
