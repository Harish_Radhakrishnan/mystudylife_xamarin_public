﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Controls {
    public static class UIViewEx {
        public static void Fade(this UIView view, bool fadeIn, double duration = 0.3, Action onFinished = null) {
            var opaqueAlpha = (nfloat)1.0f;
            var transparentAlpha = (nfloat)0.0f;

            view.Alpha = fadeIn ? transparentAlpha : opaqueAlpha;
            view.Transform = CGAffineTransform.MakeIdentity();
            UIView.Animate(duration, 0, UIViewAnimationOptions.CurveEaseInOut,
                () => {
                    view.Alpha = fadeIn ? opaqueAlpha : transparentAlpha;
                },
                onFinished
            );
        }

        public static void Strikethrough(this UILabel label, bool strikeThrough, NSRange? range = null) {
            if (label.AttributedText == null) {
                return;
            }
            
            // NOTE: Removing strikethrough doesn't always work in iOS 13 so we set the color to clear
            var attributedText = new NSMutableAttributedString(label.AttributedText);
            range ??= new NSRange(0, attributedText.Length);

            if (strikeThrough) {
                attributedText.AddAttribute(
                    UIStringAttributeKey.StrikethroughStyle,
                    NSNumber.FromInt64((long)NSUnderlineStyle.Single),
                    range.Value
                );
                attributedText.RemoveAttribute(UIStringAttributeKey.StrikethroughColor, range.Value);
            }
            else {
                attributedText.RemoveAttribute(
                    UIStringAttributeKey.StrikethroughStyle,
                    range.Value
                );
                attributedText.AddAttribute(
                    UIStringAttributeKey.StrikethroughColor,
                    UIColor.Clear,
                    range.Value
                );
            }
            
            label.AttributedText = attributedText;
        }

        public static void ApplyAppearance(this UISegmentedControl segmentedControl, UIColor accentColor = null) {
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0)) {
                segmentedControl.SelectedSegmentTintColor = UIColor.White;
                segmentedControl.SetTitleTextAttributes(new UITextAttributes {TextColor = UIColor.White}, UIControlState.Normal);
                segmentedControl.SetTitleTextAttributes(new UITextAttributes {TextColor = accentColor ?? Resources.Colors.Accent}, UIControlState.Selected);
            }
            else {
                segmentedControl.TintColor = UIColor.White;
            }
        }
    }
}
