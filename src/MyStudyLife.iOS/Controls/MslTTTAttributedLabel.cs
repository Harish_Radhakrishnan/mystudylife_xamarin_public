using System;
using MvvmCross;
using CoreGraphics;
using CoreText;
using Foundation;
using EvanRobertson.TTTAttributedLabel;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.iOS.Controls {
    public class MslAttributedLabel : TTTAttributedLabel {
        public event EventHandler<SelectedLinkWithUrlEventArgs> SelectedLinkWithUrl;

        // Used by the binding to reset text whenever this changes... it's required :-(
        public event EventHandler LinkForegroundColorChanged;

        private CGColor _linkForegroundColor = Resources.Colors.Accent.CGColor;
        public CGColor LinkForegroundColor {
            get { return _linkForegroundColor; }
            set {
                if (_linkForegroundColor != value) {
                    _linkForegroundColor = value;
                    this.LinkAttributes = new CTStringAttributes {
                        ForegroundColor = value,
                        UnderlineStyle = CTUnderlineStyle.None
                    }.Dictionary;
                    this.LinkForegroundColorChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public MslAttributedLabel() {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
            this.Delegate = new MslDelegate(this);
            this.TextColor = Resources.Colors.Foreground;
            this.TranslatesAutoresizingMaskIntoConstraints = false;
            this.LinkAttributes = new CTStringAttributes {
                ForegroundColor = LinkForegroundColor,
                UnderlineStyle = CTUnderlineStyle.None
            }.Dictionary;
// ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        private class MslDelegate : TTTAttributedLabelDelegate {
            private MslAttributedLabel _label;

            public MslDelegate(MslAttributedLabel label) {
                this._label = label;
            }

            public override void DidSelectLinkWithURL(TTTAttributedLabel label, NSUrl url) {
                var args = new SelectedLinkWithUrlEventArgs(url);

                this._label.SelectedLinkWithUrl?.Invoke(this, args);

                if (!args.Handled) {
                    Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new System.Uri(url.ToString()));
                }
            }

            protected override void Dispose(bool disposing) {
                if (disposing) {
                    _label = null;
                }

                base.Dispose(disposing);
            }
        }
    }

    public class SelectedLinkWithUrlEventArgs : EventArgs {
        public NSUrl Url { get; private set; }

        /// <summary>
        ///     If true, the default action will not occur.
        /// </summary>
        public bool Handled { get; set; }

        public SelectedLinkWithUrlEventArgs(NSUrl url) {
            this.Url = url;
        }
    }
}