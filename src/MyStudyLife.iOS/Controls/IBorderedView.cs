using UIKit;

namespace MyStudyLife.iOS.Controls {
    public interface IBorderedView {
        UIColor BorderColor { get; set; }

        Thickness BorderThickness { get; set; }
    }
}