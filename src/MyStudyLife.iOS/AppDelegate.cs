using System;
using System.Globalization;
using System.Threading;
using AppTrackingTransparency;
using BigTed;
using Foundation;
using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using MyStudyLife.Globalization;
using MyStudyLife.iOS.Platform;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Sync;
using MyStudyLife.UI;
using MyStudyLife.UI.Presentation.DeepLinking;
using MyStudyLife.UI.Services;
using SafariServices;
using UIKit;

namespace MyStudyLife.iOS {
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate<Setup, MslApp> {
        private bool _wasResumed;

        public override void WillEnterForeground(UIApplication application) {
            base.WillEnterForeground(application);
            _wasResumed = true;
        }

        public override void OnActivated(UIApplication application) {
            // Write code to request IDFA request in OnActivated as 
            // In iOS 15, it can only be requested with ATTrackingManager.requestTrackingAuthorization
            // if the application state is already active, so it should be moved
            // from didFinishLaunchingWithOptions to applicationDidBecomeActive

            if (UIDevice.CurrentDevice.CheckSystemVersion(14, 0)) {
                DoRequestAppTrackingAuthorization();
            }
        }

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options) {
            app.SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
            app.SetMinimumBackgroundFetchInterval(UIApplication.BackgroundFetchIntervalMinimum);

            SetupAppearance();

            return base.FinishedLaunching(app, options);
        }

        protected override void RunAppStart(object hint = null) {
            base.RunAppStart(hint);

            var syncService = Mvx.IoCProvider.Resolve<ISyncService>();
            syncService.StatusChanged += SyncServiceOnStatusChanged;
            syncService.Completed += SyncServiceOnCompleted;
            syncService.Error += SyncServiceOnError;

            // Workaround for https://bugzilla.xamarin.com/show_bug.cgi?id=23544
            // and https://bugzilla.xamarin.com/show_bug.cgi?id=27725
            try {
                var formatter = new NSDateFormatter {
                    Locale = NSLocale.CurrentLocale,
                    DateStyle = NSDateFormatterStyle.None,
                    TimeStyle = NSDateFormatterStyle.Short
                };

                var dateStr = formatter.StringFor(NSDate.Now);

                var shouldBe24Hour = !dateStr.Contains(formatter.AMSymbol) && !dateStr.Contains(formatter.PMSymbol);
                var monoIs24Hour = DateTimeFormat.Is24Hour;


                if (shouldBe24Hour != monoIs24Hour ||
                    String.IsNullOrEmpty(CultureInfo.CurrentCulture.DateTimeFormat.DayNames[0])) {

                    var newCultureInfo = new CultureInfo(CultureInfo.CurrentCulture.Name);
                    var newUiCultureInfo = new CultureInfo(CultureInfo.CurrentUICulture.Name);

                    DateTimeFormat.EnsureDayNames(newCultureInfo.DateTimeFormat);
                    DateTimeFormat.EnsureDayNames(newUiCultureInfo.DateTimeFormat);

                    if (shouldBe24Hour && !monoIs24Hour) {
                        DateTimeFormat.Ensure24Hours(newCultureInfo.DateTimeFormat);
                        DateTimeFormat.Ensure24Hours(newUiCultureInfo.DateTimeFormat);
                    }
                    else if (!shouldBe24Hour && monoIs24Hour) {
                        DateTimeFormat.Ensure12Hours(newCultureInfo.DateTimeFormat);
                        DateTimeFormat.Ensure12Hours(newUiCultureInfo.DateTimeFormat);
                    }

                    Thread.CurrentThread.CurrentCulture = newCultureInfo;
                    Thread.CurrentThread.CurrentUICulture = newUiCultureInfo;
                    CultureInfo.DefaultThreadCurrentCulture = newCultureInfo;
                    CultureInfo.DefaultThreadCurrentUICulture = newUiCultureInfo;
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }


            // TODO(iOS) Handle local notification opening
            //if (options != null && options.ContainsKey(UIApplication.LaunchOptionsLocalNotificationKey)) {
            //    var userInfo = (NSDictionary) options.ValueForKey(UIApplication.LaunchOptionsLocalNotificationKey);

            //    var reminderJson = (NSString) userInfo.ValueForKey(new NSString("REMINDER"));

            //    if (reminderJson != null) {
            //        var reminder = JsonConvert.DeserializeObject<Reminder>(reminderJson);

            //        if (reminder != null) {

            //        }
            //    }
            //}
        }

        protected override object GetAppStartHint(object hint = null) {
            return new AppStartHint(_wasResumed ? AppStartMode.Resume : AppStartMode.New);
        }

        public override async void PerformFetch(UIApplication app, Action<UIBackgroundFetchResult> completionHandler) {
            var result = UIBackgroundFetchResult.NoData;

            try {
                if (iOSBackgroundTaskHelper.EnsureInitialized(this, Window)) {
                    await Mvx.IoCProvider.Resolve<IReminderService>().RunAsync();
                }
            }
            catch {
                result = UIBackgroundFetchResult.Failed;
            }
            finally {
                completionHandler(result);
            }
        }

        public override bool OpenUrl(UIApplication application, NSUrl url, string sourceApplication, NSObject annotation) {
            if (!String.Equals(url.Scheme, "msl", StringComparison.InvariantCultureIgnoreCase)) {
                return false;
            }

            var nav = application.KeyWindow.RootViewController as UINavigationController;
            (nav?.VisibleViewController as SFSafariViewController)?.DismissViewController(true, null);

            Mvx.IoCProvider.Resolve<IDeepLinkDispatcher>().Dispatch(url);

            return true;
        }

        private void SyncServiceOnStatusChanged(object sender, SyncStatusChangedEventArgs e) {
            UIApplication.SharedApplication.NetworkActivityIndicatorVisible = e.NewStatus == SyncStatus.Syncing;
        }

        private void SyncServiceOnCompleted(object sender, SyncCompletedEventArgs e) {
            UIApplication.SharedApplication.BeginInvokeOnMainThread(BTProgressHUD.Dismiss);
        }

        private void SyncServiceOnError(object sender, SyncErrorEventArgs e) {
            if (e.Exception is HttpApiUnauthorizedException)
                return;

            UIApplication.SharedApplication.BeginInvokeOnMainThread(() => Mvx.IoCProvider.Resolve<IDialogService>().ShowNotification(
                String.Empty,
                ((ISyncService)sender).Status == SyncStatus.Offline
                    ? "Sync failed: offline"
                    : "Sync failed: unknown error"
            ));
        }

        private void SetupAppearance() {
            UILabel.Appearance.TextColor = Resources.Colors.Foreground;

            UITableView.Appearance.TintColor = Resources.Colors.Accent;

            UISwitch.Appearance.OnTintColor = Resources.Colors.Accent;

            UINavigationBar.Appearance.BarTintColor = Resources.Colors.Accent;
            UINavigationBar.Appearance.TintColor = UIColor.White;

            UIToolbar.Appearance.BarTintColor = Resources.Colors.Accent;
            UIToolbar.Appearance.TintColor = UIColor.White;

            UIActionSheet.Appearance.TintColor = Resources.Colors.Accent;

            UIView.AppearanceWhenContainedIn(typeof(UITableViewHeaderFooterView), typeof(UITableView)).BackgroundColor = Resources.Colors.Background;

            // Doesn't work :-(
            // UILabel.AppearanceWhenContainedIn(typeof(UITextField)).TextColor = Resources.Colors.SubtleText;
        }

        private void DoRequestAppTrackingAuthorization() {
            UIApplication.SharedApplication.BeginInvokeOnMainThread(() => {
                ATTrackingManager.RequestTrackingAuthorization((status) => {
                    switch (status) {
                        case ATTrackingManagerAuthorizationStatus.Authorized:
                            break;
                        case ATTrackingManagerAuthorizationStatus.Denied:
                            break;
                        default:
                            break;
                    }
                });
            }
            );
        }
    }
}