using MvvmCross.Plugin.Color.Platforms.Ios;
using MyStudyLife.Data.UI;
using UIKit;

namespace MyStudyLife.iOS {
// TODO: Remove Pallette colors and AccentDark once welcome wizard replaced
    public static partial class Resources {
        public static class Colors {
            public static readonly UIColor PaletteARed = UIColor.FromRGB(168, 18, 14);
            public static readonly UIColor PaletteABlue2 = UIColor.FromRGB(22, 30, 115);
            public static readonly UIColor PaletteAGrey = UIColor.FromRGB(146, 158, 173);
            public static readonly UIColor PalleteMidBlue = UIColor.FromRGB(28, 79, 141);
            public static readonly UIColor PalletePink = UIColor.FromRGB(141, 28, 126);
            public static readonly UIColor PalleteTeal = UIColor.FromRGB(28, 134, 141);

            public static readonly UIColor Foreground = UIColor.FromRGB(43, 43, 43);
            public static readonly UIColor ForegroundMidAlt = UIColor.FromRGB(86, 97, 91);
            public static readonly UIColor ForegroundMid = UIColor.FromRGB(70, 70, 70);
            public static readonly UIColor ForegroundLight = UIColor.FromRGB(195, 197, 201);

            public static readonly UIColor Background = UIColor.FromRGB(249, 249, 249);//UIColor.FromRGB(241, 243, 247);

            public static readonly UIColor AccentDark = UIColor.FromRGB(24, 119, 100);
            public static readonly UIColor Accent = UIColor.FromRGB(28, 141, 118);
            public static readonly UIColor AccentMid = UIColor.FromRGB(34, 167, 140);
            public static readonly UIColor AccentLight = UIColor.FromRGB(113, 179, 164);

            public static readonly UIColor Subtle = UIColor.FromRGB(226, 226, 226);
            public static readonly UIColor SubtleSuper = UIColor.FromRGB(239, 239, 239);
            public static readonly UIColor SubtleText = UIColor.FromRGB(117, 117, 117);

            public static readonly UIColor Border = Subtle;
            public static readonly UIColor BorderLight = SubtleSuper;

            public static readonly UIColor AttentionForeground = UserColors.Attention.ToNativeColor();
            public static readonly UIColor WarningForeground = UserColors.Warning.ToNativeColor();
            public static readonly UIColor OkayForeground = UserColors.Okay.ToNativeColor();

            public static readonly UIColor TabForeground = UIColor.FromRGB(155, 155, 155);
// TODO: this looks like it should be an accent color!
            public static readonly UIColor TabForegroundSelected = UIColor.FromRGB(16, 142, 118);

            public static readonly UIColor Facebook = UIColor.FromRGB(29, 89, 152);
            public static readonly UIColor Google = UIColor.FromRGB(204, 55, 50);
            public static readonly UIColor Office365 = UIColor.FromRGB(218, 59, 1);
            public static readonly UIColor Twitter = UIColor.FromRGB(64, 153, 255);

            public static readonly UIColor FacebookMid = UIColor.FromRGB(94, 124, 187);
            public static readonly UIColor GoogleMid = UIColor.FromRGB(239, 90, 85);
            public static readonly UIColor Office365Mid = UIColor.FromRGB(254, 91, 31);
        }
    }
}