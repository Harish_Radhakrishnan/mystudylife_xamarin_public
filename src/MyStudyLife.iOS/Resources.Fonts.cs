using System;
using UIKit;

namespace MyStudyLife.iOS {
    public static partial class Resources {
        public static class Fonts {
            public const string DefaultFamilyName = "HelveticaNeue";

            public const float SizeTitle = (float)FontSize.Title;
            public const float SizeSubhead = (float)FontSize.Subhead;
            public const float SizeBody = (float)FontSize.Body;
            public const float SizeCaption = (float)FontSize.Caption;
            public const float SizeExtraSmall = (float)FontSize.ExtraSmall;

            public const float FontSizeHuge = 30;
            public const float FontSizeExtraLarge = 24;
            public const float FontSizeLarge = SizeTitle;
            public const float FontSizeMedium = SizeSubhead;//16;
            public const float FontSizeNormal = SizeBody;//14;
            public const float FontSizeSmall = SizeCaption;
            public const float FontSizeExtraSmall = SizeExtraSmall;

            public static UIFont OfSize(FontSize size, FontWeight weight = FontWeight.Normal) => OfSize((float)size, weight);

            public static UIFont OfSize(float size, FontWeight weight = FontWeight.Normal) {
                string name = DefaultFamilyName;

                if (weight != FontWeight.Normal) {
                    name += "-" + weight;
                }

                return UIFont.FromName(name, size);
            }

            [Obsolete("Use OfSize")]
            public static UIFont FontOfSize(float size, FontWeight weight = FontWeight.Normal)
                => OfSize(size, weight);

            public static UIFont FontLarge = OfSize(FontSizeLarge);
            public static UIFont FontMedium = OfSize(FontSizeMedium);
            public static UIFont FontNormal = OfSize(FontSizeNormal);
            public static UIFont FontSmall = OfSize(FontSizeSmall);
            public static UIFont FontExtraSmall = OfSize(FontSizeExtraSmall);
        }
    }

    public enum FontSize {
        Huge = 30,
        Title = 20,
        Subhead = 16,
        Body = 14,
        Caption = 12,
        ExtraSmall = 10
    }

    public enum FontWeight {
        Thin,
        UltraLight,
        Light,
        Normal,
        Medium,
        Bold
    }
}