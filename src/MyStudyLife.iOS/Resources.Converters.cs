namespace MyStudyLife.iOS {
    public static partial class Resources {
        public static class Converters {
            public const string Boolean = "Boolean";
            public const string InvertedBoolean = "InvertedBoolean";

            public const string StringFormat = "StringFormat";
            public const string StringToTitleCase = "StringToTitleCase";
            public const string Coalesce = "Coalesce";

            public const string TimeSpanToShortString = "TimeSpanToShortString";

            public const string NativeColor = "NativeColor";

            public const string TimeSpanToDateTime = "TimeSpanToDateTime";
            public const string IntToNFloat = "IntToNFloat";

            public const string SubjectColor = "SubjectColor";

            public const string ContextualDate = "ContextualDate";
        }
    }
}