using System;
using System.Collections.Generic;
using CoreGraphics;
using System.Windows.Input;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS {
    public static class Extensions {
        public static bool IsLandscape(this UIInterfaceOrientation orientation) {
            return orientation.In(UIInterfaceOrientation.LandscapeLeft, UIInterfaceOrientation.LandscapeRight);
        }

        public static CGRect WithX(this CGRect rect, nfloat x) {
            return new CGRect(x, rect.Y, rect.Width, rect.Height);
        }
        public static CGRect WithY(this CGRect rect, nfloat y) {
            return new CGRect(rect.X, y, rect.Width, rect.Height);
        }
        public static CGRect WithWidth(this CGRect rect, nfloat width) {
            return new CGRect(rect.X, rect.Y, width, rect.Height);
        }
        public static CGRect WithHeight(this CGRect rect, nfloat height) {
            return new CGRect(rect.X, rect.Y, rect.Width, height);
        }

        public static FluentLayout[] Fill(this UIView view, UIView parentView, nfloat? margin = default(nfloat?)) => new [] {
            view.AtTopOf(parentView, margin),
            view.AtRightOf(parentView, margin),
            view.AtBottomOf(parentView, margin),
            view.AtLeftOf(parentView, margin),
        };

        public static FluentLayout[] Fill(this UIView view, UIView parentView, UIEdgeInsets margin) => new[] {
            view.AtTopOf(parentView, margin.Top),
            view.AtRightOf(parentView, margin.Right),
            view.AtBottomOf(parentView, margin.Bottom),
            view.AtLeftOf(parentView, margin.Left),
        };

        //public static void AddConstraints(this UIView view, params IEnumerable<FluentLayout>[] fluentLayouts) {
        //    foreach (var fluentLayout in fluentLayouts) {
        //        FluentLayoutExtensions.AddConstraints(view, fluentLayout);
        //    }
        //}

        /// <summary>
        ///     Allows both single constraints and collections of constraints
        ///     to be added in a single go.
        /// </summary>
        /// <param name="view">
        ///     The view to add the constraints to.
        /// </param>
        /// <param name="fluentLayoutOrLayouts">
        ///     A collection of a combination of <see cref="IEnumerable{FluentLayout}"/>
        ///     and <see cref="FluentLayout"/>.
        /// </param>
        public static void AddConstraints(this UIView view, params object[] fluentLayoutOrLayouts) {
            foreach (var fluentLayout in fluentLayoutOrLayouts) {
                var individual = fluentLayout as FluentLayout;

                if (individual != null) {
                    FluentLayoutExtensions.AddConstraints(view, individual);
                    continue;
                }

                var collection = fluentLayout as IEnumerable<FluentLayout>;

                if (collection != null) {
                    FluentLayoutExtensions.AddConstraints(view, collection);
                    continue;
                }

                throw new ArgumentException($"Unexpected type {fluentLayout.GetType()}. Only {nameof(FluentLayout)} and IEnumerable<FluentLayout> are supported");
            }
        }

        /// <summary>
        ///     Creates a constraint for the current view where the top must
        ///     be greater than or equal to the top of the <paramref name="other"/> view.
        /// </summary>
        public static FluentLayout WithSameOrGreaterTop(this UIView view, UIView other, nfloat? margin = null) {
            var fluentLayout = view.Top().GreaterThanOrEqualTo().TopOf(other);

            if (margin.HasValue) {
                return fluentLayout.Plus(margin.Value);
            }

            return fluentLayout;
        }

        /// <summary>
        ///     Creates a constraint for the current view where the bottom must
        ///     be smaller than or equal to bottom of the <paramref name="other"/> view.
        /// </summary>
        public static FluentLayout WithSameOrLesserBottom(this UIView view, UIView other, nfloat? margin = null) {
            var fluentLayout = view.Bottom().LessThanOrEqualTo().BottomOf(other);

            if (margin.HasValue) {
                return fluentLayout.Minus(margin.Value);
            }

            return fluentLayout;
        }

        public static MvxFluentBindingDescription<TTarget, TSource> Visibility<TTarget, TSource>(
            this MvxFluentBindingDescription<TTarget, TSource> description
        )
            where TTarget : UIView {

            return description.For(x => x.Hidden).WithConversion("InvertedBoolean");
        }
        public static MvxFluentBindingDescription<TTarget, TSource> InvertedVisibility<TTarget, TSource>(
            this MvxFluentBindingDescription<TTarget, TSource> description
        )
            where TTarget : UIView {

            return description.For(x => x.Hidden).WithConversion("Boolean");
        }

        public static UIBarButtonItem WithCommand(this UIBarButtonItem item, ICommand command, object commandParameter = null) {
            command.CanExecuteChanged += (s, e) => {
                item.Enabled = command.CanExecute(commandParameter);
            };

            item.Enabled = command.CanExecute(commandParameter);

            item.Clicked += (s, e) => command.Execute(commandParameter);

            return item;
        }

        /// <summary>
        ///     An extension method to navigate to views based on MonoTouch.Dialog
        ///     from views which are not.
        /// </summary>
        public static void ActivateController(this UIViewController currentController, UIViewController controller) {
            // https://github.com/MvvmCross/MvvmCross/blob/v3.1/CrossUI/CrossUI.Touch/Dialog/DialogViewController.cs#L500

            var parent = currentController.ParentViewController;
            var nav = parent as UINavigationController;

            // We can not push a nav controller into a nav controller
            if (nav != null && !(controller is UINavigationController)) {
                nav.PushViewController(controller, true);
            }
            else {
                currentController.PresentViewController(controller, true, null);
            }
        }
        
        public static UIImage WithOverlayColor(this UIImage image, UIColor color) {
            var rect = new CGRect(0.0f, 0.0f, image.Size.Width, image.Size.Height);

            UIGraphics.BeginImageContextWithOptions(image.Size, false, image.CurrentScale);

            image.Draw(rect);

            var context = UIGraphics.GetCurrentContext();

            context.SetBlendMode(CGBlendMode.SourceIn);
            context.SetFillColor(color.CGColor);
            context.FillRect(rect);

            image = UIGraphics.GetImageFromCurrentImageContext();
            UIGraphics.EndImageContext();

            return image;
        }

        // http://developer.xamarin.com/guides/cross-platform/macios/unified/
        public static DateTime ToDateTime(this NSDate date) {
            // NSDate has a wider range than DateTime, so clip
            // the converted date to DateTime.Min|MaxValue.
            double secs = date.SecondsSinceReferenceDate;

            if (secs < -63113904000) {
                return DateTime.MinValue;
            }
            if (secs > 252423993599) {
                return DateTime.MaxValue;
            }

            return (DateTime)date;
        }

        /// <summary>
        ///     Converts an <see cref="NSDate"/> to a <see cref="DateTime"/>.
        ///
        ///     <see cref="DateTime.Kind"/> is <see cref="DateTimeKind.Unspecified"/>,
        ///     it is specified as UTC.
        /// </summary>
        public static NSDate ToNSDate(this DateTime date) {
            if (date.Kind == DateTimeKind.Unspecified) {
                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
            }

            return (NSDate)date;
        }

        /// <summary>
        ///     Converts an <see cref="NSDate"/> to a <see cref="DateTime"/>,
        ///     specifying the given <param name="kind" />.
        /// </summary>
        public static NSDate ToNSDate(this DateTime date, DateTimeKind kind) {
            if (kind == DateTimeKind.Unspecified) {
                throw new ArgumentOutOfRangeException(nameof(kind), "Cannot be 'DateTimeKind.Unspecified' for conversion to NSDate");
            }

            return (NSDate)DateTime.SpecifyKind(date, kind);
        }
    }
}