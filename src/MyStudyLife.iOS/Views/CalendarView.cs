﻿using System;
using MvvmCross.Binding.BindingContext;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.ViewModels.Calendar;
using WYPopover;
using Cirrious.FluentLayouts.Touch;
using MyStudyLife.iOS.Views.Calendar;

namespace MyStudyLife.iOS.Views {
    internal abstract class CalendarView<TViewModel> : MslViewController<TViewModel> where TViewModel : CalendarViewModel {
        private WYPopoverController _popover;
        
        protected MslNavBarTitleView TitleView { get; private set; }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            ParentViewController.NavigationItem.TitleView = TitleView = new MslNavBarTitleView {
                ShowArrow = true
            };
            TitleView.Tapped += TitleViewOnTapped;

            var todayBarButton = new UIBarButtonItem(
                Resources.Strings.Today,
                UIBarButtonItemStyle.Plain,
                null
            );
            ParentViewController.NavigationItem.SetRightBarButtonItem(todayBarButton, true);

            #region Binding

            var set = this.CreateBindingSet<CalendarView<TViewModel>, TViewModel>();

            set.Bind(todayBarButton).To(x => x.CurrentPeriodCommand);
            set.Bind(todayBarButton).For(x => x.Enabled).To(x => x.IsSelectedPeriodCurrent).WithConversion("InvertedBoolean");

            set.Apply();

            #endregion
        }

        private void TitleViewOnTapped(object sender, EventArgs e) {
            var size = new CGSize(View.Bounds.Width / 2f, 90);
            var optionHeight = (size.Height / 2f) + .5f;

            var weekOption = new WYPopoverOptionView {
                Text = Resources.Strings.Week,
                TextColor = this is CalendarWeekView ? Resources.Colors.Accent : UIColor.Black,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            var monthOption = new WYPopoverOptionView {
                Text = Resources.Strings.Month,
                TextColor = this is CalendarMonthView ? Resources.Colors.Accent : UIColor.Black,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var contentView = new UIView(new CGRect(CGPoint.Empty, size)) { weekOption, monthOption };

            contentView.AddConstraints(
                weekOption.Width().EqualTo(size.Width),
                weekOption.AtLeftOf(contentView),
                weekOption.AtRightOf(contentView),
                weekOption.AtTopOf(contentView),
                weekOption.Height().EqualTo(optionHeight),

                monthOption.AtLeftOf(contentView),
                monthOption.AtRightOf(contentView),
                monthOption.Below(weekOption),
                monthOption.AtBottomOf(contentView),
                monthOption.Height().EqualTo(optionHeight)
            );

            var dividerPath = new UIBezierPath {
                LineWidth = 1f
            };
            dividerPath.MoveTo(new CGPoint(0, optionHeight));
            dividerPath.AddLineTo(new CGPoint(size.Width, optionHeight));

            contentView.Layer.AddSublayer(new CAShapeLayer {
                Path = dividerPath.CGPath,
                StrokeColor = Resources.Colors.Border.CGColor
            });

            var theme = WYPopoverTheme.ThemeForIOS7();

            theme.OuterCornerRadius = 1;

            _popover = new WYPopoverController(new UIViewController {
                View = contentView
            }) {
                Theme = theme,
                PopoverContentSize = size
            };

            _popover.PresentPopoverFromRect(
                new CGRect(
                    (TitleView.Bounds.Width / 2f) - (size.Width / 2f),
                    (TitleView.Bounds.Height * -.9f),
                    size.Width,
                    size.Height
                ),
                TitleView,
                WYPopoverArrowDirection.Up,
                true,
                WYPopoverAnimationOptions.FadeWithScale
            );

            weekOption.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                _popover?.DismissPopoverAnimated(true);
                _popover = null;

                (this.ViewModel as CalendarMonthViewModel)?.SwitchViewCommand.Execute(null);
            }));
            monthOption.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                _popover?.DismissPopoverAnimated(true);
                _popover = null;

                (this.ViewModel as CalendarWeekViewModel)?.SwitchViewCommand.Execute(null);
            }));
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration) {
            base.WillRotate(toInterfaceOrientation, duration);

            _popover?.DismissPopoverAnimated(false);
            _popover = null;
        }

        sealed class WYPopoverOptionView : MSLLabel {
            private UIColor _backgroundColor;
            private UIColor _highlightedBackgroundColor = Resources.Colors.Border;

            public override UIColor BackgroundColor {
                get { return _backgroundColor; }
                set {
                    _backgroundColor = value;

                    if (!this.Highlighted || this.HighlightedBackgroundColor == null) {
                        this.RealBackgroundColor = value;
                    }
                }
            }

            public UIColor HighlightedBackgroundColor {
                get { return _highlightedBackgroundColor; }
                set {
                    _highlightedBackgroundColor = value;

                    if (this.Highlighted) {
                        this.RealBackgroundColor = value ?? this.BackgroundColor;
                    }
                }
            }

            private UIColor RealBackgroundColor {
                get { return base.BackgroundColor; }
                set { base.BackgroundColor = value; }
            }

            public override bool Highlighted {
                get { return base.Highlighted; }
                set {
                    base.Highlighted = value;

                    if (HighlightedBackgroundColor != null) {
                        this.RealBackgroundColor = value ? HighlightedBackgroundColor : BackgroundColor;
                    }
                }
            }

            public WYPopoverOptionView() {
                Initialize();
            }

            public WYPopoverOptionView(CGRect frame) : base(frame) {
                Initialize();
            }

            private void Initialize() {
                this.TextAlignment = UITextAlignment.Center;
                this.TextColor = UIColor.Black;
                this.UserInteractionEnabled = true;
                this.ContentEdgeInsets = new UIEdgeInsets(0, 15f, 0, 15f);
            }

            public override void TouchesBegan(NSSet touches, UIEvent evt) {
                base.TouchesBegan(touches, evt);
                this.Highlighted = true;
            }

            public override void TouchesEnded(NSSet touches, UIEvent evt) {
                base.TouchesEnded(touches, evt);
                this.Highlighted = false;
            }

            public override void TouchesCancelled(NSSet touches, UIEvent evt) {
                base.TouchesEnded(touches, evt);
                this.Highlighted = false;
            }
        }
    }
}
