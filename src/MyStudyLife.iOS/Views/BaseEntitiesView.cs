using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoreGraphics;
using System.Globalization;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using MvvmCross.Converters;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    internal abstract class BaseEntitiesView<TEntity, TViewModel> : MslViewController<TViewModel>
        where TEntity : SubjectDependentEntity, new()
        where TViewModel : BaseEntitiesViewModel<TEntity> {

        protected UITableView TableView { get; private set; }

        protected virtual string[] FilterOptions => null;

        public abstract Glyph Glyph { get; }

        public abstract string FilterTitle { get; }

        public MslNavBarTitleView TitleView { get; set; }

        protected UISegmentedControl FilterControl { get; private set; }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.SetNavigationItemButtons();

            this.ParentViewController.NavigationItem.TitleView = this.TitleView = new MslNavBarTitleView {
                Title = this.Title
            };

            this.TitleView.Tapped += (s, e) => {
                var viewController = new FilterViewController(FilterTitle) {
                    ViewModel = this.ViewModel
                };

                var navController = new UINavigationController(viewController) {
                    ModalTransitionStyle = UIModalTransitionStyle.CoverVertical
                };

                this.PresentViewController(navController, true, null);
            };

            #region Creation

            UIView filterView = null;

            if (FilterOptions != null && FilterOptions.Length > 0) {
                filterView = new UIView {
                    BackgroundColor = Resources.Colors.Accent
                };
                Add(filterView);

                var filterControl = this.FilterControl = new UISegmentedControl {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                filterControl.ApplyAppearance();
                
                filterView.Add(filterControl);

                for (int i = 0; i < FilterOptions.Length; i++) {
                    filterControl.InsertSegment(FilterOptions[i], i, false);
                }

                filterControl.SelectedSegment = 0;

                filterControl.ValueChanged += FilterControlOnValueChanged;

                View.AddConstraints(
                    filterView.WithSameLeft(View),
                    filterView.WithSameRight(View),
                    filterView.WithSameTop(View),

                    filterControl.AtLeftOf(filterView, 15),
                    filterControl.AtRightOf(filterView, 15),
                    filterControl.AtTopOf(filterView, 10),
                    filterControl.AtBottomOf(filterView, 10)
                );
            }

            TableView = new UITableView {
                AllowsSelection = true,
                BackgroundColor = UIColor.Clear,
                SeparatorColor = Resources.Colors.Border,
                TableFooterView = new UIView() // Removes extra separators
            };
            Add(TableView);

            var viewSource = CreateViewSource(TableView);

            if (viewSource != null) {
                TableView.Source = viewSource;
                TableView.ReloadData();
            }

            var lonelyView = new MslLonelyView(Glyph);
            Add(lonelyView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<BaseEntitiesView<TEntity, TViewModel>, TViewModel>();

            // Probs could be named better (maybe we have a PageTitle per ViewModel...)?
            set.Bind(this.TitleView).For(x => x.Title).To(x => x.Title);
            set.Bind(this.TitleView).For(x => x.SubTitle).To(x => x.SelectedScheduleText);

            if (viewSource != null) {
                set.Bind(viewSource).To(x => x.ViewSource.View);
                set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);
            }

            set.Bind(TableView).Visibility().To(x => x.ViewSource.Count);
            set.Bind(lonelyView).InvertedVisibility().To(x => x.ViewSource.Count);
            set.Bind(lonelyView).For(x => x.Text).To(x => x.LonelyText);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                TableView.WithSameLeft(View),
                TableView.WithSameRight(View),
                TableView.WithSameBottom(View),
                filterView != null ? TableView.Below(filterView) : TableView.WithSameTop(View)
            );

            #endregion
        }

        protected virtual void SetNavigationItemButtons() {
            ParentViewController.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIBarButtonSystemItem.Add).WithCommand(this.ViewModel.NewCommand),
                true
            );
        }

        protected abstract MslTableViewSource CreateViewSource(UITableView tableView);

        protected virtual void FilterControlOnValueChanged(object sender, EventArgs e) {}

        protected override void SizeRootViewImpl(CGRect rect) {
            base.SizeRootViewImpl(rect);

            if (View != null) {
                View.Frame = rect;
            }
        }

        private sealed class FilterViewController : MslDialogViewController<TViewModel> {
            public FilterViewController(string title) {
                this.Title = title;
            }

            public override void ViewDidLoad() {
                NavigationController.NavigationBar.BarStyle = UIBarStyle.Black;
                NavigationController.NavigationBar.Translucent = false;

                NavigationItem.HidesBackButton = false;

                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(Resources.Strings.Done, UIBarButtonItemStyle.Done,
                        (s, e) => this.DismissViewController(true, null)),
                    true
                );

                base.ViewDidLoad();

                var bindings = this.CreateInlineBindingTarget<TViewModel>();

                Root = new RootElement(this.Title) {
                    UnevenRows = true
                };

                Root.Add(new[] {
                    new Section {
                        new AcademicScheduleRootElement()
                            .Bind(bindings, x => x.AcademicYears, x => x.AcademicYears)
                            .Bind(bindings, x => x.SelectedSchedule, x => x.SelectedSchedule)
                        as Element
                    },
                    new Section {
                        new SubjectRootElement()
                            .Bind(bindings, x => x.Subjects, x => x.Subjects)
                            .Bind(bindings, x => x.SelectedSubject, x => x.SelectedSubject, new HackyNullSubjectConverter())
                        as Element
                    }
                });
            }

            class HackyNullSubjectConverter : MvxValueConverter<Subject, Subject> {
                protected override Subject Convert(Subject value, Type targetType, object parameter, CultureInfo culture) {
                    return value == null || value.Color == "NULL" ? null : value;
                }

                protected override Subject ConvertBack(Subject value, Type targetType, object parameter, CultureInfo culture) {
                    return value;
                }
            }

            // TODO: Genericise this
            // This is pretty much a duplicate of AcademicYearRootElement,
            // reason being is that using an MvxSection doesn't bind at the 
            // same time as the root, meaning that "None" is always selected.
            class SubjectRootElement : MslRootElement {
                private ObservableCollection<Subject> _subjects;
                private Subject _selectedSubject;

                public ObservableCollection<Subject> Subjects {
                    get { return _subjects; }
                    set {
                        _subjects = value;

                        SetElementsFromItemsSource();
                    }
                }

                public Subject SelectedSubject {
                    get { return _selectedSubject; }
                    set {
                        _selectedSubject = value;

                        RadioSelected = GetSelectedIndexFromSelectedSubject(value);
                    }
                }

                public event EventHandler SelectedSubjectChanged;

                public SubjectRootElement() {
                    this.Caption = Resources.Strings.Subject;
                    this.Group = new RadioGroup("Subject", 0);

                    this.Add(new Section {
                        new MslRadioElement(Resources.Strings.None, "Subject")
                    });

                    RadioSelectedChanged += (s, args) => {
                        var selectedElement = this.Sections.Count > 1 ? this.Sections[1].Elements.Cast<RadioElement>().SingleOrDefault(x => x.RadioIdx == this.RadioSelected) : null;

                        _selectedSubject = selectedElement == null ? null : (Subject)selectedElement.ObjectValue;

                        if (SelectedSubjectChanged != null) {
                            SelectedSubjectChanged(this, EventArgs.Empty);
                        }
                    };
                }

                private int GetSelectedIndexFromSelectedSubject(Subject subject) {
                    if (subject == null || this.Sections.Count < 1) {
                        return 0;
                    }

                    return (
                        from element in this.Sections[1].Elements.Cast<RadioElement>()
                        where ((Subject)element.ObjectValue).Guid == subject.Guid
                        select element.RadioIdx
                    ).FirstOrDefault();
                }

                private void SetElementsFromItemsSource() {
                    var section = this.Sections.ElementAtOrDefault(1);

                    if (section != null) {
                        section.Clear();
                    }
                    else {
                        this.Add(section = new Section());
                    }

                    // TODO(iOS) lonely section?

                    var elements = new List<MslRadioElement>();

                    int i = 1, selectedIndex = 0;

                    if (this.Subjects != null) {
                        var selectedScheduleGuid = this.SelectedSubject != null
                            ? new Guid?(this.SelectedSubject.Guid)
                            : null;

                        foreach (var subject in this.Subjects) {
                            elements.Add(new SubjectElement(subject) {
                                RadioIdx = i
                            });

                            if (subject.Guid == selectedScheduleGuid) {
                                selectedIndex = i;
                            }

                            i++;
                        }
                    }

                    section.AddAll(elements);

                    this.RadioSelected = selectedIndex;
                }
            }
        }
    }
}