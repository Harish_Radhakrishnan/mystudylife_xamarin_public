using CoreGraphics;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.iOS.Platform;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views {
    [Register("SettingsView")]
    internal sealed class SettingsView : MslDialogViewController<SettingsViewModel> {
        [UsedImplicitly]
        private MvxPropertyChangedListener _listener;

        public override string Title {
            get { return Resources.Strings.Settings; }
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.IsSigningOut), OnIsSigningOutChanged);

            ParentViewController.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("MoreIcon.png"), UIBarButtonItemStyle.Plain, (s, e) => {
                    var actionSheet = new MslActionSheet {
                        DestructiveButtonIndex = 0,
                        CancelButtonIndex = 1
                    };

                    actionSheet.AddButton(Resources.Strings.SignOut);
                    actionSheet.AddButton(Resources.Strings.Cancel);

                    actionSheet.ShowInView(this.View);

                    actionSheet.Dismissed += ActionSheetOnDismissed;
                }),
                true
            );

            var bindings = this.CreateInlineBindingTarget<SettingsViewModel>();

            var settingsSection = new MvxSection(item => {
                var settingsItem = (SettingsViewModel.SettingsItem)item;

                return new NavigationElement(settingsItem.Name) {
                    SelectedCommand = settingsItem.Command
                };
            });
            settingsSection.Bind(bindings, x => x.ItemsSource, x => x.Settings);

            var aboutSection = new Section(Resources.Strings.About) {
                new StringElement(Resources.Strings.FeedbackAndSupport) {
                    ShouldDeselectAfterTouch = true
                }.Bind(bindings, x => x.SelectedCommand, x => x.ShowFeedbackAndSupportCommand),
                new StringElement(Resources.Strings.Privacy) {
                    ShouldDeselectAfterTouch = true
                }.Bind(bindings, x => x.SelectedCommand, x => x.ShowPrivacyPolicyCommand),
                new StringElement(Resources.Strings.TermsOfUse) {
                    ShouldDeselectAfterTouch = true
                }.Bind(bindings, x => x.SelectedCommand, x => x.ShowTermsOfUseCommand)
            };

            var footerView = new UIView();
            aboutSection.FooterView = footerView;

            var versionLabel = new HelpTextLabel {
                Frame = new CGRect(0, 0, this.View.Bounds.Width, 0),
                TextAlignment = UITextAlignment.Center
            }.Bind(bindings, x => x.AppVersion, "StringFormat", "v{0}");
            footerView.Add(versionLabel);

            var midPoint = this.View.Bounds.Width/2f;
            // Gives the same distance between the cell before the version
            // and the social icons.
            float y = 46f;
            float size = 44f;
            float margin = 16f;

            var facebook = CreateSocialButton(
                new CGRect(midPoint - (size + margin / 2), y, size, size),
                "Facebook.png",
                Resources.Colors.Facebook
            ).Bind(bindings, x => x.FacebookCommand);
            footerView.Add(facebook);

            var twitter = CreateSocialButton(
                new CGRect(midPoint + (margin / 2), y, size, size),
                "Twitter.png",
                Resources.Colors.Twitter
            ).Bind(bindings, x => x.TwitterCommand);
            footerView.Add(twitter);

            footerView.Frame = new CGRect(
                0, 0,
                this.View.Bounds.Width,
                // This looks like a hack (and it is)
                // - The UITableViewController footer height measurement overrides aren't called
                // - 2x is the height at which it will scroll fully on older devices (4S)
                (y + size) * 2
            );

            Root = new MslRootElement(Resources.Strings.Settings) {
                settingsSection,
                aboutSection
            };
        }

        private void ActionSheetOnDismissed(object sender, UIButtonEventArgs e) {
            if ((int)e.ButtonIndex == 0) {
                this.ViewModel.SignOutCommand.Execute(null);
            }
        }

        private void OnIsSigningOutChanged() {
            bool isBusy = this.ViewModel != null && this.ViewModel.IsSigningOut;

            if (isBusy) {
                iOSDialogService.ShowBusyIndicator(Resources.Strings.SigningOutWithEllipsis);
            }
            else {
                iOSDialogService.HideBusyIndicator();
            }
        }

        private UIButton CreateSocialButton(CGRect frame, string imageName, UIColor tintColor) {
            var btn = new UIButton(UIButtonType.Custom) {
                Frame = frame
            };

            btn.SetImage(
                UIImage.FromBundle(imageName),
                UIControlState.Normal
            );
            btn.SetImage(
                UIImage.FromBundle(imageName).WithOverlayColor(tintColor),
                UIControlState.Highlighted
            );

            return btn;
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _listener?.Dispose();
                _listener = null;
            }

            base.Dispose(disposing);
        }
    }
}