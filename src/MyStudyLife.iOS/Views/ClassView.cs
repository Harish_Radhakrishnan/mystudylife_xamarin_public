using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Platform;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.View;
using Class = MyStudyLife.Data.Class;
using System.Linq;
using CoreGraphics;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Plugin.Color.Platforms.Ios;

namespace MyStudyLife.iOS.Views {
    [Register("ClassView")]
    internal sealed class ClassView : EntityView<Class, ClassViewModel> {
        private MvxPropertyChangedListener _listener;

        private UIView _tabControlView;
        private UISegmentedControl _tabControl;
        private UIScrollView _currentView, _scheduleView;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(() => ViewModel.IsLoaded, Invalidate)
                .Listen(() => ViewModel.IsReadOnly, Invalidate)
                .Listen(() => ViewModel.CanLeave, Invalidate)
                .Listen(() => ViewModel.IsLeaving, OnIsLeavingChanged)
                .Listen(nameof(ViewModel.Color), () => {
                    var color = ViewModel.Color?.ToNativeColor();
                    _tabControl?.ApplyAppearance(color);
                    NavigationController?.NavigationBar?.ApplyAppearance(backgroundColor: color);
                });;

            bool isTeacher = User.Current.IsTeacher;

            _tabControlView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
            };
            _tabControl = new UISegmentedControl {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _tabControl.ApplyAppearance();
            _tabControlView.Add(_tabControl);

            _tabControlView.AddConstraints(_tabControl.Fill(_tabControlView, new UIEdgeInsets(15,15,5,15)));

            _tabControl.InsertSegment(Resources.Strings.Common.Current, 0, false);
            _tabControl.InsertSegment(Resources.Strings.Schedule, 1, false);

            _tabControl.SelectedSegment = 0;

            _tabControl.ValueChanged += (s, e) => SetTabsState();

            #region Creation

            ContentView.Add(_currentView = new UIScrollView {
                BackgroundColor = UIColor.White
            });
            {
                var currentInnerView = new UIView {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                _currentView.Add(currentInnerView);

                var currentSet = this.CreateBindingSet<ClassView, ClassViewModel>();

                var instanceSub = CreateSub(currentInnerView, Glyph.Calendar);
                var locationSub = CreateSub(currentInnerView, Glyph.Location, instanceSub.View);

                currentSet.Bind(instanceSub.Label).To(x => x.Instance);
                currentSet.Bind(locationSub.Label).To(x => x.Location).WithConversion(Resources.Converters.Coalesce);

                Sub teacherSub = null;

                if (!isTeacher) {
                    teacherSub = CreateSub(currentInnerView, Glyph.Teacher, locationSub.View);

                    currentSet.Bind(teacherSub.Label).To(x => x.TeacherName).WithConversion(Resources.Converters.Coalesce);
                }

                var tasksForClass = new MSLRelatedItemsView(
                    Resources.Strings.ClassView.TasksDue,
                    MslTableViewSource.For<TaskRelatedTableViewCell>,
                    Resources.Strings.ClassView.TasksDueLonely
                );
                currentInnerView.Add(tasksForClass);

                var overdueTasksForSubject = new MSLRelatedItemsView(
                    Resources.Strings.ClassView.TasksOverdue,
                    MslTableViewSource.For<TaskRelatedTableViewCell>,
                    Resources.Strings.ClassView.TasksOverdueLonely
                );
                currentInnerView.Add(overdueTasksForSubject);

                currentSet.Bind(tasksForClass).Visibility().To(x => x.IsScheduled);
                currentSet.Bind(tasksForClass.HeaderLabel).For(x => x.TextColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
                currentSet.Bind(tasksForClass.TableView.Source).For(x => x.ItemsSource).To(x => x.TasksForClass);
                currentSet.Bind(tasksForClass.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);
                currentSet.Bind(tasksForClass.LonelyLabel).InvertedVisibility().To(x => x.TasksForClass.Count);

                currentSet.Bind(overdueTasksForSubject).Visibility().To(x => x.ShowOverdueTasks);
                // Overdue for SUBJECT, so subject color...
                currentSet.Bind(overdueTasksForSubject.HeaderLabel).For(x => x.TextColor).To(x => x.Item.Subject.Color).WithConversion("SubjectColorTo");
                currentSet.Bind(overdueTasksForSubject.TableView.Source).For(x => x.ItemsSource).To(x => x.OverdueTasksForSubject);
                currentSet.Bind(overdueTasksForSubject.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);
                currentSet.Bind(overdueTasksForSubject.LonelyLabel).InvertedVisibility().To(x => x.OverdueTasksForSubject.Count);

                currentSet.Apply();

                currentInnerView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

                ContentView.AddConstraints(
                    currentInnerView.WithSameLeft(ContentView),
                    currentInnerView.WithSameRight(ContentView),
                    currentInnerView.AtTopOf(_currentView, Resources.Dimens.DefaultMargin),
                    currentInnerView.AtBottomOf(_currentView),

                    tasksForClass.WithSameLeft(currentInnerView),
                    tasksForClass.WithSameRight(currentInnerView),
                    tasksForClass.Below((teacherSub ?? locationSub).View, Resources.Dimens.DefaultMargin),

                    overdueTasksForSubject.WithSameLeft(currentInnerView),
                    overdueTasksForSubject.WithSameRight(currentInnerView),
                    overdueTasksForSubject.Below(tasksForClass),
                    overdueTasksForSubject.WithSameBottom(currentInnerView)
                );
            }

            #region Schedule

            ;
            ContentView.Add(_scheduleView = new UIScrollView {
                BackgroundColor = UIColor.White
            });
            {
                var scheduleInnerView = new UIView {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                _scheduleView.Add(scheduleInnerView);

                var startEndDateSub = CreateSub(scheduleInnerView, Glyph.Schedule);
                var locationSub = CreateSub(scheduleInnerView, Glyph.Location, startEndDateSub.View);
                var teacherSub = CreateSub(scheduleInnerView, Glyph.Teacher, locationSub.View);

                var divider = AddDivider(scheduleInnerView, teacherSub.View, 10f);

                var timesTableView = new UIFixedTableView {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    AllowsSelection = false,
                    SeparatorStyle = UITableViewCellSeparatorStyle.None,
                    RowHeight = UITableView.AutomaticDimension,
                    EstimatedRowHeight = 59f
                };
                scheduleInnerView.Add(timesTableView);

                timesTableView.Source = new ClassTimeTableViewSource(timesTableView, typeof (ClassTimeCell));
                timesTableView.ReloadData();

                var scheduleSet = this.CreateBindingSet<ClassView, ClassViewModel>();

                scheduleSet.Bind(startEndDateSub.Label).To(x => x.ClassSchedule.StartEndDateText);
                scheduleSet.Bind(startEndDateSub.View).To(x => x.ClassSchedule.StartEndDateText).Visibility();
                scheduleSet.Bind(locationSub.Label).To(x => x.ClassSchedule.Location).WithConversion(Resources.Converters.Coalesce);
                scheduleSet.Bind(locationSub.View).To(x => x.ClassSchedule.HasMultipleLocations).InvertedVisibility();
                scheduleSet.Bind(teacherSub.Label).To(x => x.ClassSchedule.TeacherName).WithConversion(Resources.Converters.Coalesce);
                scheduleSet.Bind(timesTableView.Source).To(x => x.ClassSchedule.GroupedTimes);

                scheduleSet.Apply();

                ContentView.AddConstraints(
                    scheduleInnerView.WithSameLeft(ContentView),
                    scheduleInnerView.WithSameRight(ContentView),
                    scheduleInnerView.AtTopOf(_scheduleView, 10f),
                    scheduleInnerView.AtBottomOf(_scheduleView),

                    timesTableView.Below(divider, 5f),
                    timesTableView.AtLeftOf(scheduleInnerView, HorizontalMargin),
                    timesTableView.AtRightOf(scheduleInnerView, HorizontalMargin),
                    timesTableView.AtBottomOf(scheduleInnerView, 10f)
                );
            }

            #endregion

            #endregion

            #region Binding

            var set = this.CreateBindingSet<ClassView, ClassViewModel>();

            set.Bind(HeaderBackgroundView).For(x => x.BackgroundColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).To(x => x.SubTitle);

            set.Apply();

            #endregion

            #region Layout

            ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                _currentView.Fill(ContentView),
                _scheduleView.Fill(ContentView)
            );

            #endregion

            SetTabsState();
        }

        protected override UIView CreateContentView() {
            var contentView = new UIView {
                BackgroundColor = UIColor.White,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Add(contentView);

            View.AddConstraints(
                contentView.WithSameLeft(View),
                contentView.WithSameRight(View),
                contentView.Below(HeaderView, 10f),
                contentView.WithSameBottom(View)
            );

            return contentView;
        }

        private void Invalidate() {
            if (ViewModel != null && ViewModel.IsLoaded) {
                SetTabsState();

                if (!ViewModel.IsReadOnly) {
                    NavigationItem.SetRightBarButtonItem(
                        new UIBarButtonItem(Resources.Strings.Edit, UIBarButtonItemStyle.Plain, null).WithCommand(
                            ViewModel.EditCommand),
                        true
                    );
                }
                else if (ViewModel.CanLeave) {
                    NavigationItem.SetRightBarButtonItem(
                        new UIBarButtonItem(Resources.Strings.Leave, UIBarButtonItemStyle.Plain, null).WithCommand(
                            ViewModel.LeaveWithConfirmationCommand
                        ),
                        true
                    );
                }
            }
        }

        private bool _tabControlViewInserted;

        private void SetTabsState() {
            if (_tabControl == null) {
                return;
            }

            if (ViewModel?.Item == null || !ViewModel.IsScheduled || ViewModel.Item.IsOneOff) {
                HeaderView.RemoveArrangedSubview(_tabControlView);
                _tabControlViewInserted = false;

                _currentView.Hidden = ViewModel?.Item != null && !ViewModel.Item.IsOneOff;
                _scheduleView.Hidden = !_currentView.Hidden;
            }
            else {
                // For some reason HeaderView.ArrangedSubviews is always null, might be an issue witrh the binding lib
                if (!_tabControlViewInserted) {
                    HeaderView.AddArrangedSubview(_tabControlView);
                    _tabControlViewInserted = true;
                }

                bool isSchedule = (int)_tabControl.SelectedSegment == 1;

                _currentView.Hidden = isSchedule;
                _scheduleView.Hidden = !isSchedule;
            }
        }

        private void OnIsLeavingChanged() {
            if (ViewModel.IsLeaving) {
                iOSDialogService.ShowBusyIndicator(R.ClassInput.LeavingWithEllipsis);
            }
            else {
                iOSDialogService.HideBusyIndicator();
            }
        }

        class ClassTimeTableViewSource : MslTableViewSource {
            public ClassTimeTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) { }

            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
                => GetViewForHeader(tableView, section).Frame.Height;

            public override UIView GetViewForHeader(UITableView tableView, nint section) {
                var group = GetGroupAt((int) section);

                const float paddingTop = 15, paddingBottom = 5;

                var label = new UILabel(new CGRect(0, paddingTop, tableView.Bounds.Width, 0)) {
                    Text = group.Name,
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal),
                    TextColor = group.Color.ToNativeColor()
                };
                label.SizeToFit();

                var view = new UIView(new CGRect(0, 0, tableView.Bounds.Width, label.Frame.Height + paddingTop + paddingBottom));

                view.AddSubview(label);

                return view;
            }

            public override nfloat GetHeightForFooter(UITableView tableView, nint section) => 5f;
            public override UIView GetViewForFooter(UITableView tableView, nint section) => new UIView();

            public override nint NumberOfSections(UITableView tableView) => ItemsSource?.Cast<object>().Count() ?? 0;

            public override nint RowsInSection(UITableView tableview, nint section) => GetGroupAt((int)section)?.Times.Count ?? 0;

            protected virtual ClassViewModel.ClassScheduleViewModel.ClassTimeGroup GetGroupAt(int index)
                => ItemsSource.Cast<ClassViewModel.ClassScheduleViewModel.ClassTimeGroup>().ElementAt(index);

            protected override object GetItemAt(NSIndexPath indexPath)
                => GetGroupAt(indexPath.Section)?.Times.ElementAt(indexPath.Row);
        }

        [Register("ClassTimeCell"), MslTableViewCell(DynamicHeight = true)]
        class ClassTimeCell : MvxTableViewCell {
            public ClassTimeCell(IntPtr handle) : base(handle) {
                Initialize();
            }

            private void Initialize() {
                ContentView.TranslatesAutoresizingMaskIntoConstraints = false;

                var label = new MSLMultiLengthLabel {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    TextColor = Resources.Colors.Foreground,
                    Font = Resources.Fonts.FontNormal,
                    ContentEdgeInsets = new UIEdgeInsets(5f, 0, 0, 0)
                };
                ContentView.Add(label);

                var timeLabel = new UILabel {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    TextColor = Resources.Colors.Foreground,
                    Font = Resources.Fonts.FontSmall
                };
                ContentView.Add(timeLabel);

                UIView locationView, teacherView;

                var locationLabel = CreateLabelWithGlyph(Glyph.Location, out locationView);
                var teacherLabel = CreateLabelWithGlyph(Glyph.Teacher, out teacherView);

                ContentView.Add(locationView);
                ContentView.Add(teacherView);

                this.AddConstraints(ContentView.Fill(this));
                // For some reason the autolayout only works if we use this.ContentView.AddConstraints for these?
                ContentView.AddConstraints(
                    label.AtLeftOf(ContentView),
                    label.AtTopOf(ContentView),
                    label.AtRightOf(ContentView),

                    timeLabel.AtLeftOf(ContentView),
                    timeLabel.AtRightOf(ContentView),
                    timeLabel.Below(label),

                    locationView.AtLeftOf(ContentView),
                    locationView.AtRightOf(ContentView),
                    locationView.Below(timeLabel),

                    teacherView.AtLeftOf(ContentView),
                    locationView.AtRightOf(ContentView),
                    teacherView.Below(locationView),
                    teacherView.AtBottomOf(ContentView, 5f)
                );

                this.DelayBind(() => {
                    var set = this.CreateBindingSet<ClassTimeCell, ClassViewModel.ClassScheduleViewModel.ClassTimeViewModel>();

                    set.Bind(label).For(x => x.MultiLengthText).To(x => x.OccurrenceText);
                    set.Bind(timeLabel).To(x => x.Time);
                    set.Bind(locationLabel).To(x => x.Location);
                    set.Bind(locationView).Visibility().To(x => x.ShowLocation);
                    set.Bind(teacherLabel).To(x => x.TeacherName);
                    set.Bind(teacherView).Visibility().To(x => x.ShowTeacher);

                    set.Apply();
                });
            }

            private UILabel CreateLabelWithGlyph(Glyph glyph, out UIView parentView) {
                parentView = new MslView {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                var glyphView = new MSLIcon(glyph);
                parentView.Add(glyphView);

                var label = new UILabel {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    TextColor = Resources.Colors.Foreground,
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall, FontWeight.Medium)
                };
                parentView.Add(label);

                label.SetContentHuggingPriority((float)UILayoutPriority.Required, UILayoutConstraintAxis.Horizontal);

                parentView.AddConstraints(
                    glyphView.AtLeftOf(parentView, -2.5f),
                    glyphView.WithSameCenterY(label),

                    label.ToRightOf(glyphView),
                    label.WithSameTop(parentView),
                    label.Right().LessThanOrEqualTo().RightOf(parentView),
                    label.WithSameBottom(parentView)
                );

                return label;
            }
        }
    }
}