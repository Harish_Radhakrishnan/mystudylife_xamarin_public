using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    [Register("SearchView")]
    internal sealed class SearchView : MslViewController<SearchViewModel> {
        private UISearchBar _searchBar;
        private UIView _overlayView;
        private UIView _filterView;
        private UISegmentedControl _filterControl;

        private UITableView _classesTableView;
        private UITableView _tasksTableView;
        private UITableView _examsTableView;

        private MslLonelyView _classesLonelyView;
        private MslLonelyView _tasksLonelyView;
        private MslLonelyView _examsLonelyView;


        public override string Title => Resources.Strings.Search;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            View.BackgroundColor = UIColor.White;

            // TODO(iOS) style
            var searchBar = _searchBar = new UISearchBar {
                Placeholder = "Search your study life",
                BackgroundColor = UIColor.Clear,
                TintColor = Resources.Colors.Accent
            };

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0)) {
                searchBar.SearchTextField.BackgroundColor = UIColor.White;
            }

            ParentViewController.NavigationItem.TitleView = searchBar;

            if (ParentViewController.NavigationItem.LeftBarButtonItem != null) {
                ParentViewController.NavigationItem.LeftBarButtonItem.Clicked += MenuBarButtonItemOnClicked;
            }

            searchBar.SizeToFit();

            searchBar.OnEditingStarted += SearchBarOnEditingStarted;
            searchBar.OnEditingStopped += SearchBarOnEditingStopped;
            searchBar.CancelButtonClicked += SearchBarOnCancelButtonClicked;
            searchBar.SearchButtonClicked += SearchBarOnSearchButtonClicked;

            #region Creation

            _overlayView = new UIView {
                BackgroundColor = UIColor.Black,
                Alpha = 0
            };
            _overlayView.AddGestureRecognizer(new UITapGestureRecognizer(OverlayViewOnTapped));

            var filterView = _filterView = new UIView {
                BackgroundColor = Resources.Colors.Accent
            };
            Add(filterView);

            var filterControl = _filterControl = new UISegmentedControl {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TintColor = UIColor.White
            };
            filterControl.ApplyAppearance();
            filterView.Add(filterControl);

            filterControl.InsertSegment(Resources.Strings.Classes, 0, false);
            filterControl.InsertSegment(Resources.Strings.Tasks, 1, false);
            filterControl.InsertSegment(Resources.Strings.Exams, 2, false);

            filterControl.SelectedSegment = 0;

            filterControl.ValueChanged += FilterControlOnValueChanged;


            _classesTableView = new UITableView {
                AllowsSelection = true
            };
            Add(_classesTableView);

            var classesViewSource = new ClassTableViewSource(_classesTableView, User.Current.IsTeacher);

            _classesTableView.Source = classesViewSource;
            _classesTableView.ReloadData();


            _tasksTableView = new UITableView {
                AllowsSelection = true
            };
            Add(_tasksTableView);

            var tasksViewSource = MslTableViewSource.For<TaskTableViewCell>(_tasksTableView);

            _tasksTableView.Source = tasksViewSource;
            _tasksTableView.ReloadData();


            _examsTableView = new UITableView {
                AllowsSelection = true
            };
            Add(_examsTableView);

            var examsViewSource = MslTableViewSource.For<ExamTableViewCell>(_examsTableView);

            _examsTableView.Source = examsViewSource;
            _examsTableView.ReloadData();

            _classesLonelyView = new MslLonelyView(Glyph.Class) {
                Text = Resources.Strings.ClassesLonely
            };
            Add(_classesLonelyView);
            _tasksLonelyView = new MslLonelyView(Glyph.Task) {
                Text = Resources.Strings.TasksLonely
            };
            Add(_tasksLonelyView);
            _examsLonelyView = new MslLonelyView(Glyph.Exam) {
                Text = Resources.Strings.ExamsLonely
            };
            Add(_examsLonelyView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<SearchView, SearchViewModel>();

            set.Bind(classesViewSource).For(x => x.ItemsSource).To(x => x.ClassesViewSource.View);
            set.Bind(classesViewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Bind(tasksViewSource).For(x => x.ItemsSource).To(x => x.TasksViewSource.View);
            set.Bind(tasksViewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Bind(examsViewSource).For(x => x.ItemsSource).To(x => x.ExamsViewSource.View);
            set.Bind(examsViewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                filterView.WithSameLeft(View),
                filterView.WithSameRight(View),
                filterView.WithSameTop(View),

                filterControl.WithSameLeft(filterView).Plus(15),
                filterControl.WithSameRight(filterView).Minus(15),
                filterControl.WithSameTop(filterView).Plus(10),
                filterControl.WithSameBottom(filterView).Minus(10),

                _classesTableView.Below(filterView),
                _classesTableView.WithSameLeft(View),
                _classesTableView.WithSameRight(View),
                _classesTableView.WithSameBottom(View),

                _tasksTableView.Below(filterView),
                _tasksTableView.WithSameLeft(View),
                _tasksTableView.WithSameRight(View),
                _tasksTableView.WithSameBottom(View),

                _examsTableView.Below(filterView),
                _examsTableView.WithSameLeft(View),
                _examsTableView.WithSameRight(View),
                _examsTableView.WithSameBottom(View)
            );

            #endregion

            this.SetVisibilites();

            searchBar.BecomeFirstResponder();
        }

        private void MenuBarButtonItemOnClicked(object sender, EventArgs e) {
            this.OverlayViewOnTapped();
        }

        public override void ViewWillLayoutSubviews() {
            base.ViewWillLayoutSubviews();

            _overlayView.Frame = this.View.Bounds;
        }

        private async void SearchBarOnSearchButtonClicked(object sender, EventArgs eventArgs) {
            var searchBar = (UISearchBar) sender;

            this.UnfocusSearchBar();

            await this.ViewModel.ProcessQueryText(searchBar.Text);

            this.SetVisibilites();
        }

        private void SearchBarOnCancelButtonClicked(object sender, EventArgs e) {
            this.CancelSearch();
        }

        private void SearchBarOnEditingStarted(object sender, EventArgs e) {
            _overlayView.Alpha = 0;
            _overlayView.Frame = this.View.Bounds;
            this.View.Add(_overlayView);

            UIView.Animate(0.3f, () => _overlayView.Alpha = 0.6f);
        }

        private void SearchBarOnEditingStopped(object sender, EventArgs e) {
            if (String.IsNullOrEmpty(_searchBar.Text)) {
                this.UnfocusSearchBar();
            }
        }

        private void CancelSearch() {
            this.ViewModel.ClearQuery();

            this.UnfocusSearchBar();

            this.SetVisibilites();
        }

        private void UnfocusSearchBar() {
            UIView.Animate(0.3f, () => _overlayView.Alpha = 0f, () => _overlayView.RemoveFromSuperview());

            _searchBar.ResignFirstResponder();
        }

        private void OverlayViewOnTapped() {
            if (String.IsNullOrEmpty(_searchBar.Text)) {
                this.CancelSearch();
            }
            else {
                this.UnfocusSearchBar();
            }
        }

        private void FilterControlOnValueChanged(object sender, EventArgs e) {
            this.SetVisibilites();
        }

        private void SetVisibilites() {
            var selectedSegement = (int)_filterControl.SelectedSegment;

            bool noSearchText = String.IsNullOrEmpty(_searchBar.Text);

            _filterView.Hidden = noSearchText;

            _classesTableView.Hidden = noSearchText || this.ViewModel.ClassesViewSource.Count == 0 || selectedSegement != 0;
            _tasksTableView.Hidden = noSearchText || this.ViewModel.TasksViewSource.Count == 0 || selectedSegement != 1;
            _examsTableView.Hidden = noSearchText || this.ViewModel.ExamsViewSource.Count == 0 || selectedSegement != 2;

            _classesLonelyView.Hidden = noSearchText || this.ViewModel.ClassesViewSource.Count > 0 || selectedSegement != 0;
            _tasksLonelyView.Hidden = noSearchText || this.ViewModel.TasksViewSource.Count > 0 || selectedSegement != 1;
            _examsLonelyView.Hidden = noSearchText || this.ViewModel.ExamsViewSource.Count > 0 || selectedSegement != 2;
        }
    }
}