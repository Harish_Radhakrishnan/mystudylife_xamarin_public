using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    internal sealed class AboutView : MslDialogViewController<AboutViewModel> {
        public override string Title {
            get { return Resources.Strings.About; }
        }

        protected override void AdjustLayoutForViewWillAppear() {
            base.AdjustLayoutForViewWillAppear();

            NavigationItem.HidesBackButton = false;
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();
            
            var bindings = this.CreateInlineBindingTarget<AboutViewModel>();

            Root = new RootElement {
                UnevenRows = true
            };

            Root.Add(new [] {
                new Section {
                    new StringElement("Feedback + Support").Bind(bindings, x => x.AppVersion)
                },
                new Section("Legal") {
                    new StringElement("Privacy"),
                    new StringElement("Terms of Use")
                }
            });
        }
    }
}