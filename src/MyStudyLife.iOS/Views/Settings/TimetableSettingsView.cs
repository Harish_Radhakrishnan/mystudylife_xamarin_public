using System;
using System.Linq;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using CoreText;
using Foundation;
using UIKit;
using MyStudyLife.Data.Settings;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.iOS.Views.Input;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    [Register("TimetableSettingsView")]
    internal sealed class TimetableSettingsView : BaseSettingsView<TimetableSettingsViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _vmListener;
        [UsedImplicitly] private MvxPropertyChangedListener _settingsListener;

        private Section _weekRotationSection;
        private Section _dayRotationSection;
        
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            Action attachSettingsListener = () => {
                _settingsListener = new MvxPropertyChangedListener(this.ViewModel.Settings)
                    .Listen(() => this.ViewModel.Settings.Mode, SetSettingsVisibility);
            };

            if (this.ViewModel.Settings == null) {
                _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                    .Listen(() => this.ViewModel.Settings, () => {
                        if (this.ViewModel.Settings != null) {
                            attachSettingsListener();
                        }

                        SetSettingsVisibility();
                    });
            }
            else {
                attachSettingsListener();
            }

            var bindings = this.CreateInlineBindingTarget<TimetableSettingsViewModel>();

            var helpTextLabel = new HelpTextAttributedLabel {
                TextColor = Resources.Colors.WarningForeground,
                ContentEdgeInsets = new UIEdgeInsets(15f, 15f, 15f, 15f),
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                UserInteractionEnabled = true,
                LinkAttributes = new CTStringAttributes {
                    ForegroundColor = Resources.Colors.WarningForeground.CGColor,
                    UnderlineStyle = CTUnderlineStyle.Single
                }.Dictionary
            };

            string helpText =
                "We've added a brand new way to setup rotating schedules - you can add holidays that push your schedule too. " +
                "To get started, add an academic year and take a look at our upgrade guide." +
                Environment.NewLine +
                Environment.NewLine +
                "These settings will be removed in the future but will automatically disappear when you no longer need them.";

            helpTextLabel.Text = new NSString(helpText);

            helpTextLabel.AddLinkToURL(
                new NSUrl(this.ViewModel.UpgradeGuideUri.ToString()),
                new NSRange(helpText.IndexOf("upgrade guide"), "upgrade guide".Length)
            );

            helpTextLabel.SizeToFit();

           this.TableView.TableHeaderView = helpTextLabel;
            
            Root = new MslRootElement(Resources.Strings.Timetable) {
                UnevenRows = true
            };

            Root.Add(new[] {
                new Section {
                    new MslRootElement(Resources.Strings.Mode, new RadioGroup("Mode")) {
                        new Section {
                            new MslRadioElement(Resources.Strings.Fixed) {
                                RadioIdx = 0,
                                ObjectValue = TimetableMode.Fixed
                            },
                            new MslRadioElement(Resources.Strings.WeekRotation) {
                                RadioIdx = 1,
                                ObjectValue = TimetableMode.WeekRotation
                            },
                            new MslRadioElement(Resources.Strings.DayRotation) {
                                RadioIdx = 2,
                                ObjectValue = TimetableMode.DayRotation
                            }
                        }
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.Mode) as Element
                }
            });

            var weekRotationWeekCountSection = new Section();
            weekRotationWeekCountSection.AddAll(
                Enumerable.Range(
                    TimetableSettings.MinRotationWeekCount,
                    (TimetableSettings.MaxRotationWeekCount - TimetableSettings.MinRotationWeekCount) + 1
                ).Select(x => new MslRadioElement(String.Concat(x, " Weeks")) {
                    ObjectValue = x
                })
            );

            _weekRotationSection = new Section(Resources.Strings.WeekRotation) {
                new MslRootElement(Resources.Strings.NumberOfWeeks, new RadioGroup("WeekCount")) {
                    weekRotationWeekCountSection
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.WeekCount) as Element,

                new MslRootElement(Resources.Strings.CurrentWeek, new RadioGroup("SetWeek")) {
                    new MvxRadioSection((w) => {
                        var week = (KVP<int, string>) w;

                        return new MslRadioElement(week.Value) {
                            ObjectValue = week.Key
                        };
                    }).Bind(bindings, x => x.ItemsSource, x => x.RotationWeeks)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.SetWeek) as Element
            };
            _weekRotationSection.FooterView = new HelpTextLabel {
                Text = Resources.Strings.TimetableSettingsView.WeekRotationDisam
            };

            var dayRotationDayCountSection = new Section();
            dayRotationDayCountSection.AddAll(
                Enumerable.Range(
                    TimetableSettings.MinRotationDayCount,
                    (TimetableSettings.MaxRotationDayCount - TimetableSettings.MinRotationDayCount) + 1
                ).Select(x => new MslRadioElement(String.Concat(x, " Days")) {
                    ObjectValue = x
                })
            );

            _dayRotationSection = new Section(Resources.Strings.DayRotation) {
                new MslRootElement(Resources.Strings.NumberOfDays, new RadioGroup("DayCount")) {
                    dayRotationDayCountSection
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.DayCount) as Element,

                new MslRootElement(Resources.Strings.CurrentDay, new RadioGroup("SetDay")) {
                    new MvxRadioSection((d) => {
                        var day = (KVP<int, string>) d;

                        return new MslRadioElement(day.Value) {
                            ObjectValue = day.Key
                        };
                    }).Bind(bindings, x => x.ItemsSource, x => x.RotationDays)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.SetDay) as Element,

                new CheckboxFlagsElement(Resources.Strings.Days, new Group("Days"))
                    .Bind(bindings, x => x.ItemsSource, x => x.Days)
                    .Bind(bindings, x => x.Value, x => x.Settings.Days, new DaysOfWeekToIntConverter()) as Element
            };
            _dayRotationSection.FooterView = new HelpTextLabel {
                Text = Resources.Strings.TimetableSettingsView.DayRotationDisam
            };

            this.SetSettingsVisibility();
        }

        private void SetSettingsVisibility() {
            if (Root == null || _weekRotationSection == null || this.ViewModel.Settings == null) {
                return;
            }

            if (this.ViewModel.User.IsStudentWithUnmanagedSchool) {
                this.TableView.TableHeaderView = new HelpTextLabel {
                    Text = "Your timetable settings are managed by your school. If you think they may incorrect please ask one of your teachers for more information.",
                    TextColor = Resources.Colors.WarningForeground
                };

                this.TableView.AllowsSelection = false;

                foreach (var section in this.Root.Sections) {
                    foreach (var element in section.Elements) {
                        var cell = element.GetActiveCell();

                        if (cell != null) {
                            cell.UserInteractionEnabled = false;
                        }
                    }
                }
            }

            if (this.ViewModel.Settings.IsWeekRotation) {
                if (!this.Root.Sections.Contains(_weekRotationSection)) {
                    this.Root.Add(_weekRotationSection);
                }
            }
            else {
                this.Root.Remove(_weekRotationSection);
            }

            if (this.ViewModel.Settings.IsDayRotation) {
                if (!this.Root.Sections.Contains(_dayRotationSection)) {
                    this.Root.Add(_dayRotationSection);
                }
            }
            else {
                this.Root.Remove(_dayRotationSection);
            }
        }
    }
}