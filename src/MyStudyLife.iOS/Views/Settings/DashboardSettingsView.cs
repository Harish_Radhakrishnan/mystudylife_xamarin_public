using System.Collections.Generic;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MyStudyLife.Data.Settings;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    [Register(nameof(DashboardSettingsView))]
    internal sealed class DashboardSettingsView : BaseSettingsView<DashboardSettingsViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _vmListener;

        private Section _examsSection;

        private Element _examsTimescaleElement;
        private Element _examsHideWhenNoneElement;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this._vmListener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.ShowExams), SetSettingsVisibility);

            var bindings = this.CreateInlineBindingTarget<DashboardSettingsViewModel>();

            const string tasksTimescaleGroupName = "TasksTimescale";
            const string examsTimescaleGroupName = "ExamsTimescale";

            Root = new MslRootElement(Resources.Strings.Dashboard) {
                new Section(Resources.Strings.Tasks) {
                    (Element)new MslRootElement(Resources.Strings.Dashboard_tasks_timescale, new RadioGroup(tasksTimescaleGroupName)) {
                        new MvxRadioSection(item => {
                            var kvp = (KeyValuePair<DashboardTasksTimescale, string>)item;

                            // Don't remove ToTitleCase() here, it's for iOS styling
                            return new MslRadioElement(kvp.Value.ToTitleCase(), tasksTimescaleGroupName) {
                                ObjectValue = kvp.Key
                            };
                        }).Bind(bindings, x => x.ItemsSource, x => x.TasksTimescaleOptions)
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.TasksTimescale)
                },
                (_examsSection = new Section(Resources.Strings.Exams) {
                    new MslBooleanElement(Resources.Strings.Dashboard_show_exams)
                        .Bind(bindings, x => x.ShowExams),
                    (_examsTimescaleElement = new MslRootElement(Resources.Strings.Dashboard_exams_timescale, new RadioGroup(examsTimescaleGroupName)) {
                        new MvxRadioSection(item => {
                            var kvp = (KeyValuePair<DashboardExamsTimescale, string>)item;

                            // Don't remove ToTitleCase() here, it's for iOS styling
                            return new MslRadioElement(kvp.Value, examsTimescaleGroupName) {
                                ObjectValue = kvp.Key
                            };
                        }).Bind(bindings, x => x.ItemsSource, x => x.ExamsTimescaleOptions)
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.ExamsTimescale)),
                    // Caption generated in view model is too wide setting UI on iOS
                    (_examsHideWhenNoneElement = new MslBooleanElement(Resources.Strings.DashboardExamsHideWhenEmpty)
                        .Bind(bindings, x => x.ExamsHideWhenNone)
                    )
                })
            };

            this.SetSettingsVisibility();
        }

        private void SetSettingsVisibility() {
            if (Root == null || _examsSection == null) {
                return;
            }

            if (this.ViewModel?.ShowExams ?? true) {
                if (!_examsSection.Elements.Contains(_examsTimescaleElement)) {
                    _examsSection.Add(_examsTimescaleElement);
                }
                if (!_examsSection.Elements.Contains(_examsHideWhenNoneElement)) {
                    _examsSection.Add(_examsHideWhenNoneElement);
                }
            }
            else {
                _examsSection.Remove(_examsTimescaleElement);
                _examsSection.Remove(_examsHideWhenNoneElement);
            }
        }
    }
}