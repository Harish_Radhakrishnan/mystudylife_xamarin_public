using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using MyStudyLife.Globalization;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    internal sealed class GeneralSettingsView : BaseSettingsView<GeneralSettingsViewModel> {
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var bindings = this.CreateInlineBindingTarget<GeneralSettingsViewModel>();

            Root = new MslRootElement(Resources.Strings.General) {
                UnevenRows = true
            };

            Root.Add(
                new Section {
                    new MslRootElement(Resources.Strings.FirstDay, new RadioGroup("FirstDay")) {
                        new MvxRadioSection((dm) => {
                            var day = (DayMapping)dm;

                            return new MslRadioElement(day.Name) {
                                ObjectValue = day.DayOfWeek
                            };
                        }).Bind(bindings, x => x.ItemsSource, x => x.Days)
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.FirstDayOfWeek) as Element,
                    new MslRootElement(Resources.Strings.RotationSchedule, new RadioGroup("IsRotationScheduleLettered")) {
                        new Section {
                            new MslRadioElement(Resources.Strings.Numbered) {
                                RadioIdx = 0
                            },
                            new MslRadioElement(Resources.Strings.Lettered) {
                                RadioIdx = 1
                            }
                        }
                    }.Bind(bindings, x => x.RadioSelected, x => x.Settings.IsRotationScheduleLettered, new NullableBooleanToIntConverter()) as Element,
                    new TimeInlineElement("Default Start Time").Bind(bindings, x => x.Settings.DefaultStartTime, new TimeSpanToDateTimeConverter()),
                    new NumberElement("Default Duration") {
                        Min = 5,
                        Max = 720,
                        Suffix = "minutes"
                    }.Bind(bindings, x => x.Settings.DefaultDuration)
                }
            );
        }
    }
}