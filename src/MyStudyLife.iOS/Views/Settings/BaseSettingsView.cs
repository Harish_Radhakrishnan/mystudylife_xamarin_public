using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    internal abstract class BaseSettingsView<TViewModel> : MslDialogViewController<TViewModel> where TViewModel : BaseViewModel, ISettingsViewModel {
        public override void ViewWillDisappear(bool animated) {
            base.ViewWillDisappear(animated);

            if (this.ViewModel != null) {
                this.ViewModel.SaveChanges();
            }
        }
    }
}