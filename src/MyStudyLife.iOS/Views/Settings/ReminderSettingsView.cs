using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MyStudyLife.Data.Settings;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.iOS.Views.Settings {
    [Register("ReminderSettingsView")]
    internal sealed class ReminderSettingsView : BaseSettingsView<ReminderSettingsViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _vmListener;
        [UsedImplicitly] private MvxPropertyChangedListener _settingsListener;

        private Section _classSection;
        private Section _examSection;
        private Section _taskSection;

        private Element _classTimeElement;
        private Element _examTimeElement;
        private Element _taskTimeElement;
        
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            Action attachSettingsListener = () => {
                _settingsListener = new MvxPropertyChangedListener(this.ViewModel.Settings)
                    .Listen(() => this.ViewModel.Settings.RemindersEnabled, SetSettingsVisibility)
                    .Listen(() => this.ViewModel.Settings.ClassRemindersEnabled, SetSettingsVisibility)
                    .Listen(() => this.ViewModel.Settings.ExamRemindersEnabled, SetSettingsVisibility)
                    .Listen(() => this.ViewModel.Settings.TaskRemindersEnabled, SetSettingsVisibility);
            };

            if (this.ViewModel.Settings == null) {
                _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                    .Listen(() => this.ViewModel.Settings, () => {
                        if (this.ViewModel.Settings != null) {
                            attachSettingsListener();
                        }

                        SetSettingsVisibility();
                    });
            }
            else {
                attachSettingsListener();
            }

            var bindings = this.CreateInlineBindingTarget<ReminderSettingsViewModel>();

            Root = new MslRootElement(Resources.Strings.Reminders) {
                UnevenRows = true
            };

            TableView.TableHeaderView = new HelpTextLabel {
                Text  = "Reminders allow you be notified before a class or exam starts and can alert you about incomplete tasks the day before they are due."
            };

            Root.Add(new [] {
                new Section {
                    new MslBooleanElement(Resources.Strings.Reminders).Bind(bindings, x => x.Settings.RemindersEnabled)
                },
                (_classSection = new Section(Resources.Strings.Classes) {
                    new MslBooleanElement("Class Reminders").Bind(bindings, x => x.Settings.ClassRemindersEnabled),
                    (_classTimeElement = new MslRootElement("Remind Me", new RadioGroup("ClassReminders")) {
                        new Section {
                            new MslRadioElement("5 Minutes Before", "ClassReminders") {
                                ObjectValue = ReminderTime.Minutes5,
                                RadioIdx = 0
                            },
                            new MslRadioElement("15 Minutes Before", "ClassReminders") {
                                ObjectValue = ReminderTime.Minutes15,
                                RadioIdx = 1
                            },
                            new MslRadioElement("30 Minutes Before", "ClassReminders") {
                                ObjectValue = ReminderTime.Minutes30,
                                RadioIdx = 2
                            }
                        }
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.ClassReminderTime))
                }),
                (_examSection = new Section(Resources.Strings.Exams) {
                    new MslBooleanElement("Exam Reminders").Bind(bindings, x => x.Settings.ExamRemindersEnabled),
                    (_examTimeElement = new MslRootElement("Remind Me", new RadioGroup("ExamReminders")) {
                        new Section {
                            new MslRadioElement("5 Minutes Before", "ExamReminders") {
                                ObjectValue = ReminderTime.Minutes5,
                                RadioIdx = 0
                            },
                            new MslRadioElement("15 Minutes Before", "ExamReminders") {
                                ObjectValue = ReminderTime.Minutes15,
                                RadioIdx = 1
                            },
                            new MslRadioElement("30 Minutes Before", "ExamReminders") {
                                ObjectValue = ReminderTime.Minutes30,
                                RadioIdx = 2
                            }
                        }
                    }.Bind(bindings, x => x.RadioSelectedItem, x => x.Settings.ExamReminderTime))
                }),
                (_taskSection = new Section(Resources.Strings.Tasks) {
                    new MslBooleanElement("Task Reminders").Bind(bindings, x => x.Settings.TaskRemindersEnabled),
                    (_taskTimeElement = new TimeInlineElement(Resources.Strings.Time).Bind(bindings, x => x.Value, x => x.Settings.TaskReminderTime, new TimeSpanToDateTimeConverter()))
                })
            });

            this.SetSettingsVisibility();
        }

        private void SetSettingsVisibility() {
            if (Root == null || _classSection == null) {
                return;
            }

            var show = this.ViewModel.Settings.RemindersEnabled;

            if (show) {
                if (!this.Root.Sections.Contains(_classSection)) {
                    this.Root.Add(_classSection);
                }
                if (!this.Root.Sections.Contains(_examSection)) {
                    this.Root.Add(_examSection);
                }
                if (!this.Root.Sections.Contains(_taskSection)) {
                    this.Root.Add(_taskSection);
                }

                if (this.ViewModel.Settings.ClassRemindersEnabled) {
                    if (!_classSection.Elements.Contains(_classTimeElement)) {
                        _classSection.Add(_classTimeElement);
                    }
                }
                else {
                    _classSection.Remove(_classTimeElement);
                }

                if (this.ViewModel.Settings.ExamRemindersEnabled) {
                    if (!_examSection.Elements.Contains(_examTimeElement)) {
                        _examSection.Add(_examTimeElement);
                    }
                }
                else {
                    _examSection.Remove(_examTimeElement);
                }

                if (this.ViewModel.Settings.TaskRemindersEnabled) {
                    if (!_taskSection.Elements.Contains(_taskTimeElement)) {
                        _taskSection.Add(_taskTimeElement);
                    }
                }
                else {
                    _taskSection.Remove(_taskTimeElement);
                }
            }
            else {
                this.Root.Remove(_classSection);
                this.Root.Remove(_examSection);
                this.Root.Remove(_taskSection);
            }
        }
    }
}