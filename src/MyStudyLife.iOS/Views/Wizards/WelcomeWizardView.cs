using System;
using CoreGraphics;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using UIKit;
using MyStudyLife.Configuration;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.iOS.Platform;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ViewModels.Wizards;

namespace MyStudyLife.iOS.Views.Wizards {
    [Register("WelcomeWizardView")]
    [MvxRootPresentation]
    internal sealed class WelcomeWizardView : MslViewController<WelcomeWizardViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        /// <summary>
        ///     We're storing a local copy else it's null when
        ///     ViewWillDisappear (so we can't reset styles).
        /// </summary>
        private UINavigationController _navigationController;

        private UIBarButtonItem _skipButton, _previousButton, _nextButton, _doneButton, _addClassesButton;

        private UIView _headerView;
        private MSLLabel _headerLabel;

        private NSLayoutConstraint _stepContainerConstraint;
        private UIView _stepContainer;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.Step, SetState)
                .Listen(() => this.ViewModel.IsBusy, OnBusyChanged);

            View.BackgroundColor = UIColor.White;

            #region Creation

            _skipButton = new UIBarButtonItem(Resources.Strings.Skip, UIBarButtonItemStyle.Plain, null);
            _previousButton = new UIBarButtonItem(Resources.Strings.Back, UIBarButtonItemStyle.Plain, null);
            _nextButton = new UIBarButtonItem(Resources.Strings.Next, UIBarButtonItemStyle.Done, null);
            _doneButton = new UIBarButtonItem(Resources.Strings.NoThanks, UIBarButtonItemStyle.Plain, null);
            _addClassesButton = new UIBarButtonItem(Resources.Strings.Yes, UIBarButtonItemStyle.Done, null);

            _headerView = new UIView {
                BackgroundColor = Resources.Colors.AccentDark,
            };
            Add(_headerView);

            _headerLabel = new MSLLabel {
                Text = "Hello there",
                TextColor = UIColor.White,
                Font = Resources.Fonts.OfSize(FontSize.Huge, FontWeight.Bold),
                ContentEdgeInsets = new UIEdgeInsets(15, 15, 15, 15)
            };
            _headerView.Add(_headerLabel);

            _stepContainer = new UIView();
            Add(_stepContainer);

            var step1View = CreateStep("Welcome to My Study Life");
            {
                var text =
                    "As you're new we're going to guide you through some of the basic settings and setup to help you get around.\n\r" +
                    "If you get stuck at any point on the way, find an issue or want to give feedback please visit our feedback + support portal.";

                var textLabel = new MslAttributedLabel {
                    Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                    Lines = 0,
                    LineBreakMode = UILineBreakMode.WordWrap,
                    UserInteractionEnabled = true
                };
                step1View.Add(textLabel);

                // Text must be set after creation for some reason, otherwise lines, font etc. are overridden.
                textLabel.Text = new NSString(text);

                // ReSharper disable once StringIndexOfIsCultureSpecific.1
                textLabel.AddLinkToURL(new NSUrl(MslConfig.Current.FeedbackUri), new NSRange(
                    text.IndexOf("feedback + support portal"),
                    "feedback + support portal".Length
                ));

                step1View.AddConstraints(
                    textLabel.AtLeftOf(step1View, 15f),
                    textLabel.AtRightOf(step1View, 15f),
                    textLabel.Below(step1View.Subviews[step1View.Subviews.Length - 2], 10f),
                    textLabel.WithSameBottom(step1View)
                );
            }
            _stepContainer.Add(step1View);

            var step2View = CreateStep("A quick introduction");
            {
                var text =
                    "My Study Life works using schedules (years + terms), subjects, classes, tasks (homework) and exams. " +
                    "Subjects can be the name of a course or project and group classes, tasks and exams together.\n\r" +

                    "Have one of those tricky week or day rotation timetables? My Study Life supports those and takes the pain " +
                    "out of remembering which day or week it is. To learn more take a look at our knowledge base.";

                var textLabel = new MslAttributedLabel {
                    Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                    Lines = 0,
                    LineBreakMode = UILineBreakMode.WordWrap,
                    UserInteractionEnabled = true
                };
                step2View.Add(textLabel);

                textLabel.Text = new NSString(text);

                // ReSharper disable once StringIndexOfIsCultureSpecific.1
                textLabel.AddLinkToURL(new NSUrl(MslConfig.Current.FeedbackUri), new NSRange(
                    text.IndexOf("knowledge base"),
                    "knowledge base".Length
                ));

                step2View.AddConstraints(
                    textLabel.AtLeftOf(step2View, 15f),
                    textLabel.AtRightOf(step2View, 15f),
                    textLabel.Below(step2View.Subviews[step2View.Subviews.Length - 2], 10f),
                    textLabel.WithSameBottom(step2View)
                );
            }
            _stepContainer.Add(step2View);

            UIView step3View;
            {
                var header = new UIView();

                var titleLabel = new MSLLabel {
                    Text = "Time",
                    TextColor = Resources.Colors.Foreground,
                    Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeLarge, FontWeight.Medium),
                    ContentEdgeInsets = new UIEdgeInsets(20f, 15f, 0, 15f),
                    Lines = 0,
                    LineBreakMode = UILineBreakMode.WordWrap,
                    TranslatesAutoresizingMaskIntoConstraints = true
                };
                header.Add(titleLabel);

                var textLabel = new MSLLabel {
                    Text = "Configure My Study Life to work best for you. These settings will help you add classes and exams more quickly.",
                    Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                    ContentEdgeInsets = new UIEdgeInsets(10f, 15f, 20f, 15f),
                    Lines = 0,
                    LineBreakMode = UILineBreakMode.WordWrap,
                    TranslatesAutoresizingMaskIntoConstraints = true
                };
                header.Add(textLabel);

                var titleLabelHeight = titleLabel.SizeThatFits(new CGSize(this.View.Frame.Width, nfloat.MaxValue)).Height;
                var textLabelHeight = textLabel.SizeThatFits(new CGSize(this.View.Frame.Width, nfloat.MaxValue)).Height;

                titleLabel.Frame = new CGRect(0, 0, this.View.Frame.Width, titleLabelHeight);
                textLabel.Frame = new CGRect(0, titleLabelHeight, this.View.Frame.Width, textLabelHeight);

                header.Frame = new CGRect(0, 0, this.View.Frame.Width, titleLabelHeight + textLabelHeight);

                var bindings = this.CreateInlineBindingTarget<WelcomeWizardViewModel>();

                var root = new MslRootElement {
                    new Section {
                    new TimeInlineElement("Default Start Time").Bind(bindings, x => x.Settings.DefaultStartTime,
                        new TimeSpanToDateTimeConverter()),
                    new NumberElement("Default Duration") {
                        Min = 5,
                        Max = 720,
                        Suffix = "minutes"
                    }.Bind(bindings, x => x.Settings.DefaultDuration)
                }
                };

                root.UnevenRows = true;

                var dvc = new DialogViewController(root) {
                    TableView = {
                        TableHeaderView = header,
                        BackgroundColor = UIColor.White,
                        TranslatesAutoresizingMaskIntoConstraints = false
                    }
                };

                step3View = dvc.TableView;
            }
            _stepContainer.Add(step3View);

            var step4View = CreateStep("That's it!");
            {
                var text =
                    "If you get stuck at any point on the way, find an issue or want to give feedback please visit our feedback + support portal.\n\r" +

                    "Would you like to head over to the schedule screen to start adding your classes?";

                var textLabel = new MslAttributedLabel {
                    Text = new NSString(text),
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal),
                    Lines = 0,
                    LineBreakMode = UILineBreakMode.WordWrap,
                    UserInteractionEnabled = true
                };
                step4View.Add(textLabel);

                textLabel.Text = new NSString(text);

                // ReSharper disable once StringIndexOfIsCultureSpecific.1
                textLabel.AddLinkToURL(new NSUrl(MslConfig.Current.FeedbackUri), new NSRange(
                    text.IndexOf("feedback + support portal"),
                    "feedback + support portal".Length
                ));

                step4View.AddConstraints(
                    textLabel.AtLeftOf(step4View, 15f),
                    textLabel.AtRightOf(step4View, 15f),
                    textLabel.Below(step4View.Subviews[step4View.Subviews.Length - 2], 10f),
                    textLabel.WithSameBottom(step4View)
                );
            }
            _stepContainer.Add(step4View);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<WelcomeWizardView, WelcomeWizardViewModel>();

            set.Bind(_skipButton).To(x => x.SkipCommand);
            set.Bind(_previousButton).To(x => x.PreviousStepCommand);
            set.Bind(_nextButton).To(x => x.NextStepCommand);
            set.Bind(_doneButton).To(x => x.DoneCommand);
            set.Bind(_addClassesButton).To(x => x.AddClassesCommand);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            _stepContainerConstraint = _stepContainer.AtLeftOf(this.View).ToLayoutConstraints().Single();

            View.AddConstraint(_stepContainerConstraint);

            View.AddConstraints(
                _headerView.AtLeftOf(this.View),
                _headerView.AtRightOf(this.View),
                _headerView.AtTopOf(this.View),
                _headerView.Height().EqualTo(143f),

                _headerLabel.AtLeftOf(_headerView),
                _headerLabel.AtRightOf(_headerView),
                _headerLabel.AtBottomOf(_headerView),

                _stepContainer.Width().EqualTo().WidthOf(this.View).WithMultiplier(4),
                _stepContainer.Below(_headerView),
                _stepContainer.WithSameBottom(this.View),

                step1View.AtLeftOf(_stepContainer),
                step1View.WithSameWidth(this.View),
                step1View.AtTopOf(_stepContainer),

                step2View.ToRightOf(step1View),
                step2View.WithSameWidth(this.View),
                step2View.WithSameTop(step1View),

                step3View.ToRightOf(step2View),
                step3View.WithSameWidth(this.View),
                step3View.WithSameTop(step1View),
                step3View.WithSameBottom(_stepContainer),

                step4View.ToRightOf(step3View),
                step4View.WithSameWidth(this.View),
                step4View.WithSameTop(step1View)
            );

            #endregion

            this.ExtendedLayoutIncludesOpaqueBars = true;

            SetState();
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            _navigationController = NavigationController;

            _navigationController.ToolbarHidden = false;

        }

        protected override void InitializeNavigationBar(UINavigationBar navigationBar) {
            navigationBar.ApplyAppearance(backgroundColor: UIColor.Clear, translucent: true);
        }

        public override void ViewWillDisappear(bool animated) {
            // For some reason this method is called before the view
            // appears so we still need to check if the local nav controller
            // is null.
            if (_navigationController != null) {
                _navigationController.NavigationBar.Translucent = false;
                _navigationController.Toolbar.BarTintColor = Resources.Colors.Accent;
                _navigationController.ToolbarHidden = true;
            }

            base.ViewWillDisappear(animated);
        }

        private UIView CreateStep(string title) {
            var view = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var titleLabel = new MSLLabel {
                Text = title,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeLarge, FontWeight.Medium),
                ContentEdgeInsets = new UIEdgeInsets(20f, 15f, 0, 15f),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            view.Add(titleLabel);

            view.AddConstraints(
                titleLabel.AtLeftOf(view),
                titleLabel.AtRightOf(view),
                titleLabel.AtTopOf(view)
            );

            return view;
        }

        #region State

        private void SetState() {
            int step = this.ViewModel.Step;

            switch (step) {
                case 0:
                    SetToolbarItems(new[] {
                        _skipButton,
                        new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                        _nextButton
                    }, true);
                    break;
                case 1:
                    SetToolbarItems(new[] {
                        _previousButton,
                        new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                        _nextButton
                    }, true);
                    break;
                case 3:
                    SetToolbarItems(new[] {
                        _doneButton,
                        new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                        _addClassesButton
                    }, true);
                    break;
            }

            var color = GetColorForStep(step);

            var toolbar = NavigationController.Toolbar;

            UIView.Animate(0.6f, () => {
                _headerView.BackgroundColor = color;
                toolbar.BarTintColor = color;
            });

            // In iOS 15, UIKit has extended the usage of the scrollEdgeAppearance,
            // which by default produces a transparent background, to all navigation/tool bars Hence,
            // Have set for iOS 15 
            SetToolBarScrollEdgeAppearanceiOS15ApiAdoption(toolbar, color);

            // 1. Ensure all pending animations are complete
            this.View.LayoutIfNeeded();
            // 2. Change constraint
            _stepContainerConstraint.Constant = -this.View.Frame.Width * step;
            // 3. Yes this really works (and is how you're meant to do it)
            UIView.Animate(0.3f, 0f, UIViewAnimationOptions.CurveEaseOut, () => this.View.LayoutIfNeeded(), null);
        }

        private UIColor GetColorForStep(int step) {
            switch (step) {
                case 1:
                    return Resources.Colors.PalleteTeal;
                case 2:
                    return Resources.Colors.PalleteMidBlue;
                case 3:
                    return Resources.Colors.PalletePink;
            }

            return Resources.Colors.AccentDark;
        }

        private void SetToolBarScrollEdgeAppearanceiOS15ApiAdoption(UIToolbar toolBar, UIColor bgColor) {
            if (UIDevice.CurrentDevice.CheckSystemVersion(15, 0)) {
                toolBar.ScrollEdgeAppearance = toolBar.StandardAppearance;
                toolBar.ScrollEdgeAppearance.ConfigureWithDefaultBackground();
                toolBar.ScrollEdgeAppearance.BackgroundColor = bgColor;
            }
        }

        #endregion

        private void OnBusyChanged() {
            var isBusy = this.ViewModel.IsBusy;

            if (isBusy) {
                iOSDialogService.ShowBusyIndicator(R.Common.UpdatingWithEllipsis);
            }
            else {
                iOSDialogService.HideBusyIndicator();
            }
        }
    }
}