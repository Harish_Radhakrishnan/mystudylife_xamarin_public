using System;
using MvvmCross.Binding.BindingContext;
using CoreGraphics;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Converters;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.Data;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    [Foundation.Register("ExamsView")]
    internal class ExamsView : BaseEntitiesView<Exam, ExamsViewModel> {
        public override string Title => Resources.Strings.Exams;

        public override Glyph Glyph => Glyph.Exam;

        public override string FilterTitle => Resources.Strings.FilterExams;

        protected override string[] FilterOptions => new[] {
            Resources.Strings.Common.Current,
            Resources.Strings.Common.Past
        };

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var viewSource = new ExamsTableViewSource(TableView, typeof(ExamTableViewCell));

            TableView.Source = viewSource;
            TableView.ReloadData();

            var set = this.CreateBindingSet<ExamsView, ExamsViewModel>();

            set.Bind(this.FilterControl).For(x => x.SelectedSegment).To(x => x.Filter).WithConversion(new IntToNIntValueConverter()).OneWay();
            set.Bind(viewSource).To(x => x.GroupedViewSource.View);
            set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Apply();
        }

        protected override MslTableViewSource CreateViewSource(UITableView tableView) => null;

        protected override void FilterControlOnValueChanged(object sender, EventArgs e) {
            var selectedSegmentIndex = (int)((UISegmentedControl)sender).SelectedSegment;

            this.ViewModel?.SetFilterCommand.Execute(selectedSegmentIndex == 0 ? ExamFilterOption.Current : ExamFilterOption.Past);
        }
        
        class ExamsTableViewSource : MslGroupedTableViewSource<DateTime, Exam> {
            public ExamsTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) { }

            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
                => GetViewForHeader(tableView, section).Frame.Height;

            public override UIView GetViewForHeader(UITableView tableView, nint section) {
                var group = (ExamGrouping)GetGroupAt((int)section);

                const float hPadding = Resources.Dimens.DefaultHorizontalMargin;
                const float vPadding = 10f;

                UIColor textColor = Resources.Colors.SubtleText;

                switch (group.Kind) {
                    case ExamGroupingKind.Days3:
                        textColor = Resources.Colors.AttentionForeground;
                        break;
                    case ExamGroupingKind.Days7:
                        textColor = Resources.Colors.WarningForeground;
                        break;
                }

                var label = new UILabel(new CGRect(hPadding, vPadding, tableView.Bounds.Width - hPadding, 0)) {
                    Text = group.Name,
                    Font = Resources.Fonts.FontNormal,
                    TextColor = textColor
                };
                label.SizeToFit();

                var view = new UIView(new CGRect(0, 0, tableView.Bounds.Width, label.Frame.Height + (vPadding * 2))) {
                    BackgroundColor = Resources.Colors.Background
                };

                view.AddSubview(label);

                return view;
            }
        }
    }
}