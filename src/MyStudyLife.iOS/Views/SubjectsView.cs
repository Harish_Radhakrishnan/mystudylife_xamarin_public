using CoreGraphics;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI;
using BigTed;
using MvvmCross.Base;
using MyStudyLife.iOS.Platform;

namespace MyStudyLife.iOS.Views {
    [Register("SubjectsView")]
    internal sealed class SubjectsView : MslViewController<SubjectsViewModel> {
        private MslNavBarTitleView _titleView;
        private ProgressHUD _errorNotificationHud;

        public SubjectsView() {
            this.Title = Resources.Strings.Subjects;
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.ViewModel.EditErrorInteraction.Requested += EditErrorInteractionOnRequested;

            NavigationItem.TitleView = _titleView = new MslNavBarTitleView {
                Title = this.Title
            };

            _titleView.Tapped += (s, e) => {
                var bindings = this.CreateInlineBindingTarget<SubjectsViewModel>();
                
                var root = new AcademicScheduleRootElement()
                    .Bind(bindings, x => x.AcademicYears, x => x.AcademicYears)
                    .Bind(bindings, x => x.SelectedSchedule, x => x.SelectedSchedule);
                
                var dvc = new DialogViewController(root, true) {
                    Autorotate = true
                };

                this.ActivateController(dvc);
            };

            NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIBarButtonSystemItem.Add).WithCommand(this.ViewModel.NewCommand),
                true
            );

            #region Creation

            var tableView = new UITableView {
                AllowsSelection = true,
                SeparatorColor = Resources.Colors.Border
            };
            Add(tableView);

            var viewSource = MslTableViewSource.For<SubjectTableViewCell>(tableView);

            tableView.Source = viewSource;
            tableView.ReloadData();

            var lonelyView = new MslLonelyView(Glyph.Subject);
            Add(lonelyView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<SubjectsView, SubjectsViewModel>();

            set.Bind(_titleView).For(x => x.SubTitle).To(x => x.SelectedScheduleText);

            set.Bind(viewSource).To(x => x.ViewSource.View);
            set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.EditCommand);

            set.Bind(tableView).Visibility().To(x => x.ViewSource.Count);
            set.Bind(lonelyView).InvertedVisibility().To(x => x.ViewSource.Count);
            set.Bind(lonelyView).For(x => x.Text).To(x => x.LonelyText);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                tableView.WithSameLeft(View),
                tableView.WithSameRight(View),
                tableView.WithSameTop(View),
                tableView.WithSameBottom(View)
            );

            #endregion
        }

        private void EditErrorInteractionOnRequested(object sender, MvxValueEventArgs<string> e) {
            this._errorNotificationHud?.Dismiss();
            this._errorNotificationHud = iOSDialogService.ShowToastNotification(e.Value);
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            if (this.View != null) {
                this.View.Frame = rect;
            }
        }
    }
}