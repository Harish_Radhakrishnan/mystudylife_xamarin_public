using System;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI.ViewModels;
using CoreGraphics;
using MyStudyLife.iOS.Converters;
using MyStudyLife.UI.Data;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    [Register("TasksView")]
    internal class TasksView : BaseEntitiesView<Task, TasksViewModel> {
        public override string Title => Resources.Strings.Tasks;

        public override Glyph Glyph => Glyph.Task;

        public override string FilterTitle => Resources.Strings.FilterTasks;

        protected override string[] FilterOptions => new[] {
            Resources.Strings.Common.Current,
            Resources.Strings.Common.Past
        };

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var viewSource = new TasksTableViewSource(TableView, typeof (TaskTableViewCell));

            TableView.Source = viewSource;
            TableView.ReloadData();

            var set = this.CreateBindingSet<TasksView, TasksViewModel>();

            set.Bind(this.FilterControl).For(x => x.SelectedSegment).To(x => x.Filter).WithConversion(new IntToNIntValueConverter()).OneWay();
            set.Bind(viewSource).To(x => x.GroupedViewSource.View);
            set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Apply();
        }

        protected override MslTableViewSource CreateViewSource(UITableView tableView) => null;

        protected override void FilterControlOnValueChanged(object sender, EventArgs e) {
            var selectedSegmentIndex = (int)((UISegmentedControl)sender).SelectedSegment;

            this.ViewModel?.SetFilterCommand.Execute(selectedSegmentIndex == 0 ?  TaskFilterOption.Current : TaskFilterOption.Past);
        }

        class TasksTableViewSource : MslGroupedTableViewSource<int, Task> {
            private const string IncompleteIdentifier = "Incomplete" + nameof(TaskTableViewCell);
            private const string CompleteIdentifier = "Complete" + nameof(TaskTableViewCell);
            
            public TasksTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) {
                tableView.RegisterClassForCellReuse(cellType, new NSString(IncompleteIdentifier));
                tableView.RegisterClassForCellReuse(cellType, new NSString(CompleteIdentifier));
            }

            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
                => GetViewForHeader(tableView, section).Frame.Height;

            public override UIView GetViewForHeader(UITableView tableView, nint section) {
                var group = (TaskGrouping)GetGroupAt((int)section);

                const float hPadding = Resources.Dimens.DefaultHorizontalMargin;
                const float vPadding = 10f;

                UIColor textColor = Resources.Colors.SubtleText;

                switch (group.Kind) {
                    case TaskGroupingKind.Overdue:
                        textColor = Resources.Colors.AttentionForeground;
                        break;
                    case TaskGroupingKind.DueSoon:
                        textColor = Resources.Colors.WarningForeground;
                        break;
                }

                var label = new UILabel(new CGRect(hPadding, vPadding, tableView.Bounds.Width - hPadding, 0)) {
                    Text = group.Name,
                    Font = Resources.Fonts.FontNormal,
                    TextColor = textColor
                };
                label.SizeToFit();

                var view = new UIView(new CGRect(0, 0, tableView.Bounds.Width, label.Frame.Height + (vPadding * 2))) {
                    BackgroundColor = Resources.Colors.Background
                };

                view.AddSubview(label);

                return view;
            }

            protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item) {
                // iOS13 has issues applying strikethrough so we use a different identifier
                var task = item as Task;
                return tableView.DequeueReusableCell(task?.IsComplete ?? false ? CompleteIdentifier : IncompleteIdentifier);
            }
        }
    }
}