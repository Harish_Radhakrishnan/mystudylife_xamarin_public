using System;
using AuthenticationServices;
using CoreGraphics;
using MvvmCross;
using Cirrious.FluentLayouts.Touch;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Platform;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels;
using CoreText;
using BigTed;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MyStudyLife.Authorization;

namespace MyStudyLife.iOS.Views {
    [Register(nameof(SignInView))]
    [MvxRootPresentation]
    internal class SignInView : MslViewController<SignInViewModel>, IASAuthorizationControllerDelegate,
        IASAuthorizationControllerPresentationContextProviding {
        private const int BUTTON_HEIGHT = 48;

        private const string CONTENT_VIEW_HEIGHT_CONSTRAINT_IDENTIFIER = "$ContentView_Height$";
        private const string LOGIN_CONTAINER_TOP_GTE_CONSTRAINT_IDENTIFIER = "$LoginContainer_TopGTE$";

        private NSObject _keyboardHideSubscription, _keyboardShowSubscription, _keyboardFrameChangeSubscription;

        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        private ProgressHUD _signingInProgressHud;

        private UIScrollView _contentScrollView;
        private UIView _contentView;
        private UIButton _signInButton;
        private UIButton _emailSignInButton;
        private UIButton _signUpButton;
        private UIView _loginContainer;
        private UIView _signInOptionsView;
        private UIView _credentialsSignInView;
        private UIView _extrasView;
        private MDTextField _emailTextField;
        private MDTextField _passwordTextField;
        private UIButton _facebookButton;
        private UIButton _googleButton;
        private UIButton _office365Button;
        private MSLButton _backToLogin;

        private readonly nfloat LoginContainerMaxTopOffset = 180f;
        private nfloat DetermineLoginContainerMinTopOffset() => UIScreen.MainScreen.Bounds.Height * .3f;

        /// <summary>
        ///     A web flow result that has been set before the view model has.
        /// </summary>
        private OAuthWebFlowResult _pendingWebFlowResult;

        internal void SetOAuthWebFlowResult(OAuthWebFlowResult result) {
            if (ViewModel == null) {
                _pendingWebFlowResult = result;
            }
            else {
                ViewModel.HandleOAuthWebFlowResult(result);
            }
        }

        public override void ViewWillAppear(bool animated) {
            NavigationController?.SetNavigationBarHidden(true, animated);

            base.ViewWillAppear(animated);

            View.BackgroundColor = Resources.Colors.Accent;

            _keyboardHideSubscription =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, KeyboardWillHide);
            _keyboardShowSubscription =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, KeyboardWillShow);
            _keyboardFrameChangeSubscription =
                NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillChangeFrameNotification,
                    KeyboardWillShow);

            OnShowEmailSignInChanged(false);
        }

        public override void ViewWillDisappear(bool animated) {
            base.ViewWillDisappear(animated);

            if (_keyboardHideSubscription != null) {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardHideSubscription);
            }

            if (_keyboardShowSubscription != null) {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardShowSubscription);
            }

            if (_keyboardFrameChangeSubscription != null) {
                NSNotificationCenter.DefaultCenter.RemoveObserver(_keyboardFrameChangeSubscription);
            }

            _signingInProgressHud?.Dismiss();
            _signingInProgressHud = null;
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var pendingWebFlowResult = _pendingWebFlowResult;
            if (pendingWebFlowResult != null) {
                _pendingWebFlowResult = null;
                ViewModel.HandleOAuthWebFlowResult(pendingWebFlowResult);
            }

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(nameof(ViewModel.SigningIn), OnSigningInChanged)
                .Listen(nameof(ViewModel.ErrorMessage), OnErrorMessageChanged);

            View.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                _emailTextField?.ResignFirstResponder();
                _passwordTextField?.ResignFirstResponder();
            }) {
                CancelsTouchesInView = false
            });

            #region Creation

            var contentScrollView = _contentScrollView = new UIScrollView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Add(contentScrollView);

            var contentView = _contentView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
            };
            contentScrollView.Add(contentView);

            var logoImageView = new UIImageView {
                Image = UIImage.FromBundle("SignInLogo.png"),
                ContentMode = UIViewContentMode.ScaleAspectFit,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            contentView.Add(logoImageView);

            var errorMessageLabel = new MSLLabel {
                TextAlignment = UITextAlignment.Center,
                TextColor = UIColor.White,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            contentView.Add(errorMessageLabel);

            _loginContainer = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            contentView.Add(_loginContainer);

            _signInOptionsView = CreateSignInOptionsView();
            _loginContainer.Add(_signInOptionsView);

            _credentialsSignInView = CreateCredentialsSignInView();
            _loginContainer.Add(_credentialsSignInView);

            _loginContainer.AddConstraints(
                _signInOptionsView.Width().EqualTo().WidthOf(_loginContainer).WithMultiplier(.5f)
                    .Minus(Resources.Dimens.ScreenMargin.Left + Resources.Dimens.ScreenMargin.Right),
                _signInOptionsView.AtLeftOf(_loginContainer, Resources.Dimens.ScreenMargin.Left),
                _signInOptionsView.WithSameCenterY(_loginContainer),
                _signInOptionsView.WithSameOrGreaterTop(_loginContainer),
                _signInOptionsView.WithSameOrLesserBottom(_loginContainer),
                _credentialsSignInView.ToRightOf(_signInOptionsView,
                    Resources.Dimens.ScreenMargin.Right + Resources.Dimens.ScreenMargin.Left),
                _credentialsSignInView.WithSameTop(_signInOptionsView),
                _credentialsSignInView.WithSameWidth(_signInOptionsView),
                _credentialsSignInView.WithSameOrGreaterTop(_loginContainer),
                _credentialsSignInView.WithSameOrLesserBottom(_loginContainer)
            );

            _extrasView = CreateExtrasView();
            contentView.Add(_extrasView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<SignInView, SignInViewModel>();

            // Only show offline errors, we're showing dialogs for the rest.
            set.Bind(errorMessageLabel).For(x => x.Text).To(x => x.ErrorMessage);
            set.Bind(errorMessageLabel).InvertedVisibility().To(x => x.IsOnline);

            // Yes, we do need to bind to Enabled as well as the command...
            set.Bind(_emailTextField).To(x => x.MslEmail);
            set.Bind(_passwordTextField).To(x => x.MslPassword);
            set.Bind(_signInButton).To(x => x.MslSignInCommand);
            set.Bind(_signInButton).For(x => x.Enabled).To(x => x.SignInCommandCanExecute);

            set.Bind(_office365Button).To(x => x.Office365SignInCommand);
            set.Bind(_office365Button).For(x => x.Enabled).To(x => x.SignInCommandCanExecute);
            set.Bind(_facebookButton).To(x => x.FacebookSignInCommand);
            set.Bind(_facebookButton).For(x => x.Enabled).To(x => x.SignInCommandCanExecute);
            set.Bind(_googleButton).To(x => x.GoogleSignInCommand);
            set.Bind(_googleButton).For(x => x.Enabled).To(x => x.SignInCommandCanExecute);

            set.Bind(_emailSignInButton).To(x => x.ToggleEmailSignInCommand);
            set.Bind(_emailSignInButton).For(x => x.Enabled).To(x => x.SignInCommandCanExecute);
            set.Bind(_signUpButton).To(x => x.SignUpCommand);
            set.Bind(_backToLogin).To(x => x.ToggleEmailSignInCommand);

            set.Apply();

            OnIsOnlineChange();

            ViewModelListener
                .Listen(nameof(ViewModel.SigningIn), OnSigningInChanged)
                .Listen(nameof(ViewModel.ErrorMessage), OnErrorMessageChanged)
                .Listen(nameof(ViewModel.ShowEmailSignIn), OnShowEmailSignInChanged)
                .Listen(nameof(ViewModel.IsOnline), OnIsOnlineChange);

            #endregion

            #region Layout

            contentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            var safeGuide = View.SafeAreaLayoutGuide;

            contentScrollView.TopAnchor.ConstraintEqualTo(safeGuide.TopAnchor).Active = true;
            contentScrollView.BottomAnchor.ConstraintEqualTo(safeGuide.BottomAnchor).Active = true;

            var contentViewWidthAnchor = contentView.WidthAnchor.ConstraintEqualTo(safeGuide.WidthAnchor);
            contentViewWidthAnchor.Priority = 1000;
            contentViewWidthAnchor.Active = true;

            var contentViewHeightAnchor = contentView.HeightAnchor.ConstraintEqualTo(safeGuide.HeightAnchor);
            contentViewHeightAnchor.Priority = 250;
            contentViewHeightAnchor.Active = true;

            View.AddConstraints(
                contentScrollView.FullWidthOf(View),
                contentView.Fill(contentScrollView),
                logoImageView.WithSameCenterX(contentView),
                logoImageView.WithSameOrGreaterTop(contentView, Resources.Dimens.ScreenMargin.Top).SetPriority(1000),
                logoImageView.Bottom().LessThanOrEqualTo().TopOf(_signInOptionsView).Minus(Resources.Dimens.DefaultMargin).SetPriority(1000),
                logoImageView.CenterY().EqualTo().CenterYOf(contentView).WithMultiplier(.4f).SetPriority(750),
                errorMessageLabel.FullWidthOf(contentView, Resources.Dimens.DefaultHorizontalMargin),
                errorMessageLabel.Below(logoImageView, 15f),
                errorMessageLabel.Bottom().LessThanOrEqualTo().TopOf(_signInOptionsView).Minus(15f),
                errorMessageLabel.Bottom().LessThanOrEqualTo().TopOf(_credentialsSignInView).Minus(15f),
                _loginContainer.AtLeftOf(contentView),
                _loginContainer.Width().EqualTo().WidthOf(contentView).WithMultiplier(2),
                _extrasView.FullWidthOf(contentView),
                _extrasView.Top().GreaterThanOrEqualTo().BottomOf(_loginContainer).Plus(15f),
                _extrasView.AtBottomOf(contentView)
            );

            #endregion


            OnSigningInChanged();
            OnErrorMessageChanged();
        }

        /// <summary>
        ///     Creates the main sign in view, with sign in/up options.
        /// </summary>
        private UIView CreateSignInOptionsView() {
            var container = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
            };

            var oAuthSignInView = new UIStackView {
                Axis = UILayoutConstraintAxis.Vertical,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            container.Add(oAuthSignInView);

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0)) {
                var appleIdButton = CreateOAuthButton(
                    Resources.Strings.SignInView.SignInWithApple,
                    "SignInApple.png",
                    UIColor.White,
                    UIColor.FromRGB(220, 220, 220),
                    UIColor.Black,
                    UIColor.FromWhiteAlpha(0, 0.2f)
                );
                appleIdButton.TouchUpInside += AppleIdButtonOnTouchUpInside;
                oAuthSignInView.AddArrangedSubview(appleIdButton);
                oAuthSignInView.SetCustomSpacing(8f, appleIdButton);
            }

            _facebookButton = CreateOAuthButton(
                Resources.Strings.SignInView.SignInWithFacebook,
                "SignInFacebook.png",
                Resources.Colors.Facebook,
                Resources.Colors.FacebookMid
            );
            oAuthSignInView.AddArrangedSubview(_facebookButton);
            oAuthSignInView.SetCustomSpacing(8f, _facebookButton);

            _googleButton = CreateOAuthButton(
                Resources.Strings.SignInView.SignInWithGoogle,
                "SignInGoogle.png",
                Resources.Colors.Google,
                Resources.Colors.GoogleMid
            );
            oAuthSignInView.AddArrangedSubview(_googleButton);
            oAuthSignInView.SetCustomSpacing(8f, _googleButton);

            _office365Button = CreateOAuthButton(
                Resources.Strings.SignInView.SignInWithOffice365,
                "SignInOffice365.png",
                Resources.Colors.Office365,
                Resources.Colors.Office365Mid
            );
            oAuthSignInView.AddArrangedSubview(_office365Button);

            var orLine = new UIView {
                BackgroundColor = Resources.Colors.AccentLight,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            var orText = new MSLLabel {
                Text = Resources.Strings.SignInView.Or.ToLower(),
                TextColor = Resources.Colors.AccentLight,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                BackgroundColor = Resources.Colors.Accent,
                // 4 "pixels" on bottom to line orText up with orLine
                ContentEdgeInsets = new UIEdgeInsets(0, 10, 4, 10)
            };
            var orDivider = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
            };
            orDivider.Add(orLine);
            orDivider.Add(orText);
            oAuthSignInView.AddArrangedSubview(orDivider);

            var signInButtons = new UIView();
            oAuthSignInView.AddArrangedSubview(signInButtons);
            oAuthSignInView.SetCustomSpacing(15f, signInButtons);

            var emailSignInButton = _emailSignInButton = new MSLButton {
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = Resources.Colors.AccentMid,
                HighlightedBackgroundColor = Resources.Colors.AccentLight,
                Layer = {
                    CornerRadius = 3
                },
                TitleLabel = {
                    TextColor = UIColor.White
                }
            };
            emailSignInButton.SetTitle(Resources.Strings.SignInView.SignInWithEmail, UIControlState.Normal);
            signInButtons.AddSubview(emailSignInButton);

            _signUpButton = new MSLButton {
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = Resources.Colors.Accent,
                HighlightedBackgroundColor = Resources.Colors.AccentLight,
                Layer = {
                    BorderColor = Resources.Colors.AccentMid.CGColor,
                    BorderWidth = 2,
                    CornerRadius = 3
                },
                TitleLabel = {
                    TextColor = UIColor.White
                }
            };
            _signUpButton.SetTitle(Resources.Strings.SignInView.SignUp, UIControlState.Normal);
            signInButtons.AddSubview(_signUpButton);

            var legalLabel = new MslAttributedLabel {
                Font = Resources.Fonts.OfSize(FontSize.Caption),
                TextColor = UIColor.FromWhiteAlpha(1f, .6f),
                TextAlignment = UITextAlignment.Center,
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                LinkAttributes = new CTStringAttributes {
                    ForegroundColor = UIColor.FromWhiteAlpha(1f, .6f).CGColor,
                    UnderlineStyle = CTUnderlineStyle.Single
                }.Dictionary,
                ActiveLinkAttributes = new CTStringAttributes {
                    ForegroundColor = UIColor.FromWhiteAlpha(1f, .3f).CGColor,
                    UnderlineStyle = CTUnderlineStyle.Single
                }.Dictionary,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            oAuthSignInView.AddArrangedSubview(legalLabel);

            string text = legalLabel.Text = String.Format(
                Resources.Strings.SignInView.LegalTextFormat,
                Resources.Strings.SignInView.LegalText_PrivacyPolicy,
                Resources.Strings.SignInView.LegalText_TermsOfService
            );

            legalLabel.AddLinkToURL(
                new NSUrl("PRIVACY"),
                new NSRange(
                    text.IndexOf(Resources.Strings.SignInView.LegalText_PrivacyPolicy),
                    Resources.Strings.SignInView.LegalText_PrivacyPolicy.Length
                )
            );
            legalLabel.AddLinkToURL(
                new NSUrl("TOS"),
                new NSRange(
                    text.IndexOf(Resources.Strings.SignInView.LegalText_TermsOfService),
                    Resources.Strings.SignInView.LegalText_TermsOfService.Length
                )
            );

            legalLabel.SelectedLinkWithUrl += (s, e) => {
                switch (e.Url.ToString()) {
                    case "PRIVACY":
                        e.Handled = true;
                        ViewModel.PrivacyPolicyCommand.Execute(null);
                        break;
                    case "TOS":
                        e.Handled = true;
                        ViewModel.TermsOfServiceCommand.Execute(null);
                        break;
                }
            };

            container.AddConstraints(
                oAuthSignInView.Fill(container),
                orDivider.Height().EqualTo(48f),
                orLine.WithSameWidth(orDivider),
                orLine.AtLeftOf(orDivider),
                orLine.Height().EqualTo(1f),
                orLine.WithSameCenterY(orDivider),
                orText.WithSameCenterX(orLine),
                orText.WithSameCenterY(orLine),
                signInButtons.Height().EqualTo(BUTTON_HEIGHT),
                emailSignInButton.FullHeightOf(signInButtons),
                emailSignInButton.AtLeftOf(signInButtons),
                emailSignInButton.WithRelativeWidth(container, 0.6f).Minus(Resources.Dimens.DefaultMargin),
                _signUpButton.FullHeightOf(signInButtons),
                _signUpButton.AtRightOf(signInButtons),
                _signUpButton.WithRelativeWidth(container, 0.4f)
            );

            return container;
        }

        private UIView CreateExtrasView() {
            var extrasView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var findOutMoreLabel = new MslAttributedLabel {
                Font = Resources.Fonts.OfSize(FontSize.Body),
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                LinkAttributes = new CTStringAttributes {
                    ForegroundColor = UIColor.White.CGColor,
                    UnderlineStyle = CTUnderlineStyle.Single
                }.Dictionary,
                ActiveLinkAttributes = new CTStringAttributes {
                    ForegroundColor = UIColor.FromWhiteAlpha(1f, .6f).CGColor,
                    UnderlineStyle = CTUnderlineStyle.Single
                }.Dictionary,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            extrasView.Add(findOutMoreLabel);

            string text = findOutMoreLabel.Text = String.Format(
                Resources.Strings.SignInView.FindOutMoreFormat,
                Resources.Strings.SignInView.FindOutMoreLink
            );

            findOutMoreLabel.AddLinkToURL(
                new NSUrl("WWW"),
                new NSRange(
                    text.IndexOf(Resources.Strings.SignInView.FindOutMoreLink),
                    Resources.Strings.SignInView.FindOutMoreLink.Length
                )
            );
            findOutMoreLabel.SelectedLinkWithUrl += (s, e) => {
                ViewModel.FindOutMoreCommand.Execute(null);
                e.Handled = true;
            };

            extrasView.AddConstraints(
                findOutMoreLabel.WithSameCenterX(extrasView),
                findOutMoreLabel.WithSameOrGreaterTop(extrasView),
                findOutMoreLabel.AtBottomOf(extrasView, 20)
            );

            return extrasView;
        }

        private UIView CreateCredentialsSignInView() {
            var view = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            UIColor textFieldNormalColor = Resources.Colors.AccentLight,
                textFieldFocusColor = UIColor.White;

            _emailTextField = new MDTextField {
                KeyboardType = UIKeyboardType.EmailAddress,
                ReturnKeyType = UIReturnKeyType.Next,
                Label = Resources.Strings.SignInView.Email,
                TextColor = UIColor.White,
                AutocapitalizationType = UITextAutocapitalizationType.None,
                AutocorrectionType = UITextAutocorrectionType.No,
                EnablesReturnKeyAutomatically = true,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _emailTextField.SetLabelAndUnderlineColor(UIControlState.Normal, textFieldNormalColor);
            _emailTextField.SetLabelAndUnderlineColor(UIControlState.Focused, textFieldFocusColor);
            _emailTextField.ShouldReturn += _ => {
                _passwordTextField.BecomeFirstResponder();
                return true;
            };
            view.Add(_emailTextField);
            view.AddConstraints(
                _emailTextField.AtTopOf(view),
                _emailTextField.FullWidthOf(view)
            );

            _passwordTextField = new MDTextField {
                SecureTextEntry = true,
                ReturnKeyType = UIReturnKeyType.Go,
                Label = Resources.Strings.SignInView.Password,
                TextColor = UIColor.White,
                AutocapitalizationType = UITextAutocapitalizationType.None,
                AutocorrectionType = UITextAutocorrectionType.No,
                EnablesReturnKeyAutomatically = true,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _passwordTextField.SetLabelAndUnderlineColor(UIControlState.Normal, textFieldNormalColor);
            _passwordTextField.SetLabelAndUnderlineColor(UIControlState.Focused, textFieldFocusColor);
            _passwordTextField.ShouldReturn += _ => {
                _passwordTextField.ResignFirstResponder();
                ViewModel.MslSignInCommand.Execute(null);
                return true;
            };
            view.Add(_passwordTextField);
            view.AddConstraints(
                _passwordTextField.Below(_emailTextField, 15f),
                _passwordTextField.FullWidthOf(view)
            );

            var signInButton = _signInButton = new MSLButton {
                BackgroundColor = Resources.Colors.AccentMid,
                HighlightedBackgroundColor = Resources.Colors.AccentLight,
                TitleLabel = {
                    TextAlignment = UITextAlignment.Center
                },
                Layer = {
                    CornerRadius = 3
                },
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            signInButton.SetTitle(Resources.Strings.SignInView.SignIn, UIControlState.Normal);
            signInButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            view.Add(signInButton);
            view.AddConstraints(
                signInButton.AtRightOf(view),
                signInButton.Below(_passwordTextField, 15f),
                signInButton.Height().EqualTo(BUTTON_HEIGHT),
                signInButton.WithRelativeWidth(_passwordTextField, 0.5f).Minus(5)
            );

            var forgottenPassword = new UILabel {
                Font = Resources.Fonts.FontNormal,
                TextAlignment = UITextAlignment.Right,
                TextColor = UIColor.White,
                AttributedText = new NSAttributedString(Resources.Strings.SignInView.ForgottenPassword,
                    new CTStringAttributes {
                        UnderlineStyle = CTUnderlineStyle.Single
                    }),
                TranslatesAutoresizingMaskIntoConstraints = false,
                UserInteractionEnabled = true
            };
            forgottenPassword.AddGestureRecognizer(new UITapGestureRecognizer(() =>
                ViewModel.ForgottenPasswordCommand.Execute(null)));
            view.Add(forgottenPassword);
            view.AddConstraints(
                forgottenPassword.Below(signInButton, Resources.Dimens.DefaultMargin),
                forgottenPassword.WithSameRight(signInButton),
                forgottenPassword.AtBottomOf(view)
            );

            var backToLoginButton = _backToLogin = new MSLButton {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = UIColor.Clear,
                HighlightedBackgroundColor = UIColor.Clear,
                HighlightedAlpha = 0.6f
            };

            var backToLoginLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Text = Resources.Strings.Back,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                TextColor = UIColor.White
            };
            var backToLoginIcon = new MSLIcon(Glyph.Left) {
                TextColor = Resources.Colors.Accent,
                Scale = .875f,
                TextAlignment = UITextAlignment.Center,
                TranslatesAutoresizingMaskIntoConstraints = false,
                Layer = {
                    BackgroundColor = UIColor.White.CGColor,
                    CornerRadius = 7,
                    MasksToBounds = true
                }
            };
            backToLoginButton.Add(backToLoginLabel);
            backToLoginButton.Add(backToLoginIcon);

            view.Add(backToLoginButton);

            // Adds a larger touch area
            const float padding = 10f;

            view.AddConstraints(
                backToLoginButton.WithSameCenterY(signInButton),
                backToLoginButton.AtLeftOf(view, -padding),
                backToLoginIcon.WithSameCenterY(backToLoginButton),
                backToLoginIcon.AtLeftOf(backToLoginButton, padding),
                backToLoginIcon.Width().EqualTo(14),
                backToLoginIcon.Height().EqualTo(14),
                backToLoginIcon.Top().GreaterThanOrEqualTo().TopOf(backToLoginButton).Plus(padding),
                backToLoginIcon.Bottom().LessThanOrEqualTo().BottomOf(backToLoginButton).Minus(padding),
                backToLoginLabel.WithSameCenterY(backToLoginButton),
                backToLoginLabel.ToRightOf(backToLoginIcon, 6),
                backToLoginLabel.AtRightOf(backToLoginButton, padding),
                backToLoginLabel.Top().GreaterThanOrEqualTo().TopOf(backToLoginButton).Plus(padding),
                backToLoginLabel.Bottom().LessThanOrEqualTo().BottomOf(backToLoginButton).Minus(padding)
            );

            return view;
        }

        private void OnIsOnlineChange() {
            var isOnline = ViewModel.IsOnline;

            var alpha = isOnline ? 1f : 0.85f;

            _facebookButton.Alpha = alpha;
            _googleButton.Alpha = alpha;
            _office365Button.Alpha = alpha;
            _signInButton.Alpha = alpha;
        }

        private void OnSigningInChanged() {
            // (this also handles credentials sign in)
            var isBusy = (ViewModel?.SigningIn ?? false);

            if (isBusy) {
                if (_signingInProgressHud == null) {
                    _signingInProgressHud = iOSDialogService.ShowBlockingBusyIndicator(R.Common.SigningInWithEllipsis);
                }
            }
            else {
                _signingInProgressHud?.Dismiss();
                _signingInProgressHud = null;
            }
        }

        private void OnShowEmailSignInChanged() => OnShowEmailSignInChanged(true);

        private void OnShowEmailSignInChanged(bool animate) {
            var showEmailSignIn = ViewModel?.ShowEmailSignIn ?? false;
            var offsetX = showEmailSignIn ? -_contentView.Bounds.Width : 0;
            var alpha = showEmailSignIn ? 0 : 1;

            if (!showEmailSignIn) {
                _emailTextField.EndEditing(true);
                _passwordTextField.EndEditing(true);
            }
            else {
                _emailTextField.BecomeFirstResponder();
            }

            void ApplyChanges() {
                _loginContainer.Transform = CGAffineTransform.MakeTranslation(offsetX, 0);

                _extrasView.Alpha = alpha;
            }

            if (animate) {
                UIView.Animate(0.3f, 0, UIViewAnimationOptions.CurveEaseInOut, ApplyChanges, null);
            }
            else {
                ApplyChanges();
            }
        }

        private void OnErrorMessageChanged() {
            if ((ViewModel?.IsOnline ?? true) && !String.IsNullOrEmpty(ViewModel.ErrorMessage)) {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(
                    "Sign In Error",
                    ViewModel.ErrorMessage
                );
            }
        }

        private void KeyboardWillHide(NSNotification notification) {
            _keyboardVisible = false;

            if (_contentView == null) {
                return;
            }

            UIView.Animate(.3f, () => _contentScrollView.Transform = CGAffineTransform.MakeTranslation(0, 0));
        }

        private bool _keyboardVisible;
        private bool _viewAnimating;

        private void KeyboardWillShow(NSNotification notification) {
            bool isShowNotification = notification.Name == UIKeyboard.WillShowNotification;

            // We only want to respond to a change notification if the keyboard is already shown.
            // Change notifications are fired for show as well as size changing
            if (isShowNotification) {
                _keyboardVisible = true;
            }
            else if (!_keyboardVisible) {
                return;
            }

            if (_contentView == null || _viewAnimating) {
                return;
            }

            var beginFrame = (NSValue) notification.UserInfo.ObjectForKey(UIKeyboard.FrameBeginUserInfoKey);
            var endFrame = (NSValue) notification.UserInfo.ObjectForKey(UIKeyboard.FrameEndUserInfoKey);
            var screenHeight = UIScreen.MainScreen.Bounds.Height;

            if (beginFrame.CGRectValue == endFrame.CGRectValue || endFrame.CGRectValue.Y == screenHeight) {
                return;
            }

            _viewAnimating = true;

            var keyboardBounds = (NSValue) notification.UserInfo.ObjectForKey(UIKeyboard.BoundsUserInfoKey);
            var keyboardSize = keyboardBounds.CGRectValue;

            var keyboardHeight = keyboardSize.Height;

            var visibleHeight = screenHeight - keyboardHeight;
            var offsetInView = _credentialsSignInView.Superview
                .ConvertPointToView(_credentialsSignInView.Frame.Location, null).Y;
            offsetInView += _credentialsSignInView.Frame.Height + Resources.Dimens.DefaultHorizontalMargin;

            var targetOffset = NMath.Min(visibleHeight - offsetInView, 0);

            UIView.Animate(.3f, () => _contentScrollView.Transform = CGAffineTransform.MakeTranslation(0, targetOffset),
                () => _viewAnimating = false);
        }

        private UIButton CreateOAuthButton(string text, string image, UIColor color, UIColor highlightedColor,
            UIColor textColor = null, UIColor separatorColor = null) {
            textColor ??= UIColor.White;
            separatorColor ??= UIColor.FromWhiteAlpha(1, 0.2f);

            var button = new MSLButton {
                BackgroundColor = color,
                Layer = {
                    CornerRadius = 3
                },
                TranslatesAutoresizingMaskIntoConstraints = false,
                HighlightedBackgroundColor = highlightedColor
            };
            var icon = new UIImageView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Image = UIImage.FromBundle(image),
                ContentMode = UIViewContentMode.ScaleAspectFit
            };
            button.Add(icon);
            var separator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = separatorColor
            };
            button.Add(separator);
            var label = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Text = text,
                TextColor = textColor,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal)
            };
            button.Add(label);

            button.AddConstraints(
                button.Height().EqualTo(BUTTON_HEIGHT),
                icon.WithSameCenterY(button),
                icon.AtLeftOf(button, 18),
                icon.Width().EqualTo(24),
                icon.Height().EqualTo(24),
                separator.ToRightOf(icon, 18),
                separator.WithSameCenterY(button),
                separator.Width().EqualTo(1),
                separator.AtTopOf(button, 12),
                separator.AtBottomOf(button, 12),
                label.ToRightOf(separator, 18),
                label.WithSameCenterY(button)
            );

            return button;
        }

        #region AppleId

        private void AppleIdButtonOnTouchUpInside(object sender, EventArgs e) {
            var appleIdProvider = new ASAuthorizationAppleIdProvider();
            var request = appleIdProvider.CreateRequest();
            request.RequestedScopes = new[] {ASAuthorizationScope.Email, ASAuthorizationScope.FullName};

            var authorizationController = new ASAuthorizationController(new ASAuthorizationRequest[] {request});
            authorizationController.Delegate = this;
            authorizationController.PresentationContextProvider = this;
            authorizationController.PerformRequests();
        }

        [Export("authorizationController:didCompleteWithAuthorization:")]
        public void DidComplete(ASAuthorizationController controller, ASAuthorization authorization) {
            var appleIdCredential = authorization.GetCredential<ASAuthorizationAppleIdCredential>();

            if (appleIdCredential != null) {
                ViewModel.HandleAppleIdResult(
                    appleIdCredential.IdentityToken.ToString(),
                    appleIdCredential.FullName.GivenName,
                    appleIdCredential.FullName.FamilyName
                );
            }
        }

        [Export("authorizationController:didCompleteWithError:")]
        public void DidComplete(ASAuthorizationController controller, NSError error) {
            if (error.Code != 1001) {
                ViewModel.ErrorMessage = error.LocalizedDescription;
            }
        }

        public UIWindow GetPresentationAnchor(ASAuthorizationController controller) => View.Window;

        #endregion
    }
}