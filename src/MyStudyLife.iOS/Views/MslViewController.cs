using System;
using CoreGraphics;
using MyStudyLife.UI.ViewModels.Base;
using UIKit;
using Foundation;
using MyStudyLife.UI.Annotations;
using System.Collections.Generic;
using MvvmCross.ViewModels;
using MvvmCross.Platforms.Ios.Views;

namespace MyStudyLife.iOS.Views {
    public abstract class MslViewController<TViewModel> : MvxViewController<TViewModel> where TViewModel : BaseViewModel {
        [UsedImplicitly] private MvxPropertyChangedListener _vmlistener;

        private readonly List<Action> _onLoadedActions = new List<Action>();

        protected MvxPropertyChangedListener ViewModelListener {
            get {
                if (_vmlistener == null) {
                    throw new Exception($"Cannot get {nameof(ViewModelListener)} before {nameof(ViewModel)} is set. Did you call this before base.{nameof(ViewDidLoad)}?");
                }

                return _vmlistener;
            }
        }

        public MslViewController() { }
        public MslViewController(IntPtr handle) { }
        protected MslViewController(string nibName, NSBundle bundle) { }

        public override UIStatusBarStyle PreferredStatusBarStyle() => UIStatusBarStyle.LightContent;

        protected virtual void InitializeNavigationBar(UINavigationBar navigationBar) {
            navigationBar.ApplyAppearance();
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            // ReSharper disable once ConvertClosureToMethodGroup
            _vmlistener = new MvxPropertyChangedListener(this.ViewModel);

            if (this.ViewModel.IsLoaded) {
                OnViewModelLoadedInternal();
            }
            else {
                _vmlistener.Listen(nameof(this.ViewModel.IsLoaded), () => {
                     if (this.ViewModel.IsLoaded) {
                         OnViewModelLoadedInternal();
                     }
                });
            }
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            if (NavigationController != null) {
                InitializeNavigationBar(NavigationController.NavigationBar);
            }

            this.View.BackgroundColor = Resources.Colors.Background;

            SizeRootView();
        }

        public override bool ShouldAutorotate() {
            return false;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations() 
            => UIInterfaceOrientationMask.Portrait;
        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
            => UIInterfaceOrientation.Portrait;

        public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration) {
            base.WillAnimateRotation(toInterfaceOrientation, duration);

            SizeRootView();
        }

        protected void SizeRootView() {
            SizeRootViewImpl(DetermineRootViewSize());
        }

        protected virtual CGRect DetermineRootViewSize() {
            var width = UIScreen.MainScreen.Bounds.Width;
            var height = UIScreen.MainScreen.Bounds.Height;
            
            var navBar = this.NavigationController.NavigationBar;

            // If it's hidden it gets animated off the screen, NMath.Max as in landscape right, Y is double negative of height
            height -= NMath.Max((navBar.Frame.Height + navBar.Frame.Y), 0);

            if (!this.NavigationController.ToolbarHidden) {
                height -= this.NavigationController.Toolbar.Frame.Height;
            }

            return new CGRect(0, 0, width, height);
        }

        /// <summary>
        ///     Helper method to set the size of the "root" view,
        ///     ie. the view directly below <c>this.View</c>. This
        ///     method is called whenever the view needs to be sized
        ///     such as during <c>ViewDidAppear</c> and <c>WillAnimateRotation</c>.
        /// </summary>
        /// <param name="rect">The size to use.</param>
        protected virtual void SizeRootViewImpl(CGRect rect) { }

        private void OnViewModelLoadedInternal() {
            foreach (var a in _onLoadedActions) {
                a();
            }

            _onLoadedActions.Clear();
        }

        protected void OnViewModelLoaded(Action action) {
            if (this.ViewModel != null && this.ViewModel.IsLoaded) {
                action();
            }
            else {
                _onLoadedActions.Add(action);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _vmlistener?.Dispose();
                _vmlistener = null;
            }

            base.Dispose(disposing);
        }
    }
}