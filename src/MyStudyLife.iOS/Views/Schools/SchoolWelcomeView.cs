﻿using MvvmCross.Binding.BindingContext;
using Foundation;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.ViewModels.Schools;
using UIKit;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using OAStackView;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views.Schools {
    [Register(nameof(SchoolWelcomeView))]
    [MvxRootPresentation]
    internal sealed class SchoolWelcomeView : MslViewController<SchoolWelcomeViewModel> {
        /// <summary>
        ///     We're storing a local copy else it's null when
        ///     ViewWillDisappear (so we can't reset styles).
        /// </summary>
        private UINavigationController _navigationController;

        private UIBarButtonItem _getStartedButton;

        private UIView _headerView;
        private MSLLabel _headerLabel;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.View.BackgroundColor = UIColor.White;

            #region Creation

            SetToolbarItems(new []{
                new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                _getStartedButton = new UIBarButtonItem(Resources.Strings.GetStarted, UIBarButtonItemStyle.Done, null)
            }, true);

            _headerView = new UIView {
                BackgroundColor = Resources.Colors.Accent
            };
            Add(_headerView);

            _headerLabel = new MSLLabel {
                Text = Resources.Strings.WelcomeView.Hello_there,
                TextColor = UIColor.White,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeHuge, FontWeight.Bold),
                ContentEdgeInsets = new UIEdgeInsets(0, 15, 15, 15)
            };
            _headerView.Add(_headerLabel);

            var scrollView = new UIScrollView();
            Add(scrollView);

            var contentView = new OAStackView.OAStackView {
                Alignment = OAStackViewAlignment.Top,
                Axis = UILayoutConstraintAxis.Vertical,
                Distribution = OAStackViewDistribution.Fill,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            scrollView.Add(contentView);

            contentView.AddArrangedSubview(new UILabel {
                Text = "My Study Life is the new home of your timetable, homework and exams. It will help you stay organised and keep track of your school life. Here's how to find your way around.",
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                TranslatesAutoresizingMaskIntoConstraints = false
            });
            contentView.AddArrangedSubview(CreateSub(
                Glyph.Dashboard,
                Resources.Strings.Dashboard,
                "The dashboard shows you any classes or exams you have for the current day as well as any tasks (homework, revision or reminders) that are due soon or overdue."
            ));
            contentView.AddArrangedSubview(CreateSub(
                Glyph.Calendar,
                Resources.Strings.Calendar,
                "On the calendar you'll find all of your classes as well as any exams you have added in friendly week and month views."
            ));
            contentView.AddArrangedSubview(CreateSub(
                Glyph.Task,
                Resources.Strings.Tasks,
                "The tasks screen lets you keep track of your homework, revision and reminders in a simple chronological list view."
            ));
            contentView.AddArrangedSubview(CreateSub(
                Glyph.Exam,
                Resources.Strings.Exams,
                "On the exams screen, you can view and add the details of any exams you have."
            ));
            contentView.AddArrangedSubview(CreateSub(
                Glyph.Schedule,
                Resources.Strings.Schedule,
                "The schedule screen lists your classes, term dates and the dates of your holidays."
            ));

            #endregion

            #region Binding

            var set = this.CreateBindingSet<SchoolWelcomeView, SchoolWelcomeViewModel>();

            set.Bind(_getStartedButton).To(x => x.GetStartedCommand);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                _headerView.AtLeftOf(this.View),
                _headerView.AtRightOf(this.View),
                _headerView.AtTopOf(this.View),
                _headerView.Height().EqualTo(143f),

                _headerLabel.AtLeftOf(_headerView),
                _headerLabel.AtRightOf(_headerView),
                _headerLabel.AtBottomOf(_headerView),

                scrollView.Below(_headerView),
                scrollView.AtLeftOf(this.View),
                scrollView.AtRightOf(this.View),
                scrollView.AtBottomOf(this.View, this.NavigationController.Toolbar.Frame.Height),

                contentView.AtLeftOf(this.View, Resources.Dimens.DefaultHorizontalMargin),
                contentView.AtRightOf(this.View, Resources.Dimens.DefaultHorizontalMargin),
                contentView.AtTopOf(scrollView, 15),
                contentView.AtBottomOf(scrollView, 15)
            );

            #endregion

            this.ExtendedLayoutIncludesOpaqueBars = true;
        }

        private UIView CreateSub(Glyph glyph, string title, string content) {
            var view = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var iconView = new MSLIcon(glyph, 2f) {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(iconView);

            var titleLabel = new UILabel {
                Text = title,
                TextColor = Resources.Colors.ForegroundMid,
                Font = Resources.Fonts.OfSize(FontSize.Subhead),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(titleLabel);

            var contentLabel = new UILabel {
                Text = content,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.OfSize(FontSize.Body),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(contentLabel);

            view.AddConstraints(
                iconView.AtLeftOf(view),
                iconView.AtTopOf(view, 15f),

                titleLabel.ToRightOf(iconView, Resources.Dimens.DefaultHorizontalMargin),
                titleLabel.WithSameTop(iconView).Plus(2.5f),
                titleLabel.AtRightOf(view),

                contentLabel.WithSameLeft(titleLabel),
                contentLabel.Below(titleLabel, 5f),
                contentLabel.AtRightOf(view),
                contentLabel.AtBottomOf(view)
            );

            return view;
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            _navigationController = NavigationController;

            _navigationController.SetNavigationBarHidden(false, true);
            _navigationController.NavigationBar.Translucent = true;
            _navigationController.ToolbarHidden = false;

        }

        public override void ViewWillDisappear(bool animated) {
            // For some reason this method is called before the view
            // appears so we still need to check if the local nav controller
            // is null.
            if (_navigationController != null) {
                _navigationController.NavigationBar.Translucent = false;
                _navigationController.Toolbar.BarTintColor = Resources.Colors.Accent;
                _navigationController.ToolbarHidden = true;
            }

            base.ViewWillDisappear(animated);
        }
    }
}
