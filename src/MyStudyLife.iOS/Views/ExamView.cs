using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using Foundation;
using MvvmCross.Plugin.Color.Platforms.Ios;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.View;
using UIKit;

namespace MyStudyLife.iOS.Views {
    [Register("ExamView")]
    internal sealed class ExamView : EntityView<Exam, ExamViewModel> {
        private MvxPropertyChangedListener _listener;

        private Sub _dateSub;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.State), ViewModelOnStateChange)
                .Listen(nameof(ViewModel.Color), () => {
                    var color = ViewModel.Color?.ToNativeColor();
                    NavigationController?.NavigationBar?.ApplyAppearance(backgroundColor: color);
                });

            #region Creation

            var subContentView = new OAStackView.OAStackView {
                Axis = UILayoutConstraintAxis.Vertical
            };
            ContentView.Add(subContentView);

            _dateSub = CreateSub(subContentView, Glyph.Calendar);
            var durationSub = CreateSub(subContentView, Glyph.Stopwatch, _dateSub.View);
            var seatSub = CreateSub(subContentView, Glyph.Seat, durationSub.View);
            var roomSub = CreateSub(subContentView, Glyph.Location, seatSub.View);

            var revisionTasks = new MSLRelatedItemsView(
                Resources.Strings.ExamView.RevisionTasks,
                MslTableViewSource.For<TaskRelatedTableViewCell>,
                Resources.Strings.ExamView.RevisionTasksLonely
            );
            this.ContentView.Add(revisionTasks);

            var conflictingClasses = new MSLRelatedItemsView(
                Resources.Strings.ExamView.ConflictingClasses,
                MslTableViewSource.For<ScheduledClassTableViewCell>,
                Resources.Strings.ExamView.ConflictingClassesLonely
            );
            this.ContentView.Add(conflictingClasses);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<ExamView, ExamViewModel>();

            set.Bind(HeaderBackgroundView).For(x => x.BackgroundColor).To(x => x.Color).WithConversion("NativeColor");
            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).To(x => x.SubTitle);

            set.Bind(_dateSub.Label).To(x => x.Instance);
            set.Bind(_dateSub.SubLabel).To(x => x.DateRelativeText);
            set.Bind(_dateSub.SubLabel)
                .For(x => x.TextColor)
                .To(x => x.StateColor)
                .WithConversion(Resources.Converters.NativeColor)
                .WithFallback(_dateSub.SubLabel.TextColor);
            set.Bind(durationSub.Label).To(x => x.Duration);
            set.Bind(seatSub.Label).To(x => x.Item.Seat).WithConversion(Resources.Converters.Coalesce);
            set.Bind(roomSub.Label).To(x => x.Item.Room).WithConversion(Resources.Converters.Coalesce);

            set.Bind(revisionTasks.HeaderLabel).For(x => x.TextColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(revisionTasks.TableView.Source).To(x => x.TasksForExam);
            set.Bind(revisionTasks.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);
            set.Bind(revisionTasks.LonelyLabel).InvertedVisibility().To(x => x.TasksForExam.Count);

            set.Bind(conflictingClasses.HeaderLabel).For(x => x.TextColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(conflictingClasses.TableView.Source).For(x => x.ItemsSource).To(x => x.ConflictingClasses);
            set.Bind(conflictingClasses.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewScheduledClassCommand);
            set.Bind(conflictingClasses.LonelyLabel).InvertedVisibility().To(x => x.ConflictingClasses.Count);

            set.Apply();

            #endregion

            #region Layout

            ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                subContentView.AtTopOf(ContentView),
                subContentView.AtLeftOf(ContentView, HorizontalMargin),
                subContentView.AtRightOf(ContentView, HorizontalMargin),

                revisionTasks.WithSameLeft(View),
                revisionTasks.WithSameRight(View),
                revisionTasks.Below(subContentView),

                conflictingClasses.WithSameLeft(View),
                conflictingClasses.WithSameRight(View),
                conflictingClasses.Below(revisionTasks),
                conflictingClasses.WithSameBottom(ContentView)
            );

            #endregion

            ViewModelOnStateChange();
        }

        private void ViewModelOnStateChange() {
            var vm = this.ViewModel;
            var sub = this._dateSub;

            if (vm != null && sub != null) {
                sub.BoldSubLabel = vm.State == ExamState.InTheNext3Days || vm.State == ExamState.InTheNext7Days;
            }
        }
    }
}