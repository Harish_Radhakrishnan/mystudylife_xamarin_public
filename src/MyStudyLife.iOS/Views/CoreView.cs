using System;
using CoreGraphics;
using MvvmCross;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.ViewModels;
using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.UI.ViewModels.Components;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using SDWebImage;

namespace MyStudyLife.iOS.Views {
    [MvxRootPresentation(WrapInNavigationController = true)]
    public class CoreView : MvxViewController<NavigationDrawerViewModel> {
        private readonly UIView _viewContainer = new UIView();
        private UIView _menuView;
        private UITapGestureRecognizer _menuHideOnTapGestureRecognizer;

        private MenuHeader _menuHeader;
        private UILabel _syncStatusLabel;

        public static nfloat MenuTopOffset {
            get {
                // Handle newer devices (safe area insets) vs older (status bar frame)
                var safeAreaInsets = UIApplication.SharedApplication.KeyWindow.SafeAreaInsets;

                return safeAreaInsets.Top > 0
                    ? safeAreaInsets.Top
                    : UIApplication.SharedApplication.StatusBarFrame.Size.Height;
            }
        }

        public CoreView() {
            Request = MvxViewModelRequest<NavigationDrawerViewModel>.GetDefaultRequest();
        }

        protected internal CoreView(IntPtr handle) : base(handle) { }

        private UIView CreateMenuItem<TViewModel>(string text, Glyph glyph, UIView below = null) where TViewModel : MvxViewModel {
            var menuItem = new MenuItem {
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var menuItemIcon = new MSLIcon(glyph, 1.5f) {
                TextColor = UIColor.White,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            menuItem.Add(menuItemIcon);

            var menuItemText = new UILabel {
                Text = text,
                TextColor = UIColor.White,
                Font = Resources.Fonts.FontNormal,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            menuItem.Add(menuItemText);

            menuItem.AddConstraints(
                menuItemIcon.WithSameLeft(menuItem).Plus(15),
                menuItemIcon.WithSameCenterY(menuItem),

                menuItemText.ToRightOf(menuItemIcon).Plus(20),
                menuItemText.WithSameCenterY(menuItemIcon)
            );
            
            menuItem.AddGestureRecognizer(
                new UITapGestureRecognizer(async (r) => {
                    // Prevent navigation if the user clicks on the
                    // same 
                    if (
                        ChildViewControllers.Length > 0 &&
                        typeof (TViewModel).Name.StartsWith(ChildViewControllers[0].GetType().Name)
                    ) {
                        HideMenu();    
                    }
                    else {
                        await Mvx.IoCProvider.Resolve<INavigationService>().Navigate(typeof(TViewModel));
                    }
                })
            );

            _menuView.Add(menuItem);

            _menuView.AddConstraints(
                menuItem.WithSameLeft(_menuView),
                menuItem.WithSameRight(_menuView)
            );

            if (below != null) {
                _menuView.AddConstraints(
                    menuItem.Below(below)//.Plus(UIScreen.MainScreen.IsWidescreen() ? 20f : 15f)
                );
            }

            return menuItem;
        }

        public override void ViewDidLoad() {
            // Ensure it's visible after SignInView / WelcomeWizard
            NavigationController.SetNavigationBarHidden(false, false);
            
            base.ViewDidLoad();

            View.BackgroundColor = Resources.Colors.Foreground;

            var menuView = _menuView  = new UIView {
                BackgroundColor = Resources.Colors.Foreground
            };
            Add(menuView);

            menuView.Add(_menuHeader = new MenuHeader());
            
            var syncStatusView = new SyncStatusView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            menuView.Add(syncStatusView);
            
            syncStatusView.AddGestureRecognizer(new UITapGestureRecognizer(() => {
                ViewModel?.TriggerSyncCommand.Execute(null);
            }));
            
            // Top board
            syncStatusView.Add(new UIView(new CGRect(0, 0, syncStatusView.Frame.Width, .5f)) {
                BackgroundColor = Resources.Colors.ForegroundMid,
                AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleBottomMargin
            });
            
            _syncStatusLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.ForegroundLight,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeSmall)
            };
            syncStatusView.Add(_syncStatusLabel);
            
            var dashboardMenuItem = CreateMenuItem<DashboardViewModel>(Resources.Strings.Dashboard, Glyph.Dashboard);
            var calendarMenuItem = CreateMenuItem<CalendarWeekViewModel>(Resources.Strings.Calendar, Glyph.Calendar, dashboardMenuItem);
            var tasksMenuitem = CreateMenuItem<TasksViewModel>(Resources.Strings.Tasks, Glyph.Task, calendarMenuItem);
            var examsMenuItem = CreateMenuItem<ExamsViewModel>(Resources.Strings.Exams, Glyph.Exam, tasksMenuitem);
            var scheduleMenuItem = CreateMenuItem<ScheduleViewModel>(Resources.Strings.Schedule, Glyph.Schedule, examsMenuItem);
            var searchMenuItem = CreateMenuItem<SearchViewModel>(Resources.Strings.Search, Glyph.Search, scheduleMenuItem);
            CreateMenuItem<SettingsViewModel>(Resources.Strings.Settings, Glyph.Settings, searchMenuItem);
            
            var set = this.CreateBindingSet<CoreView, NavigationDrawerViewModel>();
            
            set.Bind(_menuHeader).For(x => x.User).To(x => x.User);
            set.Bind(_menuHeader).For(x => x.HasSchoolLogo).To(x => x.SchoolCoverLogo.IsLoaded);
            set.Bind(_menuHeader).For(x => x.SchoolLogoPath).To(x => x.SchoolCoverLogo.Path);
            set.Bind(_menuHeader).For(x => x.FillBehindContent).To(x => x.SchoolCoverLogo.IsFallback).WithConversion(Resources.Converters.InvertedBoolean);
            set.Bind(_syncStatusLabel).To(x => x.SyncStatusMessage);
            
            set.Apply();
            
            menuView.AddConstraints(
                _menuHeader.AtTopOf(menuView),
                _menuHeader.WithSameLeft(menuView),
                _menuHeader.WithSameRight(menuView),
            
                dashboardMenuItem.Below(_menuHeader, 7.5f),
                
                syncStatusView.WithSameLeft(_menuView),
                syncStatusView.WithSameRight(_menuView),
                syncStatusView.AtBottomOf(_menuView),
            
                _syncStatusLabel.AtLeftOf(menuView, 15),
                _syncStatusLabel.AtRightOf(menuView, 15),
                _syncStatusLabel.WithSameCenterY(syncStatusView)
            );

            var viewContainer = _viewContainer;
            viewContainer.TranslatesAutoresizingMaskIntoConstraints = false;
            
            View.Add(viewContainer);

            viewContainer.WidthAnchor.ConstraintEqualTo(View.WidthAnchor).Active = true;
            viewContainer.HeightAnchor.ConstraintEqualTo(View.HeightAnchor).Active = true;
            
            viewContainer.Layer.ShadowRadius = 5;
            viewContainer.Layer.ShadowOpacity = .4f;
            viewContainer.Layer.ShadowColor = UIColor.Black.CGColor;
            viewContainer.Layer.ShadowPath = UIBezierPath.FromRect(new CGRect(
                0,
                -(NavigationController.NavigationBar.Bounds.Height + MenuTopOffset + 10 /* Offset to prevent shadow rounding */),
                NavigationController.View.Bounds.Width,
                NavigationController.View.Bounds.Height + 20
            )).CGPath;

            NavigationItem.SetLeftBarButtonItems(new []{
                new UIBarButtonItem(UIImage.FromBundle("MenuIcon.png"), UIBarButtonItemStyle.Plain, (s, e) => ToggleMenu())
            }, true);

            View.AddGestureRecognizer(
                _menuHideOnTapGestureRecognizer = new UITapGestureRecognizer(r => {
                    if (MenuVisible) {
                        var l = r.LocationInView(View);

                        // If tapped off menu, hide it
                        if (l.X >= MenuWidth) {
                            HideMenu();
                        }
                    }
                }) {
                    Enabled = false
                }
            );

            nfloat originalTranslationX = 0;

            View.AddGestureRecognizer(
                new UIPanGestureRecognizer(r => {
                    switch (r.State) {
                        case UIGestureRecognizerState.Began:
                            originalTranslationX = viewContainer.Transform.x0;
                            break;
                        case UIGestureRecognizerState.Changed: {
                                var translation = r.TranslationInView(View);

                                var translationX = originalTranslationX + translation.X;

                                if (0f < translationX && translationX < MenuWidth) {
                                    OffsetMenu(translationX);
                                }
                        }
                            break;
                        case UIGestureRecognizerState.Failed:
                        case UIGestureRecognizerState.Cancelled:
                        case UIGestureRecognizerState.Ended: {
                                var velocity = r.VelocityInView(View);

                                bool enoughVelocity = Math.Abs(velocity.X) >= MenuFlickVelocity;

                                // Shows menu if the velocity exceeds the threshold and is
                                // in the right direction or the user interaction ended
                                // with the menu having greater than 1/2 it's width visible.
                                if (enoughVelocity) {
                                    if (velocity.X > 0) {
                                        ShowMenu();
                                    }
                                    else {
                                        HideMenu();
                                    }
                                }
                                else if (viewContainer.Transform.x0 > (MenuWidth / 2)) {
                                    ShowMenu();
                                }
                                else {
                                    HideMenu();
                                }
                        }
                            break;
                    }
                })
            );
        }

        public override void WillAnimateRotation(UIInterfaceOrientation toInterfaceOrientation, double duration) {
            base.WillAnimateRotation(toInterfaceOrientation, duration);

            if (_viewContainer != null) {
                // Using the existing location prevents any glitches when hiding
                // the menu.
                _viewContainer.Frame = new CGRect(_viewContainer.Frame.Location, View.Frame.Size);

                if (MenuVisible) {
                    HideMenu(false);
                }
            }
        }

        public override void ViewWillLayoutSubviews() {
            base.ViewWillLayoutSubviews();

            if (NavigationController != null) {
                var viewOffset = NavigationController.NavigationBar.Frame.Height + MenuTopOffset;

                _menuView.Frame = new CGRect(
                    0,
                    -viewOffset,
                    MenuWidth,
                    viewOffset + View.Frame.Height - UIApplication.SharedApplication.KeyWindow.SafeAreaInsets.Bottom
                );
            }
        }

        public override bool ShouldAutorotate() {
            if (ChildViewControllers.Length > 0) {
                return ChildViewControllers[0].ShouldAutorotate();
            }

            return base.ShouldAutorotate();
        }

        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation() {
            if (ChildViewControllers.Length > 0) {
                return ChildViewControllers[0].PreferredInterfaceOrientationForPresentation();
            }

            return base.PreferredInterfaceOrientationForPresentation();
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations() {
            if (ChildViewControllers.Length > 0) {
                return ChildViewControllers[0].GetSupportedInterfaceOrientations();
            }

            return base.GetSupportedInterfaceOrientations();
        }

        #region Menu

        private const float MenuWidth = 280f;
        private const float MenuFlickVelocity = 1000f;
        private const float MenuAnimationDuration = 0.3f;
        private bool _menuVisible;
        private bool MenuVisible {
            get => _menuVisible;
            set {
                _menuVisible = value;

                if (ViewModel != null) {
                    ViewModel.IsOpen = value;
                }
            }
        }

        private void ToggleMenu() {
            if (MenuVisible) {
                HideMenu();
            }
            else {
                ShowMenu();
            }
        }

        private void ShowMenu() {
            UIView.Animate(MenuAnimationDuration, () => OffsetMenu(MenuWidth), () => {
                _viewContainer.UserInteractionEnabled = false;
                MenuVisible = true;
            });
        }

        private void HideMenu(bool animated = true) {
            if (animated) {
                UIView.Animate(MenuAnimationDuration, () => OffsetMenu(0), () => {
                    _viewContainer.UserInteractionEnabled = true;
                    MenuVisible = false;
                });                
            }
            else {
                OffsetMenu(0);

                _viewContainer.UserInteractionEnabled = true;
                MenuVisible = false;
            }
        }
        
        private void OffsetMenu(nfloat offset) {
            // Null checking for #MSL-100

            var translation = CGAffineTransform.MakeTranslation(offset, 0);

            var viewContainer = _viewContainer;
            if (viewContainer != null) {
                viewContainer.Transform = translation;
            }

            var navigationBar = NavigationController?.NavigationBar;
            var toolbar = NavigationController?.Toolbar;

            if (navigationBar != null) {
                navigationBar.Transform = translation;
            }
            if (toolbar != null) {
                toolbar.Transform = translation;
            }

            MenuVisible = offset > 0;

            if (_menuHideOnTapGestureRecognizer != null) {
                // Has to be disabled when the menu is hidden, otherwise screws
                // table selection and navigation bar click events up.
                _menuHideOnTapGestureRecognizer.Enabled = MenuVisible;
            }
        }

        internal void ShowViewController(UIViewController controller) {
            var childController = controller as IChildUIController;

            childController?.OnNavigatingTo(this, MenuVisible);

            if (MenuVisible) {
                HideMenu();
            }

            if (ChildViewControllers.Length > 0) {
                var vc = ChildViewControllers[0];

                vc.WillMoveToParentViewController(null);
                vc.View.RemoveFromSuperview();
                vc.RemoveFromParentViewController();
                
                NavigationItem.SetRightBarButtonItems(new UIBarButtonItem[0], true);
                NavigationItem.TitleView = null;
            }

            AddChildViewController(controller);
            controller.View.AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight;
            _viewContainer.Add(controller.View);
            controller.View.Frame = _viewContainer.Bounds;
            controller.DidMoveToParentViewController(this);

            if (controller.Title != null) {
                Title = controller.Title;
            }
        }

        abstract class TouchView : UIView {
            private bool _touchDown;

            public override void TouchesBegan(NSSet touches, UIEvent evt) {
                base.TouchesBegan(touches, evt);

                _touchDown = true;
                OnTouchStart();
            }

            // Touch released
            public override void TouchesEnded(NSSet touches, UIEvent evt) {
                base.TouchesEnded(touches, evt);
                
                if (_touchDown) {
                    OnTouchEnd();
                    _touchDown = false;
                }
            }

            // Touch leaves view
            public override void TouchesCancelled(NSSet touches, UIEvent evt) {
                base.TouchesCancelled(touches, evt);

                if (_touchDown) {
                    OnTouchEnd();
                    _touchDown = false;
                }
            }

            protected virtual void OnTouchStart() { }
            protected virtual void OnTouchEnd() { }
        }

        class SyncStatusView : TouchView {
            const float StandardHeight = 56f;
            const float SmallHeight = 48f;

            public override CGSize IntrinsicContentSize {
                get {
                    nfloat width = base.IntrinsicContentSize.Width;
                    nfloat height = UIScreen.MainScreen.Bounds.Height >= 568 ? StandardHeight : SmallHeight;

                    return new CGSize(width, height);
                }
            }

            protected override void OnTouchStart() {
                BackgroundColor = new UIColor(0, 0, 0, 0.3f);
            }

            protected override void OnTouchEnd() {
                Animate(0.3f, () => BackgroundColor = UIColor.Clear);
            }
        }

        class MenuItem : TouchView {
            public override CGSize IntrinsicContentSize =>  new CGSize(base.IntrinsicContentSize.Width, 44);

            protected override void OnTouchStart() {
                Alpha = 0.3f;
            }

            protected override void OnTouchEnd() {
                Animate(0.3f, () => Alpha = 1f);
            }
        }

        class MenuHeader : UIView {
            const float StandardHeight = 90f;
            const float SchoolLogoHeight = 140f;
            const float UserPictureSize = 60f;

            private bool _fillBehindContent = true;

            private UIView _schoolLogoOverlay;
            private UIImageView _schoolLogoImage, _userPicturePlaceholder, _userPicture;
            private UILabel _userName, _userEmail;

            private NSLayoutConstraint[] _mutableLayoutConstraints;

            private bool _hasSchoolLogo = true;
            private string _schoolLogoPath;

            public User User {
                get => null;
                set => SetValuesFromUser(value);
            }

            public bool HasSchoolLogo {
                get => _hasSchoolLogo;
                set {
                    if (_hasSchoolLogo == value) return;
                    
                    _hasSchoolLogo = value;

                    OnHasSchoolLogoChanged();

                    SetNeedsLayout();
                    LayoutIfNeeded();
                    SetNeedsDisplay();
                }
            }

            public string SchoolLogoPath {
                get => _schoolLogoPath;
                set {
                    if (_schoolLogoPath == value) return;
                    
                    _schoolLogoPath = value;

                    SetCoverImage(value);
                }
            }
            
            public bool FillBehindContent {
                get => _fillBehindContent;
                set {
                    if (_fillBehindContent == value) return;
                    
                    _fillBehindContent = value;
                    OnHasSchoolLogoChanged();
                }
            }

            private void SetCoverImage(string path) {
                _schoolLogoImage.Image = path != null ? new UIImage(path) : null;
            }

            private bool ExpandedIfHasSchoolLogo => UIScreen.MainScreen.Bounds.Height >= 568;

            private bool Expanded => ExpandedIfHasSchoolLogo && HasSchoolLogo;

            public nfloat TopOffset => MenuTopOffset;
                
            public MenuHeader() {
                TranslatesAutoresizingMaskIntoConstraints = false;
                BackgroundColor = Resources.Colors.Accent;

                Add(_schoolLogoImage = new UIAlignedImageView {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    ContentMode = UIViewContentMode.ScaleAspectFit,
                    Alignment = UIImageViewAlignment.TopRight
                });
                Add(_schoolLogoOverlay = new UIView {
                    AutoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight,
                    BackgroundColor = UIColor.FromWhiteAlpha(0, .3f),
                    Hidden = true
                });
                
                Add(_userPicturePlaceholder = new UIImageView {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    Layer = {
                        CornerRadius = UserPictureSize / 2f,
                        MasksToBounds = true
                    }
                });
                
                Add(_userPicture = new UIImageView {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    Layer = {
                        CornerRadius = _userPicturePlaceholder.Layer.CornerRadius,
                        MasksToBounds = _userPicturePlaceholder.Layer.MasksToBounds
                    }
                });

                // The placeholder text here is required. It prevents a layout bug where the 
                // setting of the name/email is delayed (like on sign in) - NeedsLayout etc. doesn't
                // seem to fix this.
                Add(_userName = new UILabel {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    TextColor = UIColor.White,
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal, FontWeight.Medium),
                    Text = "Name"
                });

                Add(_userEmail = new UILabel {
                    TranslatesAutoresizingMaskIntoConstraints = false,
                    TextColor = UIColor.White,
                    Font = Resources.Fonts.FontNormal,
                    Text = "Email"
                });
                
                this.AddConstraints(
                    _schoolLogoImage.AtTopOf(this),
                    _schoolLogoImage.AtRightOf(this),

                    _userPicture.AtLeftOf(this, 15),
                    
                    _userPicture.Bottom().LessThanOrEqualTo().BottomOf(this).Plus(15),
                    _userPicture.Width().EqualTo(UserPictureSize),
                    _userPicture.Height().EqualTo().WidthOf(_userPicture),

                    _userPicturePlaceholder.WithSameLeft(_userPicture),
                    _userPicturePlaceholder.WithSameRight(_userPicture),
                    _userPicturePlaceholder.WithSameTop(_userPicture),
                    _userPicturePlaceholder.WithSameBottom(_userPicture),

                    _userName.AtRightOf(this, 15),
                    
                    _userEmail.WithSameLeft(_userName),
                    _userEmail.WithSameRight(_userName),
                    _userEmail.Below(_userName).Plus(2.5f)
                );

                OnHasSchoolLogoChanged();
            }

            private void OnHasSchoolLogoChanged() {
                if (_mutableLayoutConstraints != null) {
                    RemoveConstraints(_mutableLayoutConstraints);
                    _mutableLayoutConstraints = null;
                }

                _mutableLayoutConstraints = GetMutableLayoutConstraints(Expanded);

                AddConstraints(_mutableLayoutConstraints);

                BackgroundColor = HasSchoolLogo ? UIColor.White : Resources.Colors.Accent;

                if (_schoolLogoImage != null) {
                    _schoolLogoImage.Hidden = !HasSchoolLogo;
                    _schoolLogoImage.LayoutMargins = _fillBehindContent ? UIEdgeInsets.Zero : new UIEdgeInsets(20f, 5f, 2.5f, 5f);
                    _schoolLogoOverlay.Hidden = _schoolLogoImage.Hidden;
                }
            }

            private NSLayoutConstraint[] GetMutableLayoutConstraints(bool expanded) => new[] {
                _fillBehindContent
                    ? NSLayoutConstraint.Create(
                        _schoolLogoImage,
                        NSLayoutAttribute.Left,
                        NSLayoutRelation.Equal,
                        this,
                        NSLayoutAttribute.Left,
                        1,
                        0
                    )
                    : NSLayoutConstraint.Create(
                        _schoolLogoImage,
                        NSLayoutAttribute.Left,
                        NSLayoutRelation.Equal,
                        _userPicture,
                        NSLayoutAttribute.Right,
                        1,
                        0
                    ),

                NSLayoutConstraint.Create(
                    this,
                    NSLayoutAttribute.Height,
                    NSLayoutRelation.Equal,
                    null,
                    NSLayoutAttribute.NoAttribute,
                    1,
                    (expanded ? SchoolLogoHeight : StandardHeight) + TopOffset
                ),

                expanded && !_fillBehindContent
                    ? NSLayoutConstraint.Create(
                        _schoolLogoImage,
                        NSLayoutAttribute.Bottom,
                        NSLayoutRelation.Equal,
                        _userName,
                        NSLayoutAttribute.Top,
                        1,
                        0
                    )
                    : NSLayoutConstraint.Create(
                        _schoolLogoImage,
                        NSLayoutAttribute.Bottom,
                        NSLayoutRelation.Equal,
                        this,
                        NSLayoutAttribute.Bottom,
                        1,
                        0
                    ),

                NSLayoutConstraint.Create(
                    _userPicture,
                    NSLayoutAttribute.Top,
                    NSLayoutRelation.Equal,
                    this,
                    NSLayoutAttribute.Top,
                    1,
                    TopOffset + 15f
                ),

                NSLayoutConstraint.Create(
                    _userName,
                    NSLayoutAttribute.Left,
                    NSLayoutRelation.Equal,
                    expanded ? (UIView) this : _userPicture,
                    expanded ? NSLayoutAttribute.Left : NSLayoutAttribute.Right,
                    1,
                    15f
                ),

                expanded
                    ? NSLayoutConstraint.Create(
                        _userEmail,
                        NSLayoutAttribute.Bottom,
                        NSLayoutRelation.Equal,
                        this,
                        NSLayoutAttribute.Bottom,
                        1,
                        -15f
                    )
                    : NSLayoutConstraint.Create(
                        _userName,
                        NSLayoutAttribute.Bottom,
                        NSLayoutRelation.Equal,
                        _userPicture,
                        NSLayoutAttribute.CenterY,
                        1,
                        -2.5f
                    )
            };

            public void SetValuesFromUser(User user) {
                var placeholder = UIImage.FromBundle(user.IsTeacher ? "TeacherPlaceholder.png" : "StudentPlaceholder.png");

                // Gravatar images return a transparent png, so placeholder has to be beneath
                _userPicturePlaceholder.Image = placeholder;

                if (user.Picture != null) {
                    _userPicture.SetImage(
                        user.Picture,
                        placeholder,
                        SDWebImageOptions.RefreshCached,
                        (image, error, cacheType, url) => {
                            var scale = (float)UIScreen.MainScreen.Scale;

                            if (image != null && scale > 1.0f) {
                                _userPicture.Image = new UIImage(image.CGImage, scale, UIImageOrientation.Up);
                            }
                        }
                    );
                }
                else {
                    _userPicture.Image = placeholder;
                }

                _userName.Text = user.FullName;
                _userEmail.Text = user.Email;

                _userName.SetNeedsDisplay();
                _userEmail.SetNeedsDisplay();
            }
        }

        #endregion

        public interface IChildUIController {
            void OnNavigatingTo(CoreView coreView, bool fromMenu);
        }
    }
}