using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MyStudyLife.Data;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels.Input;
using UIKit;

namespace MyStudyLife.iOS.Views.Input {
    [Register(nameof(ExamInputView))]
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal sealed class ExamInputView : EntityInputView<Exam, ExamInputViewModel> {
        protected override string TypeName => Resources.Strings.Exam;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            Root.Sections[0].AddAll(new Element[] {
                new MslBooleanElement(Resources.Strings.Resit)
                    .Bind(Bindings, x => x.InputItem.Resit),
                new MslEntryElement {
                    Placeholder = Resources.Strings.Module
                }.Bind(Bindings, x => x.InputItem.Module)
            });

            Root.Add(new [] {
                new Section {
                    new DateTimeInlineElement(Resources.Strings.Date).Bind(Bindings, x => x.InputItem.Date),
                    new NumberElement(Resources.Strings.Duration) {
                        Min = 5,
                        Max = 720,
                        Suffix = "minutes"
                    }.Bind(Bindings, x => x.InputItem.Duration)
                },
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Seat
                    }.Bind(Bindings, x => x.InputItem.Seat),
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Room
                    }.Bind(Bindings, x => x.InputItem.Room)
                }
            });

            this.AddDeleteElementIfNeeded();
        }
    }
}