using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.iOS.Views.Input {
    [Register(nameof(ClassTimeInputView))]
    internal sealed class ClassTimeInputView : InputView<ClassTimeInputViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _vmListener;

        private MslStaticRootElement _dayTypeElement;
        private readonly CheckboxFlagsElement _daysElement;
        private readonly CheckboxFlagsElement _rotationDaysElement;

        public ClassTimeInputView() {
            this._daysElement = new CheckboxFlagsElement(Resources.Strings.Days, new Group("Days"));
            this._rotationDaysElement = new CheckboxFlagsElement(Resources.Strings.ClassInputView.RotationDays, new Group("RDays"));
        }

        protected override void InitializeNavigationBar(UINavigationBar navigationBar) {
            // Do not override previously set styles from ClassInputView
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.DayType), SetDayVisibility);

            var bindings = this.CreateInlineBindingTarget<ClassTimeInputViewModel>();

            Root = new RootElement {
                UnevenRows = true
            }.Bind(bindings, x => x.Caption, x => x.InputTypeText, "StringFormat", "{0} Time");

            this._daysElement
                .Bind(bindings, x => x.ItemsSource, x => x.Days)
                .Bind(bindings, x => x.Value, x => x.InputItem.Days, new DaysOfWeekToIntConverter())
                .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor);

            Root.Add(new[] {
                new Section {
                    _daysElement as Element
                },
                new Section {
                    new TimeInlineElement(Resources.Strings.StartTime)
                        .Bind(bindings, x => x.InputItem.StartTime, Resources.Converters.TimeSpanToDateTime)
                        .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor),
                    new TimeInlineElement(Resources.Strings.EndTime)
                        .Bind(bindings, x => x.InputItem.EndTime, Resources.Converters.TimeSpanToDateTime)
                        .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor),
                }
            });

            this.OnViewModelLoaded(() => {
                if (this.ViewModel.IsDayRotationEnabled) {
                    this._dayTypeElement = new MslStaticRootElement(Resources.Strings.ClassInputView.DayType, new RadioGroup("DayType")) {
                        new Section {
                            new MslRadioElement(Resources.Strings.ClassInputView.DayOfWeek, "DayType"),
                            new MslRadioElement(Resources.Strings.ClassInputView.RotationDay, "DayType")
                        }
                    };

                    this._dayTypeElement
                        .Bind(bindings, x => x.RadioSelected, x => x.DayType)
                        .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor);


                    this.Root.Sections[0].Insert(0, _dayTypeElement);

                    this._rotationDaysElement
                        .Bind(bindings, x => x.ItemsSource, x => x.RotationDays)
                        .Bind(bindings, x => x.Value, x => x.InputItem.RotationDays, new RotationDaysToIntConverter())
                        .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor);
                }
                else if (this.ViewModel.IsWeekRotationEnabled) {
                    this.Root.Sections[0].Add(
                        new MslRootElement(Resources.Strings.ClassInputView.RotationWeek, new RadioGroup("RWeek")) {
                            new MvxRadioSection((rWeek) => {
                                var rWeekTuple = (Tuple<int, string>) rWeek;

                                return new MslRadioElement(rWeekTuple.Item2);
                            }).Bind(bindings, x => x.ItemsSource, x => x.RotationWeeks)
                        }
                        .Bind(bindings, x => x.RadioSelected, x => x.InputItem.RotationWeek)
                        .Bind(bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor) as Element
                    );
                }

                if (!this.ViewModel.IsNew) {
                    this.Root.Add(
                        new Section {
                            new DeleteElement(Resources.Strings.Delete).Bind(bindings, x => x.SelectedCommand, x => x.DeleteCommand)
                        }
                    );
                }
            });
        }

        private void SetDayVisibility() {
            if (this.Root == null || !this.Root.Sections.Any()) {
                return;
            }

            var targetSection = this.Root.Sections[0];

            if (this.ViewModel.IsDayRotation) {
                targetSection.Remove(_daysElement);

                if (!targetSection.Elements.Contains(_rotationDaysElement)) {
                    targetSection.Add((Element)_rotationDaysElement);
                }
            }
            else {
                targetSection.Remove(_rotationDaysElement);

                if (!targetSection.Elements.Contains(_daysElement)) {
                    targetSection.Add((Element)_daysElement);
                }
            }
        }
    }

    class CheckboxFlagsElement : RootElement, ITintable {
        private UIColor _tintColor;
        private int _value;
        private ICollection<Tuple<int, string, int>> _itemsSource;

        public event EventHandler ValueChanged;

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    this.SelectMany(x => x.Elements)
                        .OfType<FlagCheckboxElement>()
                        .Apply(x => x.TintColor = _tintColor);
                }
            }
        }

        public int? Value {
            get { return _value; }
            set {
                // Lazy null handling
                _value = value.GetValueOrDefault();

                SetSelectedCheckboxes();
            }
        }

        public ICollection<Tuple<int, string, int>> ItemsSource {
            get { return _itemsSource; }
            set {
                _itemsSource = value;

                SetElementsFromItemsSource();
            }
        }

        public CheckboxFlagsElement(string caption, Group group) : base(caption, group) { }

        protected void SetElementsFromItemsSource() {
            if (!this.Sections.Any()) {
                this.Add(new Section());
            }
            else {
                this.Sections[0].Clear();
            }

            if (ItemsSource == null) {
                return;
            }

            foreach (var a in ItemsSource) {
                var item = a;

                var element = new FlagCheckboxElement(item.Item2, item.Item1, (_value & item.Item1) == item.Item1) {
                    TintColor = TintColor
                };

                element.ValueChanged += (s, e) => {
                    _value = element.Value ? (_value | item.Item1) : (_value & ~item.Item1);

                    ValueChanged?.Invoke(this, EventArgs.Empty);
                };

                this.Sections[0].Add(element);
            }
        }

        private void SetSelectedCheckboxes() {
            foreach (var section in this.Sections) {
                foreach (var chk in section.Elements.OfType<FlagCheckboxElement>()) {
                    var flag = chk.FlagValue;

                    chk.Value = (_value & flag) == flag;
                }
            }
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(CellKey) ?? new UITableViewCell(UITableViewCellStyle.Value1, CellKey) {
                SelectionStyle = UITableViewCellSelectionStyle.Blue,
                Accessory = UITableViewCellAccessory.DisclosureIndicator
            };

            cell.TextLabel.Text = Caption;

            if (cell.DetailTextLabel != null) {
                cell.DetailTextLabel.Text = Summary();
            }

            return cell;
        }

        protected override void UpdateCellDisplay(UITableViewCell cell) {
            base.UpdateCellDisplay(cell);

            if (cell.DetailTextLabel != null) {
                cell.DetailTextLabel.Text = Summary();
            }
        }

        public override string Summary() {
            if (this.ItemsSource == null || _value <= 0) {
                return Resources.Strings.None;
            }

            var sb = new StringBuilder();

            foreach (var item in this.ItemsSource) {
                if ((_value & item.Item1) == item.Item1) {
                    sb.AppendFormat("{0}, ", item.Item2);
                }
            }

            return sb.ToString(0, Math.Max(sb.Length - 2, 0));
        }
    }

    class FlagCheckboxElement : CheckboxElement, ITintable {
        private UIColor _tintColor;

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    var cac = CurrentAttachedCell;
                    if (cac != null) {
                        cac.TintColor = value;
                    }
                }
            }
        }

        public int FlagValue { get; set; }

        public FlagCheckboxElement(string caption, int flagValue, bool isChecked) : base(caption, isChecked) {
            this.FlagValue = flagValue;
            this.ShouldDeselectAfterTouch = true;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);
            cell.TintColor = TintColor;
            return cell;
        }
    }
}