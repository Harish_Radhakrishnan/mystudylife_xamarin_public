using System;
using CoreGraphics;
using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using MvvmCross.Plugin.Color.Platforms.Ios;
using MvvmCross.ViewModels;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.iOS.Views.Input {
    internal abstract class EntityInputView<TEntity, TViewModel> : InputView<TViewModel>
        where TEntity : SubjectDependentEntity
        where TViewModel : SubjectDependentInputViewModel<TEntity> {
        private MvxPropertyChangedListener _listener;

        protected MvxInlineBindingTarget<TViewModel> Bindings { get; private set; } 

        protected abstract string TypeName { get; }
        
        #region Title

        private UILabel _titleLabel;
        private UILabel _subTitleLabel;
        private string _subTitle;

        protected UILabel TitleLabel {
            get { return _titleLabel; }
        }

        protected UILabel SubTitleLabel {
            get { return _subTitleLabel; }
        }

        public override string Title {
            get { return base.Title; }
            set {
                base.Title = value;

                if (_titleLabel != null) {
                    _titleLabel.Text = value;

                    this.SizeTitleView();
                }
            }
        }

        public string SubTitle {
            get { return _subTitle; }
            set {
                _subTitle = value;

                if (_subTitleLabel != null) {
                    _subTitleLabel.Text = value;

                    this.SizeTitleView();
                }
            }
        }

        private void SizeTitleView() {
            _titleLabel.SizeToFit();
            _subTitleLabel.SizeToFit();

            // A non-static value creates stuttery animation
            const float width = 200f;// Math.Max(_titleLabel.Frame.Width, _subTitleLabel.Frame.Width);

            _titleLabel.Frame = _titleLabel.Frame.WithWidth(width);
            _subTitleLabel.Frame = new CGRect(
                _subTitleLabel.Frame.X,
                _titleLabel.Frame.Height,
                width,
                _subTitleLabel.Frame.Height
            );

            NavigationItem.TitleView.Frame = new CGRect(
                NavigationItem.TitleView.Frame.X,
                NavigationItem.TitleView.Frame.Y,
                width,
                _titleLabel.Frame.Height + _subTitleLabel.Frame.Height
            );
        }

        #endregion

        protected override void InitializeNavigationBar(UINavigationBar navigationBar) {
            base.InitializeNavigationBar(navigationBar);

            navigationBar.ApplyAppearance(backgroundColor: ViewModel?.Color.ToNativeColor());
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(nameof(ViewModel.Color), () => {
                    var color = ViewModel.Color.ToNativeColor();
                    NavigationController.NavigationBar.ApplyAppearance(backgroundColor: color);
                });
            
            #region Title

            _titleLabel = new UILabel {
                Text = this.Title,
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.BoldSystemFontOfSize(17)
            };

            _subTitleLabel = new UILabel {
                Text = this.SubTitle,
                TextColor = UIColor.White,
                TextAlignment = UITextAlignment.Center,
                BackgroundColor = UIColor.Clear,
                Font = UIFont.SystemFontOfSize(12)
            };

            var titleView = new MSLNavigationTitleView {
                BackgroundColor = UIColor.Clear
            };

            titleView.Add(_titleLabel);
            titleView.Add(_subTitleLabel);

            NavigationItem.TitleView = titleView;

            SizeTitleView();

            titleView.TouchUpInside += TitleViewOnTouchUpInside;

            #endregion

            Bindings = this.CreateInlineBindingTarget<TViewModel>();

            this.Bind(Bindings, x => x.Title, x => x.InputType, "StringFormat", "{0} " + TypeName);
            this.Bind(Bindings, x => x.SubTitle, x => x.SelectedScheduleText);

            _listener.Listen(nameof(ViewModel.Color), () => {
                var color = ViewModel.Color.ToNativeColor();
                NavigationController.NavigationBar.ApplyAppearance(backgroundColor: color);
            });

            Root = new MslRootElement {
                UnevenRows = true
            }
            .Bind(Bindings, x => x.Caption, x => x.InputType, "StringFormat", "{0} " + TypeName)
            .Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor);

            Root.Add(
                new Section {
                    new MslRootElement(Resources.Strings.Subject, new RadioGroup("Subject"), this.ViewModel.NewSubjectCommand) {
                        new MvxRadioSection(item => new SubjectElement(item as Subject)).Bind(Bindings, x => x.ItemsSource, x => x.Subjects)
                    }.Bind(Bindings, x => x.RadioSelectedItem, x => x.InputItem.Subject) as Element
                }
            );
        }

        protected virtual void TitleViewOnTouchUpInside(object sender, EventArgs e) {
            // TODO(iOS) Add help text
            var root = new AcademicScheduleRootElement(this.ViewModel.NewAcademicYearCommand)
                .Bind(Bindings, x => x.AcademicYears, x => x.AcademicYears)
                .Bind(Bindings, x => x.SelectedSchedule, x => x.SelectedSchedule);

            root.Selected(this);
        }
    }

    class MSLNavigationTitleView : UIButton {
        public override bool Highlighted {
            get { return base.Highlighted; }
            set {
                base.Highlighted = value;

                if (value) {
                    Alpha = 0.3f;
                }
                else {
                    Animate(0.3f, () => this.Alpha = 1);
                }
            }
        }
    }
}