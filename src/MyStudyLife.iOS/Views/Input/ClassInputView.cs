using System.ComponentModel;
using MvvmCross.Binding;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.WeakSubscription;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using Class = MyStudyLife.Data.Class;

namespace MyStudyLife.iOS.Views.Input {
    // TODO(JC): There's a lot of explicit bindings for TintColor here, would be better if
    // they were propogated down on Add(Section) or Add(Element)

    [Register(nameof(ClassInputView))]
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal sealed class ClassInputView : EntityInputView<Class, ClassInputViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _vmListener;
        [UsedImplicitly] private MvxPropertyChangedListener _itemListener;

        private Section _oneOffSection;
        private ClassTimesSection _classTimesSection;
        private Section _startEndDateSection;
        private readonly DateInlineElement _startDateElement;
        private readonly DateInlineElement _endDateElement;

        private ColorElement _colorElement;

        protected override string TypeName => Resources.Strings.Class;

        public ClassInputView() {
            _startDateElement = new DateInlineElement(Resources.Strings.StartDate);
            _endDateElement = new DateInlineElement(Resources.Strings.EndDate);
        }
        
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.OnViewModelLoaded(() => {
                _itemListener = new MvxPropertyChangedListener(this.ViewModel.InputItem)
                    .Listen(nameof(this.ViewModel.InputItem.HasStartEndDates), SetStartEndDateVisibility);

                SetOneOffOrRecurringVisibility();
                SetStartEndDateVisibility();

                SetColorVisibility();
            });

            _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.InputClassType), SetOneOffOrRecurringVisibility)
                .Listen(nameof(this.ViewModel.SelectedColor), SetColorVisibility);

            _startDateElement
                .Bind(Bindings, x => x.InputItem.StartDate)
                .Bind(Bindings, x => x.MinDate, x => x.MinStartDate)
                .Bind(Bindings, x => x.MaxDate, x => x.MaxStartDate)
                .Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor);
            _endDateElement
                .Bind(Bindings, x => x.InputItem.EndDate)
                .Bind(Bindings, x => x.MinDate, x => x.MinEndDate)
                .Bind(Bindings, x => x.MaxDate, x => x.MaxEndDate)
                .Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor);

            var helpTextLabel = new HelpTextLabel(label => {
                // Yes, this is how it has to be done (bleugh)
                var tv = label.Superview as UITableView;

                if (tv != null) {
                    tv.TableHeaderView = label;
                }
            });
            helpTextLabel.Bind(Bindings, x => x.HelpText);

            TableView.TableHeaderView = helpTextLabel;
            
            Root.Add(new[] {
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Room
                    }.Bind(Bindings, x => x.InputItem.Room),
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Building
                    }.Bind(Bindings, x => x.InputItem.Building)
                }
            });

            if (User.Current.IsTeacher) {
                _colorElement = new ColorElement(Resources.Strings.Color)
                    .Bind(Bindings, x => x.SelectedColor)
                    .Bind(Bindings, x => x.Colors, x => x.Colors);

                Root.Sections[0].Add(
                    new MslBooleanElement(Resources.Strings.ClassInputView.OverrideSubjectColor)
                        .Bind(Bindings, x => x.SelectedColor, "Boolean", mode: MvxBindingMode.OneWay)
                        .Bind(Bindings, x => x.CheckedCommand, x => x.OverrideColorCommand)
                );

                Root.Insert(1,
                    UITableViewRowAnimation.None,
                    new Section {
                        new MslEntryElement {
                            Placeholder = Resources.Strings.NameOrModule
                        }.Bind(Bindings, x => x.InputItem.Module)
                    }
                );
            }
            else {
                Root.Sections[0].Add(
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Module
                    }.Bind(Bindings, x => x.InputItem.Module)
                );

                Root.Add(new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Teacher
                    }.Bind(Bindings, x => x.InputItem.TeacherName)
                });
            }

            this.OnViewModelLoaded(() => {
                if (this.ViewModel.IsNew) {
                    this.Root.Insert(3,
                        new Section {
                            new MslStaticRootElement(Resources.Strings.ClassInputView.Occurs, new RadioGroup("Type")) {
                                new Section {
                                    new MslRadioElement(Resources.Strings.ClassInputView.Once, "Type"),
                                    new MslRadioElement(Resources.Strings.ClassInputView.MultipleTimes, "Type")
                                }
                            }
                            .Bind(Bindings, x => x.RadioSelected, x => x.InputClassType)
                            .Bind(Bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor) as Element
                        }
                    );
                }
            });

            _oneOffSection = new Section {
                new DateInlineElement(Resources.Strings.Date)
                    .Bind(Bindings, x => x.InputItem.Date)
                    .Bind(Bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor),
                new TimeInlineElement(Resources.Strings.StartTime)
                    .Bind(Bindings, x => x.InputItem.StartTime, Resources.Converters.TimeSpanToDateTime)
                    .Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor),
                new TimeInlineElement(Resources.Strings.EndTime)
                    .Bind(Bindings, x => x.InputItem.EndTime, Resources.Converters.TimeSpanToDateTime)
                    .Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor)
            };
            _classTimesSection =  new ClassTimesSection(this) {
                Caption = Resources.Strings.Times
            }.Bind(Bindings, x => x.ItemsSource, x => x.InputItem.Times);

            _startEndDateSection = new Section {
                new MslBooleanElement(Resources.Strings.ClassInputView.AddStartEndDates)
                    .Bind(Bindings, x => x.InputItem.HasStartEndDates, mode: MvxBindingMode.OneWay)
                    .Bind(Bindings, x => x.CheckedCommand, x => x.HasStartEndDatesCommand)
                    .Bind(Bindings, x => x.TintColor, x => x.Color, Resources.Converters.NativeColor)
            };

            this.AddDeleteElementIfNeeded();
        }

        private void SetOneOffOrRecurringVisibility() {
            if (this.Root == null || _oneOffSection == null || _classTimesSection == null) {
                return;
            }

            bool isOneOff = this.ViewModel.InputItem != null && !this.ViewModel.IsRecurring;

            if (isOneOff) {
                if (!this.Root.Contains(_oneOffSection)) {
                    this.Root.Add(_oneOffSection);
                }
                
                this.Root.Remove(_classTimesSection);
                this.Root.Remove(_startEndDateSection);
            }
            else {
                if (!this.Root.Contains(_classTimesSection)) {
                    this.Root.Add(_classTimesSection);
                }

                if (!this.Root.Contains(_startEndDateSection)) {
                    this.Root.Add(_startEndDateSection);
                }
                
                this.Root.Remove(_oneOffSection);
            }
        }

        private void SetStartEndDateVisibility() {
            if (this.Root == null || _startEndDateSection == null) {
                return;
            }

            bool show = this.ViewModel.InputItem != null && this.ViewModel.InputItem.HasStartEndDates;

            if (show) {
                if (!_startEndDateSection.Elements.Contains(_startDateElement)) {
                    _startEndDateSection.Add(_startDateElement);
                }

                if (!_startEndDateSection.Elements.Contains(_endDateElement)) {
                    _startEndDateSection.Add(_endDateElement);
                }

                TableView.ScrollToRow(NSIndexPath.FromRowSection(0, this.Root.Sections.IndexOf(_startEndDateSection)), UITableViewScrollPosition.Top, true);

                if (_startEndDateSection.IndexPath != null) {
                    TableView.ScrollToRow(_startEndDateSection.IndexPath, UITableViewScrollPosition.None, true);
                }
            }
            else {
                _startEndDateSection.Remove(_startDateElement);
                _startEndDateSection.Remove(_endDateElement);
            }
        }

        private void SetColorVisibility() {
            if (this.Root == null || this.Root.Sections.Count == 0 || _colorElement == null) {
                return;
            }

            bool show = this.ViewModel.SelectedColor != null;

            if (show) {
                if (!Root.Sections[0].Elements.Contains(_colorElement)) {
                    Root.Sections[0].Add(_colorElement);
                }
            }
            else {
                Root.Sections[0].Remove(_colorElement);
            }
        }

        class ClassTimesSection : MvxSection {
            private readonly ClassInputView _view;
            private readonly StringElement _footerElement;

            public override Element FooterElement {
                get { return _footerElement; }
            }

            public ClassTimesSection(ClassInputView view) {
                this._view = view;
                this._footerElement = new NewElement(Resources.Strings.ClassInputView.NewTime) {
                    ShouldDeselectAfterTouch = true,
                    SelectedCommand = view.ViewModel.NewTimeCommand
                };
            }

            protected override Element CreateElement(object item) {
                var classTime = (ClassTime)item;

                return new ClassTimeElement(classTime, _view) {
                    SelectedCommand = _view.ViewModel.EditTimeCommand
                };
            }
        }

        class ClassTimeElement : Element {
            private static readonly NSString _key = new NSString("ClassTimeElement");

            private MvxNotifyPropertyChangedEventSubscription _subscription;

            private ClassTime _classTime;
            private ClassInputView _view;

            public ClassTimeElement(ClassTime classTime, ClassInputView view) : base(classTime.Time) {
                this._classTime = classTime;
                this._view = view;

                _subscription = classTime.WeakSubscribe(ClassTimeOnPropertyChanged);
            }

            private void ClassTimeOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
                if (this._classTime == null || this._view == null) {
                    if (_subscription != null) {
                        _subscription.Dispose();
                        _subscription = null;
                    }

                    return;
                }

                this.UpdateCaptionDisplay(this.CurrentAttachedCell);
            }

            protected override UITableViewCell GetCellImpl(UITableView tv) {
                var cell = tv.DequeueReusableCell(_key) ?? new UITableViewCell(UITableViewCellStyle.Subtitle, _key) {
                    SelectionStyle = UITableViewCellSelectionStyle.None
                };

                return cell;
            }


            protected override void UpdateCaptionDisplay(UITableViewCell cell) {
                if (cell == null || cell.TextLabel == null || cell.DetailTextLabel == null) {
                    return;
                }

                cell.TextLabel.Text = this._classTime != null ? this._classTime.Time : "--";
                cell.TextLabel.SetNeedsDisplay();

                cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;
                cell.DetailTextLabel.Text = this._classTime != null ? this._classTime.GetOccurrenceText() : null;
                cell.DetailTextLabel.SetNeedsDisplay();
            }

            public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
                SelectedCommand?.Execute(_classTime);

                tableView.DeselectRow(path, true);
            }

            protected override void Dispose(bool disposing) {
                if (disposing) {
                    _view = null;
                    _classTime = null;

                    if (_subscription != null) {
                        _subscription.Dispose();
                        _subscription = null;
                    }
                }
                
                base.Dispose(disposing);
            }
        }
    }
}