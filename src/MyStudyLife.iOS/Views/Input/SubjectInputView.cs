using System.Collections.Generic;
using CoreGraphics;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.Data.UI;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Plugin.Color.Platforms.Ios;

namespace MyStudyLife.iOS.Views.Input {
    [Register("SubjectInputView")]
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal sealed class SubjectInputView : InputView<SubjectInputViewModel> {
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var bindings = this.CreateInlineBindingTarget<SubjectInputViewModel>();
            
            var inset = TableView.SeparatorInset.Left;

            var scheduleSection = new Section("Advanced") {
                new AcademicScheduleRootElement(this.ViewModel.NewAcademicYearCommand)
                    .Bind(bindings, x => x.AcademicYears, x => x.AcademicYears)
                    .Bind(bindings, x => x.SelectedSchedule, x => x.SelectedSchedule) as Element
            };
            scheduleSection.FooterView = new HelpTextLabel {
                Text =
                    "Select a year or term for this subject if you are only taking it for a single year or term. " +
                    "If you are taking this subject over multiple years, assign the year or term to the class instead."
            };

            Root = new RootElement {
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Name
                    }.Bind(bindings, x => x.InputItem.Name),
                    new ColorElement(Resources.Strings.Color)
                        .Bind(bindings, x => x.SelectedColor)
                        .Bind(bindings, x => x.Colors, x => x.Colors)
                },
                new Section("What Are Subjects?") {
                    FooterView = new HelpTextLabel {
                        ContentEdgeInsets = new UIEdgeInsets(0,inset,inset,inset),
                        Text = "Subjects are also known as courses and enable you to group all of your classes, tasks and exams for that course."
                    }
                },
                scheduleSection
            }.Bind(bindings, x => x.Caption, x => x.InputType, "StringFormat", "{0} Subject");

            this.AddDeleteElementIfNeeded();
        }

        protected override void PrepareDeletionActionSheet(MslActionSheet actionSheet) {
            actionSheet.Title = R.SubjectDeletion;
        }
    }

    /// <remarks>
    ///     Based on <see cref="DateTimeElement"/>
    /// </remarks>
    internal class ColorElement : ValueElement<UserColor> {
        private IEnumerable<UserColor> _colors;
        private ColorViewSource _currentViewSource;

        public IEnumerable<UserColor> Colors {
            get { return _colors; }
            set {
                _colors = value;

                var viewSource = _currentViewSource;
                if (viewSource != null) {
                    viewSource.ItemsSource = _colors;
                }
            }
        }

        private UIView _colorIndicator;

        public ColorElement(string caption = null) : base(caption ?? "Color") { }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);

            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

            _colorIndicator = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Layer = {
                    CornerRadius = 10f,
                    BackgroundColor = (Value != null ? Value.Color.ToNativeColor() : Resources.Colors.Accent).CGColor
                }
            };
            cell.ContentView.AddSubview(_colorIndicator);

            cell.ContentView.AddConstraints(
                _colorIndicator.Width().EqualTo(20f),
                _colorIndicator.Height().EqualTo(20f),
                _colorIndicator.WithSameCenterY(cell.ContentView),
                _colorIndicator.WithSameRight(cell.ContentView)
            );

            return cell;
        }

        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            tableView.DeselectRow(path, false);

            var vc = new UICollectionViewController(new UICollectionViewFlowLayout {
                ItemSize = new CGSize(40, 40),
                SectionInset = new UIEdgeInsets(15, 15, 15, 15)
            }) {
                Title = Resources.Strings.Color,
                CollectionView = {
                    BackgroundColor = Resources.Colors.Background,
                    AllowsSelection = true
                }
            };

            vc.CollectionView.RegisterClassForCell(typeof(ColorCell), ColorCell.Identifier);

            var viewSource = _currentViewSource = new ColorViewSource(_colors, vc.CollectionView);

            vc.CollectionView.Source = viewSource;
            vc.CollectionView.ReloadData();

            viewSource.SelectedItemChanged += (s, e) => {
                var selectedItemPath = vc.CollectionView.GetIndexPathsForSelectedItems().Single();

                this.OnUserValueChanged(_colors.ElementAt((int)selectedItemPath.Item));

                var parent = vc.ParentViewController;
                var nav = parent as UINavigationController;

                if (nav != null) {
                    nav.PopViewController(true);
                }
                else {
                    vc.DismissViewController(true, null);
                }

                _currentViewSource = null;
            };

            dvc.ActivateController(vc);
        }

        protected override void UpdateDetailDisplay(UITableViewCell cell) {
            if (cell == null || _colorIndicator == null) {
                return;
            }

            _colorIndicator.Layer.BackgroundColor =
                (Value != null ? Value.Color.ToNativeColor() : Resources.Colors.Accent).CGColor;
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _currentViewSource?.Dispose();
                _currentViewSource = null;
            }

            base.Dispose(disposing);
        }

        private sealed class ColorViewSource : MvxCollectionViewSource {
            public ColorViewSource(IEnumerable<UserColor> colors, UICollectionView collectionView) : base(collectionView) {
                this.ItemsSource = colors;
            }

            public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath) {
                var cell = (ColorCell)collectionView.DequeueReusableCell(ColorCell.Identifier, indexPath);

                cell.Color = ((UserColor)this.GetItemAt(indexPath));

                return cell;
            }
        }

        [UsedImplicitly]
        private sealed class ColorCell : UICollectionViewCell {
            public static readonly NSString Identifier = new NSString("ColorCell");

            private UserColor _color;

            public UserColor Color {
                get { return _color; }
                set {
                    _color = value;

                    if (this.ContentView != null && value != null) {
                        this.ContentView.Layer.BackgroundColor = value.Color.ToNativeColor().CGColor;
                    }
                }
            }

            public override bool Highlighted {
                get { return base.Highlighted; }
                set {
                    base.Highlighted = value;

                    if (value) {
                        this.Alpha = 0.3f;
                    }
                    else {
                        Animate(0.3f, () => this.Alpha = 1f);
                    }
                }
            }

            // TODO(iOS) Color selected style?
            //public override bool Selected 

            [Foundation.Export("initWithFrame:")]
            public ColorCell(CGRect frame) : base(frame) {
                this.ContentView.Layer.CornerRadius = frame.Width / 2f;
            }
        }
    }
}