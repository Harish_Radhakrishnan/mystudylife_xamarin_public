using System;
using System.Collections.Generic;
using MvvmCross;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using MyStudyLife.UI.ViewModels.Base;
using UIKit;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.iOS.Views.Input {
    internal abstract class InputView<TViewModel> : MslDialogViewController<TViewModel> where TViewModel : BaseViewModel, IInputViewModel {
        [UsedImplicitly] private MvxPropertyChangedListener _vmlistener;

        private readonly List<Action> _onLoadedActions = new List<Action>(); 
        
        protected InputView() {}

        public override void ViewDidLoad() {
            base.ViewDidLoad();

// ReSharper disable once ConvertClosureToMethodGroup
            _vmlistener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.IsLoaded, () => {
                    if (this.ViewModel.IsLoaded) {
                        OnViewModelLoadedInternal();
                    }

                    // Forces view title to update and also fixes issue
                    // whereby some input views (bound radio elements, date picker)
                    // did not set the value on first bind...
                    //
                    // Perhaps they just need this?
                    // https://github.com/MvvmCross/MvvmCross/blob/v3.1/CrossUI/CrossUI.Touch/Dialog/Elements/StringElement.cs#L56
                    ReloadData();
                })
                .Listen(() => this.ViewModel.Errors, OnErrorsChanged);

            NavigationItem.SetHidesBackButton(true, true);

            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(UIBarButtonSystemItem.Cancel).WithCommand(this.ViewModel.CancelInputCommand),
                true
            );

            NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIBarButtonSystemItem.Save).WithCommand(this.ViewModel.SaveCommand),
                true
            );

            TableView.SeparatorColor = Resources.Colors.Border;
            TableView.BackgroundColor = Resources.Colors.Background;
        }

        protected void AddDeleteElementIfNeeded() {
            this.OnViewModelLoaded(() => {
                if (this.ViewModel.CanDelete) {
                    var element = new DeleteElement(Resources.Strings.Delete);

                    element.Tapped += DeleteOnTapped;

                    Root.Add(
                        new Section {
                            element
                        }
                    );
                }
            });
        }

        protected virtual void PrepareDeletionActionSheet(MslActionSheet actionSheet) {

        }

        private void DeleteOnTapped() {
            var actionSheet = new MslActionSheet {
                DestructiveButtonIndex = 0,
                CancelButtonIndex = 1,
                Title = R.DeletionMessage
            };

            actionSheet.AddButton(Resources.Strings.Delete);
            actionSheet.AddButton(Resources.Strings.Cancel);

            PrepareDeletionActionSheet(actionSheet);

            actionSheet.ShowInView(this.View);

            actionSheet.Dismissed += DeleteActionSheetOnDismissed;
        }

        private void DeleteActionSheetOnDismissed(object sender, UIButtonEventArgs e) {
            if ((int)e.ButtonIndex == 0) {
                this.ViewModel.DeleteCommand.Execute(null);
            }
        }

        private void OnViewModelLoadedInternal() {
            foreach (var a in _onLoadedActions) {
                a();
            }

            _onLoadedActions.Clear();
        }
        
        protected void OnViewModelLoaded(Action action) {
            if (this.ViewModel != null && this.ViewModel.IsLoaded) {
                action();
            }
            else {
                _onLoadedActions.Add(action);
            }
        }

        protected virtual void OnErrorsChanged() {
            var errors = this.ViewModel.Errors;

            if (errors != null) {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(
                    "Validation Errors",
                    String.Join("\n", errors.Values)
                );
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _vmlistener?.Dispose();
                _vmlistener = null;
            }

            base.Dispose(disposing);
        }
    }
}