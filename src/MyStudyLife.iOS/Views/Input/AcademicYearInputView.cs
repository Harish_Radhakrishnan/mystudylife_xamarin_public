using System;
using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.ViewModels.Input;
using CrossUI.iOS.Dialog;
using MvvmCross.ViewModels;
using MvvmCross.Platforms.Ios.Presenters.Attributes;

namespace MyStudyLife.iOS.Views.Input {
    [Register("AcademicYearInputView")]
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal sealed class AcademicYearInputView : InputView<AcademicYearInputViewModel> {
        [UsedImplicitly]
        private MvxPropertyChangedListener _vmListener, _schedulingOptionsListener;

        private SchedulingRootElement _schedulingElement;

        private Section _weekRotationSection, _dayRotationSection;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            Action attachSchedulingOptionsListener = () => {
                _schedulingOptionsListener = new MvxPropertyChangedListener(this.ViewModel.InputItem.Scheduling)
                    .Listen(() => this.ViewModel.InputItem.Scheduling.Mode, SetSchedulingOptionsVisibility);
            };

            if (this.ViewModel.InputItem == null) {
                _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                    .Listen(() => this.ViewModel.InputItem, () => {
                        if (this.ViewModel.InputItem != null) {
                            attachSchedulingOptionsListener();
                        }

                        SetSchedulingOptionsVisibility();
                    });
            }
            else {
                attachSchedulingOptionsListener();
            }

            var bindings = this.CreateInlineBindingTarget<AcademicYearInputViewModel>();

            Root = new MslRootElement {
                UnevenRows = true
            }.Bind(bindings, x => x.Caption, x => x.InputType, "StringFormat", "{0} Academic Year");

            var inset = TableView.SeparatorInset.Left;

            _schedulingElement = new SchedulingRootElement(Resources.Strings.Scheduling, new RadioGroup("Mode")) {
                new Section("CLASSES OCCUR ON") {
                    new RadioElement("The same day every week", "Mode"),
                    new RadioElement("The same day every x weeks", "Mode"),
                    new RadioElement("A numbered or lettered day", "Mode")
                }
            }
            .Bind(bindings, x => x.RadioSelected, x => x.InputItem.Scheduling.Mode)
            .Bind(bindings, x => x.Mode, x => x.InputItem.Scheduling.Mode)
            .Bind(bindings, x => x.WeekCount, x => x.InputItem.Scheduling.WeekCount);

            Root.Add(new[] {
                new Section {
                    new DateInlineElement(Resources.Strings.StartDate).Bind(bindings, x => x.InputItem.StartDate),
                    new DateInlineElement(Resources.Strings.EndDate).Bind(bindings, x => x.InputItem.EndDate)
                },
                new Section(Resources.Strings.Scheduling) {
                    _schedulingElement as Element
                },
                new TermsSection(this) {
                    Caption = "Terms"
                }.Bind(bindings, x => x.ItemsSource, x => x.InputItem.Terms),
                new Section("What Are Academic Years?") {
                    FooterView = new HelpTextLabel {
                        ContentEdgeInsets = new UIEdgeInsets(0, inset, inset / 2f, inset),
                        Text = "An academic year and its terms are used to represent your school year and any terms (eg. semesters, trimesters, quarters) that you may have."
                    }
                }
            });

            _weekRotationSection = new Section {
                new MslRootElement(Resources.Strings.NumberOfWeeks, new RadioGroup("WeekCount")) {
                    new MvxRadioSection((w) => new MslRadioElement(w.ToString()) {
                        ObjectValue = w
                    }).Bind(bindings, x => x.ItemsSource, x => x.WeekCountOptions)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.InputItem.Scheduling.WeekCount) as Element,
                new MslRootElement(Resources.Strings.StartWeek, new RadioGroup("StartWeek")) {
                    new MvxRadioSection((w) => {
                        var week = (KVP<short, string>)w;

                        return new MslRadioElement(week.Value) {
                            ObjectValue = week.Key
                        };
                    }).Bind(bindings, x => x.ItemsSource, x => x.AvailableWeeks)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.InputItem.Scheduling.StartWeek) as Element
            };

            _dayRotationSection = new Section {
                new MslRootElement(Resources.Strings.NumberOfDays, new RadioGroup("DayCount")) {
                    new MvxRadioSection((d) => new MslRadioElement(d.ToString()) {
                        ObjectValue = d
                    }).Bind(bindings, x => x.ItemsSource, x => x.DayCountOptions)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.InputItem.Scheduling.DayCount) as Element,
                new MslRootElement(Resources.Strings.StartDay, new RadioGroup("StartDay")) {
                    new MvxRadioSection((d) => {
                        var day = (KVP<int, string>) d;

                        return new MslRadioElement(day.Value) {
                            ObjectValue = day.Key
                        };
                    }).Bind(bindings, x => x.ItemsSource, x => x.AvailableDays)
                }.Bind(bindings, x => x.RadioSelectedItem, x => x.InputItem.Scheduling.StartDay) as Element,
                new CheckboxFlagsElement(Resources.Strings.Days, new Group("Days")) {}
                    .Bind(bindings, x => x.ItemsSource, x => x.Days)
                    .Bind(bindings, x => x.Value, x => x.InputItem.Scheduling.Days, new DaysOfWeekToIntConverter()) as Element
            };

            _dayRotationSection.FooterView = new HelpTextLabel(label => {
                // Fixes not updating footer sizing
                (label.Superview as UITableView)?.ReloadData();
            }) {
                ContentEdgeInsets = new UIEdgeInsets(inset / 2f, inset, inset / 2f, inset),
                TextColor = Resources.Colors.WarningForeground
            }.Bind(bindings, x => x.DayRotationWarning);

            this.SetSchedulingOptionsVisibility();
            this.AddDeleteElementIfNeeded();
        }

        private void SetSchedulingOptionsVisibility() {
            if (_schedulingElement == null || _weekRotationSection == null || this.ViewModel.InputItem == null) {
                return;
            }

            if (this.ViewModel.InputItem.Scheduling.IsWeekRotation) {
                if (!_schedulingElement.Sections.Contains(_weekRotationSection)) {
                    _schedulingElement.Add(_weekRotationSection);
                }
            }
            else {
                _schedulingElement.Remove(_weekRotationSection);
            }

            if (this.ViewModel.InputItem.Scheduling.IsDayRotation) {
                if (!_schedulingElement.Sections.Contains(_dayRotationSection)) {
                    _schedulingElement.Add(_dayRotationSection);
                }
            }
            else {
                _schedulingElement.Remove(_dayRotationSection);
            }
        }

        protected override void PrepareDeletionActionSheet(MslActionSheet actionSheet) {
            actionSheet.Title = R.AcademicScheduleDeletion;
        }

        class SchedulingRootElement : MslStaticRootElement {
            private static readonly NSString _key = new NSString("SchedulingRootElement");

            private SchedulingMode _mode;
            private int? _weekCount;

            public SchedulingMode Mode {
                get { return _mode; }
                set {
                    _mode = value;

                    if (this.CurrentAttachedCell != null) {
                        this.UpdateCaptionDisplay(this.CurrentAttachedCell);
                    }
                }
            }

            public int? WeekCount {
                get { return _weekCount; }
                set {
                    _weekCount = value;

                    if (this.CurrentAttachedCell != null) {
                        this.UpdateCaptionDisplay(this.CurrentAttachedCell);
                    }
                }
            }

            public SchedulingRootElement(string caption, RadioGroup group) : base(caption, group) {

            }

            protected override UITableViewCell GetCellImpl(UITableView tv) {
                var cell = tv.DequeueReusableCell(_key) ?? new UITableViewCell(UITableViewCellStyle.Subtitle, _key) {
                    SelectionStyle = UITableViewCellSelectionStyle.Blue,
                    Accessory = UITableViewCellAccessory.DisclosureIndicator
                };

                return cell;
            }

            protected override void UpdateCaptionDisplay(UITableViewCell cell) {
                if (cell == null || cell.TextLabel == null || cell.DetailTextLabel == null) {
                    return;
                }

                cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;

                switch (this.Mode) {
                    case SchedulingMode.Fixed:
                        cell.TextLabel.Text = Resources.Strings.Fixed;
                        cell.DetailTextLabel.Text = "Classes occur on the same day every week";
                        break;
                    case SchedulingMode.WeekRotation:
                        cell.TextLabel.Text = Resources.Strings.WeekRotation;
                        cell.DetailTextLabel.Text = "Classes occur on the same day every {0} weeks".WithArgs(this.WeekCount);
                        break;
                    case SchedulingMode.DayRotation:
                        cell.TextLabel.Text = Resources.Strings.DayRotation;
                        cell.DetailTextLabel.Text = "Classes occur on a numbered or lettered day";
                        break;
                }

                cell.TextLabel.SetNeedsDisplay();
                cell.DetailTextLabel.SetNeedsDisplay();
            }
        }

        class TermsSection : MvxSection {
            private readonly AcademicYearInputView _view;
            private readonly StringElement _footerElement;

            public override Element FooterElement {
                get { return _footerElement; }
            }

            public TermsSection(AcademicYearInputView view) {
                this._view = view;
                this._footerElement = new NewElement(Resources.Strings.AcademicYearInputView.NewTerm) {
                    SelectedCommand = view.ViewModel.NewTermCommand
                };
            }

            protected override Element CreateElement(object item) {
                var term = (AcademicTerm)item;

                return new AcademicTermElement(term) {
                    SelectedCommand = _view.ViewModel.EditTermCommand
                };
            }
        }

        class AcademicTermElement : Element {
            private static readonly NSString _key = new NSString("AcademicTermElement");

            private readonly AcademicTerm _term;

            public AcademicTermElement(AcademicTerm term) : base(term.Name) {
                this._term = term;
            }

            protected override UITableViewCell GetCellImpl(UITableView tv) {
                var cell = tv.DequeueReusableCell(_key) ?? new UITableViewCell(UITableViewCellStyle.Subtitle, _key) {
                    SelectionStyle = UITableViewCellSelectionStyle.Blue
                };

                return cell;
            }

            protected override void UpdateCaptionDisplay(UITableViewCell cell) {
                if (cell == null || cell.TextLabel == null || cell.DetailTextLabel == null || _term == null) {
                    return;
                }

                cell.TextLabel.Text = this._term.Name;
                cell.TextLabel.SetNeedsDisplay();

                cell.DetailTextLabel.Text = this._term.Dates;
                cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;
                cell.DetailTextLabel.SetNeedsDisplay();
            }

            public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
                if (SelectedCommand != null) {
                    SelectedCommand.Execute(_term);
                }

                tableView.DeselectRow(path, true);
            }
        }
    }
}