using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MyStudyLife.Data;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;
using UIKit;

namespace MyStudyLife.iOS.Views.Input {
    [Foundation.Register("TaskInputView")]
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal class TaskInputView : EntityInputView<Task, TaskInputViewModel> {
        [UsedImplicitly] private MvxPropertyChangedListener _itemListener;

        private readonly MvxRootElement _examElement;
        private readonly MvxRadioSection _examSection;

        protected override string TypeName =>  Resources.Strings.Task;

        public TaskInputView() {
            _examElement = new MslRootElement("Exam", new RadioGroup("Exam")) {
                new Section {
                    new MslRadioElement("None")
                },
                (_examSection = new MvxRadioSection(item => new ExamElement(item as Exam)) {
                    StartIndex = 1
                })
            };
        }
        
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.OnViewModelLoaded(() => {
                SetExamVisibility();

                _itemListener = new MvxPropertyChangedListener(this.ViewModel.InputItem)
                    .Listen(nameof(this.ViewModel.InputItem.IsRevision), SetExamVisibility);
            });
            
            // Bind directly the task as we're not using the "null" exam
            _examElement.Bind(Bindings, x => x.RadioSelectedItem, x => x.InputItem.Exam);
            // Needed as it's not always a child of the root
            _examElement.Bind(Bindings, x => x.TintColor, x => x.ColorOrNull, Resources.Converters.NativeColor);

            _examSection.Bind(Bindings, x => x.ItemsSource, x => x.Exams);

            Root.Add(new [] {
                new Section {
                    new MslStaticRootElement(Resources.Strings.Type, new RadioGroup("Type")) {
                        new Section {
                            new MslRadioElement(Resources.Strings.Assignment, "Type"),
                            new MslRadioElement(Resources.Strings.Reminder, "Type"),
                            new MslRadioElement(Resources.Strings.Revision, "Type")
                        }
                    }
                    .Bind(Bindings, x => x.RadioSelected, x => x.InputItem.Type) as Element
                },
                new Section {
                    new DateInlineElement(Resources.Strings.Due)
                        .Bind(Bindings, x => x.InputItem.DueDate)
                },
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Title
                    }.Bind(Bindings, x=> x.InputItem.Title),
                    new MslMultilineEntryElement {
                        Placeholder = Resources.Strings.Detail
                    }.Bind(Bindings, x => x.InputItem.Detail)
                }
            });

            this.SetExamVisibility();

            this.AddDeleteElementIfNeeded();
        }

        private void SetExamVisibility() {
            if (this.Root == null || !this.Root.Sections.Any()) {
                return;
            }

            bool show = (this.ViewModel?.InputItem?.IsRevision).GetValueOrDefault();

            var targetSection = this.Root.Sections[1];

            if (show) {
                if (!targetSection.Elements.Contains(_examElement)) {
                    targetSection.Elements.Add(_examElement);
                    
                    this.ReloadTableView();
                }
            }
            else {
                if (targetSection.Elements.Remove(_examElement)) {
                    this.ReloadTableView();
                }
            }
        }
    }
}