using MvvmCross.Binding.BindingContext;
using CrossUI.iOS.Dialog.Elements;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.iOS.Views.Input {
    [Foundation.Register("AcademicTermInputView")]
    internal sealed class AcademicTermInputView : InputView<AcademicTermInputViewModel> {
        public override void ViewDidLoad() {
            base.ViewDidLoad();

            var bindings = this.CreateInlineBindingTarget<AcademicTermInputViewModel>();


            Root = new MslRootElement {
                UnevenRows = true
            }.Bind(bindings, x => x.Caption, x => x.InputType, "StringFormat", "{0} Academic Term");

            Root.Add(new[] {
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Name
                    }.Bind(bindings, x => x.InputItem.Name)
                },
                new Section {
                    new DateInlineElement(Resources.Strings.StartDate)
                        .Bind(bindings, x => x.InputItem.StartDate)
                        .Bind(bindings, x => x.MinDate, x => x.MinStartDate)
                        .Bind(bindings, x => x.MaxDate, x => x.MaxStartDate),
                    new DateInlineElement(Resources.Strings.EndDate)
                        .Bind(bindings, x => x.InputItem.EndDate)
                        .Bind(bindings, x => x.MinDate, x => x.MinEndDate)
                        .Bind(bindings, x => x.MaxDate, x => x.MaxEndDate)
                }
            });

            this.AddDeleteElementIfNeeded();
        }

        protected override void PrepareDeletionActionSheet(MslActionSheet actionSheet) {
            actionSheet.Title = R.AcademicScheduleDeletion;
        }
    }
}