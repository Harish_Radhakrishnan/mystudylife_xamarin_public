using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.iOS.Views.Input {
    [Foundation.Register("HolidayInputView")]
    internal sealed class HolidayInputView : InputView<HolidayInputViewModel> {
        [UsedImplicitly] private IDisposable _vmListener;

        private Section _datesSection;
        private MslBooleanElement _pushesScheduleElement;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _vmListener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.ShowPushesSchedule), SetPushesScheduleVisibility);

            var bindings = this.CreateInlineBindingTarget<HolidayInputViewModel>();

            Root = new MslRootElement {
                UnevenRows = true
            }.Bind(bindings, x => x.Caption, x => x.InputType, "StringFormat", "{0} Holiday");

            Root.Add(new[] {
                new Section {
                    new MslEntryElement {
                        Placeholder = Resources.Strings.Name
                    }.Bind(bindings, x => x.InputItem.Name)
                },
                _datesSection = new Section {
                    new DateInlineElement(Resources.Strings.StartDate)
                        .Bind(bindings, x => x.InputItem.StartDate)
                        .Bind(bindings, x => x.MinDate, x => x.InputAcademicYear.StartDate)
                        .Bind(bindings, x => x.MaxDate, x => x.InputAcademicYear.EndDate),
                    new DateInlineElement(Resources.Strings.EndDate)
                        .Bind(bindings, x => x.InputItem.EndDate)
                        .Bind(bindings, x => x.MinDate, x => x.InputAcademicYear.StartDate)
                        .Bind(bindings, x => x.MaxDate, x => x.InputAcademicYear.EndDate),
                },
                new Section("What Are Holidays?") {
                    FooterView = new HelpTextLabel {
                        ContentEdgeInsets = new UIEdgeInsets(0,15f,15f,15f)
                    }.Bind(bindings, x => x.HelpText)
                }
            });

            _pushesScheduleElement = new PushesScheduleElement()
                .Bind(bindings, x => x.InputItem.PushesSchedule)
                .Bind(bindings, x => x.DetailText, x => x.PushesScheduleHelpText);
            
            this.AddDeleteElementIfNeeded();
        }

        private void SetPushesScheduleVisibility() {
            if (_datesSection == null) {
                return;
            }

            if (this.ViewModel.ShowPushesSchedule) {
                if (!_datesSection.Elements.Contains(_pushesScheduleElement)) {
                    _datesSection.Elements.Add(_pushesScheduleElement);

                    this.ReloadTableView();
                }
            }
            else if (_datesSection.Elements.Remove(_pushesScheduleElement)) {
                this.ReloadTableView();
            }
        }

        protected override void PrepareDeletionActionSheet(MslActionSheet actionSheet) {
            actionSheet.Title = R.HolidayDeletion;
        }

        class PushesScheduleElement : MslBooleanElement {
            private static readonly NSString Key = new NSString("PushesScheduleElement");
            private UISwitch _switch;

            private string _detailText;

            public string DetailText {
                get { return this.CurrentAttachedCell.DetailTextLabel.Text; }
                set {
                    _detailText = value;

                    if (this.CurrentAttachedCell != null) {
                        this.UpdateCaptionDisplay(this.CurrentAttachedCell);
                    }
                }
            }

            public PushesScheduleElement() : base(Resources.Strings.PushesSchedule) { }

            protected override UITableViewCell GetCellImpl(UITableView tv) {
                if (_switch == null) {
                    _switch = CreateSwitch();
                    _switch.AddTarget(delegate { base.OnUserValueChanged(_switch.On); }, UIControlEvent.ValueChanged);
                }
                else
                    _switch.On = Value;

                var cell = tv.DequeueReusableCell(Key);
                if (cell == null) {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, Key) {
                        SelectionStyle = UITableViewCellSelectionStyle.None
                    };
                }
                else
                    cell.AccessoryView = null;

                if (_switch.Superview != null) {
                    var oldCell = _switch.Superview as UITableViewCell;
                    if (oldCell != null)
                        oldCell.AccessoryView = null;
                }

                cell.TextLabel.Text = Caption;
                cell.AccessoryView = _switch;

                return cell;
            }

            protected override void UpdateCaptionDisplay(UITableViewCell cell) {
                if (cell == null || cell.TextLabel == null || cell.DetailTextLabel == null) {
                    return;
                }

                cell.DetailTextLabel.Text = _detailText;
                cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;

                cell.TextLabel.SetNeedsDisplay();
                cell.DetailTextLabel.SetNeedsDisplay();
            }

            protected override void Dispose(bool disposing) {
                if (disposing) {
                    if (_switch != null) {
                        _switch.Dispose();
                        _switch = null;
                    }
                }
            }

            protected override void UpdateDetailDisplay(UITableViewCell cell) {
                if (_switch != null)
                    _switch.On = Value;
            }
        }
    }
}