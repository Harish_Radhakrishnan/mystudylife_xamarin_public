using System;
using CoreGraphics;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Plugin.Color.Platforms.Ios;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.ViewModels.View;
using OAStackView;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    // Would be nice to use UIModalPresentationStyle.Automatic for the card effect, but the navigation service needs to be aware of this
    // https://github.com/MvvmCross/MvvmCross/issues/3546
    [MvxModalPresentation(WrapInNavigationController = true, ModalPresentationStyle = UIModalPresentationStyle.FullScreen)]
    internal abstract class EntityView<TEntity, TViewModel> : MslViewController<TViewModel>
        where TEntity : SubjectDependentEntity
        where TViewModel : EntityViewModel<TEntity> {

        public const float HorizontalMargin = 15f;

        private UIScrollView _scrollView;

        protected UIView HeaderBackgroundView { get; private set; }
        protected OAStackView.OAStackView HeaderView { get; private set; }
        protected UIView ContentView { get; private set; }
        protected MSLLabel TitleLabel { get; private set; }
        protected MSLLabel SubTitleLabel { get; private set; }

        protected override void InitializeNavigationBar(UINavigationBar navigationBar) {
            base.InitializeNavigationBar(navigationBar);

            navigationBar.ApplyAppearance(backgroundColor: ViewModel?.Color?.ToNativeColor());
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            NavigationItem.SetLeftBarButtonItem(
                new UIBarButtonItem(Resources.Strings.Close, UIBarButtonItemStyle.Plain, null).WithCommand(this.ViewModel.CloseCommand),
                true
            );

            NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(Resources.Strings.Edit, UIBarButtonItemStyle.Plain, null).WithCommand(this.ViewModel.EditCommand),
                true
            );

            #region Creation

            // Stack views are non drawing
            HeaderBackgroundView = new UIView {
                BackgroundColor = Resources.Colors.Accent,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Add(HeaderBackgroundView);

            HeaderView = new OAStackView.OAStackView {
                Alignment = OAStackViewAlignment.Fill,
                Axis = UILayoutConstraintAxis.Vertical,
                Distribution = OAStackViewDistribution.Fill,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            Add(HeaderView);

            TitleLabel = new MSLLabel {
                TextColor = UIColor.White,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeExtraLarge, FontWeight.Light),
                ContentEdgeInsets = new UIEdgeInsets(30,15,0,15),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            HeaderView.AddArrangedSubview(TitleLabel);

            SubTitleLabel = new MSLLabel {
                TextColor = UIColor.White,
                Font = Resources.Fonts.FontNormal,
                ContentEdgeInsets = new UIEdgeInsets(0, 15, 0, 15),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            HeaderView.AddArrangedSubview(SubTitleLabel);

            ContentView = CreateContentView();

            //AddDivider(ContentView, SubTitleLabel, 10f);

            #endregion

            #region Layout

            View.AddConstraints(
                HeaderView.AtLeftOf(View),
                HeaderView.AtRightOf(View),
                HeaderView.AtTopOf(View),

                HeaderBackgroundView.WithSameLeft(HeaderView),
                HeaderBackgroundView.WithSameTop(HeaderView),
                HeaderBackgroundView.WithSameRight(HeaderView),
                HeaderBackgroundView.WithSameBottom(HeaderView).Plus(15)
            );

            #endregion
        }

        /// <summary>
        ///     Creates and adds the <see cref="ContentView"/> to the view.
        /// </summary>
        protected virtual UIView CreateContentView() {
            var scrollView = _scrollView = new UIScrollView {
                BackgroundColor = UIColor.White
            };
            Add(scrollView);

            var contentView = new UIView {
                BackgroundColor = UIColor.White,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            scrollView.Add(contentView);

            View.AddConstraints(
                contentView.WithSameLeft(View),
                contentView.WithSameRight(View),
                contentView.AtTopOf(scrollView, 10f),
                contentView.WithSameBottom(scrollView)
            );

            return contentView;
        }

        private nfloat _headerViewHeight;

        public override void ViewDidLayoutSubviews() {
            base.ViewDidLayoutSubviews();

            if (HeaderBackgroundView != null && HeaderBackgroundView.Frame.Height != _headerViewHeight) {
                _headerViewHeight = HeaderBackgroundView.Frame.Height;
                SizeRootView();
            }
        }

        protected Sub CreateSub(UIView superView, Glyph glyph, UIView below = null) {
            var superStackView = superView as OAStackView.OAStackView;

            UIView view;

            if (superStackView != null) {
                view = new UIView();

                superStackView.AddArrangedSubview(view);
            }
            else {
                view = new MslView {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                superView.Add(view);
            }

            var icon = new MSLIcon(glyph, 2f) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.ForegroundMid
            };
            view.Add(icon);

            var labelView = new OAStackView.OAStackView {
                Axis = UILayoutConstraintAxis.Vertical,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(labelView);

            var label = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontNormal
            };
            labelView.AddArrangedSubview(label);

            var subLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontSmall
            };
            labelView.AddArrangedSubview(subLabel);

            view.AddConstraints(
                icon.AtLeftOf(view),
                icon.AtTopOf(view, 10),
                icon.AtBottomOf(view, 10),

                labelView.ToRightOf(icon, 10),
                labelView.WithSameCenterY(view)
            );

            if (superStackView == null) {
                superView.AddConstraints(
                    view.WithSameLeft(superView).Plus(HorizontalMargin),
                    view.WithSameRight(superView).Minus(HorizontalMargin)
                );

                superView.AddConstraints(below != null ? view.Below(below) : view.AtTopOf(superView));
            }

            return new Sub(view, label, subLabel);
        }

        [Obsolete("Use CreateSub instead - SubLabels will auto hide")]
        protected Sub CreateSubWithSubLabel(UIView superView, Glyph glyph, UIView below = null)
            => CreateSub(superView, glyph, below);

        protected UIView CreateDivider() {
            var border = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = Resources.Colors.Border
            };

            border.AddConstraints(
                border.Height().EqualTo(.5f)
            );

            return border;
        }

        protected UIView AddDivider(UIView superView, UIView below, float marginTop = 0f) {
            var border = CreateDivider();
            superView.Add(border);

            superView.AddConstraints(
                border.WithSameLeft(superView).Plus(15f),
                border.WithSameRight(superView)
            );

            if (below != null) {
                superView.AddConstraints(
                    border.Below(below).Plus(marginTop)
                );
            }

            return border;
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            base.SizeRootViewImpl(rect);

            if (this._scrollView != null) {
                if (HeaderBackgroundView != null) {
                    var headerViewY = HeaderBackgroundView.Frame.Height + HeaderBackgroundView.Frame.Y;

                    rect = new CGRect(
                        rect.X,
                        rect.Y + headerViewY,
                        rect.Width,
                        rect.Height - headerViewY
                    );
                }

                this._scrollView.Frame = rect;
            }
        }
    }

    class Sub {
        private bool _boldSubLabel;

        public UIView View { get; private set; }

        public UILabel Label { get; private set; }

        public UILabel SubLabel { get; private set; }

        public bool BoldSubLabel {
            get { return _boldSubLabel; }
            set {
                if (this.SubLabel != null && _boldSubLabel != value) {
                    this._boldSubLabel = value;
                    this.SetSubLabelFont();
                }
            }
        }

        public Sub(UIView view, UILabel label) {
            this.View = view;
            this.Label = label;
        }

        public Sub(UIView view, UILabel label, UILabel subLabel) : this(view, label) {
            this.SubLabel = subLabel;
            this.SetSubLabelFont();
        }

        private void SetSubLabelFont() {
            this.SubLabel.Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall, this._boldSubLabel ? FontWeight.Medium : FontWeight.Normal);
        }
    }

    sealed class MSLRelatedItemsView : MslView {
        const float HorizontalMargin = Resources.Dimens.DefaultHorizontalMargin;

        public MSLLabel HeaderLabel { get; private set; }

        public MslFixedTableView TableView { get; private set; }

        public MSLLabel LonelyLabel { get; private set; }

        public MSLRelatedItemsView(string header, Func<UITableView, MslTableViewSource> getSource, string lonelyText = null) {
            UIView divider;

            this.Add(divider = this.CreateDivider());
            this.Add(this.HeaderLabel = CreateRelatedHeaderLabel(header));
            this.Add(this.TableView = CreatedRelatedTableView(getSource));
            this.Add(this.LonelyLabel = CreateRelatedLonelyLabel(lonelyText));

            this.TranslatesAutoresizingMaskIntoConstraints = false;

            this.AddConstraints(
                divider.AtLeftOf(this, HorizontalMargin),
                divider.AtRightOf(this),
                divider.AtTopOf(this),

                HeaderLabel.AtLeftOf(this),
                HeaderLabel.AtRightOf(this),
                HeaderLabel.Below(divider, 15f).WithIdentifier("$HeaderBelowDivider$"),

                TableView.AtLeftOf(this),
                TableView.AtRightOf(this),
                TableView.Below(HeaderLabel),

                LonelyLabel.AtLeftOf(this),
                LonelyLabel.AtRightOf(this),
                LonelyLabel.Below(TableView),
                LonelyLabel.AtBottomOf(this, 5f)
            );
        }

        private UIView CreateDivider() {
            var border = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = Resources.Colors.Border
            };

            border.AddConstraints(
                border.Height().EqualTo(.5f)
            );

            return border;
        }

        private MSLLabel CreateRelatedHeaderLabel(string text) {
            return new MSLLabel {
                Text = text,
                TextColor = UIColor.White,
                Font = Resources.Fonts.FontNormal,
                ContentEdgeInsets = new UIEdgeInsets(5, HorizontalMargin, 5, HorizontalMargin),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
        }

        private MslFixedTableView CreatedRelatedTableView(Func<UITableView, MslTableViewSource> getSource) {
            var tableView = new MslFixedTableView {
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var viewSource = getSource(tableView);

            tableView.Source = viewSource;
            tableView.ReloadData();

            return tableView;
        }

        private MSLLabel CreateRelatedLonelyLabel(string text = null) {
            return new MSLLabel {
                Text = text,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                ContentEdgeInsets = new UIEdgeInsets(5f, 15f, 10f, 15f),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
        }
    }
}