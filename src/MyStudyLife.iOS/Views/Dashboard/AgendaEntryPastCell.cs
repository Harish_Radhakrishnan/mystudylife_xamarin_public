using System;
using Cirrious.FluentLayouts.Touch;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.Scheduling;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    [Register(nameof(AgendaEntryPastCell))]
    public class AgendaEntryPastCell : AgendaEntryTableViewCellBase {
        public const float Height = 44f + (VERTICAL_SPACING * 2f);

        public override float DesiredHeight => Height;
        public override float TimeLabelCenterOffset => .5f;

        public AgendaEntryPastCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            base.InitializeImpl();

            ContentView.Alpha = 0.5f;

            var titleLabel = new UILabel {
                AdjustsFontSizeToFitWidth = false,
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal)
            };
            ContentView.Add(titleLabel);

            ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            ContentView.AddConstraints(
                titleLabel.AtLeftOf(ContentView, PrimaryContentLeftOffset),
                titleLabel.AtRightOf(ContentView),
                titleLabel.WithSameCenterY(ContentView)
            );

            this.DelayBind(() => {
                var set = this.CreateBindingSet<AgendaEntryPastCell, AgendaEntry>();

                set.Bind(titleLabel).To(x => x.Title);

                set.Apply();
            });
        }
    }
}