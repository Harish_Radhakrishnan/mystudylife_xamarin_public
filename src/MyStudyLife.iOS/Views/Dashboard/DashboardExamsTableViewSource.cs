using System;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.Models;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    public class DashboardExamsTableViewSource : DashboardGroupedTableViewSource<DateTime, ExamModel> {
        public DashboardExamsTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) { }

        public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, nint section) {
            var group = (ExamModelGrouping)GetGroupAt((int)section);

            UIColor textColor = Resources.Colors.SubtleText;

            switch (group.Kind) {
                case ExamGroupingKind.Days3:
                    textColor = Resources.Colors.AttentionForeground;
                    break;
                case ExamGroupingKind.Days7:
                    textColor = Resources.Colors.WarningForeground;
                    break;
            }

            ((MSLTableHeaderView)headerView).TextColor = textColor;
        }
    }
}