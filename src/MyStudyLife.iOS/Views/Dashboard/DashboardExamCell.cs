using System;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Models;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    [UsedImplicitly]
    [Register(nameof(DashboardExamCell))]
    [MslTableViewCell(Height = Height)]
    public class DashboardExamCell : ExamTableViewCell {
        public new const float Height = 64f + (BorderedTableViewCellBase.VERTICAL_SPACING * 2f);

        private UIView _contentView;
        private UIView _revisionTasksView;
        private UILabel _revisionTasksLabel;

        public override UIView ContentView => _contentView;

        public DashboardExamCell(IntPtr handle) : base(handle) { }

        #region Highlighted

        public override void SetHighlighted(bool highlighted, bool animated) {
            if (highlighted) {
                ContentView.BackgroundColor = Resources.Colors.Subtle;
            }
            else {
                Animate(0.3f, () => ContentView.BackgroundColor = UIColor.White);
            }
        }

        #endregion

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            // We override ContentView hence not using .Subviews[0]
            ContentView.Frame =
                BorderedTableViewCellBase.DefaultContentEdgeInsets.InsetRect(new CGRect(0, 0, Bounds.Width, Height));
        }

        protected override void Initialize() {
            BackgroundColor = UIColor.Clear;

            base.ContentView.Add(_contentView = new UIBorderedView {
                BorderColor = Resources.Colors.BorderLight,
                BorderThickness = new Thickness(0, .5f)
            });

            base.Initialize();
        }

        protected override void InitializeImpl() {
            base.InitializeImpl();

            _revisionTasksView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _revisionTasksView.SetContentCompressionResistancePriority((float) UILayoutPriority.DefaultHigh,
                UILayoutConstraintAxis.Horizontal);
            ContentView.Add(_revisionTasksView);

            var taskIcon = new UIImageView(UIImage.FromBundle("Checkbox.png")) {
                ContentMode = UIViewContentMode.ScaleAspectFit,
                ClipsToBounds = true,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _revisionTasksView.Add(taskIcon);

            _revisionTasksLabel = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontSmall
            };
            _revisionTasksView.Add(_revisionTasksLabel);

            ContentView.AddConstraints(
                _revisionTasksView.Left().GreaterThanOrEqualTo().RightOf(SubTitleLabel).Plus(5f),
                _revisionTasksView.WithSameCenterY(SubTitleLabel),
                _revisionTasksView.WithSameRight(RightLabel),
                taskIcon.AtTopOf(_revisionTasksView),
                taskIcon.AtBottomOf(_revisionTasksView),
                taskIcon.AtRightOf(_revisionTasksView),
                taskIcon.Width().EqualTo(13),
                taskIcon.Height().EqualTo(12),
                _revisionTasksLabel.AtLeftOf(_revisionTasksView),
                _revisionTasksLabel.ToLeftOf(taskIcon, 2),
                _revisionTasksLabel.WithSameCenterY(taskIcon)
            );
        }

        protected override void DoBind() {
            var set = this.CreateBindingSet<ExamTableViewCell, ExamModel>();

            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).To(x => x.SubTitle);
            set.Bind(RightLabel).To(x => x.DurationText);
            set.Bind(_revisionTasksView).Visibility().To(x => x.Tasks);
            set.Bind(_revisionTasksLabel).To(x => x.TasksText);
            set.Bind(ColorIndicator)
                .For("LayerBackgroundColor")
                .To(x => x.Color)
                .WithConversion(Resources.Converters.NativeColor);

            set.Apply();
        }
    }
}