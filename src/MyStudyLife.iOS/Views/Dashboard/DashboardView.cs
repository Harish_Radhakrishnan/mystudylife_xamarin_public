using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using iOSCharts;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Converters;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using UIKit;
using static MyStudyLife.iOS.Resources;

namespace MyStudyLife.iOS.Views.Dashboard {
    [Register(nameof(DashboardView))]
    [MvxChildPresentation]
    public class DashboardView : MslViewController<DashboardViewModel> {
        /// <summary>
        ///     The padding that the Pie chart library adds internally
        ///     that we can't change :-(
        /// </summary>
        private const float PIE_CHART_PADDING = 18f;

        private UIView _tabsView;
        private DashboardTab _examsTab;
        private MSLPageViewController _pager;
        private FixedUIPageViewControllerDataSource _pagerDataSource;
        private UIViewController _todayPage;
        private UIViewController _examsPage;
        private UIView _todayContentView;
        private readonly List<UIScrollView> _scrollViews = new List<UIScrollView>();

        public override string Title => Strings.Dashboard;

        public override void ViewDidLoad() {
            View.BackgroundColor = Colors.Background;

            base.ViewDidLoad();

            ParentViewController.NavigationItem.SetRightBarButtonItem(
                new UIBarButtonItem(UIImage.FromBundle("NewTaskIcon.png"), UIBarButtonItemStyle.Plain, (s, e) => {}) {
                    Width = 35
                }.WithCommand(ViewModel.NewTaskCommand),
                true
            );

            #region Creation

            var tabsView = _tabsView = new DashboardTabset {
                BackgroundColor = UIColor.White
            };
            View.Add(tabsView);

            var todayTab = new DashboardTab(Glyph.Today) {
                Selected = true
            };
            tabsView.Add(todayTab);

            var tasksTab = new DashboardTab(Glyph.Task);
            tabsView.Add(tasksTab);

            _examsTab = new DashboardTab(Glyph.Exam);
            tabsView.Add(_examsTab);

            foreach (var tabView in tabsView.Subviews.OfType<UIButton>()) {
                tabView.TouchUpInside += TabViewOnTouchUpInside;
            }

            var pagerDataSource = _pagerDataSource = new FixedUIPageViewControllerDataSource(
                 _todayPage = CreateTodayPage(),
                 CreateTasksPage(),
                 _examsPage = CreateExamsPage()
             );
            var pager = _pager = new MSLPageViewController(UIPageViewControllerTransitionStyle.Scroll, UIPageViewControllerNavigationOrientation.Horizontal) {
                 DataSource = pagerDataSource
            };
            pager.CurrentIndexChanged += (s, e) => {
                 for (int i = 0; i < tabsView.Subviews.Length; i++) {
                     ((UIControl)tabsView.Subviews[i]).Selected = (i == e.NewIndex);
                 }
            };
            
            pager.SetViewControllers(
                 new[] {
                     pagerDataSource.ViewControllers.First()
                 },
                 UIPageViewControllerNavigationDirection.Forward,
                 false,
                 null
            );

            pager.WillMoveToParentViewController(this);
            AddChildViewController(pager);
            pager.DidMoveToParentViewController(this);
            
            View.Add(pager.View);

            View.BringSubviewToFront(tabsView);

            #endregion

            #region Binding

            ViewModelListener
                .Listen(nameof(ViewModel.ShowTodayLonely), SetTodayLonelyState)
                .Listen(nameof(ViewModel.ShowExamsTab), SetExamsPageState);

            OnViewModelLoaded(() => {
                SetTodayLonelyState();
                SetExamsPageState();
            });
            
            var set = this.CreateBindingSet<DashboardView, DashboardViewModel>();

            set.Bind(todayTab.PieChartView).For("DialData").To(x => x.TodayDial);
            set.Bind(todayTab.PieChartView).For(x => x.CenterText).To(x => x.TodayDial.Sum).WithFallback(0);
            set.Bind(tasksTab.PieChartView).For("DialData").To(x => x.TasksDial);
            set.Bind(tasksTab.PieChartView).For(x => x.CenterText).To(x => x.TasksDial.Sum).WithFallback(0);
            set.Bind(_examsTab.PieChartView).For("DialData").To(x => x.ExamsDial);
            set.Bind(_examsTab.PieChartView).For(x => x.CenterText).To(x => x.ExamsDial.Sum).WithFallback(0);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                tabsView.AtTopOf(View),
                tabsView.FullWidthOf(View),
                tabsView.Height().EqualTo(56),

                pager.View.FullWidthOf(View),
                pager.View.Below(tabsView),
                pager.View.AtBottomOf(View)
            );

            #endregion
        }

        private void TabViewOnTouchUpInside(object sender, EventArgs e) {
            if (_pager == null || _pagerDataSource == null) {
                return;
            }

            var tabView = (UIButton)sender;
            int idx = tabView.Superview.Subviews.FirstIndexOf(x => x.Equals(tabView));

            var viewController = _pagerDataSource.ViewControllers.ElementAt(idx);

            if (idx != _pager.CurrentIndex) {
                _pager.SetViewControllers(
                    new[] {
                        viewController
                    },
                    idx < _pager.CurrentIndex
                        ? UIPageViewControllerNavigationDirection.Reverse
                        : UIPageViewControllerNavigationDirection.Forward,
                    true,
                    null
                );
            }
        }

        #region Today Page

        private UIViewController CreateTodayPage() {
            #region Creation

            var scrollView = new UIScrollView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _scrollViews.Add(scrollView);

            // View inside scrollview to enable us to
            // easily set content height to fill screen using constraints
            var contentView = new UIStackView {
                Alignment = UIStackViewAlignment.Fill,
                Axis = UILayoutConstraintAxis.Vertical,
                Distribution = UIStackViewDistribution.Fill,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            _todayContentView = contentView;
            scrollView.Add(contentView);

            var todayLabel = new MSLLabel {
                Text = Strings.Common.Today
            }.WithStyle("SectionHeader");
            contentView.AddArrangedSubview(todayLabel);

            var agendaContentView = new UIView();
            contentView.AddArrangedSubview(agendaContentView);

            var agendaTableView = new UIFixedTableView {
                BackgroundColor = UIColor.Clear,
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false,
                ClipsToBounds = false
            };
            agendaContentView.Add(agendaTableView);

            var agendaSource = new AgendaTableViewSource(agendaTableView) {
                UseAnimations = false
            };

            agendaTableView.Source = agendaSource;
            agendaTableView.ReloadData();

            var agendaTimelineView = new AgendaTimelineView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            agendaContentView.Add(agendaTimelineView);

            var agendaLonelyView = new MslLonelyView(Glyph.Holiday) {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            agendaContentView.Add(agendaLonelyView);

            var nextDayContentView = new UIView();
            contentView.AddArrangedSubview(nextDayContentView);

            var nextDayLabel = new MSLMultiLengthLabel {
                TranslatesAutoresizingMaskIntoConstraints = false
            }.WithStyle("SectionHeader");
            nextDayContentView.Add(nextDayLabel);

            var nextDayPieChartsView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            nextDayContentView.Add(nextDayPieChartsView);

            var nextDayClassesPieChartView = CreatePieChartWithDefaultStyles();
            nextDayPieChartsView.Add(nextDayClassesPieChartView);

            var nextDayTasksPieChartView = CreatePieChartWithDefaultStyles();
            nextDayPieChartsView.Add(nextDayTasksPieChartView);

            var nextDayExamsPieChartView = CreatePieChartWithDefaultStyles();
            nextDayPieChartsView.Add(nextDayExamsPieChartView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<DashboardView, DashboardViewModel>();

            set.Bind(todayLabel).For(x => x.Text).To(x => x.CurrentDayTitle);
            set.Bind(todayLabel).InvertedVisibility().To(x => x.ShowTodayLonely);

            set.Bind(agendaSource).To(x => x.EntriesView.View);
            set.Bind(agendaSource).For(x => x.RowClickCommand).To(x => x.ViewEntryCommand);

            set.Bind(agendaTimelineView).For(x => x.Offset).To(x => x.TimelineOffset);
            set.Bind(agendaTimelineView).Visibility().To(x => x.ShowTimeline);
            set.Bind(agendaTimelineView).For(x => x.TimelineColor).To(x => x.TimelineColor).WithConversion(Resources.Converters.NativeColor);
            set.Bind(agendaTimelineView).For(x => x.TimelineText).To(x => x.TimelineText);
            set.Bind(agendaTimelineView).For(x => x.DurationIconVisible).To(x => x.HasCurrentEntry);

            set.Bind(agendaLonelyView).For(x => x.Text).To(x => x.TodayLonely);
            set.Bind(agendaLonelyView).For(x => x.Glyph).To(x => x.TodayLonelyGlyph);
            set.Bind(agendaLonelyView).For(x => x.SubText).To(x => x.TodayLonelySubTitle);
            set.Bind(agendaLonelyView).Visibility().To(x => x.ShowTodayLonely);

            set.Bind(nextDayContentView).Visibility().To(x => x.NextDay);
            set.Bind(nextDayLabel).For(x => x.MultiLengthText).To(x => x.NextDayTitle);

            set.Bind(nextDayClassesPieChartView).For("DialData").To(x => x.NextDayClassesDial);
            set.Bind(nextDayClassesPieChartView)
                .For(x => x.CenterAttributedText)
                .To(x => x.NextDayClassesDial.Sum)
                .WithConversion(new NextDayCenterTextValueConverter("Class{0:;es}"));
            set.Bind(nextDayTasksPieChartView).For("DialData").To(x => x.NextDayTasksDial);
            set.Bind(nextDayTasksPieChartView)
                .For(x => x.CenterAttributedText)
                .To(x => x.NextDayTasksDial.Sum)
                .WithConversion(new NextDayCenterTextValueConverter("Task{0:;s} due"));
            set.Bind(nextDayExamsPieChartView).For("DialData").To(x => x.NextDayExamsDial);
            set.Bind(nextDayExamsPieChartView)
                .For(x => x.CenterAttributedText)
                .To(x => x.NextDayExamsDial.Sum)
                .WithConversion(new NextDayCenterTextValueConverter("Exam{0:;s}"));

            set.Apply();

            #endregion

            #region Layout

            contentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            var rootView = new UIView {
                scrollView
            };

            const float chartMargin = 10f;
            
            rootView.AddConstraints(
                scrollView.Fill(rootView),
                contentView.FullWidthOf(rootView),
                contentView.AtTopOf(scrollView),
                contentView.AtBottomOf(scrollView),

                agendaTableView.FullWidthOf(agendaContentView),
                agendaTableView.AtTopOf(agendaContentView),
                agendaTableView.Bottom().LessThanOrEqualTo().BottomOf(agendaContentView),

                agendaTimelineView.AtLeftOf(agendaContentView),
                agendaTimelineView.FullHeightOf(agendaTableView),

                nextDayLabel.FullWidthOf(nextDayContentView),
                nextDayLabel.AtTopOf(nextDayContentView, 10f),

                nextDayPieChartsView.Below(nextDayLabel, 5f),
                nextDayPieChartsView.FullWidthOf(nextDayContentView),
                nextDayPieChartsView.AtBottomOf(nextDayContentView, Dimens.DefaultHorizontalMargin),

                nextDayClassesPieChartView.AtLeftOf(nextDayPieChartsView, Dimens.DefaultHorizontalMargin - PIE_CHART_PADDING),
                // Don't expand more than 100f
                nextDayClassesPieChartView
                    .Width()
                    .EqualTo(84f + (PIE_CHART_PADDING * 2))
                    .SetPriority(UILayoutPriority.DefaultLow),
                // Otherwise be 1/3 of the view, excluding the padding we can't remove
                nextDayClassesPieChartView
                    .Width()
                    .LessThanOrEqualTo()
                    .WidthOf(nextDayContentView)
                    .WithMultiplier(1 / 3f)
                    // Padding seems to be constant
                    .Plus((PIE_CHART_PADDING * 2f) - (((chartMargin + Dimens.DefaultHorizontalMargin) * 2f) / 3f))
                    .SetPriority(UILayoutPriority.Required),
                // It's square
                nextDayClassesPieChartView.Height().EqualTo().WidthOf(nextDayClassesPieChartView),
                nextDayClassesPieChartView.AtTopOf(nextDayPieChartsView, -PIE_CHART_PADDING),
                nextDayClassesPieChartView.AtBottomOf(nextDayPieChartsView, -PIE_CHART_PADDING),

                nextDayTasksPieChartView.ToRightOf(nextDayClassesPieChartView, chartMargin - (PIE_CHART_PADDING * 2)),
                // Use first chart as reference
                nextDayTasksPieChartView.Width().EqualTo().HeightOf(nextDayClassesPieChartView),
                nextDayTasksPieChartView.Height().EqualTo().WidthOf(nextDayClassesPieChartView),
                nextDayTasksPieChartView.AtTopOf(nextDayPieChartsView, -PIE_CHART_PADDING),
                nextDayTasksPieChartView.AtBottomOf(nextDayPieChartsView, -PIE_CHART_PADDING),

                nextDayExamsPieChartView.ToRightOf(nextDayTasksPieChartView, chartMargin - (PIE_CHART_PADDING * 2)),
                // Use first chart as reference
                nextDayExamsPieChartView.Width().EqualTo().HeightOf(nextDayClassesPieChartView),
                nextDayExamsPieChartView.Height().EqualTo().WidthOf(nextDayClassesPieChartView),
                nextDayExamsPieChartView.AtTopOf(nextDayPieChartsView, -PIE_CHART_PADDING),
                nextDayExamsPieChartView.AtBottomOf(nextDayPieChartsView, -PIE_CHART_PADDING)
            );

            #endregion

            return new UIViewController {
                View = rootView
            };
        }

        private void SetTodayLonelyState() {
            if (_todayContentView == null) {
                return;
            }

            bool isLonely = ViewModel?.ShowTodayLonely ?? true;

            const string constraintIdentifier = "$TodayContentView_FillHeight$";

            var constraints = _todayPage.View.Constraints.Where(x => x.GetIdentifier() == constraintIdentifier);

            if (constraints.Any()) {
                constraints.Apply(x => x.Active = isLonely);
            }
            else if (isLonely) {
                _todayPage.View.AddConstraints(
                    _todayContentView
                        .FullHeightOf(_todayPage.View)
                        .Select(x => x.WithIdentifier(constraintIdentifier))
                );
            }
        }

        #endregion

        private UIViewController CreateTasksPage() {
            #region Creation

            var rootView = new UIView();

            var tableView = new UITableView {
                BackgroundColor = UIColor.Clear,
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            rootView.Add(tableView);

            var viewSource = new DashboardTasksTableViewSource(tableView, typeof(TaskBorderedTableViewCell));

            tableView.Source = viewSource;
            tableView.ReloadData();

            var lonelyView = new MslLonelyView(Glyph.Task);
            rootView.Add(lonelyView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<DashboardView, DashboardViewModel>();

            set.Bind(viewSource).To(x => x.TasksGroupedViewSource.View);
            set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Bind(lonelyView).For(x => x.Text).To(x => x.TasksLonely);
            set.Bind(lonelyView).InvertedVisibility().To(x => x.TasksGroupedViewSource.Count);

            set.Apply();

            #endregion

            #region Layout

            rootView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            rootView.AddConstraints(
                tableView.Fill(rootView),

                lonelyView.WithSameCenterX(rootView),
                lonelyView.WithSameCenterY(rootView)
            );

            #endregion

            return new UIViewController {
                View = rootView
            };
        }

        private UIViewController CreateExamsPage() {
            #region Creation

            var rootView = new UIView();

            var tableView = new UITableView {
                BackgroundColor = UIColor.Clear,
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            rootView.Add(tableView);

            var viewSource = new DashboardExamsTableViewSource(tableView, typeof(DashboardExamCell));

            tableView.Source = viewSource;
            tableView.ReloadData();

            var lonelyView = new MslLonelyView(Glyph.Exam);
            rootView.Add(lonelyView);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<DashboardView, DashboardViewModel>();

            set.Bind(viewSource).To(x => x.ExamsGroupedViewSource.View);
            set.Bind(viewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Bind(lonelyView).For(x => x.Text).To(x => x.ExamsLonely);
            set.Bind(lonelyView).InvertedVisibility().To(x => x.Exams.Count);

            set.Apply();

            #endregion

            #region Layout

            rootView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            rootView.AddConstraints(
                tableView.Fill(rootView),

                lonelyView.WithSameCenterX(rootView),
                lonelyView.WithSameCenterY(rootView)
            );

            #endregion

            return new UIViewController {
                View = rootView
            };
        }


        private void SetExamsPageState() {
            if (_pager == null) {
                return;
            }

            var vm = ViewModel;
            bool visible = vm == null || vm.ShowExamsTab;
            bool isLoaded = vm != null && vm.IsLoaded;

            if (visible) {
                if (!_pagerDataSource.ViewControllers.Contains(_examsPage)) {
                    _pagerDataSource.AddViewController(_examsPage);
                }

                if (!_tabsView.Subviews.Contains(_examsTab)) {
                    _tabsView.Add(_examsTab);
                }
            }
            else {
                int examsPageIndex = _pagerDataSource.GetViewControllerIndex(_examsPage);

                _pagerDataSource.RemoveViewController(_examsPage);
                _examsTab.RemoveFromSuperview();

                // We have to manually change the current page if we were viewing
                // the exams page. (This could be caused by a sync or delayed load.)
                if (_pager.CurrentIndex == examsPageIndex) {
                    _pager.SetViewControllers(
                        new[] {
                            _pagerDataSource.ViewControllers.ElementAt(Math.Max(0, examsPageIndex - 1))
                        },
                        UIPageViewControllerNavigationDirection.Reverse,
                        false,
                        null
                    );
                }
            }
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            base.SizeRootViewImpl(rect);
            
            _scrollViews.Apply(x => x.Frame = _pager.View.Bounds);
        }

        public override void ViewDidLayoutSubviews() {
            base.ViewDidLayoutSubviews();

            _scrollViews.Apply(x => x.Frame = _pager.View.Bounds);
        }

        public static PieChartView CreatePieChartWithDefaultStyles() => new PieChartView {
            HoleRadiusPercent = .9f,
            TransparentCircleRadiusPercent = 0f,
            HoleColor = UIColor.Clear,
            Legend = {
                Enabled = false
            },
            // These two are required to remove default text,
            // NoDataText shows up before the data has loaded
            // in to the dials and Descriptions is part of the
            // legend.
            NoDataText = String.Empty,
            //DescriptionText = String.Empty,
            CenterAttributedText = new NSMutableAttributedString("0", foregroundColor: Colors.Foreground),
            UserInteractionEnabled = false,
            TranslatesAutoresizingMaskIntoConstraints = false
        };

        class DashboardTabset : UIBorderedView {
            public DashboardTabset() {
                BorderColor = Colors.Border;
                BorderThickness = new Thickness(0, 0, 0, .5f);
                BorderDrawingMode = BorderDrawingMode.Outside;
            }

            public override void LayoutSubviews() {
                base.LayoutSubviews();

                var tabCount = Subviews.Count();
                var tabWidth = Bounds.Width / (float) tabCount;

                for (int i = 0; i < Subviews.Length; i++) {
                    var subview = Subviews[i];

                    subview.Frame = new CGRect(i * tabWidth, 0, tabWidth, Bounds.Height);
                }
            }
        }

        class DashboardTab : UIButton {
            private readonly MSLIcon _iconView;
            private readonly PieChartView _pieChartView;

            public PieChartView PieChartView => _pieChartView;

            /// <summary>
            ///     Sets the <see cref="PieChartView.CenterText"/>
            ///     property via <see cref="PieChartView.CenterAttributedText"/>
            ///     enabling us to use our styling with binding.
            /// </summary>
            public string PieChartCenterText {
                get => _pieChartView.CenterAttributedText?.ToString();
                set => _pieChartView.CenterAttributedText = new NSMutableAttributedString(value, foregroundColor: Colors.Foreground);
            }

            public override bool Selected {
                get => base.Selected;
                set {
                    base.Selected = value;

                    _iconView.TextColor = value ? Colors.TabForegroundSelected : Colors.TabForeground;
                }
            }

            public DashboardTab(Glyph glyph) {
                _iconView = new MSLIcon(glyph, 1.875f) {
                    TextColor = Colors.TabForeground,
                    UserInteractionEnabled = false,
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                Add(_iconView);

                _pieChartView = CreatePieChartWithDefaultStyles();
                _pieChartView.HoleRadiusPercent = .88f;
                Add(_pieChartView);

                this.AddConstraints(
                    _iconView.Right().EqualTo().CenterXOf(this).Minus(2.5f),
                    _iconView.WithSameCenterY(this),

                    // Sizing of pie chart is wierd, so we actually have to make the view larger
                    // than the tab itself
                    _pieChartView.Left().EqualTo().CenterXOf(this).Minus(12f),
                    _pieChartView.WithSameCenterY(this),
                    _pieChartView.Width().EqualTo().HeightOf(_iconView).Plus(35f),
                    _pieChartView.Height().EqualTo().HeightOf(_iconView).Plus(35f)
                );
            }

            #region Touch Handling

            private bool _touchDown;

            public override void TouchesBegan(NSSet touches, UIEvent evt) {
                base.TouchesBegan(touches, evt);

                _touchDown = true;
                OnTouchStart();
            }

            // Touch released
            public override void TouchesEnded(NSSet touches, UIEvent evt) {
                base.TouchesEnded(touches, evt);

                if (_touchDown) {
                    OnTouchEnd();
                    _touchDown = false;
                }
            }

            // Touch leaves view
            public override void TouchesCancelled(NSSet touches, UIEvent evt) {
                base.TouchesCancelled(touches, evt);

                if (_touchDown) {
                    OnTouchEnd();
                    _touchDown = false;
                }
            }

            protected void OnTouchStart() => _iconView.Alpha = 0.3f;
            protected void OnTouchEnd() => Animate(0.3f, () => _iconView.Alpha = 1f);

            #endregion
        }

        /// <summary>
        ///     Converter that allows neat binding but still applies required
        ///     styles to NSMutableAttributedString.
        /// </summary>
        class NextDayCenterTextValueConverter : MvxValueConverter<int, NSMutableAttributedString> {
            private readonly string _label;
            private readonly bool _labelIsFormat;

            public NextDayCenterTextValueConverter(string label) {
                _label = label;
                _labelIsFormat = label.Contains("{0");
            }

            protected override NSMutableAttributedString Convert(int value, Type targetType, object parameter, CultureInfo culture) {
                var valueStr = value.ToString();
                var label = _labelIsFormat ? String.Format(new PluralFormatProvider(), _label, value) : _label;
                var str = $"{value}\n{label}";

                var attributedStr = new NSMutableAttributedString(str, foregroundColor: Colors.Foreground);
                attributedStr.AddAttributes(
                    new UIStringAttributes {
                        ForegroundColor = Colors.ForegroundMidAlt,
                        Font = Fonts.OfSize(28),
                        ParagraphStyle = new NSMutableParagraphStyle {
                            Alignment = UITextAlignment.Center,
                            LineHeightMultiple = .9f
                        }
                    },
                    new NSRange(0, str.Length)
                );
                attributedStr.AddAttribute(
                    UIStringAttributeKey.Font,
                    Fonts.OfSize(11),
                    new NSRange(str.Length - label.Length, label.Length)
                );

                return attributedStr;
            }
        }
    }
}