using System;
using Foundation;
using MyStudyLife.iOS.Controls.Cells;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    [Register(nameof(AgendaEntryCurrentCell))]
    public class AgendaEntryCurrentCell : AgendaEntryTableViewCell {
        public new const float VERTICAL_SPACING = BorderedTableViewCellBase.VERTICAL_SPACING * 2f;
        public new const float Height = 94f + (VERTICAL_SPACING * 2f);

        public override float DesiredHeight => Height;

        public AgendaEntryCurrentCell(IntPtr handle) : base(handle) { }

        protected override void InitializeImpl() {
            base.InitializeImpl();

            ContentEdgeInsets = new UIEdgeInsets(
                VERTICAL_SPACING,
                0,
                VERTICAL_SPACING,
                0
            );
            BorderColor = Resources.Colors.Border;

            // TODO(iOS): Set color not Alpha
            StartTimeLabel.Alpha = .3f;
            EndTimeLabel.Alpha = .3f;
        }
    }
}