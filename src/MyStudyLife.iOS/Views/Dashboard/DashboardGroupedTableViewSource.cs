using System;
using Foundation;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    public abstract class DashboardGroupedTableViewSource<TKey, TItem> : MslGroupedTableViewSource<TKey, TItem> {
        public DashboardGroupedTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) {
            CellSelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public override UIView GetViewForHeader(UITableView tableView, nint section)
            => new MSLTableHeaderView(tableView, TitleForHeader(tableView, section)) {
                ContentEdgeInsets = new UIEdgeInsets(
                    section == 0 ? 15f : 12.5f,
                    Resources.Dimens.DefaultHorizontalMargin,
                    12.5f,
                    Resources.Dimens.DefaultHorizontalMargin
                )
            };

        public override nfloat GetHeightForHeader(UITableView tableView, nint section) {
            var view = GetViewForHeader(tableView, section);

            view.SizeToFit();

            return view.Frame.Height;
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath) {
            base.WillDisplay(tableView, cell, indexPath);

            // Ensures shadow from card view does not overlap
            cell.Layer.ZPosition = indexPath.Row;
        }
    }
}