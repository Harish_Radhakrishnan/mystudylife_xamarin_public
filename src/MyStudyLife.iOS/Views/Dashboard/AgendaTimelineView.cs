using System;
using System.Collections.Specialized;
using Cirrious.FluentLayouts.Touch;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    [Register(nameof(AgendaTimelineView))]
    public class AgendaTimelineView : UIView {
        private const float ENTRY_VERTICAL_MARGIN = BorderedTableViewCellBase.VERTICAL_SPACING;
        private const float CURRENT_ENTRY_VERTICAL_MARGIN = AgendaEntryCurrentCell.VERTICAL_SPACING;
        private const float CURRENT_ENTRY_HEIGHT = AgendaEntryCurrentCell.Height - (CURRENT_ENTRY_VERTICAL_MARGIN * 2f);
        private const float PAST_ENTRY_HEIGHT_AND_MARGIN = AgendaEntryPastCell.Height;
        private const float TIMELINE_HEIGHT = 20f;

        private UIView _timeline;
        private MSLIcon _icon;
        private UILabel _label;

        private TimelineOffset _offset;

        public TimelineOffset Offset {
            get => _offset;
            set {
                if (!Equals(_offset, value)) {
                    _offset = value;

                    UpdateOffset();
                }
            }
        }

        public UIColor TimelineColor {
            get => _timeline.BackgroundColor;
            set => _timeline.BackgroundColor = value;
        }

        public bool DurationIconVisible {
            get => !_icon.Hidden;
            set => _icon.Hidden = !value;
        }

        public string TimelineText {
            get => _label.Text;
            set => _label.Text = value;
        }

        public AgendaTimelineView() {
            Add(_timeline = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                BackgroundColor = Resources.Colors.Accent
            });

            float timelineWidth = AgendaEntryTableViewCellBase.PrimaryContentLeftOffset - 2f;
            float timelineHeight = TIMELINE_HEIGHT;
            float arrowWidth = timelineHeight / 2f;

            // Draws the arrow
            var path = new CGPath();
            path.MoveToPoint(0, 0);
            path.AddLineToPoint(timelineWidth - arrowWidth, 0);
            path.AddLineToPoint(timelineWidth, timelineHeight / 2f);
            path.AddLineToPoint(timelineWidth - arrowWidth, timelineHeight);
            path.AddLineToPoint(0, timelineHeight);
            path.AddLineToPoint(0, 0);

            _timeline.Layer.Mask = new CAShapeLayer {
                Path = path
            };

            _timeline.Add(_icon = new MSLIcon(Glyph.Stopwatch, 14 / 16f) {
                TextColor = UIColor.White
            });
            _timeline.Add(_label = new MSLLabel {
                TextColor = UIColor.White,
                Font = Resources.Fonts.OfSize(11f, FontWeight.Medium)
            });

            this.AddConstraints(
                _timeline.AtTopOf(this),
                _timeline.AtLeftOf(this),
                _timeline.Width().EqualTo(timelineWidth),
                _timeline.Height().EqualTo(timelineHeight),
                _icon.AtLeftOf(_timeline, Resources.Dimens.ColorStripSize),
                _icon.WithSameCenterY(_timeline),
                _label.ToRightOf(_icon, 2.5f),
                _label.WithSameCenterY(_timeline)
            );
        }

        private void OnEntriesCollectionChange(object sender, NotifyCollectionChangedEventArgs e) {
            UpdateOffset();
        }

        private void UpdateOffset() {
            // In most scenarios, there will only ever be a "part" current entry, that is, an entry
            // with an offset based on the minutes remaining of the current entry
            float wholeCurrentEntries = (float) Math.Floor(_offset.CurrentEntriesPosition);
            float fractionalCurrentEntries = _offset.CurrentEntries - wholeCurrentEntries;

            float offsetY =
                // Ensures offset of timeline starts with the arrow on the top of the first entry
                (TIMELINE_HEIGHT / -2f) +
                // Offset by each past entry and any margin
                (_offset.PastEntries * PAST_ENTRY_HEIGHT_AND_MARGIN) +
                // Where there are multiple, offset by any current entries and their margins
                (wholeCurrentEntries * (CURRENT_ENTRY_HEIGHT + (CURRENT_ENTRY_VERTICAL_MARGIN * 2))) +
                // Offset by chosen current entry margin
                (fractionalCurrentEntries * CURRENT_ENTRY_VERTICAL_MARGIN) +
                // Offset by position within current entry
                ((_offset.CurrentEntriesPosition - wholeCurrentEntries) * CURRENT_ENTRY_HEIGHT) +
                // Offset to align with top of future entry if any (and no current entry)
                ((wholeCurrentEntries <= 0f && _offset.FutureEntries > 0) ? ENTRY_VERTICAL_MARGIN : 0);

            Animate(0.3f, () => _timeline.Transform = CGAffineTransform.MakeTranslation(0, offsetY));
        }
    }
}