using System;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.Data;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard {
    public class DashboardTasksTableViewSource : DashboardGroupedTableViewSource<int, Task> {
        public DashboardTasksTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) { }

        public override void WillDisplayHeaderView(UITableView tableView, UIView headerView, nint section) {
            var group = (TaskGrouping)GetGroupAt((int)section);

            UIColor textColor = Resources.Colors.SubtleText;

            switch (group.Kind) {
                case TaskGroupingKind.Overdue:
                    textColor = Resources.Colors.AttentionForeground;
                    break;
                case TaskGroupingKind.DueSoon:
                    textColor = Resources.Colors.WarningForeground;
                    break;
            }

            ((MSLTableHeaderView)headerView).TextColor = textColor;
        }
    }
}