using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Foundation;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.WeakSubscription;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.Scheduling;
using UIKit;

namespace MyStudyLife.iOS.Views.Dashboard
{
    public class AgendaTableViewSource : MvxTableViewSource {
        private List<IDisposable> _agendaEntrySubscriptions;

        public ICommand RowClickCommand { get; set; }

        public AgendaTableViewSource(UITableView tableView) : base(tableView) {
            tableView.RegisterClassForCellReuse(typeof(AgendaEntryCurrentCell), new NSString(nameof(AgendaEntryCurrentCell)));
            tableView.RegisterClassForCellReuse(typeof(AgendaEntryTableViewCell), new NSString(nameof(AgendaEntryTableViewCell)));
            tableView.RegisterClassForCellReuse(typeof(AgendaEntryPastCell), new NSString(nameof(AgendaEntryPastCell)));
        }

        public override void ReloadTableData() {
            base.ReloadTableData();

            SubscribeToStateChange();
        }

        protected override void CollectionChangedOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args) {
            base.CollectionChangedOnCollectionChanged(sender, args);

            SubscribeToStateChange();
        }

        private void SubscribeToStateChange() {
            _agendaEntrySubscriptions?.Apply(x => x.Dispose());
            _agendaEntrySubscriptions = ItemsSource
                ?.Cast<AgendaEntry>()
                .Select(x => (IDisposable)x.WeakSubscribe(AgendaEntryOnPropertyChanged))
                .ToList();
        }

        private void AgendaEntryOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            try {
                if (e.PropertyName == nameof(AgendaEntry.State)) {
                    int index = ItemsSource?.Cast<AgendaEntry>().FirstIndexOf(x => ReferenceEquals(x, sender)) ?? -1;

                    if (0 <= index) {
                        var indexPath = NSIndexPath.FromRowSection(index, 0);
                        var cell = TableView.CellAt(indexPath);
                        var newCellType = CellTypeForState(((AgendaEntry)sender).State);

                        if (cell != null && !Equals(cell.GetType(), newCellType)) {
                            TableView.ReloadRows(new[] {
                                indexPath
                            }, UITableViewRowAnimation.Automatic);
                        }
                    }
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<AgendaTableViewSource>>().LogWarning($"Masked exception during {nameof(AgendaEntryOnPropertyChanged)}: {ex.ToLongString()}");
            }
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath) {
            // Ensures shadow from card view does not overlap
            cell.Layer.ZPosition = indexPath.Row;

            // For some reason this has to be set here, not within our cell class
            cell.SelectionStyle = UITableViewCellSelectionStyle.None;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
            var ae = (AgendaEntry)GetItemAt(indexPath);

            switch (ae.State) {
                case AgendaEntryState.Past:
                    return AgendaEntryPastCell.Height;
                case AgendaEntryState.Current:
                    return AgendaEntryCurrentCell.Height;
                default:
                    return AgendaEntryTableViewCell.Height;

            }
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
            => tableView.DequeueReusableCell(CellIdentifierForState(((AgendaEntry)item).State));

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath) {
            var item = GetItemAt(indexPath);
            var rowClickCommand = RowClickCommand;

            if (item != null && rowClickCommand != null) {
                rowClickCommand.Execute(item);

                tableView.DeselectRow(indexPath, false);
            }
            else base.RowSelected(tableView, indexPath);
        }

        private string CellIdentifierForState(AgendaEntryState state)
            => CellTypeForState(state).Name;

        private Type CellTypeForState(AgendaEntryState state) {
            switch (state) {
                case AgendaEntryState.Past:
                    return typeof(AgendaEntryPastCell);
                case AgendaEntryState.Current:
                    return typeof(AgendaEntryCurrentCell);
                default:
                    return typeof(AgendaEntryTableViewCell);
            }
        }
    }
}