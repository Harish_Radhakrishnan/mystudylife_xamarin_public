using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using CoreText;
using Foundation;
using MvvmCross.Plugin.Color.Platforms.Ios;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Converters;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Views {
    [Register("TaskView")]
    internal class TaskView : EntityView<Task, TaskViewModel> {
        private MvxPropertyChangedListener _listener;

        private Sub _dueDateSub;
        private Sub _doneSub;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.State), ViewModelOnStateChange)
                .Listen(nameof(this.ViewModel.CompletedRelativeText), SetDoneSubText);

            #region Creation

            var subContentView = new OAStackView.OAStackView {
                Axis = UILayoutConstraintAxis.Vertical
            };
            ContentView.Add(subContentView);

            _dueDateSub = CreateSub(subContentView, Glyph.Calendar);

            var progressView = new UIView();
            subContentView.AddArrangedSubview(progressView);

            var icon = new MSLIcon(Glyph.Progress, 2f) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.ForegroundMid
            };
            progressView.Add(icon);

            var progressSlider = new MSLSlider {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Step = 5,
                MinimumValue = 0,
                MaximumValue = 100,
                ThumbOffColor = Resources.Colors.Accent,
                ThumbOnColor = Resources.Colors.Accent,
                TrackOnColor = Resources.Colors.Accent,
                TickMarksColor = UIColor.Clear,
                EnabledValueLabel = true,
                ValueFormatString = "%.f%%"
            };
            progressView.Add(progressSlider);

            var label = new UILabel {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontNormal
            };
            progressView.Add(label);

            progressSlider.OnTouchesBegan += (s, e) => label.Fade(false);
            progressSlider.OnTouchesEnded += (s, e) => label.Fade(true);
            progressSlider.OnTouchesCancelled += (s, e) => label.Fade(true);

            progressView.AddConstraints(
                icon.WithSameLeft(progressView),
                icon.AtTopOf(progressView, 10),
                icon.AtBottomOf(progressView, 10),

                label.ToRightOf(icon, 10),
                label.Right().LessThanOrEqualTo().RightOf(progressView),
                label.AtTopOf(progressView, 10),

                // progressSlider has padding of 16
                progressSlider.ToRightOf(icon, -6),
                progressSlider.AtRightOf(progressView),
                progressSlider.AtBottomOf(progressView, -6)
            );

            var progressSub = new Sub(progressView, label);
            _doneSub = CreateSubWithSubLabel(subContentView, Glyph.Tick, _dueDateSub.View);

            _doneSub.SubLabel.UserInteractionEnabled = true;
            _doneSub.SubLabel.AddGestureRecognizer(new UITapGestureRecognizer(() => this.ViewModel.SetIncompleteCommand.Execute(null)));

            var detailDivider = AddDivider(ContentView, null);

            var detailLabel = new MslAttributedLabel {
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                LinkAttributes = new CTStringAttributes {
                    ForegroundColor = Resources.Colors.Accent.CGColor,
                    UnderlineStyle = CTUnderlineStyle.None
                }.Dictionary
            };
            ContentView.Add(detailLabel);

            var detailLonelyLabel = new MSLLabel {
                TextColor = Resources.Colors.SubtleText,
                Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall),
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                ContentEdgeInsets = new UIEdgeInsets(15f, 15f, 15f, 15f),
                TranslatesAutoresizingMaskIntoConstraints = false,
                UserInteractionEnabled = true
            };
            ContentView.Add(detailLonelyLabel);

            detailLonelyLabel.AddGestureRecognizer(
                new UITapGestureRecognizer(() => this.ViewModel.EditCommand.Execute(null)));

            var detailLonelyText = new NSMutableAttributedString(Resources.Strings.TaskView.DetailLonely);

// ReSharper disable once StringIndexOfIsCultureSpecific.1
            var detailAddSomeRange = new NSRange(
                Resources.Strings.TaskView.DetailLonely.IndexOf(Resources.Strings.TaskView.DetailLonelyAddSome),
                Resources.Strings.TaskView.DetailLonelyAddSome.Length
            );

            Action<UIColor> setDetailAddSomeAttrs = color => {
                detailLonelyText.SetAttributes(new UIStringAttributes {
                    ForegroundColor = color,
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall, FontWeight.Bold)
                }, detailAddSomeRange);
                detailLonelyLabel.AttributedText = detailLonelyText;
            };
            setDetailAddSomeAttrs(Resources.Colors.Accent);

            var dueForClassView = new MSLRelatedItemsView(
                Resources.Strings.TaskView.DueForClass,
                MslTableViewSource.For<ScheduledClassTableViewCell>,
                Resources.Strings.TaskView.DueForClassLonely
            );
            ContentView.Add(dueForClassView);

            var revisionForExamView = new MSLRelatedItemsView(
                Resources.Strings.TaskView.RevisionForExam,
                MslTableViewSource.For<ExamRelatedTableViewCell>,
                Resources.Strings.TaskView.RevisionForExamLonely
            );
            ContentView.Add(revisionForExamView);

            #endregion

            #region Binding

            _listener.Listen(nameof(ViewModel.Color), () => {
                var color = ViewModel.Color?.ToNativeColor();
                SetDoneSubText();
                setDetailAddSomeAttrs(color);
                NavigationController?.NavigationBar?.ApplyAppearance(backgroundColor: color);
            });
            
            var set = this.CreateBindingSet<TaskView, TaskViewModel>();

            set.Bind(HeaderBackgroundView)
                .For(x => x.BackgroundColor).To(x => x.Color)
                .WithConversion(Resources.Converters.NativeColor);
            
            set.Bind(TitleLabel).To(x => x.Title);
            set.Bind(SubTitleLabel).To(x => x.SubTitle);

            set.Bind(_dueDateSub.Label).To(x => x.DueText);
            set.Bind(_dueDateSub.SubLabel).To(x => x.DueRelativeText);
            set.Bind(_dueDateSub.SubLabel)
                .For(x => x.TextColor)
                .To(x => x.StateColor)
                .WithConversion(Resources.Converters.NativeColor)
                .WithFallback(_dueDateSub.SubLabel.TextColor);

            set.Bind(progressSub.View).InvertedVisibility().To(x => x.Item.IsComplete);
            set.Bind(progressSub.Label)
                .To(x => x.Item.Progress)
                .WithConversion(Resources.Converters.StringFormat, Resources.Strings.TaskView.PercentCompleteFormat);
            set.Bind(_doneSub.View).Visibility().To(x => x.Item.IsComplete);
            set.Bind(_doneSub.Label)
                .To(x => x.Item.Progress)
                .WithConversion(Resources.Converters.StringFormat, Resources.Strings.TaskView.PercentCompleteFormat);

            set.Bind(progressSlider).For(x => x.Value).To(x => x.Item.Progress).WithConversion(Resources.Converters.IntToNFloat);
            set.Bind(progressSlider).For(x => x.TintColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(progressSlider).For(x => x.ThumbOnColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(progressSlider).For(x => x.ThumbOffColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(progressSlider).For(x => x.TrackOnColor).To(x => x.Color).WithConversion(Resources.Converters.NativeColor);
            set.Bind(progressSlider).For(x => x.Hidden).To(x => x.Item.IsComplete);

            set.Bind(detailLabel).For("AutoLinkText").To(x => x.Item.Detail);
            set.Bind(detailLabel).For(x => x.LinkForegroundColor).To(x => x.Color).WithConversion(new ColorToCGColorValueConverter());
            set.Bind(detailLabel).Visibility().To(x => x.Item.Detail);
            set.Bind(detailLonelyLabel).InvertedVisibility().To(x => x.Item.Detail);

            set.Bind(dueForClassView.HeaderLabel)
                .For(x => x.TextColor)
                .To(x => x.Color)
                .WithConversion(Resources.Converters.NativeColor);
            set.Bind(dueForClassView.TableView.Source).For(x => x.ItemsSource).To(x => x.ClassForTask);
            set.Bind(dueForClassView.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewScheduledClassCommand);
            set.Bind(dueForClassView.LonelyLabel).InvertedVisibility().To(x => x.ClassForTask.Count);

            set.Bind(revisionForExamView).Visibility().To(x => x.Item.IsRevision);
            set.Bind(revisionForExamView.HeaderLabel)
                .For(x => x.TextColor)
                .To(x => x.Color)
                .WithConversion(Resources.Converters.NativeColor);
            set.Bind(revisionForExamView.TableView.Source).For(x => x.ItemsSource).To(x => x.ExamForTask);
            set.Bind(revisionForExamView.TableView.Source).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);
            set.Bind(revisionForExamView.LonelyLabel).InvertedVisibility().To(x => x.ExamForTask.Count);

            set.Apply();

            #endregion

            #region Layout

            ContentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                subContentView.AtTopOf(ContentView),
                subContentView.AtLeftOf(ContentView, HorizontalMargin),
                subContentView.AtRightOf(ContentView, HorizontalMargin),

                detailDivider.Below(subContentView, 10f),

                detailLabel.AtLeftOf(View, 15f),
                detailLabel.AtRightOf(View, 15f),
                detailLabel.Below(detailDivider, 15f),

                detailLonelyLabel.WithSameLeft(View),
                detailLonelyLabel.WithSameRight(View),
                detailLonelyLabel.Below(detailDivider),

                dueForClassView.WithSameLeft(View),
                dueForClassView.WithSameRight(View),
                dueForClassView.Top().GreaterThanOrEqualTo().BottomOf(detailLabel).Plus(15f),
                dueForClassView.Top().GreaterThanOrEqualTo().BottomOf(detailLonelyLabel),

                revisionForExamView.WithSameLeft(View),
                revisionForExamView.WithSameRight(View),
                revisionForExamView.Below(dueForClassView),
                revisionForExamView.WithSameBottom(ContentView)
            );

            #endregion

            ViewModelOnStateChange();
            SetDoneSubText();
        }

        private void ViewModelOnStateChange() {
            var vm = this.ViewModel;
            var sub = this._dueDateSub;

            if (vm != null && sub != null) {
                sub.BoldSubLabel = vm.State == TaskState.Overdue || vm.State == TaskState.IncompleteAndDueSoon;
            }
        }

        private void SetDoneSubText() {
            var vm = this.ViewModel;
            var subLabel = this._doneSub?.SubLabel;

            if (vm == null || subLabel == null) {
                return;
            }

            var text = vm.CompletedRelativeText;

            if (text != null) {
                var setIncompleteText = new NSMutableAttributedString($"{text} ({Resources.Strings.TaskView.SetIncomplete})");

                // ReSharper disable once StringIndexOfIsCultureSpecific.1
                var setIncompleteRange = new NSRange(text.Length + 2, Resources.Strings.TaskView.SetIncomplete.Length);

                setIncompleteText.SetAttributes(new UIStringAttributes {
                    ForegroundColor = vm.Color?.ToNativeColor(),
                    Font = Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeSmall, FontWeight.Medium)
                }, setIncompleteRange);

                subLabel.AttributedText = setIncompleteText;
            }
            else {
                subLabel.Text = String.Empty;
            }
        }
    }
}