using System;
using CoreGraphics;
using Foundation;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.ViewModels;
using MyStudyLife.iOS.Controls;
using MyStudyLife.Scheduling;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Calendar;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    public class CalendarWeekCollectionViewCell : MvxCollectionViewCell {
        public static readonly NSString AgendaEntryCellReuseIdentifier = new NSString("AgendaEntryCellReuseIdentifier");
        public static readonly NSString DayColumnHeaderReuseIdentifier = new NSString("DayColumnHeaderReuseIdentifier");
        public static readonly NSString TimeRowHeaderReuseIdentifier = new NSString("TimeRowHeaderReuseIdentifier");

        private IDisposable _subscription;

        private CalendarWeekLayout _collectionViewLayout;
        private MslCollectionView _collectionView;
        private CalendarWeekHeaderView _dayHeader, _holidayHeader;
        private UILabel[] _dayLabels = new UILabel[7];
        private CalendarWeekHolidayIndicatorView[] _holidayIndicators = new CalendarWeekHolidayIndicatorView[7];

        private CGPoint _scrollOffset;
        private UIEdgeInsets _contentInset;

        public CGPoint ScrollOffset {
            get => _scrollOffset;
            set {
                _scrollOffset = value;

                _collectionView?.SetContentOffset(_scrollOffset, false);
            }
        }
        
        public UIEdgeInsets ContentInset {
            get => _contentInset = SafeAreaInsets;
            set {
                _contentInset = value;

                if (_collectionViewLayout != null) {
                    _collectionViewLayout.ContentInset = value;
                }
        
                if (_dayHeader != null) {
                    _dayHeader.ContentInset = value;
                }
                
                if (_holidayHeader != null) {
                    _holidayHeader.ContentInset = value;
                }
            }
        }

        public new CalendarWeekModel DataContext => (CalendarWeekModel)base.DataContext;

        public CalendarWeekCollectionViewCell(IntPtr handle) : base(handle) {
            Initialize();
        }

        private void Initialize() {

            var set = this.CreateBindingSet<CalendarWeekCollectionViewCell, CalendarWeekModel>();

            #region Creation

            var frame = ContentView.Bounds;

            var collectionViewLayout = _collectionViewLayout = new CalendarWeekLayout();
            var collectionView = _collectionView = new MslCollectionView(frame, collectionViewLayout) {
                BackgroundColor = UIColor.White,
                ShowsVerticalScrollIndicator = false
            };

            collectionView.SetContentOffset(ScrollOffset, false);

            collectionView.RegisterClassForCell(typeof(AgendaEntryCell), AgendaEntryCellReuseIdentifier);

            collectionView.RegisterClassForSupplementaryView(typeof(TimeRowHeader), new NSString(CalendarWeekLayout.ElementKindTimeRowHeader), TimeRowHeaderReuseIdentifier);

            collectionView.CollectionViewLayout.RegisterClassForDecorationView(typeof(CalendarCurrentDayHighlight), new NSString(CalendarWeekLayout.ElementKindCurrentDayHighlight));
            collectionView.CollectionViewLayout.RegisterClassForDecorationView(typeof(CalendarTimeline), new NSString(CalendarWeekLayout.ElementKindTimeline));
            collectionView.CollectionViewLayout.RegisterClassForDecorationView(typeof(CalendarGridline), new NSString(CalendarWeekLayout.ElementKindHorizontalGridLine));
            collectionView.CollectionViewLayout.RegisterClassForDecorationView(typeof(CalendarGridline), new NSString(CalendarWeekLayout.ElementKindVerticalGridLine));

            var collectionViewSource = new CalendarWeekViewSource(collectionView) {
                ItemSelectedCommand = new MvxCommand<AgendaEntry>(Mvx.Resolve<INavigationService>().ViewEntry)
            };

            collectionView.Source = collectionViewSource;

            ContentView.Add(collectionView);

            _dayHeader = new CalendarWeekHeaderView(new CGRect(0, 0, frame.Width, CalendarWeekLayout.DayHeaderHeight)) {
                BackgroundColor = UIColor.FromWhiteAlpha(1f, 0.95f),
                ContentInset = ContentInset
            };
            ContentView.Add(_dayHeader);

            _holidayHeader = new CalendarWeekHolidayHeaderView(new CGRect(0, CalendarWeekLayout.DayHeaderHeight, frame.Width, CalendarWeekLayout.HolidayHeaderHeight)) {
                BackgroundColor = UIColor.FromWhiteAlpha(1f, 0.95f),
                ContentInset = ContentInset
            };
            ContentView.Add(_holidayHeader);

            for (int i = 0; i < 7; i++) {
                var dayLabel = _dayLabels[i] = new UILabel {
                    Font = Resources.Fonts.OfSize(12),
                    TextColor = Resources.Colors.SubtleText
                };
                _dayHeader.Add(dayLabel);

                var holidayIndicator = _holidayIndicators[i] = new CalendarWeekHolidayIndicatorView();
                _holidayHeader.Add(holidayIndicator);

                // Indexed props must be constant, hence described binding
                // http://stackoverflow.com/questions/20777698/binding-to-an-indexed-property-in-mvvmcross
                set.Bind(dayLabel).For(x => x.Text).SourceDescribed("Days[" + i + "].AbbrHeading");
                set.Bind(holidayIndicator).SourceDescribed("Days[" + i + "].IsHoliday").For("Visible");
                set.Bind(holidayIndicator).SourceDescribed("Days[" + i + "].HolidayName").For(x => x.HolidayName);
            }

            #endregion

            #region Binding

            set.Bind(collectionViewSource).To(x => x.Entries);
            set.Bind(_holidayHeader).Visibility().To(x => x.HasHolidays);

            this.DelayBind(set.Apply);

            #endregion

            this.DelayBind(() => {
                _subscription = new MvxPropertyChangedListener(DataContext)
                    .Listen(() => DataContext.InitialTime, OnInitialTimeChanged)
                    .Listen(() => DataContext.HasHolidays, SetHasHolidays)
                    .Listen(() => DataContext.CurrentDayIndex, SetCurrentDayIndex);

                SetHasHolidays();
                SetCurrentDayIndex();
            });

            BindingContext.DataContextChanged += BindingContextOnDataContextChanged;
        }

        public override void PrepareForReuse() {
            if (_collectionViewLayout != null) {
                _collectionViewLayout.CurrentDayIndex = -1;
            }

            base.PrepareForReuse();
        }

        private void BindingContextOnDataContextChanged(object sender, EventArgs e) {
            SetHasHolidays();
            SetCurrentDayIndex();
        }

        private bool _initialTimeScrolled;

        private void OnInitialTimeChanged() {
            if (_initialTimeScrolled) return;
            _initialTimeScrolled = true;

            DoFirstScroll();
        }

        private void DoFirstScroll() {
            try {
                var dataContext = DataContext;

                if (dataContext == null || _collectionView?.Source == null) return;
                
                var source = (CalendarWeekViewSource) _collectionView.Source;
                var initialTime = dataContext.InitialTime;

                if (source.UserScrolled || !initialTime.HasValue) return;
                    
                var scrollOffset = new CGPoint(
                    0,
                    (float) initialTime.Value.TotalHours*_collectionViewLayout.HourHeight
                );

                source.DelayRun(() =>
                    _collectionView.SetContentOffset(
                        scrollOffset,
                        true
                    )
                );

                CalendarWeekView.Instance.CurrentScrollOffset = scrollOffset;
            }
// ReSharper disable once RedundantCatchClause, EmptyGeneralCatchClause
            catch {
#if DEBUG
                throw;
#endif
            }
        }

        private void SetHasHolidays() {
            if (_collectionView != null && DataContext != null) {
                ((CalendarWeekLayout)_collectionView.CollectionViewLayout).HasHolidays = DataContext.HasHolidays;
            }
        }

        private void SetCurrentDayIndex() {
            if (_collectionView != null && DataContext != null) {
                int currentDayIndex = DataContext.CurrentDayIndex;

                ((CalendarWeekLayout)_collectionView.CollectionViewLayout).CurrentDayIndex = DataContext.CurrentDayIndex;

                for (int i = 0; i < _dayLabels.Length; i++) {
                    _dayLabels[i].TextColor = i == currentDayIndex
                        ? Resources.Colors.Accent
                        : Resources.Colors.SubtleText;
                }
            }
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            if (_collectionView != null) {
                _collectionView.Frame = new CGRect(
                    0, 0,
                    ContentView.Frame.Width,
                    ContentView.Frame.Height
                );
            }

            if (_dayHeader != null && _holidayHeader != null) {
                _dayHeader.Frame = _dayHeader.Frame.WithWidth(ContentView.Frame.Width);
                _holidayHeader.Frame = _holidayHeader.Frame.WithWidth(ContentView.Frame.Width);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _collectionViewLayout = null;
                _collectionView = null;
                _dayHeader = null;
                _dayLabels = null;
                _holidayHeader = null;
                _holidayIndicators = null;

                if (_subscription != null) {
                    _subscription.Dispose();
                    _subscription = null;
                }
            }

            base.Dispose(disposing);
        }
    }
}