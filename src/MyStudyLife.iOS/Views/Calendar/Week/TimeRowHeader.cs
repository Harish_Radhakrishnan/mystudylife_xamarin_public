using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    class TimeRowHeader : UICollectionReusableView {
        private UILabel _label;

        private static readonly nfloat Height = new NSString("00").StringSize(Resources.Fonts.FontOfSize(12)).Height;

        public string Time {
            get { return _label.Text; }
            set { _label.Text = value; }
        }

        [Export("initWithFrame:")]
        public TimeRowHeader(CGRect frame) : base(frame) {
            Initialize();
        }

        private void Initialize() {
            _label = new UILabel(new CGRect(
                0, 2,
                Frame.Width - 4,
                Height
            )) {
                Font = Resources.Fonts.FontOfSize(12),
                TextAlignment = UITextAlignment.Right,
                TextColor = Resources.Colors.SubtleText
            };
            Add(_label);
        }
    }
}