using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    class CalendarCurrentDayHighlight : UICollectionReusableView {
        [Export("initWithFrame:")]
        public CalendarCurrentDayHighlight(CGRect frame) : base(frame) {
            Initialize();
        }

        private void Initialize() {
            BackgroundColor = Resources.Colors.Accent.ColorWithAlpha(.1f);
        }
    }
}