using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading;
using System.Windows.Input;
using Foundation;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.Globalization;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week
{
    public class CalendarWeekViewSource : MvxCollectionViewSource
    {
        private readonly ILogger _log = Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(CalendarWeekViewSource));

        private bool _userScrolled;

        public bool UserScrolled => _userScrolled;

        public ICommand ItemSelectedCommand { get; set; }

        public CalendarWeekViewSource(UICollectionView collectionView) : base(collectionView) { }

        protected override UICollectionViewCell GetOrCreateCellFor(UICollectionView collectionView, NSIndexPath indexPath, object item)
        {
            return (AgendaEntryCell)collectionView.DequeueReusableCell(CalendarWeekCollectionViewCell.AgendaEntryCellReuseIdentifier, indexPath);
        }

        public override UICollectionReusableView GetViewForSupplementaryElement(UICollectionView collectionView, NSString elementKind, NSIndexPath indexPath)
        {
            string kind = elementKind.ToString();

            switch (kind)
            {
                case CalendarWeekLayout.ElementKindTimeRowHeader:
                    {
                        var header = (TimeRowHeader)collectionView.DequeueReusableSupplementaryView(elementKind, CalendarWeekCollectionViewCell.TimeRowHeaderReuseIdentifier, indexPath);

                        header.Time = DateTimeFormat.Hours[(int)indexPath.Item];

                        return header;
                    }
            }

            return null;
        }

        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            base.ItemSelected(collectionView, indexPath);

            ItemSelectedCommand?.Execute(GetItemAt(indexPath));
        }

        public override void Scrolled(UIScrollView scrollView)
        {
            _userScrolled = true;

            CalendarWeekView.Instance.CurrentScrollOffset = scrollView.ContentOffset;
        }

        private Timer _scheduleReloadDataTimer;
        private bool _reloadDataScheduled;

        protected override void CollectionChangedOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            base.CollectionChangedOnCollectionChanged(sender, e);

            // The view model assumes we can handle individual events being added, replaced or removed. We
            // can't. This means a simple change such as replacing an item would cause 2 calls to ReloadData
            // which causes the app to freeze on older iPhones (such as the 5). The 5ms timer delay is small
            // enough for the user not to notice but long enough that we can ignore future change notifications.
            if (_reloadDataScheduled)
            {
                return;
            }

            _scheduleReloadDataTimer?.Dispose();

            _scheduleReloadDataTimer = new Timer((t) =>
            {
                InvokeOnMainThread(ReloadData);

                _reloadDataScheduled = false;
                _scheduleReloadDataTimer = null;
            }, null, TimeSpan.FromMilliseconds(5), TimeSpan.FromMilliseconds(-1));

            _reloadDataScheduled = true;
        }

        private bool _dataLoaded;
        private List<Action> _delayedActions;

        /// <summary>
        ///     Ensures the given <paramref name="action"/>
        ///     is run
        /// </summary>
        /// <param name="action"></param>
        public void DelayRun(Action action)
        {
            if (_dataLoaded)
            {
                action();
            }
            else
            {
                if (_delayedActions == null)
                {
                    _delayedActions = new List<Action>();
                }

                _delayedActions.Add(action);
            }
        }

        public override void ReloadData()
        {
            ((CalendarWeekLayout)CollectionView.CollectionViewLayout).InvalidateLayoutCache();

            // Calling ReloadData() on a one-section collection doesn't animate
            // http://stackoverflow.com/questions/13272315/uicollectionview-animate-data-change

            CollectionView.PerformBatchUpdates(() =>
            {
                try
                {
                    CollectionView.ReloadSections(NSIndexSet.FromIndex(0));
                }
                catch (Exception ex)
                {
                    // Same as base.ReloadData()
                    _log.Log(LogLevel.Warning, ex, "Exception masked during CollectionView ReloadData");
                }
            }, (greatSuccess) =>
            {
                if (greatSuccess)
                {
                    _dataLoaded = true;

                    if (_delayedActions != null)
                    {
                        foreach (var action in _delayedActions)
                        {
                            action();
                        }
                    }

                    _delayedActions = null;
                }
            });
        }
    }
}