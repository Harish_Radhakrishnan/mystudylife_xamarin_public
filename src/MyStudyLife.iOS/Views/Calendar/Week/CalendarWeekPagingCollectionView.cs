using CoreGraphics;
using MyStudyLife.iOS.Controls;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    class CalendarWeekPagingCollectionView : PagingCollectionView {
        public CalendarWeekPagingCollectionView(CGRect frame, UICollectionViewLayout layout) : base(frame, layout) { }
    }
}