using CoreAnimation;
using CoreGraphics;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    public sealed class CalendarWeekHolidayHeaderView : CalendarWeekHeaderView {
        private const int LineCount = 8;
        private readonly CALayer[] _lines = new CALayer[LineCount];

        public CalendarWeekHolidayHeaderView(CGRect frame) : base(frame) {
            for (int i = 0; i < LineCount; i++) {
                var line = _lines[i] = new CALayer {
                    BackgroundColor = Resources.Colors.Border.CGColor,
                    ZPosition = 999999 // Lines in front of the holiday views
                };

                Layer.AddSublayer(line);
            }
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            var borderWidth = CalendarWeekLayout.LineWidth;

            for (var i = 0; i < LineCount; i++) {
                _lines[i].Frame = new CGRect(GetDayOffset(i), 0, borderWidth, Frame.Height);
            }
        }
    }
}