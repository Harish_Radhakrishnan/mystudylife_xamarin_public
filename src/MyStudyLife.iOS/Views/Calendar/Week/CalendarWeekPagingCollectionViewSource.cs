using Foundation;
using MyStudyLife.iOS.Controls;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    public class CalendarWeekPagingCollectionViewSource : MvxPagingCollectionViewSource {

        public CalendarWeekPagingCollectionViewSource(PagingCollectionView collectionView, NSString defaultCellIdentifier) : base(collectionView, defaultCellIdentifier) {}

        protected override UICollectionViewCell GetOrCreateCellFor(UICollectionView collectionView, NSIndexPath indexPath, object item) {
            var cell = (CalendarWeekCollectionViewCell) base.GetOrCreateCellFor(collectionView, indexPath, item);

            cell.ScrollOffset = CalendarWeekView.Instance.CurrentScrollOffset;
            cell.ContentInset = collectionView.SafeAreaInsets;
            
            return cell;
        }
    }
}