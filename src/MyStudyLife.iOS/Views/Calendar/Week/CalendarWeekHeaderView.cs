using System;
using CoreAnimation;
using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    public class CalendarWeekHeaderView : UIView {
        private readonly CALayer _borderBottom;
        private UIEdgeInsets _contentInset;

        public sealed override CALayer Layer => base.Layer;

        public UIEdgeInsets ContentInset {
            get => _contentInset;
            set {
                if (_contentInset == value) return;
                
                _contentInset = value;
                
                SetNeedsLayout();
                LayoutIfNeeded();
            }
        }
        
        protected nfloat LeftInset { get; private set; }
        protected nfloat DayWidth { get; private set; }
        
        public CalendarWeekHeaderView(CGRect frame) : base(frame) {
            var borderWidth = CalendarWeekLayout.LineWidth;

            _borderBottom = new CALayer {
                Frame = new CGRect(0, frame.Height - borderWidth, frame.Width, borderWidth),
                BackgroundColor = Resources.Colors.Border.CGColor
            };
            
            Layer.AddSublayer(_borderBottom);
        }

        protected nfloat GetDayOffset(int day) => LeftInset + (DayWidth * day);

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            if (_borderBottom != null) {
                _borderBottom.Frame = _borderBottom.Frame.WithWidth(Frame.Width);
            }

            LeftInset = ContentInset.Left + CalendarWeekLayout.TimeColumnWidth;
            DayWidth = (Bounds.Width - (LeftInset + ContentInset.Right))/7f;

            for (var i = 0; i < Subviews.Length; i++) {
                Subviews[i].Frame = new CGRect(
                    GetDayOffset(i),
                    0,
                    (float)Math.Ceiling(DayWidth), // Ceiling fixes a weird border being drawn
                    Bounds.Height
                );
            }
        }
    }
}