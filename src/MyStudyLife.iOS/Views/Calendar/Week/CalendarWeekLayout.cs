using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CoreGraphics;
using Foundation;
using MvvmCross;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.Diagnostics;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    public class CalendarWeekLayout : UICollectionViewLayout {
        private Timer _timer;

        // Supplementary views
        public const string ElementKindTimeRowHeader = "TimeRowHeader";

        // Decoration views
        public const string ElementKindCurrentTimeIndicator = "CurrentTimeIndicator";
        public const string ElementKindCurrentTimeHorizontalGridLine = "CurrentTimeHorizontalGridLine";
        public const string ElementKindVerticalGridLine = "VerticalGridLine";
        public const string ElementKindHorizontalGridLine = "HorizontalGridLine";
        public const string ElementKindTimeRowHeaderBackground = "RowHeaderBackground";
        public const string ElementKindTimeline = "Timeline";
        public const string ElementKindCurrentDayHighlight = "CurrentDayHighlight";

        // Attributes
        private List<UICollectionViewLayoutAttributes> _allAttributes;
        private NSMutableDictionary _itemAttributes;
        private NSMutableDictionary _timeRowHeaderAttributes;
        private NSMutableDictionary _horizontalGridlineAttributes;
        private NSMutableDictionary _verticalGridlineAttributes;
        private NSMutableDictionary _timelineAttributes;
        private NSMutableDictionary _currentDayHighlightAttributes;

        public static readonly nfloat DayHeaderHeight = 30;
        public static readonly nfloat HolidayHeaderHeight = 30;
        public static readonly nfloat TimeColumnWidth = 20;
        public static readonly nfloat MinHourHeight = 60;

        public static float LineWidth =>
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            (float)UIScreen.MainScreen.Scale == 2.0 ? 0.5f : 1.0f;

        private nfloat _dayWidth;
        private nfloat _hourHeight;

        private nfloat _horizontalGridlineHeight;
        private nfloat _verticalGridlineWidth;

        private bool _hasHolidays;
        private int _currentDayIndex = -1;

        private UIEdgeInsets? _calendarGridInset;
        private CGSize? _collectionViewContentSize;
        private UIEdgeInsets _contentInset;

        public nfloat HourHeight => _hourHeight;

        public bool HasHolidays {
            set {
                if (_hasHolidays == value) return;

                _hasHolidays = value;

                InvalidateLayoutCache();
                InvalidateLayout();
            }
        }

        public int CurrentDayIndex {
            set {
                if (_currentDayIndex == value) return;

                _currentDayIndex = value;

                InvalidateLayoutCache();
                InvalidateLayout();
            }
        }

        public UIEdgeInsets ContentInset {
            get => _contentInset;
            set {
                if (_contentInset == value) return;

                _contentInset = value;

                InvalidateLayoutCache();
                InvalidateLayout();
            }
        }

        private UIEdgeInsets CalendarGridInset {
            get {
                if (_calendarGridInset.HasValue) return _calendarGridInset.Value;
                
                var insets = ContentInset;
                var leftOffset = TimeColumnWidth + insets.Left;
                var rightOffset = insets.Right;
                var topOffset = DayHeaderHeight;

                if (_hasHolidays) {
                    topOffset += HolidayHeaderHeight;
                }
                
                _calendarGridInset = new UIEdgeInsets(topOffset, leftOffset, insets.Bottom, rightOffset);

                return _calendarGridInset.Value;
            }
        }

        public override CGSize CollectionViewContentSize {
            get {
                _collectionViewContentSize ??= new CGSize(
                    CollectionView.Bounds.Size.Width,
                    CalendarGridInset.Top + (_hourHeight * 24) + CalendarGridInset.Bottom
                );
                
                return _collectionViewContentSize.Value;
            }
        }

        #region Ctor

        public CalendarWeekLayout() {
            Initialize();
        }

        private void Initialize() {
            _allAttributes = new List<UICollectionViewLayoutAttributes>();
            _itemAttributes = new NSMutableDictionary();
            _timeRowHeaderAttributes = new NSMutableDictionary();
            _horizontalGridlineAttributes = new NSMutableDictionary();
            _verticalGridlineAttributes = new NSMutableDictionary();
            _timelineAttributes = new NSMutableDictionary();
            _currentDayHighlightAttributes = new NSMutableDictionary();

// ReSharper disable CompareOfFloatsByEqualityOperator
            _horizontalGridlineHeight = LineWidth;
            _verticalGridlineWidth = LineWidth;
// ReSharper restore CompareOfFloatsByEqualityOperator

            _timer = new Timer(MinuteTimerCallback, null, TimeSpan.FromSeconds(60d - DateTimeEx.Now.Second), TimeSpan.FromMinutes(1d));
        }

        #endregion

        private void MinuteTimerCallback(object state) {
            InvokeOnMainThread(() => {
                try {
                    SetAttributesForTimeline();
                    if (CollectionView == null) {
                        throw new NullReferenceException("CalendarWeekView.CollectionView is null (MSL-455)");
                    }

                    // Update the entries to fade out if they're past.
                    var source = (MvxCollectionViewSource)CollectionView.Source;

                    if (source?.ItemsSource != null) {
                        var entries = (IList<AgendaEntry>)source.ItemsSource;
                        int entryCount = (int)CollectionView.NumberOfItemsInSection(0);

                        if (entries.Count != entryCount) {
                            CollectionView.ReloadData();

                            return;
                        }

                        for (int i = 0; i < entryCount; i++) {
                            var indexPath = NSIndexPath.FromItemSection(i, 0);

                            var ae = entries[i];

                            var layoutAttributes = LayoutAttributesForCellAtIndexPath(indexPath, _itemAttributes);

                            if (layoutAttributes == null) {
                                throw new NullReferenceException(
                                    $"LayoutAttributesForCellAtIndexPath returned null for indexPath {indexPath} (MSL-455)"
                                );
                            }

                            layoutAttributes.Alpha = ae.IsPast ? 0.5f : 1f;
                        }
                    }

                    InvalidateLayout();
                }
                catch (Exception e) {
                    Mvx.Resolve<IBugReporter>().Send(e, true, new[] { "MinuteTimerCallback" });
                }
            });
        }

        // Should populate _layoutAttributes
        public override void PrepareLayout() {
            base.PrepareLayout();
            
            _calendarGridInset = null;
            _collectionViewContentSize = null;
            
            _dayWidth = (CollectionView.Bounds.Width - CalendarGridInset.Left - CalendarGridInset.Right) / 7f;
            // We aim to display 8 hours with space above and below in the calendar
            _hourHeight = NMath.Max((CollectionView.Bounds.Height - CalendarGridInset.Top - CalendarGridInset.Bottom) / 10, MinHourHeight);

            PrepareSectionLayoutForSections();

            if (_allAttributes.Count == 0) {
                _allAttributes.AddRange(_itemAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
                _allAttributes.AddRange(_horizontalGridlineAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
                _allAttributes.AddRange(_verticalGridlineAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
                _allAttributes.AddRange(_timeRowHeaderAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
                _allAttributes.AddRange(_timelineAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
                _allAttributes.AddRange(_currentDayHighlightAttributes.Values.Cast<UICollectionViewLayoutAttributes>());
            }
        }

        public override void PrepareForCollectionViewUpdates(UICollectionViewUpdateItem[] updateItems) {
            InvalidateLayoutCache();

            // Update the layout with the new items
            PrepareLayout();

            base.PrepareForCollectionViewUpdates(updateItems);
        }

        private void PrepareSectionLayoutForSections() {
            var contentInset = ContentInset;
            var calendarGridInset = CalendarGridInset;
            var calendarGridWidth = CollectionViewContentSize.Width - CalendarGridInset.Left;
            var calendarGridHeight = CollectionViewContentSize.Height - (CalendarGridInset.Top + CalendarGridInset.Bottom);

            var source = (MvxCollectionViewSource)CollectionView.Source;

            if (source == null) {
                return;
            }

            if (source.ItemsSource != null) {
                var entries = (IList<AgendaEntry>)source.ItemsSource;
                int entryCount = (int)CollectionView.NumberOfItemsInSection(0);
                int i = 0;

                try {
                    for (; i < entryCount; i++) {
                        if (entries.Count <= i) {
                            // Candidate fix for MSL-247 - potentially caused by the ItemsSource
                            // collection being changed whilst enumerating (if this were a foreach
                            // loop it'd throw that error). Reloading data would prevent an inconsistent
                            // state but could have perf implications

                            CollectionView.ReloadData();

                            return;
                        }

                        var indexPath = NSIndexPath.FromItemSection(i, 0);

                        var ae = entries[i];

                        var layoutAttributes = LayoutAttributesForCellAtIndexPath(indexPath, _itemAttributes);

                        layoutAttributes.ZIndex = 1000;

                        var entryWidth = _dayWidth / (ae.ConflictCount + 1);

                        layoutAttributes.Frame = new CGRect(
                            calendarGridInset.Left + (_dayWidth * L10n.GetDayIndex(ae.StartTime.DayOfWeek)) + (ae.ConflictIndex > 0 ? (ae.ConflictIndex * entryWidth) : 0),
                            calendarGridInset.Top + ((float)ae.StartTime.TimeOfDay.TotalHours * _hourHeight),
                            entryWidth,
                            (float)ae.Duration.TotalHours * _hourHeight
                        );

                        layoutAttributes.Alpha = ae.IsPast ? 0.5f : 1f;
                    }
                }
                catch (ArgumentOutOfRangeException ex) {
                    // Diagnostics for MSL-247
                    ex.Data["Locals"] = $"entires.count={entries.Count}, entryCount={entryCount}, i={i}";

                    throw;
                }
            }
            
            // Hours
            for (int hour = 0; hour <= 24; hour++) {
                var indexPath = NSIndexPath.FromItemSection(hour, 0);

                var xOffset = NMath.Max(TimeColumnWidth, calendarGridInset.Left);
                var yOffset = (calendarGridInset.Top + (_hourHeight * hour)) - (_horizontalGridlineHeight / 2.0f);

                var gridline = LayoutAttributesForDecorationViewAtIndexPath(indexPath, new NSString(ElementKindHorizontalGridLine), _horizontalGridlineAttributes);
                gridline.Frame = new CGRect(xOffset, yOffset, calendarGridWidth, _horizontalGridlineHeight);

                if (hour < 24) {
                    var timeRowHeader = LayoutAttributesForSupplementaryViewAtIndexPath(indexPath, new NSString(ElementKindTimeRowHeader), _timeRowHeaderAttributes);
                    timeRowHeader.Frame = new CGRect(contentInset.Left, yOffset, TimeColumnWidth, _hourHeight);
                }
            }

            // Days
            var verticalLineCount = calendarGridInset.Right > 0 ? 8 : 7; // Display a right border on the last day of the week when there is an inset
            
            for (int day = 0; day < verticalLineCount; day++) {
                var indexPath = NSIndexPath.FromItemSection(day, 0);

                var xOffset = calendarGridInset.Left + (day * _dayWidth);
                var yOffset = calendarGridInset.Top;

                var gridline = LayoutAttributesForDecorationViewAtIndexPath(indexPath, new NSString(ElementKindVerticalGridLine), _verticalGridlineAttributes);
                gridline.Frame = new CGRect(xOffset, yOffset, _verticalGridlineWidth, calendarGridHeight);
            }

            // Timeline
            SetAttributesForTimeline();

            // Day highlight
            SetAttributesForCurrentDayHighlight();
        }

        private void SetAttributesForTimeline() {
            // Don't show timeline in non current weeks
            if (_currentDayIndex < 0) {
                return;
            }

            var calendarGridInset = CalendarGridInset;

            var timelineAttrs = LayoutAttributesForDecorationViewAtIndexPath(NSIndexPath.FromRowSection(0, 0), new NSString(ElementKindTimeline), _timelineAttributes);

            timelineAttrs.Frame = new CGRect(
                calendarGridInset.Left, 
                calendarGridInset.Top + (float)(DateTimeEx.Now.TimeOfDay.TotalHours * _hourHeight), 
                CollectionViewContentSize.Width - CalendarGridInset.Left, 
                1f);
            timelineAttrs.ZIndex = 5001;
        }

        private void SetAttributesForCurrentDayHighlight() {
            if (_currentDayIndex < 0) {
                return;
            }

            var calendarGridInset = CalendarGridInset;

            var highlightAttrs = LayoutAttributesForDecorationViewAtIndexPath(
                NSIndexPath.FromRowSection(0, 0),
                new NSString(ElementKindCurrentDayHighlight),
                _currentDayHighlightAttributes
            );

            highlightAttrs.Frame = new CGRect(calendarGridInset.Left + (_currentDayIndex * _dayWidth), calendarGridInset.Top, _dayWidth, (CollectionViewContentSize.Height - calendarGridInset.Top));
        }

        public override UICollectionViewLayoutAttributes InitialLayoutAttributesForAppearingItem(NSIndexPath itemIndexPath) {
            var attrs = base.InitialLayoutAttributesForAppearingItem(itemIndexPath);

            // Sometimes this is called when _itemAttributes is empty
            if (_itemAttributes.TryGetValue(itemIndexPath, out var cachedAttrs)) {
                attrs.Alpha = ((UICollectionViewLayoutAttributes)cachedAttrs).Alpha; // Prevents flash (required)
            }

            return attrs;
        }

        // Should return a layout attribute for each cell, supplementary and decoration view
        // that would be visible inside the given rect.
        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect) {
            var attributesForRect = new List<UICollectionViewLayoutAttributes>();

            // Reason: PERF
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var attribute in _allAttributes) {
                if (rect.IntersectsWith(attribute.Frame)) {
                    attributesForRect.Add(attribute);
                }
            }

            return attributesForRect.ToArray();
        }

        public override bool ShouldInvalidateLayoutForBoundsChange(CGRect newBounds) {
            var oldBounds = CollectionView.Bounds;

// ReSharper disable once CompareOfFloatsByEqualityOperator
            return oldBounds.Width != newBounds.Width;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath indexPath) {
            return (UICollectionViewLayoutAttributes)_itemAttributes[indexPath];
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForSupplementaryView(NSString kind, NSIndexPath indexPath) {
            switch (kind.ToString()) {
                case ElementKindTimeRowHeader:
                    return (UICollectionViewLayoutAttributes)_timeRowHeaderAttributes[indexPath];
            }

            return null;
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForDecorationView(NSString kind, NSIndexPath indexPath) {
            switch (kind.ToString()) {
                case ElementKindHorizontalGridLine:
                    return (UICollectionViewLayoutAttributes)_horizontalGridlineAttributes[indexPath];
                case ElementKindVerticalGridLine:
                    return (UICollectionViewLayoutAttributes)_verticalGridlineAttributes[indexPath];
            }

            return null;
        }

        private UICollectionViewLayoutAttributes LayoutAttributesForDecorationViewAtIndexPath(NSIndexPath indexPath, NSString kind, NSMutableDictionary itemCache) {
            var attributes =  (UICollectionViewLayoutAttributes) itemCache[indexPath];

            if (attributes == null) {
                attributes = UICollectionViewLayoutAttributes.CreateForDecorationView(kind, indexPath);
                itemCache[indexPath] = attributes;
            }

            return attributes;
        }

        private UICollectionViewLayoutAttributes LayoutAttributesForSupplementaryViewAtIndexPath(NSIndexPath indexPath, NSString kind, NSMutableDictionary itemCache) {
            var attributes = (UICollectionViewLayoutAttributes)itemCache[indexPath];

            if (attributes == null) {
                attributes = UICollectionViewLayoutAttributes.CreateForSupplementaryView(kind, indexPath);
                itemCache[indexPath] = attributes;
            }

            return attributes;
        }

        private UICollectionViewLayoutAttributes LayoutAttributesForCellAtIndexPath(NSIndexPath indexPath, NSMutableDictionary itemCache) {
            var attributes = (UICollectionViewLayoutAttributes)itemCache[indexPath];

            if (attributes == null) {
                attributes = UICollectionViewLayoutAttributes.CreateForCell(indexPath);
                itemCache[indexPath] = attributes;
            }

            return attributes;
        }

        public void InvalidateLayoutCache() {
            _calendarGridInset = null;
            _collectionViewContentSize = null;

            _allAttributes.Clear();
            _itemAttributes.Clear();
            _timeRowHeaderAttributes.Clear();
            _horizontalGridlineAttributes.Clear();
            _verticalGridlineAttributes.Clear();
            _timelineAttributes.Clear();
            _currentDayHighlightAttributes.Clear();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (_timer != null) {
                    _timer.Dispose();
                    _timer = null;
                }
            }

            base.Dispose(disposing);
        }
    }
}