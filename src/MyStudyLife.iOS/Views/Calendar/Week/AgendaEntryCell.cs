using System;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.Scheduling;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    sealed class AgendaEntryCell : MvxCollectionViewCell {
        private readonly UIEdgeInsets _padding = new UIEdgeInsets(2, 2, 2, 2);

        private UILabel _titleLabel, _locationLabel;

        public override bool Highlighted {
            get => base.Highlighted;
            set {
                base.Highlighted = value;

                if (value) {
                    Alpha = 0.7f;
                }
                else {
                    float newAlpha = (DataContext != null && ((AgendaEntry) DataContext).IsPast) ? 0.5f : 1f;

                    Animate(0.5f, () => Alpha = newAlpha);
                }
            }
        }

        [Export("initWithFrame:")]
        public AgendaEntryCell(CGRect frame) {
            Frame = frame;
            
            Initialize();

            BindingContext.DataContextChanged += BindingContextOnDataContextChanged;
        }

        // This is needed for when the cell goes in and out of the view... despite LayoutAttributes :-(
        private void BindingContextOnDataContextChanged(object sender, EventArgs e) {
            Alpha = (DataContext != null && ((AgendaEntry)DataContext).IsPast) ? 0.5f : 1f;
        }

        private void Initialize() {
            // Setting ZPosition here as well as in the layout attribute fixes a bug where it's not respected
            // thereby causing decorations (such as lines) to be displayed over our cells
            Layer.ZPosition = 5000;
            ClipsToBounds = true;

            var view = ContentView;

            _titleLabel = new UILabel {
                Font = Resources.Fonts.OfSize(10, FontWeight.Bold),
                TextColor = UIColor.White,
                Lines = 0,
                LineBreakMode = UILineBreakMode.CharacterWrap,
                TranslatesAutoresizingMaskIntoConstraints = false,
            };
            view.Add(_titleLabel);

            _locationLabel = new UILabel {
                Font = Resources.Fonts.OfSize(10),
                TextColor = UIColor.White,
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            view.Add(_locationLabel);

            view.AddConstraints(
                _titleLabel.AtLeftOf(view, _padding.Left),
                _titleLabel.AtRightOf(view, _padding.Right),
                _titleLabel.AtTopOf(view, _padding.Top),

                _locationLabel.AtLeftOf(view, _padding.Left),
                _locationLabel.AtRightOf(view, _padding.Right),
                _locationLabel.Below(_titleLabel),
                _locationLabel.Bottom().LessThanOrEqualTo().BottomOf(view).Minus(_padding.Bottom)
            );

            this.DelayBind(DoBind);
        }

        private void DoBind() {
            var set = this.CreateBindingSet<AgendaEntryCell, AgendaEntry>();

            set.Bind(this).For(x => x.BackgroundColor).To(x => x.Color).WithConversion("SubjectColorTo");
            set.Bind(_titleLabel).To(x => x.Title);
            set.Bind(_locationLabel).To(x => x.Location);

            set.Apply();
        }
    }
}