using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    class CalendarGridline : UICollectionReusableView {
        [Export("initWithFrame:")]
        public CalendarGridline(CGRect frame) : base(frame) {
            Initialize();
        }

        private void Initialize() {
            BackgroundColor = Resources.Colors.Subtle;
        }
    }
}