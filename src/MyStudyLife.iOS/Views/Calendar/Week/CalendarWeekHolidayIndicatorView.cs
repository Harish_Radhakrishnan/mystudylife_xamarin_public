using System;
using MvvmCross;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    internal sealed class CalendarWeekHolidayIndicatorView : MSLIcon {
        public string HolidayName { get; set; }

        public CalendarWeekHolidayIndicatorView() : base(Glyph.Holiday) {
            BackgroundColor = Resources.Colors.SubtleSuper;
            TextColor = Resources.Colors.SubtleText;
            TextAlignment = UITextAlignment.Center;
            UserInteractionEnabled = true;

            AddGestureRecognizer(new UITapGestureRecognizer(() => {
                if (!Hidden && !String.IsNullOrEmpty(HolidayName)) {
                    Mvx.IoCProvider.Resolve<IDialogService>().ShowNotification(null, R.Holiday + ": " + HolidayName);
                }
            }));
        }
    }
}