using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Week {
    class CalendarTimeline : UICollectionReusableView {
        [Export("initWithFrame:")]
        public CalendarTimeline(CGRect frame) : base(frame) {
            Initialize();
        }

        private void Initialize() {
            Layer.ZPosition = 5001;
            BackgroundColor = Resources.Colors.Accent;
        }
    }
}