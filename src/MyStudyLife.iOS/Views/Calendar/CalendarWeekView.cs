using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Views.Calendar.Week;
using MyStudyLife.UI.ViewModels.Calendar;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar {
    [Register("CalendarWeekView")]
    internal class CalendarWeekView : CalendarView<CalendarWeekViewModel> {
        private UICollectionView _collectionView;

        public CGPoint CurrentScrollOffset;

        public static CalendarWeekView Instance { get; private set; }

        public CalendarWeekView() {
            Instance = this;
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            View.BackgroundColor = UIColor.White;

            #region Creation

            var collectionViewLayout = new PagingCollectionViewLayout();

            var collectionView = new CalendarWeekPagingCollectionView(
View.Frame,
                collectionViewLayout
            ) {
                BackgroundColor = UIColor.White
            };

            collectionView.RegisterClassForCell(typeof (CalendarWeekCollectionViewCell), new NSString("CalendarWeek"));

            if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0)) {
                collectionView.ContentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.Never;
            }

            var collectionViewSource = new CalendarWeekPagingCollectionViewSource(collectionView, new NSString("CalendarWeek"));

            collectionView.Source = collectionViewSource;
            collectionView.ReloadData();

            Add(collectionView);

            _collectionView = collectionView;

            #endregion

            #region Binding

            var set = this.CreateBindingSet<CalendarWeekView, CalendarWeekViewModel>();

            set.Bind(TitleView).For(x => x.Title).To(x => x.PageTitleWithoutWeek);
            set.Bind(TitleView).For(x => x.SubTitle).To(x => x.RotationWeekText);
            set.Bind(collectionViewSource).To(x => x.Weeks);
            set.Bind(collectionViewSource).For(x => x.SelectedItem).To(x => x.SelectedWeek);

            set.Apply();

            #endregion
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            base.SizeRootViewImpl(rect);

            if (View != null) {
                View.Frame = rect;
            }
        }
        
        public override void ViewWillLayoutSubviews() {
            base.ViewWillLayoutSubviews();

            _collectionView.Frame = View.Frame;

            var layout = (UICollectionViewFlowLayout) _collectionView.CollectionViewLayout;

            layout.ItemSize = _collectionView.Bounds.Size;

            layout.InvalidateLayout();

            var source = (CalendarWeekPagingCollectionViewSource)_collectionView.Source;

            source?.ScrollToItem(source.SelectedItem, false);
        }

        public override bool ShouldAutorotate() {
            return true;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations() {
            return UIInterfaceOrientationMask.AllButUpsideDown;
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration) {
            base.WillRotate(toInterfaceOrientation, duration);

            bool hide = toInterfaceOrientation.IsLandscape();

            NavigationController.SetNavigationBarHidden(hide, true);
            UIApplication.SharedApplication.SetStatusBarHidden(hide, true);
        }

        public override void ViewWillAppear(bool animated) {
            bool hide = InterfaceOrientation.IsLandscape();

            NavigationController.SetNavigationBarHidden(hide, true);
            UIApplication.SharedApplication.SetStatusBarHidden(hide, true);
        }

        public override void ViewWillDisappear(bool animated) {
            UIApplication.SharedApplication.SetStatusBarHidden(false, true);
        }
    }
}
