using System;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.Scheduling;

namespace MyStudyLife.iOS.Views.Calendar.Month {
    sealed class CalendarMonthEntryIndicatorViewCell : MvxCollectionViewCell {
        public static readonly NSString Identifer = new NSString("CalendarMonthEntryIndicatorViewCell");

        [Export("initWithFrame:")]
        public CalendarMonthEntryIndicatorViewCell(CGRect frame) {
            Frame = frame;
            
            Initialize();
        }
        
        public CalendarMonthEntryIndicatorViewCell(IntPtr handle) : base(handle) {
            Initialize();
        }

        private void Initialize() {
            this.ContentView.Layer.CornerRadius = 2.5f;

            this.DelayBind(() => {
                var set = this.CreateBindingSet<CalendarMonthEntryIndicatorViewCell, AgendaEntry>();

                set.Bind(this.ContentView).For("LayerBackgroundColor").To(x => x.Color).WithConversion("SubjectColorTo");

                set.Apply();
            });
        }
    }
}