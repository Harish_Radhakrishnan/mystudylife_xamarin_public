using System;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.UI.ViewModels.Calendar;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Month {
    [Register(nameof(CalendarMonthCollectionViewCell))]
    class CalendarMonthCollectionViewCell : MvxCollectionViewCell {
        public static readonly nfloat DayHeaderHeight = 30;

        private UICollectionView _collectionView;
        private MvxCollectionViewSource _collectionViewSource;

        private UILabel[] _dayLabels = new UILabel[7];

        public CalendarMonthCollectionViewCell(IntPtr handle) : base(handle) {
            this.Initialize();
        }

        private void Initialize() {
            var itemSize = this.ContentView.Bounds.Width / 7f;

            for (int i = 0; i < 7; i++) {
                var dayLabel = _dayLabels[i] = new UILabel(new CGRect(
                    (itemSize * i),
                    0,
                    (float)Math.Ceiling(itemSize), // Ceiling fixes a weird border being drawn
                    DayHeaderHeight
                )) {
                    Font = Resources.Fonts.OfSize(12, FontWeight.Medium),
                    Text = Globalization.DateTimeFormat.GetShortestDayName(Globalization.L10n.GetDayFromIndex(i)).Substring(0,1),
                    TextColor = UIColor.White,
                    TextAlignment = UITextAlignment.Center,
                    BackgroundColor = Resources.Colors.Accent
                };
                this.ContentView.Add(dayLabel);
            }

            var layout = new UICollectionViewFlowLayout {
                MinimumLineSpacing = 0,
                MinimumInteritemSpacing = 0,
                ItemSize = new CGSize(itemSize, itemSize)
            };

            _collectionView = new UICollectionView(new CGRect(
                0,
                DayHeaderHeight,
                this.Bounds.Width,
                this.Bounds.Height - DayHeaderHeight
            ), layout) {
                BackgroundColor = UIColor.White
            };

            _collectionView.RegisterClassForCell(typeof(CalendarMonthDayViewCell), CalendarMonthDayViewCell.Identifer);

            _collectionViewSource = new MvxCollectionViewSource(_collectionView, CalendarMonthDayViewCell.Identifer);

            _collectionView.Source = _collectionViewSource;
            _collectionView.ReloadData();

            this.ContentView.Add(_collectionView);

            #region Binding

            this.DelayBind(() => {
                var set = this.CreateBindingSet<CalendarMonthCollectionViewCell, CalendarMonthModel>();

                set.Bind(_collectionViewSource).For(x => x.ItemsSource).To(x => x.Days);
                set.Bind(_collectionViewSource).For(x => x.SelectedItem).To(x => x.SelectedDay);

                set.Apply();
            });

            #endregion
        }
    }
}