using System;
using Foundation;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Platforms.Ios.Binding.Views;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Month
{
    class CalendarMonthEntryCollectionViewSource : MvxCollectionViewSource
    {
        private readonly ILogger _log = Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(CalendarMonthEntryCollectionViewSource));

        public CalendarMonthEntryCollectionViewSource(UICollectionView collectionView, NSString defaultCellIdentifier) : base(collectionView, defaultCellIdentifier) { }

        public override void ReloadData()
        {
            ((CalendarMonthEntryCollectionViewLayout)this.CollectionView.CollectionViewLayout).InvalidateLayoutCache();

            // Calling ReloadData() on a one-section collection doesn't animate
            // http://stackoverflow.com/questions/13272315/uicollectionview-animate-data-change
            this.CollectionView.PerformBatchUpdates(() =>
            {
                try
                {
                    this.CollectionView.ReloadSections(NSIndexSet.FromIndex(0));
                }
                catch (Exception ex)
                {
                    // Same as base.ReloadData()
                    _log.Log(LogLevel.Warning, ex, "Exception masked during CollectionView ReloadData");
                }
            }, null);
        }
    }
}