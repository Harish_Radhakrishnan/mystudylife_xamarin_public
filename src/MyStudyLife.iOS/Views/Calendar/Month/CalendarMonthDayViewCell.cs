using System;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.UI.ViewModels.Calendar;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Month {
    [Register(nameof(CalendarMonthDayViewCell))]
    class CalendarMonthDayViewCell : MvxCollectionViewCell {
        public static readonly NSString Identifer = new NSString("CalendarMonthDayViewCell");

        private UIView _holidayIndicatorView;
        private UILabel _dayLabel;
        private UICollectionView _collectionView;

        public override bool Selected {
            get => base.Selected;
            set {
                base.Selected = value;

                SetDayLabelTextColor();
            }
        }

        public CalendarMonthDayViewCell(IntPtr handle) : base(handle) {
            Initialize();
        }

        private void Initialize() {
            SelectedBackgroundView = new UIView {
                Layer = {
                    BackgroundColor = Resources.Colors.Accent.CGColor,
                    CornerRadius = (Frame.Width * 0.75f) / 2f
                }
            };

            BackgroundView = new UIView();
            BackgroundView.Add(_holidayIndicatorView = new UIView {
                Hidden = true,
                Frame = Bounds,
                Layer = {
                    BackgroundColor = Resources.Colors.SubtleSuper.CGColor,
                    CornerRadius = (Frame.Width * 0.75f) / 2f
                }
            });

            _dayLabel = new UILabel(new CGRect(
                0, 0, Bounds.Width, Bounds.Height - 10f
            )) {
                TextAlignment = UITextAlignment.Center,
                Font = Resources.Fonts.FontNormal
            };
            ContentView.Add(_dayLabel);

            var layout = new CalendarMonthEntryCollectionViewLayout();

            _collectionView = new UICollectionView(
                new CGRect(0, Bounds.Height - 10f, Bounds.Width, 5f),
                layout
            ) {
                BackgroundColor = UIColor.Clear,
                ScrollEnabled = false
            };

            _collectionView.RegisterClassForCell(typeof(CalendarMonthEntryIndicatorViewCell), CalendarMonthEntryIndicatorViewCell.Identifer);

            var source = new CalendarMonthEntryCollectionViewSource(_collectionView, CalendarMonthEntryIndicatorViewCell.Identifer);

            _collectionView.Source = source;
            _collectionView.ReloadData();

            Add(_collectionView);

            BindingContext.DataContextChanged += BindingContextOnDataContextChanged;

            this.DelayBind(() => {
                var set = this.CreateBindingSet<CalendarMonthDayViewCell, CalendarMonthModel.DayAndEntries>();

                set.Bind(this).For(x => x.Selected).To(x => x.IsSelected);
                set.Bind(_holidayIndicatorView).Visibility().To(x => x.IsHoliday);
                set.Bind(_dayLabel).To(x => x.Day.Day);
                set.Bind(source).To(x => x.Entries);

                set.Apply();

                Selected = CalendarMonthView.Instance.ViewModel.SelectedDay == DataContext;

                SetDayLabelTextColor();
            });
        }

        private void BindingContextOnDataContextChanged(object sender, EventArgs e) {
            SetDayLabelTextColor();
        }

        private void SetDayLabelTextColor() {
            if (DataContext == null || _dayLabel == null) {
                return;
            }

            var day = (CalendarMonthModel.DayAndEntries) DataContext;

            if (Selected) {
                _dayLabel.TextColor = UIColor.White;
            }
            else {
                if (day.IsCurrent) {
                    _dayLabel.TextColor = Resources.Colors.Accent;
                }
                else {
                    _dayLabel.TextColor = day.IsOtherMonth
                        ? Resources.Colors.SubtleText
                        : Resources.Colors.Foreground;
                }
            }
        }

        public override void LayoutSubviews() {
            base.LayoutSubviews();

            if (SelectedBackgroundView != null && _holidayIndicatorView != null && _dayLabel != null) {
                var size = Frame.Width * 0.7f;

                var frame = new CGRect(
                    (_dayLabel.Frame.Width - size) / 2f,
                    (_dayLabel.Frame.Height - size) / 2f,
                    size,
                    size
                );

                SelectedBackgroundView.Frame = frame;
                _holidayIndicatorView.Frame = frame;
            }
        }
    }
}