using System.Linq;
using CoreGraphics;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Views.Calendar.Month {
    class CalendarMonthEntryCollectionViewLayout : UICollectionViewLayout {
        private NSMutableDictionary _itemAttributes;

        public override CGSize CollectionViewContentSize {
            get { return this.CollectionView.Bounds.Size; }
        }

        public CalendarMonthEntryCollectionViewLayout() {
            this.Initialize();
        }

        private void Initialize() {
            this._itemAttributes = new NSMutableDictionary();
        }

        public override void PrepareLayout() {
            base.PrepareLayout();

            const float itemSize = 5f;
            const float itemSpacing = 2.5f;

            int itemCount = (int)this.CollectionView.NumberOfItemsInSection(0);

            if (itemCount == 0) {
                return;
            }

            var availableWidth = this.CollectionView.Bounds.Width;
            float requiredWidth = (itemCount * itemSize) + ((itemCount - 1) * itemSpacing);

            var startX = (availableWidth - requiredWidth) / 2f;

            for (int i = 0; i < itemCount; i++) {
                var indexPath = NSIndexPath.FromItemSection(i, 0);

                var attrs = (UICollectionViewLayoutAttributes)_itemAttributes[indexPath];

                if (attrs == null) {
                    attrs = UICollectionViewLayoutAttributes.CreateForCell(indexPath);
                    _itemAttributes[indexPath] = attrs;
                }

                attrs.Frame = new CGRect(
                    startX + ((itemSize + itemSpacing) * i),
                    0,
                    itemSize,
                    itemSize
                );
            }
        }

        public override UICollectionViewLayoutAttributes LayoutAttributesForItem(NSIndexPath indexPath) {
            return (UICollectionViewLayoutAttributes)this._itemAttributes[indexPath];
        }

        // This thing doesn't scroll, therefore we don't care about checking the rect
        // because they should always be within the rect. If not you've screwed up the
        // code inPrepareLayout();
        public override UICollectionViewLayoutAttributes[] LayoutAttributesForElementsInRect(CGRect rect) {
            return _itemAttributes.Values.Cast<UICollectionViewLayoutAttributes>().ToArray();
        }

        public override void PrepareForCollectionViewUpdates(UICollectionViewUpdateItem[] updateItems) {
            this.InvalidateLayoutCache();

            // Update the layout with the new items
            this.PrepareLayout();

            base.PrepareForCollectionViewUpdates(updateItems);
        }

        public void InvalidateLayoutCache() {
            this._itemAttributes.Clear();
        }
    }
}