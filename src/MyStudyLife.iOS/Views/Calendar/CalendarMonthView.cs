using CoreGraphics;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Views.Calendar.Month;

namespace MyStudyLife.iOS.Views {
    [Register("CalendarMonthView")]
    internal sealed class CalendarMonthView : CalendarView<CalendarMonthViewModel> {
        private PagingCollectionView _collectionView;

        public static CalendarMonthView Instance { get; private set; }

        public CalendarMonthView() {
            Instance = this;
        }

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            #region Creation

            var scrollView = new UIScrollView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this.Add(scrollView);

            var collectionViewCardLayout = new UIBorderedView {
                BorderColor = Resources.Colors.Border,
                BorderThickness = new Thickness(0, 0, 0, .5f)
            };
            scrollView.Add(collectionViewCardLayout);

            var calWidth = this.View.Frame.Width;
            var calHeight = CalendarMonthCollectionViewCell.DayHeaderHeight + ((calWidth / 7f) * 6);

            var collectionViewLayout = new PagingCollectionViewLayout {
                ItemSize = new CGSize(calWidth, calHeight)
            };

            _collectionView = new PagingCollectionView(
                new CGRect(0, 0, calWidth, calHeight),
                collectionViewLayout
            ) {
                BackgroundColor = UIColor.White
            };

            _collectionView.RegisterClassForCell(typeof(CalendarMonthCollectionViewCell), new NSString("CalendarMonth"));

            var collectionViewSource = new MvxPagingCollectionViewSource(_collectionView, new NSString("CalendarMonth"));

            _collectionView.Source = collectionViewSource;
            _collectionView.ReloadData();

            collectionViewCardLayout.Add(_collectionView);

            var selectedDayLabel = new MSLLabel {
                TranslatesAutoresizingMaskIntoConstraints = false
            }.WithStyle("SectionHeader");
            scrollView.Add(selectedDayLabel);

            var dayContentView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            scrollView.Add(dayContentView);

            var dayTableView = new UIFixedTableView {
                BackgroundColor = UIColor.Clear,
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false,
                ClipsToBounds = false
            };
            dayContentView.Add(dayTableView);

            var daySource = MslTableViewSource.For<AgendaEntryTableViewCell>(dayTableView);
            daySource.CellSelectionStyle = UITableViewCellSelectionStyle.None;

            dayTableView.Source = daySource;
            dayTableView.ReloadData();

            var dayLonelyLabel = new MSLLabel {
                TranslatesAutoresizingMaskIntoConstraints = false
            }.WithStyle("Lonely");
            dayContentView.Add(dayLonelyLabel);

            var tasksContentView = new UIView {
                TranslatesAutoresizingMaskIntoConstraints = false,
                Hidden = true
            };
            scrollView.Add(tasksContentView);

            var tasksTitleLabel = new MSLLabel {
                Text = Resources.Strings.TasksDue,
                TranslatesAutoresizingMaskIntoConstraints = false
            }.WithStyle("SectionHeader");
            tasksContentView.Add(tasksTitleLabel);

            var tasksTableView = new UIFixedTableView {
                BackgroundColor = UIColor.Clear,
                SeparatorStyle = UITableViewCellSeparatorStyle.None,
                TranslatesAutoresizingMaskIntoConstraints = false,
                ClipsToBounds = false
            };
            tasksContentView.Add(tasksTableView);

            var tasksSource = MslTableViewSource.For<TaskBorderedTableViewCell>(tasksTableView);
            tasksSource.CellSelectionStyle = UITableViewCellSelectionStyle.None;

            tasksSource.UseAnimations = false;
            tasksTableView.Source = tasksSource;
            tasksTableView.ReloadData();

            #endregion

            #region Binding

            var set = this.CreateBindingSet<CalendarMonthView, CalendarMonthViewModel>();

            set.Bind(TitleView).For(x => x.Title).To(x => x.PageTitle);
            set.Bind(collectionViewSource).For(x => x.ItemsSource).To(x => x.Months);
            set.Bind(collectionViewSource).For(x => x.SelectedItem).To(x => x.SelectedMonth);

            set.Bind(selectedDayLabel).To(x => x.SelectedDay.Day).WithConversion("StringFormat", "{0:D}");
            set.Bind(daySource).For(x => x.ItemsSource).To(x => x.SelectedDay.Entries);
            set.Bind(daySource).For(x => x.RowClickCommand).To(x => x.ViewEntryCommand);

            set.Bind(dayLonelyLabel).Visibility().To(x => x.SelectedDayLonelyText);
            set.Bind(dayLonelyLabel).For(x => x.Text).To(x => x.SelectedDayLonelyText);

            set.Bind(tasksContentView).Visibility().To(x => x.SelectedDayTasks.Count);
            set.Bind(tasksSource).To(x => x.SelectedDayTasks);
            set.Bind(tasksSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Apply();

            #endregion

            #region Layout

            scrollView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            this.View.AddConstraints(
                scrollView.WithSameLeft(this.View),
                scrollView.WithSameRight(this.View),
                scrollView.WithSameTop(this.View),
                scrollView.WithSameBottom(this.View),

                collectionViewCardLayout.FullWidthOf(this.View),
                collectionViewCardLayout.AtTopOf(scrollView),
                collectionViewCardLayout.Width().EqualTo(calWidth),
                collectionViewCardLayout.Height().EqualTo(calHeight),

                _collectionView.Fill(collectionViewCardLayout),

                selectedDayLabel.AtLeftOf(this.View),
                selectedDayLabel.AtRightOf(this.View),
                selectedDayLabel.Below(collectionViewCardLayout),

                dayContentView.AtLeftOf(this.View),
                dayContentView.AtRightOf(this.View),
                dayContentView.Below(selectedDayLabel),
                dayContentView.Bottom().LessThanOrEqualTo().BottomOf(scrollView).Minus(Resources.Dimens.DefaultMargin),

                dayTableView.AtLeftOf(dayContentView),
                dayTableView.AtRightOf(dayContentView),
                dayTableView.AtTopOf(dayContentView),
                dayTableView.Bottom().LessThanOrEqualTo().BottomOf(dayContentView),

                dayLonelyLabel.AtLeftOf(selectedDayLabel, Resources.Dimens.ScreenMargin.Left),
                dayLonelyLabel.AtRightOf(selectedDayLabel, Resources.Dimens.ScreenMargin.Right),
                dayLonelyLabel.Below(selectedDayLabel),
                dayLonelyLabel.Bottom().LessThanOrEqualTo().BottomOf(dayContentView),

                tasksContentView.AtLeftOf(this.View),
                tasksContentView.AtRightOf(this.View),
                tasksContentView.Below(dayContentView, Resources.Dimens.DefaultMargin),
                tasksContentView.WithSameBottom(scrollView).Minus(Resources.Dimens.ScreenMargin.Bottom),

                tasksTitleLabel.AtLeftOf(tasksContentView),
                tasksTitleLabel.AtRightOf(tasksContentView),
                tasksTitleLabel.AtTopOf(tasksContentView),

                tasksTableView.AtLeftOf(tasksContentView),
                tasksTableView.AtRightOf(tasksContentView),
                tasksTableView.Below(tasksTitleLabel),
                tasksTableView.WithSameBottom(tasksContentView)
            );

            #endregion
        }

        public override void ViewWillLayoutSubviews() {
            base.ViewWillLayoutSubviews();

            // Month view doesn't support rotation but this is still required...
            // should probably look at integating this into PagingCollectionViewSource
            // soon.
            var source = (MvxPagingCollectionViewSource)this._collectionView.Source;
            source?.ScrollToItem(source.SelectedItem, false);
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            if (this.View != null) {
                this.View.Frame = rect;
            }
        }

        public override bool ShouldAutorotate() => false;

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
            => UIInterfaceOrientationMask.Portrait;

        public override UIInterfaceOrientation PreferredInterfaceOrientationForPresentation()
            => UIInterfaceOrientation.Portrait;
    }
}