using System;
using Cirrious.FluentLayouts.Touch;
using CoreGraphics;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.iOS.Binding;
using MyStudyLife.iOS.Controls;
using MyStudyLife.iOS.Controls.Cells;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using UIKit;

namespace MyStudyLife.iOS.Views {
    [Register(nameof(ScheduleView))]
    internal class ScheduleView : MslViewController<ScheduleViewModel>, CoreView.IChildUIController {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        private UIView _tabView;
        private UISegmentedControl _tabControl;
        private MslTabListView _classesView, _holidaysView;
        private UIBarButtonItem _newClassBarButton, _newHolidayBarButton;

        public override string Title => Resources.Strings.Schedule;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            _listener = new MvxPropertyChangedListener(ViewModel)
                .Listen(nameof(ViewModel.ShowLonely), SetTabsState)
                .Listen(nameof(ViewModel.IsLoaded), () => {
                    SetNavigationBarState();
                    SetTabsState();
                });

            View.BackgroundColor = UIColor.White;

            SetNavigationBarState();

            if (NavigationController != null)
                NavigationController.ToolbarHidden = false;

            var titleView = new MslNavBarTitleView {
                Title = Title
            };
            ParentViewController.NavigationItem.TitleView = titleView;

            _newClassBarButton = new UIBarButtonItem(R.NewClass, UIBarButtonItemStyle.Plain, null);
            _newHolidayBarButton = new UIBarButtonItem(R.NewHoliday, UIBarButtonItemStyle.Plain, null);

            titleView.Tapped += (s, e) => {
                var bindings = this.CreateInlineBindingTarget<SubjectsViewModel>();

                var root = new AcademicScheduleRootElement(allowNone: ViewModel.HasClassesWithoutSchedule)
                    .Bind(bindings, x => x.AcademicYears, x => x.AcademicYears)
                    .Bind(bindings, x => x.SelectedSchedule, x => x.SelectedSchedule);

                var viewController = new AcademicScheduleDialogViewController(root);

                var navController = new UINavigationController(viewController) {
                    ModalTransitionStyle = UIModalTransitionStyle.CoverVertical
                };

                PresentViewController(navController, true, null);
            };

            #region Creation

            _tabView = new UIView {
                BackgroundColor = Resources.Colors.Accent
            };
            Add(_tabView);

            _tabControl = new UISegmentedControl {
                TranslatesAutoresizingMaskIntoConstraints = false,
                TintColor = UIColor.White
            };
            _tabControl.ApplyAppearance();
            _tabView.Add(_tabControl);

            _tabControl.InsertSegment(Resources.Strings.Classes, 0, false);
            _tabControl.InsertSegment(Resources.Strings.Holidays, 1, false);

            _tabControl.SelectedSegment = 0;

            _tabControl.ValueChanged += (s, e) => SetTabsState();

            _classesView = new MslTabListView(Glyph.Class);
            var classesViewSource = new ClassTableViewSource(_classesView.TableView, User.Current.IsTeacher);

            _classesView.TableView.Source = classesViewSource;
            _classesView.TableView.ReloadData();

            Add(_classesView);

            _holidaysView = new MslTabListView(Glyph.Holiday);
            var holidaysViewSource = new MslTableViewSource(_holidaysView.TableView, typeof(HolidayTableViewCell));

            _holidaysView.TableView.Source = holidaysViewSource;
            _holidaysView.TableView.ReloadData();

            Add(_holidaysView);

            var lonelyViewContainer = new UIView();
            Add(lonelyViewContainer);

            var lonelyView = new MslLonelyView(Glyph.Schedule) {
                TranslatesAutoresizingMaskIntoConstraints = false,
                AutoAlignInSuperview = false
            };
            lonelyViewContainer.Add(lonelyView);

            var plusIcon = UIImage.FromBundle("PlusIcon.png");

            var lonelyNewButton = new UIButton {
                ContentEdgeInsets = new UIEdgeInsets(0, 0, 0, plusIcon.Size.Width / 2),
                Font = Resources.Fonts.FontNormal,
                TranslatesAutoresizingMaskIntoConstraints = false,
                TitleLabel = {
                    AdjustsFontSizeToFitWidth = false,
                    LineBreakMode = UILineBreakMode.Clip
                }
            };
            lonelyViewContainer.Add(lonelyNewButton);

            var highlightedColor = Resources.Colors.Accent.ColorWithAlpha(.3f);

            lonelyNewButton.SetImage(plusIcon, UIControlState.Normal);
            lonelyNewButton.SetImage(plusIcon.WithOverlayColor(highlightedColor), UIControlState.Highlighted);
            lonelyNewButton.TitleEdgeInsets = new UIEdgeInsets(0, plusIcon.Size.Width / 2, 0, plusIcon.Size.Width / -2);
            lonelyNewButton.SetTitle(R.NewAcademicYear, UIControlState.Normal);
            lonelyNewButton.SetTitleColor(Resources.Colors.Accent, UIControlState.Normal);
            lonelyNewButton.SetTitleColor(highlightedColor, UIControlState.Highlighted);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<ScheduleView, ScheduleViewModel>();

            set.Bind(titleView).For(x => x.UserInteractionEnabled).To(x => x.ShowLonely).WithConversion("InvertedBoolean");
            set.Bind(titleView).For(x => x.SubTitle).To(x => x.SelectedScheduleText);

            set.Bind(_newClassBarButton).To(x => x.NewClassCommand);
            set.Bind(_newHolidayBarButton).To(x => x.NewHolidayCommand);

            set.Bind(_tabView).InvertedVisibility().To(x => x.ShowLonely);
            set.Bind(lonelyViewContainer).Visibility().To(x => x.ShowLonely);
            set.Bind(lonelyView).For(x => x.Text).To(x => x.LonelyText);
            set.Bind(lonelyView).For(x => x.SubText).To(x => x.LonelySubText);
            set.Bind(lonelyNewButton).To(x => x.NewAcademicYearCommand);
            set.Bind(lonelyNewButton).Visibility().To(x => x.ShowNewYearButton);

            set.Bind(_classesView.TableView).Visibility().To(x => x.ClassesViewSource.Count);
            set.Bind(_classesView.LonelyView).InvertedVisibility().To(x => x.ClassesViewSource.Count);
            set.Bind(_classesView.LonelyView).For(x => x.Text).To(x => x.ClassesLonelyText);
            set.Bind(_classesView.LonelyView).For(x => x.SubText).To(x => x.ClassesLonelySubText);
            set.Bind(classesViewSource).To(x => x.ClassesViewSource.View);
            set.Bind(classesViewSource).For(x => x.RowClickCommand).To(x => x.ViewEntityCommand);

            set.Bind(_holidaysView.TableView).Visibility().To(x => x.HolidaysViewSource.Count);
            set.Bind(_holidaysView.LonelyView).InvertedVisibility().To(x => x.HolidaysViewSource.Count);
            set.Bind(_holidaysView.LonelyView).For(x => x.Text).To(x => x.HolidaysLonelyText);
            set.Bind(holidaysViewSource).To(x => x.HolidaysViewSource.View);
            set.Bind(holidaysViewSource).For(x => x.RowClickCommand).To(x => x.EditHolidayCommand);

            set.Apply();

            #endregion

            #region Layout

            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                _tabView.AtLeftOf(View),
                _tabView.AtRightOf(View),
                _tabView.AtTopOf(View),

                _tabControl.WithSameCenterX(View),
                _tabControl.AtLeftOf(_tabView, 15),
                _tabControl.AtRightOf(_tabView, 15),
                _tabControl.AtTopOf(_tabView, 10),
                _tabControl.AtBottomOf(_tabView, 10),

                _classesView.AtLeftOf(View),
                _classesView.AtRightOf(View),
                _classesView.Below(_tabView),
                _classesView.AtBottomOf(View),

                _holidaysView.AtLeftOf(View),
                _holidaysView.AtRightOf(View),
                _holidaysView.Below(_tabView),
                _holidaysView.AtBottomOf(View),

                lonelyViewContainer.FullWidthOf(View, 15),
                lonelyViewContainer.WithSameCenterY(View),

                lonelyView.FullWidthOf(lonelyViewContainer),
                lonelyView.AtTopOf(lonelyViewContainer),

                lonelyNewButton.WithSameCenterX(lonelyView),
                lonelyNewButton.Below(lonelyView, 10f),
                lonelyNewButton.AtBottomOf(lonelyViewContainer)
            );

            #endregion

            SetTabsState();
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            SetTabsState();
        }

        public override void ViewWillDisappear(bool animated) {
            if (NavigationController != null) {
                NavigationController.ToolbarHidden = true;
            }

            ParentViewController.SetToolbarItems(null, false);

            base.ViewWillDisappear(animated);
        }

        private void SetTabsState() {
            if (NavigationController == null) {
                return;
            }

            var vm = ViewModel;

            if (vm == null || vm.ShowLonely) {
                _holidaysView.Hidden = true;
                _classesView.Hidden = true;

                NavigationController.ToolbarHidden = true;
            }
            else {
                // In iOS 15, UIKit has extended the usage of the scrollEdgeAppearance,
                // which by default produces a transparent background, to all navigation/toolbar Hence,
                // Have set apperarance for iOS 15 to Default ToolBar
                if (UIDevice.CurrentDevice.CheckSystemVersion(15, 0)) {
                    var toolBar = ParentViewController.NavigationController?.Toolbar;
                    if (toolBar != null) {
                        toolBar.ScrollEdgeAppearance = toolBar.StandardAppearance;
                        toolBar.ScrollEdgeAppearance.ConfigureWithDefaultBackground();
                        toolBar.ScrollEdgeAppearance.BackgroundColor = _tabView.BackgroundColor;
                    }
                }

                bool isClasses = (int)_tabControl.SelectedSegment == 0;
                bool isHolidays = (int)_tabControl.SelectedSegment == 1;
                bool canAddClasses = vm.CanAddClasses;
                bool canAddHolidays = vm.CanModifyYear;

                _holidaysView.Hidden = !isHolidays;
                _classesView.Hidden = !_holidaysView.Hidden;

                NavigationController.ToolbarHidden = (isClasses && !canAddClasses) || (isHolidays && !canAddHolidays);
                ParentViewController.SetToolbarItems(new[] {
                    new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
                    isHolidays ? _newHolidayBarButton : _newClassBarButton
                }, false);
            }

            SizeRootView();
        }

        private void SetNavigationBarState() {
            var vm = ViewModel;

            // Don't show if the user can't do anything the action sheet would display
            if (!vm.IsLoaded || (!vm.CanAddYears && !vm.CanManageSubjects)) {
                return;
            }

            ParentViewController.NavigationItem.SetRightBarButtonItems(
                new[] {
                    new UIBarButtonItem(UIImage.FromBundle("MoreIcon.png"), UIBarButtonItemStyle.Plain, (s, e) => {
                        var actionSheet = new MslActionSheet();

                        nfloat joinClassesIndex = -1, editYearIndex = -1, newYearIndex = -1, manageSubjectsIndex = -1;

                        //if (this.ViewModel.ShowJoinClassesCommand.CanExecute(null)) {
                        //    actionSheet.AddButton(Resources.Strings.JoinClasses);
                        //}

                        if (ViewModel.EditAcademicYearCommand.CanExecute(null)) {
                            editYearIndex = actionSheet.ButtonCount;
                            actionSheet.AddButton(R.Edit + " " + ViewModel.SelectedYear);
                        }

                        if (ViewModel.NewAcademicYearCommand.CanExecute(null) && !ViewModel.ShowLonely) {
                            newYearIndex = actionSheet.ButtonCount;
                            actionSheet.AddButton(R.NewAcademicYear);
                        }

                        if (ViewModel.ShowManageSubjectsCommand.CanExecute(null)) {
                            manageSubjectsIndex = actionSheet.ButtonCount;
                            actionSheet.AddButton(Resources.Strings.ManageSubjects);
                        }

                        actionSheet.CancelButtonIndex = actionSheet.ButtonCount;
                        actionSheet.AddButton(Resources.Strings.Cancel);

                        actionSheet.ShowInView(View);

                        actionSheet.Dismissed += (s2, e2) => {
                            if (e2.ButtonIndex == joinClassesIndex) {
                                ViewModel.ShowJoinClassesCommand.Execute(null);
                            }
                            else if (e2.ButtonIndex == editYearIndex) {
                                ViewModel.EditAcademicYearCommand.Execute(null);
                            }
                            else if (e2.ButtonIndex == newYearIndex) {
                                ViewModel.NewAcademicYearCommand.Execute(null);
                            }
                            else if (e2.ButtonIndex == manageSubjectsIndex) {
                                ViewModel.ShowManageSubjectsCommand.Execute(null);
                            }
                        };
                    })
                },
                true
            );
        }

        protected override void SizeRootViewImpl(CGRect rect) {
            base.SizeRootViewImpl(rect);

            if (View != null) {
                View.Frame = rect;
            }
        }

        public void OnNavigatingTo(CoreView coreView, bool fromMenu) {
            // Fixes bug where toolbar will appear over the navigation drawer
            if (fromMenu && coreView.NavigationController != null) {
                coreView.NavigationController.ToolbarHidden = false;
            }
        }

        class AcademicScheduleDialogViewController : DialogViewController {
            public AcademicScheduleDialogViewController(RootElement root) : base(root, true) {
                Autorotate = true;
            }

            public override void ViewDidLoad() {
                NavigationController.NavigationBar.ApplyAppearance();

                NavigationItem.HidesBackButton = false;

                NavigationItem.SetRightBarButtonItem(
                    new UIBarButtonItem(Resources.Strings.Done, UIBarButtonItemStyle.Done,
                        (s, e) => DismissViewController(true, null)),
                    true
                );

                base.ViewDidLoad();
            }
        }

        class MslTabListView : UIView {
            private UITableView _tableView;
            private MslLonelyView _lonelyView;

            public UITableView TableView => _tableView;

            public MslLonelyView LonelyView => _lonelyView;

            public MslTabListView(Glyph glyph) {
                #region Creation

                _tableView = new UITableView {
                    AllowsSelection = true,
                    SeparatorColor = Resources.Colors.Border,
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                Add(_tableView);

                _lonelyView = new MslLonelyView(glyph) {
                    TranslatesAutoresizingMaskIntoConstraints = false
                };
                Add(_lonelyView);

                #endregion

                #region Layout

                this.AddConstraints(
                    _tableView.AtLeftOf(this),
                    _tableView.AtRightOf(this),
                    _tableView.AtBottomOf(this),
                    _tableView.AtTopOf(this)
                );

                #endregion
            }

            protected override void Dispose(bool disposing) {
                if (_tableView != null) {
                    _tableView.Dispose();
                    _tableView = null;
                }
                if (_lonelyView != null) {
                    _lonelyView.Dispose();
                    _lonelyView = null;
                }

                base.Dispose(disposing);
            }
        }
    }
}