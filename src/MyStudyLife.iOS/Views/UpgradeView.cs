using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.ViewModels;
using Foundation;
using UIKit;
using MyStudyLife.iOS.Platform;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using BigTed;
using MyStudyLife.iOS.Controls;

namespace MyStudyLife.iOS.Views {
    [Register(nameof(UpgradeView))]
    internal class UpgradeView : MslViewController<UpgradeViewModel> {
        private ProgressHUD _upgradingHud;

        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        public override void ViewDidLoad() {
            base.ViewDidLoad();

            this.OnUpgradingChanged();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(this.ViewModel.IsUpgrading), OnUpgradingChanged);
                
            NavigationController.SetNavigationBarHidden(true, false);

            View.BackgroundColor = Resources.Colors.Accent;

            #region Creation

            var backgroundView = new UIView {
                BackgroundColor = UIColor.White,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this.View.Add(backgroundView);

            var contentView = new UIView {
                BackgroundColor = UIColor.White,
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            this.View.Add(contentView);

            var titleView = new MSLLabel {
                Text = Resources.Strings.UpgradeView.ErrorTitle,
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontLarge
            };
            contentView.Add(titleView);

            var detailView = new MslAttributedLabel {
                TextColor = Resources.Colors.Foreground,
                Font = Resources.Fonts.FontNormal,
                Lines = 0,
                LineBreakMode = UILineBreakMode.WordWrap
            };
            contentView.Add(detailView);

            string text = detailView.Text = String.Format(
                Resources.Strings.UpgradeView.ErrorMessage,
                Resources.Strings.UpgradeView.ErrorMessage_WebApp
            );

            detailView.AddLinkToURL(
                new NSUrl("WEB_APP"),
                new NSRange(
                    Resources.Strings.UpgradeView.ErrorMessage.IndexOf("{0"),
                    Resources.Strings.UpgradeView.ErrorMessage_WebApp.Length
                )
            );

            detailView.SelectedLinkWithUrl += (s, e) => {
                if (e.Url.ToString() == "WEB_APP") {
                    this.ViewModel.OpenWebAppCommand.Execute(null);
                    e.Handled = true;
                }
            };

            var contactSupportButton = new UIButton {
                Font = Resources.Fonts.FontNormal
            };
            contactSupportButton.SetTitle(Resources.Strings.ContactSupport, UIControlState.Normal);
            contactSupportButton.SetTitleColor(Resources.Colors.Accent, UIControlState.Normal);
            contentView.Add(contactSupportButton);

            var continueButton = new MSLButton {
                BackgroundColor = Resources.Colors.Accent,
                HighlightedBackgroundColor = Resources.Colors.AccentMid,
                TitleLabel = {
                    TextAlignment = UITextAlignment.Center
                },
                Layer = {
                    CornerRadius = 3
                },
                Font = Resources.Fonts.OfSize(Resources.Fonts.FontSizeNormal),
                TranslatesAutoresizingMaskIntoConstraints = false
            };
            continueButton.SetTitle(Resources.Strings.Continue, UIControlState.Normal);
            continueButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            contentView.Add(continueButton);

            #endregion

            #region Binding

            var set = this.CreateBindingSet<UpgradeView, UpgradeViewModel>();

            set.Bind(contentView).To(x => x.HasFailed).Visibility();
            set.Bind(contactSupportButton).To(x => x.EmailErrorCommand);
            set.Bind(continueButton).To(x => x.ContinueCommand);

            set.Apply();

            #endregion

            #region Layout

            contentView.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            View.AddConstraints(
                backgroundView.Fill(View, new UIEdgeInsets {
                    Top = UIApplication.SharedApplication.StatusBarFrame.Size.Height
                }),
                contentView.Fill(View, new UIEdgeInsets {
                    Left = Resources.Dimens.ScreenMargin.Left,
                    Right = Resources.Dimens.ScreenMargin.Right,
                    Top = UIApplication.SharedApplication.StatusBarFrame.Size.Height + Resources.Dimens.ScreenMargin.Top,
                    Bottom = Resources.Dimens.ScreenMargin.Bottom
                }),

                titleView.AtTopOf(contentView),
                titleView.FullWidthOf(contentView),

                detailView.FullWidthOf(contentView),
                detailView.Below(titleView, 5f),

                contactSupportButton.WithSameLeft(detailView),
                contactSupportButton.Below(detailView, 5),

                continueButton.FullWidthOf(contentView),
                continueButton.AtBottomOf(contentView),
                continueButton.Top().GreaterThanOrEqualTo().BottomOf(contactSupportButton).Plus(Resources.Dimens.DefaultHorizontalMargin),
                continueButton.Height().EqualTo(48f)
            );

            #endregion
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            this.View.BackgroundColor = Resources.Colors.Accent;
        }

        private void OnUpgradingChanged() {
            var isBusy = (this.ViewModel?.IsUpgrading ?? true);

            if (isBusy) {
                if (this._upgradingHud == null) {
                    this._upgradingHud = iOSDialogService.ShowBlockingBusyIndicator(R.Common.UpdatingWithEllipsis);
                }
            }
            else {
                this._upgradingHud?.Dismiss();
                this._upgradingHud = null;
            }
        }
    }
}
