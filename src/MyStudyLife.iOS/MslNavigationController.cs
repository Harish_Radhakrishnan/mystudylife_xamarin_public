using MvvmCross.Platforms.Ios.Views;
using UIKit;

namespace MyStudyLife.iOS {
    internal sealed class MslNavigationController : MvxNavigationController {
        private UIViewController _rootViewController;

        public MslNavigationController(UIViewController rootViewController) : base(rootViewController) {
            // This may hopefully fix an iOS8 bug where TopViewController is null
            // https://app.raygun.io/dashboard/7n77c5/errors/487084781
            _rootViewController = rootViewController;
        }

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations() {
            var controller = TopViewController ?? _rootViewController;

            return controller?.GetSupportedInterfaceOrientations() ?? base.GetSupportedInterfaceOrientations();
        }

        public override bool ShouldAutorotate() {
            var controller = TopViewController ?? _rootViewController;

            return controller?.ShouldAutorotate() ?? base.ShouldAutorotate();
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                // Just release the reference, don't call .Dispose() on it
                _rootViewController = null;
            }
            base.Dispose(disposing);
        }
    }
}