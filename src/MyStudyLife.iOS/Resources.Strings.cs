using System;

namespace MyStudyLife.iOS {
    public static partial class Resources {
        public static class Strings {
            public const string Dashboard = "Dashboard";
            public const string Calendar = "Calendar";
            public const string Subject = "Subject";
            public const string Subjects = "Subjects";
            public const string Class = "Class";
            public const string Classes = "Classes";
            public const string Holidays = "Holidays";
            public const string Schedule = "Schedule";
            public const string Task = "Task";
            public const string Tasks = "Tasks";
            public const string Exam = "Exam";
            public const string Exams = "Exams";
            public const string Search = "Search";
            public const string Settings = "Settings";

            public const string AcademicYears = "Academic Years";
            public const string Term = "Term";
            public const string Terms = "Terms";
            public const string Scheduling = "Scheduling";
            public const string Week = "Week";
            public const string Month = "Month";
            public const string Today = "Today";

            public const string AcademicYearsLonely = "Not a single year, term, semester, quarter or trimester!";
            public const string SubjectsLonely = "No subjects.";
            public const string ClassesLonely = "No classes.";
            public const string TasksLonely = "No tasks.";
            public const string ExamsLonely = "No exams.";

            public const string TasksDue = "Tasks Due";
            public const string NoClassesOrExams = "No classes/exams.";

            public const string Reminders = "Reminders";
            public const string Sync = "Sync";
            public const string Timetable = "Timetable";
            public const string General = "General";
            public const string About = "About";
            public const string AboutMyStudyLife = "About My Study Life";

            public const string Leave = "Leave";
            public const string Teacher = "Teacher";
            public const string Student = "Student";
            public const string NoThanks = "No Thanks";
            public const string Complete = "Complete";
            public const string Due = "Due";
            public const string Date = "Date";
            public const string StartDate = "Start Date";
            public const string EndDate = "End Date";
            public const string StartTime = "Start Time";
            public const string EndTime = "End Time";
            public const string Duration = "Duration";

            public const string Type = "Type";
            public const string Assignment = "Assignment";
            public const string Reminder = "Reminder";
            public const string Revision = "Revision";

            public const string Title = "Title";
            public const string Detail = "Detail";
            public const string Module = "Module";
            public const string NameOrModule = "Name/Module";

            public const string Resit = "Resit";

            public const string Time = "Time";
            public const string Times = "Times";

            public const string Seat = "Seat";
            public const string Room = "Room";
            public const string Building = "Building";

            public const string Name = "Name";
            public const string Color = "Color";
            public const string None = "None";
            public const string PushesSchedule = "Pushes Schedule";
            public const string JoinClasses = "Join Classes";

            public const string Yes = "Yes";
            public const string Close = "Close";
            public const string Edit = "Edit";
            public const string Save = "Save";
            public const string New = "New";
            public const string Cancel = "Cancel";
            public const string Done = "Done";
            public const string Delete = "Delete";
            public const string Skip = "Skip";
            public const string Back = "Back";
            public const string Next = "Next";
            public const string Continue = "Continue";
            public const string GetStarted = "Get Started";

            public const string ManageSubjects = "Manage Subjects";
            public const string ManageAcademicSchedules = "Manage Years + Terms";

            public const string SignOut = "Sign Out";
            public const string SigningOutWithEllipsis = "Signing out...";

            public const string AcademicYearOrTerm = "Academic Year/Term";

            public const string Dashboard_tasks_timescale = "Show Tasks Due Within";
            public const string Dashboard_show_exams = "Show Exams";
            public const string Dashboard_exams_timescale = "Show Exams Within";
            public const string DashboardExamsHideWhenEmpty = "Hide When No Exams";

            public const string Mode = "Mode";
            public const string Fixed = "Fixed";
            public const string WeekRotation = "Week Rotation";
            public const string DayRotation = "Day Rotation";
            public const string NumberOfWeeks = "Number of Weeks";
            public const string NumberOfDays = "Number of Days";
            public const string StartWeek = "Start Week";
            public const string StartDay = "Start Day";
            public const string Numbered = "Numbered";
            public const string Lettered = "Lettered";
            public const string CurrentWeek = "Current Week";
            public const string CurrentDay = "Current Day";
            public const string Days = "Days";

            public const string FilterClasses = "Filter Classes";
            public const string FilterTasks = "Filter Tasks";
            public const string FilterExams = "Filter Exams";

            public const string FirstDay = "First Day";
            public const string RotationSchedule = "Rotation Schedule";

            public const string FeedbackAndSupport = "Feedback and Support";
            public const string Privacy = "Privacy";
            public const string TermsOfUse = "Terms of Use";
            public const string ContactSupport = "Contact Support";

            public static class Common {
                public const string Today = "Today";
                public const string Tomorrow = "Tomorrow";

                public const string Current = "Current";
                public const string Completed = "Completed";
                public const string Past = "Past";

                public const string Save = "Save";
                public const string Okay = "Okay";
            }

            public static class SignInView {
                public const string Or = "Or";
                public const string Email = "Email";
                public const string Password = "Password";
                public const string SignIn = "Sign in";
                public const string SignInWithApple = "Continue with Apple";
                public const string SignInWithOffice365 = "Continue with Office 365";
                public const string SignInWithFacebook = "Continue with Facebook";
                public const string SignInWithGoogle = "Continue with Google";
                public const string SignInWithEmail = "Sign in with email";
                public const string SignUp = "Sign up";
                public const string ForgottenPassword = "Forgotten password?";
                public const string LegalTextFormat = "by continuing you agree to our {0} and {1}";
                public const string LegalText_PrivacyPolicy = "privacy policy";
                public const string LegalText_TermsOfService = "terms of service";
                public const string FindOutMoreFormat = "New to My Study Life? {0}";
                public const string FindOutMoreLink = "Find out more";
            }

            public static class UpgradeView {
                public const string ErrorTitle = "Data Upgrade Failed";
                public static readonly string ErrorMessage =
                    "An error occurred whilst upgrading you data. It is likely that your data is synced but continuing please visit the {0} to check." +
                    Environment.NewLine +
                    Environment.NewLine +
                    "If your data is not synced please send contact us by tapping the link below.";
                public const string ErrorMessage_WebApp = "web app";
            }

            public static class WelcomeView {
                public const string Hello_there = "Hello there";
            }

            public static class DashboardView {
                public const string UpcomingTasks = "Upcoming Tasks";
            }

            public static class TaskView {
                public const string PercentCompleteFormat = "{0}% complete";
                public const string DetailLonely = "You haven't added any detail to this task yet, add some.";
                public const string DetailLonelyAddSome = "add some";
                public const string SetIncomplete = "set incomplete";

                public const string DueForClass = "Due for class";
                public const string RevisionForExam = "Revision for exam";

                public const string DueForClassLonely = "It doesn't look like there are any classes on the day this task is due.";
                public const string RevisionForExamLonely = "This revision task is not assigned to an exam.";
            }

            public static class ExamView {
                public const string RevisionTasks = "Revision tasks";
                public const string RevisionTasksLonely = "This exam has no incomplete revision tasks.";

                public const string ConflictingClasses = "Conflicting classes";
                public const string ConflictingClassesLonely = "It doesn't look like there are any classes which conflict with this exam.";
            }

            public static class ClassView {
                public const string TasksDue = "Tasks due for this class";
                public const string TasksDueLonely = "This class doesn't have any tasks due.";

                public const string TasksOverdue = "Tasks overdue for this subject";
                public const string TasksOverdueLonely = "It doesn't look like there are any overdue tasks for this subject.";
            }

            public static class ClassInputView {
                public const string NewTime = "New Time";

                public const string DayType = "Day Type";

                public const string DayOfWeek = "Day of Week";
                public const string RotationDay = "Rotation Day";
                public const string RotationWeek = "Rotation Week";

                public const string RotationDays = "Rotation Days";

                public const string Occurs = "Occurs";
                public const string Once = "Once";
                public const string MultipleTimes = "Multiple Times (Repeats)";

                public const string AddStartEndDates = "Add start/end dates?";

                public const string OverrideSubjectColor = "Override Subject Color?";
            }

            public static class AcademicYearInputView {
                public const string NewTerm = "New Term";
            }

            public static class TimetableSettingsView {
                public const string WeekRotationDisam =
                    "Use this when classes don't occur every week but they do occur every x weeks.\n\rIf your classes do not occur every fixed number of weeks then use fixed mode.";

                public const string DayRotationDisam =
                    "Use this when classes rotate on a fixed number of days (for example Class A occurs every x days).\n\rIf your timetable occurs outside the normal 5 day week be sure to check include Saturdays and/or Sundays.";
            }
        }
    }
}