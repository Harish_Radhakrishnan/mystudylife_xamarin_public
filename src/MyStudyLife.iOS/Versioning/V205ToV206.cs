﻿using MyStudyLife.Versioning.Shared;
using System;

namespace MyStudyLife.iOS.Versioning {
    public class V205ToV206 : SchoolLogoUpgrade {
        public override Version FromVersion => new Version(2, 0, 5);

        public override Version ToVersion => new Version(2, 0, 6);
    }
}
