using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.iOS.Versioning {
    public sealed class V1ToV2 : HolidaysUpgrade {
        public override Version FromVersion {
            get { return new Version(1,0); }
        }

        public override Version ToVersion {
            get { return new Version(2,0); }
        }
    }
}
