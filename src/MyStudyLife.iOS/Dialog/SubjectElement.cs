using Foundation;
using UIKit;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls.Cells;

namespace MyStudyLife.iOS.Dialog {
    public class SubjectElement : MslRadioElement {
        private readonly Subject _subject;

// ReSharper disable once InconsistentNaming
        private static readonly NSString _cellKey = new NSString(SubjectTableViewCell.Identifier);

        public SubjectElement(Subject subject) {
            this.ObjectValue = _subject = subject;

            this.Caption = subject.Name;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(_cellKey) as SubjectTableViewCell ?? new SubjectTableViewCell();

            cell.DataContext = _subject;
            // Override tint color since we don't want it to be the current subject's color
            cell.TintColor = UITableViewCell.Appearance.TintColor;

            return cell;
        }

        protected override void UpdateCellDisplay(UITableViewCell cell) {
            // Just update the accessoary display - everything else is bound
            UpdateAccessoryDisplay(cell);
        }
    }
}