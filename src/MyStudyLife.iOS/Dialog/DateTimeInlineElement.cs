using System;
using System.Collections.Generic;
using System.Linq;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class DateTimeInlineElement : DateTimeElement, IInlinePickerElement, ITintable {
        private bool _isPickerPresent = false;
        private UIViewElement _datePickerContainer;
        private UIDatePicker _datePicker;
        private UIColor _tintColor;

        protected UIDatePicker DatePicker => _datePicker;

        public UIColor TintColor {
            get { return _tintColor ?? UILabel.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    var textLabel = CurrentAttachedCell?.DetailTextLabel;
                    if (textLabel != null && _isPickerPresent) {
                        textLabel.TextColor = _tintColor;
                    }
                }
            }
        }

        public DateTimeInlineElement(string caption = null) : this(caption, null) { }

        public DateTimeInlineElement(string caption, DateTime? date) : base(caption, date) {
            this.DateTimeFormat = Globalization.DateTimeFormat.ShortFullDateTimePattern;
        }

        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            if (!this.IsPickerVisible) {
                if (_datePicker == null) {
                    _datePicker = CreatePicker();
                    // Whenever the DatePicker is changed, get the DateTimeElement cell and change the text.
                    _datePicker.ValueChanged += (s, e) => OnDateTimeFromPicker(_datePicker.Date);
                }

                _datePicker.Date = DateTimeToPickerDateTime(Value.HasValue ? Value.Value : DateTime.UtcNow).ToNSDate();

                // ToArray prevents collection modified during enumeration
                var pickerElements =
                    this.GetImmediateRootElement()
                        .Sections.SelectMany(x => x.Elements.OfType<IInlinePickerElement>())
                        .ToArray();

                foreach (var e in pickerElements) {
                    if (e != this) {
                        e.HidePicker(dvc);
                    }
                }
            }

            TogglePicker(dvc, tableView, this.IndexPath ?? path);

            // Deselect the row so the row highlint tint fades away.
            tableView.DeselectRow(path, true);
        }

        // This fixes a bug where british summer time screws up the time.
        //
        // - MvvmCross implementation forces local time
        // - This works with all timezones except for London in British Summer Time where
        //   an hour is added to the selected time in the picker.
        // - This bug only affects the timepicker, not when also used with a date.
        protected override DateTime DateTimeToPickerDateTime(DateTime simpleDate) {
            return DateTime.SpecifyKind(simpleDate, DateTimeKind.Utc);
        }
        protected override DateTime DateTimeFromPickerDateTime(NSDate simpleDate) {
            return DateTime.SpecifyKind(simpleDate.ToDateTime(), DateTimeKind.Utc);
        }
        public override UIDatePicker CreatePicker() {
            var picker = base.CreatePicker();
            
            picker.TimeZone = NSTimeZone.FromName("UTC");

            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 4)) {
                picker.PreferredDatePickerStyle = UIDatePickerStyle.Wheels;
            }

            return picker;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);
        
            cell.Accessory = UITableViewCellAccessory.None;

            return cell;
        }

        private UIColor _textColorBeforeSelected;

        public void TogglePicker(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            var sectionAndIndex = GetMySectionAndIndex(dvc);

            if (sectionAndIndex.Key != null) {
                Section section = sectionAndIndex.Key;
                int index = sectionAndIndex.Value;

                var cell = tableView.CellAt(path);

                if (_isPickerPresent) {
                    if (cell?.DetailTextLabel != null && _textColorBeforeSelected != null) {
                        cell.DetailTextLabel.TextColor = _textColorBeforeSelected;
                    }

                    section.Remove(_datePickerContainer);
                    _isPickerPresent = false;
                }
                else {
                    if (cell?.DetailTextLabel != null) {
                        _textColorBeforeSelected = cell.DetailTextLabel.TextColor;

                        cell.DetailTextLabel.TextColor = TintColor;
                    }

                    _datePickerContainer = new UIViewElement(String.Empty, _datePicker, false);
                    
                    section.Insert(index + 1, UITableViewRowAnimation.Bottom, _datePickerContainer);
                    _isPickerPresent = true;

                    tableView.ScrollToRow(_datePickerContainer.IndexPath, UITableViewScrollPosition.None, true);
                }
            }
        }

        /// <summary>
        /// Locates this instance of this Element within a given DialogViewController.
        /// </summary>
        /// <returns>The Section instance and the index within that Section of this instance.</returns>
        /// <param name="dvc">Dvc.</param>
        private KeyValuePair<Section, int> GetMySectionAndIndex(DialogViewController dvc) {
            foreach (var section in dvc.Root) {
                for (int i = 0; i < section.Count; i++) {
                    if (section[i] == this) {
                        return new KeyValuePair<Section, int>(section, i);
                    }
                }
            }
            return new KeyValuePair<Section, int>();
        }

        #region IInlinePickerElement

        public bool IsPickerVisible {
            get { return _isPickerPresent; }
        }

        public void ShowPicker(DialogViewController dvc) {
            if (!IsPickerVisible && this.IndexPath != null) {
                TogglePicker(dvc, this.GetContainerTableView(), this.IndexPath);
            }
        }

        public void HidePicker(DialogViewController dvc) {
            if (IsPickerVisible && this.IndexPath != null) {
                TogglePicker(dvc, this.GetContainerTableView(), this.IndexPath);
            }
        }

        #endregion
    }
}