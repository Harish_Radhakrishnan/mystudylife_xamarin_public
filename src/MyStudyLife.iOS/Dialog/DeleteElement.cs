using CrossUI.iOS.Dialog.Elements;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public sealed class DeleteElement : StringElement {
        public DeleteElement(string caption) : base(caption) {
            this.Alignment = UITextAlignment.Center;
            this.ShouldDeselectAfterTouch = true;
        }
        
        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);

            cell.TextLabel.TextColor = Resources.Colors.AttentionForeground; //new UIColor(0.945f, 0.231f, 0.129f, 1f);

            return cell;
        }
    }
}