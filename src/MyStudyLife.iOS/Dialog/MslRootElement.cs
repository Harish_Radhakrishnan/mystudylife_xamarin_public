using System;
using System.Windows.Input;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using CrossUI.iOS.Dialog;
using System.Linq;

namespace MyStudyLife.iOS.Dialog {
    public class MslRootElement : MvxRootElement {
        private readonly ICommand _newCommand;

        public MslRootElement(string caption = null) : base(caption) {}

        public MslRootElement(ICommand newCommand) {
            this._newCommand = newCommand;
        }

        public MslRootElement(string caption, Group group, ICommand newCommand = null) : base(caption, group) {
            this._newCommand = newCommand;
        }

        protected override void PrepareDialogViewController(UIViewController viewController) {
            var dvc = (DialogViewController)viewController;

            if (dvc.TableView != null) {
                dvc.TableView.BackgroundColor = Resources.Colors.Background;
                dvc.TableView.SeparatorColor = Resources.Colors.Border;
            }

            if (_newCommand != null) {
                Action setNewNavigationItem = () => {
                    dvc.NavigationItem.SetRightBarButtonItem(
                        _newCommand.CanExecute(null) ? new UIBarButtonItem(UIBarButtonSystemItem.Add).WithCommand(this._newCommand) : null,
                        true
                    );
                };

                _newCommand.CanExecuteChanged += (s, e) => setNewNavigationItem();
                setNewNavigationItem();
            }

            base.PrepareDialogViewController(dvc);
        }

        public void Selected(DialogViewController dvc) {
            this.Selected(dvc, null, null);
        }

        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            if (tableView != null && path != null) {
                tableView.DeselectRow(path, false);
            }

            var newDvc = MakeViewController();
            PrepareDialogViewController(newDvc);
            dvc.ActivateController(newDvc);
        }
    }

    public class MslStaticRootElement : RootElement, ITintable {
        private UIColor _tintColor;

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    this.SelectMany(x => x.Elements)
                        .OfType<ITintable>()
                        .Apply(x => x.TintColor = _tintColor);
                }
            }
        }

        public MslStaticRootElement(string caption = null) : base(caption) {}

        public MslStaticRootElement(string caption, Group group) : base(caption, group) { }

        protected override void PrepareDialogViewController(UIViewController viewController) {
            var dvc = (DialogViewController)viewController;

            if (dvc.TableView != null) {
                dvc.TableView.BackgroundColor = Resources.Colors.Background;
                dvc.TableView.SeparatorColor = Resources.Colors.Border;
            }

            base.PrepareDialogViewController(dvc);
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);
            cell.TintColor = TintColor;
            return cell;
        }
    }
}