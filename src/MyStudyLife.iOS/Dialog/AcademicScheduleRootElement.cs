using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using CrossUI.iOS.Dialog.Elements;
using MyStudyLife.Data;

namespace MyStudyLife.iOS.Dialog {
    internal sealed class AcademicScheduleRootElement : MslRootElement {
        private readonly bool _allowNone;
        private ObservableCollection<AcademicYear> _academicYears;
        private IAcademicSchedule _selectedSchedule;

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return _academicYears; }
            set {
                _academicYears = value;

                SetElementsFromItemsSource();
            }
        }

        public IAcademicSchedule SelectedSchedule {
            get { return _selectedSchedule; }
            set {
                _selectedSchedule = value;

                RadioSelected = GetSelectedIndexFromSelectedSchedule(value);
            }
        }

        public event EventHandler SelectedScheduleChanged;

        public AcademicScheduleRootElement(ICommand newCommand = null, bool allowNone = true) : base(newCommand) {
            this.Caption = Resources.Strings.AcademicYearOrTerm;
            this.Group = new RadioGroup("AcademicSchedule", 0);
            this._allowNone = allowNone;

            if (allowNone) {
                this.Add(new Section {
                    new MslRadioElement(Resources.Strings.None, "AcademicSchedule")
                });
            }

            RadioSelectedChanged += (s, args) => {
                RadioElement selectedElement = null;

                if (this.Sections.Count > (this._allowNone ? 1 : 0)) {
                    selectedElement = this.Sections[(this._allowNone ? 1 : 0)].Elements.Cast<RadioElement>().SingleOrDefault(x => x.RadioIdx == this.RadioSelected);
                }
                
                _selectedSchedule = (IAcademicSchedule) selectedElement?.ObjectValue;

                SelectedScheduleChanged?.Invoke(this, EventArgs.Empty);
            };
        }

        public AcademicScheduleRootElement(ObservableCollection<AcademicYear> years, IAcademicSchedule selectedSchedule) : this() {
            _academicYears = years;
            _selectedSchedule = selectedSchedule;
        }

        private int GetSelectedIndexFromSelectedSchedule(IAcademicSchedule schedule) {
            if (schedule == null || this.Sections.Count < (this._allowNone ? 1 : 0)) {
                return 0;
            }

            return (
                from element in this.Sections[this._allowNone ? 1 : 0].Elements.Cast<RadioElement>()
                where ((IAcademicSchedule)element.ObjectValue).Guid == schedule.Guid
                select element.RadioIdx
            ).FirstOrDefault();
        }

        private void SetElementsFromItemsSource() {
            var section = this.Sections.ElementAtOrDefault((this._allowNone ? 1 : 0));

            if (section != null) {
                section.Clear();    
            }
            else {
                this.Add(section = new Section());
            }

            // TODO(iOS) lonely section?

            var elements = new List<MslRadioElement>();

            int i = (this._allowNone ? 1 : 0), selectedIndex = 0;

            if (this.AcademicYears != null) {
                var selectedScheduleGuid = this.SelectedSchedule != null
                    ? new Guid?(this.SelectedSchedule.Guid)
                    : null;

                foreach (var year in this.AcademicYears) {
                    elements.Add(new AcademicScheduleElement(year) {
                        RadioIdx = i
                    });

                    if (year.Guid == selectedScheduleGuid) {
                        selectedIndex = i;
                    }

                    i++;

                    foreach (var term in year.Terms) {
                        elements.Add(new AcademicScheduleElement(term) {
                            RadioIdx = i
                        });

                        if (term.Guid == selectedScheduleGuid) {
                            selectedIndex = i;
                        }

                        i++;
                    }
                }
            }

            section.AddAll(elements);

            this.RadioSelected = selectedIndex;
        }
    }
}