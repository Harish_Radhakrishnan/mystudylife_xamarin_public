using System;
using System.Linq;
using CrossUI.iOS.Dialog.Elements;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class MvxRootElement : RootElement, ITintable {
        public event EventHandler RadioSelectedItemChanged;

        private object _radioSelectedItem;
        private UIColor _tintColor;

        public object RadioSelectedItem {
            get { return _radioSelectedItem; }
            set {
                _radioSelectedItem = value;

                SetRadioSelectedIndexFromItem(value);
            }
        }

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    this.OfType<ITintable>().Union(
                        this.SelectMany(x => x.Elements)
                        .OfType<ITintable>()
                    ).Apply(x => x.TintColor = _tintColor);
                }
            }
        }

        public MvxRootElement(string caption = null) : base(caption) {
            this.RadioSelectedChanged += OnRadioSelectedChanged;
        }

        public MvxRootElement(string caption, Group group) : base(caption, group) {
            this.RadioSelectedChanged += OnRadioSelectedChanged;
        }

        private void OnRadioSelectedChanged(object sender, EventArgs e) {
            this.UpdateCellDisplay(this.CurrentAttachedCell);

            var selectedElement = GetRadioElementFromIndex(RadioSelected);

            if (selectedElement != null && this.RadioSelectedItem != selectedElement.ObjectValue) {
                this.RadioSelectedItem = selectedElement.ObjectValue;

                RadioSelectedItemChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private void SetRadioSelectedIndexFromItem(object item) {
            this.RadioSelectedChanged -= OnRadioSelectedChanged;

            var selectedElement = GetRadioElementFromItem(item);

            this.RadioSelected = selectedElement != null ? selectedElement.RadioIdx : -1;
            
            this.UpdateCellDisplay(this.CurrentAttachedCell);

            this.RadioSelectedChanged += OnRadioSelectedChanged;
        }

        protected override void UpdateCellDisplay(UITableViewCell cell) {
            if (cell != null) {
                cell.TintColor = TintColor;

                UpdateDetailDisplay(cell);
            }

            base.UpdateCellDisplay(cell);
        }

        protected virtual void UpdateDetailDisplay(UITableViewCell cell) {
            if (cell == null || cell.DetailTextLabel == null) {
                return;
            }

            var selectedElement = GetRadioElementFromIndex(this.RadioSelected);

            string newDetailText = selectedElement != null ? selectedElement.Summary() : String.Empty;

            if (cell.DetailTextLabel.Text != newDetailText) {
                cell.DetailTextLabel.Text = newDetailText;

                // Fixes bug where detail text doesn't appear unless the whole table is reloaded
                if (this.IndexPath != null) {
                    var tableView = GetContainerTableView();

                    if (tableView != null) {
                        tableView.ReloadRows(
                            new[] {IndexPath},
                            UITableViewRowAnimation.None
                        );
                    }
                }
            }
        }

        private RadioElement GetRadioElementFromItem(object item) {
            foreach (var section in this.Sections) {
                foreach (var element in section.Elements.OfType<RadioElement>()) {
                    if (
                        (element.ObjectValue == null && item == null) ||
                        (element.ObjectValue != null && item != null && element.ObjectValue.Equals(item))
                    ) {
                        return element;
                    }
                }
            }

            return null;
        }

        private RadioElement GetRadioElementFromIndex(int index) {
            foreach (var section in this.Sections) {
                foreach (var element in section.Elements.OfType<RadioElement>()) {
                    if (element.RadioIdx == index) {
                        return element;
                    }
                }
            }

            return null;
        }
    }
}