﻿using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public interface ITintable {
        UIColor TintColor { get; set; }
    }
}
