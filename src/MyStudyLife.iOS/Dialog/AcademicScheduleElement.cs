using System;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using MyStudyLife.Data;

namespace MyStudyLife.iOS.Dialog {
    public class AcademicScheduleElement : MslRadioElement {
        private static readonly NSString _key = new NSString("AcademicScheduleElement");

        private IAcademicSchedule _schedule;

        public AcademicScheduleElement(IAcademicSchedule schedule) {
            this.ObjectValue = _schedule = schedule;

            var term = schedule as AcademicTerm;
            bool isTerm = term != null;

            this.Caption = isTerm ? term.Name : schedule.ToString();

            this.IndentationLevel = isTerm ? 1 : 0;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(_key) ?? new UITableViewCell(UITableViewCellStyle.Subtitle, _key) {
                SelectionStyle = UITableViewCellSelectionStyle.None
            };

            // UITableViewCell has an indentation property but this is better
            var inset = cell.SeparatorInset;

            cell.SeparatorInset = new UIEdgeInsets(
                inset.Top,
                inset.Left * (IndentationLevel + 1),
                inset.Bottom,
                inset.Right
            );

            SubscribeToRoot();

            return cell;
        }

        protected override void UpdateCaptionDisplay(UITableViewCell cell) {
            if (cell == null || cell.TextLabel == null || cell.DetailTextLabel == null) {
                return;
            }

            cell.TextLabel.Text = this.Caption;
            cell.TextLabel.SetNeedsDisplay();

            cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;
            cell.DetailTextLabel.Text = this._schedule.Dates;
            cell.DetailTextLabel.SetNeedsDisplay();
        }

        private bool _alreadySubscribed;

        private void SubscribeToRoot() {
            if (_alreadySubscribed) {
                return;
            }

            var root = (RootElement)Parent.Parent;

            if (!(root.Group is RadioGroup))
                throw new Exception("The RootElement's Group is null or is not a RadioGroup");

            root.RadioSelectedChanged += RootOnRadioSelectedChanged;
            _alreadySubscribed = true;
        }

        private void RootOnRadioSelectedChanged(object sender, EventArgs eventArgs) {
            var cell = GetActiveCell();
            UpdateCellDisplay(cell);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _schedule = null;
            }

            base.Dispose(disposing);
        }
    }
}