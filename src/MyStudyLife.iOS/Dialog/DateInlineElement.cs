using System;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class DateInlineElement : DateTimeInlineElement {
        private DateTime? _minDate, _maxDate;

        public DateTime? MinDate {
            get { return _minDate; }
            set {
                if (_minDate != value) {
                    _minDate = value;
                    UpdatePickerMinAndMaxDates();
                }
            }
        }

        public DateTime? MaxDate {
            get { return _maxDate; }
            set {
                if (_maxDate != value) {
                    _maxDate = value;
                    UpdatePickerMinAndMaxDates();
                }
            }
        }

        public DateInlineElement(string caption = null) : this(caption, DateTimeEx.Now) { }

        public DateInlineElement(string caption, DateTime date) : base(caption, date) {
            DateTimeFormat = Globalization.DateTimeFormat.ShortLongDatePattern;
        }

        public override UIDatePicker CreatePicker() {
            var picker = base.CreatePicker();
            picker.Mode = UIDatePickerMode.Date;
            UpdatePickerMinAndMaxDates(picker);
            return picker;
        }

        protected void UpdatePickerMinAndMaxDates(UIDatePicker picker = null) {
            picker = picker ?? this.DatePicker;

            if (picker != null) {
                picker.MinimumDate = (MinDate != null ? (DateTime?)DateTimeToPickerDateTime(MinDate.Value) : null)?.ToNSDate();
                picker.MaximumDate = (MaxDate != null ? (DateTime?)DateTimeToPickerDateTime(MaxDate.Value) : null)?.ToNSDate();
            }
        }
    }
}