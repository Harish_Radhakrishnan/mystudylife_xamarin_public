using System;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class TimeInlineElement : DateTimeInlineElement {
        public TimeInlineElement(string caption = null) : this(caption, null) { }

        public TimeInlineElement(string capation, DateTime? date) : base(capation, date) {
            DateTimeFormat = "t";
        }

        public override UIDatePicker CreatePicker() {
            var picker = base.CreatePicker();

            picker.Mode = UIDatePickerMode.Time;
            
            return picker;
        }
    }
}