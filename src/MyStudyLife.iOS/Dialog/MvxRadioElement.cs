using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class MslRadioElement : RadioElement, ITintable {
        private UIColor _tintColor;

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    var cac = CurrentAttachedCell;
                    if (cac != null) {
                        cac.TintColor = value;
                    }
                }
            }
        }

        public int IndentationLevel { get; set; }

        public sealed override object ObjectValue { get; set; }

        public MslRadioElement() { }
        public MslRadioElement(string caption = null) : base(caption) { }
        public MslRadioElement(string caption, string group) : base(caption, group) { }
        
        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath indexPath) {
            base.Selected(dvc, tableView, indexPath);
            
            dvc.DeactivateController(true);
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);

            // UITableViewCell has an indentation property but this is better
            var inset = cell.SeparatorInset;

            cell.TintColor = TintColor;
            cell.SeparatorInset = new UIEdgeInsets(
                inset.Top,
                inset.Left * (IndentationLevel + 1),
                inset.Bottom,
                inset.Right
            );

            return cell;
        }
    }
}