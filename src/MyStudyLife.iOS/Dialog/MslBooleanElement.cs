using System;
using System.Windows.Input;
using CrossUI.iOS.Dialog.Elements;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    internal class MslBooleanElement : BooleanElement, ITintable {
        private UIColor _tintColor;

        public UIColor TintColor {
            get { return _tintColor ?? Resources.Colors.Accent; }
            set {
                if (!Equals(_tintColor, value)) {
                    _tintColor = value;

                    var swtch = Switch;
                    if (swtch != null) {
                        swtch.OnTintColor = value;
                    }
                }
            }
        }

        public ICommand CheckedCommand { get; set; }

        public MslBooleanElement(string caption) {
            this.Caption = caption;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);

            cell.TextLabel.Font = Resources.Fonts.FontMedium;
            
            return cell;
        }

        protected override UISwitch CreateSwitch() {
            var sw = base.CreateSwitch();

            sw.OnTintColor = TintColor;
            sw.ValueChanged += SwitchOnValueChanged;

            return sw;
        }

        private void SwitchOnValueChanged(object sender, EventArgs e) {
            CheckedCommand?.Execute(((UISwitch)sender).On);
        }
    }
}