using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using CrossUI.iOS.Dialog.Elements;
using MvvmCross.WeakSubscription;

namespace MyStudyLife.iOS.Dialog {
    public class MvxSection : Section {
        private readonly Func<object, Element> _getElement;

        #region ItemsSource

        private MvxNotifyCollectionChangedEventSubscription _itemsSourceSubscription;

        private IEnumerable _itemsSource;

        public IEnumerable ItemsSource {
            get { return _itemsSource; }
            set {
                if (_itemsSourceSubscription != null) {
                    _itemsSourceSubscription.Dispose();
                }

                _itemsSource = value;

                if (value == null) {
                    return;
                }

                this.SetElementsFromItemsSource();

                var observableCollection = value as INotifyCollectionChanged;

                if (observableCollection != null) {
                    _itemsSourceSubscription = observableCollection.WeakSubscribe(ItemsSourceOnCollectionChanged);
                }
            }
        }

        private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            // TODO: Use something like InsertVisual(Elements.Count - 1, UITableViewRowAnimation.None, 1); ??????

            this.SetElementsFromItemsSource();
        }

        #endregion

        /// <summary>
        ///     A fixed element that always appears
        ///     at the top of the section.
        /// </summary>
        public virtual Element HeaderElement {
            get { return null; }
        }

        /// <summary>
        ///     A fixed element that always appears
        ///     at the bottom of the section.
        /// </summary>
        public virtual Element FooterElement {
            get { return null; }
        }

        public MvxSection(Func<object, Element> getElement = null) {
            this._getElement = getElement;
        }

        protected virtual void SetElementsFromItemsSource() {
            // Note we're pretty much emulating the base Add/Insert/Clear
            // methods but not Disposing the Header/Footer elements. We're
            // also not trying to do multiple animations at once!
            if (Elements.Count > 0) {
                foreach (var element in this.Elements) {
                    if (element != this.HeaderElement && element != this.FooterElement) {
                        element.Dispose();
                    }
                }

                this.Elements.Clear();
            }

            if (this.HeaderElement != null) {
                this.HeaderElement.Parent = this;
                this.Elements.Add(this.HeaderElement);
            }

            if (ItemsSource != null) {
                this.Elements.AddRange(ItemsSource.Cast<object>().Select(x => {
                    var element = CreateElement(x);

                    element.Parent = this;

                    return element;
                }));
            }

            if (this.FooterElement != null) {
                this.FooterElement.Parent = this;
                this.Elements.Add(this.FooterElement);
            }

            var root = Parent as RootElement;

            if (root != null && root.TableView != null) {
                root.TableView.ReloadData();
            }
        }

        protected virtual Element CreateElement(object item) {
            return _getElement?.Invoke(item) ?? new StringElement(item.ToString());
        }
    }
}