namespace MyStudyLife.iOS.Dialog {
    public class NewElement : NavigationElement {
        public NewElement(string caption = Resources.Strings.New) : base(caption) { }
    }
}