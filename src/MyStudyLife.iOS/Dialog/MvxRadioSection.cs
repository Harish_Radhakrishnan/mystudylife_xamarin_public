using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using CrossUI.iOS.Dialog.Elements;
using MvvmCross.WeakSubscription;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    public class MvxRadioSection : Section, ITintable {
        private readonly Func<object, RadioElement> _getElement;
        private int _startIndex;
        private UIColor _tintColor;

        public int StartIndex {
            get { return _startIndex; }
            set {
                if (value < 0) {
                    throw new ArgumentException("Cannot be less than 0");
                }

                _startIndex = value;
            }
        }

        public UIColor TintColor {
            get { return _tintColor ?? UITableViewCell.Appearance.TintColor; }
            set {
                if (_tintColor != value) {
                    _tintColor = value;

                    this.Elements
                        .OfType<ITintable>()
                        .Apply(x => x.TintColor = _tintColor);
                }
            }
        }

        #region ItemsSource

        private MvxNotifyCollectionChangedEventSubscription _itemsSourceSubscription;

        private IEnumerable _itemsSource;

        public IEnumerable ItemsSource {
            get { return _itemsSource; }
            set {
                if (_itemsSourceSubscription != null) {
                    _itemsSourceSubscription.Dispose();
                }

                _itemsSource = value;

                if (value == null) {
                    return;
                }

                this.SetElementsFromItemsSource();

                var observableCollection = value as INotifyCollectionChanged;

                if (observableCollection != null) {
                    _itemsSourceSubscription = observableCollection.WeakSubscribe(ItemsSourceOnCollectionChanged);
                }
            }
        }

        private void ItemsSourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            // TODO: Use something like InsertVisual(Elements.Count - 1, UITableViewRowAnimation.None, 1); ??????

            this.SetElementsFromItemsSource();
        }

        #endregion

        public MvxRadioSection(Func<object, RadioElement> getElement = null) {
            _getElement = getElement;
        }

        protected void SetElementsFromItemsSource() {
            this.Clear();

            if (ItemsSource == null) {
                return;
            }

            int i = StartIndex;

            foreach (var item in ItemsSource) {
                var element = CreateElement(item);
                
                element.RadioIdx = i;

                this.Add(element);

                i++;
            }
        }

        private RadioElement CreateElement(object item) {
            var element = _getElement?.Invoke(item) ?? new MslRadioElement(item.ToString());

            var tintableElement = element as ITintable;
            if (tintableElement != null) {
                tintableElement.TintColor = TintColor;
            }

            return element;
        }
    }
}