// MvvmCross stopped supporting this in V5, so integrated into project. License below.

// MvvmCross is licensed using Microsoft Public License (Ms-PL)
// Contributions and inspirations noted in readme.md and license.txt

using System;
using System.Reflection;
using CrossUI.iOS.Dialog.Elements;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;

namespace MyStudyLife.iOS.Dialog.Binding
{
    public class MvxValueElementValueBinding : MvxPropertyInfoTargetBinding<ValueElement> {
        public MvxValueElementValueBinding(object target, PropertyInfo targetPropertyInfo) : base(target, targetPropertyInfo) {
            var valueElement = this.View;
            if (valueElement == null) {
                Mvx.IoCProvider.Resolve<ILogger<MvxValueElementValueBinding>>().LogError("Error - valueElement is null in MvxValueElementValueBinding");
            }
            else {
                valueElement.ValueChanged += this.ValueElementOnValueChanged;
            }
        }

        private void ValueElementOnValueChanged(object sender, EventArgs eventArgs) {
            this.FireValueChanged(this.View.ObjectValue);
        }

        public override MvxBindingMode DefaultMode => MvxBindingMode.TwoWay;

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                var valueElement = this.View;
                if (valueElement != null) {
                    valueElement.ValueChanged -= this.ValueElementOnValueChanged;
                }
            }

            base.Dispose(isDisposing);
        }
    }
}