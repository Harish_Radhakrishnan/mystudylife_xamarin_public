using System;
using MvvmCross.Binding;
using MvvmCross.Binding.Bindings.Target;

namespace MyStudyLife.iOS.Dialog.Binding {
    public class MvxRootElementRadioSelectedItemBinding : MvxTargetBinding {
        public const string Name = "RadioSelectedItem";

        public new MvxRootElement Target {
            get { return (MvxRootElement) base.Target; }
        }

        public MvxRootElementRadioSelectedItemBinding(MvxRootElement target) : base(target) { }

        public override void SubscribeToEvents() {
            this.Target.RadioSelectedItemChanged += TargetOnValueChanged;
        }

        public override void SetValue(object value) {
            this.Target.RadioSelectedItem = value;
        }

        public override Type TargetType {
            get { return typeof(object); }
        }

        public override MvxBindingMode DefaultMode {
            get { return MvxBindingMode.TwoWay; }
        }

        protected override void Dispose(bool isDisposing) {
            if (isDisposing) {
                this.Target.RadioSelectedItemChanged -= TargetOnValueChanged;
            }

            base.Dispose(isDisposing);
        }

        private void TargetOnValueChanged(object sender, EventArgs e) {
            if (this.Target == null) return;

            this.FireValueChanged(this.Target.RadioSelectedItem);
        }
    }
}