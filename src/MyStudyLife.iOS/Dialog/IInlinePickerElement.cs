using CrossUI.iOS.Dialog;

namespace MyStudyLife.iOS.Dialog {
    public interface IInlinePickerElement {
        bool IsPickerVisible { get; }

        void ShowPicker(DialogViewController dvc);

        void HidePicker(DialogViewController dvc);
    }
}