using Foundation;
using UIKit;
using MyStudyLife.Data;

namespace MyStudyLife.iOS.Dialog {
    public class ExamElement : MslRadioElement {
        // ReSharper disable once InconsistentNaming
        private static readonly NSString _cellKey = new NSString("ExamElementTableViewCell");

        public ExamElement(Exam exam) {
            this.ObjectValue = exam;

            this.Caption = exam.Title;
            this.Value = exam.Date.ToString("f");
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(_cellKey) ?? new UITableViewCell(UITableViewCellStyle.Subtitle, _cellKey);

            if (cell.DetailTextLabel != null) {
                cell.DetailTextLabel.TextColor = Resources.Colors.SubtleText;
            }

            cell.TintColor = TintColor;

            return cell;
        }
    }
}