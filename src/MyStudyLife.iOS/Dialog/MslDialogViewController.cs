using System;
using System.Collections.Generic;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MyStudyLife.UI.ViewModels.Base;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    internal class MslDialogViewController<TViewModel> : MvxEventSourceDialogViewController, IMvxIosView<TViewModel>, IMvxBindingContextOwner where TViewModel : BaseViewModel {
        public MslDialogViewController() : base(pushing: true) {
            this.AdaptForBinding();
        }
        public MslDialogViewController(IntPtr handle) : base(handle) {
            this.AdaptForBinding();
        }

        public bool DismissKeyboardOnTap { get; set; } = true;

        public object DataContext {
            get => this.BindingContext.DataContext;
            set => this.BindingContext.DataContext = value;
        }

        public TViewModel ViewModel {
            get => this.DataContext as TViewModel;
            set => this.DataContext = value;
        }

        public MvxViewModelRequest Request { get; set; }

        public IMvxBindingContext BindingContext { get; set; }

        protected T Bind<T>(T element, string bindingDescription) {
            return element.Bind(this, bindingDescription);
        }

        protected T Bind<T>(T element, IEnumerable<MvxBindingDescription> bindingDescription) {
            return element.Bind(this, bindingDescription);
        }

        public MvxFluentBindingDescriptionSet<IMvxIosView<TViewModel>, TViewModel> CreateBindingSet() {
            return new MvxFluentBindingDescriptionSet<IMvxIosView<TViewModel>, TViewModel>(this);
        }

        public override void ViewDidLoad() {
            if (DismissKeyboardOnTap) {
                var tap = new UITapGestureRecognizer(() => View.EndEditing(true)) {
                    CancelsTouchesInView = false
                };

                View.AddGestureRecognizer(tap);
            }

            base.ViewDidLoad();

            TableView.SeparatorColor = Resources.Colors.Border;
        }

        public override void ViewWillAppear(bool animated) {
            base.ViewWillAppear(animated);

            if (NavigationController != null) {
                InitializeNavigationBar(NavigationController.NavigationBar);
            }

            View.BackgroundColor = Resources.Colors.Background;
        }

        protected virtual void InitializeNavigationBar(UINavigationBar navigationBar) {
            navigationBar.ApplyAppearance();
        }

        public override bool ShouldAutorotate() => false;

        public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations()
            => UIInterfaceOrientationMask.Portrait;

        IMvxViewModel IMvxView.ViewModel {
            get => ViewModel;
            set => ViewModel = (TViewModel)value;
        }
    }
}