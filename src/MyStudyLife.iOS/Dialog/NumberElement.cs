using System;
using System.Collections.Generic;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    /// <summary>
    ///     An element for inputting an integer
    ///     number.
    /// </summary>
    public class NumberElement : ValueElement<int>, IInlinePickerElement {
        private static readonly NSString Key = new NSString("NumberElement");

        private bool _isPickerPresent = false;
        private UIViewElement _pickerContainer;
        private UIPickerView _picker;
        private int _min = 1;
        private int _max = 5;

        public int Min {
            get { return _min; }
            set { _min = value; }
        }

        public int Max {
            get { return _max; }
            set { _max = value; }
        }

        public string Suffix { get; set; }

        public NumberElement(string caption = null) : base(caption) { }

        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            if (_picker == null) {
                _picker = new UIPickerView {
                    Model = new ViewModel(this)
                };
                // NMath.Min(NMath.Max(..)) ensures the value we are setting is in the picker,
                // else it will throw an NSRangeException
                _picker.Select(NMath.Min(NMath.Max((nint)Value - _min, 0), _max), 0, false);
            }

            TogglePicker(dvc, tableView, path);

            // Deselect the row so the row highlint tint fades away.
            tableView.DeselectRow(path, true);
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(Key) ?? new UITableViewCell(UITableViewCellStyle.Value1, Key);

            UpdateDetailDisplay(cell);

            return cell;
        }

        protected override void UpdateDetailDisplay(UITableViewCell cell) {
            if (cell == null || cell.DetailTextLabel == null) {
                return;
            }

            cell.DetailTextLabel.Text = !String.IsNullOrWhiteSpace(Suffix) ? String.Concat(Value, " ", Suffix) : Value.ToString();
        }

        private UIColor _textColorBeforeSelected;

        private void TogglePicker(DialogViewController dvc, UITableView tableView, NSIndexPath path) {
            var sectionAndIndex = GetMySectionAndIndex(dvc);

            if (sectionAndIndex.Key != null) {
                Section section = sectionAndIndex.Key;
                int index = sectionAndIndex.Value;

                var cell = tableView.CellAt(path);

                if (_isPickerPresent) {
                    if (cell.DetailTextLabel != null && _textColorBeforeSelected != null) {
                        cell.DetailTextLabel.TextColor = _textColorBeforeSelected;
                    }

                    section.Remove(_pickerContainer);
                    _isPickerPresent = false;
                }
                else {
                    if (cell.DetailTextLabel != null) {
                        _textColorBeforeSelected = cell.DetailTextLabel.TextColor;

                        cell.DetailTextLabel.TextColor = UILabel.Appearance.TintColor;
                    }

                    _pickerContainer = new UIViewElement(String.Empty, _picker, false);

                    section.Insert(index + 1, UITableViewRowAnimation.Bottom, _pickerContainer);
                    _isPickerPresent = true;

                    tableView.ScrollToRow(_pickerContainer.IndexPath, UITableViewScrollPosition.None, true);
                }
            }
        }

        /// <summary>
        /// Locates this instance of this Element within a given DialogViewController.
        /// </summary>
        /// <returns>The Section instance and the index within that Section of this instance.</returns>
        /// <param name="dvc">Dvc.</param>
        private KeyValuePair<Section, int> GetMySectionAndIndex(DialogViewController dvc) {
            foreach (var section in dvc.Root) {
                for (int i = 0; i < section.Count; i++) {
                    if (section[i] == this) {
                        return new KeyValuePair<Section, int>(section, i);
                    }
                }
            }
            return new KeyValuePair<Section, int>();
        }

        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
            if (disposing) {
                if (_picker != null) {
                    _picker.Dispose();
                    _picker = null;
                }
            }
        }

        class ViewModel : UIPickerViewModel {
            private readonly NumberElement _element;

            private readonly int _min;
            private readonly int _max;
            private readonly string _suffix;
            private readonly bool _hasSuffix;

            public ViewModel(NumberElement element) {
                this._element = element;

                this._min = element.Min;
                this._max = element.Max;
                this._suffix = element.Suffix;
                this._hasSuffix = !String.IsNullOrWhiteSpace(this._suffix);
            }

            public override nint GetComponentCount(UIPickerView picker) {
                return 1;
            }

            public override nint GetRowsInComponent(UIPickerView picker, nint component) {
                return (this._max - this._min) + 1;
            }

            public override string GetTitle(UIPickerView picker, nint row, nint component) {
                var number = (int) (row + _min);

                if (_hasSuffix) {
                    return String.Concat(number, " ", _suffix);
                }

                return number.ToString();
            }

            public override void Selected(UIPickerView picker, nint row, nint component) {
                _element.OnUserValueChanged((int) (_min + row));
            }
        }

        #region IInlinePickerElement

        public bool IsPickerVisible {
            get { return _isPickerPresent; }
        }

        public void ShowPicker(DialogViewController dvc) {
            if (!IsPickerVisible && this.IndexPath != null) {
                TogglePicker(dvc, this.GetContainerTableView(), this.IndexPath);
            }
        }

        public void HidePicker(DialogViewController dvc) {
            if (IsPickerVisible && this.IndexPath != null) {
                TogglePicker(dvc, this.GetContainerTableView(), this.IndexPath);
            }
        }

        #endregion
    }
}
