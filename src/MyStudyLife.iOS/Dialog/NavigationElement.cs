using CrossUI.iOS.Dialog.Elements;
using UIKit;

namespace MyStudyLife.iOS.Dialog {
    /// <summary>
    ///     <see cref="StringElement"/> with the disclosure indicator.
    /// </summary>
    public class NavigationElement : StringElement {
        public NavigationElement(string caption) : base(caption) {
            this.ShouldDeselectAfterTouch = true;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = base.GetCellImpl(tv);

            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

            return cell;
        }
    }
}