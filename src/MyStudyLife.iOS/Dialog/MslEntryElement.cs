using System;
using CoreGraphics;
using System.Linq;
using CrossUI.iOS.Dialog;
using CrossUI.iOS.Dialog.Elements;
using Foundation;
using UIKit;
using IElementSizing = CrossUI.iOS.Dialog.Elements.IElementSizing;

namespace MyStudyLife.iOS.Dialog {
    /// <summary>
    ///     Text entry that when nested
    ///     in a section is aligned with other
    ///     elements.
    /// </summary>
    public class MslEntryElement : EntryElement, IFocusableElement, ITintable {
        private UIColor _tintColor;

        public UIColor TintColor {
            get { return _tintColor ?? UITextView.Appearance.TintColor; }
            set {
                if (!Equals(_tintColor, value)) {
                    _tintColor = value;

                    var textView = Entry;
                    if (textView != null) {
                        textView.TintColor = _tintColor;
                    }
                }
            }
        }

        protected override CGSize ComputeEntryPosition(UITableView tv, UITableViewCell cell) {
            var pos = base.ComputeEntryPosition(tv, cell);

            if (this.Parent as Section != null) {
                pos.Width -= 10f;
            }

            return pos;
        }
        
        protected override void MoveFocusToNextElement() {
            RootElement root = GetImmediateRootElement();
            IFocusableElement focus = null;

            if (root == null) {
                return;
            }

            foreach (var s in root.Sections) {
                foreach (var e in s.Elements) {
                    if (e == this) {
                        focus = this;
                    }
                    else if (focus != null && e is IFocusableElement) {
                        focus = e as IFocusableElement;
                        break;
                    }
                }

                if (focus != null && focus != this) {
                    break;
                }
            }

            if (focus == null) {
                return;
            }

            if (focus != this) {
                focus.BecomeFirstResponder(true);
            }
            else {
                focus.ResignFirstResponder(true);
            }
        }

        protected override void EnsureEntryElement(UITableView tv, UITableViewCell cell) {
            base.EnsureEntryElement(tv, cell);

            // Unfortunately we can't reimplement this method as it accesses hidden fields
            // but this should be triggered after the what it attached in the base method.
            // https://github.com/MvvmCross/MvvmCross/blob/v3.1/CrossUI/CrossUI.Touch/Dialog/Elements/EntryElement.cs#L291
            Entry.Started += (sender, args) => {
                EntryElement self = null;

                if (!ReturnKeyType.HasValue) {
                    var returnType = UIReturnKeyType.Default;

                    foreach (var e in this.GetImmediateRootElement().Sections.SelectMany(x => x.Elements)) {
                        if (e == this) {
                            self = this;
                        }
                        else if (self != null && (e is EntryElement || e is MultilineEntryElement)) {
                            returnType = UIReturnKeyType.Next;
                        }
                    }

                    Entry.ReturnKeyType = returnType;
                }
            };

            Entry.TintColor = TintColor;
        }
    }

    public class MslMultilineEntryElement : MultilineEntryElement {
        public MslMultilineEntryElement() : base(null) {}

        protected override CGSize ComputeEntryPosition(UITableView tv, UITableViewCell cell) {
            var pos = base.ComputeEntryPosition(tv, cell);

            if (this.Parent as Section != null) {
                pos.Width -= 10f;
            }

            return pos;
        }
    }

    /// <summary> 
    ///     Author: Jamie Clarke
    /// 
    ///     Derived from EntryElement source code.    
    /// </summary>
    public class MultilineEntryElement : ValueElement<string>, IElementSizing, IFocusableElement, ITintable {
// ReSharper disable InconsistentNaming
        private static readonly NSString _entryKey = new NSString("EntryElement");
        private static readonly NSString _cellKey = new NSString("EntryElement");
// ReSharper restore InconsistentNaming

        protected static readonly UIFont DefaultFont = UIFont.BoldSystemFontOfSize(17);

        private UITextView _entry;
        private UILabel _placeholderView;
        private UIKeyboardType _keyboardType = UIKeyboardType.Default;
        private UIReturnKeyType? _returnKeyType;
        private UITextAutocapitalizationType _autocapitalizationType = UITextAutocapitalizationType.Sentences;
        private UITextAutocorrectionType _autocorrectionType = UITextAutocorrectionType.Default;
        private float _height = 112f;
        private string _placeholder;
        private UIColor _tintColor;

        private bool _becomeResponder;

        public event EventHandler Changed;

        protected UITextView Entry {
            get { return _entry; }
        }

        protected virtual NSString EntryKey {
            get { return _entryKey; }
        }

        protected override NSString CellKey {
            get { return _cellKey; }
        }

        public UIKeyboardType KeyboardType {
            get { return _keyboardType; }
            set {
                _keyboardType = value;

                if (_entry != null) {
                    _entry.KeyboardType = value;
                }
            }
        }

        public UIReturnKeyType? ReturnKeyType {
            get { return _returnKeyType; }
            set {
                _returnKeyType = value;

                if (_entry != null && value.HasValue) {
                    _entry.ReturnKeyType = value.Value;
                }
            }
        }

        public UITextAutocapitalizationType AutocapitalizationType {
            get { return _autocapitalizationType; }
            set {
                _autocapitalizationType = value;

                if (_entry != null) {
                    _entry.AutocapitalizationType = value;
                }
            }
        }

        public UITextAutocorrectionType AutocorrectionType {
            get { return _autocorrectionType; }
            set {
                _autocorrectionType = value;

                if (_entry != null) {
                    _entry.AutocorrectionType = value;
                }
            }
        }

        public float Height {
            get { return _height; }
            set {
// ReSharper disable CompareOfFloatsByEqualityOperator
                if (_height == value) {
                    return;
                }

                _height = value;

                if (_entry != null) {
                    var size = ComputeEntryPosition(GetContainerTableView(), CurrentAttachedCell);

                    if (_entry.Frame.Height != size.Height) {
                        _entry.Frame = new CGRect(
                            _entry.Frame.Left,
                            _entry.Frame.Top,
                            size.Width,
                            size.Height
                        );

                        _entry.LayoutIfNeeded();
                    }
                }
// ReSharper restore CompareOfFloatsByEqualityOperator
            }
        }

        public string Placeholder {
            get { return _placeholder; }
            set {
                _placeholder = value;

                if (_placeholderView != null) {
                    _placeholderView.Text = value;
                }
            }
        }

        public UIColor TintColor {
            get { return _tintColor ?? UITextView.Appearance.TintColor; }
            set {
                if (!Equals(_tintColor, value)) {
                    _tintColor = value;

                    var textView = _entry;
                    if (textView != null) {
                        textView.TintColor = _tintColor;
                    }
                }
            }
        }

        public MultilineEntryElement(string caption) : this(caption, String.Empty) { }

        public MultilineEntryElement(string caption, string placeholder) : this(caption, placeholder, String.Empty) { }

        public MultilineEntryElement(string caption, string placeholder, string value) : base(caption) {
            this.Value = value;
            this.Placeholder = placeholder;
        }
        
        protected override void UpdateDetailDisplay(UITableViewCell cell) {
            if (_entry != null) {
                _entry.Text = Value;
            }

            if (_placeholderView != null) {
                _placeholderView.Text = _placeholder;
                _placeholderView.Hidden = !String.IsNullOrEmpty(Value);
            }
        }

        public override string Summary() {
            return Value;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv) {
            var cell = tv.DequeueReusableCell(CellKey);

            if (cell == null) {
                cell = new UITableViewCell(UITableViewCellStyle.Default, CellKey) {
                    SelectionStyle = UITableViewCellSelectionStyle.None
                };
            }
            else {
                RemoveTag(cell, 1);
            }

            EnsureEntryElement(tv, cell);

            cell.TextLabel.Text = Caption;
            cell.ContentView.AddSubview(_placeholderView);
            cell.ContentView.AddSubview(_entry);

            return cell;
        }

        protected virtual UILabel CreatePlaceholder(CGRect frame) {
            return new UILabel(frame) {
                UserInteractionEnabled = false,
                Font = UIFont.SystemFontOfSize(17),
                TextColor = UIColor.FromWhiteAlpha(0.8f, 1)
            };
        }

        protected virtual UITextView CreateTextView(CGRect frame) {
            return new UITextView(frame) {
                AutoresizingMask = UIViewAutoresizing.FlexibleWidth,
                Text = Value ?? String.Empty,
                Font = UIFont.SystemFontOfSize(17),
                BackgroundColor = UIColor.Clear,
                TextContainer = {
                    LineFragmentPadding = 0
                },
                Tag = 1
            };
        }

        /// <remarks>
        ///     Sets the size of the UITextView ensuring it is no smaller
        ///     than sibling rows but no larger than the specified <see cref="Height"/>.
        /// </remarks>
        protected virtual CGSize ComputeEntryPosition(UITableView tv, UITableViewCell cell) {
            var s = (Section) this.Parent;

            var max = new CGSize(-15, Height);

            foreach (var e in s.Elements) {
                var ee = e as EntryElement;

                if (ee == null) {
                    continue;
                }

                if (ee.Caption != null) {
                    var size = ee.Caption.StringSize(DefaultFont);

                    if (size.Width > max.Width) {
                        max.Width = size.Width;
                    }

                    if (size.Height > max.Height) {
                        max.Height = size.Height;
                    }
                }
            }
            
            s.EntryAlignment = new CGSize(25 + NMath.Min(max.Width, 160), max.Height);

            return s.EntryAlignment;
        }

        protected virtual void EnsureEntryElement(UITableView tv, UITableViewCell cell) {

            if (_entry == null) {
                var size = ComputeEntryPosition(tv, cell);

                var yOffset = NMath.Max((cell.ContentView.Bounds.Height - size.Height) / 2 - 1, 0);
                var width = cell.ContentView.Bounds.Width - size.Width;

                _entry = CreateTextView(new CGRect(size.Width, yOffset, width, size.Height));

                _entry.Changed += (s, e) => FetchAndUpdateValue();
                _entry.Ended += (s, e) => FetchAndUpdateValue();
                _entry.Started += (s, e) => tv.ScrollToRow(IndexPath, UITableViewScrollPosition.Middle, true);

                // Use content view height as that defaults to the default row height
                // before IElementSizing.GetHeight() is called.
                _placeholderView = CreatePlaceholder(
                    new CGRect(
                        size.Width,
                        yOffset - 4f,
                        width,
                        cell.ContentView.Bounds.Height
                    )
                );
            }

            if (_becomeResponder) {
                _entry.BecomeFirstResponder();
                _becomeResponder = false;
            }

            _entry.KeyboardType = KeyboardType;
            _entry.AutocapitalizationType = AutocapitalizationType;
            _entry.AutocorrectionType = AutocorrectionType;
            _entry.TintColor = TintColor;
        }

        /// <summary>
        ///  Copies the value from the UITextView in the MultilineEntryElement to the
        //   Value property and raises the Changed event if necessary.
        /// </summary>
        public void FetchAndUpdateValue() {
            if (_entry == null) {
                return;
            }

            if (_placeholderView != null) {
                _placeholderView.Hidden = !String.IsNullOrEmpty(_entry.Text);
            }

            if (_entry.Text == Value) {
                return;
            }

            OnUserValueChanged(_entry.Text);

            if (Changed != null) {
                Changed(this, EventArgs.Empty);
            }
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                if (_entry != null) {
                    _entry.Dispose();
                    _entry = null;
                }
                if (_placeholderView != null) {
                    _placeholderView.Dispose();
                    _placeholderView = null;
                }
            }
        }

        public override void Selected(DialogViewController dvc, UITableView tableView, NSIndexPath indexPath) {
            BecomeFirstResponder(true);
            tableView.DeselectRow(indexPath, true);
        }

        public override bool Matches(string text) {
            return (Value != null && Value.IndexOf(text, StringComparison.CurrentCultureIgnoreCase) > -1) || base.Matches(text);
        }

        /// <summary>
        /// Makes this cell the first responder (get the focus)
        /// </summary>
        /// <param name="animated">
        /// Whether scrolling to the location of this cell should be animated
        /// </param>
        public void BecomeFirstResponder(bool animated) {
            _becomeResponder = true;
            var tv = GetContainerTableView();

            if (tv == null) {
                return;
            }

            tv.ScrollToRow(IndexPath, UITableViewScrollPosition.Middle, animated);

            if (_entry != null) {
                _entry.BecomeFirstResponder();
                _becomeResponder = false;
            }
        }

        public void ResignFirstResponder(bool animated) {
            _becomeResponder = false;
            var tv = GetContainerTableView();

            if (tv == null) {
                return;
            }

            tv.ScrollToRow(IndexPath, UITableViewScrollPosition.Middle, animated);

            if (_entry != null) {
                _entry.ResignFirstResponder();
            }
        }

        #region IElementSizing

        public nfloat GetHeight(UITableView tv, NSIndexPath indexPath) {
            return Height;
        }

        #endregion
    }

    public interface IFocusableElement {
        void BecomeFirstResponder(bool animated);

        void ResignFirstResponder(bool animated);
    }
}