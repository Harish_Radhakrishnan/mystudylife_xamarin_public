using System.IO;
using System.Security.Cryptography;
using System.Text;
using MyStudyLife.Security.Crypto;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSCryptoProvider : ICryptoProvider {
        public byte[] ComputeHMACSHA1(string content, string privateKey) {
            var algorithm = new HMACSHA1(Encoding.UTF8.GetBytes(privateKey));
            byte[] byteArray = Encoding.UTF8.GetBytes(content);

            using (var cryptStream = new MemoryStream(byteArray)) {
                return algorithm.ComputeHash(cryptStream);
            }
        }
    }
}