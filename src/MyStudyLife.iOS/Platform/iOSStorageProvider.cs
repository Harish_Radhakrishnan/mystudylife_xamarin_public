using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Foundation;
using MyStudyLife.Data;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSStorageProvider : IStorageProvider {
        #region Files

        private static readonly ConcurrentDictionary<string, object> _fileLocks = new ConcurrentDictionary<string, object>();

        private Lazy<string> _appDataDirectoryPath = new Lazy<string>(() =>
            Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library")
        );

        private string AppDataDirectoryPath => _appDataDirectoryPath.Value;

        private string GetFullFilePath(string filePath) {
            Check.NotNullOrEmpty(ref filePath, nameof(filePath));

            return Path.Combine(AppDataDirectoryPath, filePath);
        }

        private object GetFileLock(string fileName) => _fileLocks.GetOrAdd(fileName, new object());

        public Task<string> LoadFileContentsAsync(string filePath) => Task.Run(() => {
            string fullFilePath = GetFullFilePath(filePath);

            lock (GetFileLock(fullFilePath)) {
                if (new FileInfo(fullFilePath).Exists) {
                    return File.ReadAllText(fullFilePath);
                }
            }

            return null;
        });

        public Task<Stream> FileOpenReadAsync(string filePath) {
            string fullFilePath = GetFullFilePath(filePath);

            return Task.FromResult<Stream>(File.Exists(fullFilePath) ? File.OpenRead(fullFilePath) : null);
        }

        public Stream FileOpenRead(string filePath) {
            string fullFilePath = GetFullFilePath(filePath);

            return File.Exists(fullFilePath) ? File.OpenRead(fullFilePath) : null;
        }

        public Task WriteFileContentsAsync(string filePath, string contents) => Task.Run(() => {
            string fullFileName = GetFullFilePath(filePath);

            lock (GetFileLock(fullFileName)) {
                File.WriteAllText(fullFileName, contents);
            }
        });

        public Task RemoveFilesAsync() => Task.Run(() => {
            DirectoryInfo di = new DirectoryInfo(AppDataDirectoryPath);

            var files = di.GetFiles("*.json").ToList();

            foreach (var f in files) {
                lock (GetFileLock(f.FullName)) {
                    f.Delete();
                }
            }
        });

        public DateTimeOffset GetLastModified(string path) => new FileInfo(Path.Combine(AppDataDirectoryPath, path)).LastWriteTime;

        #endregion

        #region Settings

        public T GetSetting<T>(string settingName) {
            var val = NSUserDefaults.StandardUserDefaults.StringForKey(settingName);

            if (val != null) {
                return JsonConvert.DeserializeObject<T>(val);
            }

            return default(T);
        }

        public void AddOrUpdateSetting<T>(string settingName, T settingValue) {
            var settings = NSUserDefaults.StandardUserDefaults;

            settings.SetString(JsonConvert.SerializeObject(settingValue), settingName);

            settings.Synchronize();
        }

        public void RemoveSetting(string settingName) {
            var settings = NSUserDefaults.StandardUserDefaults;

            settings.RemoveObject(new NSString(settingName));

            settings.Synchronize();
        }

        #endregion
    }
}