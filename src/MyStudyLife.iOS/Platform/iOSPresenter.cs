using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Core;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.Platforms.Ios.Presenters;
using MvvmCross.Platforms.Ios.Presenters.Attributes;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using MyStudyLife.iOS.Views;
using MyStudyLife.UI;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Schools;
using MyStudyLife.UI.ViewModels.Wizards;
using UIKit;

namespace MyStudyLife.iOS.Platform {
    // ReSharper disable once InconsistentNaming
    public class iOSPresenter : MvxIosViewPresenter {
        private readonly Lazy<INavigationService> _navigationService = new Lazy<INavigationService>(Mvx.IoCProvider.Resolve<INavigationService>);
        private readonly Lazy<IMvxMessenger> _messenger = new Lazy<IMvxMessenger>(Mvx.IoCProvider.Resolve<IMvxMessenger>);

        private INavigationService NavigationService => _navigationService.Value;

        // TODO: Replace this with interfaces?
        // eg. IMslModalView
        // eg. IMslCoreView
        private static readonly List<Type> CoreViews = new List<Type>();
        private static readonly List<Type> ModalViews = new List<Type>();

        private readonly Stack<UINavigationController> _modalNavigationControllers = new Stack<UINavigationController>();

        private UINavigationController CurrentTopNavigationController
            => _modalNavigationControllers.Count > 0 ? _modalNavigationControllers.Peek() : MasterNavigationController;

        public iOSPresenter(IMvxApplicationDelegate applicationDelegate, UIWindow window)
            : base(applicationDelegate, window) {

            AddPresentationHintHandler<WelcomeWizardCompletedPresentationHint>(hint =>
                Show(MvxViewModelRequest.GetDefaultRequest(hint.AddClasses ? typeof(ScheduleViewModel) : typeof(DashboardViewModel)))
            );
            AddPresentationHintHandler<EntityDeletedPresentationHint>(async hint => {

                var navController = CurrentTopNavigationController;

                // Hacky way to determine if we need to go back 2 view controllers
                if (navController.ViewControllers.Length > 1) {
                    var currentViewController = navController.ViewControllers[navController.ViewControllers.Length - 1];
                    var previousViewController = navController.ViewControllers[navController.ViewControllers.Length - 2];

                    var currentViewControllerName = currentViewController.GetType().Name;
                    var previousViewControllerName = previousViewController.GetType().Name;

                    // eg. currentViewControllerName=TaskInputView, previousViewControllerName=TaskView
                    bool backTwice = previousViewControllerName == currentViewControllerName.Replace("Input", String.Empty);

                    if (_modalNavigationControllers.Count > 0) {
                        var topCtrl = _modalNavigationControllers.Pop();

                        topCtrl.DismissViewController(true, topCtrl.Dispose);

                        NavigationService.BackStack.Pop();
                    }
                    else {
                        // If Top ViewController is present modally then dismiss it otherwise try to pop it
                        // Note : It was not working while deleting subject. 
                        if (IsViewControllerPresentedModally(navController.VisibleViewController)) {
                            await Close(hint.ViewModelToClose);
                        }
                        else {
                            navController.PopToViewController(navController.ViewControllers[navController.ViewControllers.Length - (backTwice ? 3 : 2)], true);

                            NavigationService.BackStack.Pop(backTwice ? 2 : 1);
                        }
                    }
                }
                else {
                    await Close(hint.ViewModelToClose);
                }

                return true;
            });
            AddPresentationHintHandler<AuthorizationChangedPresentationHint>(async hint => {
                if (hint.Cause == Authorization.AuthStatusChangeCause.SignIn)
                    return false;

                return await Show(MvxViewModelRequest<SignInViewModel>.GetDefaultRequest());
            });
            AddPresentationHintHandler<OAuthWebFlowResultPresentationHint>(async hint => {
                if (CurrentTopNavigationController.VisibleViewController is SignInView currentSignInView) {
                    currentSignInView.SetOAuthWebFlowResult(hint.Result);
                }
                else {
                    await Show(new MvxViewModelRequest {
                        ViewModelType = typeof(SignInViewModel),
                        ParameterValues = SignInViewModel.NavObject.ForOAuthResult(hint.Result).ToSimplePropertyDictionary(),
                        PresentationValues = new Dictionary<string, string> {
                            ["ExternalRequest"] = Boolean.TrueString
                        }
                    });
                }

                return true;
            });
        }

        public override async Task<bool> Show(MvxViewModelRequest request) {
            var viewModelType = request.ViewModelType;
            var result = true;

            if (viewModelType.In(typeof(SignInViewModel), typeof(WelcomeWizardViewModel), typeof(WelcomeViewModel), typeof(SchoolWelcomeViewModel))) {
                var vc = (UIViewController)this.CreateViewControllerFor(request);

                if (MasterNavigationController == null) {
                    MasterNavigationController = CreateNavigationController(vc);
                    SetWindowRootViewController(MasterNavigationController, new MvxRootPresentationAttribute());
                }
                else {
                    MasterNavigationController.SetViewControllers(new[] { vc }, true);
                }
            }
            else if (CoreViews.Contains(viewModelType)) {
                CoreView coreView;

                if (MasterNavigationController == null) {
                    coreView = new CoreView();

                    await ShowRootViewController(coreView, new MvxRootPresentationAttribute { WrapInNavigationController = true }, request);
                }
                else {
                    coreView = MasterNavigationController.ViewControllers?.OfType<CoreView>().FirstOrDefault();

                    if (coreView == null) {
                        coreView = new CoreView();
                        // animated: false else we get a back button in iOS 14
                        MasterNavigationController.SetViewControllers(new UIViewController[] { coreView }, false);
                    }
                }

                var viewController = (UIViewController)this.CreateViewControllerFor(request);

                coreView.ShowViewController(viewController);
            }
            else {
                result = await base.Show(request);
            }

            NavigationService.BackStack.Push(viewModelType);

            return result;
        }

        protected override MvxNavigationController CreateNavigationController(UIViewController viewController)
            => new MslNavigationController(viewController);

        public void RegisterCoreView<TViewModel>() where TViewModel : BaseViewModel {
            var tType = typeof(TViewModel);

            if (!CoreViews.Contains(tType)) {
                CoreViews.Add(tType);
            }
        }

        public void RegisterModalView<TViewModel>() where TViewModel : BaseViewModel {
            var tType = typeof(TViewModel);

            if (!ModalViews.Contains(tType)) {
                ModalViews.Add(tType);
            }
        }

        private bool IsViewControllerPresentedModally(UIViewController viewController) {
            if (viewController.PresentingViewController != null) {
                return true;
            }

            if (viewController.NavigationController?.PresentingViewController?.PresentedViewController != null &&
                viewController.NavigationController?.PresentingViewController?.PresentedViewController == viewController.NavigationController) {
                return true;
            }

            if (viewController?.TabBarController?.PresentingViewController is UITabBarController) {
                return true;
            }

            return false;
        }
    }
}