using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross.Base;
using MyStudyLife.Services;
using Serilog.Core;
using UIKit;

namespace MyStudyLife.iOS.Platform
{
    // ReSharper disable once InconsistentNaming
    public class iOSPushNotificationService : PushNotificationServiceBase, IPushNotificationService
    {
        private readonly IMvxMainThreadAsyncDispatcher _dispatcher;

        public iOSPushNotificationService(IMvxMainThreadAsyncDispatcher dispatcher, ILogger<PushNotificationServiceBase> logger) : base(logger)
        {
            _dispatcher = dispatcher;
        }

        public Task RegisterTokenAsync(string token)
        {
            throw new NotImplementedException();
        }

        public async Task EnsureNotificationChannelAsync()
        {
            await _dispatcher.ExecuteOnMainThreadAsync(() =>
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge)
            );
        }

        public async Task CloseNotificationChannelAsync()
        {
            await _dispatcher.ExecuteOnMainThreadAsync(() =>
                UIApplication.SharedApplication.UnregisterForRemoteNotifications()
            );
        }
    }
}