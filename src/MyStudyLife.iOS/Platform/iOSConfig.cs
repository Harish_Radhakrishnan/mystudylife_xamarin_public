using System;
using Foundation;
using MyStudyLife.Configuration;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSConfig : MslConfig {
        internal const string RaygunApiKey = "HsIwd9MQ50NgEzHgFj/9bw==";

        private static readonly Lazy<string> _appVersionName = new Lazy<string>(() =>
            NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"].ToString());

        private static readonly Lazy<Version> _appVersion = new Lazy<Version>(() =>
            Version.Parse(_appVersionName.Value.Split("-")[0]));

        public override Version AppVersion => _appVersion.Value;

        public override string AppVersionName => _appVersionName.Value;

        // Dev KiURDqpzCmbOanYY7acWLLXYUjJ1NsaZ
        protected override string ApiClientId => "IThTzgifyPhay3RC7lI81zeycD4pbCMA";
    }
}