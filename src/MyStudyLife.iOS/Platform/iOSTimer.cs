using System;
using System.Threading;
using MyStudyLife.UI.Reactive;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSTimer : ITimer {
        private Timer _internalTimer;

        public ITimer Start(Action callback, TimeSpan startIn, TimeSpan repeatEvery) {
            _internalTimer = new Timer((state) => callback(), null, startIn, repeatEvery);

            return this;
        }

        public void Change(TimeSpan startIn, TimeSpan repeatEvery) {
            if (_internalTimer == null)
                throw new InvalidOperationException("Cannot call ITimer.Change() before ITimer.Start()");

            _internalTimer.Change(startIn, repeatEvery);
        }

        public void Dispose() {
            var t = _internalTimer;

            _internalTimer = null;
            t.Dispose();
        }
    }
}