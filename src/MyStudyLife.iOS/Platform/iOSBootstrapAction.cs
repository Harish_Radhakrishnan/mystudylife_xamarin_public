using System.Threading.Tasks;
using MvvmCross;
using UIKit;
using MyStudyLife.Data.Settings;
using MyStudyLife.Reminders;
using MyStudyLife.UI.Bootstrap;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public sealed class iOSBootstrapAction : IBootstrapAction {
        public async Task RunAsync() {
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0)) {
                var deviceSettings = await new SettingsRepository().GetDeviceSettingsAsync();

                if (deviceSettings.RemindersEnabled) {
                    UIApplication.SharedApplication.BeginInvokeOnMainThread(() =>
                        UIApplication.SharedApplication.RegisterUserNotificationSettings(
                            UIUserNotificationSettings.GetSettingsForTypes(
                                UIUserNotificationType.Alert |
                                UIUserNotificationType.Sound,
#warning When we implement badges in iOS, this needs to be implemented too
                                null
                            )
                        )
                    );
                }
            }

            await Mvx.IoCProvider.Resolve<IReminderService>().RunAsync();
        }
    }
}