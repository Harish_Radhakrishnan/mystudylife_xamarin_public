﻿using Google.Analytics;
using MyStudyLife.Configuration;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;

namespace MyStudyLife.iOS.Platform {
    public class iOSAnalyticsService : AnalyticsServiceBase {
        private readonly ITracker _tracker;

        private bool _isNewSession;

        public iOSAnalyticsService(IMslConfig config, INavigationService navigationService) : base(navigationService) {
            Gai.SharedInstance.DispatchInterval = DispatchInterval.TotalSeconds;

            _tracker = Gai.SharedInstance.GetTracker(PropertyId);
            _tracker.Set(GaiConstants.AppName, "My Study Life for iOS");
            _tracker.Set(GaiConstants.AppId, "mystudylife.ios");
            _tracker.Set(GaiConstants.AppVersion, "ios-" + config.AppVersion.ToString(3));
        }

        protected override void SetUserInfoImpl(string userId, string userType, string schoolId, string schoolType) {
            _tracker.Set(Fields.CustomDimension(1), userType);
            _tracker.Set(Fields.CustomDimension(2), schoolId);
            _tracker.Set(Fields.CustomDimension(3), schoolType);
            _tracker.Set("&uid", userId);
        }

        protected override void TrackScreenImpl(string screenName) {
            _tracker.Set(GaiConstants.ScreenName, screenName);

            Send(DictionaryBuilder.CreateScreenView());
        }

        protected override void TrackEventImpl(string category, string action) {
            Send(DictionaryBuilder.CreateEvent(category, action, null, 0));
        }
        protected override void TrackEventImpl(string category, string action, string label) {
            Send(DictionaryBuilder.CreateEvent(category, action, label, 0));
        }

        protected override void NotifySessionEndImpl() {
            _isNewSession = true;
        }

        private void Send(DictionaryBuilder builder) {
            if (_isNewSession) {
                builder.Set("start", GaiConstants.SessionControl);
                _isNewSession = false;
            }

            _tracker.Send(builder.Build());
        }
    }
}
