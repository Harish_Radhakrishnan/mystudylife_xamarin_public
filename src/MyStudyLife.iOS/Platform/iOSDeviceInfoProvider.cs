using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using MyStudyLife.Data;
using MyStudyLife.Utility;
using UIKit;
using Constants = ObjCRuntime.Constants;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.iOS.Platform {
    // ReSharper disable once InconsistentNaming
    public class iOSDeviceInfoProvider : IDeviceInfoProvider {
        private const string HardwareProperty = "hw.machine";

        [DllImport(Constants.SystemLibrary)]
        internal static extern int sysctlbyname(
            [MarshalAs(UnmanagedType.LPStr)] string property, // name of the property
            IntPtr output, // output
            IntPtr oldLen, // IntPtr.Zero
            IntPtr newp, // IntPtr.Zero
            uint newlen // 0
        );

        /// <summary>
        ///		Gets the unique identifier for the 
        ///		current hardware.
        /// </summary>
        /// <returns>The hardware's unique id.</returns>
        public Task<Device> GetDeviceInformationAsync() {
            // We're doing this interop stuff instead of using UIDevice.CurrentDevice.Model
            // because that simply returns "iPad" or "iPhone" whereas the stuff below will
            // return the actual model and revision eg. "iPhone 4,1"
            string model = null;

            // Get the length of the string that will be returned
            var pLen = Marshal.AllocHGlobal(sizeof(int));
            sysctlbyname(HardwareProperty, IntPtr.Zero, pLen, IntPtr.Zero, 0);

            var length = Marshal.ReadInt32(pLen);

            if (length > 0) {
                // Get the hardware string
                var pStr = Marshal.AllocHGlobal(length);
                sysctlbyname(HardwareProperty, pStr, pLen, IntPtr.Zero, 0);

                // Convert the native string into a C# string
                model = Marshal.PtrToStringAnsi(pStr);
            
                // Native cleanup
                Marshal.FreeHGlobal(pStr);
            }

            // Native cleanup
            Marshal.FreeHGlobal(pLen);

            return Task.FromResult(new Device {
                Manufacturer = "Apple",
                Model = model,
                // We're using this and not the advertising identifier because...
                // http://techcrunch.com/2014/02/03/apples-latest-crackdown-apps-pulling-the-advertising-identifier-but-not-showing-ads-are-being-rejected-from-app-store/
                HardwareId = UIDevice.CurrentDevice.IdentifierForVendor.AsString(),
                OSVersion = UIDevice.CurrentDevice.SystemVersion
            });
        }
    }
}