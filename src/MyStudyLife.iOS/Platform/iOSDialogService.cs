using System;
using System.Threading.Tasks;
using BigTed;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using UIKit;

namespace MyStudyLife.iOS.Platform
{
    // ReSharper disable once InconsistentNaming
    public class iOSDialogService : DialogServiceBase, IDialogService {
        protected override void RequestConfirmationImpl(string title, string message, string cancelText, string confirmText, Action<bool> onActioned) {
            var confirmation = new UIAlertView(title, message, null, cancelText, confirmText);

            confirmation.Clicked += (s, e) => onActioned((int) e.ButtonIndex == 1);

            confirmation.Show();
        }

        public void ShowDismissible(string title, string message) {
            new UIAlertView(title, message, null, R.Common.Okay).Show();
        }

        [Obsolete("Use ShowToastNotification and keep the reference")]
        public void ShowNotification(string title, string message) {
            if (!String.IsNullOrEmpty(title)) { 
                Mvx.IoCProvider.Resolve<ILogger<iOSDialogService>>().LogWarning("title will not be used when showing notifications on iOS");
            }

            ProgressHUD.Shared.Ring.Color = Resources.Colors.Accent;
            ProgressHUD.Shared.HudForegroundColor = UIColor.White;
            ProgressHUD.Shared.HudBackgroundColour = UIColor.Black.ColorWithAlpha(0.8f);
            ProgressHUD.Shared.HudFont = Resources.Fonts.OfSize(FontSize.Body, FontWeight.Light);

            ProgressHUD.Shared.ShowToast(message, ProgressHUD.MaskType.None, ProgressHUD.ToastPosition.Bottom, 3000d);
        }

        /// <summary>
        ///     Shows a notification which appears at the bottom of the screen and will auto dismiss
        ///     after the specified number of milliseconds.
        /// </summary>
        /// <param name="message">
        ///     The message to display.
        /// </param>
        /// <param name="timeout">
        ///     The number of milliseconds the notification show auto dismiss after.
        /// </param>
        /// <returns></returns>
        public static ProgressHUD ShowToastNotification(string message, double timeout = 3000) {
            Check.NotNullOrEmpty(ref message, nameof(message));

            var hud = new ProgressHUD {
                Ring = {
                    Color = Resources.Colors.Accent
                },
                HudForegroundColor = UIColor.White,
                HudBackgroundColour = UIColor.Black.ColorWithAlpha(0.8f),
                HudFont = Resources.Fonts.OfSize(FontSize.Body, FontWeight.Light)
            };

            hud.ShowToast(message, ProgressHUD.MaskType.None, ProgressHUD.ToastPosition.Bottom, timeout);

            return hud;
        }

        public void ShowCustom(string title, string message, CustomDialogAction leftAction, CustomDialogAction rightAction) {
            // iOS has cancel on the left so switch around...
            var temp = leftAction;

            leftAction = rightAction;
            rightAction = temp;

            var confirmation = new UIAlertView(title, message, null, leftAction.Content, rightAction.Content);

            confirmation.Clicked += (s, e) => {
                if ((int)e.ButtonIndex == 0) {
                    leftAction.TryTriggerOnChosen();
                }
                else {
                    rightAction.TryTriggerOnChosen();
                }
            };

            confirmation.Show();
        }

        public Task<CustomDialogResult> ShowCustomAsync(string title, string message, string positiveAction, string negativeAction) {
            var tcs = new TaskCompletionSource<CustomDialogResult>();

            var confirmation = new UIAlertView(title, message, null, negativeAction, positiveAction);

            confirmation.Clicked += (s, e) => tcs.TrySetResult(new CustomDialogResult((int)e.ButtonIndex == 1));

            confirmation.Show();

            return tcs.Task;
        }

        /// <summary>
        ///     Shows a loading modal in the center of the screen.
        ///
        ///     A new instance of the modal is created every time this
        ///     method is called, and should be dismissed from the retirned.
        ///     <see cref="ProgressHUD"/>
        /// </summary>
        /// <param name="statusText">
        ///     The text that will be displayed in the HUD.
        /// </param>
        public static ProgressHUD ShowBlockingBusyIndicator(string statusText) {
            var hud = new ProgressHUD {
                Ring = {
                    Color = Resources.Colors.Accent
                },
                HudForegroundColor = Resources.Colors.Foreground,
                HudBackgroundColour = UIColor.White.ColorWithAlpha(0.8f),
                HudFont = Resources.Fonts.OfSize(FontSize.Body, FontWeight.Light)
            };

            hud.ShowContinuousProgress(statusText, ProgressHUD.MaskType.Black);

            return hud;
        }

        [Obsolete("Use ShowBlockingBusyIndicator and keep the reference")]
        public static void ShowBusyIndicator(string status) {
            ProgressHUD.Shared.Ring.Color = Resources.Colors.Accent;
            ProgressHUD.Shared.HudForegroundColor = Resources.Colors.Foreground;
            ProgressHUD.Shared.HudBackgroundColour = UIColor.White.ColorWithAlpha(0.8f);
            ProgressHUD.Shared.HudFont = Resources.Fonts.OfSize(FontSize.Body, FontWeight.Light);

            ProgressHUD.Shared.ShowContinuousProgress(status, ProgressHUD.MaskType.Black);
        }

        [Obsolete("Use ShowBlockingBusyIndicator and keep the reference")]
        public static void HideBusyIndicator() {
            ProgressHUD.Shared.Dismiss();
        }
    }
}