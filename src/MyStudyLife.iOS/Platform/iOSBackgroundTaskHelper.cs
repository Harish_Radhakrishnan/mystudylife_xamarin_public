using Mindscape.Raygun4Net;
using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using MyStudyLife.Data;
using MyStudyLife.Versioning;
using UIKit;

namespace MyStudyLife.iOS.Platform {
    public static class iOSBackgroundTaskHelper {
        /// <summary>
        ///     Ensures the IoC is initialized and checks whether there
        ///     are any pending upgrades to the app which should be performed
        ///     in the foreground.
        /// </summary>
        /// <returns>
        ///     False if there are pending upgrades.
        /// </returns>
        public static bool EnsureInitialized(IMvxApplicationDelegate appDelegate, UIWindow window) {
            var setupSingleton = MvxIosSetupSingleton.EnsureSingletonAvailable(appDelegate, window);
            setupSingleton.EnsureInitialized();
            
            try {
                if (RaygunClient.Current == null) {
                    var user = Mvx.IoCProvider.Resolve<IUserStore>().GetCurrentUser();

                    iOSBugReporter.Attach(user?.Id.ToString());
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            return !Mvx.IoCProvider.Resolve<IUpgradeService>().RequiresUpgrade;
        }
    }
}