using System;
using Foundation;
using UIKit;
using MyStudyLife.Reminders;
using Newtonsoft.Json;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSReminderScheduler : IReminderScheduler {
        public bool ScheduleReminder(Reminder reminder) {
            string alertBody = reminder.Title;

            if (reminder.Message != null) {
                alertBody += Environment.NewLine + reminder.Message;
            }

            UIApplication.SharedApplication.InvokeOnMainThread(() => {
                var notification = new UILocalNotification {
                    FireDate = reminder.Time.ToNSDate(DateTimeKind.Local),
                    TimeZone = NSTimeZone.DefaultTimeZone,
                    AlertBody = alertBody,
                    UserInfo = NSDictionary.FromObjectAndKey(
                        NSObject.FromObject(JsonConvert.SerializeObject(reminder)),
                        NSObject.FromObject("REMINDER")
                    )
                };

                UIApplication.SharedApplication.ScheduleLocalNotification(notification);
            });

            return true;
        }

        public void ClearReminders() {
            UIApplication.SharedApplication.InvokeOnMainThread(UIApplication.SharedApplication.CancelAllLocalNotifications);
        }
    }
}