using System.Net.Http;
using MyStudyLife.Net;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSHttpClientFactory : IHttpClientFactory {
        // NOTE:
        // When running against a local API, 401 request are sometimes retried
        // as there is internal authentication challenge handling within NSURLSession.
        // This is not an issue against a hosted API. (This means the sign in screen may return
        // a throttled error.
        //
        // https://github.com/xamarin/xamarin-macios/issues/6443
        public HttpClient Get() => new HttpClient(new NSUrlSessionHandler {
            AllowsCellularAccess = true,
            DisableCaching = true
        });
    }
}