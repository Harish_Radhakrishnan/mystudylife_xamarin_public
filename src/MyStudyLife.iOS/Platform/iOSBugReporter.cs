using System;
using System.Collections;
using System.Collections.Generic;
using Mindscape.Raygun4Net;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSBugReporter : BugReporter {
        protected override void SetUserImpl(string uid) {
            RaygunClient.Current.User = uid;
        }

        protected override void SendImpl(Exception exception, IList<string> tags, IDictionary customUserData) {
            RaygunClient.Current.Send(exception, tags, customUserData);
        }
        
        /// <summary>
        ///     Listens to and sends all unhandled exceptions and unobserved task exceptions.
        /// </summary>
        /// <param name="userId">
        ///     The current user's identifier.
        /// </param>
        public static void Attach(string userId = null) {
            if (RaygunClient.Current == null) {
                RaygunClient.Attach(iOSConfig.RaygunApiKey, userId, true);
            }
        }
    }
}