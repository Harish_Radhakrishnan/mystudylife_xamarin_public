using System;
using Foundation;
using MessageUI;
using UIKit;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.Configuration;
using SafariServices;
using MvvmCross;
using BindingFlags = System.Reflection.BindingFlags;
using System.Linq;
using MvvmCross.Plugin.Color.Platforms.Ios;
using MvvmCross.Views;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public class iOSTaskProvider : ITaskProvider {
        public void ShowEmailComposer(string subject, string body, string to) {
            try {
                var mail = new MFMailComposeViewController();

                mail.SetToRecipients(new[] {to});
                mail.SetSubject(subject);
                mail.SetMessageBody(body, false);

                mail.Finished += (s, e) => e.Controller.DismissViewController(true, null);

                UIApplication.SharedApplication.Delegate.GetWindow().RootViewController.PresentViewController(mail, true, null);
            }
            catch (Exception ex) {
                // This will be thrown if the device doe not have a mail client configured.
                if (!ex.Message.Contains("'init' method returned nil", StringComparison.OrdinalIgnoreCase) &&
                    !ex.Message.Contains("ModelNotImplementedException", StringComparison.OrdinalIgnoreCase)) {
                    throw;
                }

                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(
                    "No Email Client",
                    "It doesn't look like you have configured email for this device."
                );
            }
        }

        public void ReviewApp() {
            UIApplication.SharedApplication.OpenUrl(new NSUrl("itms-apps://itunes.apple.com/app/id910639339"));
        }

        public bool TryShowNativeUri(Uri uri) => UIApplication.SharedApplication.OpenUrl(uri);

        public void ShowWebUri(Uri uri) {
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0)) {
                // Only use a SafariViewController on iOS 10 and above,
                // due to the fact that setting the tint color doesn't
                // work prior to iOS 10 meaning the controls in safari
                // are not visible since it uses a light background
                // but our app's status bar tint color (white)
                var rootViewController = UIApplication.SharedApplication.KeyWindow.RootViewController;
                var rootNavigationController = rootViewController.PresentedViewController as UINavigationController;
                var presentedViewController = rootNavigationController?.ChildViewControllers.LastOrDefault();

                var mvxView = presentedViewController as IMvxView;
                var color = (mvxView?.ViewModel?.GetType())?.GetProperty("Color", BindingFlags.Instance | BindingFlags.Public |
                    BindingFlags.FlattenHierarchy)?.GetValue(mvxView.ViewModel) as System.Drawing.Color?;
                var uiColor = color?.ToNativeColor() ?? Resources.Colors.Accent;

                var svc = new SFSafariViewController(new NSUrl(uri.ToString())) {
                    PreferredBarTintColor = uiColor,
                    PreferredControlTintColor = UIColor.White,
                    HidesBottomBarWhenPushed = true,
                    ModalPresentationStyle = UIModalPresentationStyle.Popover,
                    View = {
                        TintColor = UIColor.White
                    }
                };

                var viewController = rootNavigationController ?? rootViewController;
                viewController.PresentViewController(svc, true, null);
            }
            else {
                UIApplication.SharedApplication.OpenUrl(uri);
            }
        }

        public void ShowFacebookPage() {
            if (!TryShowNativeUri(new Uri(MslConfig.Current.FacebookPageDeeplink))) {
                ShowWebUri(new Uri(MslConfig.Current.FacebookPageUri));
            }
        }
    }
}