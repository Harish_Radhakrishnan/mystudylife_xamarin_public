using System;
using Foundation;

namespace MyStudyLife.iOS.Platform {
// ReSharper disable once InconsistentNaming
    public sealed class iOSDateTimeProvider : IDateTimeProvider {
        // https://bugzilla.xamarin.com/show_bug.cgi?id=22648
        public DateTime Now {
            get { return NSDate.Now.ToDateTime(); }
        }
    }
}