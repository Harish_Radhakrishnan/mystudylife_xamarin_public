using CrossUI.iOS.Dialog.Elements;
using EvanRobertson.TTTAttributedLabel;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Bindings.Target.Construction;
using MvvmCross.Converters;
using MvvmCross.IoC;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.Platforms.Ios.Presenters;
using MvvmCross.Plugin.File.Platforms.Ios;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;
using MyStudyLife.iOS.Binding.Target;
using MyStudyLife.iOS.Dialog;
using MyStudyLife.iOS.Dialog.Binding;
using MyStudyLife.iOS.Platform;
using MyStudyLife.iOS.Versioning;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Security.Crypto;
using MyStudyLife.Services;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Converters;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.Utility;
using MyStudyLife.Versioning;
using Serilog;
using Serilog.Extensions.Logging;
using Log = Serilog.Log;

namespace MyStudyLife.iOS
{
    public class Setup : MvxIosSetup<MslApp>{
        protected override IMvxIosViewPresenter CreateViewPresenter(){
            var presenter = new iOSPresenter(ApplicationDelegate, Window);
            presenter.RegisterCoreView<DashboardViewModel>();
            presenter.RegisterCoreView<CalendarWeekViewModel>();
            presenter.RegisterCoreView<CalendarMonthViewModel>();
            presenter.RegisterCoreView<TasksViewModel>();
            presenter.RegisterCoreView<ExamsViewModel>();
            presenter.RegisterCoreView<ScheduleViewModel>();
            presenter.RegisterCoreView<SearchViewModel>();
            presenter.RegisterCoreView<SettingsViewModel>();

            return presenter;
        }

        protected override void InitializeFirstChance(IMvxIoCProvider iocProvider){
            base.InitializeFirstChance(iocProvider);

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INavigationService, IMvxViewModelLoader>(
                (loader) => new NavigationService(loader, Mvx.IoCProvider.Resolve<IMvxViewDispatcher>(), iocProvider));

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IBugReporter, iOSBugReporter>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IMslConfig, iOSConfig>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INetworkProvider, iOSNetworkProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IStorageProvider, iOSStorageProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ICryptoProvider, iOSCryptoProvider>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDeviceInfoProvider, iOSDeviceInfoProvider>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IHttpClientFactory, iOSHttpClientFactory>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDialogService, iOSDialogService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IReminderScheduler, iOSReminderScheduler>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IPushNotificationService, iOSPushNotificationService>();

            Mvx.IoCProvider.RegisterType<IBootstrapAction, iOSBootstrapAction>();

            Mvx.IoCProvider.RegisterType<ITaskProvider, iOSTaskProvider>();

            Mvx.IoCProvider.RegisterType<ITimer, iOSTimer>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAnalyticsService, iOSAnalyticsService>();

            // Setup File Plugin... 
            var filePlugin = new FilePluginSetupiOS();
            filePlugin.Load();
        }

        protected override void InitializeLastChance(IMvxIoCProvider iocProvider){
            base.InitializeLastChance(iocProvider);
            RegisterUpgrades(Mvx.IoCProvider.Resolve<IUpgradeService>());
        }

        private void RegisterUpgrades(IUpgradeService upgradeService){
            upgradeService.RegisterUpgrade<V1ToV2>();
            upgradeService.RegisterUpgrade<V205ToV206>();
        }

        protected override void FillTargetFactories(IMvxTargetBindingFactoryRegistry registry){
            base.FillTargetFactories(registry);

            registry.RegisterCustomBindingFactory<TTTAttributedLabel>("AutoLinkText", view => new AutoLinkerTargetBinding(view));

            registry.RegisterFactory(new MvxSimplePropertyInfoTargetBindingFactory(typeof(MvxValueElementValueBinding), typeof(ValueElement), "Value"));
            registry.RegisterCustomBindingFactory<MvxRootElement>(MvxRootElementRadioSelectedItemBinding.Name, view => new MvxRootElementRadioSelectedItemBinding(view));

            LayerBackgroundColorTargetBinding.Register(registry);
            TaskStateIndicatorTargetBinding.Register(registry);
            PieChartTargetBinding.Register(registry);
        }

        protected override void FillBindingNames(IMvxBindingNameRegistry registry){
            registry.AddOrOverwrite(typeof(ValueElement), "Value");
            base.FillBindingNames(registry);
        }

        protected override void FillValueConverters(IMvxValueConverterRegistry registry){
            base.FillValueConverters(registry);

            registry.AddOrOverwrite("ContextualDate", new ContextualDateConverter());

            // TODO: Phase out and introduce System.Drawing.Color in list item view models...
            var scvc = Mvx.IoCProvider.IoCConstruct<SubjectColorToValueConverter>();

            registry.AddOrOverwrite("SubjectColor", scvc);
            registry.AddOrOverwrite("SubjectColorTo", scvc);
        }

        protected override ILoggerProvider CreateLogProvider(){
            return new SerilogLoggerProvider();
        }

        protected override ILoggerFactory CreateLogFactory(){
            // serilog configuration
            var log = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .CreateLogger();

            Log.Logger = log;

            return new SerilogLoggerFactory(log);
        }
    }
}