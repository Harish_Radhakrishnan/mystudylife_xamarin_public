using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS {
    public static class NavigationBarExtensions {
        public static void ApplyAppearance(this UINavigationBar navigationBar, UIColor foregroundColor = null, UIColor backgroundColor = null, bool translucent = false) {
            foregroundColor ??= UIColor.White;
            backgroundColor ??= Resources.Colors.Accent;
            
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0)) {
                var navBarAppearance = new UINavigationBarAppearance();
                navBarAppearance.ConfigureWithOpaqueBackground();
                navBarAppearance.LargeTitleTextAttributes = new UIStringAttributes {ForegroundColor = foregroundColor};
                navBarAppearance.TitleTextAttributes = new UIStringAttributes {ForegroundColor = foregroundColor};
                navBarAppearance.BackgroundColor = backgroundColor;

                navigationBar.StandardAppearance = navBarAppearance;
                navigationBar.CompactAppearance = navBarAppearance;
                navigationBar.ScrollEdgeAppearance = navBarAppearance;

                navigationBar.Translucent = translucent;
            }
            else {
                navigationBar.BarStyle = UIBarStyle.Black;

                navigationBar.Translucent = translucent;
                navigationBar.TintColor = foregroundColor;
                navigationBar.BarTintColor = backgroundColor;
            }
            
            navigationBar.RemoveShadow();
        }
        
        public static void RemoveShadow(this UINavigationBar navigationBar) {
            if (UIDevice.CurrentDevice.CheckSystemVersion(13, 0)) {
                navigationBar.CompactAppearance.ShadowColor = 
                    navigationBar.StandardAppearance.ShadowColor = 
                        navigationBar.ScrollEdgeAppearance.ShadowColor = UIColor.Clear;

                navigationBar.CompactAppearance.ShadowImage =
                    navigationBar.StandardAppearance.ShadowImage =
                        navigationBar.ScrollEdgeAppearance.ShadowImage = new UIImage();
            }
            else {
                var navigationBarWidth = navigationBar.Frame.Width;
                var navigationBarHeight = navigationBar.Frame.Height;
                var statusBarHeight = UIApplication.SharedApplication.StatusBarFrame.Height;

                UIGraphics.BeginImageContextWithOptions(new CGSize(navigationBarWidth, navigationBarHeight + statusBarHeight), false, 0f);
                UIImage blankImage = UIGraphics.GetImageFromCurrentImageContext();
                UIGraphics.EndImageContext();

                navigationBar.SetBackgroundImage(blankImage, UIBarMetrics.Default);
                navigationBar.ShadowImage = new UIImage();
            }
        }
    }
}