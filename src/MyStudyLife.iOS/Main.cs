using MyStudyLife.iOS.Platform;
using UIKit;

namespace MyStudyLife.iOS {
    public class Application {
        static void Main(string[] args) {
            SQLitePCL.Batteries_V2.Init();

            iOSBugReporter.Attach();
            
            UIApplication.Main(args, null, "AppDelegate");
        }
    }
}