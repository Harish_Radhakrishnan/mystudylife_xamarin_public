using System.Collections.Generic;
using UIKit;
using MyStudyLife.iOS.Controls;
using static MyStudyLife.iOS.Resources;

namespace MyStudyLife.iOS {
    public static class Theming {
        public static TLabel WithStyle<TLabel>(this TLabel view, string key) where TLabel : MSLLabel {
            switch (key) {
                case "Lonely":
                    view.ContentEdgeInsets = new UIEdgeInsets(5f, 0, 10f, 0);
                    view.TextColor = Colors.SubtleText;
                    view.Lines = 0;
                    view.LineBreakMode = UILineBreakMode.WordWrap;
                    view.TranslatesAutoresizingMaskIntoConstraints = false;
                    view.Font = Fonts.OfSize(FontSize.Caption);
                    return view;
                case "SectionHeader":
                    view.Font = Fonts.OfSize(FontSize.Body);
                    view.ContentEdgeInsets = new UIEdgeInsets(
                        Dimens.DefaultHorizontalMargin,
                        Dimens.DefaultHorizontalMargin,
                        Dimens.DefaultMargin + 2.5f,
                        Dimens.DefaultHorizontalMargin
                    );
                    return view;
            }

            throw new KeyNotFoundException("Could not find style with key '" + key + "'");
        }
    }
}