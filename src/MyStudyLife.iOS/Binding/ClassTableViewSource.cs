using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using MvvmCross.WeakSubscription;
using UIKit;
using MyStudyLife.iOS.Controls.Cells;
using Class = MyStudyLife.Data.Class;

namespace MyStudyLife.iOS.Binding {
    internal sealed class ClassTableViewSource : MslTableViewSource {
        private readonly nfloat _timeHeight;

        private readonly HashSet<IDisposable> _timesCollectionChangedSubscriptions = new HashSet<IDisposable>();

        public override IEnumerable ItemsSource {
            get => base.ItemsSource;
            set {
                base.ItemsSource = value;

                SubscribeToTimesCollectionChanged();
            }
        }

        public ClassTableViewSource(UITableView tableView, bool isTeacher) : base(tableView, isTeacher ? typeof(TeacherClassTableViewCell) : typeof(StudentClassTableViewCell)) {
            _timeHeight = new NSString("M").StringSize(Resources.Fonts.FontOfSize(Resources.Fonts.FontSizeNormal)).Height;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
            var cls = (Class)GetItemAt(indexPath);

            if (cls.IsOneOff) {
                return ListItemTableViewCell.Height;
            }

            return ListItemTableViewCell.Height + ((cls.Times.Count - 1) * _timeHeight);
        }

        private void SubscribeToTimesCollectionChanged() {
            foreach (var subscription in _timesCollectionChangedSubscriptions) {
                subscription.Dispose();
            }
            
            _timesCollectionChangedSubscriptions.Clear();

            if (ItemsSource == null) {
                return;
            }

            int i = 0;

            foreach (var c in ItemsSource.Cast<Class>()) {
                if (!c.IsOneOff) {
                    int i1 = i;

                    _timesCollectionChangedSubscriptions.Add(
                        // When an entity is updated, we copy the property values back
                        // from the editted to original - so listen for prop change vs.
                        // collection changed.
                        c.WeakSubscribe<Class>(property: "Times", (s, e) => {
                            // Check the class is still in the list when we receive this...
                            if (i1 < ItemsSource.Cast<object>().Count()) {
                                TableView.ReloadRows(
                                    new[] { NSIndexPath.FromItemSection(i1, 0) },
                                    UITableViewRowAnimation.Fade
                                );
                            }
                        })
                    );
                }

                i++;
            }
        }
    }
}