﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Binding.Bindings.Target.Construction;
using iOSCharts;
using MvvmCross.Plugin.Color.Platforms.Ios;
using MyStudyLife.Data.UI;
using MyStudyLife.UI.Data;
using UIKit;

namespace MyStudyLife.iOS.Binding.Target {
    public class PieChartTargetBinding : MvxConvertingTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<PieChartView>("DialData", pieChart => new PieChartTargetBinding(pieChart));
        }

        public PieChartTargetBinding(PieChartView target) : base(target) { }

        public override Type TargetType => typeof(List<DialDataset>);

        protected override void SetValueImpl(object target, object value) {
            var chartView = target as PieChartView;

            if (chartView == null) {
                return;
            }

            chartView.Clear();

            var dataset = (value as IList<DialDataset>) ?? ((value as IEnumerable<DialDataset>) ?? Enumerable.Empty<DialDataset>())?.ToList();

            var length = Math.Max(dataset.Count, 1);
            var dataEntries = new PieChartDataEntry[length];
            var colors = new UIColor[length];
            var count = 0;

            for (var i = 0; i < dataset.Count; i++) {
                var e = dataset[i];

                count += e.Count;
                dataEntries[i] = new PieChartDataEntry(e.Value);
                colors[i] = e.Color.ToNativeColor();
            }

            if (dataset.Count == 0) {
                dataEntries[0] = new PieChartDataEntry(1);
                colors[0] = UserColors.Empty.ToNativeColor();
                chartView.AnimateWithYAxisDuration(0);
            }
            else {
                chartView.AnimateWithYAxisDuration(.6f, ChartEasingOption.EaseInOutCubic);
            }

            var dataSet = new PieChartDataSet(dataEntries, String.Empty) {
                SliceSpace = length > 1 ? 2f : 0
            };
            dataSet.SetColors(colors, 1f);
            // disable drawing labels related to the values of slices
            dataSet.DrawValuesEnabled = false;

            var data = new PieChartData();
            data.AddDataSet(dataSet);

            chartView.Data = data;
            chartView.NotifyDataSetChanged();

            chartView.SetNeedsDisplay();
        }
    }
}
