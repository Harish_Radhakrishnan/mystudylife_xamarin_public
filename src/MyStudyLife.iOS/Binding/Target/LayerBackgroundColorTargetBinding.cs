using System;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Binding.Bindings.Target.Construction;
using CoreGraphics;
using UIKit;

namespace MyStudyLife.iOS.Binding.Target {
    public class LayerBackgroundColorTargetBinding : MvxConvertingTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) 
            => registry.RegisterCustomBindingFactory<UIView>("LayerBackgroundColor", view => new LayerBackgroundColorTargetBinding(view));

        public override Type TargetType => typeof(CGColor);

        public LayerBackgroundColorTargetBinding(object target) : base (target) {}

        protected override object MakeSafeValue(object value) {
            // Prevents exception being triggered when MvvmCross automatically
            // tries to do the conversion.
            return value;
        }

        protected override void SetValueImpl(object target, object value) {
            var view = (UIView) target;

            if (view == null || value == null) {
                return;
            }

            if (view.Layer == null) {
                return;
            }

            var cgColor = (value as UIColor)?.CGColor ?? (CGColor)value;

            view.Layer.BackgroundColor = cgColor;
        }
    }
}