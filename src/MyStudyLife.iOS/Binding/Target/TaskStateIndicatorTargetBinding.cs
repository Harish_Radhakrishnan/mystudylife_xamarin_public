﻿using System;
using MvvmCross.Binding.Bindings.Target;
using MvvmCross.Binding.Bindings.Target.Construction;
using MyStudyLife.Data;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI;

namespace MyStudyLife.iOS.Binding.Target {
    class TaskStateIndicatorTargetBinding : MvxConvertingTargetBinding {
        public static void Register(IMvxTargetBindingFactoryRegistry registry) {
            registry.RegisterCustomBindingFactory<MSLIcon>("TaskState", icon => new TaskStateIndicatorTargetBinding(icon));
        }

        public TaskStateIndicatorTargetBinding(MSLIcon target) : base(target) { }

        public override Type TargetType {
            get { return typeof (Task); }
        }

        protected override void SetValueImpl(object target, object value) {
            var icon = target as MSLIcon;
            var task = value as Task;

            if (icon == null) {
                return;
            }

            if (task == null) {
                icon.Hidden = true;
                return;
            }

            if (task.IsOverdue || task.IsIncompleteAndDueSoon) {
                icon.Glyph = Glyph.Attention;
                icon.TextColor = task.IsOverdue ? Resources.Colors.AttentionForeground : Resources.Colors.WarningForeground;
                icon.Hidden = false;
            }
            else if (task.IsComplete) {
                icon.Glyph = Glyph.Tick;
                icon.TextColor = Resources.Colors.OkayForeground;
                icon.Hidden = false;
            }
            else {
                icon.Hidden = true;
            }
        }
    }
}
