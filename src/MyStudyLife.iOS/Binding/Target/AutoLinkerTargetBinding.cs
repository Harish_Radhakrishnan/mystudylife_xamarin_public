using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmCross.Binding.Bindings.Target;
using Foundation;
using EvanRobertson.TTTAttributedLabel;
using MyStudyLife.iOS.Controls;
using MyStudyLife.UI.Helpers;

namespace MyStudyLife.iOS.Binding.Target {
    public class AutoLinkerTargetBinding : MvxConvertingTargetBinding {
        private NSString _textCache;
        private List<Tuple<NSUrl, NSRange>> _linkCache;

        public AutoLinkerTargetBinding(TTTAttributedLabel target) : base(target) {
            if (target is MslAttributedLabel mslAttributedLabel) {
                mslAttributedLabel.LinkForegroundColorChanged += OnLinkForegroundColorChanged;
            }
        }

        // TTTAttributedLabel doesn't have a redraw method, so we have to tell it to set the text again
        private void OnLinkForegroundColorChanged(object sender, EventArgs e) {
            var attributedLabel = (TTTAttributedLabel)sender;

            if (_textCache == null || _linkCache == null) {
                return;
            }

            attributedLabel.Text = _textCache;
            SetLinks(attributedLabel, _linkCache);
        }

        public override Type TargetType => typeof(string);

        protected override void SetValueImpl(object target, object value) {
            var attributedLabel = target as TTTAttributedLabel;

            if (attributedLabel == null) {
                return;
            }

            var text = value as string;

            if (String.IsNullOrEmpty(text)) {
                attributedLabel.Text = NSString.Empty;
                return;
            }
            var matches = AutoLinker.GetMatches(text).ToList();

            if (matches.Count == 0) {
                attributedLabel.Text = new NSString(text);
                return;
            }

            var sb = new StringBuilder();
            var links = new List<Tuple<NSUrl, NSRange>>();

            int lastMatchEndIndex = 0;

            for (int i = 0; i < matches.Count; i++) {
                var match = matches[i];

                if (match.StartIndex > 0) {
                    sb.Append(text.Substring(lastMatchEndIndex, match.StartIndex - lastMatchEndIndex));
                }

                links.Add(new Tuple<NSUrl, NSRange>(NSUrl.FromString(match.Uri.ToString()), new NSRange(sb.Length, match.Text.Length)));
                sb.Append(match.Text);

                lastMatchEndIndex = match.EndIndex;

                if (i == (matches.Count - 1)) {
                    if (lastMatchEndIndex < (text.Length - 1)) {
                        sb.Append(text.Substring(lastMatchEndIndex, text.Length - lastMatchEndIndex));
                    }
                }
            }

            attributedLabel.Text = _textCache = new NSString(sb.ToString());
            _linkCache = links;

            SetLinks(attributedLabel, links);
        }

        private static void SetLinks(TTTAttributedLabel attributedLabel, List<Tuple<NSUrl, NSRange>> links) {
            foreach (var link in links) {
                attributedLabel.AddLinkToURL(link.Item1, link.Item2);
            }
        }
    }
}
