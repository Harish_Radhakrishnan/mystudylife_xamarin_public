using System;
using System.Linq;
using System.Reflection;
using System.Windows.Input;
using Foundation;
using MvvmCross.Platforms.Ios.Binding.Views;
using MyStudyLife.ObjectModel;
using UIKit;

namespace MyStudyLife.iOS.Binding {
    public class MslTableViewSource : MvxTableViewSource {
        private readonly string _identifer;
        private readonly bool _dynamicHeight;
        private readonly nfloat? _height;
        
        public ICommand RowClickCommand { get; set; }

        public UITableViewCellSelectionStyle CellSelectionStyle { get; set; } = UITableViewCellSelectionStyle.Default;

        public MslTableViewSource(UITableView tableView, Type cellType) : base(tableView) {
            var attribute = cellType.GetCustomAttribute<MslTableViewCellAttribute>(true);

            if (attribute != null) {
                _height = attribute.Height;
                _dynamicHeight = attribute.DynamicHeight;
                _identifer = attribute.Identifier ?? cellType.GetCustomAttribute<RegisterAttribute>().Name;
            }
            else {
                _identifer = cellType.GetCustomAttribute<RegisterAttribute>().Name;
            }

            tableView.RegisterClassForCellReuse(cellType, new NSString(_identifer));

            this.UseAnimations = true;
        }

        public MslTableViewSource(UITableView tableView, Type cellType, float rowHeight) : this(tableView, cellType) {
            _height = rowHeight;
            
            this.UseAnimations = true;
        }

        public static MslTableViewSource For<TCellType>(UITableView tableView) where TCellType : MvxTableViewCell {
            return new MslTableViewSource(tableView, typeof(TCellType));
        }

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath) {
            // For some reason this has to be set here, not within our cell class
            cell.SelectionStyle = CellSelectionStyle;
            // Fixes separator inset (if this ever fails, we need to use tableView.CellLayoutMarginsFollowReadableWidth = false too, but seems fine for now)
            cell.LayoutMargins = UIEdgeInsets.Zero;
            cell.SeparatorInset = UIEdgeInsets.Zero;
            cell.PreservesSuperviewLayoutMargins = false;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath) {
            return _dynamicHeight ? UITableView.AutomaticDimension : _height.GetValueOrDefault(59f);
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item) {
            return tableView.DequeueReusableCell(_identifer);
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath) {
            var item = GetItemAt(indexPath);
            var rowClickCommand = this.RowClickCommand;

            if (item != null && rowClickCommand != null) {
                rowClickCommand.Execute(item);

                tableView.DeselectRow(indexPath, false);
            }
            else base.RowSelected(tableView, indexPath);
        }
    }

    public class MslGroupedTableViewSource<TKey, TItem> : MslTableViewSource {
        public MslGroupedTableViewSource(UITableView tableView, Type cellType) : base(tableView, cellType) {
            this.UseAnimations = false;
            this.ReloadOnAllItemsSourceSets = true;
        }
        
        public override string TitleForHeader(UITableView tableView, nint section) 
            => GetGroupAt((int)section)?.Name;
        
        public override nint NumberOfSections(UITableView tableView)
            => this.ItemsSource?.Cast<object>().Count() ?? 0;

        public override nint RowsInSection(UITableView tableview, nint section)
            => GetGroupAt((int)section)?.Items.Count ?? 0;

        public override void WillDisplay(UITableView tableView, UITableViewCell cell, NSIndexPath indexPath) {
            base.WillDisplay(TableView, cell, indexPath);

            // Remove separator for last row in group
            if (TableView.SeparatorStyle == UITableViewCellSeparatorStyle.None) {
                return;
            }

            if (indexPath.Row == RowsInSection(tableView, indexPath.Section) - 1) {
                foreach (var view in cell.Subviews.Where(x => !Equals(x, cell.ContentView))) {
                    view.RemoveFromSuperview();
                }
            }
        }

        protected virtual Grouping<TKey, TItem> GetGroupAt(int index) { 
            var groupings = this.ItemsSource?.Cast<Grouping<TKey, TItem>>();

            if (groupings == null) {
                return null;
            }

            int i = 0;

            foreach (var group in groupings) {
                if (i == index) return group;
                i++;
            }

            return null;
        }

        protected override object GetItemAt(NSIndexPath indexPath) {
            var group = GetGroupAt(indexPath.Section);

            if (group == null) {
                return null;
            }

            return group.Items.ElementAtOrDefault(indexPath.Row);
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MslTableViewCellAttribute : Attribute {
        public float Height { get; set; }

        public bool DynamicHeight { get; set; }

        public string Identifier { get; set; }
    }
}