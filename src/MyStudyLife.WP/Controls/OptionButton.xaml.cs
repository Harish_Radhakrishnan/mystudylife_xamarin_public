﻿using System.Windows;

namespace MyStudyLife.WP.Controls {
    public partial class OptionButton {
        public static readonly DependencyProperty GlyphProperty = DependencyProperty.Register(
            "Glyph", typeof (string), typeof (OptionButton), new PropertyMetadata(default(string)));

        public string Glyph {
            get { return (string) GetValue(GlyphProperty); }
            set { SetValue(GlyphProperty, value); }
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof (string), typeof (OptionButton), new PropertyMetadata(default(string)));

        public string Text {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public OptionButton() {
            InitializeComponent();
        }
    }
}
