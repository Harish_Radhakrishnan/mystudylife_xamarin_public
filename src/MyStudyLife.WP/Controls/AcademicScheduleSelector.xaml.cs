﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Media;
using Cirrious.CrossCore;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MyStudyLife.WP.Controls {
	public partial class AcademicScheduleSelector {

		private Popup _hostPopup;

	    public bool IsOpen {
	        get { return _hostPopup != null && _hostPopup.IsOpen; }
	    }

	    public event EventHandler Closed {
	        add {
	            if (_hostPopup != null) {
                    _hostPopup.Closed += value;
	            }
	        }
            remove {
                if (_hostPopup != null) {
                    _hostPopup.Closed -= value;
                }
            }
	    }

		#region Academic Years

		public static readonly DependencyProperty AcademicYearsProperty =
			DependencyProperty.Register("AcademicYears", typeof(ObservableCollection<AcademicYear>), typeof(AcademicScheduleSelector), new PropertyMetadata(null, OnAcademicYearsChanged));

		private static void OnAcademicYearsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var ctrl = (AcademicScheduleSelector)d;

			var selectedSchedule = ctrl.SelectedSchedule;

			ctrl._preventSelectionChangedEvent = true;

            ctrl.YearListPicker.ItemsSource = ctrl.GetYearsItemSource(ctrl.AcademicYears);

			ctrl._preventSelectionChangedEvent = false;

			ctrl.SelectedSchedule = selectedSchedule;

			ctrl.SetPickersFromSelectedSchedule();
			ctrl.SetCurrentButtonDisabled();
		}

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return (ObservableCollection<AcademicYear>)GetValue(AcademicYearsProperty); }
			set { SetValue(AcademicYearsProperty, value); }
		}

		#endregion

		#region Selected Schedule

		public static readonly DependencyProperty SelectedScheduleProperty =
			DependencyProperty.Register("SelectedSchedule", typeof(IAcademicSchedule), typeof(AcademicScheduleSelector), new PropertyMetadata(null, OnSelectedScheduleChanged));

		private static void OnSelectedScheduleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var selector = (AcademicScheduleSelector)d;

			selector.SetPickersFromSelectedSchedule();
		}

        public IAcademicSchedule SelectedSchedule {
            get { return (IAcademicSchedule)GetValue(SelectedScheduleProperty); }
			set { SetValue(SelectedScheduleProperty, value); }
		}

		private bool _preventSelectionChangedEvent;

		private void SetPickersFromSelectedSchedule() {
			var selector = this;

			_preventSelectionChangedEvent = true;

			if (this.SelectedSchedule == null || this.SelectedSchedule.Guid.IsEmpty()) {
				selector.YearListPicker.SelectedIndex = 0;
				selector.TermListPicker.ItemsSource = GetTermsItemSource(null);
			}
            else {
                var schedule = this.SelectedSchedule;
                var term = schedule as AcademicTerm;

                if (term != null) {
                    var year = AcademicYears.SingleOrDefault(x => x.Guid == term.YearGuid);

					selector.YearListPicker.SelectedItem = year;
					selector.TermListPicker.ItemsSource = year != null ? GetTermsItemSource(year.Terms) : null;
					selector.TermListPicker.SelectedItem = year != null ? year.Terms.SingleOrDefault(x => x.Guid == schedule.Guid) : null;
				}
				else {
                    var year = AcademicYears.SingleOrDefault(x => x.Guid == schedule.Guid);

					selector.YearListPicker.SelectedItem = year;
					selector.TermListPicker.ItemsSource = year != null ? GetTermsItemSource(year.Terms) : null;
					selector.TermListPicker.SelectedIndex = 0;
				}
			}

			selector.SetCurrentButtonDisabled();

			_preventSelectionChangedEvent = false;
		}

// ReSharper disable ReturnTypeCanBeEnumerable.Local
        private static ObservableCollection<AcademicTerm> GetTermsItemSource(IEnumerable<AcademicTerm> terms) {
            var nCollection = terms != null ? terms.ToObservableCollection() : new ObservableCollection<AcademicTerm>();

            nCollection.Insert(0, new AcademicTerm());

			return nCollection;
		}
        private ObservableCollection<AcademicYear> GetYearsItemSource(IEnumerable<AcademicYear> years) {
            var nCollection = years != null ? years.ToObservableCollection() : new ObservableCollection<AcademicYear>();

            if (this.ShowNone) {
                nCollection.Insert(0, new AcademicYear());
            }

            return nCollection;
		}
// ReSharper restore ReturnTypeCanBeEnumerable.Local

		#endregion
        
        #region Show New 

	    public static readonly DependencyProperty ShowNewProperty = DependencyProperty.Register(
	        "ShowNew", typeof (bool), typeof (AcademicScheduleSelector), new PropertyMetadata(true));

	    public bool ShowNew {
	        get { return (bool) GetValue(ShowNewProperty); }
	        set { SetValue(ShowNewProperty, value); }
	    }

        #endregion

        #region Show None

	    public static readonly DependencyProperty ShowNoneProperty = DependencyProperty.Register(
            "ShowNone", typeof(bool), typeof(AcademicScheduleSelector), new PropertyMetadata(true, OnAcademicYearsChanged));

	    public bool ShowNone {
	        get { return (bool) GetValue(ShowNoneProperty); }
	        set { SetValue(ShowNoneProperty, value); }
	    }

        #endregion

		public AcademicScheduleSelector() {
			this.InitializeComponent();

			this.Loaded += OnLoaded;

			this.CurrentScheduleButton.Tap += CurrentScheduleButtonOnTap;
		}

		private void OnLoaded(object sender, RoutedEventArgs e) {
			this.SetCurrentButtonDisabled();

			this.YearListPicker.SelectionChanged += YearListPickerOnSelectionChanged;
			this.TermListPicker.SelectionChanged += TermComboBoxOnSelectionChanged;
		}

		private void YearListPickerOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
			if (_preventSelectionChangedEvent) return;

		    var selectedSchedule = this.SelectedSchedule;
            var newYear = this.YearListPicker.SelectedItem as AcademicYear;

            if (selectedSchedule != null && newYear != null) {
                AcademicTerm selectedTerm;

                if (selectedSchedule.Equals(newYear) || ((selectedTerm = selectedSchedule as AcademicTerm) != null && selectedTerm.Year.Equals(newYear))) {
                    return;
                }
            }

            this.SelectedSchedule = newYear == null || newYear.Guid.IsEmpty() ? null : newYear;
		}

		private void TermComboBoxOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
			if (_preventSelectionChangedEvent) return;

            var s = this.TermListPicker.SelectedItem as IAcademicSchedule ?? this.YearListPicker.SelectedItem as IAcademicSchedule;

            this.SelectedSchedule = s == null || s.Guid.IsEmpty() ? (IAcademicSchedule)this.YearListPicker.SelectedItem : s;
		}

		private async void CurrentScheduleButtonOnTap(object sender, RoutedEventArgs e) {
			using (var acYearRepo = new AcademicYearRepository()) {
				this.SelectedSchedule = await acYearRepo.GetCurrentAsync();
			}
		}

		private async void SetCurrentButtonDisabled() {
			// There's no schedules, obviously disabled.
			if (this.YearListPicker.Items == null || this.YearListPicker.Items.Count == 0) {
				this.CurrentScheduleButton.IsEnabled = false;
				
				return;
			}

			using (var acScheduleRepo = new AcademicYearRepository()) {
				var currentSchedule = await acScheduleRepo.GetCurrentAsync();

				this.CurrentScheduleButton.IsEnabled = (this.SelectedSchedule == null && currentSchedule != null) ||
													   (this.SelectedSchedule != null && currentSchedule != null && this.SelectedSchedule.Guid != currentSchedule.Guid);
			}
		}

		private void SetMetrics() {
			this.Width = this.Root.Width = App.Current.RootFrame.ActualWidth;
			this.Height = this.Root.Height = App.Current.RootFrame.ActualHeight;
		}

		private IApplicationBar _viewAppBar;

		public static AcademicScheduleSelector GetInstanceAndShow(MslAppView view, string helpText, bool showNew = true) {
		    var selector = new AcademicScheduleSelector {
		        HelpText = {
		            Text = helpText
		        },
                ShowNew = showNew
		    };

		    return GetInstanceAndShowInternal(selector, view);
		}

		private static AcademicScheduleSelector GetInstanceAndShowInternal(AcademicScheduleSelector selector, MslAppView view) {
			selector.SetMetrics();

			selector.DataContext = view.DataContext;

			selector.SetBinding(AcademicYearsProperty, new Binding {
				Path = new PropertyPath("AcademicYears")
			});
			selector.SetBinding(SelectedScheduleProperty, new Binding {
				Path = new PropertyPath("SelectedSchedule"),
				Mode = BindingMode.TwoWay
			});

			selector._hostPopup = new Popup {
				Child = selector,
				IsOpen = true
			};

			SwivelTransition transition = new SwivelTransition { Mode = SwivelTransitionMode.ForwardIn };
			ITransition actualTransition = transition.GetTransition(selector);
			actualTransition.Completed += (s, e) => actualTransition.Stop();
			actualTransition.Begin();

			selector._viewAppBar = view.ApplicationBar;

			view.ApplicationBar = new ApplicationBar {
				BackgroundColor = (Color)App.Current.Resources["ColorAccent"]
			};

			var doneAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Check, "done");

			doneAppBarButton.Click += (s, e) => selector.Done();

			view.ApplicationBar.Buttons.Add(doneAppBarButton);

			EventHandler<CancelEventArgs> backHandler = (s, e) => {
				e.Cancel = true;

				selector.Done();
			};

			view.BackKeyPress += backHandler;

			selector._hostPopup.Closed += (b, c) => {
				selector._hostPopup = null;

				view.ApplicationBar = selector._viewAppBar;

				view.BackKeyPress -= backHandler;
			};

			return selector;
		}

		private void Done() {
			SwivelTransition transition = new SwivelTransition { Mode = SwivelTransitionMode.BackwardOut };
			ITransition actualTransition = transition.GetTransition(this);
			actualTransition.Completed += (s, e) => {
				actualTransition.Stop();

				if (_hostPopup != null) {
					_hostPopup.IsOpen = false;
				}
			};
			actualTransition.Begin();
		}

	    private void NewOnTap(object sender, GestureEventArgs e) {
            this.Done();

	        Mvx.Resolve<INavigationService>().NavigateTo<AcademicYearInputViewModel>();
	    }
	}

	public class AcademicScheduleSelectorItemConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value as IAcademicSchedule == null || ((IAcademicSchedule)value).Guid.IsEmpty()) {
                return "No " + parameter;
            }

            var term = value as AcademicTerm;

            if (term != null) {
                return term.Name;
            }

            return value.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotSupportedException();
		}
	}
}
