﻿using System;
using System.Windows;
using System.Windows.Controls;
using MyStudyLife.UI;
using Cirrious.CrossCore;

namespace MyStudyLife.WP.Controls {
	public partial class NavigationButton : UserControl {
		 public static readonly DependencyProperty IconProperty =
			DependencyProperty.Register("Icon", typeof(string), typeof(NavigationButton), new PropertyMetadata(null));

        public string Icon {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty TypeNameProperty =
			DependencyProperty.Register("TypeName", typeof(string), typeof(NavigationButton), new PropertyMetadata(null));

        public string TypeName {
            get { return (string)GetValue(TypeNameProperty); }
            set { SetValue(TypeNameProperty, value); }
		}

		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register("Text", typeof(string), typeof(NavigationButton), new PropertyMetadata(null));

		public string Text {
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}

        private Type _viewModelType;

        private Type ViewModelType {
            get {
                return _viewModelType ??
                    (_viewModelType = Type.GetType(String.Format("MyStudyLife.UI.ViewModels.{0}ViewModel, MyStudyLife.UI.Core", TypeName), true));
            }
        }

		public NavigationButton() {
            this.InitializeComponent();

            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs) {
            if (String.IsNullOrWhiteSpace(TypeName)) return;

            var iconResourceName = (String.IsNullOrWhiteSpace(Icon) 
                ? (TypeName.EndsWith("es") ? TypeName.Substring(0, TypeName.Length - 2) : TypeName.TrimEnd('s'))
                : Icon) + "Glyph";

            this.NavigationIcon.Text = (String) App.Current.Resources[iconResourceName];
            this.NavigationText.Text = (String.IsNullOrWhiteSpace(Text) ? TypeName : Text);
        }

        private void OnTap(object sender, RoutedEventArgs e) {
            Mvx.Resolve<INavigationService>().NavigateTo(ViewModelType);
        }
	}
}
