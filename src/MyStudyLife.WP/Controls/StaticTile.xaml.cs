﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MyStudyLife.WP.Controls
{
    public partial class StaticTile : UserControl
    {
        #region Dependency Properties

        public static readonly DependencyProperty TopTextProperty =
            DependencyProperty.Register("TopText", typeof(string), typeof(StaticTile), new PropertyMetadata(String.Empty, PropertyChangedCallback));

        /// <summary>
        /// The text displaed at the 
        /// top left of the tile.
        /// </summary>
        /// <remarks>Does not wrap.</remarks>
        public string TopText
        {
            get { return (string) GetValue(TopTextProperty); }
            set { SetValue(TopTextProperty, value); }
        }

        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(string), typeof(StaticTile), new PropertyMetadata("--", PropertyChangedCallback));

        /// <summary>
        /// The large number displayed
        /// in the center of the tile.
        /// </summary>
        public string Number
        {
            get { return (string)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        public static readonly DependencyProperty NumberAnnotationProperty =
            DependencyProperty.Register("NumberAnnotation", typeof(string), typeof(StaticTile), new PropertyMetadata(String.Empty, PropertyChangedCallback));

        /// <summary>
        /// The annotation displayed
        /// next to the number in the
        /// center of the tile.
        /// </summary>
        public string NumberAnnotation
        {
            get { return (string)GetValue(NumberAnnotationProperty); }
            set { SetValue(NumberAnnotationProperty, value); }
        }

        public static readonly DependencyProperty BottomTextProperty =
            DependencyProperty.Register("BottomText", typeof(string), typeof(StaticTile), new PropertyMetadata(String.Empty, PropertyChangedCallback));

        /// <summary>
        /// The annotation displayed
        /// next to the number in the
        /// center of the tile.
        /// </summary>
        public string BottomText
        {
            get { return (string)GetValue(BottomTextProperty); }
            set { SetValue(BottomTextProperty, value); }
        }

        #endregion

        public StaticTile()
        {
            InitializeComponent();

            this.Background = App.Current.Resources["BrushAccent"] as SolidColorBrush;
            this.Foreground = new SolidColorBrush(Colors.White);
        }

        private static void PropertyChangedCallback(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            
        }
    }
}
