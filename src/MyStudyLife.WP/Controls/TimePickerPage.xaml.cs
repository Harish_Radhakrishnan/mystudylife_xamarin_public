﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Primitives;

namespace MyStudyLife.WP.Controls {

    /// <summary>
    /// Represents a page used by the TimePicker control that allows the user to choose a time (hour/minute/am/pm).
    /// </summary>
    public partial class TimePickerPage {
        /// <summary>
        /// Initializes a new instance of the TimePickerPage control.
        /// </summary>
        public TimePickerPage() {
            InitializeComponent();

            // Hook up the data sources
            PrimarySelector.DataSource = DateTimeWrapper.CurrentCultureUsesTwentyFourHourClock() ?
                 (DataSource)(new TwentyFourHourDataSource()) : new TwelveHourDataSource();
            SecondarySelector.DataSource = new MinuteDataSource();
            TertiarySelector.DataSource = new AmPmDataSource();

            ((DataSource)this.PrimarySelector.DataSource).SelectionChanged += this.OnSelectionChanged;
            ((DataSource)this.SecondarySelector.DataSource).SelectionChanged += this.OnSelectionChanged;
            ((DataSource)this.TertiarySelector.DataSource).SelectionChanged += this.OnSelectionChanged;


            InitializeDateTimePickerPage(PrimarySelector, SecondarySelector, TertiarySelector);
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            DataSource source = (DataSource)sender;

            this.PrimarySelector.DataSource.SelectedItem = source.SelectedItem;
            this.SecondarySelector.DataSource.SelectedItem = source.SelectedItem;
            this.TertiarySelector.DataSource.SelectedItem = source.SelectedItem;
        }

        /// <summary>
        /// Gets a sequence of LoopingSelector parts ordered according to culture string for date/time formatting.
        /// </summary>
        /// <returns>LoopingSelectors ordered by culture-specific priority.</returns>
        protected override IEnumerable<LoopingSelector> GetSelectorsOrderedByCulturePattern() {
            string pattern = CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern.ToUpperInvariant();

            // The goal is to put the AM/PM part at the beginning for RTL languages.
            if (IsRTLLanguage()) {
                var parts = pattern.Split(' ');
                Array.Reverse(parts);
                pattern = string.Join(" ", parts);
            }

            return GetSelectorsOrderedByCulturePattern(
                pattern,
                new char[] { 'H', 'M', 'T' },
                new LoopingSelector[] { PrimarySelector, SecondarySelector, TertiarySelector });
        }

        /// <summary>
        /// Handles changes to the page's Orientation property.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnOrientationChanged(OrientationChangedEventArgs e) {
            if (null == e) {
                throw new ArgumentNullException("e");
            }

            base.OnOrientationChanged(e);
            SystemTrayPlaceholder.Visibility = (0 != (PageOrientation.Portrait & e.Orientation)) ?
                Visibility.Visible :
                Visibility.Collapsed;
        }

        /// <summary>
        /// Sets the selectors and title flow direction.
        /// </summary>
        /// <param name="flowDirection">Flow direction to set.</param>
        public override void SetFlowDirection(FlowDirection flowDirection) {
            HeaderTitle.FlowDirection = flowDirection;

            PrimarySelector.FlowDirection = flowDirection;
            SecondarySelector.FlowDirection = flowDirection;
            TertiarySelector.FlowDirection = flowDirection;
        }

        /// <summary>
        /// Returns true if the current language is RTL.
        /// </summary>
        internal static bool IsRTLLanguage() {
            // Currently supported RTL languages are arabic, hebrew and persian.
            string lang = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            return lang == "ar" || lang == "he" || lang == "fa";
        }
    }
}