﻿using System;
using System.Windows;
using System.Windows.Input;
using MyStudyLife.UI;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Controls {
    public partial class EntityListHeader {
        public static readonly DependencyProperty PluralTypeNameProperty =
            DependencyProperty.Register("PluralTypeName", typeof (string), typeof (EntityListHeader), new PropertyMetadata("MY STUDY LIFE"));

        public string PluralTypeName {
            get { return (string) GetValue(PluralTypeNameProperty); }
            set { SetValue(PluralTypeNameProperty, value); }
        }

        public EntityListHeader() {
            InitializeComponent();
        }

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this.GetVisualParent<MslAppView>(),
                String.Format(R.AcademicScheduleTaskExamListHelp, PluralTypeName),
                false
            );
        }
    }
}
