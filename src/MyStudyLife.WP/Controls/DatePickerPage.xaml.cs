﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Controls.Primitives;

namespace MyStudyLife.WP.Controls {
    /// <summary>
    /// Represents a page used by the DatePicker control that allows the user to choose a date (day/month/year).
    /// </summary>
    public partial class DatePickerPage {
        /// <summary>
        /// Initializes a new instance of the DatePickerPage control.
        /// </summary>
        public DatePickerPage() {
            InitializeComponent();

            // Hook up the data sources
            PrimarySelector.DataSource = new YearDataSource();
            SecondarySelector.DataSource = new MonthDataSource();
            TertiarySelector.DataSource = new DayDataSource();

            ((DataSource)this.PrimarySelector.DataSource).SelectionChanged    += this.OnSelectionChanged;
            ((DataSource)this.SecondarySelector.DataSource).SelectionChanged  += this.OnSelectionChanged;
            ((DataSource)this.TertiarySelector.DataSource).SelectionChanged   += this.OnSelectionChanged;

            InitializeDateTimePickerPage(PrimarySelector, SecondarySelector, TertiarySelector);
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            DataSource source = (DataSource)sender;

            this.PrimarySelector.DataSource.SelectedItem    = source.SelectedItem;
            this.SecondarySelector.DataSource.SelectedItem  = source.SelectedItem;
            this.TertiarySelector.DataSource.SelectedItem   = source.SelectedItem;
        }

        /// <summary>
        /// Gets a sequence of LoopingSelector parts ordered according to culture string for date/time formatting.
        /// </summary>
        /// <returns>LoopingSelectors ordered by culture-specific priority.</returns>
        protected override IEnumerable<LoopingSelector> GetSelectorsOrderedByCulturePattern() {
            string pattern = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpperInvariant();

            if (DateShouldFlowRTL()) {
                char[] reversedPattern = pattern.ToCharArray();
                Array.Reverse(reversedPattern);
                pattern = new string(reversedPattern);
            }

            return GetSelectorsOrderedByCulturePattern(
                pattern,
                new char[] { 'Y', 'M', 'D' },
                new LoopingSelector[] { PrimarySelector, SecondarySelector, TertiarySelector });
        }

        /// <summary>
        /// Handles changes to the page's Orientation property.
        /// </summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnOrientationChanged(OrientationChangedEventArgs e) {
            if (null == e) {
                throw new ArgumentNullException("e");
            }

            base.OnOrientationChanged(e);
            SystemTrayPlaceholder.Visibility = (0 != (PageOrientation.Portrait & e.Orientation)) ?
                Visibility.Visible :
                Visibility.Collapsed;
        }

        /// <summary>
        /// Sets the selectors and title flow direction.
        /// </summary>
        /// <param name="flowDirection">Flow direction to set.</param>
        public override void SetFlowDirection(FlowDirection flowDirection) {
            HeaderTitle.FlowDirection = flowDirection;

            PrimarySelector.FlowDirection = flowDirection;
            SecondarySelector.FlowDirection = flowDirection;
            TertiarySelector.FlowDirection = flowDirection;
        }

        /// <summary>
        /// Date should flow from right to left for arabic and persian.
        /// </summary>
        internal static bool DateShouldFlowRTL() {
            string lang = CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
            return lang == "ar" || lang == "fa";
        }
    }
}