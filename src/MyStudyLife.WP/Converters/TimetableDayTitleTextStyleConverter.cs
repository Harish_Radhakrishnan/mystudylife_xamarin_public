﻿using System;
using System.Windows;
using System.Windows.Data;

namespace MyStudyLife.WP.Converters {
    [System.Diagnostics.DebuggerStepThrough]
    public class TimetableDayTitleTextStyleConverter : IValueConverter {
        static Style _currentDayStyle, _standardDayStyle;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (_standardDayStyle == null || _currentDayStyle == null) {
                _standardDayStyle = (Style)App.Current.Resources["TimetableDayTitleTextStyle"];
                _currentDayStyle = (Style)App.Current.Resources["TimetableCurrentDayTitleTextStyle"];
            }

            return DateTime.Today == ((DateTime)value).Date ? _currentDayStyle : _standardDayStyle;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}
