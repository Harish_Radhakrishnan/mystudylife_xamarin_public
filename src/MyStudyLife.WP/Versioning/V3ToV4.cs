﻿using MyStudyLife.Versioning.Shared;
using System;

namespace MyStudyLife.WP.Versioning {
	
	public class V3ToV4 : JsonToSQLite {
		public override Version FromVersion {
			get { return new Version(3, 0); }
		}
		public override Version ToVersion {
			get { return new Version(4, 0); }
		}
	}
}
