﻿using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.WP.Versioning {
    public sealed class V4ToV5 : TeacherSharingUpgrade {
        public override Version FromVersion {
            get { return new Version(4,0); }
        }

        public override Version ToVersion {
            get { return new Version(5,0); }
        }
    }
}
