﻿using MyStudyLife.Versioning.Shared;
using System;

namespace MyStudyLife.WP.Versioning {
    public class V605ToV606 : SchoolLogoUpgrade {
        public override Version FromVersion => new Version(6, 0, 5);

        public override Version ToVersion => new Version(6, 0, 6);
    }
}
