﻿using System;
using MyStudyLife.Versioning.Shared;

namespace MyStudyLife.WP.Versioning {
    public sealed class V5ToV6 : HolidaysUpgrade {
        public override Version FromVersion {
            get { return new Version(5,0); }
        }

        public override Version ToVersion {
            get { return new Version(6,0); }
        }
    }
}
