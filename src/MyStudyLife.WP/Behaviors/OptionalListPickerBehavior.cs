﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Cirrious.CrossCore.WeakSubscription;
using Microsoft.Phone.Controls;

namespace MyStudyLife.WP.Behaviors {
    public class OptionalListPickerBehavior : Behavior<ListPicker> {
        private IDisposable _itemsSourceSubscription;

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(
            "ItemsSource", typeof(object), typeof(OptionalListPickerBehavior), new PropertyMetadata(null, ItemsSourceOnChanged));

        private static void ItemsSourceOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = (OptionalListPickerBehavior)d;

            if (behavior._itemsSourceSubscription != null) {
                behavior._itemsSourceSubscription.Dispose();
                behavior._itemsSourceSubscription = null;
            }

            var incc = e.NewValue as INotifyCollectionChanged;

            if (incc != null) {
                behavior._itemsSourceSubscription = incc.WeakSubscribe(
                    new EventHandler<NotifyCollectionChangedEventArgs>((sender, args) => behavior.SetItemsSource())
                );
            }

            behavior.SetItemsSource();
        }

        public object ItemsSource {
            get { return GetValue(ItemsSourceProperty); }
            set {
                SetValue(ItemsSourceProperty, value);
            }
        }

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(
            "SelectedItem", typeof(object), typeof(OptionalListPickerBehavior), new PropertyMetadata(null, SelectedItemOnChanged));

        private static void SelectedItemOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = (OptionalListPickerBehavior)d;

            behavior.AssociatedObject.SelectedItem = e.NewValue ?? NullShim.Instance;
        }

        public object SelectedItem {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        private void SetItemsSource() {
            var listPicker = this.AssociatedObject;

            if (listPicker != null) {
                var itemsSource = new List<object> {
                    NullShim.Instance
                };

// ReSharper disable once LoopCanBeConvertedToQuery
                foreach (var item in (IEnumerable)this.ItemsSource) {
                   itemsSource.Add(item);
                }

                listPicker.ItemsSource = itemsSource;
            }
        }

        protected override void OnAttached() {
            base.OnAttached();

            this.AssociatedObject.SelectionChanged += ListPickerOnSelectionChanged;
        }

        private void ListPickerOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var selectedItem = this.AssociatedObject.SelectedItem;

            this.SelectedItem = selectedItem is NullShim ? null : selectedItem;
        }

        protected override void OnDetaching() {
            if (_itemsSourceSubscription != null) {
                _itemsSourceSubscription.Dispose();
                _itemsSourceSubscription = null;
            }

            base.OnDetaching();
        }

        public sealed class NullShim {
            private static NullShim _instance;

            public static NullShim Instance {
                get { return _instance ?? (_instance = new NullShim()); }
            }

            public bool IsNull { get { return true; } }

            private NullShim() { }
        }
    }
}
