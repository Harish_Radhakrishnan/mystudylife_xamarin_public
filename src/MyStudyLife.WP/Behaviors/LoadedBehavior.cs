﻿using System;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MyStudyLife.WP.Behaviors {
    public class LoadedBehavior : Behavior<FrameworkElement> {
        protected override void OnAttached() {
            base.OnAttached();

            AssociatedObject.Loaded += AssociatedObject_Loaded;

            AssociatedObject.Opacity = 0;
            AssociatedObject.RenderTransform = new ScaleTransform {
                ScaleX = 0,
                ScaleY = 0,
                // Use common sizes
                CenterX = 432 / 2,
                CenterY = 42
            };
        }

        protected override void OnDetaching() {
            base.OnDetaching();

            AssociatedObject.Loaded -= AssociatedObject_Loaded;
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs e) {
            var transform = (ScaleTransform)AssociatedObject.RenderTransform;

            transform.CenterX = AssociatedObject.ActualWidth / 2;
            transform.CenterY = AssociatedObject.ActualHeight / 2;

            AssociatedObject.Dispatcher.BeginInvoke(CreateStoryboard().Begin);
        }

        private Storyboard CreateStoryboard() {
            Storyboard sb = new Storyboard();

            DoubleAnimation opacityAnimation = new DoubleAnimation {
                Duration = TimeSpan.FromMilliseconds(300),
                From = 0,
                To = 1,
                EasingFunction = new CubicEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("(UIElement.Opacity)"));

            DoubleAnimation scaleXAnimation = new DoubleAnimation {
                Duration = TimeSpan.FromMilliseconds(300),
                From = 0,
                To = 1,
                EasingFunction = new CubicEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(scaleXAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleX)"));

            DoubleAnimation scaleYAnimation = new DoubleAnimation {
                Duration = TimeSpan.FromMilliseconds(300),
                From = 0,
                To = 1,
                EasingFunction = new CubicEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(scaleYAnimation, new PropertyPath("(UIElement.RenderTransform).(ScaleTransform.ScaleY)"));

            sb.Children.Add(opacityAnimation);
            sb.Children.Add(scaleXAnimation);
            sb.Children.Add(scaleYAnimation);

            Storyboard.SetTarget(sb, AssociatedObject);

            return sb;
        }
    }
}
