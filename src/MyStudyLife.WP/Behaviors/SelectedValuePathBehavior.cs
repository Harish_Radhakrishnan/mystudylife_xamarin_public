﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Cirrious.CrossCore.IoC;
using Cirrious.CrossCore.Platform;
using Microsoft.Phone.Controls;

namespace MyStudyLife.WP.Behaviors {
	public sealed class SelectedValuePathBehavior : Behavior<ListPicker> {
		private Type _selectedValueType;

		public static readonly DependencyProperty SelectedValueProperty =
			DependencyProperty.Register("SelectedValue", typeof(object), typeof(SelectedValuePathBehavior), new PropertyMetadata(default(object), OnDependencyPropertyChanged));

		private static void OnDependencyPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var behavior = (SelectedValuePathBehavior) d;

		    if (behavior.AssociatedObject != null && behavior.EnsureType()) {
		        var property = behavior._selectedValueType.GetProperty(behavior.SelectedValuePath);

		        if (behavior.SelectedValue == null) {
		            behavior.AssociatedObject.SelectedItem = behavior.AssociatedObject.Items.First();
		        }
		        else {
                    var selectedItem = behavior.AssociatedObject.Items.FirstOrDefault(x => property.GetValue(x, null).Equals(behavior.SelectedValue));

		            if (selectedItem != null) {
		                behavior.AssociatedObject.SelectedItem = selectedItem;
		            }
		            else {
		                MvxTrace.Warning("Attempted to set ListPicker.SelectedItem to null");
		            }
		        }
		    }
		}

		public object SelectedValue {
			get { return GetValue(SelectedValueProperty); }
			set { SetValue(SelectedValueProperty, value); }
		}

		public static readonly DependencyProperty SelectedValuePathProperty =
			DependencyProperty.Register("SelectedValuePath", typeof(string), typeof(SelectedValuePathBehavior), new PropertyMetadata(null, OnDependencyPropertyChanged));
			
		public string SelectedValuePath {
			get { return (string)GetValue(SelectedValuePathProperty); }
			set { SetValue(SelectedValuePathProperty, value); }
		}

		protected override void OnAttached() {
			base.OnAttached();

			this.AssociatedObject.SelectionChanged += AssociatedObjectOnSelectionChanged;
		}
		protected override void OnDetaching() {
			base.OnDetaching();

			this.AssociatedObject.SelectionChanged -= AssociatedObjectOnSelectionChanged;
		}

		private void AssociatedObjectOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.SetSelectedItem();
		}

		private void SetSelectedItem() {
			if (this.AssociatedObject.SelectedItem == null || this.SelectedValuePath == null) {
				this.SelectedValue = _selectedValueType == null ? null : _selectedValueType.CreateDefault();
			}
			else {
				EnsureType();

				this.SelectedValue = _selectedValueType.GetProperty(this.SelectedValuePath).GetValue(this.AssociatedObject.SelectedItem, null);
			}
		}

		private bool EnsureType() {
			if (this._selectedValueType == null) {
				if (this.AssociatedObject.SelectedItem != null) {
					this._selectedValueType = this.AssociatedObject.SelectedItem.GetType();
				}
				else if (this.AssociatedObject.ItemsSource != null && this.AssociatedObject.ItemsSource.Cast<object>().Any()) {
					this._selectedValueType = this.AssociatedObject.ItemsSource.Cast<object>().ElementAt(0).GetType();
				}
				else {
					return false;
				}
			}

			return true;
		}
	}
}
