﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;
using MyStudyLife.Data;

namespace MyStudyLife.WP.Behaviors {
    public abstract class TeacherOrStudentColorBehavior : Behavior<FrameworkElement> {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof(User), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public User User {
            get { return (User) GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public static readonly DependencyProperty ColorIfTeacherProperty = DependencyProperty.Register(
            "ColorIfTeacher", typeof(Brush), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public Brush ColorIfTeacher {
            get { return (Brush) GetValue(ColorIfTeacherProperty); }
            set { SetValue(ColorIfTeacherProperty, value); }
        }

        public static readonly DependencyProperty ColorIfStudentProperty = DependencyProperty.Register(
            "ColorIfStudent", typeof(Brush), typeof(TeacherOrStudentColorBehavior),
            new PropertyMetadata(null, PropertyChangedCallback));

        public Brush ColorIfStudent {
            get { return (Brush) GetValue(ColorIfStudentProperty); }
            set { SetValue(ColorIfStudentProperty, value); }
        }

        private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            ((TeacherOrStudentColorBehavior)d).Invalidate();
        }

        protected override void OnAttached() {
            this.Invalidate();
        }

        private void Invalidate() {
            if (this.AssociatedObject == null || this.ColorIfStudent == null && this.ColorIfTeacher == null) {
                return;
            }

            bool isTeacher = this.User != null && this.User.IsTeacher;

            this.SetColor(isTeacher
                ? ColorIfTeacher ?? ColorIfStudent
                : ColorIfStudent
            );
        }

        protected abstract void SetColor(Brush brush);
    }

    public class TeacherOrStudentFillBehavior : TeacherOrStudentColorBehavior {
        protected override void SetColor(Brush brush) {
            ((Rectangle) this.AssociatedObject).Fill = brush;
        }
    }

    public class TeacherOrStudentBackgroundBehavior : TeacherOrStudentColorBehavior {
        private Action<Brush> _setColorAction;

        protected override void OnAttached() {
            base.OnAttached();

            if (this.AssociatedObject is Border) {
                _setColorAction = brush => {
                    ((Border) (this.AssociatedObject)).Background = brush;
                };
            }
            else if (this.AssociatedObject is Panel) {
                _setColorAction = brush => {
                    ((Panel) (this.AssociatedObject)).Background = brush;
                };
            }
            else {
                Debug.WriteLine("TeacherOrStudentBackgroundBehavior: Unknown type '{0}'", this.AssociatedObject.GetType());
            }
        }

        protected override void SetColor(Brush brush) {
            if (_setColorAction != null) {
                _setColorAction(brush);
            }
        }
    }

    public class TeacherOrStudentForegroundBehavior : TeacherOrStudentColorBehavior {
        protected override void SetColor(Brush brush) {
            ((TextBlock) this.AssociatedObject).Foreground = brush;
        }
    }

    public class TeacherOrStudentBorderBrushBehavior : TeacherOrStudentColorBehavior {
        protected override void SetColor(Brush brush) {
            ((Border)this.AssociatedObject).BorderBrush = brush;
        }
    }
}
