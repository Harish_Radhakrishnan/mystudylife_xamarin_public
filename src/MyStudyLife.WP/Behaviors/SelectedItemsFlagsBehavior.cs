﻿using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using Microsoft.Phone.Controls;

namespace MyStudyLife.WP.Behaviors {
	public class SelectedItemsFlagsBehavior : Behavior<ListPicker> {
		public static readonly DependencyProperty FlagsProperty =
			DependencyProperty.Register("Flags", typeof (int), typeof (SelectedItemsFlagsBehavior), new PropertyMetadata(0, FlagsOnChanged));

		private static void FlagsOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var behavior = (SelectedItemsFlagsBehavior) d;

		    if (behavior.AssociatedObject.ItemsSource != null) {
		        int flags = (int) e.NewValue;

		        behavior.AssociatedObject.SelectedItems = behavior.AssociatedObject.ItemsSource.Cast<object>().Where(x => {
		            var flag = behavior.GetFlag(x);

		            return (flags & flag) == flag;
		        }).ToList();
		    }
		}

		public int Flags {
			get { return (int)GetValue(FlagsProperty); }
			set { SetValue(FlagsProperty, value); }
		}

		public static readonly DependencyProperty FlagMemberPathProperty =
			DependencyProperty.Register("FlagMemberPath", typeof (string), typeof (SelectedItemsFlagsBehavior), new PropertyMetadata(default(string)));

		public string FlagMemberPath {
			get { return (string) GetValue(FlagMemberPathProperty); }
			set { SetValue(FlagMemberPathProperty, value); }
		}

		protected override void OnAttached() {
			base.OnAttached();

			this.AssociatedObject.SelectionChanged += AssociatedObjectOnSelectionChanged;
		}

		private PropertyInfo _flagProperty;

		private void AssociatedObjectOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
			this.Flags = this.AssociatedObject.SelectedItems == null ? 0 : this.AssociatedObject.SelectedItems.Cast<object>().Aggregate<object, int>(0, (current, item) => current | GetFlag(item));
		}

		private int GetFlag(object item) {
			if (FlagMemberPath == null) {
				return (int)item;
			}

			if (_flagProperty == null) {
				_flagProperty = item.GetType().GetProperty(FlagMemberPath);
			}

			return (int) _flagProperty.GetValue(item, null);
		}
	}
}
