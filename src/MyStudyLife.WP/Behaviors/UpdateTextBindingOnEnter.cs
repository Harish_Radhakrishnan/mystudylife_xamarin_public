﻿using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace MyStudyLife.WP.Behaviors
{
    /// <summary>
    /// Custom behavior that updates the source of 
    /// a binding on a text box when the enter or submit
    /// key is pressed.
    /// </summary>
    public class UpdateTextBindingOnEnter : Behavior<TextBox>
    {
        private BindingExpression _expression;

        /// <summary>
        /// Called after the behavior is attached to an AssociatedObject.
        /// </summary>
        /// <remarks>
        /// Override this to hook up functionality to the AssociatedObject.
        /// </remarks>
        protected override void OnAttached()
        {
            base.OnAttached();

            this._expression = this.AssociatedObject.GetBindingExpression(TextBox.TextProperty);
            this.AssociatedObject.KeyDown += this.OnKeyDown;
        }

        /// <summary>
        /// Called when the behavior is being detached from its AssociatedObject, but before it has actually occurred.
        /// </summary>
        /// <remarks>
        /// Override this to unhook functionality from the AssociatedObject.
        /// </remarks>
        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.KeyDown -= this.OnKeyDown;
            this._expression = null;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            // PlatformKeyCode allows for external input such as keyboard on emulator.
            if (e.Key == Key.Enter || e.PlatformKeyCode == 0x0a)
            {
                this._expression.UpdateSource();
            }
        }
    }
}
