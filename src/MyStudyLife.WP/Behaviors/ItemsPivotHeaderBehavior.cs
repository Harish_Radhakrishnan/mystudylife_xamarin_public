﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;
using System.Windows.Media;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.WP.Converters;

namespace MyStudyLife.WP.Behaviors {
	public class ItemsPivotHeaderBehavior : Behavior<TextBlock> {

		public static readonly DependencyProperty PluralTypeNameProperty =
			DependencyProperty.Register("PluralTypeName", typeof (string), typeof (ItemsPivotHeaderBehavior), new PropertyMetadata(default(string), PluralTypeNameOnChanged));

		private static void PluralTypeNameOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((ItemsPivotHeaderBehavior) d).Set();
		}

		public static readonly DependencyProperty SelectedSubjectProperty =
			DependencyProperty.Register("SelectedSubject", typeof (Subject), typeof (ItemsPivotHeaderBehavior), new PropertyMetadata(default(Subject), SelectedSubjectOnChanged));

		private static void SelectedSubjectOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((ItemsPivotHeaderBehavior)d).Set();
		}

		public Subject SelectedSubject {
			get { return (Subject) GetValue(SelectedSubjectProperty); }
			set { SetValue(SelectedSubjectProperty, value); }
		}

		public string PluralTypeName {
			get { return (string) GetValue(PluralTypeNameProperty); }
			set { SetValue(PluralTypeNameProperty, value); }
		}

		protected override void OnAttached() {
			base.OnAttached();

			this.Set();
		}

		private SolidColorBrush _accentBrush;
		private SubjectColorToValueConverter _subjectColorConverter;

		private SolidColorBrush AccentBrush {
			get {
				return _accentBrush ??
				       (_accentBrush = (SolidColorBrush) Application.Current.Resources["BrushAccent"]);
			}
		}
		private SubjectColorToValueConverter SubjectColorConverter {
			get {
				return _subjectColorConverter ??
					(_subjectColorConverter = (SubjectColorToValueConverter) Application.Current.Resources["SubjectColorToValueConverter"]);
			}
		}

		private void Set() {
		    if (DesignerProperties.IsInDesignTool || AssociatedObject == null) return;

			if (SelectedSubject == null || SelectedSubject.Color == "NULL") {
				AssociatedObject.Foreground = AccentBrush;
				AssociatedObject.Text = (PluralTypeName ?? String.Empty).ToUpper();
			}
			else {
				AssociatedObject.Foreground = (SolidColorBrush)SubjectColorConverter.Convert(SelectedSubject.Color, typeof(SolidColorBrush), null, CultureInfo.CurrentCulture);
				AssociatedObject.Text = String.Format("{0} - {1}", PluralTypeName, SelectedSubject.Name).ToUpper();
			}
		}
	}
}
