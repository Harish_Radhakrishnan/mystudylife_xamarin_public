﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;

namespace MyStudyLife.WP.Behaviors {
    public class PopupStoryboardBehavior : Behavior<Popup> {
        #region Storyboards

        public static readonly DependencyProperty OnOpenProperty =
            DependencyProperty.Register("OnOpen", typeof(string), typeof(PopupStoryboardBehavior), new PropertyMetadata(null));

        public static readonly DependencyProperty OnCloseProperty =
            DependencyProperty.Register("OnClose", typeof(string), typeof(PopupStoryboardBehavior), new PropertyMetadata(null));

        public string OnOpen {
            get { return (string)GetValue(OnOpenProperty); }
            set { SetValue(OnOpenProperty, value); }
        }

        public string OnClose {
            get { return (string)GetValue(OnCloseProperty); }
            set { SetValue(OnCloseProperty, value); }
        }

        #endregion

        protected override void OnAttached() {
            base.OnAttached();

            AssociatedObject.Opened += AssociatedObject_Opened;
            AssociatedObject.Closed += AssociatedObject_Closed;
        }

        protected override void OnDetaching() {
            base.OnDetaching();

            AssociatedObject.Opened -= AssociatedObject_Opened;
            AssociatedObject.Closed -= AssociatedObject_Closed;
        }

        private void AssociatedObject_Opened(object sender, EventArgs e) {
            if (!String.IsNullOrWhiteSpace(OnOpen)) {
                ((Storyboard)AssociatedObject.Resources[OnOpen]).Begin();
            }
        }

        private void AssociatedObject_Closed(object sender, EventArgs e) {
            if (!String.IsNullOrWhiteSpace(OnClose)) {
                ((Storyboard)AssociatedObject.Resources[OnClose]).Begin();
            }
        }
    }
}
