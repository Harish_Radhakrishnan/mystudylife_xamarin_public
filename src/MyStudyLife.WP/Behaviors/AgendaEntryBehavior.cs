﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Interactivity;
using System.Windows.Media.Animation;
using MyStudyLife.Data;
using MyStudyLife.Scheduling;

namespace MyStudyLife.WP.Behaviors {
    /// <summary>
    /// A behavior which allows the agenda entry
    /// style to be changed dependent on it's
    /// properties.
    /// </summary>
    public class AgendaEntryBehavior : Behavior<FrameworkElement> {

        #region Dependency Properties

        public static readonly DependencyProperty AgendaEntryProperty =
            DependencyProperty.Register("AgendaEntry", typeof(AgendaEntry), typeof(AgendaEntryBehavior), new PropertyMetadata(null));

        public AgendaEntry AgendaEntry {
            get { return (AgendaEntry)GetValue(AgendaEntryProperty); }
            set { SetValue(AgendaEntryProperty, value); }
        }

        #endregion

        protected override void OnAttached() {
            base.OnAttached();

            AssociatedObject.Loaded += AssociatedObject_Loaded;
        }
        protected override void OnDetaching() {
            base.OnDetaching();

            AssociatedObject.Loaded -= AssociatedObject_Loaded;

            if (AgendaEntry != null) {
                AgendaEntry.PropertyChanged -= AgendaEntryOnPropertyChanged;
            }
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs e) {
            SetStyle();

            if (AgendaEntry != null) {
                AgendaEntry.PropertyChanged += AgendaEntryOnPropertyChanged;
            }
        }

        private void AgendaEntryOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "IsConflicting" || e.PropertyName == "MinutesRemaining") {
                SetStyle();
            }
        }

        private void SetStyle() {
            if (AgendaEntry.EndTime < DateTime.Now || AgendaEntry.IsConflicting) {
                ((Storyboard)AssociatedObject.Resources["PastOrConflictStoryboard"]).Begin();
            }
            else {
                AssociatedObject.SetValue(UIElement.OpacityProperty, 1);
            }
        }
    }
}
