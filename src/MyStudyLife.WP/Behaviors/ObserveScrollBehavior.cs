﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace MyStudyLife.WP.Behaviors {
    public class ObserveScrollBehavior : Behavior<ScrollViewer> {
        #region Properties

        public static readonly DependencyProperty VerticalOffsetProperty =
            DependencyProperty.Register("VerticalOffset", typeof(double), typeof(ObserveScrollBehavior), new PropertyMetadata(default(double), VerticalOffset_Changed));


        public double VerticalOffset {
            get { return (double)GetValue(VerticalOffsetProperty); }
            set { SetValue(VerticalOffsetProperty, value); }
        }

        public static readonly DependencyProperty HorizontalOffsetProperty =
            DependencyProperty.Register("HorizontalOffset", typeof(double), typeof(ObserveScrollBehavior), new PropertyMetadata(default(double), HorizontalOffset_Changed));

        public double HorizontalOffset {
            get { return (double)GetValue(HorizontalOffsetProperty); }
            set { SetValue(HorizontalOffsetProperty, value); }
        }

        #endregion

        private volatile bool _ignoreUpdates = false;

        protected override void OnAttached() {
            base.OnAttached();

            AssociatedObject.LayoutUpdated += AssociatedObject_LayoutUpdated;
            AssociatedObject.Loaded += AssociatedObject_Loaded;
        }

        protected override void OnDetaching() {
            base.OnDetaching();

            AssociatedObject.LayoutUpdated -= AssociatedObject_LayoutUpdated;
            AssociatedObject.Loaded -= AssociatedObject_Loaded;
        }

        private void AssociatedObject_LayoutUpdated(object sender, EventArgs e) {
            _ignoreUpdates = true;

            this.VerticalOffset = AssociatedObject.VerticalOffset;
            this.HorizontalOffset = AssociatedObject.HorizontalOffset;

            _ignoreUpdates = false;
        }

        private void AssociatedObject_Loaded(object sender, RoutedEventArgs routedEventArgs) {
            AssociatedObject.ScrollToVerticalOffset(this.VerticalOffset);
            AssociatedObject.ScrollToHorizontalOffset(this.HorizontalOffset);
        }

        private static void VerticalOffset_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = d as ObserveScrollBehavior;

            if (behavior != null && !behavior._ignoreUpdates && behavior.AssociatedObject != null) {
                behavior.AssociatedObject.ScrollToVerticalOffset((double)e.NewValue);
            }
        }

        private static void HorizontalOffset_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var behavior = d as ObserveScrollBehavior;

            if (behavior != null && !behavior._ignoreUpdates && behavior.AssociatedObject != null) {
                behavior.AssociatedObject.ScrollToHorizontalOffset((double)e.NewValue);
            }
        }
    }
}
