﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Xml.Linq;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Mindscape.Raygun4Net;
using MyStudyLife.Diagnostics;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Sync;
using MyStudyLife.UI;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Platform;
using MyStudyLife.WP.Services;

namespace MyStudyLife.WP {
    sealed partial class App {
        #region Properties

        public static new App Current { get; private set; }

        public TypedResourceDictionary MslResources { get; private set; }

        /// <summary>
        ///		Gets the current application's root frame.
        /// </summary>
        public TransitionFrame RootFrame { get; private set; }

	    private Version _version;

		/// <summary>
		///		Gets the current application version.
		/// </summary>
	    public Version Version {
		    get {
			    if (_version == null) {
				    var manifest = XDocument.Load("WMAppManifest.xml");

// ReSharper disable PossibleNullReferenceException
					_version = new Version(manifest.Root.Element("App").Attribute("Version").Value);
// ReSharper restore PossibleNullReferenceException
			    }

			    return _version;
		    }
	    }

        #endregion

		public readonly RaygunClient RaygunClient = new RaygunClient(WPConfig.RaygunClientApiKey);

        public App() {
            RaygunClient.ApplicationVersion = Version.ToString(3);

			this.UnhandledException += (s, e) => {
				// This occurs when SelectedItem is attempted to be set to null for ListPickers. This
                // should only occur after saving an item.
                if (e.ExceptionObject.Message == "SelectedItem must always be set to a valid value.") {

                    e.Handled = true;
					return;
				}

				if (Debugger.IsAttached) {
					Debugger.Break();
				}
				else {
					RaygunClient.Send(e);
				}
			};

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

			#region Debug Options
            // Show graphics profiling information while debugging.
            if (Debugger.IsAttached) {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = false;

                // Show the areas of the app that are being redrawn in each frame.
                Application.Current.Host.Settings.EnableRedrawRegions = false;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are handed off to GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Disable the application idle detection by setting the UserIdleDetectionMode property of the
                // application's PhoneApplicationService object to Disabled.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
			#endregion

            MslResources = new TypedResourceDictionary(Resources);

            Current = this;
        }

	    #region Phone application initialization

        // Avoid double-initialization
        private bool _phoneApplicationInitialized;

        // Do not add any additional code to this method
        private void InitializePhoneApplication() {
            if (_phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new TransitionFrame {
                Background = new SolidColorBrush(Colors.White)
            };
			RootFrame.Navigated += CompleteInitializePhoneApplication;
            RootFrame.Navigated += RootFrameOnNavigated;

			RootFrame.Navigating += RootFrameOnNavigating;
            RootFrame.NavigationFailed += RootFrameOnNavigationFailed;
            
            var setup = new WPSetup(RootFrame);
            setup.Initialize();
			
            // Ensure we don't initialize again
			_phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e) {
            // Set the root visual to allow the application to render
            RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
			RootFrame.Navigated -= CompleteInitializePhoneApplication;
        
			Mvx.Resolve<ISyncService>().Completed += OnCompleted;
		}

		#region Reminder Service

        private IReminderService _reminderService;

        private IReminderService ReminderService {
		    get {
		        return _reminderService ?? (_reminderService = Mvx.Resolve<IReminderService>());
		    }
	    }

	    private async void OnCompleted(object sender, SyncCompletedEventArgs e) {
		    try {
			    await ReminderService.RunAsync();
		    }
		    catch (Exception ex) {
			    if (!(ex is HttpApiUnauthorizedException)) {
				    Mvx.Resolve<IBugReporter>().Send(ex, new List<string> { "ReminderService" });
			    }
		    }
	    }

	    #endregion

	    private void RootFrameOnNavigationFailed(object sender, NavigationFailedEventArgs e) {
			Debug.WriteLine("Navigation failed: " + e.Exception);

			e.Handled = true;
		}

		#region MvvmCross

		private bool _hasDoneFirstNavigation, _reset;

        private void RootFrameOnNavigating(object sender, NavigatingCancelEventArgs e) {
            // Fast app resume handling, prevents duplicate back presses required
            if (_reset && e.IsCancelable && e.Uri.OriginalString == "/MainPage.xaml") {
                e.Cancel = true;
                _reset = false;
            }

			if (_hasDoneFirstNavigation) return;

			e.Cancel = true;
			_hasDoneFirstNavigation = true;

	        var appStart = Mvx.Resolve<IMvxAppStart>();

			RootFrame.Dispatcher.BeginInvoke(() => appStart.Start(new AppStartHint(PhoneApplicationService.Current.StartupMode == StartupMode.Activate ? AppStartMode.Resume : AppStartMode.New)));
        }

        private void RootFrameOnNavigated(object sender, NavigationEventArgs e) {
            _reset = e.NavigationMode == NavigationMode.Reset;
        }

		#endregion

		#region Application Lifecycle

	    private void Application_Launching(object sender, LaunchingEventArgs e) {
	    }

        private void Application_Deactivated(object sender, DeactivatedEventArgs e) {
            BackgroundTaskRegistrationService.AttemptLaunch();
        }

        private void Application_Closing(object sender, ClosingEventArgs e) {
            BackgroundTaskRegistrationService.AttemptLaunch();
        }

		#endregion

        #endregion
    }
}