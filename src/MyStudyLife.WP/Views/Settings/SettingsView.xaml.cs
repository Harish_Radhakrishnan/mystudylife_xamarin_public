﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.WP.Views.Settings {
    public partial class SettingsView {
        public new SettingsViewModel ViewModel {
            get { return (SettingsViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override bool AutoAppBarCreation {
            get { return false; }
        }

        public SettingsView() {
            InitializeComponent();
        }

        private void NavigationItem_Tapped(object sender, GestureEventArgs e) {
            var items = this.ViewModel.Settings;

            if (items != null) {
                int itemIndex = items.IndexOf((string)((Button)sender).DataContext);

                string viewModel;

                switch (itemIndex) {
                    case 0:
                        viewModel = "General";
                        break;
                    case 1:
                        viewModel = "Reminder";
                        break;
                    case 2:
                        viewModel = "Sync";
                        break;
                    case 3:
                        viewModel = "Timetable";
                        break;
                    default:
                        throw new NotImplementedException("Extra item found in settings list.");
                }

                this.ViewModel.NavigateCommand.Execute("Settings." + viewModel + "Settings");
            }
        }
    }
}