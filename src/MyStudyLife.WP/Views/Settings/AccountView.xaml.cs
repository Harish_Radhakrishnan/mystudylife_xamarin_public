﻿namespace MyStudyLife.WP.Views.Settings {
    public partial class AccountView {
	    #region Overrides of MslAppView

	    /// <summary>
	    ///     True if the view automatically handles
	    ///     the creation/destruction of the app bar.
	    /// </summary>
	    public override bool AutoAppBarCreation {
		    get { return false; }
	    }

	    #endregion

	    public AccountView() {
		    this.InitializeComponent();
	    }
    }
}