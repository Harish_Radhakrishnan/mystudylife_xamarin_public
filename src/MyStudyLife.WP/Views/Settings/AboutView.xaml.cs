﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Tasks;

namespace MyStudyLife.WP.Views.Settings {
    public partial class AboutView {
	    #region Overrides of MslAppView

	    /// <summary>
	    ///     True if the view automatically handles
	    ///     the creation/destruction of the app bar.
	    /// </summary>
	    public override bool AutoAppBarCreation {
		    get { return false; }
	    }

	    #endregion

	    public AboutView() {
            InitializeComponent();

	        this.VersionRunText.Text = App.Current.Version.ToString(3);
            this.YearRunText.Text = DateTime.Now.Year.ToString(CultureInfo.InvariantCulture);
        }

        private void RequestSupport_OnClick(object sender, RoutedEventArgs e) {
            EmailComposeTask emailComposeTask = new EmailComposeTask {
                To = "support@mystudylife.com",
                Subject = String.Format("Windows Phone {0} support", this.VersionRunText.Text)
            };

            emailComposeTask.Show();
        }

        private void HyperlinkButton_OnClick(object sender, RoutedEventArgs e) {
            WebBrowserTask webBrowserTask = new WebBrowserTask {
                Uri = new Uri(((FrameworkElement)sender).Tag.ToString())
            };

            webBrowserTask.Show();
        }
    }
}