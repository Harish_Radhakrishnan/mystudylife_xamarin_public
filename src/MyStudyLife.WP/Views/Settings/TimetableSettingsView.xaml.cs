﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Cirrious.MvvmCross.ViewModels;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Settings;

namespace MyStudyLife.WP.Views.Settings {
	public partial class TimetableSettingsView {
	    [UsedImplicitly] private MvxPropertyChangedListener _listener;

		public new TimetableSettingsViewModel ViewModel {
			get { return base.ViewModel as TimetableSettingsViewModel; }
		}

        public TimetableSettingsView() {
			InitializeComponent();

			this.DaysPicker.SummaryForSelectedItemsDelegate = SummaryForSelectedItemsDelegate;
		}

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.User, SetIsReadOnly);

            this.SetIsReadOnly();
        }

        private void SetIsReadOnly() {
            var user = this.ViewModel != null ? this.ViewModel.User : null;

            if (user != null) {
                // Students with schools will never see this
#pragma warning disable CS0618 // Type or member is obsolete
                if (user.IsStudentWithSchool || user.IsTeacher) {
                    this.WarningMessage.Visibility = Visibility.Visible;
                    ((TextBlock)this.WarningMessage.Child).Text = user.IsStudentWithSchool
                        ? R.TimetableSettings.StudentWithSchoolMessage
                        : R.TimetableSettings.TeacherWithSchoolMessage;
                }
                else {
                    this.WarningMessage.Visibility = Visibility.Collapsed;
                }

                this.SettingsContent.IsHitTestVisible = !user.IsStudentWithSchool;
                this.SettingsContent.Opacity = this.SettingsContent.IsHitTestVisible ? 1 : 0.5f;
#pragma warning restore CS0618 // Type or member is obsolete
            }
        }

		private string SummaryForSelectedItemsDelegate(IList list) {
			if (list == null || list.Count == 0) return "No Days Selected";

			return list.Cast<Tuple<int, string, int>>().Select(x => x.Item2).Aggregate((a, b) => String.Format("{0}, {1}", a, b));
		}
    }
}