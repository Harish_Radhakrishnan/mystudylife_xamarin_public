﻿using System.Windows;
using MyStudyLife.UI.ViewModels.Settings;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.Settings {
    public abstract class SettingsItemView : MslAppView {
        public override bool AutoAppBarCreation {
            get { return false; }
        }

        private new ISettingsViewModel ViewModel {
            get { return (ISettingsViewModel)DataContext; }
            set { DataContext = value; }
        }

        protected SettingsItemView() {
            this.Unloaded += OnUnloaded;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e) {
            if (this.ViewModel != null) {
                this.ViewModel.SaveChanges();
            }
        }
    }
}