﻿using System.Collections.Generic;
using Microsoft.Phone.Controls;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.WP.Views {
    public class InternalTasksView : BaseEntitiesView<Task, TasksViewModel> {}

    public partial class TasksView {

        private readonly List<LongListMultiSelector> _filteredLists;

        public override IEnumerable<LongListMultiSelector> FilteredLists {
            get { return _filteredLists; }
        }

        protected override string PluralTypeName {
            get { return R.Tasks; }
        }

        public TasksView() {
            InitializeComponent();

            this._filteredLists = new List<LongListMultiSelector> {
                this.CurrentTasksList, this.PastTasksList
            };
        }

        protected override void SetCurrentFilter(int pivotIndex) {
            this.ViewModel.Filter = (TaskFilterOption)pivotIndex;
        }
    }
}