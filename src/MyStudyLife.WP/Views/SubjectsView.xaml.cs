﻿using System;
using System.Windows;
using System.Windows.Input;
using Cirrious.CrossCore;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views {
	public partial class SubjectsView {

		public new SubjectsViewModel ViewModel {
			get { return base.ViewModel as SubjectsViewModel; }
		}

		private readonly ApplicationBarIconButton _addAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.New);
		private readonly ApplicationBarMenuItem _changeScheduleMenuItem = new ApplicationBarMenuItem("change year / term");

		public SubjectsView() {
			InitializeComponent();
		}

		#region Overrides of MslAppView

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            this._addAppBarButton.Click += (s, e) => this.ViewModel.NewCommand.Execute(null);
            this._changeScheduleMenuItem.Click += (s, e) => ShowScheduleSelector();
	    }

	    protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.AddButton(this._addAppBarButton);
			appBar.AddMenuItem(this._changeScheduleMenuItem);
		}

		#endregion

        private void SubjectOnTap(object sender, GestureEventArgs e) {
            var acYear = (Subject)((FrameworkElement)sender).DataContext;

            if (acYear.CanBeEditedBy(this.ViewModel.User)) {
                this.ViewModel.EditCommand.Execute(acYear);
            }
            else {
                Mvx.Resolve<IDialogService>().ShowDismissible(
                    R.ReadOnly,
                    R.SubjectFromSchoolError
                );
            }
        }

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            ShowScheduleSelector();
        }

	    private void ShowScheduleSelector() {
	        AcademicScheduleSelector.GetInstanceAndShow(
                this,
                String.Format(R.AcademicScheduleTaskExamListHelp, R.Subjects.ToLower())
            );
	    }
	}
}