﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.WindowsPhone.Platform;
using Microsoft.Phone.Controls;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.WP;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views {
	public sealed partial class SignInView {
		public const int EnterPlatformKeyCode = 0x0a;

		public static SignInView Current { get; private set; }

		public new SignInViewModel ViewModel {
			get { return base.ViewModel as SignInViewModel; }
			set { base.ViewModel = value; }
		}

		public override bool AutoAppBarCreation {
			get { return false; }
		}

		public SignInView() {
			Current = this;

			InitializeComponent();

			this.MslEmailText.Loaded += SetEmailWatermark;
			this.MslEmailText.GotFocus += MslEmailTextOnFocus;
			this.MslEmailText.LostFocus += SetEmailWatermark;
			this.MslEmailText.KeyDown += MslEmailTextBoxOnKeyDown;
			this.MslEmailText.TextChanged += SetEmailWatermark;

			this.MslPasswordText.Loaded += SetPasswordWatermark;
			this.MslPasswordText.GotFocus += MslPasswordTextOnFocus;
			this.MslPasswordText.LostFocus += SetPasswordWatermark;
			this.MslPasswordText.KeyDown += MslPasswordBoxOnKeyDown;
			this.MslPasswordText.PasswordChanged += SetPasswordWatermark;

            this.OAuthBrowser.Opacity = 0.01;
			this.OAuthBrowser.Navigating += OAuthBrowserOnNavigating;
			this.OAuthBrowser.LoadCompleted += OAuthBrowserOnLoadCompleted;
			this.OAuthBrowser.NavigationFailed += OAuthBrowserOnNavigationFailed;
		}

		private void OAuthBrowserOnNavigating(object sender, NavigatingEventArgs e) {
		    this.OAuthBrowser.Opacity = 0.01;
		    this.OAuthBrowserProgressPanel.Visibility = Visibility.Visible;
			this.OAuthBrowserProgressBar.IsIndeterminate = true;
		}

        private void OAuthBrowserOnLoadCompleted(object sender, NavigationEventArgs e) {
            // Prevents flicker when we clear the screen using NavigateToString("")
            if (e.Uri.ToString() != String.Empty) {
                this.OAuthBrowser.Opacity = 1;
                this.OAuthBrowserProgressPanel.Visibility = Visibility.Collapsed;
                this.OAuthBrowserProgressBar.IsIndeterminate = false;
            }
        }

        private void OAuthBrowserOnNavigationFailed(object sender, NavigationFailedEventArgs e) {
            this.OAuthBrowser.Opacity = 1;
            this.OAuthBrowserProgressPanel.Visibility = Visibility.Collapsed;
			this.OAuthBrowserProgressBar.IsIndeterminate = false;
		}

		#region Input Box Event Handlers
		
		private void MslEmailTextBoxOnKeyDown(object sender, KeyEventArgs e) {
			if (e.Key.In(Key.Enter, Key.Tab) || e.PlatformKeyCode == EnterPlatformKeyCode) {
				this.MslPasswordText.Focus();
			}
		}

		private void SetEmailWatermark(object sender, RoutedEventArgs e) {
			this.MslEmailWatermark.Visibility = (this.MslEmailText.Text.Length == 0).ToVisibility();
		}

		private void MslEmailTextOnFocus(object sender, RoutedEventArgs e) {
			this.MslEmailWatermark.Visibility = Visibility.Collapsed;
		}

	    private void SetPasswordWatermark(object sender, RoutedEventArgs e) {
			this.MslPasswordWatermark.Visibility = (this.MslPasswordText.Password.Length == 0).ToVisibility();
		}

		private void MslPasswordTextOnFocus(object sender, RoutedEventArgs e) {
			this.MslPasswordWatermark.Visibility = Visibility.Collapsed;
		}

		private void MslPasswordBoxOnKeyDown(object sender, KeyEventArgs e) {
			if (e.Key.In(Key.Enter, Key.Tab) || e.PlatformKeyCode == EnterPlatformKeyCode) {
				this.ViewModel.MslSignInCommand.Execute(null);
			}
		}

		#endregion

		#region On Back Key Press

		protected override void OnBackKeyPress(CancelEventArgs e) {
			if (this.OAuthPopup.IsOpen) {
				this.OAuthPopup.IsOpen = false;

				e.Cancel = true;
			}
			else if (App.Current.RootFrame.CanGoBack) {
				try {
                    while (App.Current.RootFrame.BackStack.Any()) {
                        App.Current.RootFrame.RemoveBackEntry();
                    }
				}
// ReSharper disable once EmptyGeneralCatchClause
				catch { }
			}
		}

		#endregion
		
		public Task<OAuthAuthorizationResult> PerformOAuthAuthorizationAsync(AuthenticationProvider provider) {
		    var cfg = Mvx.Resolve<IMslConfig>();

			this.OAuthBrowser.NavigateToString(String.Empty);
			this.OAuthPopup.IsOpen = true;

		    var tcs = new TaskCompletionSource<OAuthAuthorizationResult>();

            Dispatcher.BeginInvoke(async () => {
                EventHandler onPopupClosed = null;
                EventHandler<NavigatingEventArgs> onNavigating = null;

			    try {
			        onPopupClosed = (s, e) => {
// ReSharper disable once AccessToModifiedClosure
                        this.OAuthPopup.Closed -= onPopupClosed;

			            tcs.TrySetResult(
			                new OAuthAuthorizationResult(OAuthAuthorizationStatus.UserCancel)
			            );
			        };

			        onNavigating = (s, e) => {
			            if (e.Uri.ToString().StartsWith(cfg.OAuthEndUri)) {

			                e.Cancel = true;

			                // ReSharper disable once AccessToModifiedClosure
			                this.OAuthBrowser.Navigating -= onNavigating;

			                tcs.TrySetResult(OAuthAuthorizationResult.FromUri(e.Uri));

			                this.OAuthPopup.Closed -= onPopupClosed;
			                this.OAuthPopup.IsOpen = false;
			            }
			        };

			        this.OAuthPopup.Closed += onPopupClosed;
			        this.OAuthBrowser.Navigating += onNavigating;

                    await this.OAuthBrowser.ClearInternetCacheAsync();
			        this.OAuthBrowser.Navigate(new Uri(cfg.GetOAuthStartUri(provider)));
			    }
			    catch (Exception ex) {
                    tcs.TrySetException(ex);
                    this.OAuthPopup.Closed -= onPopupClosed;
                    this.OAuthBrowser.Navigating -= onNavigating;
                    this.OAuthPopup.IsOpen = false;
			    }
			});

		    return tcs.Task;
		}
	}
}