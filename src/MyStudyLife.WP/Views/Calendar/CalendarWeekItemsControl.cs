﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MyStudyLife.Globalization;
using MyStudyLife.Scheduling;
using MyStudyLife.WP.Converters;

namespace MyStudyLife.WP.Views.Calendar {
    public class CalendarWeekItemsControl : ItemsControl {
	    public static readonly DependencyProperty DayWidthProperty =
		    DependencyProperty.Register("DayWidth", typeof (double), typeof (CalendarWeekItemsControl), new PropertyMetadata(default(double), DayWidthOnChanged));

	    public double DayWidth {
		    get { return (double) GetValue(DayWidthProperty); }
		    set { SetValue(DayWidthProperty, value); }
	    }

		private static void DayWidthOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			var ctrl = (CalendarWeekItemsControl) d;

			foreach (var ae in ctrl.Items.Cast<AgendaEntry>()) {
				ctrl.SetEntryWidth(ae);
			}
		}

		protected override void PrepareContainerForItemOverride(DependencyObject element, object item) {
			Binding topBinding = new Binding {
				Path = new PropertyPath("StartTime.TimeOfDay"),
				Converter = new TimeSpanToLengthConverter(),
				ConverterParameter = CalendarWeekView.HourHeight
			};

			Binding heightBinding = new Binding {
				Path = new PropertyPath("Duration"),
				Converter = new TimeSpanToLengthConverter(),
				ConverterParameter = CalendarWeekView.HourHeight
			};

			FrameworkElement contentControl = (FrameworkElement)element;
			contentControl.SetBinding(Canvas.TopProperty, topBinding);
			contentControl.SetBinding(HeightProperty, heightBinding);

			var agendaEntry = item as AgendaEntry;

			if (agendaEntry != null) {
				if (agendaEntry.IsPast)
					contentControl.SetValue(OpacityProperty, 0.5);
			}

			SetEntryWidth(agendaEntry, contentControl);

			base.PrepareContainerForItemOverride(element, item);
		}

		private void SetEntryWidth(AgendaEntry agendaEntry, FrameworkElement element = null) {
			var itemContainerGenerator = this.ItemContainerGenerator;

			if (element == null && itemContainerGenerator != null) {
				element = ((FrameworkElement) itemContainerGenerator.ContainerFromItem(agendaEntry));
			}

			if (element != null && agendaEntry != null) {
				var entryWidth = DayWidth / (agendaEntry.ConflictCount + 1);

				element.Width = Math.Round(entryWidth, 1) - 1;

                Canvas.SetLeft(element, (L10n.GetDayIndex((int)agendaEntry.StartTime.DayOfWeek) * DayWidth) + (agendaEntry.ConflictIndex > 0 ? (agendaEntry.ConflictIndex * entryWidth) : 0));
			}
		}
    }
}
