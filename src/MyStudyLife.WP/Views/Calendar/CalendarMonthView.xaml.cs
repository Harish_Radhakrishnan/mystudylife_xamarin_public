﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using Cirrious.CrossCore.WeakSubscription;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Shell;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.Calendar {
    public partial class CalendarMonthView {

        private static WeakReference<CalendarMonthView> _instanceRef;

        public static CalendarMonthView Instance {
            get {
                CalendarMonthView instance;

                if (_instanceRef != null && _instanceRef.TryGetTarget(out instance)) {
                    return instance;
                }

                return null;
            }
        }

        [UsedImplicitly] private MvxPropertyChangedListener _listener;

        private new CalendarMonthViewModel ViewModel {
			get { return (CalendarMonthViewModel)base.ViewModel; }
        }

		public override bool ShowSystemTray {
			get { return false; }
		}

        private readonly ApplicationBarIconButton _currentPeriodAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Today);
        private readonly ApplicationBarIconButton _periodAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Calendar, "week");

        public Style DayGridStyle {
            get { return (Style) this.Resources["DayGridStyle"]; }
        }
        public Style DayGridSelectedStyle {
            get { return (Style) this.Resources["DayGridSelectedStyle"]; }
        }
        public Style DayGridHolidayStyle {
            get { return (Style)this.Resources["DayGridHolidayStyle"]; }
        }
        public Style DayItemsControlStyle {
            get { return (Style) this.Resources["DayItemsControlStyle"]; }
        }
        public Style DayTextStyle {
            get { return (Style) this.Resources["DayTextStyle"]; }
        }

        public Style DayTextSelectedStyle {
            get { return (Style) this.Resources["DayTextSelectedStyle"]; }
        }
        public Style DayTextCurrentStyle {
            get { return (Style) this.Resources["DayTextCurrentStyle"]; }
        }
        public Style DayTextOtherMonthStyle {
            get { return (Style) this.Resources["DayTextOtherMonthStyle"]; }
        }

		public CalendarMonthView() {
		    _instanceRef = new WeakReference<CalendarMonthView>(this);

            InitializeComponent();
        }

        protected override void OnViewModelSet() {
            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.IsSelectedPeriodCurrent, SetCurrentPeriodAppBarButtonEnabled); ;
        }

        #region App Bar

		protected override void OnAppBarCreating(IApplicationBar appBar) {
			appBar.BackgroundColor = Colors.Black;
			appBar.Mode = ApplicationBarMode.Minimized;
            appBar.Opacity = 0.5;

            _currentPeriodAppBarButton.Click += (s, e) => this.ViewModel.CurrentPeriodCommand.Execute(null);
            _periodAppBarButton.Click += (s, e) => this.ViewModel.SwitchViewCommand.Execute(null);
	    }

	    protected override void OnAppBarCreated(IApplicationBar appBar) {
            appBar.Buttons.Add(_currentPeriodAppBarButton);
            appBar.Buttons.Add(_periodAppBarButton);
        }

        private void SetCurrentPeriodAppBarButtonEnabled() {
            _currentPeriodAppBarButton.IsEnabled = this.ViewModel != null && !this.ViewModel.IsSelectedPeriodCurrent;
        }

        #endregion

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            var calendarWeekView = NavigationService.BackStack.LastOrDefault(entry => entry.Source.ToString().Contains(typeof(CalendarWeekView).Name + ".xaml"));

            if (calendarWeekView != null) {
                NavigationService.RemoveBackEntry();
            }
        }
    }

    public class CalendarMonthLayout : Grid {
        public CalendarMonthLayout() {
            int i = 0;

            for (int w = 0; w < 6; w++) {
                this.RowDefinitions.Add(new RowDefinition());

                for (int d = 0; d < 7; d++) {
                    if (w == 0) {
                        this.ColumnDefinitions.Add(new ColumnDefinition());
                    }

                    var dayControl = new CalendarMonthDayControl();

                    Grid.SetRow(dayControl, w);
                    Grid.SetColumn(dayControl, d);

                    dayControl.SetBinding(CalendarMonthDayControl.DayProperty, new Binding {
                        Path = new PropertyPath(String.Concat("Days[", i, "]"))
                    });

                    dayControl.Tap += DayControlOnTap;

                    this.Children.Add(dayControl);

                    i++;
                }
            }
        }

        private void DayControlOnTap(object sender, GestureEventArgs e) {
            ((CalendarMonthModel)this.DataContext).SelectedDay = ((CalendarMonthDayControl)sender).Day;
        }
    }

    public class CalendarMonthDayControl : Grid {
        // We can use DataContextChanged if/when we upgrade to WP8.1
        public static readonly DependencyProperty DayProperty = DependencyProperty.Register(
            "Day", typeof(CalendarMonthModel.DayAndEntries), typeof(CalendarMonthDayControl), new PropertyMetadata(null, DataContextChanged));

        public CalendarMonthModel.DayAndEntries Day {
            get { return (CalendarMonthModel.DayAndEntries)GetValue(DayProperty); }
            set { SetValue(DayProperty, value); }
        }

        private TextBlock _dayText;
        private CalendarMonthDayItemsControl _itemsControl;

        public CalendarMonthDayControl() {
            this.Name = "DayControl";
            this.Style = CalendarMonthView.Instance.DayGridStyle;

            var dayText = _dayText = new TextBlock {
                Style = CalendarMonthView.Instance.DayTextStyle
            };
            var itemsControl = _itemsControl = new CalendarMonthDayItemsControl {
                Style = CalendarMonthView.Instance.DayItemsControlStyle
            };

            this.RowDefinitions.Add(new RowDefinition {
                Height = GridLength.Auto
            });
            this.RowDefinitions.Add(new RowDefinition {
                Height = new GridLength(1, GridUnitType.Star)
            });

            dayText.SetBinding(TextBlock.TextProperty, new Binding {
                Source = this,
                Path = new PropertyPath("Day.Day.Day")
            });
            itemsControl.SetBinding(ItemsControl.ItemsSourceProperty, new Binding {
                Source = this,
                Path = new PropertyPath("Day.Entries")
            });

            Grid.SetRow(dayText, 0);
            Grid.SetRow(itemsControl, 1);

            this.Children.Add(dayText);
            this.Children.Add(itemsControl);

            SetState();
        }

        private IDisposable _subscription;

        private static void DataContextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var ctrl = (CalendarMonthDayControl) d;
            var dataContext = ctrl.Day;

            if (ctrl._subscription != null) {
                ctrl._subscription.Dispose();
                ctrl._subscription = null;
            }

            if (dataContext != null) {
                ctrl._subscription = dataContext.WeakSubscribe(ctrl.DataContextOnPropertyChanged);

                ctrl.SetState();
            }
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In("IsSelected", "IsHoliday", "IsOtherMonth")) {
                SetState();
            }
        }

        private void SetState() {
            var day = this.Day;

            if (day == null) {
                return;
            }

            if (day.IsCurrent) {
                this._dayText.Style = CalendarMonthView.Instance.DayTextCurrentStyle;
            }
            else if (day.IsOtherMonth) {
                this._dayText.Style = CalendarMonthView.Instance.DayTextOtherMonthStyle;
            }
            else {
                this._dayText.Style = CalendarMonthView.Instance.DayTextStyle;
            }

            if (day.IsSelected) {
                this.Style = CalendarMonthView.Instance.DayGridSelectedStyle;
                this._dayText.Style = CalendarMonthView.Instance.DayTextSelectedStyle;
            }
            else if (day.IsHoliday) {
                this.Style = CalendarMonthView.Instance.DayGridHolidayStyle;
            }
            else {
                this.Style = CalendarMonthView.Instance.DayGridStyle;
            }

            var holidayGlyph = this.Children.OfType<TextBlock>().FirstOrDefault(x => (string) x.Tag == "HOLIDAY_GLYPH");

            if (day.IsHoliday) {
                if (holidayGlyph == null) {
                    holidayGlyph = new TextBlock {
                        Text = (string) App.Current.Resources["HolidayGlyph"],
                        FontSize = 24,
                        Style = (Style) App.Current.Resources["IconFontStyle"],
                        Tag = "HOLIDAY_GLYPH",
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };

                    Grid.SetRow(holidayGlyph, 1);

                    this.Children.Add(holidayGlyph);
                }

                if (day.IsSelected) {
                    holidayGlyph.Foreground = new SolidColorBrush(Colors.White);
                }
                else {
                    holidayGlyph.Foreground = (SolidColorBrush) App.Current.Resources["BrushSubtleText"];
                }
            }
            else if (holidayGlyph != null) {
                this.Children.Remove(holidayGlyph);
            }
        }
    }
}