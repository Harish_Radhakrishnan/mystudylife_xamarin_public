﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Cirrious.CrossCore.WeakSubscription;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.Globalization;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels.Calendar;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Converters;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MyStudyLife.WP.Views.Calendar {
    public partial class CalendarWeekView {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

	    public const double WeekTimeColWidth = 24.0;
	    public const double HourHeight = 72d;

        private static WeakReference<CalendarWeekView> _instanceRef;

        public static CalendarWeekView Instance {
            get {
                CalendarWeekView instance;

                if (_instanceRef != null && _instanceRef.TryGetTarget(out instance)) {
                    return instance;
                }

                return null;
            }
        }

        public double CurrentScrollOffset;

	    private GridLength? _hourRowHeight;
	    public GridLength HourRowHeight {
		    get {
			    if (!_hourRowHeight.HasValue) {
				    _hourRowHeight = new GridLength(HourHeight);
			    }

			    return _hourRowHeight.Value;
		    }
	    }

        private new CalendarWeekViewModel ViewModel {
			get { return (CalendarWeekViewModel)base.ViewModel; }
        }

		public override bool ShowSystemTray {
			get { return false; }
		}

        private readonly DispatcherTimer _timer;

        private readonly ApplicationBarIconButton _currentPeriodAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Today);
        private readonly ApplicationBarIconButton _periodAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Calendar, "month");

        public CalendarWeekView() {
            _instanceRef = new WeakReference<CalendarWeekView>(this);

            InitializeComponent();

            this.WeekPivot.SelectionChanged += WeekPivotOnSelectionChanged;

            SetWeekPivotMargin();

            var n = DateTime.Now;

            _timer = new DispatcherTimer {
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                Interval = TimeSpan.FromSeconds(60 - n.Second)
            };

            _timer.Tick += TimerOnTick;

            _timer.Start();
        }

        protected override void OnViewModelSet() {
            base.OnViewModelSet();

            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.IsSelectedPeriodCurrent, SetCurrentPeriodAppBarButtonEnabled);
        }

        private void WeekPivotOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var selectedItem = this.WeekPivot.ItemContainerGenerator.ContainerFromItem(e.AddedItems[0]);

            if (selectedItem != null) {
                selectedItem.FindVisualChildren<ScrollViewer>().First().ScrollToVerticalOffset(CurrentScrollOffset);
                selectedItem.FindVisualChildren<CalendarWeekLayout>().First().SetTimelineOffset();
            }

            TryHideHolidayNamePopup();
        }

        protected override void OnOrientationChanged(OrientationChangedEventArgs e) {
            SetWeekPivotMargin();
            TryHideHolidayNamePopup();

            base.OnOrientationChanged(e);
        }

        private void SetWeekPivotMargin() {
            if (this.Orientation.HasFlag(PageOrientation.Portrait)) {
                this.ApplicationBar.IsVisible = true;
                this.ViewRoot.Margin = new Thickness(0,0,0,30);
            }
            else {
                this.ApplicationBar.IsVisible = false;
                this.ViewRoot.Margin = new Thickness(0);
            }
        }

        #region App Bar
        
		protected override void OnAppBarCreating(IApplicationBar appBar) {
			appBar.BackgroundColor = Colors.Black;
			appBar.Mode = ApplicationBarMode.Minimized;
            appBar.Opacity = 0.5;

            _currentPeriodAppBarButton.Click += (s, e) => this.ViewModel.CurrentPeriodCommand.Execute(null);
            _periodAppBarButton.Click += (s, e) => this.ViewModel.SwitchViewCommand.Execute(null);

            SetCurrentPeriodAppBarButtonEnabled();
	    }

	    protected override void OnAppBarCreated(IApplicationBar appBar) {
            appBar.Buttons.Add(_currentPeriodAppBarButton);
            appBar.Buttons.Add(_periodAppBarButton);
        }

        private void SetCurrentPeriodAppBarButtonEnabled() {
            _currentPeriodAppBarButton.IsEnabled = this.ViewModel != null && !this.ViewModel.IsSelectedPeriodCurrent;
        }

        #endregion

        private void TimerOnTick(object sender, object o) {
            if (_timer.Interval.TotalMinutes < 1.0) {
                _timer.Interval = TimeSpan.FromMinutes(1);
            }
            
			// ReSharper disable once CompareOfFloatsByEqualityOperator
		    if (this.WeekPivot != null && this.WeekPivot.SelectedItem != null) {
		        var selectedItem = this.WeekPivot.ItemContainerGenerator.ContainerFromItem(this.WeekPivot.SelectedItem);

		        if (selectedItem != null) {
		            selectedItem.FindVisualChildren<CalendarWeekLayout>().First().SetTimelineOffset();
		        }
		    }
		}

        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            var calendarMonthView = NavigationService.BackStack.LastOrDefault(entry => entry.Source.ToString().Contains(typeof(CalendarMonthView).Name + ".xaml"));

            if (calendarMonthView != null) {
                NavigationService.RemoveBackEntry();
            }
        }

        // For some reason the command doesn't bind on the button.
        // Might be because we're creating the items control in C#.
        private void AgendaEntryOnTap(object sender, GestureEventArgs e) {
            this.ViewModel.ViewEntryCommand.Execute(((FrameworkElement)sender).DataContext);
        }

        #region Holiday Name Popup

        private readonly WeakReference<Popup> _holidayNamePopup = new WeakReference<Popup>(null);
        private Timer _holidayNamePopupTimer;

        private void HolidayIndicatorOnTap(object sender, GestureEventArgs e) {
            e.Handled = true;

            ShowHolidayNamePopup((FrameworkElement) sender);
        }

        private void ShowHolidayNamePopup(FrameworkElement holidayIndicator) {
            string text = ((CalendarWeekModel.DayInfo) holidayIndicator.DataContext).HolidayName;

            Popup popup;
            Border border;

            if (!_holidayNamePopup.TryGetTarget(out popup)) {
                popup = new Popup {
                    Child = border = new Border {
                        MaxWidth = this.ActualWidth - 24,
                        Padding = new Thickness(6),
                        Background = new SolidColorBrush(Colors.White),
                        BorderBrush = App.Current.MslResources.BrushForeground,
                        BorderThickness = new Thickness(2),
                        Child = new TextBlock {
                            Text = text,
                            TextWrapping = TextWrapping.Wrap,
                            Style = App.Current.MslResources.Get<Style>("MyStudyLifeMediumTextStyle")
                        }
                    },
                    Tag = holidayIndicator
                };

                border.SizeChanged += (s, e) => PositionHolidayNamePopup(popup);

                // Required to respect the orientation
                this.LayoutRoot.Children.Add(popup);

                _holidayNamePopup.SetTarget(popup);
            }
            else {
                border = (Border) popup.Child;

                popup.Tag = holidayIndicator;
                border.MaxWidth = this.ActualWidth - 24;

                ((TextBlock) border.Child).Text = text;

                PositionHolidayNamePopup(popup);
            }

            popup.IsOpen = true;

            if (_holidayNamePopupTimer == null) {
                _holidayNamePopupTimer = new Timer(ignored => Dispatcher.BeginInvoke(TryHideHolidayNamePopup));
            }

            _holidayNamePopupTimer.Change(TimeSpan.FromSeconds(3), TimeSpan.FromMilliseconds(-1));
        }

        private void PositionHolidayNamePopup(Popup popup) {
            var holidayIndicator = (FrameworkElement) popup.Tag;

            var absolutePos = holidayIndicator.TransformToVisual(this).Transform(new Point());

            var popupWidth = ((FrameworkElement)popup.Child).ActualWidth;

            popup.HorizontalOffset = Math.Max(
                Math.Min(
                    (absolutePos.X + (holidayIndicator.ActualWidth / 2)) - (popupWidth / 2),
                    this.ActualWidth - popupWidth - 12
                ),
                12
            );
            popup.VerticalOffset = absolutePos.Y + holidayIndicator.ActualHeight + 12;
        }

        private void TryHideHolidayNamePopup() {
            Popup popup;

            if (_holidayNamePopup.TryGetTarget(out popup)) {
                popup.IsOpen = false;
            }

            if (_holidayNamePopupTimer != null) {
                _holidayNamePopupTimer.Dispose();
                _holidayNamePopupTimer = null;
            }
        }

        protected override void OnTap(GestureEventArgs e) {
            base.OnTap(e);

            if (!e.Handled) {
                TryHideHolidayNamePopup();
            }
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e) {
            TryHideHolidayNamePopup();
            
            base.OnNavigatingFrom(e);
        }

        #endregion
    }

    public class CalendarWeekLayout : Grid {
        public const double HourColumnWidth = 36d;
        public const double HourHeight = 72d;

        #region Resources

        private SolidColorBrush _brushSubtle;
        private SolidColorBrush _brushSubtleSuper;
        private SolidColorBrush _brushSubtleText;

        private SolidColorBrush BrushSubtle {
            get { return _brushSubtle ?? (_brushSubtle = (SolidColorBrush)App.Current.Resources["BrushSubtle"]); }
        }
        private SolidColorBrush BrushSubtleSuper {
            get { return _brushSubtleSuper ?? (_brushSubtleSuper = (SolidColorBrush)App.Current.Resources["BrushSubtleSuper"]); }
        }
        private SolidColorBrush BrushSubtleText {
            get { return _brushSubtleText ?? (_brushSubtleText = (SolidColorBrush)App.Current.Resources["BrushSubtleText"]); }
        }

        #endregion
        
        private bool _userScrolled;
        private IDisposable _subscription;

        private readonly Line _timeline;
        private readonly Border _currentDayHighlight;

        public new CalendarWeekModel DataContext {
            get { return (CalendarWeekModel)base.DataContext; }
        }

        public CalendarWeekLayout() {
            const double dayHeight = HourHeight * 24;

            var gridWidth = App.Current.RootFrame.ActualWidth - HourColumnWidth;

            var grid = this;

            grid.ColumnDefinitions.Add(new ColumnDefinition {
                Width = new GridLength(HourColumnWidth)
            });

            var fill = new Rectangle {
                Fill = new SolidColorBrush(Colors.White)
            };

            Grid.SetColumn(fill, 1);
            Grid.SetColumnSpan(fill, 7);
            Grid.SetRowSpan(fill, 24);

            grid.Children.Add(fill);

            for (int i = 0; i < 7; i++) {
                grid.ColumnDefinitions.Add(new ColumnDefinition());

                var l = new Line {
                    X1 = 0,
                    X2 = 0,
                    Y1 = 0,
                    Y2 = dayHeight,
                    Stroke = BrushSubtle,
                    StrokeThickness = 1.0,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Tag = "VERTICAL"
                };

                Grid.SetColumn(l, i + 1);
                Grid.SetRow(l, 0);
                Grid.SetRowSpan(l, 24);

                grid.Children.Add(l);
            }

            for (int i = 0; i <= 24; i++) {
                var l = new Line {
                    X1 = 0,
                    X2 = gridWidth,
                    Y1 = 0,
                    Y2 = 0,
                    Stroke = BrushSubtle,
                    StrokeThickness = 1.0,
                    VerticalAlignment = i == 24 ? VerticalAlignment.Bottom : VerticalAlignment.Top,
                    Tag = "HORIZONTAL"
                };

                Grid.SetColumn(l, 1);
                Grid.SetColumnSpan(l, 7);
                Grid.SetRow(l, i == 24 ? 23 : i);

                grid.Children.Add(l);

                if (i < 24) {
                    grid.RowDefinitions.Add(new RowDefinition());

                    var timeLabel = new TextBlock {
                        Text = DateTimeFormat.Hours[i],
                        Margin = new Thickness(0, 0, 0, 1),
                        Padding = new Thickness(0, 4, 8, 0),
                        TextAlignment = TextAlignment.Right,
                        FontSize = (double)App.Current.Resources["MyStudyLifeSmallFontSize"],
                        Foreground = BrushSubtleText,
                        HorizontalAlignment = HorizontalAlignment.Stretch,
                        VerticalAlignment = VerticalAlignment.Top
                    };

                    Grid.SetRow(timeLabel, i);

                    grid.Children.Add(timeLabel);

                    var lHalf = new Line {
                        X1 = 0,
                        X2 = gridWidth,
                        Y1 = 0,
                        Y2 = 0,
                        Stroke = BrushSubtleSuper,
                        StrokeThickness = 1.0,
                        VerticalAlignment = VerticalAlignment.Center,
                        Tag = "HORIZONTAL"
                    };

                    Grid.SetColumn(lHalf, 1);
                    Grid.SetColumnSpan(lHalf, 7);
                    Grid.SetRow(lHalf, i);

                    grid.Children.Add(lHalf);
                }
            }

            _currentDayHighlight = new Border {
                Background = new SolidColorBrush((Color)App.Current.Resources["ColorAccent"]) {
                    Opacity = 0.1
                }
            };

            _currentDayHighlight.SetBinding(
                VisibilityProperty,
                new Binding {
                    Path = new PropertyPath("IsCurrentWeek"),
                    Converter = new VisibilityConverter()
                }
            );
            _currentDayHighlight.SetBinding(
                Grid.ColumnProperty,
                new Binding {
                    Path = new PropertyPath("CurrentDayIndex"),
                    Converter = new CurrentDayIndexConverter()
                }
            );

            Grid.SetRowSpan(_currentDayHighlight, 24);

            grid.Children.Add(_currentDayHighlight);

            var itemsControl = new CalendarWeekItemsControl {
                DayWidth = gridWidth / 7f
            };

            itemsControl.SetBinding(
                ItemsControl.ItemsSourceProperty,
                new Binding("Entries")
            );

            Grid.SetColumn(itemsControl, 1);
            Grid.SetColumnSpan(itemsControl, 7);

            grid.Children.Add(itemsControl);
            
            _timeline = new Line {
                X1 = 0,
                X2 = gridWidth,
                Y1 = 0,
                Y2 = 0,
                Stroke = (SolidColorBrush) App.Current.Resources["BrushAccent"],
                StrokeThickness = 2.0,
                Tag = "HORIZONTAL"
            };
            
            _timeline.SetBinding(
                VisibilityProperty,
                new Binding("IsCurrentWeek") {
                    Converter = new VisibilityConverter()
                }
            );
            
            this.SetTimelineOffset();

            Grid.SetColumn(_timeline, 1);
            Grid.SetColumnSpan(_timeline, 7);
            Grid.SetRowSpan(_timeline, 24);

            grid.Children.Add(_timeline);

            this.Loaded += OnLoaded;
            this.SizeChanged += OnSizeChanged;
        }

        public void SetTimelineOffset() {
            if (_timeline != null) {
                double vOffset = DateTime.Now.TimeOfDay.TotalHours*HourHeight;

                _timeline.Y1 = vOffset;
                _timeline.Y2 = vOffset;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            var scrollViewer = (ScrollViewer) this.Parent;

            scrollViewer.ScrollToVerticalOffset(CalendarWeekView.Instance.CurrentScrollOffset);

            // No scroll event - http://stackoverflow.com/a/5264782/491468
            scrollViewer.MouseMove += ScrollViewerOnScroll;

            if (this.DataContext != null) {
                if (this.DataContext.InitialTime.HasValue) {
                    DoFirstScroll();
                }
                else {
                    _subscription = this.DataContext.WeakSubscribe(DataContextOnPropertyChanged);
                }
            }
        }

        private void OnSizeChanged(object sender, SizeChangedEventArgs e) {
// ReSharper disable once CompareOfFloatsByEqualityOperator
            if (e.NewSize.Width == e.PreviousSize.Width) {
                return;
            }

            var gridWidth = (e.NewSize.Width - HourColumnWidth);

            this.Children.OfType<Line>().Where(x => (string)x.Tag == "HORIZONTAL").Apply(x => x.X2 = gridWidth);
            this.Children.OfType<Line>().Where(x => (string)x.Tag == "VERTICAL").Apply(x => x.Y2 = e.NewSize.Height);

            this.Children.OfType<CalendarWeekItemsControl>().First().DayWidth = gridWidth/7f;
        }

        private void ScrollViewerOnScroll(object sender, MouseEventArgs e) {
            _userScrolled = true;

            CalendarWeekView.Instance.CurrentScrollOffset = ((ScrollViewer)sender).VerticalOffset;
        }

        private void DataContextOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "InitialTime") {
                if (_subscription != null) {
                    _subscription.Dispose();
                    _subscription = null;
                }

                DoFirstScroll();
            }
        }

        private void DoFirstScroll() {
            try {
                var dataContext = this.DataContext; // Prevent 2 casts!

                if (dataContext == null) {
                    return;
                }

                var initialTime = dataContext.InitialTime;

                if (!this._userScrolled && initialTime.HasValue && CalendarWeekView.Instance.CurrentScrollOffset <= 0) {
                    var scrollOffset = initialTime.Value.TotalHours * HourHeight;

                    ((ScrollViewer)this.Parent).ScrollToVerticalOffset(scrollOffset);

                    CalendarWeekView.Instance.CurrentScrollOffset = scrollOffset;
                }
            }
            // ReSharper disable once EmptyGeneralCatchClause, RedundantCatchClause
            catch {
#if DEBUG
                throw;
#endif
            }
        }

        class CurrentDayIndexConverter : IValueConverter {
            public object Convert(object value, Type targetType, object parameter, CultureInfo language) {
                return ((int)value) + 1;
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo language) {
                throw new NotSupportedException();
            }
        }
    }
}