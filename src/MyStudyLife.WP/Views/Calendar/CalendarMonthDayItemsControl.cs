﻿using System.Windows;
using System.Windows.Controls;
using MyStudyLife.Scheduling;

namespace MyStudyLife.WP.Views.Calendar {
    public class CalendarMonthDayItemsControl : ItemsControl {
        protected override void PrepareContainerForItemOverride(DependencyObject element, object item) {
            var contentControl = (FrameworkElement)element;

            var agendaEntry = item as AgendaEntry;

            if (agendaEntry != null && agendaEntry.IsPast) {
                contentControl.SetValue(OpacityProperty, 0.5);
            }

            base.PrepareContainerForItemOverride(element, item);
        }
    }
}
