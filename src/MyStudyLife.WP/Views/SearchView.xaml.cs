﻿using System.Windows.Input;
using System.Windows.Controls;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.WP.Views {
    public partial class SearchView {
        public new SearchViewModel ViewModel {
            get { return base.ViewModel as SearchViewModel; }
        }

	    #region Overrides of MslAppView

	    /// <summary>
	    ///     True if the view automatically handles
	    ///     the creation/destruction of the app bar.
	    /// </summary>
	    public override bool AutoAppBarCreation {
		    get { return false; }
	    }

	    #endregion

	    public SearchView() {
            InitializeComponent();
        }

        private async void QueryTextOnKeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter || e.PlatformKeyCode == 0x0a) {
                this.Focus();

                await this.ViewModel.ProcessQueryText(((TextBox) sender).Text);
            }
        }
    }
}