﻿using System.Linq;
using System.Windows.Media;
using System.Windows.Navigation;
using Cirrious.CrossCore;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Views.Wizards;
using MyStudyLife.UI.ViewModels.Components;
using Cirrious.MvvmCross.ViewModels;
using System;

namespace MyStudyLife.WP.Views {
    public partial class DashboardView {
        private IDisposable _navVmSubscription;

        public new DashboardViewModel ViewModel {
            get { return base.ViewModel as DashboardViewModel; }
        }

		private readonly ApplicationBarIconButton _syncAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Sync);
		private readonly ApplicationBarMenuItem _accountAppBarMenuItem = new ApplicationBarMenuItem("account");
        private readonly ApplicationBarMenuItem _aboutAppBarMenuItem = new ApplicationBarMenuItem("about");

        public DashboardView() {
            InitializeComponent();

	        this.Panorama.DefaultItem = this.Panorama.Items[1];
            
            var navVm = (NavigationDrawerViewModel) Mvx.Resolve<IMvxViewModelLoader>().LoadViewModel(MvxViewModelRequest<NavigationDrawerViewModel>.GetDefaultRequest(), null);

            this.HeaderGrid.DataContext = navVm;

            _navVmSubscription = new MvxPropertyChangedListener(navVm)
                .Listen(nameof(NavigationDrawerViewModel.SchoolLogoLoaded), OnSystemTraySet);
        }
        
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            base.OnNavigatedTo(e);

            var welcomeWizardView = NavigationService.BackStack.LastOrDefault(entry => entry.Source.ToString().Contains(typeof(WelcomeWizardView).Name + ".xaml"));

            if (welcomeWizardView != null) {
                Mvx.Resolve<IDialogService>().RequestConfirmation(
                    "Setup your schedule",
                    "Would you like to head over to the schedule screen to start adding your classes?",
                    Mvx.Resolve<INavigationService>().NavigateTo<ScheduleViewModel>
                );
            }

            while (NavigationService.CanGoBack) {
                NavigationService.RemoveBackEntry();
            }
        }

        #region App Bar

	    protected override void OnAppBarCreating(IApplicationBar appBar) {
			appBar.BackgroundColor = Colors.Black;
			appBar.Mode = ApplicationBarMode.Minimized;
            appBar.Opacity = 0.5;

            _syncAppBarButton.Click += (s, e) => this.ViewModel.TriggerSyncCommand.Execute(null);
            _accountAppBarMenuItem.Click += (s, e) => this.ViewModel.NavigateCommand.Execute("Settings.Account");
            _aboutAppBarMenuItem.Click += (s, e) => this.ViewModel.NavigateCommand.Execute("Settings.About");
	    }

	    protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.Buttons.Add(_syncAppBarButton);
			appBar.MenuItems.Add(_accountAppBarMenuItem);
			appBar.MenuItems.Add(_aboutAppBarMenuItem);
        }

	    #endregion

	    #region System Tray

	    protected override void OnSystemTraySet() {
            Color foreground = Colors.White;

            var vm = this.HeaderGrid?.DataContext as NavigationDrawerViewModel;

            if (vm != null && vm.SchoolLogoLoaded) {
                foreground = (Color)App.Current.Resources["ColorForegroundMid"];
            }

            SystemTray.SetForegroundColor(this, foreground);
	    }

	    #endregion
    }
}