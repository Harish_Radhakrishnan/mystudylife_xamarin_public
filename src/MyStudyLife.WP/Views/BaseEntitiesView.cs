﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views {
    // USE VIRTUAL / instead of abstract so the designer does not have a fit.

    public class BaseEntitiesView<TEntity, TViewModel> : MslAppView
        where TEntity : SubjectDependentEntity, new()
        where TViewModel : BaseEntitiesViewModel<TEntity> {

		protected new TViewModel ViewModel {
			get { return (TViewModel) base.ViewModel; }
        }

        protected virtual string PluralTypeName { get { return "items"; } }

        private readonly ApplicationBarIconButton _addAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.New);
        private readonly ApplicationBarIconButton _selectAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Select);
        private readonly ApplicationBarIconButton _deleteAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Delete);

		private readonly ApplicationBarMenuItem _changeScheduleMenuItem = new ApplicationBarMenuItem("change year / term");

        public virtual IEnumerable<LongListMultiSelector> FilteredLists {
            get {
                return new List<LongListMultiSelector>();
            }
        }

	    public virtual bool IsSelectionEnabled {
		    get {
			    return true;
		    }
	    }

        protected BaseEntitiesView() {
            this.BindAppBarEvents();

            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs e) {
            ((Pivot) this.FindName("LockablePivot")).SelectionChanged += OnSelectionChanged;

            foreach (var list in FilteredLists) {
                list.IsSelectionEnabledChanged += OnIsSelectionEnabledChanged;
            }
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (this.ViewModel == null) {
                return;
            }

            var selectedIndex = ((Pivot) sender).SelectedIndex;

            if (selectedIndex < 0) {
                return;
            }

            this.SetCurrentFilter(selectedIndex);
        }

        protected virtual void SetCurrentFilter(int pivotIndex) {
            // Exception should never be thrown, this is an abstract method really
            // but because WP designer doesn't like abstract classes, it's virtual
            throw new NotImplementedException();
        }

        #region App Bar

        protected override void OnAppBarCreated(IApplicationBar appBar) {
            SetAppBarButtons();
        }

        private void OnIsSelectionEnabledChanged(object sender, DependencyPropertyChangedEventArgs e) {
            SetAppBarButtons();

            ((Pivot) this.FindName("LockablePivot")).IsLocked = (bool) e.NewValue;
        }

		/// <remarks>
		///		The <c>Contains()</c> checks below look
		///		unnecessary but they're not.
		/// </remarks>		
        private void SetAppBarButtons() {
			if (this.FilteredLists.Any(x => x.IsSelectionEnabled)) {
                this.ApplicationBar.Buttons.Remove(_addAppBarButton);

				if (this.IsSelectionEnabled) {
					this.ApplicationBar.Buttons.Remove(_selectAppBarButton);
				}

				if (!this.ApplicationBar.Buttons.Contains(_deleteAppBarButton)) {
		            this.ApplicationBar.Buttons.Add(_deleteAppBarButton);
	            }

	            this.ApplicationBar.MenuItems.Remove(_changeScheduleMenuItem);
            }
            else {
				if (!this.ApplicationBar.Buttons.Contains(_addAppBarButton)) {
		            this.ApplicationBar.Buttons.Add(_addAppBarButton);
	            }

				if (this.IsSelectionEnabled && !this.ApplicationBar.Buttons.Contains(_selectAppBarButton)) {
		            this.ApplicationBar.Buttons.Add(_selectAppBarButton);
	            }

	            this.ApplicationBar.Buttons.Remove(_deleteAppBarButton);


				if (!this.ApplicationBar.MenuItems.Contains(_changeScheduleMenuItem)) {
		            this.ApplicationBar.MenuItems.Add(_changeScheduleMenuItem);
	            }
            }
        }

        private void BindAppBarEvents() {
            this._addAppBarButton.Click += (s, e) => this.ViewModel.NewCommand.Execute(null);
	        this._deleteAppBarButton.Click += (s, e) => this.ViewModel.DeleteSelectedCommand.Execute(null);
			this._changeScheduleMenuItem.Click += (s, e) => AcademicScheduleSelector.GetInstanceAndShow(
                this,
                String.Format(R.AcademicScheduleTaskExamListHelp, PluralTypeName.ToLower()),
                false
            );

			if (this.IsSelectionEnabled) {
				this._selectAppBarButton.Click += SetSelectionEnabled;
			}
        }

	    private void SetSelectionEnabled(object sender, EventArgs e) {
            Pivot lockablePivot;

		    if (this.FilteredLists != null && (lockablePivot = this.FindName("LockablePivot") as Pivot) != null) {
			    this.FilteredLists.ElementAt(lockablePivot.SelectedIndex).IsSelectionEnabled = true;
		    }
	    }

	    #endregion

        #region OnBackKeyPress(...)

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e) {
            if (this.FilteredLists != null && (this.ViewModel.SelectedItems.Any() || this.FilteredLists.Any(x => x.IsSelectionEnabled))) {
                this.ViewModel.SelectedItems.Clear();

                this.FilteredLists.Apply(x => x.IsSelectionEnabled = false);

                e.Cancel = true;
            }
        }

        #endregion
    }
}
