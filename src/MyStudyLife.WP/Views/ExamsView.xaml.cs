﻿using System.Collections.Generic;
using Microsoft.Phone.Controls;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.WP.Views {
    public class InternalExamsView : BaseEntitiesView<Exam, ExamsViewModel> {}

    public partial class ExamsView {
        private readonly List<LongListMultiSelector> _filteredLists;

        public override IEnumerable<LongListMultiSelector> FilteredLists {
            get { return _filteredLists; }
        }

        protected override string PluralTypeName {
            get { return R.Exams; }
        }

        public ExamsView() {
            InitializeComponent();

            this._filteredLists = new List<LongListMultiSelector> {
                this.CurrentExamsList, this.PastExamsList
            };
        }

        protected override void SetCurrentFilter(int pivotIndex) {
            this.ViewModel.Filter = (ExamFilterOption)pivotIndex;
        }
    }
}