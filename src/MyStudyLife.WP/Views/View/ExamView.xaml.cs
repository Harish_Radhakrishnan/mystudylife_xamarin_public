﻿using System;
using Cirrious.CrossCore;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.View {
    public partial class ExamView {
        public new ExamViewModel ViewModel  {
            get { return base.ViewModel as ExamViewModel; }
        }

        private readonly ApplicationBarIconButton _editAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Edit);
        private readonly ApplicationBarIconButton _deleteAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Delete);

        public ExamView() {
            InitializeComponent();
        }

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            _editAppBarButton.Click += (s, a) => this.ViewModel.EditCommand.Execute(null);
            _deleteAppBarButton.Click += DeleteAppBarButtonOnClick;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
            this.ApplicationBar.Buttons.Add(_editAppBarButton);
            this.ApplicationBar.Buttons.Add(_deleteAppBarButton);
        }

        private void DeleteAppBarButtonOnClick(object sender, EventArgs eventArgs) {
            Mvx.Resolve<IDialogService>().RequestConfirmation(R.ConfirmationTitle, R.DeletionMessage, () => {
                UIHelper.AnimateViewDeletion(this, () => this.NavigationService.GoBack());

                this.ViewModel.DeleteCommand.Execute(null);
            });
        }
    }
}