﻿using System;
using System.Linq;
using System.Windows;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.WP.Common;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MyStudyLife.WP.Views.View {
    public partial class ClassView {
        [UsedImplicitly] private IDisposable _listener;

        public new ClassViewModel ViewModel  {
            get { return base.ViewModel as ClassViewModel; }
        }

        private readonly ApplicationBarIconButton _editAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Edit);
        private readonly ApplicationBarIconButton _deleteAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Delete);
        private readonly ApplicationBarMenuItem _leaveAppBarMenuItem = new ApplicationBarMenuItem("leave class");

        private readonly PivotItem _studentsPivot;
        private readonly PivotItem _schedulePivot;
        private readonly PivotItem _currentPivot;
        private readonly PivotItem _tasksDuePivot;
	    private readonly PivotItem _tasksOverduePivot;

        public ClassView() {
            InitializeComponent();

            this._studentsPivot = (PivotItem)this.FindName("StudentsPivot");
            this._schedulePivot = (PivotItem)this.FindName("SchedulePivot");
            this._currentPivot = (PivotItem)this.FindName("CurrentPivot");
            this._tasksDuePivot = (PivotItem)this.FindName("TasksDuePivot");
			this._tasksOverduePivot = (PivotItem)this.FindName("TasksOverduePivot");
            
            this.SetPivotState();
		}

        protected override void OnViewModelSet() {
            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(nameof(ViewModel.IsLoaded), () => {
                    if (this.ViewModel.IsLoaded) {
                        this.ViewModelOnLoaded();
                    }
                })
                .Listen(() => this.ViewModel.User, SetPivotState)
                .Listen(() => this.ViewModel.Item, SetPivotState)
                .Listen(() => this.ViewModel.ScheduledItem, SetPivotState)
                .Listen(() => this.ViewModel.ShowOverdueTasks, SetPivotState)
                .Listen(() => this.ViewModel.IsReadOnly, () => {
                    if (this.ViewModel.IsReadOnly) {
                        this.ApplicationBar.Mode = ApplicationBarMode.Minimized;

                        this.ApplicationBar.Buttons.Clear();

                        if (this.ViewModel.User.IsStudentWithUnmanagedSchool && !this.ApplicationBar.MenuItems.Contains(_leaveAppBarMenuItem)) {
                            this.ApplicationBar.MenuItems.Add(_leaveAppBarMenuItem);
                        }
                    }
                })
                .Listen(() => this.ViewModel.IsLeaving, () => {
                    this.ApplicationBar.IsVisible = !this.ViewModel.IsLeaving;
                });

            if (this.ViewModel.IsLoaded) {
                this.ViewModelOnLoaded();
            }
        }

        private void ViewModelOnLoaded() {
            var vm = this.ViewModel;

            if (vm.User.IsStudentWithManagedSchool || vm.IsReadOnly) {
                this.RootContent.Margin = (Thickness)App.Current.Resources["MyStudyLifeRootContentMargin"];
            }

            if (vm.User.IsStudentWithManagedSchool) {
                this.ApplicationBar.IsVisible = false;
            }

            this.SetPivotState();
        }

		private void SetPivotState() {
            var vm = this.ViewModel;

			if (vm == null || !vm.IsScheduled) {
                this.LockablePivot.Items.Remove(_currentPivot);
                this.LockablePivot.Items.Remove(_tasksDuePivot);
				this.LockablePivot.Items.Remove(_tasksOverduePivot);

                // Show class as header when viewed from the schedule screen
                _schedulePivot.Header = "class";
            }
			else {
                _schedulePivot.Header = "schedule";

                this.LockablePivot.Items.InsertWhereNotExists(_currentPivot, 0);
                this.LockablePivot.Items.AddWhereNotExists(_tasksDuePivot);

			    bool showOverdueTasksPivot = this.ViewModel.ShowOverdueTasks;

                if (!this.LockablePivot.Items.Contains(_tasksOverduePivot) && showOverdueTasksPivot) {
			        this.LockablePivot.Items.Add(_tasksOverduePivot);
			    }
			    else if (!showOverdueTasksPivot) {
                    this.LockablePivot.Items.Remove(_tasksOverduePivot);
                }

                if (this.ViewModel.Item.IsOneOff) {
                    this.LockablePivot.Items.Remove(_schedulePivot);
                }
            }

            if (this.ViewModel == null || this.ViewModel.User == null || !this.ViewModel.User.IsTeacher) {
                this.LockablePivot.Items.Remove(_studentsPivot);
            }
            else if (this.ViewModel.StudentsCanJoinClass && !this.LockablePivot.Items.Contains(_studentsPivot)) {
                this.LockablePivot.Items.Insert(1, _studentsPivot);
            }
        }

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            _leaveAppBarMenuItem.Click += (s, e) => this.ViewModel.LeaveWithConfirmationCommand.Execute(null);

            _editAppBarButton.Click += (s, e) => this.ViewModel.EditCommand.Execute(null);
            _deleteAppBarButton.Click += DeleteAppBarButtonOnClick;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {

            if (this.ViewModel != null && this.ViewModel.IsReadOnly) {
                appBar.Mode = ApplicationBarMode.Minimized;
                
                this.ApplicationBar.MenuItems.Add(_leaveAppBarMenuItem);

                return;
            }

            this.ApplicationBar.Buttons.Add(_editAppBarButton);
            this.ApplicationBar.Buttons.Add(_deleteAppBarButton);
        }

        private void DeleteAppBarButtonOnClick(object sender, EventArgs e) {
            Mvx.Resolve<IDialogService>().RequestConfirmation(
                R.ConfirmationTitle,
                this.ViewModel.ScheduledItem == null ? R.DeletionMessage : R.ScheduledClassDeletion,
                () => {
                    UIHelper.AnimateViewDeletion(this, () => this.NavigationService.GoBack());

                    this.ViewModel.DeleteCommand.Execute(null);
                }
            );
        }

        private void ViewStudentsOnTap(object sender, GestureEventArgs e) {
            if (this.ViewModel.StudentsHasErrored) {
                this.ViewModel.SetStudentsCommand.Execute(null);
            }

            this.LockablePivot.SelectedIndex = 1;
        }
    }
}