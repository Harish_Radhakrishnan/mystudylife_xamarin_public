﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.CrossCore.WeakSubscription;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.View {
    public partial class TaskView {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;
        [UsedImplicitly] private MvxPropertyChangedListener _itemListener;

        public new TaskViewModel ViewModel {
            get { return base.ViewModel as TaskViewModel; }
        }

        private readonly ApplicationBarIconButton _doneAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Check, "complete");
        private readonly ApplicationBarIconButton _editAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Edit);
        private readonly ApplicationBarIconButton _deleteAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Delete);

	    private readonly PivotItem _revisionPivot;

        public TaskView() {
            InitializeComponent();

	        this._revisionPivot = (PivotItem) this.FindName("RevisionPivot");

			this.ProgressSlider.ManipulationCompleted += ProgressSliderOnManipulationCompleted;
        }

        protected override void OnViewModelSet() {
            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.Item, ViewModelOnItemChanged);

            SetDoneAppBarButtonState();

            ViewModelOnItemChanged();
        }

        private void ViewModelOnItemChanged() {
            SetPivotState();

            if (this.ViewModel.Item != null) {
                SetDoneAppBarButtonState();

                _itemListener = new MvxPropertyChangedListener(this.ViewModel.Item)
                    .Listen(() => this.ViewModel.Item.Progress, SetDoneAppBarButtonState);
            }
        }
        
        private void ProgressSliderOnManipulationCompleted(object sender, ManipulationCompletedEventArgs e) {
			e.Handled = true;
		}

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            _doneAppBarButton.Click += (s, e) => this.ViewModel.MarkAsCompleteCommand.Execute(null);
            _editAppBarButton.Click += (s, a) => this.ViewModel.EditCommand.Execute(null);
            _deleteAppBarButton.Click += DeleteAppBarButtonOnClick;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
	        this.ApplicationBar.Buttons.Add(_editAppBarButton);
            this.ApplicationBar.Buttons.Add(_deleteAppBarButton);

	        SetDoneAppBarButtonState();
	    }

        private void DeleteAppBarButtonOnClick(object sender, EventArgs eventArgs) {
            Mvx.Resolve<IDialogService>().RequestConfirmation(R.ConfirmationTitle, R.DeletionMessage, () => {
                UIHelper.AnimateViewDeletion(this, () => this.NavigationService.GoBack());

                this.ViewModel.DeleteCommand.Execute(null);
            });
        }

        private void ProgressSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            // Keep steps to 5%
            ((Slider)sender).Value = (int)Math.Round(e.NewValue / 5) * 5;
        }

        private void SetDoneAppBarButtonState() {
            if (this.ViewModel == null || this.ViewModel.Item == null || this.ViewModel.Item.IsComplete) {
                this.ApplicationBar.Buttons.Remove(_doneAppBarButton);
            }
            else if (!this.ApplicationBar.Buttons.Contains(_doneAppBarButton)) {
                this.ApplicationBar.Buttons.Insert(0, _doneAppBarButton);
            }
        }

	    private void SetPivotState() {
		    if (this.ViewModel == null || this.ViewModel.Item == null || this.ViewModel.Item.Type != TaskType.Revision) {
			    this.LockablePivot.Items.Remove(_revisionPivot);
		    }
			else if (!this.LockablePivot.Items.Contains(_revisionPivot)) {
				this.LockablePivot.Items.Add(_revisionPivot);
			}
	    }
    }
}