﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Schools;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.Schools {
    public partial class SchoolWelcomeView : MslAppView {
        public new SchoolWelcomeViewModel ViewModel {
            get { return base.ViewModel as SchoolWelcomeViewModel; }
        }

        public override bool AutoAppBarCreation {
            get {
                return false;
            }
        }

        public SchoolWelcomeView() {
            InitializeComponent();
        }
    }
}