﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views {
	public partial class ScheduleView {
	    [UsedImplicitly] private MvxPropertyChangedListener _subscription;

	    private AcademicScheduleSelector _scheduleSelector;

		public new ScheduleViewModel ViewModel {
            get { return base.ViewModel as ScheduleViewModel; }
		}

	    private readonly AppBarIconButton _newYearAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.New, "new year");
        private readonly AppBarIconButton _newClassAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.New, R.NewClass.ToLower());
        private readonly AppBarIconButton _newHolidayAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.New, R.NewHoliday.ToLower());
        private readonly AppBarMenuItem _joinClassesAppBarMenuItem = new AppBarMenuItem("join classes");
        private readonly AppBarMenuItem _newAcademicYearAppBarMenuItem = new AppBarMenuItem(R.NewAcademicYear.ToLower());
        private readonly AppBarMenuItem _editAcademicYearAppBarMenuItem = new AppBarMenuItem(R.EditAcademicYear.ToLower());
        private readonly AppBarMenuItem _manageSubjectsAppBarMenuItem = new AppBarMenuItem("manage subjects");

		public ScheduleView() {
			InitializeComponent();

            this.LockablePivot.SelectionChanged += (s, e) => SetAppBarNewButton();
		}

	    #region Overrides of MslAppView

	    protected override void OnViewModelSet() {
	        _subscription = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.ShowLonely, SetAppBar)
                .Listen(() => this.ViewModel.SelectedYear, SetAppBar);

            this.SetCanJoinClasses();

            this._newYearAppBarButton.Command = this.ViewModel.NewAcademicYearCommand;
            this._newClassAppBarButton.Command = this.ViewModel.NewClassCommand;
            this._newHolidayAppBarButton.Command = this.ViewModel.NewHolidayCommand;
            this._joinClassesAppBarMenuItem.Command = this.ViewModel.ShowJoinClassesCommand;
	        this._newAcademicYearAppBarMenuItem.Command = this.ViewModel.NewAcademicYearCommand;
	        this._editAcademicYearAppBarMenuItem.Command = this.ViewModel.EditAcademicYearCommand;
	        this._manageSubjectsAppBarMenuItem.Command = this.ViewModel.ShowManageSubjectsCommand;

            this._joinClassesAppBarMenuItem.Command.CanExecuteChanged += (s, e) => SetCanJoinClasses();
        }

        #endregion

        private void SetAppBarNewButton() {
            var appBar = this.ApplicationBar;

            // If the user is part of a managed school then they won't need to be able to access the
            // appBar as all the options will be disabled anyways
            if (this.ViewModel != null && !this.ViewModel.User.IsStudentWithManagedSchool) {
                if (this.LockablePivot.SelectedIndex == 1) {
                    appBar.RemoveButton(_newClassAppBarButton);
                    appBar.AddButton(_newHolidayAppBarButton);
                }
                else {
                    appBar.RemoveButton(_newHolidayAppBarButton);
                    appBar.AddButton(_newClassAppBarButton);
                }
            }
            else {
                appBar.IsVisible = false;
            }
        }

	    private void SetCanJoinClasses() {
            if (this.ViewModel.ShowJoinClassesCommand.CanExecute(null)) {
                this.ApplicationBar.InsertMenuItem(0, _joinClassesAppBarMenuItem);
            }
            else {
                this.ApplicationBar.RemoveMenuItem(_joinClassesAppBarMenuItem);
            }
	    }

	    private void SetLonelyAppBar() {
	        var appBar = this.ApplicationBar;

            appBar.RemoveButton(_newClassAppBarButton);
            appBar.RemoveButton(_newHolidayAppBarButton);
            appBar.RemoveMenuItem(_editAcademicYearAppBarMenuItem);
            appBar.RemoveMenuItem(_newAcademicYearAppBarMenuItem);
            appBar.AddButton(_newYearAppBarButton);
	    }

        private void SetNormalAppBar() {
            var appBar = this.ApplicationBar;

            appBar.RemoveButton(_newYearAppBarButton);

            this.SetAppBarNewButton();

            var menuItemLoc = appBar.MenuItems.IndexOf(_manageSubjectsAppBarMenuItem);

            if (this.ViewModel != null && this.ViewModel.EditAcademicYearCommand.CanExecute(null)) {
                _editAcademicYearAppBarMenuItem.Text = "edit " + this.ViewModel.SelectedYear;
                
                appBar.InsertMenuItem(menuItemLoc, _editAcademicYearAppBarMenuItem);
                
                menuItemLoc++;
            }
            else {
                appBar.RemoveMenuItem(_editAcademicYearAppBarMenuItem);
            }

            appBar.InsertMenuItem(menuItemLoc, _newAcademicYearAppBarMenuItem);

	    }

        private void SetAppBar() {
            if (_scheduleSelector != null && _scheduleSelector.IsOpen) {
                return;
            }

            IApplicationBar appBar = this.ApplicationBar;

            appBar.AddMenuItem(_manageSubjectsAppBarMenuItem);

            if (User.Current.IsStudentWithManagedSchool) {
                appBar.IsVisible = false;
                this.RootContent.Margin = (Thickness)App.Current.Resources["MyStudyLifeRootContentMargin"];
            }
            else {
                appBar.IsVisible = true;
                this.RootContent.Margin = (Thickness)App.Current.Resources["RootContentWithAppBarMargin"];

                if (this.ViewModel.ShowLonely) {
                    this.SetLonelyAppBar();
                }
                else {
                    this.SetNormalAppBar();
                }
            }
	    }

	    protected override void OnAppBarCreated(IApplicationBar appBar) {
            this.SetAppBar();
		}

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            _scheduleSelector = AcademicScheduleSelector.GetInstanceAndShow(
                this.GetVisualParent<MslAppView>(),
                R.GetScheduleViewFilterHelp(this.ViewModel.HasClassesWithoutSchedule),
                false
            );

            _scheduleSelector.SetBinding(AcademicScheduleSelector.ShowNoneProperty, new Binding("HasClassesWithoutSchedule"));

            _scheduleSelector.Closed += ScheduleSelectorOnClosed;
        }

        private void ScheduleSelectorOnClosed(object sender, EventArgs e) {
            if (_scheduleSelector != null) {
                _scheduleSelector.Closed -= ScheduleSelectorOnClosed;

                _scheduleSelector = null;
            }

            this.OnAppBarCreated(this.ApplicationBar);
        }
	}

    public class SchoolVisibilityConverter : DependencyObject, IValueConverter {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof(User), typeof(SchoolVisibilityConverter), new PropertyMetadata(null));

        public User User {
            get { return (User)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || this.User == null) {
                return Visibility.Collapsed;
            }

            return (((IBelongToSchoolOrUser)value).SchoolId.HasValue && this.User.IsStudentWithUnmanagedSchool).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }

    public class HolidayPushesScheduleVisibilityConverter : DependencyObject, IValueConverter {
        public static readonly DependencyProperty AcademicYearProperty = DependencyProperty.Register(
            "AcademicYear", typeof(AcademicYear), typeof(HolidayPushesScheduleVisibilityConverter), new PropertyMetadata(null));

        public AcademicYear AcademicYear {
            get { return (AcademicYear)GetValue(AcademicYearProperty); }
            set { SetValue(AcademicYearProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || this.AcademicYear == null) {
                return Visibility.Collapsed;
            }

            return ((Holiday)value).EffectivePushesSchedule(this.AcademicYear).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }

    public class ClassSchoolVisibilityConverter : DependencyObject, IValueConverter {
        public static readonly DependencyProperty UserProperty = DependencyProperty.Register(
            "User", typeof(User), typeof(ClassSchoolVisibilityConverter), new PropertyMetadata(null));

        public User User {
            get { return (User)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || this.User == null) {
                return Visibility.Collapsed;
            }

            return (((Class)value).IsFromSchool && this.User.IsStudentWithUnmanagedSchool).ToVisibility();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}