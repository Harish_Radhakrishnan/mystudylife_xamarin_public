﻿using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Shell;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;
using MyStudyLife.UI.Annotations;

namespace MyStudyLife.WP.Views.Input {
    public partial class AcademicTermInputView {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

	    public new AcademicTermInputViewModel ViewModel {
			get { return base.ViewModel as AcademicTermInputViewModel; }
	    }

		private readonly AppBarIconButton _saveAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Save);
		private readonly AppBarIconButton _cancelAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Cancel);
        private readonly AppBarMenuItem _deleteMenuItem = new AppBarMenuItem("delete academic term");

		public AcademicTermInputView() {
            InitializeComponent();
        }

        protected override void OnViewModelSet() {
            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.IsLoaded, SetDeleteAppBarButton);

            this._saveAppBarButton.Command = this.ViewModel.SaveCommand;
            this._cancelAppBarButton.Command = this.ViewModel.CancelInputCommand;
            this._deleteMenuItem.Command = this.ViewModel.DeleteWithConfirmationCommand;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.Buttons.Add(_saveAppBarButton);
			appBar.Buttons.Add(_cancelAppBarButton);

            this.SetDeleteAppBarButton();
        }

        private void SetDeleteAppBarButton() {
            IApplicationBar appBar = ApplicationBar;

            if (this.ViewModel.CanDelete) {
                if (!appBar.MenuItems.Contains(this._deleteMenuItem)) {
                    appBar.MenuItems.Add(this._deleteMenuItem);
                }
            }
            else {
                appBar.MenuItems.Remove(this._deleteMenuItem);
            }
        }
    }
}