﻿using System;
using System.Windows.Input;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views.Input {
    public partial class ExamInputView {
        public new ExamInputViewModel ViewModel {
            get { return (ExamInputViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private readonly ApplicationBarIconButton _saveAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Save);
        private readonly ApplicationBarIconButton _cancelAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Cancel);

        private readonly ApplicationBarMenuItem _changeScheduleMenuItem = new ApplicationBarMenuItem(R.ChangeAcademicSchedule);

        public ExamInputView() {
            InitializeComponent();
        }

        #region Overrides of MslAppView

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            this._saveAppBarButton.Click += (s, e) => this.ViewModel.SaveCommand.Execute(null);
            this._cancelAppBarButton.Click += CancelAppBarButtonOnClick;
            this._changeScheduleMenuItem.Click += (s, e) => HeaderOnTap(s, null);
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
            appBar.Buttons.Add(_saveAppBarButton);
            appBar.Buttons.Add(_cancelAppBarButton);
	        appBar.MenuItems.Add(_changeScheduleMenuItem);
        }

	    #endregion

        private void CancelAppBarButtonOnClick(object sender, EventArgs e) {
            App.Current.RootFrame.GoBack();
        }

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this,
                R.AcademicScheduleTaskExamInputHelp,
                false
            );
        }
    }
}