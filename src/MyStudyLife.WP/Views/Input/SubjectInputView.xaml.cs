﻿using System;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views.Input {
    public partial class SubjectInputView {
        [UsedImplicitly] private MvxPropertyChangedListener _listener;

	    public new SubjectInputViewModel ViewModel {
			get { return base.ViewModel as SubjectInputViewModel; }
	    }

		private readonly ApplicationBarIconButton _saveAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Save);
		private readonly ApplicationBarIconButton _cancelAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Cancel);
		private readonly ApplicationBarMenuItem _deleteMenuItem = new ApplicationBarMenuItem("delete subject");

        public SubjectInputView() {
            InitializeComponent();
        }

        protected override void OnViewModelSet() {
            _listener = new MvxPropertyChangedListener(this.ViewModel)
                .Listen(() => this.ViewModel.CanDelete, SetDeleteAppBarButton);
        }

	    #region Overrides of MslAppView

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            this._saveAppBarButton.Click += (s, e) => this.ViewModel.SaveCommand.Execute(null);
            this._cancelAppBarButton.Click += CancelAppBarButtonOnClick;
            this._deleteMenuItem.Click += DeleteAppBarButtonOnClick;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.Buttons.Add(_saveAppBarButton);
			appBar.Buttons.Add(_cancelAppBarButton);

			SetDeleteAppBarButton();
	    }

		private void SetDeleteAppBarButton() {
			IApplicationBar appBar = ApplicationBar;

			if (this.ViewModel.CanDelete && !appBar.MenuItems.Contains(this._deleteMenuItem)) {
				appBar.MenuItems.Add(this._deleteMenuItem);
			}
			else {
				appBar.MenuItems.Remove(this._deleteMenuItem);
			}
	    }

	    #endregion

		private void CancelAppBarButtonOnClick(object sender, EventArgs e) {
			App.Current.RootFrame.GoBack();
		}

        private void DeleteAppBarButtonOnClick(object sender, EventArgs e) {
            Mvx.Resolve<IDialogService>().RequestConfirmation(
                R.ConfirmationTitle,
                R.SubjectDeletion,
                () => {
                    UIHelper.AnimateViewDeletion(this, () => this.NavigationService.GoBack());

                    this.ViewModel.DeleteCommand.Execute(null);
                }
            );
        }

	    private void AcademicScheduleInputOnTap(object sender, GestureEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this,
                R.SubjectInput.AcademicScheduleListHelp,
                false
            );
	    }
    }
}