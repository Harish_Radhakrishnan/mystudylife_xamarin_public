﻿using System;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace MyStudyLife.WP.Views.Input {
    public partial class ClassInputView {
        public new ClassInputViewModel ViewModel {
            get { return (ClassInputViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
		}

		private readonly ApplicationBarIconButton _saveAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Save);
		private readonly ApplicationBarIconButton _cancelAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Cancel);

        private readonly ApplicationBarMenuItem _changeScheduleMenuItem = new ApplicationBarMenuItem(R.ChangeAcademicSchedule);

        public ClassInputView() {
            InitializeComponent();
		}

		#region Overrides of MslAppView

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            this._saveAppBarButton.Click += (s, e) => this.ViewModel.SaveCommand.Execute(null);
            this._cancelAppBarButton.Click += CancelAppBarButtonOnClick;
            this._changeScheduleMenuItem.Click += (s, e) => HeaderOnTap(s, null);
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.AddButton(_saveAppBarButton);
            appBar.AddButton(_cancelAppBarButton);
            appBar.AddMenuItem(_changeScheduleMenuItem);
		}

		#endregion

		private void CancelAppBarButtonOnClick(object sender, EventArgs e) {
			App.Current.RootFrame.GoBack();
		}

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this,
                R.ClassInput.AcademicScheduleHelp
            );
        }
    }
}