﻿using System;
using System.Collections;
using System.Linq;
using Microsoft.Phone.Shell;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.WP.Common;

namespace MyStudyLife.WP.Views.Input {
    public partial class ClassTimeInputView {

	    public new ClassTimeInputViewModel ViewModel {
			get { return (ClassTimeInputViewModel) base.ViewModel; }
	    }

		private readonly ApplicationBarIconButton _saveAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Save);
		private readonly ApplicationBarIconButton _cancelAppBarButton = AppBarHelper.CreateButton(AppBarButtonKind.Cancel);

        public ClassTimeInputView() {
            InitializeComponent();

            this.DaysPicker.SummaryForSelectedItemsDelegate = SummaryForSelectedItemsDelegate;
            this.RotationDaysPicker.SummaryForSelectedItemsDelegate = SummaryForSelectedItemsDelegate;
        }
        
        private string SummaryForSelectedItemsDelegate(IList list) {
            if (list == null || list.Count == 0) return "No Days Selected";

            return list.Cast<Tuple<int, string, int>>().Select(x => x.Item2).Aggregate((a, b) => String.Format("{0}, {1}", a, b));
        }

	    #region Overrides of MslAppView

        protected override void OnAppBarCreating(IApplicationBar appBar) {
            this._saveAppBarButton.Click += (s, e) => this.ViewModel.SaveCommand.Execute(null);
            this._cancelAppBarButton.Click += CancelAppBarButtonOnClick;
        }

        protected override void OnAppBarCreated(IApplicationBar appBar) {
			appBar.Buttons.Add(_saveAppBarButton);
			appBar.Buttons.Add(_cancelAppBarButton);
	    }

	    #endregion

		private void CancelAppBarButtonOnClick(object sender, EventArgs e) {
			App.Current.RootFrame.GoBack();
		}
    }
}