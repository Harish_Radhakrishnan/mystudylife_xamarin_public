﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Cirrious.CrossCore.WeakSubscription;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using MyStudyLife.UI.ViewModels.Wizards;

namespace MyStudyLife.WP.Views.Wizards {
    public partial class WelcomeWizardView {
        // ReSharper disable once NotAccessedField.Local
        private MvxNotifyPropertyChangedEventSubscription _viewModelPropertyChangedEventSubscription;

        public new WelcomeWizardViewModel ViewModel {
            get { return (WelcomeWizardViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override bool AutoAppBarCreation {
            get { return false; }
        }

        public WelcomeWizardView() {
	        App.Current.RootFrame.RemoveBackEntry();

            InitializeComponent();

            double width = Application.Current.Host.Content.ActualWidth;

            if (!DesignerProperties.IsInDesignTool) {
                ((TranslateTransform)this.Step0.RenderTransform).X = width;
                ((TranslateTransform)this.Step1.RenderTransform).X = width;
                ((TranslateTransform)this.Step2.RenderTransform).X = width;
                ((TranslateTransform)this.Step3.RenderTransform).X = width;
            }

            VisualStateManager.GoToState(this, "Step0State", true);

			this.Loaded += OnLoaded;
        }

		private void OnLoaded(object sender, RoutedEventArgs e) {
		    if (this.ViewModel != null) {
		        _viewModelPropertyChangedEventSubscription = this.ViewModel.WeakSubscribe(ViewModelOnPropertyChanged);
		    }
		}

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "Step") {
                // Then we're passed finished
                if (this.ViewModel.Step >= 4) {
                    return;
                }

                VisualStateManager.GoToState(this, "Step" + this.ViewModel.Step + "State", true);
            }
            else if (e.PropertyName == "IsBusy") {
                SystemTray.SetForegroundColor(this, this.ViewModel.IsBusy ? Colors.White : (Color) App.Current.Resources["ColorAccent"]);
            }
	    }

        // Required until MvvmCross 3.1.2 released with strong command helper for WP (doesn't become
        // enabled if bound to the command).
        // https://github.com/MvvmCross/MvvmCross/commit/f1ebeb5188a16c21d2179f02b2efb840d597f0bc
        private void PreviousButtonOnTap(object sender, GestureEventArgs e) {
            this.ViewModel.PreviousStepCommand.Execute(null);
        }
    }
}