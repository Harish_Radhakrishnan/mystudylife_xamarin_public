﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Cirrious.CrossCore.WeakSubscription;
using Microsoft.Phone.Shell;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Controls;

namespace MyStudyLife.WP.Views.Wizards {
    public partial class JoinClassesView {
        // ReSharper disable once NotAccessedField.Local
        private MvxNotifyPropertyChangedEventSubscription _viewModelPropertyChangedEventSubscription;

        public new JoinClassesViewModel ViewModel {
            get { return (JoinClassesViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override bool AutoAppBarCreation {
            get { return false; }
        }

        public JoinClassesView() {
            InitializeComponent();

            double width = Application.Current.Host.Content.ActualWidth;

            if (!DesignerProperties.IsInDesignTool) {
                ((TranslateTransform)this.Step0.RenderTransform).X = width;
                ((TranslateTransform)this.Step1.RenderTransform).X = width;
                ((TranslateTransform)this.Step2.RenderTransform).X = width;
                ((TranslateTransform)this.Step3.RenderTransform).X = width;
            }

            VisualStateManager.GoToState(this, "Step0State", true);

			this.Loaded += OnLoaded;
        }

		private void OnLoaded(object sender, RoutedEventArgs e) {
		    if (this.ViewModel != null) {
		        _viewModelPropertyChangedEventSubscription = this.ViewModel.WeakSubscribe(ViewModelOnPropertyChanged);
		    }
		}

	    private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == "Step") {
                // Then we're passed finished
                if (this.ViewModel.Step >= 5) {
                    return;
                }

                this.SchedulePickerButton.IsEnabled = this.ViewModel.Step == 0;

                if (this.ViewModel.Step == 3) {
                    this.ClassTimesConfirmationList.ItemsSource = this.ViewModel.SelectedClasses.Cast<Class>().Select<Class, KeyedList<ClassTime>>(x =>
                        new KeyedList<ClassTime>(x.Title, x.Times)
                    ).ToList();
                }

                VisualStateManager.GoToState(this, "Step" + this.ViewModel.Step + "State", true);
            }
            else if (e.PropertyName == "IsBusy") {
                SystemTray.SetForegroundColor(this, this.ViewModel.IsBusy ? Colors.White : (Color) App.Current.Resources["ColorAccent"]);
            }
            else if (e.PropertyName == "GroupedClasses" && this.ViewModel.GroupedClasses != null) {
                this.ClassesSelector.ItemsSource = this.ViewModel.GroupedClasses.Select<IGrouping<string, Class>, KeyedList<Class>>(x =>
                    new KeyedList<Class>(x.Key, x)
                ).ToList();
            }
	    }

        protected override void OnBackKeyPress(CancelEventArgs e) {
            if (this.ViewModel != null && this.ViewModel.CanGoBack) {
                e.Cancel = true;

                this.ViewModel.PreviousStepCommand.Execute();
            }
        }

        private void HeaderOnTap(object sender, GestureEventArgs e) {
            AcademicScheduleSelector.GetInstanceAndShow(
                this,
                R.ClassInput.JoinWizardAcademicScheduleHelp,
                false
            );
        }

        // Required until MvvmCross 3.1.2 released with strong command helper for WP (doesn't become
        // enabled if bound to the command).
        // https://github.com/MvvmCross/MvvmCross/commit/f1ebeb5188a16c21d2179f02b2efb840d597f0bc
        private void PreviousButtonOnTap(object sender, GestureEventArgs e) {
            this.ViewModel.PreviousStepCommand.Execute(null);
        }
        private void NextButtonOnTap(object sender, GestureEventArgs e) {
            this.ViewModel.NextStepCommand.Execute(null);
        }

        public class KeyedList<T> : List<T> {
            public string Key { get; set; }

            public KeyedList(string key, IEnumerable<T> items) : base(items) {
                this.Key = key;
            }
        }
    }
}