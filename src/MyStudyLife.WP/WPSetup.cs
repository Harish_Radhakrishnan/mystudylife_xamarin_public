using System.Windows.Media;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.BindingEx.WindowsShared;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsPhone.Platform;
using Cirrious.MvvmCross.WindowsPhone.Views;
using Microsoft.Phone.Controls;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Store;
using MyStudyLife.Diagnostics;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Security.Crypto;
using MyStudyLife.Services;
using MyStudyLife.UI;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.Versioning;
using MyStudyLife.WP.Platform;
using MyStudyLife.WP.Versioning;

namespace MyStudyLife.WP {
    public class WPSetup : MvxPhoneSetup {
        public WPSetup(PhoneApplicationFrame rootFrame)
            : base(rootFrame) {

            DataStore.TraceEnabled = false;
        }

        protected override IMvxApplication CreateApp() => new UI.App();

        protected override IMvxTrace CreateDebugTrace() {
            return new MslDebugTrace();
        }

        #region Overrides of MvxSetup

		protected override void InitializeFirstChance() {
			base.InitializeFirstChance();

			Mvx.LazyConstructAndRegisterSingleton<IBugReporter, WPBugReporter>();

            Mvx.LazyConstructAndRegisterSingleton<IMslConfig, WPConfig>();
            Mvx.LazyConstructAndRegisterSingleton<INetworkProvider, WPNetworkProvider>();
            Mvx.LazyConstructAndRegisterSingleton<IStorageProvider, WPStorageProvider>();
            Mvx.LazyConstructAndRegisterSingleton<IAuthorizationProvider, WPAuthorizationProvider>();
			Mvx.LazyConstructAndRegisterSingleton<ICryptoProvider, WPCryptoProvider>();
			Mvx.LazyConstructAndRegisterSingleton<IReminderScheduler, WPReminderScheduler>();

            Mvx.LazyConstructAndRegisterSingleton<IDialogService, WPDialogService>();

            Mvx.LazyConstructAndRegisterSingleton<IPushNotificationService, WPPushNotificationService>();

			Mvx.RegisterType<IBootstrapAction, WPBootstrapAction>();

			Mvx.RegisterType<ITaskProvider, WPTaskProvider>();

            Mvx.RegisterType<ITimer, WPTimer>();

            Mvx.LazyConstructAndRegisterSingleton<IAnalyticsService, WPAnalyticsService>();
        }

        protected override void InitializeLastChance() {
            base.InitializeLastChance();

            var upgradeService = new WPUpgradeService(Mvx.Resolve<IMslConfig>(), Mvx.Resolve<IStorageProvider>());

            Mvx.RegisterSingleton(typeof(IUpgradeService), upgradeService);

            RegisterUpgrades(upgradeService);

	        new MvxWindowsBindingBuilder().DoRegistration();
        }

        protected override IMvxPhoneViewPresenter CreateViewPresenter(PhoneApplicationFrame rootFrame) => new WPPresenter(rootFrame);

        private void RegisterUpgrades(IUpgradeService upgradeService) {
            upgradeService.RegisterUpgrade<V3ToV4>();
            upgradeService.RegisterUpgrade<V4ToV5>();
            upgradeService.RegisterUpgrade<V5ToV6>();
            upgradeService.RegisterUpgrade<V605ToV606>();
        }

        #endregion
    }
}