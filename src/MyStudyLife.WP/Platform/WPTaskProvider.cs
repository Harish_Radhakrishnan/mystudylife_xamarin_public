﻿using System;
using Microsoft.Phone.Tasks;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.WP.Platform {
	public class WPTaskProvider : ITaskProvider {
		public void ShowEmailComposer(string subject, string body, string to) {
			new EmailComposeTask {
				Subject = subject, 
				Body = body, 
				To = to
			}.Show();
		}

	    public void ReviewApp() {
			new MarketplaceReviewTask().Show();
		}

        public bool TryShowNativeUri(Uri uri) {
            return false;
        }

	    public void ShowWebUri(Uri uri) {
	        new WebBrowserTask {
	            Uri = uri
	        }.Show();
	    }
	}
}
