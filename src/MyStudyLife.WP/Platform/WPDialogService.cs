﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.UI;
using MyStudyLife.UI.Services;

namespace MyStudyLife.WP.Platform {
    public class WPDialogService : DialogServiceBase, IDialogService {
        private Dispatcher Dispatcher => App.Current.RootVisual.Dispatcher;

        protected override void RequestConfirmationImpl(string title, string message, string cancelText, string confirmText, Action<bool> onActioned) {
            Dispatcher.BeginInvoke(() => {
                var msg = new CustomMessageBox {
                    Caption = title,
                    Message = message,

                    LeftButtonContent = confirmText.ToLower(),
                    RightButtonContent = cancelText.ToLower()
                };

                msg.Dismissed += (sender, args) => {
                    UnsetMessageBoxSystemTray();

                    onActioned(args.Result == CustomMessageBoxResult.LeftButton);
                };

                msg.Show();

                SetMessageBoxSystemTray();
            });
        }

        public void ShowDismissible(string title, string message) {
            var msg = new CustomMessageBox {
                Caption = title,
                Message = message,
                RightButtonContent = R.Common.Okay.ToLower()
            };

            msg.Dismissed += (sender, args) => UnsetMessageBoxSystemTray();

            msg.Show();

            SetMessageBoxSystemTray();
        }

        public void ShowNotification(string title, string message) {
            // Can't show ShellToasts in the foreground.

            this.ShowDismissible(
                String.IsNullOrWhiteSpace(title) ? R.Information : title,
                message
            );
        }

        public void ShowCustom(string title, string message, CustomDialogAction leftAction, CustomDialogAction rightAction) {
			var msg = new CustomMessageBox {
				Caption = title,
				Message = message
			};

		    if (leftAction != null) {
				msg.LeftButtonContent = leftAction.Content.ToLower();
		    }

		    if (rightAction != null) {
			    msg.RightButtonContent = rightAction.Content.ToLower();
		    }

			msg.Dismissed += (sender, args) => {
				UnsetMessageBoxSystemTray();

				if (args.Result == CustomMessageBoxResult.LeftButton && leftAction != null) {
					leftAction.TryTriggerOnChosen();
				}
				else if (args.Result == CustomMessageBoxResult.RightButton && rightAction != null) {
					rightAction.TryTriggerOnChosen();
				}
			};

			msg.Show();

			SetMessageBoxSystemTray();
	    }

        public async Task<CustomDialogResult> ShowCustomAsync(string title, string message, string positiveAction, string negativeAction) {
            var tcs = new TaskCompletionSource<CustomDialogResult>();

            Dispatcher.BeginInvoke(() => {
                var msg = new CustomMessageBox {
                    Caption = title,
                    Message = message,

                    LeftButtonContent = positiveAction.ToLower(),
                    RightButtonContent = negativeAction.ToLower()
                };

                msg.Dismissed += (sender, args) => {
                    UnsetMessageBoxSystemTray();

                    tcs.TrySetResult(
                        args.Result == CustomMessageBoxResult.LeftButton
                            ? new CustomDialogResult(true)
                            : new CustomDialogResult(false)
                    );
                };

                msg.Show();

                SetMessageBoxSystemTray();
            });

            return await tcs.Task;
        }

        #region System Tray

        private static double _sysTrayOpacity;
        private static bool _sysTrayVisibility;
        private static Color _sysTrayForegroundColor;
        private static Color _sysTrayBackgroundColor;

        /// <summary>
        /// Sets the system tray style for display
        /// with a visible <see cref="CustomMessageBox" />.
        /// </summary>
        public static void SetMessageBoxSystemTray() {
            DependencyObject element = ((Frame)Application.Current.RootVisual).Content as PhoneApplicationPage;

            _sysTrayOpacity = SystemTray.GetOpacity(element);
            _sysTrayVisibility = SystemTray.GetIsVisible(element);
            _sysTrayForegroundColor = SystemTray.GetForegroundColor(element);
            _sysTrayBackgroundColor = SystemTray.GetBackgroundColor(element);

            SystemTray.SetOpacity(element, 0.99);
            SystemTray.SetIsVisible(element, true);
            SystemTray.SetForegroundColor(element, Colors.White);
            SystemTray.SetBackgroundColor(element, (Color)Application.Current.Resources["ColorAccent"]);
        }

        /// <summary>
        /// Returns the system tray styles to
        /// before a <see cref="CustomMessageBox" /> was visible.
        /// </summary>
        public static void UnsetMessageBoxSystemTray() {
            DependencyObject element = ((Frame)Application.Current.RootVisual).Content as PhoneApplicationPage;

            SystemTray.SetOpacity(element, _sysTrayOpacity);
            SystemTray.SetIsVisible(element, _sysTrayVisibility);
            SystemTray.SetForegroundColor(element, _sysTrayForegroundColor);
            SystemTray.SetBackgroundColor(element, _sysTrayBackgroundColor);
        }

        #endregion
    }
}
