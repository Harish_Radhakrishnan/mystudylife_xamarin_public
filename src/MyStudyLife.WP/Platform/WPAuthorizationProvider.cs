﻿using System;
using System.Threading.Tasks;
using Microsoft.Phone.Info;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.WP.Views;

namespace MyStudyLife.WP.Platform {

    /// <summary>
    /// The authorization provider.
    /// </summary>
    public class WPAuthorizationProvider : IAuthorizationProvider {
        #region Instance Creation

        private readonly IStorageProvider _storageProvider;

        public WPAuthorizationProvider(IStorageProvider storageProvider) {
            if (storageProvider == null)
                throw new ArgumentNullException(nameof(storageProvider));

            this._storageProvider = storageProvider;
        }

        #endregion

        #region Device Information

        public Task<Device> GetDeviceInformationAsync() {
            return System.Threading.Tasks.Task.FromResult(new Device {
	            Manufacturer = DeviceStatus.DeviceManufacturer,
	            Model = DeviceStatus.DeviceName,
	            HardwareId = this.GetHardwareId(),
	            OSVersion = Environment.OSVersion.Version.ToString()
            });
        }

        // Note: to get a result requires ID_CAP_IDENTITY_DEVICE  
        // to be added to the capabilities of the WMAppManifest  
        // this will then warn users in marketplace  
        private string GetHardwareId() {
            byte[] result = null;
            object uniqueId;

            if (DeviceExtendedProperties.TryGetValue("DeviceUniqueId", out uniqueId))
                result = (byte[])uniqueId;

            if (result != null)
                return Convert.ToBase64String(result);

            return null;
        }

        #endregion

        #region OAuth

        public Task<OAuthAuthorizationResult> PerformOAuthAuthorizationAsync(AuthenticationProvider provider) {
            return SignInView.Current.PerformOAuthAuthorizationAsync(provider);
        }

		#endregion
    }
}
