﻿using System;
using System.Linq;
using Microsoft.Phone.Scheduler;
using MyStudyLife.Reminders;
using Reminder = MyStudyLife.Reminders.Reminder;

namespace MyStudyLife.WP.Platform {
	public sealed class WPReminderScheduler : IReminderScheduler {
        private static object m_lock = new object();

		/// <summary>
		///     Schedules a reminder to be displayed to a user at a future point in time.
		/// </summary>
		/// <returns>
		///     True if successful. Can be false if reminders are disabled by the device.
		/// </returns>
		public bool ScheduleReminder(Reminder reminder) {
            // See http://blogs.codes-sources.com/kookiz/archive/2011/10/22/wp7-scheduledactionservice-replace-bug-or-documentation-error.aspx if you're confused by the below!

		    lock (m_lock) {
		        string name = reminder.GetUniqueName();
		        string title = reminder.Title.Length >= 63
		            ? String.Concat(reminder.Title.Substring(0, 60).Trim(), "...")
		            : reminder.Title;
                string content = (
                    reminder.Message != null && reminder.Message.Length >= 253
		                ? reminder.Message.Substring(0, 253).TrimEnd(' ', '-')
		                : reminder.Message
                ) ?? String.Empty;

		        // TODO: Add navigation URI
		        try {
		            ScheduledNotification pReminder;

                    if ((pReminder = ScheduledActionService.Find(name) as ScheduledNotification) != null) {
                        pReminder.Title = title;
                        pReminder.Content = content;
                        pReminder.BeginTime = reminder.Time;

		                ScheduledActionService.Replace(pReminder);
		            }
		            else {
		                ScheduledActionService.Add(new Microsoft.Phone.Scheduler.Reminder(name) {
		                    Title = title,
                            Content = content,
                            BeginTime = reminder.Time
		                });
		            }
		        }
		        catch (InvalidOperationException) {
		            return false;
		        }

		        return true;
		    }
		}

		public void ClearReminders() {
		    lock (m_lock) {
		        var reminders = ScheduledActionService.GetActions<Microsoft.Phone.Scheduler.Reminder>().ToList();

		        foreach (var reminder in reminders) {
		            try {
		                ScheduledActionService.Remove(reminder.Name);
		            }
		            catch (InvalidOperationException) {}
		        }
		    }
		}
	}
}
