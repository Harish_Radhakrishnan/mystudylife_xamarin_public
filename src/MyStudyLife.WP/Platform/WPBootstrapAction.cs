﻿using System.Threading.Tasks;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.WP.Services;

namespace MyStudyLife.WP.Platform {
    public class WPBootstrapAction : IBootstrapAction {
        public System.Threading.Tasks.Task RunAsync() {
            return System.Threading.Tasks.Task.Run(() => BackgroundTaskRegistrationService.EnsureTask());
        }
    }
}
