﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Exceptions;
using Cirrious.CrossCore.Platform;
using Microsoft.Phone.Notification;
using MyStudyLife.Net;
using MyStudyLife.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.WP.Platform {
    public class WPPushNotificationService : PushNotificationServiceBase, IPushNotificationService {
        public const string SyncChannelName = "MSL_SYNC";
        
        private readonly IHttpApiClient _apiClient;

        private HttpNotificationChannel _syncChannel;

        public WPPushNotificationService(IHttpApiClient apiClient) {
            this._apiClient = apiClient;
        }
        
        /// <summary>
        /// Ensures there is an active notification channel
        /// for the current user.
        /// </summary>
        public Task EnsureNotificationChannelAsync() {
            if (_syncChannel != null) {
                return TaskEx.NoOp;
            }

            try {
                _syncChannel = HttpNotificationChannel.Find(SyncChannelName) ?? new HttpNotificationChannel(SyncChannelName);

                _syncChannel.ChannelUriUpdated += async (sender, args) => {
                    try {
                        string jsonContent = JsonConvert.SerializeObject(new Dictionary<string, string> {
                            {"uri", args.ChannelUri.ToString()}
                        });

                        await _apiClient.PostJsonAsync("push/mpns", jsonContent);
                    }
                    catch (Exception ex) {
                        Mvx.TaggedError("PNS", "Failed to register notification channel with MSL: " + ex.ToLongString());
                    }
                };

                _syncChannel.ErrorOccurred += (sender, args) =>
                    Debug.WriteLine("PushNotificationService: An error {0} occured whilst ensuring an active channel: {1}.", args.ErrorType, args.Message);

                _syncChannel.HttpNotificationReceived += ChannelOnNotificationReceived;
                
                _syncChannel.Open();
            }
            catch (Exception ex) {
                Mvx.TaggedError("PNS", "Failed to open notification channel: " + ex.ToLongString());
                _syncChannel = null;
            }

            return TaskEx.NoOp;
        }

        /// <summary>
        /// Closes the current notification channel.
        /// 
        /// Should be called before the user logs out. When a user signs out
        /// all channels will automatically be closed from the signout auth method.
        /// </summary>
        public Task CloseNotificationChannelAsync() {
            _syncChannel?.Close();
            return TaskEx.NoOp;
        }

        protected void ChannelOnNotificationReceived(object sender, HttpNotificationEventArgs e) {
            if (e.Notification.Channel.ChannelName != SyncChannelName) {
                Trace(MvxTraceLevel.Diagnostic, $"Received message on unknown channel '{SyncChannelName}'");
                return;
            }

            if (e.Notification.Body == null || e.Notification.Headers == null) return;

            using (StreamReader reader = new StreamReader(e.Notification.Body)) {
                var base64EncodedContent = reader.ReadToEnd();
                var contentBytes = Convert.FromBase64String(base64EncodedContent);
                var content = System.Text.Encoding.UTF8.GetString(contentBytes, 0, contentBytes.Length);
                
                // WARNING: If we send nested objects in the notification JSON, this won't work!
                HandleRawNotification(JsonConvert.DeserializeObject<Dictionary<string, object>>(content));
            }
        }
    }
}
