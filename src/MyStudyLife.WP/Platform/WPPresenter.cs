﻿using System;
using System.Linq;
using System.Windows.Navigation;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.ViewModels;
using Cirrious.MvvmCross.WindowsPhone.Views;
using Microsoft.Phone.Controls;
using MyStudyLife.Data;
using MyStudyLife.UI;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.WP.Common;
using MyStudyLife.WP.Services;
using MyStudyLife.WP.Views;

namespace MyStudyLife.WP.Platform {
	public class WPPresenter : MvxPhoneViewPresenter, IMvxViewPresenterWithDelegate {
	    private readonly INavigationService _navigationService;

        #region IMvxViewPresenterWithDelegate

        private readonly MvxViewPresenterDelegate _delegate = new MvxViewPresenterDelegate();

        public MvxViewPresenterDelegate Delegate {
            get { return _delegate; }
        }

        #endregion

		public IMvxViewModel CurrentViewModel {
			get { return ((MslAppView)RootFrame.Content).DataContext as IMvxViewModel; }
		}

	    public WPPresenter(PhoneApplicationFrame rootFrame) : base(rootFrame) {
	        rootFrame.Navigating += RootFrameOnNavigating;

	        _navigationService = Mvx.Resolve<INavigationService>();

            this.AddPresentationHintHandler<WelcomeWizardCompletedPresentationHint>(hint => {
                // In WP, we show a popup in the dashboard after the welcome wizard is completed
                this.Show(MvxViewModelRequest<DashboardViewModel>.GetDefaultRequest());

                if (RootFrame.CanGoBack) {
                    RootFrame.RemoveBackEntry();
                }
            });
            this.AddPresentationHintHandler<EntityDeletedPresentationHint>(hint =>
                UIHelper.AnimateViewDeletion(RootFrame.Content as MslAppView, () => base.ChangePresentation(hint))
            );
            this.AddPresentationHintHandler<UpgradeCompletePresentationHint>(hint => {
                RootFrame.RemoveBackEntry();

                UpgradeViewModel.AfterPresentationHintHandledAsync(hint);
            });
            this.AddPresentationHintHandler<AuthorizationChangedPresentationHint>(hint => {
                while (RootFrame.BackStack.Any() && !(RootFrame.Content is SignInView)) {
                    RootFrame.RemoveBackEntry();
                }

                new LiveTileService(Mvx.Resolve<IStorageProvider>()).ClearPrimaryTile();

                _navigationService.BackStack.Clear();

                this.Show(new MvxViewModelRequest {
                    ViewModelType = typeof(SignInViewModel),
                    RequestedBy = new MvxRequestedBy(MvxRequestedByType.UserAction)
                });
            });
	    }

	    private void RootFrameOnNavigating(object sender, NavigatingCancelEventArgs e) {
            if (e.NavigationMode == NavigationMode.Back) {
                var view = RootFrame.Content as MslAppView;

                if (view != null && view.ViewModel != null) {
                    var viewModelType = ((Object)view.ViewModel).GetType();

                    _navigationService.BackStack.Pop(viewModelType);
                }
                else {
                    _navigationService.BackStack.Pop();
                }
	        }
	    }

	    public override void Show(MvxViewModelRequest request) {
	        var transaction = _navigationService.BackStack.BeginTransaction();
	        base.Show(request);
            transaction.Push(request.ViewModelType);
            transaction.Commit();
	    }

	    public override void ChangePresentation(MvxPresentationHint hint) {
	        if (!Delegate.HandleChangePresentation(hint)) {
                base.ChangePresentation(hint);
	        }
		}
	}
}