﻿using System;
using System.Collections;
using System.Collections.Generic;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.WP.Platform {
	public class WPBugReporter : BugReporter {
        protected override void SetUserImpl(string uid) {
			App.Current.RaygunClient.User = uid;
		}

		protected override void SendImpl(Exception exception, IList<string> tags, IDictionary customUserData) {
			App.Current.RootFrame.Dispatcher.BeginInvoke(() =>
				App.Current.RaygunClient.Send(exception, tags, customUserData));
		}
	}
}
