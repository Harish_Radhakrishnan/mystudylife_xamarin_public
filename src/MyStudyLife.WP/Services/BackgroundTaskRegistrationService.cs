﻿using System;
using System.Diagnostics;
using System.Windows.Threading;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Core;
using Microsoft.Phone.Scheduler;
using MyStudyLife.UI.Services;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.WP.Services {
    public static class BackgroundTaskRegistrationService {
        public const string LiveTileAgentName = "MSL_LTA";

        /// <summary>
        /// Ensures the My Study Life background task
        /// is registered in the phone. Shows error messages
        /// if task has been disabled or if the limit of
        /// background tasks allowed has been reached. 
        /// </summary>
        /// <param name="attemptLaunch">
        /// True to attempt to launch the background task.
        /// </param>
        public static void EnsureTask(bool attemptLaunch = false) {
            PeriodicTask liveTileAgent = ScheduledActionService.Find(LiveTileAgentName) as PeriodicTask;
            
            try {
                // The background task expires every 2 weeks. Therefore we shouldn't just be checking
                // the expiration time, we should be removing and creating a new one every time
                // the app starts to it lasts 2 weeks after the app starts.
                if (liveTileAgent != null) {
                    ScheduledActionService.Remove(LiveTileAgentName);
                }

                liveTileAgent = new PeriodicTask(LiveTileAgentName) {
                    Description =
                        "Periodic task to keep My Study Life's live tiles up to date and set reminders."
                };

                ScheduledActionService.Add(liveTileAgent);
            }
            catch (InvalidOperationException ex) {
                if (ex.Message.Contains("BNS Error: The action is disabled")) {
                    MvxMainThreadDispatcher.Instance.RequestMainThreadAction(() => 
                        Mvx.Resolve<IDialogService>().ShowDismissible(
                            "Background tasks",
                            "Background tasks for My Study Life have been disabled, to use reminders and live tiles please enable in your phone's settings."
                        )
                    );
                }
            }
            catch (SchedulerServiceException) {
                MvxMainThreadDispatcher.Instance.RequestMainThreadAction(() => 
				    Mvx.Resolve<IDialogService>().ShowDismissible(
                        "Background tasks",
                        "You have reached the maximum limit of background tasks allowed, to use reminders and live tiles" +
                        " please remove one or more background tasks in your phone's settings."
                    )
                );
            }
            finally {
                try {
#if RELEASE
                    if (attemptLaunch)
#endif
                    ScheduledActionService.LaunchForTest(LiveTileAgentName, TimeSpan.FromMilliseconds(1500));
                }
// ReSharper disable EmptyGeneralCatchClause, RedundantCatchClause
                catch {
#if DEBUG
                    if (Debugger.IsAttached) {
                        Debugger.Break();
                    }
#endif
                }
// ReSharper restore RedundantCatchClause, EmptyGeneralCatchClause
            }
        }

        /// <summary>
        /// Attempts to launch the background task
        /// using the <see cref="ScheduledActionService.LaunchForTest"/>
        /// method.
        /// </summary>
        [Conditional("DEBUG")]
        public static void AttemptLaunch() {
            try {
                ScheduledActionService.LaunchForTest(LiveTileAgentName, TimeSpan.FromMilliseconds(1500));
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch { }
            // ReSharper restore EmptyGeneralCatchClause
        }
    }
}
