﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Cirrious.CrossCore;
using MyStudyLife.UI.Tasks;

namespace MyStudyLife.WP.Common {
    public static class AutoLinker {
        private static FontFamily _hyperlinkFontFamily;

        static AutoLinker() {
            _hyperlinkFontFamily = (FontFamily) App.Current.Resources["PhoneFontFamilySemiBold"];
        }

        public static SolidColorBrush GetHyperlinkBrush(DependencyObject d) => (SolidColorBrush)d.GetValue(HyperlinkBrush);
        public static void SetHyperlinkBrush(DependencyObject d, SolidColorBrush brush) => d.SetValue(HyperlinkBrush, brush);

        public static readonly DependencyProperty HyperlinkBrush = DependencyProperty.RegisterAttached(
            "HyperlinkBrush", typeof(SolidColorBrush), typeof(AutoLinker), new PropertyMetadata((SolidColorBrush)App.Current.Resources["BrushAccent"], OnHyperlinkBrushChanged));

        public static void OnHyperlinkBrushChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var textBlock = d as RichTextBox;

            if (textBlock?.Blocks == null) {
                return;
            }

            var hyperlinks = textBlock.Blocks.OfType<Hyperlink>().ToList();

            foreach (var p in textBlock.Blocks.OfType<Paragraph>()) {
                hyperlinks.AddRange(p.Inlines.OfType<Hyperlink>());
            }

            if (hyperlinks != null) {
                hyperlinks.Apply(x => {
                    x.Foreground = (SolidColorBrush)e.NewValue;
                    x.MouseOverForeground = (SolidColorBrush)e.NewValue;
                });
            }
        }

        public static string GetText(DependencyObject d) {
            return (string)d.GetValue(TextProperty);
        }

        public static void SetText(DependencyObject d, string text) {
            d.SetValue(TextProperty, text);
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.RegisterAttached(
            "Text", typeof(string), typeof(AutoLinker), new PropertyMetadata(null, OnTextChanged));

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var textBlock = d as RichTextBox;

            if (textBlock == null) {
                return;
            }

            var text = e.NewValue as string;
            textBlock.Blocks.Clear();

            if (String.IsNullOrEmpty(text)) {
                return;
            }

            var matches = UI.Helpers.AutoLinker.GetMatches(text).ToList();

            var paragraph = new Paragraph();

            if (matches.Count == 0) {
                paragraph.Inlines.Add(new Run {
                    Text = text
                });
                textBlock.Blocks.Add(paragraph);
                return;
            }

            var hyperlinkBrush = GetHyperlinkBrush(d);
            int lastMatchEndIndex = 0;

            for (int i = 0; i < matches.Count; i++) {
                var match = matches[i];

                if (match.StartIndex > 0) {
                    paragraph.Inlines.Add(new Run {
                        Text = text.Substring(lastMatchEndIndex, match.StartIndex - lastMatchEndIndex)
                    });
                }

                var hyperlink = new Hyperlink {
                    NavigateUri = match.Uri,
                    Foreground = hyperlinkBrush,
                    TextDecorations = null,
                    MouseOverForeground = hyperlinkBrush,
                    MouseOverTextDecorations = null,
                    FontFamily = _hyperlinkFontFamily
                };
                hyperlink.Click += HyperlinkOnClick;

                hyperlink.Inlines.Add(new Run {
                    Text = match.Text
                });

                paragraph.Inlines.Add(hyperlink);

                lastMatchEndIndex = match.EndIndex;

                if (i == (matches.Count - 1)) {
                    if (lastMatchEndIndex < (text.Length - 1)) {
                        paragraph.Inlines.Add(new Run {
                            Text = text.Substring(lastMatchEndIndex, text.Length - lastMatchEndIndex)
                        });
                    }
                }
            }
            
            textBlock.Blocks.Add(paragraph);
        }

        private static void HyperlinkOnClick(object sender, RoutedEventArgs e) {
            Mvx.Resolve<ITaskProvider>().ShowWebUri(((Hyperlink)sender).NavigateUri);
        }
    }
}
