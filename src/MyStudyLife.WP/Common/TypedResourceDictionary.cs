﻿using System.Windows;
using System.Windows.Media;

namespace MyStudyLife.WP.Common {
    public class TypedResourceDictionary {
        private ResourceDictionary _resources;

        public TypedResourceDictionary(ResourceDictionary resources) {
            _resources = resources;
        }

        public T Get<T>(string key) {
            return (T)_resources[key];
        }

        private T CachedValue<T>(string key, ref T value) where T : class {
            return value ?? (value = (T)_resources[key]);
        }


        #region Glyphs

        private string _glyphAttention;
        private string _glyphTick;

        public string GlyphAttention {
            get { return CachedValue("AttentionGlyph", ref _glyphAttention); }
        }
        public string GlyphTick {
            get { return CachedValue("TickGlyph", ref _glyphTick); }
        }

        #endregion

        #region Colors/Brushes

        private SolidColorBrush _brushForeground;
        private SolidColorBrush _brushAttention;
        private SolidColorBrush _brushWarning;
        private SolidColorBrush _brushOkay;

        public SolidColorBrush BrushForeground {
            get { return CachedValue("BrushForeground", ref _brushForeground); }
        }
        public SolidColorBrush BrushAttention {
            get { return CachedValue("BrushAttentionForeground", ref _brushAttention); }
        }
        public SolidColorBrush BrushWarning {
            get { return CachedValue("BrushWarningForeground", ref _brushWarning); }
        }
        public SolidColorBrush BrushOkay {
            get { return CachedValue("MyStudyLifeOkayBrush", ref _brushOkay); }
        }

        #endregion
    }
}
