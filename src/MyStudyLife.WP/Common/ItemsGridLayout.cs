﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MyStudyLife.WP.Common {
    public static class ItemsGridLayout {
    
        // Using a DependencyProperty as the backing store for GridRow.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridRowProperty =
            DependencyProperty.RegisterAttached("GridRow", typeof(int), typeof(ItemsGridLayout), new PropertyMetadata(0, (s, e) => SetRowWhenLoaded(s, (int)e.NewValue)));

        public static int GetGridRow(DependencyObject obj) {
            return (int)obj.GetValue(GridRowProperty);
        }

        public static void SetGridRow(DependencyObject obj, int value) {
            obj.SetValue(GridRowProperty, value);
        }

        // Using a DependencyProperty as the backing store for GridColumn.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GridColumnProperty =
            DependencyProperty.RegisterAttached("GridColumn", typeof(int), typeof(ItemsGridLayout), new PropertyMetadata(0, (s, e) => SetColumnWhenLoaded(s, (int) e.NewValue)));

        public static int GetGridColumn(DependencyObject obj) {
            return (int)obj.GetValue(GridColumnProperty);
        }

        public static void SetGridColumn(DependencyObject obj, int value) {
            obj.SetValue(GridColumnProperty, value);
        }

        private static void SetColumnWhenLoaded(DependencyObject target, int column) {
            if (target != null) {
                ((FrameworkElement)target).Loaded += (s, e) => Grid.SetColumn(GetItemsPresenter(target), column);
            }
        }

        private static void SetRowWhenLoaded(DependencyObject target, int row) {
            if (target != null) {
                ((FrameworkElement)target).Loaded += (s, e) => Grid.SetRow(GetItemsPresenter(target), row);
            }
        }

        private static FrameworkElement GetItemsPresenter(DependencyObject target) {
            while (target != null) {
                if (target is ContentPresenter) {
                    return target as FrameworkElement;
                }
                target = VisualTreeHelper.GetParent(target);
            }
            return null;
        }
    }
}
