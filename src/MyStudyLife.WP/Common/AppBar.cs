﻿using System;
using System.Windows.Input;
using System.Windows.Media;
using Microsoft.Phone.Shell;

namespace MyStudyLife.WP.Common {
    public enum AppBarButtonKind {
        New,
        Edit,
        Delete,
        Save,
        Cancel,
        Select,
        Check,
        Pin,
        Sync,
        Previous,
        Next,
        Search,
        Today,
        ToggleWeek,
        Calendar
    }

    /// <summary>
    /// A class containing helper methods
    /// and properties for the application bar.
    /// </summary>
    [System.Diagnostics.DebuggerStepThrough]
    public static class AppBarHelper {
        private static Color? _colorAccent;

        private static Color ColorAccent {
            get { return (_colorAccent ?? (_colorAccent = (Color) App.Current.Resources["ColorAccent"])).Value; }
        }

        #region Icon Uris

        /// <summary>
        /// The directory in which the icons are stored.
        /// </summary>
        private const string BaseIconUri = "/Assets/AppBar/";

        #endregion

        public static IApplicationBar CreateDefaultAppBar() {
            return new ApplicationBar {
                BackgroundColor = ColorAccent,
                ForegroundColor = Colors.White,
                Opacity = .99 // Fixes flicker bug by forcing render of the background
            };
        }

        public static AppBarIconButton CreateButton(AppBarButtonKind kind, string text = null) {
            return new AppBarIconButton {
                IconUri = GetIconUri(kind),
                Text = !String.IsNullOrWhiteSpace(text) ? text.ToLower() : GetDefaultText(kind)
            };
        }

        /// <summary>
        /// Gets the URI of the icon for the given
        /// <see cref="AppBarButtonKind"/>.
        /// </summary>
        /// <param name="kind">The kind of button.</param>
        /// <returns>The URI of the button's icon.</returns>
        public static Uri GetIconUri(AppBarButtonKind kind) {
            string uri = String.Format("{0}{1}.png", BaseIconUri, kind);

            return new Uri(uri, UriKind.Relative);
        }

        /// <summary>
        ///     Adds a button to the app bar, ensuring it doesn't
        ///     already exist in the buttons collection.
        /// </summary>
        public static void AddButton(this IApplicationBar appBar, ApplicationBarIconButton button) {
            if (!appBar.Buttons.Contains(button)) {
                appBar.Buttons.Add(button);
            }
        }

        /// <summary>
        ///     Shorthand for Buttons.Remove
        /// </summary>
        public static void RemoveButton(this IApplicationBar appBar, ApplicationBarIconButton button) {
            appBar.Buttons.Remove(button);
        }

        /// <summary>
        ///     Adds a menu item to the app bar, ensuring it doesn't
        ///     already exist in the menu items collection.
        /// </summary>
        public static void AddMenuItem(this IApplicationBar appBar, ApplicationBarMenuItem item) {
            if (!appBar.MenuItems.Contains(item)) {
                appBar.MenuItems.Add(item);
            }
        }

        /// <summary>
        ///     Shorthand for MenuItems.Remove
        /// </summary>
        public static void RemoveMenuItem(this IApplicationBar appBar, ApplicationBarMenuItem item) {
            appBar.MenuItems.Remove(item);
        }

        /// <summary>
        ///     Inserts a menu item to the app bar, ensuring it doesn't
        ///     already exist in the menu items collection.
        /// </summary>
        public static void InsertMenuItem(this IApplicationBar appBar, int index, ApplicationBarMenuItem item) {
            if (!appBar.MenuItems.Contains(item)) {
                appBar.MenuItems.Insert(index, item);
            }
        }

        #region Privates

        private static string GetDefaultText(AppBarButtonKind kind) {
            return kind.ToString().ToLower();
        }

        #endregion
    }


    public class AppBarMenuItem : ApplicationBarMenuItem {
        private ICommand _command;

        public ICommand Command {
            get { return _command; }
            set {
                if (_command != null) {
                    _command.CanExecuteChanged -= CommandOnCanExecuteChanged;
                }

                _command = value;

                if (_command != null) {
                    _command.CanExecuteChanged += CommandOnCanExecuteChanged;
                }
            }
        }

        public object CommandParameter { get; set; }

        private void CommandOnCanExecuteChanged(object sender, EventArgs e) {
            this.IsEnabled = this.Command.CanExecute(this.CommandParameter);
        }

        public AppBarMenuItem(string text = null) : base(text) {
            this.Click += OnClick;
        }

        private void OnClick(object sender, EventArgs e) {
            if (this.Command != null && this.IsEnabled) {
                this.Command.Execute(this.CommandParameter);
            }
        }
    }

    public class AppBarIconButton : ApplicationBarIconButton {
        private ICommand _command;

        public ICommand Command {
            get { return _command; }
            set {
                if (_command != null) {
                    _command.CanExecuteChanged -= CommandOnCanExecuteChanged;
                }

                _command = value;

                if (_command != null) {
                    _command.CanExecuteChanged += CommandOnCanExecuteChanged;
                }
            }
        }

        public object CommandParameter { get; set; }

        private void CommandOnCanExecuteChanged(object sender, EventArgs e) {
            this.IsEnabled = this.Command.CanExecute(this.CommandParameter);
        }

        public AppBarIconButton() {
            this.Click += OnClick;
        }

        private void OnClick(object sender, EventArgs e) {
            if (this.Command != null && this.IsEnabled) {
                this.Command.Execute(this.CommandParameter);
            }
        }
    }


}
