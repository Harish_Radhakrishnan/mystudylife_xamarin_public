﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace MyStudyLife.WP.Common {
    public static class Effects {
        #region Fade In

        public static readonly DependencyProperty IsFadeInEnabledProperty = DependencyProperty.RegisterAttached(
            "IsFadeInEnabled", typeof (bool), typeof (Effects), new PropertyMetadata(false, OnIsFadeInEnabledChanged));
            
        public static bool GetIsFadeInEnabled(DependencyObject d) {
            return (bool) d.GetValue(IsFadeInEnabledProperty);
        }

        public static void SetIsFadeInEnabled(DependencyObject d, bool value) {
            d.SetValue(IsFadeInEnabledProperty, value);
        }

        private static void OnIsFadeInEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var el = d as FrameworkElement;

            if (el == null) {
			    return;
			}

            if ((bool) e.NewValue) {
                el.Opacity = 0;
                el.Loaded += FadeInEffectOnLoaded;
            }
            else {
                el.Loaded -= FadeInEffectOnLoaded;
            }
        }

        private static void FadeInEffectOnLoaded(object sender, RoutedEventArgs e) {
            var el = (FrameworkElement) sender;

            var sb = new Storyboard();
            var animation = new DoubleAnimation {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromSeconds(0.3))
            };

            Storyboard.SetTarget(animation, el);
            Storyboard.SetTargetProperty(animation, new PropertyPath("Opacity"));

            sb.Children.Add(animation);
            
            sb.Begin();
        }

        #endregion
    }
}
