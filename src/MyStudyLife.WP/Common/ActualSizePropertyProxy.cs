﻿using System.ComponentModel;
using System.Windows;

namespace MyStudyLife.WP.Common {
	public class ActualSizePropertyProxy : FrameworkElement, INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged;

		public FrameworkElement Element {
			get { return (FrameworkElement)GetValue(ElementProperty); }
			set { SetValue(ElementProperty, value); }
		}

		public double ActualHeightValue {
			get { return Element == null ? 800 : Element.ActualHeight; }
		}

		public double ActualWidthValue {
			get { return Element == null ? 480 : Element.ActualWidth; }
		}

		public static readonly DependencyProperty ElementProperty = DependencyProperty.Register(
			"Element",
			typeof(FrameworkElement),
			typeof(ActualSizePropertyProxy),
			new PropertyMetadata(null, OnElementPropertyChanged)
		);

		private static void OnElementPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
			((ActualSizePropertyProxy)d).OnElementChanged(e);
		}

		private void OnElementChanged(DependencyPropertyChangedEventArgs e) {
			FrameworkElement oldElement = (FrameworkElement)e.OldValue;
			FrameworkElement newElement = (FrameworkElement)e.NewValue;

			newElement.SizeChanged += Element_SizeChanged;
			if (oldElement != null) {
				oldElement.SizeChanged -= Element_SizeChanged;
			}
			RaisePropertyChanged();
		}

		private void Element_SizeChanged(object sender, SizeChangedEventArgs e) {
			RaisePropertyChanged();
		}

		private void RaisePropertyChanged() {
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs("ActualWidthValue"));
				PropertyChanged(this, new PropertyChangedEventArgs("ActualHeightValue"));
			}
		}
	}
}
