﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Microsoft.Phone.Controls;

namespace MyStudyLife.WP.Common {
    /// <summary>
    /// Attached properties that help maintain the MVVM pattern.
    /// </summary>
    public static class MvvmHelper {
        #region Selected Items

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(ObservableCollection<object>), typeof(LongListMultiSelector),
                                        new PropertyMetadata(new ObservableCollection<object>(), OnSelectedItemsChanged));

        /// <summary>
        /// Gets the SelectedItems property.
        /// </summary>
        /// <param name="d">The <see cref="DependencyObject"/> to get the property from.</param>
        /// <returns>The value of the SelectedItems property.</returns>
        public static ObservableCollection<object> GetSelectedItems(DependencyObject d) {
            return (ObservableCollection<object>)d.GetValue(SelectedItemsProperty);
        }

        /// <summary>
        /// Sets the SelectedItems property.
        /// </summary>
        /// <param name="d">The <see cref="DependencyObject"/> to set the property on.</param>
        /// <param name="value">The value to set the selected items property to.</param>
        public static void SetSelectedItems(DependencyObject d, ObservableCollection<object> value) {
            d.SetValue(SelectedItemsProperty, value);
        }

        private static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var multiselectList = (LongListMultiSelector)d;
            multiselectList.SelectionChanged += MultiselectListOnSelectionChanged;

            var selectedItems = GetSelectedItems(d);

            if (selectedItems != null) {
                selectedItems.Clear();

                foreach (var item in multiselectList.SelectedItems) {
                    selectedItems.Add(item);
                }
            }
        }

        private static void MultiselectListOnSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var mutliselectList = sender as LongListMultiSelector;

            if (mutliselectList == null)
                return;

            var selectedItems = GetSelectedItems(mutliselectList);

            if (selectedItems != null) {
                foreach (var item in e.RemovedItems) {
                    selectedItems.Remove(item);
                }

                foreach (var item in e.AddedItems) {
                    selectedItems.Add(item);
                }
            }
        }

        #endregion
    }
}
