﻿using System;
using System.IO.IsolatedStorage;
using System.Windows;
using System.Windows.Media.Imaging;

namespace MyStudyLife.WP.Common {
    public static class ImageEx {
        public static DependencyProperty LocalStorageUriSource = DependencyProperty.RegisterAttached(
            "LocalStorageUriSource", typeof(string), typeof(ImageEx), new PropertyMetadata(null, OnPathChanged));

        public static string GetLocalStorageUriSource(DependencyObject d) => (string)d.GetValue(LocalStorageUriSource);
        public static void SetLocalStorageUriSource(DependencyObject d, string text) => d.SetValue(LocalStorageUriSource, text);

        private static void OnPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) {
            var image = d as BitmapImage;

            if (image == null) {
                return;
            }

            var path = (string)e.NewValue;

            if (path == null) {
                image.UriSource = null;
            }
            else {
                using (var stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile(path, System.IO.FileMode.Open)) {
                    image.SetSource(stream);
                }
            }
        }
    }
}
