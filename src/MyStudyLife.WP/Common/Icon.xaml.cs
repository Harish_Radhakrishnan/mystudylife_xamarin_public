﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MyStudyLife.WP.Common {
    public sealed partial class Icon : UserControl {
        private const double BaseFontSize = 16d;

        #region FontSize

        public static readonly new DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(Icon), new PropertyMetadata(BaseFontSize, OnFontSizeChanged));

        public new double FontSize {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        private static void OnFontSizeChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e) {
            var ctrl = (Icon)sender;

            var nFontSize = (double)e.NewValue;
            var scale = nFontSize / BaseFontSize;

            ctrl.MinWidth = nFontSize;
            ctrl.MinHeight = nFontSize;

            if (ctrl.Root.LayoutTransform as ScaleTransform == null) {
                ctrl.Root.LayoutTransform = new ScaleTransform();
            }

            ((ScaleTransform)ctrl.Root.LayoutTransform).ScaleX = scale;
            ((ScaleTransform)ctrl.Root.LayoutTransform).ScaleY = scale;
        }

        #endregion

        #region PathData

        public static readonly DependencyProperty PathDataProperty =
            DependencyProperty.Register("PathData", typeof(string), typeof(Icon), new PropertyMetadata(null));

        public string PathData {
            get { return (string)GetValue(PathDataProperty); }
            set { SetValue(PathDataProperty, value); }
        }

        #endregion

        public Icon() {
            this.InitializeComponent();
        }
    }
}
