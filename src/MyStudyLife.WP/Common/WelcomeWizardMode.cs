﻿namespace MyStudyLife.WP.Common {

    /// <summary>
    /// The mode to display
    /// the wizard in.
    /// </summary>
    public enum WelcomeWizardMode {
        /// <summary>
        /// The user has never used 
        /// My Study Life for Windows Phone before.
        /// </summary>
        NewUser,
        /// <summary>
        /// The user has used 
        /// My Study Life for Windows Phone
        /// prior to V3 and is not signed in.
        /// </summary>
        ExistingUser,
        /// <summary>
        /// The user has used
        /// My Study Life for Windows Phone
        /// prior to V3 and is signed in.
        /// </summary>
        DataUpgrade
    }
}
