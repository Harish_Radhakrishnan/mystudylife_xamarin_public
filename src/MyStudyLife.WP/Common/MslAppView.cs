﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Navigation;
using System.Windows.Threading;
using Cirrious.CrossCore;
using Cirrious.MvvmCross.WindowsPhone.Views;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MyStudyLife.Net;
using MyStudyLife.Sync;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.WP.Common {
    public abstract class MslAppView : MvxPhonePage {
        public new IDictionary<string, object> State {
            get { return PhoneApplicationService.Current.State; }
        }

        /// <summary>
        /// Creates a new instance of the <see cref="MslAppView"/> class.
        /// </summary>
        protected MslAppView() {
            SetSystemTray();
            CreateAppBar();

            this.Foreground = (SolidColorBrush)App.Current.Resources["BrushForeground"];
            this.Background = (SolidColorBrush)App.Current.Resources["BrushBackground"];

            TiltEffect.TiltableItems.Add(typeof(MultiselectItem));

            this.Loaded += OnLoaded;
        }

        protected virtual void OnViewModelSet() {}

		private void OnLoaded(object sender, RoutedEventArgs e) {
			LoadProgressIndicator();
            OnViewModelSet();
		}

		private void OnUnloaded(object sender, RoutedEventArgs e) {
			UnloadAppBar();
			UnloadProgressIndicator();
		}

        #region App bar
        
        /// <summary>
        ///     True if the view automatically handles
        ///     the creation/destruction of the app bar.
        /// </summary>
        public virtual bool AutoAppBarCreation {
            get { return true; }
        }

        private void CreateAppBar() {
            if (this.AutoAppBarCreation) {
                var appBar = AppBarHelper.CreateDefaultAppBar();

				OnAppBarCreating(appBar);

	            this.ApplicationBar = appBar;

                this.Loaded += (s, e) => OnAppBarCreated(this.ApplicationBar);
                this.Unloaded += OnUnloaded;
            }
        }

		private void UnloadAppBar() {
			if (this.AutoAppBarCreation && this.ApplicationBar != null) {
				this.ApplicationBar.Buttons.Clear();
				this.ApplicationBar.MenuItems.Clear();
			}
	    }

		/// <summary>
		///		<para>
		///			Triggered just before the <paramref name="appBar" />
		///			is added to the view.
		///		</para>
		///		<para>
		///			Use this to set app bar styling and to subscribe to events.	
		///		</para>
		/// </summary>
	    protected virtual void OnAppBarCreating(IApplicationBar appBar) {}

		/// <summary>
		///		<para>
		///			Called after the <paramref name="appBar"/> is added
		///			to the view.
		///		</para>
		///		<para>
		///			Use this to add buttons/menu items.	
		///		</para>
		/// </summary>
		/// <remarks>
		///     Do not use this method to subscribe to events, it will
		///     be triggered every time the view is loaded.
		/// </remarks>
        protected virtual void OnAppBarCreated(IApplicationBar appBar) {}

        #endregion
		
        #region Progress Indicator

		// This has to be on a per-view basis.

	    private void LoadProgressIndicator() {
			var syncService = Mvx.Resolve<ISyncService>();

			syncService.StatusChanged += SyncServiceOnStatusChanged;
			syncService.Error += SyncServiceOnError;

			SetWhenSyncing(syncService.Status);
	    }

	    private void UnloadProgressIndicator() {
			var syncService = Mvx.Resolve<ISyncService>();

			syncService.StatusChanged -= SyncServiceOnStatusChanged;
			syncService.Error -= SyncServiceOnError;
	    }

		private DispatcherTimer _errorTimeout;

		private void SyncServiceOnStatusChanged(object sender, SyncStatusChangedEventArgs e) {
			Dispatcher.BeginInvoke(() => SetWhenSyncing(e.NewStatus));
		}

		private void SetWhenSyncing(SyncStatus status) {
			CancelErrorTimeout();

			var progressIndicator = EnsureProgressIndicator();

			progressIndicator.Text = "Syncing...";
			progressIndicator.IsIndeterminate = status == SyncStatus.Syncing;
			progressIndicator.IsVisible = status == SyncStatus.Syncing;
		}

		private void SyncServiceOnError(object sender, SyncErrorEventArgs e) {
			Dispatcher.BeginInvoke(() => {
				CancelErrorTimeout();

				var progressIndicator = EnsureProgressIndicator();

				progressIndicator.IsIndeterminate = false;

				if (!(e.Exception is HttpApiUnauthorizedException)) {
					progressIndicator.IsVisible = true;

					progressIndicator.Text = ((ISyncService) sender).Status == SyncStatus.Offline
						? "Sync failed: offline"
						: "Sync failed: unknown error";

					_errorTimeout = new DispatcherTimer {
						Interval = TimeSpan.FromSeconds(5.0)
					};
					_errorTimeout.Tick += ErrorTimeoutOnTick;
					_errorTimeout.Start();
				}
				else {
					progressIndicator.IsVisible = false;
				}
			});
		}

		private void ErrorTimeoutOnTick(object sender, EventArgs e) {
			CancelErrorTimeout();

			ProgressIndicator progressIndicator = SystemTray.ProgressIndicator;

			if (progressIndicator == null) return;

			progressIndicator.Text = String.Empty;
			progressIndicator.IsIndeterminate = false;
			progressIndicator.IsVisible = false;
		}

		private void CancelErrorTimeout() {
			if (_errorTimeout != null) {
				_errorTimeout.Stop();
				_errorTimeout = null;
			}
		}

		private ProgressIndicator EnsureProgressIndicator() {
			ProgressIndicator progressIndicator = SystemTray.ProgressIndicator;

			if (progressIndicator == null) {
				progressIndicator = new ProgressIndicator();

				progressIndicator.SetValue(ProgressIndicator.IsIndeterminateProperty, true);

				SystemTray.SetProgressIndicator(this, progressIndicator);
			}

			return progressIndicator;
		}

        #endregion

        #region SystemTray

	    public virtual bool ShowSystemTray {
		    get { return true; }
	    }

        /// <summary>
        /// Sets the system tray style
        /// based on the current network connectivity.
        /// </summary>
        private void SetSystemTray() {
	        if (this.ShowSystemTray) {
				SystemTray.SetOpacity(this, 0);
				SystemTray.SetIsVisible(this, true);
                SystemTray.SetForegroundColor(this, (Color)App.Current.Resources["ColorAccent"]);
	        }
	        else {
				SystemTray.SetIsVisible(this, false);
	        }
        }

		protected virtual void OnSystemTraySet() {}

        #endregion
    }
}
