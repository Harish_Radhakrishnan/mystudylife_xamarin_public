﻿using System;
using System.Windows;
using System.Windows.Controls;
using MyStudyLife.WP.Behaviors;

namespace MyStudyLife.WP.Common {
    public abstract class DataTemplateSelector : ContentControl {
        public virtual DataTemplate SelectTemplate(object item, DependencyObject container) {
            return null;
        }

        protected DataTemplateSelector() {
            this.Loaded += OnLoaded;
        }

        // Required as OnContentChanged won't be called for nulls
        private void OnLoaded(object sender, RoutedEventArgs e) {
            ContentTemplate = SelectTemplate(this.Content, this);
        }

        protected override void OnContentChanged(object oldContent, object newContent) {
            base.OnContentChanged(oldContent, newContent);
            
            ContentTemplate = SelectTemplate(newContent, this);
        }
    }

    public class NullTemplateSelector : DataTemplateSelector {
        public DataTemplate NullTemplate { get; set; }
        public DataTemplate ItemTemplate { get; set; }

        public NullTemplateSelector() {
            this.HorizontalContentAlignment = HorizontalAlignment.Stretch;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container) {
            if (item == null || item is OptionalListPickerBehavior.NullShim) {
                return NullTemplate;
            }

            return ItemTemplate;
        }
    }
}
