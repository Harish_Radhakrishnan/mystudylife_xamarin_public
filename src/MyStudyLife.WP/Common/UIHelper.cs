﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace MyStudyLife.WP.Common {
    public static class UIHelper {
        public static void AnimateViewDeletion(MslAppView view, Action onAnimateComplete) {
            #region Setup

            var transforms = new TransformGroup();

            var scaleTransform = new ScaleTransform {
                ScaleX = 1,
                ScaleY = 1,
                CenterX = view.RenderSize.Width / 2,
                CenterY = view.RenderSize.Height / 2,
            };
            var translateTransform = new TranslateTransform { X = 0, Y = 0 };

            transforms.Children.Add(scaleTransform);
            transforms.Children.Add(translateTransform);

            view.RenderTransform = transforms;

            #endregion

            var sb = new Storyboard {
                FillBehavior = FillBehavior.HoldEnd
            };

            var aniDuration = TimeSpan.FromMilliseconds(300);
            var scaleTo = 0.5;
            var easing = new ExponentialEase {
                EasingMode = EasingMode.EaseInOut,
                Exponent = 6
            };

            var scaleAniX = new DoubleAnimation {
                To = scaleTo,
                BeginTime = TimeSpan.FromSeconds(0.5),
                Duration = aniDuration,
                EasingFunction = easing
            };
            Storyboard.SetTarget(scaleAniX, scaleTransform);
            Storyboard.SetTargetProperty(scaleAniX, new PropertyPath(ScaleTransform.ScaleXProperty));
            sb.Children.Add(scaleAniX);

            var scaleAniY = new DoubleAnimation {
                To = scaleTo,
                BeginTime = TimeSpan.FromSeconds(0.5),
                Duration = aniDuration,
                EasingFunction = easing
            };
            Storyboard.SetTarget(scaleAniY, scaleTransform);
            Storyboard.SetTargetProperty(scaleAniY, new PropertyPath(ScaleTransform.ScaleYProperty));
            sb.Children.Add(scaleAniY);

            var translateAni = new DoubleAnimation {
                To = 600,
                BeginTime = TimeSpan.FromSeconds(1),
                Duration = aniDuration,
                EasingFunction = easing
            };
            Storyboard.SetTarget(translateAni, translateTransform);
            Storyboard.SetTargetProperty(translateAni, new PropertyPath(TranslateTransform.YProperty));
            sb.Children.Add(translateAni);

            if (onAnimateComplete != null) {
                translateAni.Completed += (s, e) => onAnimateComplete();
            }

            sb.Begin();
        }
    }
}
