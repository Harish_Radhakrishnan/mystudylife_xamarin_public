﻿using System;

namespace MyStudyLife.UI.Tasks {
	public interface ITaskProvider {
		void ShowEmailComposer(string subject, string body, string to);

		void ReviewApp();

	    bool TryShowNativeUri(Uri uri);

	    void ShowWebUri(Uri uri);

        /// <summary>
        ///     Shows the My Study Life Facebook page in
        ///     the native application if available, falling
        ///     back to the mobile web.
        /// </summary>
        void ShowFacebookPage();
	}
}
