﻿using System;
using System.Collections.Generic;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyStudyLife.Scheduling;
using MyStudyLife.Data;

namespace MyStudyLife.UI {
    public interface INavigationService : IMvxNavigationService {
        NavigationBackStack BackStack { get; }

        /// <summary>
        ///     Backwards compat for view model pattern prior to MvvmCross 6 - Init(object) methods.
        /// </summary>
        System.Threading.Tasks.Task<bool> Navigate<TViewModel>(object parameterValuesObject) where TViewModel : IMvxViewModel;
        /// <summary>
        ///     Backwards compat for view model pattern prior to MvvmCross 6 - Init(object) methods.
        /// </summary>
        System.Threading.Tasks.Task<bool> Navigate(Type viewModelType, object parameterValuesObject);

        /// <summary>
        ///     Backwards compat for view model pattern prior to MvvmCross 6 - Init(object) methods.
        /// </summary>
        System.Threading.Tasks.Task<bool> Navigate(Type viewModelType, object parameterValuesObject, IDictionary<string, string> presentationValues);

        void ViewEntry(AgendaEntry entry);

        MvxViewModelRequest GetRequest(AgendaEntry entry);
        MvxViewModelRequest GetRequest<T>(T entity) where T : SubjectDependentEntity;
        Type GetFirstViewAuthorized(User user);
        System.Threading.Tasks.Task<bool> NavigateFirstViewAuthorized(User user);
    }
}
