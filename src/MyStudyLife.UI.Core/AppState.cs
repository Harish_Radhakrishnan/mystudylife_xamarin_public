﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Annotations;
using MyStudyLife.Versioning;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI
{
    public interface IAppStateManager {
        void EnsureInitialized();

        Task<IAppState> GetStateAsync();
    }

    public interface IAppState {
        bool NoSchedulesPromptIgnored { get; set; }

        Guid? CurrentScheduleGuid { get; set; }

        /// <summary>
        ///     The current user.
        /// </summary>
        User User { get; }

        /// <summary>
        ///     Sets the current user and updates the associated state.
        /// </summary>
        /// <param name="user">
        ///     The current user.
        /// </param>
        void SetUser(User user);
    }

    public class AppStateManager : IAppStateManager {
        private readonly IMvxMessenger _messenger;
        private readonly IUpgradeService _upgradeService;
        private readonly IAuthorizationService _authService;
        private readonly IStorageProvider _storageProvider;
        private readonly IUserStore _userStore;
        private readonly IAnalyticsService _analyticsService;
        private readonly IBugReporter _bugReporter;

        [UsedImplicitly]
        private IDisposable _authStatusChangedMessage;

        private readonly object _initializationSyncRoot = new object();
        private bool _initialized;

        private readonly Lazy<IAppState> _appState;

        public AppStateManager(
            IMvxMessenger messenger,
            IUpgradeService upgradeService,
            IAuthorizationService authService,
            IStorageProvider storageProvider,
            IUserStore userStore,
            IAnalyticsService analyticsService,
            IBugReporter bugReporter
        ) {
            this._messenger = messenger;
            this._upgradeService = upgradeService;
            this._authService = authService;
            this._storageProvider = storageProvider;
            this._userStore = userStore;
            this._analyticsService = analyticsService;
            this._bugReporter = bugReporter;
            
            this._appState = new Lazy<IAppState>(LoadState);
        }

        public void EnsureInitialized() {
            if (_initialized) {
                return;
            }

            lock (_initializationSyncRoot) {
                if (_initialized) {
                    return;
                }

                InitializeImpl();

                _initialized = true;
            }
        }

        protected virtual void InitializeImpl() {
            this._authStatusChangedMessage = this._messenger.SubscribeOnThreadPoolThread<AuthStatusChangedEventMessage>(OnAuthStatusChanged);

            // Purposely a fire and forget
            Task.Run(GetStateAsync).ConfigureAwait(false);
        }

        public Task<IAppState> GetStateAsync() => Task.FromResult(_appState.Value);

        private IAppState LoadState()
        {
            var appState = new AppState(_analyticsService, _bugReporter, _storageProvider);

            // User format might have changed and therefore cannot be loaded until upgraded
            if (!_upgradeService.RequiresUpgrade && _authService.IsAuthorized) {
                var user = _userStore.GetCurrentUser();
                    
                appState.SetUser(user);
            }

            return appState;
        }

        private async void OnAuthStatusChanged(AuthStatusChangedEventMessage message) {
            try {
                // Set user to null on sign out to trigger analytics/bug reporter updates.
                // (changing app state does not persist to the underlying store)
                if (!message.IsAuthorized && message.Cause == AuthStatusChangeCause.SignOut) {
                    (await GetStateAsync()).SetUser(null);
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<AppStateManager>>().LogTrace("Caught unhandled exception whilst handling auth status changed event: " + ex.ToLongString());
            }
        }

        private sealed class AppState : IAppState {
            private const string SCHEDULES_PROMPT_IGNORED_KEY = "MSL_STATE_SCHEDULES_PROMPT_IGNORED";

            private readonly IStorageProvider _storageProvider;
            private readonly IAnalyticsService _analyticsService;
            private readonly IBugReporter _bugReporter;

            private bool? _noSchedulesPromptIgnored;
            public bool NoSchedulesPromptIgnored {
                get {
                    if (!_noSchedulesPromptIgnored.HasValue) {
                        _noSchedulesPromptIgnored = _storageProvider.GetSetting<bool?>(SCHEDULES_PROMPT_IGNORED_KEY).GetValueOrDefault(false);
                    }

                    return _noSchedulesPromptIgnored.Value;
                }
                set {
                    _storageProvider.AddOrUpdateSetting(SCHEDULES_PROMPT_IGNORED_KEY, value);

                    _noSchedulesPromptIgnored = value;
                }
            }

            // TODO: Implement this
            private static Guid? _currentScheduleGuid;
            public Guid? CurrentScheduleGuid {
                get { return _currentScheduleGuid; }
                set { _currentScheduleGuid = value; }
            }

            public User User { get; private set; }

            public AppState(
                IAnalyticsService analyticsService,
                IBugReporter bugReporter,
                IStorageProvider storageProvider
            ) {
                this._analyticsService = analyticsService;
                this._bugReporter = bugReporter;
                this._storageProvider = storageProvider;
            }

            public void SetUser(User user) {
                if (user?.Id != this.User?.Id) {
                    this.ClearState();
                }

                this.User = User.Current = user;

                // TODO: App state does not depend on these, it's the other way around, we
                // need some sort of eventing
                this._analyticsService.SetUserInfo(user);
                this._bugReporter.SetUser(user);

                user?.ApplyLocaleSettings();
            }

            private void ClearState() {
                this.User = null;

                this._noSchedulesPromptIgnored = false;
                this._storageProvider.RemoveSetting(SCHEDULES_PROMPT_IGNORED_KEY);

                this.CurrentScheduleGuid = null;
            }
        }
    }
}
