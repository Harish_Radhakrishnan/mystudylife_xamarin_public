﻿using System;
using MvvmCross.ViewModels;

namespace MyStudyLife.UI.Presentation {
    /// <summary>
    ///     Presentation hint used when the application is
    ///     handling a deeplink.
    /// </summary>
    public class DeepLinkPresentationHint : MvxPresentationHint {
        /// <summary>
        ///     The deeplink the app has been activated with.
        /// </summary>
        public Uri Uri { get; }

        /// <summary>
        ///     Instantiates a new instance of <see cref="DeepLinkPresentationHint"/>
        ///     with the provided deeplink.
        /// </summary>
        /// <param name="uri">
        ///     The uniform resource identifier.
        /// </param>
        public DeepLinkPresentationHint(Uri uri) {
            Check.NotNull(uri, nameof(uri));

            this.Uri = uri;
        }
    }
}
