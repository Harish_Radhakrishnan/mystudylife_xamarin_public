﻿using System;

namespace MyStudyLife.UI.Presentation.DeepLinking {
    public interface IDeepLinkDispatcher {
        System.Threading.Tasks.Task Dispatch(Uri uri);
    }
}
