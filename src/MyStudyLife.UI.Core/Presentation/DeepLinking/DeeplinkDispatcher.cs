﻿using System;
using MyStudyLife.Authorization;

namespace MyStudyLife.UI.Presentation.DeepLinking
{
    public class DeepLinkDispatcher : IDeepLinkDispatcher {
        private readonly INavigationService _navigationService;

        public DeepLinkDispatcher(INavigationService navigationService) {
            _navigationService = navigationService;
        }

        public async System.Threading.Tasks.Task Dispatch(Uri uri) {
            // In the future, this should actually dispatch to other classes, but this will do for now
            // so we don't have to repeat on a per platform basis
            if (uri.Host == "oauth" && uri.AbsolutePath == "/callback") {
                var result = OAuthWebFlowResult.FromQueryString(uri.Query);
                
                await _navigationService.ChangePresentation(new OAuthWebFlowResultPresentationHint(result));
            }
            else {
                await _navigationService.ChangePresentation(new DeepLinkPresentationHint(uri));
            }
        }
    }
}
