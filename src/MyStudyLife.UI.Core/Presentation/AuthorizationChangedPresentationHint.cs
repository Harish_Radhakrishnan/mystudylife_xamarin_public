﻿using MvvmCross.ViewModels;
using MyStudyLife.Authorization;

namespace MyStudyLife.UI.Presentation {
    public class AuthorizationChangedPresentationHint : MvxPresentationHint {
        public AuthStatusChangeCause Cause { get; private set; }

        public AuthorizationChangedPresentationHint(AuthStatusChangeCause cause) {
            this.Cause = cause;
        }
    }
}
