﻿using System;
using MvvmCross.Presenters.Hints;
using MvvmCross.ViewModels;
using MyStudyLife.Data;

namespace MyStudyLife.UI.Presentation {
    /// <summary>
    ///     A custom presentation hint which falls back to closing the view.
    /// 
    ///     Allows deletions to be handled on a per platform basis - ie. Windows
    ///     Phone shows an animation and handles navigation itself.
    /// </summary>
    public sealed class EntityDeletedPresentationHint : MvxClosePresentationHint {
        public EntityDeletedPresentationHint(IDbRow entity, IMvxViewModel viewModelToClose) : base(viewModelToClose) {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            this.Type = entity.GetType();
            this.Entity = entity;
        }

        public Type Type { get; private set; }

        public IDbRow Entity { get; private set; }

        public static EntityDeletedPresentationHint ForEntity<T>(T entity, IMvxViewModel viewModel) where T : IDbRow {
            return new EntityDeletedPresentationHint(entity, viewModel);
        }
    }
}