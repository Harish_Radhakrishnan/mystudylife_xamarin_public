﻿using MvvmCross.ViewModels;

namespace MyStudyLife.UI.Presentation {
    public class WelcomeWizardCompletedPresentationHint : MvxPresentationHint {
        public bool Skipped { get; set; }

        public bool AddClasses { get; set; }
    }
}
