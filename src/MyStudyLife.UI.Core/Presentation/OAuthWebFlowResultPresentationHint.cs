﻿using MvvmCross.ViewModels;
using MyStudyLife.Authorization;

namespace MyStudyLife.UI.Presentation {
    public class OAuthWebFlowResultPresentationHint : MvxPresentationHint {
        public OAuthWebFlowResult Result { get; }

        public OAuthWebFlowResultPresentationHint(OAuthWebFlowResult result) {
            this.Result = result;
        }
    }
}
