﻿using System;
using Task = MyStudyLife.Data.Task;
using MyStudyLife.ObjectModel;
using MyStudyLife.Data.Filters;
using MyStudyLife.Globalization;
using MyStudyLife.Data.UI;
using System.Linq;

namespace MyStudyLife.UI.Data {
    public class TaskGrouping : Grouping<int, Task>, IUIGrouping {
        public long Id { get; }

        public TaskGroupingKind Kind { get; }

        public System.Drawing.Color Color { get; }

        public TaskGrouping(int key, string name) : this(key, name, TaskGroupingKind.DatePeriod) { }

        public TaskGrouping(int key, string name, TaskGroupingKind kind) : base(key) {
            Key = key;
            Name = name;
            Kind = kind;

            if (kind == TaskGroupingKind.Overdue) {
                Color = UserColors.Attention;
            }
            else if (kind == TaskGroupingKind.DueSoon) {
                Color = UserColors.Warning;
            }
            else {
                Color = UserColors.Todo;
            }

            // Only positive ids
            Id = name.GetHashCode() & Int32.MaxValue;
        }

        public override void AddItem(Task item) {
            Check.NotNull(item, nameof(item));

            SortOrder order = TaskFilter.FilterCurrentPredicate(item) ? SortOrder.Ascending : SortOrder.Descending;

            var sortedList = this.Items.ToList();

            sortedList.Add(item);

            sortedList.Sort((x, y) =>{
                // x > = 1, y > = -1
                int move = x.DueDate.CompareTo(y.DueDate);

                if (order == SortOrder.Descending) {
                    move *= -1;
                }

                return move;
            });

            int idx = sortedList.IndexOf(item);

            InsertItem(idx, item);
        }

        public static TaskGrouping For(Task task) {
            // #MSL-99 We're using integer, hardcoded keys so the sorting is always correct,
            // and swapping filters in GroupedCollectionViewSource works on iOS
            if (TaskFilter.FilterCurrentPredicate(task)) {
                if (task.IsOverdue) return new TaskGrouping(0, R.Overdue, TaskGroupingKind.Overdue);

                if (task.DueDate.Date.IsToday()) return new TaskGrouping(1, R.DueToday, TaskGroupingKind.DueSoon);

                if (task.DueDate.Date.IsTomorrow()) return new TaskGrouping(2, R.DueTomorrow, TaskGroupingKind.DueSoon);

                if (task.DueDate.IsCurrentWeek()) return new TaskGrouping(3, R.DueThisWeek);

                if (task.DueDate.GetWeekOfYear() == DateTimeEx.Today.AddWeeks(1).GetWeekOfYear()) return new TaskGrouping(4, R.DueNextWeek);

                if (task.DueDate.IsCurrent(DateTimeComparison.Month)) return new TaskGrouping(5, R.DueLaterThisMonth);
            }

            // Key here would be 201608 for August 2016, which we can easily sort by
            return new TaskGrouping(((task.DueDate.Year * 100) + task.DueDate.Month), R.Due + " " + task.DueDate.ToString("MMMM yyyy"));
        }
    }

    public enum TaskGroupingKind {
        Overdue,
        DueSoon,
        DatePeriod,
        Holiday // TBC?
    }
}
