﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MyStudyLife.UI.Data {
    /// <summary>
    ///     Represents data that will be used to create
    ///     a dial.
    /// </summary>
    public class DialData : IEnumerable<DialDataset>, INotifyPropertyChanged {
#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067

        private readonly IEnumerable<DialDataset> _datasets;

        /// <summary>
        ///     The sum of all the <see cref="DialDataset.Count" />
        ///     for the datasets in this collection.
        /// </summary>
        public int Sum { get; private set; }

        /// <summary>
        ///     Creates an empty instance of <see cref="DialData"/>.
        /// </summary>
        public DialData() {
            this._datasets = Enumerable.Empty<DialDataset>();
        }

        /// <summary>
        ///     Creates an instance of <see cref="DialData"/> for the
        ///     given <param name="datasets" />
        /// </summary>
        public DialData(IEnumerable<DialDataset> datasets) {
            Check.NotNull(datasets, nameof(datasets));

            this._datasets = datasets;
            this.Sum = datasets.Sum(x => x.Count);
        }

        #region IEnumerable

        public IEnumerator<DialDataset> GetEnumerator() => _datasets.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        #endregion
    }
}
