﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MyStudyLife.UI.Data.Statistics {
    public sealed class StatisticGroup {
        private readonly ObservableCollection<StatisticPair> _statistics;

        public string Title { get; private set; }

        public ObservableCollection<StatisticPair> Statistics {
            get { return _statistics; }
        }

        public StatisticGroup(string title, IEnumerable<StatisticPair> statistics) {
            this.Title = title;

            _statistics = statistics.ToObservableCollection();
        }
    }
}
