﻿using System;
using System.ComponentModel;
using MyStudyLife.UI.Annotations;

namespace MyStudyLife.UI.Data.Statistics {
    /// <summary>
    ///     Because some statistics don't like
    ///     to be alone.
    /// </summary>
	public sealed class StatisticPair : INotifyPropertyChanged {
#pragma warning disable CS0067
        [UsedImplicitly]
		public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067

        private readonly string _oneFormat, _twoFormat;
        private readonly bool _pluralFormatConcatValue;

        public string One { get; private set; }

        public string Two { get; private set; }

        public StatisticPair(string oneFormat, string twoFormat, bool pluralFormatConcatValue = true) {
            this._oneFormat = oneFormat;
            this._twoFormat = twoFormat;
            this._pluralFormatConcatValue = pluralFormatConcatValue;

            this.SetOne(0);
            this.SetTwo(0);
        }

        public void SetOne(int count) {
            this.One = String.Format(new PluralFormatProvider(_pluralFormatConcatValue), _oneFormat, count);
        }

        public void SetTwo(int count) {
            this.Two = String.Format(new PluralFormatProvider(_pluralFormatConcatValue), _twoFormat, count);
        }
    }
}
