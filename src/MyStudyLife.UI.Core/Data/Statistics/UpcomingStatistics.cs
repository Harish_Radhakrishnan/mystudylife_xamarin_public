﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Scheduling;
using Task = MyStudyLife.Data.Task;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.Data.Statistics {
    public class UpcomingStatistics {
        public StatisticGroup Today { get; private set; }

        public StatisticGroup Classes { get; private set; }

        public StatisticGroup Tasks { get; private set; }

        public StatisticGroup Exams { get; private set; }

        public UpcomingStatistics() {
            // Giving a pair count here because otherwise we'd
            // have to expose an ObservableCollection etc... (and
            // we've tried that, and it's not very performant on Android).

            // We provide the formats here so if they take a while to calculate, it won't be a jarring
            // change from "--" to the text.
            this.Today = new StatisticGroup("Today", new [] {
                new StatisticPair("{0:class;classes} today", "{0:exam;exams} today")
            });
            this.Classes = new StatisticGroup("Classes", new[] {
                new StatisticPair("{0:class;classes} remaining today", "{0:class;classes} attended"),
                new StatisticPair("{0} {0:class;classes} tomorrow", "{0} {0:class;classes} {0:has;have} tasks due", false)
            });
            this.Tasks = new StatisticGroup("Tasks", new [] {
                new StatisticPair("{0:task;tasks} due today", "{0:task;tasks} given today"),
                new StatisticPair("{0:task;tasks} due tomorrow", "{0:task;tasks} overdue")
            });
            this.Exams = new StatisticGroup("Exams", new [] {
                new StatisticPair("{0} {0:exam;exams} in the next 7 days", "{0} incomplete revision {0:task;tasks} due", false), 
            });
        }

        private static IStorageProvider _storageProvider;

        public static IStorageProvider StorageProvider {
            get { return _storageProvider ?? (_storageProvider = Mvx.IoCProvider.Resolve<IStorageProvider>()); }
        }

        public static async Task<UpcomingStatistics> GetForCurrentDateAsync() {
            var that = new UpcomingStatistics();

            IEnumerable<AgendaEntry> entries = await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetForDateRangeAsync(DateTimeEx.Today, DateTimeEx.Today.AddDays(1d));

            await that.SetFromEntriesAsync(entries);

            return that;
        }

        /// <summary>
        ///     Updates or "sets" the statistics using the given collection of entries.
        /// </summary>
        /// <remarks>
        ///     This helps improve performance by not performing
        ///     unnecessary calls to the <see cref="AgendaScheduler"/>.
        /// </remarks>
        /// <param name="entries">
        ///     The collection of scheduled entries.
        /// 
        ///     Can contain entries for any date but only
        ///     today and tomorrow will be taken account of.
        /// </param>
        /// <param name="isTodayOnly">
        ///     True if the collection only contains entries
        ///     for today.
        /// 
        ///     This defaults to true and means an additional 
        ///     call is made to schedule tomorrows entries.
        /// </param>
        public async AsyncTask SetFromEntriesAsync(IEnumerable<AgendaEntry> entries, bool isTodayOnly = true) {
            DateTime today = DateTimeEx.Today;
            DateTime tomorrow = today.AddDays(1d);
            DateTime sevenDaysTime = today.AddDays(7d).EndOfDay();

            entries = entries.ToList();

            List<AgendaEntry> entriesToday = entries.Where(x => x.StartTime.Date == today).ToList();
            List<AgendaEntry> entriesTomorrow;


            if (isTodayOnly) {
                entriesTomorrow = (await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetForDateAsync(tomorrow, AgendaSchedulerOptions.SetTaskCounts)).ToList();
            }
            else {
                entriesTomorrow = entries.Where(x => x.StartTime.Date == tomorrow).ToList();
            }

            #region Today

            var todayPair = this.Today.Statistics[0];

            todayPair.SetOne(entriesToday.Count(x => x.Type == AgendaEntryType.Class));
            todayPair.SetTwo(entriesToday.Count(x => x.Type == AgendaEntryType.Exam));

            #endregion

            #region Classes

            this.SetUltraTimeSensitive(entriesToday);
            
            var classesTmrwPair = this.Classes.Statistics[1];

            classesTmrwPair.SetOne(entriesTomorrow.Count(x => x.Type == AgendaEntryType.Class));
            classesTmrwPair.SetTwo(entriesTomorrow.Count(x => x.Type == AgendaEntryType.Class && x.TasksDue > 0));

            #endregion

            Func<Task, bool> revisionTaskPredicate =
                x => x.Type == TaskType.Revision && x.Progress < 100 && today <= x.DueDate.Date && x.DueDate.Date <= sevenDaysTime;

            #region Tasks

            List<Task> tasks;

            using (var taskRepo = new TaskRepository(StorageProvider)) {
				// Using params not .Date for SQLite
	            DateTime startOfTodayUtc = DateTime.UtcNow.StartOfDay();
	            DateTime endOfTodayUtc = DateTime.UtcNow.EndOfDay();

                tasks = (await taskRepo.GetAllNonDeletedAsync(x => 
                    // Where the task is due today or tomorrow (no matter completion)
                    (x.DueDate == today || x.DueDate == tomorrow) ||
                    // The task was "given" today (ie. created)
					(startOfTodayUtc <= x.CreatedAt && x.CreatedAt <= endOfTodayUtc) ||
                    // It's overdue
                    (x.DueDate <= today && x.Progress < 100) ||
                    // For exams below, it's a revision task for an exam in the next 7 days
                    (x.Type == TaskType.Revision && x.Progress < 100 && today <= x.DueDate && x.DueDate <= sevenDaysTime)
                )).ToList();
            }

            var tasksTodayPair = this.Tasks.Statistics[0];

            tasksTodayPair.SetOne(tasks.Count(x => x.DueDate.Date == today));
            tasksTodayPair.SetTwo(tasks.Count(x => x.CreatedAt.Date == today));

            var tasksTmrwPair = this.Tasks.Statistics[1];

            tasksTmrwPair.SetOne(tasks.Count(x => x.DueDate.Date == tomorrow));
            tasksTmrwPair.SetTwo(tasks.Count(x => x.IsOverdue));

            #endregion

            #region Exams

            List<Exam> exams;

            using (var examRepo = new ExamRepository(StorageProvider)) {
                exams = (await examRepo.GetAllNonDeletedAsync(x => today <= x.Date && x.Date <= sevenDaysTime)).ToList();
            }

            var examsPair = this.Exams.Statistics[0];

            examsPair.SetOne(exams.Count);
            examsPair.SetTwo(tasks.Count(revisionTaskPredicate));

            #endregion
        }

        /// <summary>
        ///     <para>
        ///         Updates the ultra time sensitive 
        ///         properties using the given collection
        ///         of entries.
        ///     </para>
        /// </summary>
        /// <param name="entriesToday"></param>
        public void SetUltraTimeSensitive(ICollection<AgendaEntry> entriesToday) {
            var classesTodayPair = this.Classes.Statistics[0];

            classesTodayPair.SetOne(entriesToday.Count(x => x.Type == AgendaEntryType.Class && (x.IsCurrent || x.IsNext)));
            classesTodayPair.SetTwo(entriesToday.Count(x => x.Type == AgendaEntryType.Class && x.IsPast));
        }
    }
}
