﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace MyStudyLife.UI.Data {
    public abstract class CollectionViewSourceBase<T> : INotifyPropertyChanged {
        private ObservableCollection<T> _source;
        private Predicate<T> _filter;
        private IEqualityComparer<T> _equalityComparer = EqualityComparer<T>.Default;

        public ObservableCollection<T> Source {
            get { return _source; }
            set {
                if (_source == value) return;

                if (_source != null)
                    _source.CollectionChanged -= SourceOnCollectionChanged;

                _source = value;

                if (_source != null)
                    _source.CollectionChanged += SourceOnCollectionChanged;

                OnSourceChanged();
            }
        }

        public Predicate<T> Filter {
            get { return this._filter; }
            set {
                if (_filter == value) return;

                _filter = value;

                OnFilterChanged();
            }
        }

        /// <summary>
        ///     Used to determine wheather to replace
        ///     an item or do seperate add/remove events.
        /// </summary>
        public IEqualityComparer<T> EqualityComparer {
            get { return _equalityComparer; }
            set {
                if (value == null) {
                    throw new ArgumentNullException(nameof(value));
                }

                _equalityComparer = value;
            }
        }

        public abstract int Count { get; }

        /// <summary>
        ///     The number of items after which a single
        ///     <see cref="NotifyCollectionChangedAction.Reset"/> event
        ///     will be fired as opposed to multiple events.
        /// </summary>
        /// <remarks>
        ///     This is due to portable class library's insufficient support
        ///     for <see cref="NotifyCollectionChangedEventArgs"/>.
        /// </remarks>
        public virtual int ResetThreshold { get; set; } = 5;
        
        private void SourceOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            var toAdd = new List<T>();
            var toRemove = new List<T>();
            var toReplace = new List<Tuple<T, T>>();

            switch (e.Action) {
                case NotifyCollectionChangedAction.Add:
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in e.NewItems.Cast<T>().Where(MatchFilter)) {
                        toAdd.Add(item);
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    for (int i = 0; i < e.OldItems.Count; i++) {
                        toReplace.Add(new Tuple<T, T>((T) e.OldItems[i], (T) e.NewItems[i]));
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var item in e.OldItems.Cast<T>()) {
                        toRemove.Add(item);
                    }
                    break;
                case NotifyCollectionChangedAction.Reset:
                    EnsureView();
                    break;
            }

            PerformAddAndRemove(toAdd, toRemove, toReplace);
        }

        protected virtual void OnSourceChanged() => EnsureView();
        protected virtual void OnFilterChanged() => EnsureView();

        protected abstract void EnsureView();

        protected abstract void PerformAddAndRemove(IList<T> toAdd, IList<T> toRemove, IList<Tuple<T, T>> toReplace);

        #region Utils

        public void NotifyItemsSourceChanged() {
            this.EnsureView();
        }

        protected bool MatchFilter(T obj) {
            return Filter == null || Filter(obj);
        }

        #endregion

        #region Implementation of INotifyPropertyChanged

        protected virtual event PropertyChangedEventHandler PropertyChanged;

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
            add {
                PropertyChanged += value;
            }
            remove {
                PropertyChanged -= value;
            }
        }

        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }

    /// <summary>
    ///     A simple implementation of CollectionView / CollectionViewSource
    ///     for portable class libraries.
    /// </summary>
    /// <remarks>
    ///     Unfortunately due some Windows 8 oddness, we had to resort to using
    ///     an <see cref="ObservableCollection{T}"/> as the view. Probably something
    ///     to do with ObservableMap but ItemsControls and the inherited versions
    ///     don't subscribe to the <see cref="INotifyCollectionChanged.CollectionChanged"/>
    ///     event.
    /// 
    ///     (Sept. 2013)
    /// </remarks>
    public class CollectionViewSource<T> : CollectionViewSourceBase<T>, IEnumerable<T> {

        /// <summary>
        ///     Triggered whenever the view is changed.
        /// </summary>
        public event EventHandler ViewChanged;

        private readonly ObservableCollection<SortDescription<T>> _sortDescriptions = new ObservableCollection<SortDescription<T>>();
        private ObservableCollection<T> _view;

        public ICollection<SortDescription<T>> SortDescriptions => _sortDescriptions;

        public virtual ObservableCollection<T> View {
            get { return _view; }
            protected set { SetView(value); }
        }

        private void SetView(ObservableCollection<T> view, bool suppressEvents = false) {
            if (_view != null)
                _view.CollectionChanged -= ViewOnCollectionChanged;

            _view = view;

            if (_view != null)
                _view.CollectionChanged += ViewOnCollectionChanged;

            if (suppressEvents) 
                return;
            
            OnViewChanged();

            NotifyPropertyChanged();
        }

        private void ViewOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            OnViewChanged();
        }

        public override int Count => _view?.Count ?? 0;
        
        private void OnViewChanged() {
            ViewChanged?.Invoke(this, EventArgs.Empty);
            NotifyPropertyChanged(nameof(Count));
        }

        #region Constructors

        public CollectionViewSource(ObservableCollection<T> source) {
            this.Init(source);
        }

        public CollectionViewSource(ObservableCollection<T> source, params SortDescription<T>[] sortDescriptions) {
            if (source == null)
                throw new ArgumentNullException(nameof(source));
            
            if (sortDescriptions == null)
                throw new ArgumentNullException(nameof(sortDescriptions));

            this.SortDescriptions.AddRange(sortDescriptions);

            this.Init(source);
        } 

        public CollectionViewSource(ObservableCollection<T> source, Predicate<T> filter, params SortDescription<T>[] sortDescriptions) {
            if (source == null)
                throw new ArgumentNullException(nameof(source));

            if (filter == null)
                throw new ArgumentNullException(nameof(filter));

            this.Filter = filter;

            this.SortDescriptions.AddRange(sortDescriptions);
            
            this.Init(source);
        } 

        private void Init(ObservableCollection<T> source) {
            this.SetView(new ObservableCollection<T>(), true);

            this.Source = source;

            this._sortDescriptions.CollectionChanged += SortDescriptionsOnCollectionChanged;
		}

        #endregion

        /// <summary>
        ///     Resets the filter and sort descriptions to the given
        ///     values.
        /// </summary>
        /// <remarks>
        ///     Much more efficient than clearing/adding to the
        ///     sort descriptions collection by hand.
        /// </remarks>
        public void SetFilterAndSort(Predicate<T> filter, params SortDescription<T>[] sortDescriptions) {
            this._sortDescriptions.CollectionChanged -= SortDescriptionsOnCollectionChanged;

            this._sortDescriptions.Clear();
            this._sortDescriptions.AddRange(sortDescriptions);

            this.Filter = filter;

            this._sortDescriptions.CollectionChanged += SortDescriptionsOnCollectionChanged;
        }

        #region OnChanged
        
        private void SortDescriptionsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.ReSort();
        }

        #endregion


        protected override void EnsureView() {
            if (Source == null) {
                this.View = new ObservableCollection<T>();
                return;
            }

            var toAdd = Source.Where(MatchFilter).Where(item => !this.View.Contains(item, EqualityComparer)).ToList();

            var toRemove = new List<T>();
            var toReplace = new List<Tuple<T, T>>();

            foreach (var itemInView in this._view) {
                if (!MatchFilter(itemInView)) {
                    toRemove.Add(itemInView);
                }
                else {
                    var itemInSource = Source.FirstOrDefault(x => EqualityComparer.Equals(x, itemInView));

                    if (itemInSource == null) {
                        toRemove.Add(itemInView);
                    }
                    else {
                        toReplace.Add(new Tuple<T, T>(itemInView, itemInSource));
                    }
                }
            }

            PerformAddAndRemove(toAdd, toRemove, toReplace);
        }

        protected override void PerformAddAndRemove(IList<T> toAdd, IList<T> toRemove, IList<Tuple<T, T>> toReplace) {
            if ((toAdd.Count + toRemove.Count + toReplace.Count) > ResetThreshold) {
                var newCollection = this._view.ToList();

                foreach (var item in toAdd) {
                    newCollection.Add(item);
                }

                foreach (var item in toRemove) {
                    newCollection.Remove(item);
                }

                foreach (var item in toReplace) {
                    newCollection.Remove(item.Item1);
                    newCollection.Add(item.Item2);
                }

                newCollection.Sort(SortComparison);

                this.View = newCollection.ToObservableCollection();
            }
            else {
                foreach (var item in toAdd) {
                    AddSorted(item);
                }

                foreach (var item in toRemove) {
                    this.View.Remove(item);
                }

                foreach (var item in toReplace) {
                    ReplaceSorted(item.Item1, item.Item2);
                }
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(nameof(Count));
        }

        public virtual void ReSort() {
            var viewList = this._view.ToList();
            var sortedItems = this._view.ToList();

            sortedItems.Sort(SortComparison);

            int notifyCount = 0;

            for (int i = 0; i < viewList.Count; i++) {
                int currentIndex = i;
                int sortedIndex = sortedItems.IndexOf(viewList[i]);

                if (sortedIndex != currentIndex) {
                    this._view.Move(i, sortedIndex);

                    notifyCount++;

                    if (notifyCount > ResetThreshold) {
                        break;
                    }
                }
            }

            if (notifyCount > ResetThreshold) {
                this.View = sortedItems.ToObservableCollection();
            }
        }

        #region Utils

        protected Comparison<T> SortComparison {
            get {
                return (x, y) => {
                    // x > = 1, y > = -1
                    int move = 0;

                    foreach (var sd in SortDescriptions) {
                        var xPropVal = sd.Property(x);
                        var yPropVal = sd.Property(y);

                        if (xPropVal == null && yPropVal == null) {
                            move = 0;
                        }
                        else if (xPropVal == null) {
                            move = 1;
                        }
                        else if (yPropVal == null) {
                            move = -1;
                        }
                        else {
                            move = sd.Property(x).CompareTo(sd.Property(y));
                        }

                        if (sd.Order == SortOrder.Descending) {
                            move *= -1;
                        }

                        if (move != 0) return move;
                    }

                    return move;
                };
            }
        }  

        private int AddSorted(T item, IList<T> collection = null) {
            var sortedList = (collection ?? View).ToList();

            sortedList.Add(item);

            sortedList.Sort(SortComparison);

            int idx = sortedList.IndexOf(item);

            (collection ?? View).Insert(idx, item);

            return idx;
        }

        private int ReplaceSorted(T toReplace, T item, IList<T> collection = null) {
            var sortedList = (collection ?? View).ToList();

            sortedList.Remove(toReplace);
            sortedList.Add(item);

            sortedList.Sort(SortComparison);

            int idx = sortedList.IndexOf(item);

            if (idx == (collection ?? View).IndexOf(toReplace)) {
                (collection ?? View)[idx] = item;
            }
            else {
                (collection ?? View).Remove(toReplace);
                (collection ?? View).Insert(idx, item);
            }

            return idx;
        }

        #endregion

        #region Implementation of IEnumerable

        public IEnumerator<T> GetEnumerator() {
            return this.View.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerator();
        }

        #endregion
    }

    public class SortDescription<T> {
        
        public Func<T, IComparable> Property { get; private set; }

        public SortOrder Order { get; private set; }

        public SortDescription(Func<T, IComparable> property, SortOrder order = SortOrder.Ascending) {
            if (property == null)
                throw new ArgumentNullException(nameof(property));

            this.Property = property;
            this.Order = order;
        }
    }

    public enum SortOrder {
        Ascending,
        Descending
    }
}
