﻿using System;
using System.Linq;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.Data.UI;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Models;

namespace MyStudyLife.UI.Data {
    public class ExamGrouping : Grouping<DateTime, Exam>, IUIGrouping {
        public long Id { get; }

        public ExamGroupingKind Kind { get; }

        public System.Drawing.Color Color { get; }

        public ExamGrouping(DateTime key, string name) : this(key, name, ExamGroupingKind.DatePeriod) { }

        public ExamGrouping(DateTime key, string name, ExamGroupingKind kind) : base(key) {
            Key = key;
            Name = name;
            Kind = kind;

            if (kind == ExamGroupingKind.Days3) {
                Color = UserColors.Attention;
            }
            else if (kind == ExamGroupingKind.Days7) {
                Color = UserColors.Warning;
            }
            else {
                Color = UserColors.Todo;
            }

            // Only positive ids
            Id = name.GetHashCode() & Int32.MaxValue;
        }

        public override void AddItem(Exam item) {
            Check.NotNull(item, nameof(item));

            SortOrder order = ExamFilter.FilterCurrentPredicate(item) ? SortOrder.Ascending : SortOrder.Descending;

            var sortedList = this.Items.ToList();

            sortedList.Add(item);

            sortedList.Sort((x, y) => {
                // x > = 1, y > = -1
                int move = x.Date.CompareTo(y.Date);

                if (order == SortOrder.Descending) {
                    move *= -1;
                }

                return move;
            });

            int idx = sortedList.IndexOf(item);

            InsertItem(idx, item);
        }

        public static ExamGrouping For(Exam exam) {
            var today = DateTimeEx.Today;

            if (ExamFilter.FilterCurrentPredicate(exam)) {
                if (exam.Date.Range(today).CalendarDays <= 3) return new ExamGrouping(today.AddDays(3), R.InTheNext3Days, ExamGroupingKind.Days3);

                if (exam.Date.Range(today).CalendarDays <= 7) return new ExamGrouping(today.AddDays(7), R.InTheNext7Days, ExamGroupingKind.Days7);

                if (exam.Date.Range(today).CalendarDays <= 14) return new ExamGrouping(today.AddDays(14), R.InTheNext14Days);

                if (exam.Date.IsSame(today, DateTimeComparison.Month)) return new ExamGrouping(today.EndOfMonth(), R.LaterThisMonth);
            }

            return new ExamGrouping(exam.Date.StartOfMonth(), exam.Date.ToString("MMMM yyyy"));
        }
    }

    public class ExamModelGrouping : Grouping<DateTime, ExamModel>, IUIGrouping {
        public long Id { get; }

        public ExamGroupingKind Kind { get; }

        public System.Drawing.Color Color { get; }

        public ExamModelGrouping(ExamGrouping grouping) : base(grouping.Key) {
            this.Id = grouping.Id;
            this.Kind = grouping.Kind;
            this.Color = grouping.Color;
            this.Name = grouping.Name;
        }

        public override void AddItem(ExamModel item) {
            Check.NotNull(item, nameof(item));

            SortOrder order = ExamFilter.FilterCurrentPredicate(item.Base) ? SortOrder.Ascending : SortOrder.Descending;

            var sortedList = this.Items.ToList();

            sortedList.Add(item);

            sortedList.Sort((x, y) => {
                // x > = 1, y > = -1
                int move = x.Date.CompareTo(y.Date);

                if (order == SortOrder.Descending) {
                    move *= -1;
                }

                return move;
            });

            int idx = sortedList.IndexOf(item);

            InsertItem(idx, item);
        }

        public static ExamModelGrouping For(ExamModel model) => new ExamModelGrouping(ExamGrouping.For(model.Base));
    }


    public enum ExamGroupingKind {
        Days3,
        Days7,
        DatePeriod
    }
}
