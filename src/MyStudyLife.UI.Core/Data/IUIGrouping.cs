﻿namespace MyStudyLife.UI.Data {
    public interface IUIGrouping {
        long Id { get; }

        string Name { get; }
    }
}