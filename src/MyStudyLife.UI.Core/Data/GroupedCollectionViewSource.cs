﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MvvmCross.Binding.Binders;
using MyStudyLife.ObjectModel;

namespace MyStudyLife.UI.Data {
    public class GroupedCollectionViewSource<TKey, TItem> : CollectionViewSourceBase<TItem>
        where TKey : IComparable {
        
        private Func<TItem, Grouping<TKey, TItem>> _grouping;
        private SortOrder _sortOrder = SortOrder.Ascending;
        private ObservableCollection<Grouping<TKey, TItem>> _view = new ObservableCollection<Grouping<TKey, TItem>>();

        public Func<TItem, Grouping<TKey, TItem>> Grouping {
            get { return this._grouping; }
            set {
                if (_grouping == value) return;

                _grouping = value;

                EnsureView();
            }
        }

        /// <summary>
        ///     The direction in which the collection
        ///     is sorted by. The sort property or value
        ///     is determined by <see cref="MvxAutoValueConverters.Key"/>.
        /// </summary>
        public SortOrder SortOrder {
            get { return _sortOrder; }
            set {
                _sortOrder = value;

                this.ReSort();
            }
        }

        public virtual ObservableCollection<Grouping<TKey, TItem>> View {
            get { return _view; }
            private set {
                _view = value;

                NotifyPropertyChanged();
            }
        }

        public override int Count => _view?.Sum(x => x.Count()) ?? 0;

        #region Constructors

        public GroupedCollectionViewSource(ObservableCollection<TItem> source, Func<TItem, Grouping<TKey, TItem>> grouping) {
            this.Init(source, grouping);
        }
        public GroupedCollectionViewSource(ObservableCollection<TItem> source, Func<TItem, Grouping<TKey, TItem>> grouping, Predicate<TItem> filter) {
            if (filter == null) throw new ArgumentNullException(nameof(filter));

            this.Filter = filter;

            this.Init(source, grouping);
        }

        public GroupedCollectionViewSource(ObservableCollection<TItem> source, Func<TItem, Grouping<TKey, TItem>> grouping, SortOrder sortOrder) {
            this._sortOrder = sortOrder;

            this.Init(source, grouping);
        }

        public GroupedCollectionViewSource(ObservableCollection<TItem> source, Func<TItem, Grouping<TKey, TItem>> grouping, Predicate<TItem> filter, SortOrder sortOrder) {
            if (filter == null) throw new ArgumentNullException(nameof(filter));

            this.Filter = filter;

            this._sortOrder = sortOrder;

            this.Init(source, grouping);
        }

        private void Init(ObservableCollection<TItem> source, Func<TItem, Grouping<TKey, TItem>> grouping) {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (grouping == null) throw new ArgumentNullException(nameof(grouping));

            this._grouping = grouping;

            // Calls ensure view
            this.Source = source;
        }

        #endregion

        /// <summary>
        ///     Resets the filter and sort descriptions to the given
        ///     values.
        /// </summary>
        /// <remarks>
        ///     Much more efficient than clearing/adding to the
        ///     sort descriptions collection by hand.
        /// </remarks>
        public void SetFilterAndSort(Predicate<TItem> filter, SortOrder sortOrder) {
            this._sortOrder = sortOrder;
            this.Filter = filter;
        }

        protected override void EnsureView() {
            if (Source == null) {
                this.View = new ObservableCollection<Grouping<TKey, TItem>>();
                return;
            }

            var viewItems = this.View.SelectMany(x => x);

            var toAdd = Source.Where(MatchFilter).Where(item => !viewItems.Contains(item, EqualityComparer)).ToList();

            var toRemove = new List<TItem>();
            var toReplace = new List<Tuple<TItem, TItem>>();

            foreach (var itemInView in viewItems) {
                if (!MatchFilter(itemInView)) {
                    toRemove.Add(itemInView);
                }
                else {
                    var itemInSource = Source.FirstOrDefault(x => EqualityComparer.Equals(x, itemInView));

                    if (itemInSource == null) {
                        toRemove.Add(itemInView);
                    }
                    else {
                        toReplace.Add(new Tuple<TItem, TItem>(itemInView, itemInSource));
                    }
                }
            }

            PerformAddAndRemove(toAdd, toRemove, toReplace);
        }

        protected override void PerformAddAndRemove(IList<TItem> toAdd, IList<TItem> toRemove, IList<Tuple<TItem, TItem>> toReplace) {
            if (this.View.Count == 0 || (toAdd.Count + toRemove.Count + toReplace.Count) > ResetThreshold) {
                var newCollection = this.View.ToList();

                foreach (var item in toAdd) {
                    AddToGrouping(item, newCollection);
                }

                foreach (var item in toRemove) {
                    RemoveFromGrouping(item, newCollection);
                }

                foreach (var item in toReplace) {
                    AddToGrouping(item.Item2, newCollection);
                    RemoveFromGrouping(item.Item1, newCollection);
                }

                newCollection.Sort(SortComparison);

                this.View = newCollection.ToObservableCollection();
            }
            else {
                // We're keeping track of whether a grouping has changed, and invalidating the
                // entire View if it has. If new groups are added, the View isn't invalidated.
                bool groupingChanged = false;

                foreach (var item in toAdd) {
                    groupingChanged = AddToGrouping(item) || groupingChanged;
                }

                foreach (var item in toRemove) {
                    groupingChanged = RemoveFromGrouping(item) || groupingChanged;
                }

                foreach (var item in toReplace) {
                    groupingChanged = RemoveFromGrouping(item.Item1) || groupingChanged;
                    groupingChanged = AddToGrouping(item.Item2) || groupingChanged;
                }

                if (groupingChanged) {
                    // ReSharper disable once ExplicitCallerInfoArgument
                    NotifyPropertyChanged(nameof(View));
                }
            }

            // ReSharper disable once ExplicitCallerInfoArgument
            NotifyPropertyChanged(nameof(Count));
        }

        /// <summary>
        ///     Tells the collection an item has been modified and therefore
        ///     its group may have changed.
        /// </summary>
        /// <param name="item">The item.</param>
        public void NotifyItemChanged(TItem item) {
            Check.NotNull(item, nameof(item));

            var groupingChanged = RemoveFromGrouping(item);

            if (MatchFilter(item))
                groupingChanged = AddToGrouping(item) || groupingChanged;

            // The consumers of the view don't "deep listen" for changes within a group (yet)
            if (groupingChanged) {
                // ReSharper disable once ExplicitCallerInfoArgument
                NotifyPropertyChanged(nameof(View));
            }
        }
        
        public void ReSort() {
            // Unfortunately PCL's don't implement NotifyCollectionChangedEventArgs.Move so we have to reset the
            // list whenever this changes :(
            var sortedItems = this.View.ToList();

            sortedItems.Sort(SortComparison);

            int notifyCount = 0;

            foreach (var item in this._view) {
                int sortedIndex = sortedItems.IndexOf(item);

                if (sortedIndex != this._view.IndexOf(item)) {
                    this._view.Remove(item);
                    this._view.Insert(sortedIndex, item);

                    notifyCount++;

                    if (notifyCount > ResetThreshold) {
                        break;
                    }
                }
            }

            if (notifyCount > ResetThreshold) {
                this.View = sortedItems.ToObservableCollection();
            }
        }

        #region Utils

        private Comparison<Grouping<TKey, TItem>> SortComparison => (x, y) => {
            // x > = 1, y > = -1
            int move = x.Key.CompareTo(y.Key);

            if (this.SortOrder == SortOrder.Descending) {
                move *= -1;
            }

            return move;
        };

        private bool AddToGrouping(TItem item, IList<Grouping<TKey, TItem>> collection = null) {
            var grouping = this.Grouping(item);
            var existingGrouping = (collection ?? View).SingleOrDefault(x => x.Key.Equals(grouping.Key));

            if (existingGrouping == null) {
                grouping.AddItem(item);

                var sortedList = (collection ?? View).ToList();

                sortedList.Add(grouping);

                sortedList.Sort(SortComparison);

                (collection ?? View).Insert(sortedList.IndexOf(grouping), grouping);

                return false;
            }

            existingGrouping.AddItem(item);

            return true;
        }

        private bool RemoveFromGrouping(TItem item, IList<Grouping<TKey, TItem>> collection = null) {
            // Should be able to use single or default but this ensures
            // it will be removed from all groups no matter what.
            var groups = (collection ?? View).Where(x => x.Items.Contains(item, EqualityComparer)).ToList();

            foreach (var g in groups) {
                g.Items.RemoveAll(x => ReferenceEquals(item, x));

                if (g.Items.Count == 0) {
                    (collection ?? View).Remove(g);
                }
                else {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}