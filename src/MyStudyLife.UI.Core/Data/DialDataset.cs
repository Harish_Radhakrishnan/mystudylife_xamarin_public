﻿namespace MyStudyLife.UI.Data {
    public class DialDataset {

        /// <summary>
        ///     The value applied to the dial segment
        /// </summary>
        public int Value { get; }
        /// <summary>
        ///     A number added up to display in the center of the dial
        /// </summary>
        public int Count { get; }
        public System.Drawing.Color Color { get; }

        public DialDataset(int value, System.Drawing.Color color) : this(value, 1, color) { }
        public DialDataset(int value, int count, System.Drawing.Color color) {
            this.Value = value;
            this.Count = count;
            this.Color = color;
        }
    }
}
