﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class TimeSpanToLengthConverter : MvxValueConverter {
        /// <summary>
        ///     Returns a double length based on the total hours of the timespan <param name="value" />
        ///     and length per hour of <param name="parameter" />
        /// </summary>
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            double length;

            if (!(value is TimeSpan))
                return value;

            if (parameter is double)
                length = (double)parameter;
            else
                return value;

            return ((TimeSpan)value).TotalHours * length;
        }
    }
}
