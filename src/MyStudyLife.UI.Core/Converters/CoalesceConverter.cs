﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class CoalesceConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || String.IsNullOrWhiteSpace(value.ToString()))
                return parameter ?? "--";

            return value.ToString();
        }
    }
}
