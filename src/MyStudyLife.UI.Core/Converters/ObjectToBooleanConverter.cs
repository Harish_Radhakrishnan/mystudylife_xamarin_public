﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class ObjectToBooleanConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            bool bParameter = true;

            if (parameter != null) {
                if (!(parameter is bool)) {
                    try {
                        bParameter = (bool)Boolean.Parse(parameter.ToString());
                    }
                    // ReSharper disable EmptyGeneralCatchClause
                    catch
                        // ReSharper restore EmptyGeneralCatchClause
                    {
                    }
                }
                else
                    bParameter = (bool)parameter;
            }

            return (value is string ? !String.IsNullOrWhiteSpace(value.ToString()) : value != null) ? bParameter : !bParameter;
        }
    }
}
