﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class TimeSpanToDateTimeConverter : MvxValueConverter<TimeSpan?, DateTime?> {
        protected override DateTime? Convert(TimeSpan? ts, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
	        if (!ts.HasValue) return null;

			return new DateTime(1970, 1, 1, ts.Value.Hours, ts.Value.Minutes, ts.Value.Seconds);
        }

        protected override TimeSpan? ConvertBack(DateTime? dt, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
	        if (!dt.HasValue) return null;

	        return dt.Value.TimeOfDay;
        }
    }
}
