﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class StringToLowercaseConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;

            return value.ToString().ToLower();
        }
    }

    public class StringToTitleCaseConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;

            return value.ToString().ToTitleCase();
        }
    }

    public class StringToUppercaseConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) return null;

            return value.ToString().ToUpper();
        }
    }

	public class SubstringConverter : MvxValueConverter<string, string> {
		protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null) return null;

			// For some reason android uses long
			if (parameter is long) {
				parameter = System.Convert.ToInt32(parameter);
			}

			if (!(parameter is int)) {
#if DEBUG
				throw new NotSupportedException("Use an integer type for parameter, shouldn't be parsing strings.");
#else
				return value;
#endif
			}

			var length = (int)parameter;

			return value.Length >= length ? value.Substring(0, length) : value;
		}
	}

    public class StringToSentenceCaseConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => (value as string)?.ToSentenceCase();
    }
}
