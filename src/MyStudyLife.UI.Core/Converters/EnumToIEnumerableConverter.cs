﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    /// <summary>
    ///     Converts an enum a bindable collection.
    /// </summary>
    public class EnumToIEnumerableConverter : MvxValueConverter {
        private readonly Dictionary<Type, List<Enum>> _cache = new Dictionary<Type, List<Enum>>();

        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            var type = value.GetType();

            if (!this._cache.ContainsKey(type)) {
                this._cache[type] = Enum.GetValues(type).OfType<Enum>().ToList();
            }

            return this._cache[type];
        }
    }
}
