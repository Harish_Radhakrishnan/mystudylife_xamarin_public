﻿using System;
using MvvmCross.Converters;
using MyStudyLife.Data;

namespace MyStudyLife.UI.Converters {
	public class EnumToBooleanConverter : MvxValueConverter {
		public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value.Equals(parameter);
		}

		public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			if (value is bool && (bool)value) return parameter;

			return null;
		}
	}

	#region Custom Enums
	public class ClassTypeToBooleanConverter : MvxValueConverter<ClassType?, bool> {
		protected override bool Convert(ClassType? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value.Equals((ClassType) parameter);
		}

		protected override ClassType? ConvertBack(bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value ? (ClassType?) (int) parameter : null;
		}
	}

	public class ClassDayTypeToBooleanConverter : MvxValueConverter<ClassDayType?, bool> {
		protected override bool Convert(ClassDayType? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value.Equals((ClassDayType)parameter);
		}

		protected override ClassDayType? ConvertBack(bool value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value ? (ClassDayType?)(int)parameter : null;
		}
	}
	#endregion
}
