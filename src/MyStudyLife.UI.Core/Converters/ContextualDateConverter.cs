﻿using System;
using MvvmCross.Converters;
using MyStudyLife.UI.Extensions;

namespace MyStudyLife.UI.Converters {
    public class ContextualDateConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            DateTime? dt = value as DateTime?;

            if (dt == null) {
                return null;
            }

            ContextualDateFormat format;

            if (!(parameter is ContextualDateFormat)) {
                char? formatSpecifier = parameter as char?;

                if (!formatSpecifier.HasValue) {
                    var formatSpecifierStr = (parameter as string);

                    if (formatSpecifierStr != null && formatSpecifierStr.Length > 0) {
                        formatSpecifier = formatSpecifierStr[0];
                    }
                }

                if (formatSpecifier.HasValue) {
                    switch (formatSpecifier) {
                        case 'D':
                            format = ContextualDateFormat.Long;
                            break;
                        case 'A':
                            format = ContextualDateFormat.Abbreviated;
                            break;
                        case 'S':
                            format = ContextualDateFormat.Short;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(parameter), parameter, "Invalid contextual date format specifier");
                    }
                }
                else {
                    format = ContextualDateFormat.Long;
                }
            }
            else {
                format = (ContextualDateFormat)parameter;
            }

            return dt.Value.ToContextualDateString(format);
        }
    }
}