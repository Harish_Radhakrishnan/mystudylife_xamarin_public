﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    /// <summary>
    ///     Returns true if the value equals the parameter, false otherwise.
    /// </summary>
    public class ParameterToBooleanConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || parameter == null) {
                return value == null && parameter == null;
            }

            if (value.GetType() == parameter.GetType()) {
                return value.Equals(parameter);
            }

            return value.ToString() == parameter.ToString();
        }
    }

    /// <summary>
    ///     Like <see cref="ParameterToBooleanConverter"/> but returns the opposite value.
    /// </summary>
    public class ParameterToBooleanNegationConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || parameter == null) {
                return value != parameter;
            }

            if (value.GetType() == parameter.GetType()) {
                return !value.Equals(parameter);
            }

            return value.ToString() != parameter.ToString();
        }
    }
}
