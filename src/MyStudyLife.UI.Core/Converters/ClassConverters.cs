﻿using System;
using System.Diagnostics;
using System.Globalization;
using MvvmCross;
using MvvmCross.Converters;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Globalization;

namespace MyStudyLife.UI.Converters {
	public sealed class ClassOccurrenceConverter : MvxValueConverter {
		/// <summary>
		///		Theoretically, this should allow a whole list
		///		to load without calling the settings repo more than once.
		///		
		///		A small amount of time is vital in case settings change!
		/// </summary>
		private const int SecondsUntilStale = 5;

		private DateTime _timeSet;
		private TimetableSettings _timetableSettings;
		
		private void EnsureInitialize() {
			if ((DateTime.UtcNow - _timeSet).TotalSeconds > SecondsUntilStale) {
                _timetableSettings = null;
			}

		    if (_timetableSettings != null) {
		        return;
		    }

            var timetableSettingsTask = TimetableSettings.Get(Mvx.IoCProvider.Resolve<ISingletonDataStore>());
		    timetableSettingsTask.Wait();
		    _timetableSettings = timetableSettingsTask.Result;

            _timeSet = DateTime.Now;
		}

        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			EnsureInitialize();
			
			var c = value as Class;
            var t = value as ClassTime;

            if (t != null) {
                if (parameter is DateComponentNaming) {
                    return t.GetOccurrenceText(_timetableSettings, (DateComponentNaming)parameter);
                }

                return t.GetOccurrenceText(_timetableSettings);
            }

            if (c != null) {
                Debug.Assert(parameter as string == null, "Use bool not string");

                bool includeStartEndDates = parameter == null || (parameter is bool && (bool)parameter);

                return c.GetOccurrenceText(_timetableSettings, includeStartEndDates);
            }

            return "--";
        }
	}
}
