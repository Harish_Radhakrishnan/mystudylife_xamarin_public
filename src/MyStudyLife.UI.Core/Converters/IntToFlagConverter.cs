﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class IntToFlagConverter : MvxValueConverter<int, int> {
        protected override int Convert(int value, Type targetType, object parameter, CultureInfo culture) {

            // Used for rotation day
            if ((parameter as bool?).GetValueOrDefault()) {
                value--;
            }

            return 1 << value;
        }
    }
}