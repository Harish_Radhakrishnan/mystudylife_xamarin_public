﻿using System;
using System.Globalization;
using MvvmCross.Converters;
using MyStudyLife.UI.Utilities;

namespace MyStudyLife.UI.Converters {
    // ReSharper disable CompareOfFloatsByEqualityOperator
    public class RelativeTimeConverter : MvxValueConverter<DateTime, string> {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo culture) => value.ToRelativeTimeString();
    }

    public class NumericRelativeDateConverter :MvxValueConverter<DateTime, string> {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo culture) => value.ToRelativeDateString(Tense.Any, false);
    }

    public class RelativeDateConverter : MvxValueConverter<DateTime?, string> {
        public Tense AllowedTenses { get; set; }

        public RelativeDateConverter() {
            this.AllowedTenses = Tense.Any;
        }

        public RelativeDateConverter(Tense allowedTenses) {
            this.AllowedTenses = allowedTenses;
        }

		protected override string Convert(DateTime? value, Type targetType, object parameter, CultureInfo culture) {
		    if (!value.HasValue) {
		        return "unknown";
		    }

		    var allowedTenses = this.AllowedTenses;

		    if (parameter != null) {
		        if (parameter is Tense || parameter is int) {
		            allowedTenses = (Tense) parameter;
		        }
		        else {
		            var str = parameter as string;

		            if (str != null) {
		                allowedTenses = (Tense) Enum.Parse(typeof (Tense), str);
		            }
		            else {
		                throw new Exception("Invalid parameter value '" + parameter + "'");
		            }
		        }
		    }

            return value.Value.ToRelativeDateString(allowedTenses);
		}
	}
    // ReSharper restore CompareOfFloatsByEqualityOperator
}
