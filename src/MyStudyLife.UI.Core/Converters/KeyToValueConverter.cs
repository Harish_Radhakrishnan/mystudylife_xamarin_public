﻿using System;
using System.Collections.Generic;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
	public sealed class KeyToValueConverter : MvxValueConverter<IDictionary<string, string>, string> {
		protected override string Convert(IDictionary<string, string> value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null || parameter == null) return null;

			string rVal;

			value.TryGetValue((string) parameter, out rVal);

			return rVal;
		}
	}
}
