﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class StringFormatConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (parameter == null)
                return value;

            return String.Format((string)parameter, value);
        }
    }

    public class StringFormatUppercaseConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (parameter == null)
                return value;

            return String.Format((string)parameter, value).ToUpper();
        }
    }
}
