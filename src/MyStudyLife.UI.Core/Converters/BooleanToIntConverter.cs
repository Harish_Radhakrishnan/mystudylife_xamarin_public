﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
	public class BooleanToIntConverter : MvxValueConverter<bool, int> {
		protected override int Convert(bool value, Type targetType, object parameter, CultureInfo culture) {
			return value ? 1 : 0;
		}

		protected override bool ConvertBack(int value, Type targetType, object parameter, CultureInfo culture) {
			return value == 1;
		}
	}

    public class NullableBooleanToIntConverter : MvxValueConverter<bool?, int> {
        protected override int Convert(bool? value, Type targetType, object parameter, CultureInfo culture) {
            return value.GetValueOrDefault() ? 1 : 0;
        }

        protected override bool? ConvertBack(int value, Type targetType, object parameter, CultureInfo culture) {
            return value == 1;
        }
    }
}
