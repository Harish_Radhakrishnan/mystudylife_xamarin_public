﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class PluralFormatConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (parameter == null)
                return value;

            return String.Format(new PluralFormatProvider(false), (string)parameter, value);
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return value;
        }
    }

	public class PluralFormatWithValueConverter : MvxValueConverter {
		public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			if (parameter == null)
				return value;

			return String.Format(new PluralFormatProvider(true), (string)parameter, value);
		}

		public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return value;
		}
	}
}
