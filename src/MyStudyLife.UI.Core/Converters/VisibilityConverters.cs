﻿using System.Globalization;
using MvvmCross.Plugin.Visibility;
using MvvmCross.UI;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Extensions;

namespace MyStudyLife.UI.Converters {
	public sealed class TaskRevisionTypeVisibilityConverter : MvxBaseVisibilityValueConverter<TaskType> {
		protected override MvxVisibility Convert(TaskType value, object parameter, CultureInfo culture) {
			return (value == TaskType.Revision).ToVisibility();
		}
	}

	public sealed class TimetableModeVisibilityConverter : MvxBaseVisibilityValueConverter<TimetableMode> {
		protected override MvxVisibility Convert(TimetableMode value, object parameter, CultureInfo culture) {
			return ((int)value == (long)parameter).ToVisibility();
		}
	}

	public sealed class ClassOneOffTypeVisibilityConverter : MvxBaseVisibilityValueConverter<ClassType> {
		protected override MvxVisibility Convert(ClassType value, object parameter, CultureInfo culture) {
			return (value == ClassType.OneOff).ToVisibility();
		}
	}

	public sealed class ClassRecurringTypeVisibilityConverter : MvxBaseVisibilityValueConverter<ClassType> {
		protected override MvxVisibility Convert(ClassType value, object parameter, CultureInfo culture) {
			return (value == ClassType.Recurring).ToVisibility();
		}
	}

	public sealed class ClassDayOfWeekDayTypeVisibilityConverter : MvxBaseVisibilityValueConverter<ClassDayType> {
		protected override MvxVisibility Convert(ClassDayType value, object parameter, CultureInfo culture) {
			return (value == ClassDayType.DayOfWeek).ToVisibility();
		}
	}

	public sealed class ClassRotationDayTypeVisibilityConverter : MvxBaseVisibilityValueConverter<ClassDayType> {
		protected override MvxVisibility Convert(ClassDayType value, object parameter, CultureInfo culture) {
			return (value == ClassDayType.Rotation).ToVisibility();
		}
	}

    public sealed class SetFlagCountVisibilityConverter : MvxBaseVisibilityValueConverter<int> {
        protected override MvxVisibility Convert(int value, object parameter, CultureInfo culture) {
            return (EnumEx.GetSetFlagCount(value) >= ((parameter as long? != null) ? (long) parameter : 1)).ToVisibility();
        }
    }
    
    public static class MvxVisibilityExtensions {
		public static MvxVisibility ToVisibility(this bool b) {
			return b ? MvxVisibility.Visible : MvxVisibility.Collapsed;
		}
	}
}
