﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using MvvmCross.Converters;
using MvvmCross.Plugin.Color;
using MyStudyLife.Data.UI;
using MyStudyLife.Utility;

namespace MyStudyLife.UI.Converters {
    public class SubjectColorToValueConverter : MvxColorValueConverter<string> {
        private IFeatureService _featureService;
        public static readonly System.Drawing.Color DefaultColor = System.Drawing.Color.FromArgb(unchecked((int)0xFF1C8D76));

        // TODO: this is shit, get rid of it
        public SubjectColorToValueConverter(IFeatureService featureService) {
            _featureService = featureService;
        }

        // This *needs* to be lazy - else it adds 200+ms to Android startup (on HTC One X)
        // plus we may never use it anyway!
        private static readonly Lazy<Regex> HexColorRegex = new Lazy<Regex>(() => new Regex(@"^#(?:[0-9a-fA-F]{3}){1,2}$", RegexOptions.IgnoreCase));
        
        protected override System.Drawing.Color Convert(string value, object parameter, CultureInfo culture) {
            if (value == null) {
                return DefaultColor;
            }

            System.Drawing.Color? color = null;

            var extendedColors = _featureService.CanUseFeatureAsync(Feature.ExtendedColors).ConfigureAwait(false).GetAwaiter().GetResult();
            var subjectColor = UserColors.GetColor(value, extendedColors);

            if (subjectColor != null) {
                color = subjectColor.Color;
            }

            if (color == null && HexColorRegex.Value.IsMatch(value)) {
                color = Color.FromArgb(Int32.Parse(value.Substring(1), NumberStyles.HexNumber), Color.White);
            }

            return color ?? DefaultColor;
        }
    }

    public class SubjectColorToColorObjectConverter : MvxValueConverter<string, UserColor> {
        private IFeatureService _featureService;

        public SubjectColorToColorObjectConverter(IFeatureService featureService) {
            _featureService = featureService;
        }

        protected override UserColor Convert(string value, Type targetType, object parameter, CultureInfo culture) {
            var extendedColors = _featureService.CanUseFeatureAsync(Feature.ExtendedColors).GetAwaiter().GetResult();
            return value == null ? null : UserColors.GetColor(value, extendedColors);
        }

        protected override string ConvertBack(UserColor value, Type targetType, object parameter, CultureInfo culture) {
            return value == null ? null : value.Identifier;
        }
    }
}
