﻿using System;
using MvvmCross.Converters;
using MyStudyLife.Data.Annotations;

namespace MyStudyLife.UI.Converters {
    public class EnumToTitleConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return value == null ? null : ((Enum)value).GetTitle();
        }
    }
}
