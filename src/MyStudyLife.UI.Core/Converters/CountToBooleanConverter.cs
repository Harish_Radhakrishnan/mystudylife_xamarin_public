﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class CountToBooleanConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (value is int && (int)value > 0);
        }
    }

	public class CountToBooleanNegationConverter : MvxValueConverter {
		public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (value is int && (int)value == 0);
		}
	}
}
