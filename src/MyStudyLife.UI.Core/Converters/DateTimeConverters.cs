﻿using System;
using System.Globalization;
using MvvmCross.Converters;
using MyStudyLife.Globalization;

namespace MyStudyLife.UI.Converters {
    public sealed class LocalDateTimeFormatConverter : MvxValueConverter<DateTime?, string> {
        protected override string Convert(DateTime? value, Type targetType, object parameter, CultureInfo culture) {
            if (!value.HasValue) return "--";

            string format = parameter as string ?? "f";

            return value.Value.ToLocalTime().ToString(format);
        }
    }

	public sealed class AbbrLongDateFormatConverter : MvxValueConverter<DateTime?, string> {
		protected override string Convert(DateTime? value, Type targetType, object parameter, CultureInfo culture) {
			if (!value.HasValue) return "--";

			return value.Value.ToString(DateTimeFormat.AbbrLongDatePattern);
		}
	}

    public sealed class DateTimeToDateTimeOffsetConverter : MvxValueConverter<DateTime, DateTimeOffset> {
        protected override DateTimeOffset Convert(DateTime value, Type targetType, object parameter, CultureInfo culture) {
            return new DateTimeOffset(value);
        }

        protected override DateTime ConvertBack(DateTimeOffset value, Type targetType, object parameter, CultureInfo culture) {
            return value.DateTime;
        }
    }
}
