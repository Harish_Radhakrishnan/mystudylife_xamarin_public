﻿using System;
using MvvmCross.Converters;
using MyStudyLife.Globalization;

namespace MyStudyLife.UI.Converters {
    public class TimeSpanToShortStringConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is TimeSpan)
                return ((TimeSpan)value).ToShortTimeStringEx();
            else if (value is DateTime)
                return ((DateTime)value).ToShortTimeStringEx();
            else
                return "--";
        }
    }
}
