﻿using System;
using MvvmCross.Converters;
using MyStudyLife.Data;

namespace MyStudyLife.UI.Converters {
	// These look necessary but unfortunately they're not.

	public class DaysOfWeekToIntConverter : MvxValueConverter<DaysOfWeek?, int> {
		protected override int Convert(DaysOfWeek? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (int) value.GetValueOrDefault();
		}

		protected override DaysOfWeek? ConvertBack(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (DaysOfWeek?)value;
		}
	}

	public class RotationDaysToIntConverter : MvxValueConverter<RotationDays?, int> {
		protected override int Convert(RotationDays? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (int)value.GetValueOrDefault();
		}

		protected override RotationDays? ConvertBack(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (RotationDays?)value;
		}
	}

	public class ClassTypeToIntConverter : MvxValueConverter<ClassType?, int> {
		protected override int Convert(ClassType? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (int)value.GetValueOrDefault();
		}

		protected override ClassType? ConvertBack(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (ClassType?)value;
		}
	}

	public class ClassDayTypeToIntConverter : MvxValueConverter<ClassDayType?, int> {
		protected override int Convert(ClassDayType? value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (int)value.GetValueOrDefault();
		}

		protected override ClassDayType? ConvertBack(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
			return (ClassDayType?)value;
		}
	}
}
