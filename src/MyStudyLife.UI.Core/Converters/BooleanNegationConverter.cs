﻿using System;
using MvvmCross.Converters;

namespace MyStudyLife.UI.Converters {
    public class BooleanNegationConverter : MvxValueConverter {
        public override object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            if (value is bool) {
                return !(bool) value;
            }

            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return Convert(value, targetType, parameter, culture);
        }
    }
}
