﻿using System;
using System.Globalization;
using MvvmCross.Plugin.Color;

namespace MyStudyLife.UI.Converters {
    public sealed class DueDateToColorConverter : MvxColorValueConverter<DateTime> {
        protected override System.Drawing.Color Convert(DateTime value, object parameter, CultureInfo culture) {
            if (value < DateTimeEx.Now) {
                return System.Drawing.Color.FromArgb(141, 28, 52);
            }

            return System.Drawing.Color.FromArgb(43, 43, 43);
        }
    }
}
