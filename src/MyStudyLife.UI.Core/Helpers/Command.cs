﻿using System;
using System.Windows.Input;
using MvvmCross.Commands;

namespace MyStudyLife.UI.Helpers {
    public static class Command {
        public static MvxCommand Create(ref MvxCommand? store, Action a, Func<bool> canExecute = null) {
            return store ??= new MvxCommand(a, canExecute);
        }

        public static ICommand Create(ref ICommand? store, Action a, Func<bool> canExecute = null) {
            return store ??= new MvxCommand(a, canExecute);
        }

        public static MvxCommand<T> Create<T>(ref MvxCommand<T>? store, Action<T> a, Func<T, bool> canExecute = null) {
            return store ??= new MvxCommand<T>(a, canExecute);
        }

        public static ICommand Create<T>(ref ICommand? store, Action<T> a, Func<T, bool> canExecute = null) {
            return store ??= new MvxCommand<T>(a, canExecute);
        }

        public static ICommand Create(Action a) => new MvxCommand(a);
        public static ICommand Create<T>(Action<T> a) => new MvxCommand<T>(a);
    }
}
