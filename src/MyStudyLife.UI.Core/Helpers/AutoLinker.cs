﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using RegexMatch = System.Text.RegularExpressions.Match;

namespace MyStudyLife.UI.Helpers {
    public static class AutoLinker {
        /// <summary>
        ///     Match protocol, allow in format "http://" or "mailto:".
        ///     However, do not match the first part of something like 'link:http://www.google.com' (i.e. don't match "link:").
        ///     Also, make sure we don't interpret 'google.com:8000' as if 'google.com' was a protocol here (i.e. ignore a trailing port number in this regex)
        /// </summary>
        private static readonly Regex ProtocolRegex = new Regex(@"(?:[A-Za-z][-.+A-Za-z0-9]+:(?![A-Za-z][-.+A-Za-z0-9]+:\/\/)(?!\d+\/?)(?:\/\/)?)");

        private static readonly Regex WwwRegex = new Regex(@"(?:www\.)");

        private static readonly Regex DomainNameRegex = new Regex(@"[A-Za-z0-9\.\-]*[A-Za-z0-9\-]");

        /// <summary>
        ///     Known TLDs.
        /// </summary>
        private static readonly Regex TldRegex = new Regex(@"\.(?:international|construction|contractors|enterprises|photography|productions|foundation|immobilien|industries|management|properties|technology|christmas|community|directory|education|equipment|institute|marketing|solutions|vacations|bargains|boutique|builders|catering|cleaning|clothing|computer|democrat|diamonds|graphics|holdings|lighting|partners|plumbing|supplies|training|ventures|academy|careers|company|cruises|domains|exposed|flights|florist|gallery|guitars|holiday|kitchen|neustar|okinawa|recipes|rentals|reviews|shiksha|singles|support|systems|agency|berlin|camera|center|coffee|condos|dating|estate|events|expert|futbol|kaufen|luxury|maison|monash|museum|nagoya|photos|repair|report|social|supply|tattoo|tienda|travel|viajes|villas|vision|voting|voyage|actor|build|cards|cheap|codes|dance|email|glass|house|mango|ninja|parts|photo|shoes|solar|today|tokyo|tools|watch|works|aero|arpa|asia|best|bike|blue|buzz|camp|club|cool|coop|farm|fish|gift|guru|info|jobs|kiwi|kred|land|limo|link|menu|mobi|moda|name|pics|pink|post|qpon|rich|ruhr|sexy|tips|vote|voto|wang|wien|wiki|zone|bar|bid|biz|cab|cat|ceo|com|edu|gov|int|kim|mil|net|onl|org|pro|pub|red|tel|uno|wed|xxx|xyz|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cw|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|za|zm|zw)\b");

        /// <summary>
        ///     Allow optional path, query string, and hash anchor, not ending in the following characters: "?!:,.;"
        ///     http://blog.codinghorror.com/the-problem-with-urls/
        /// </summary>
        private static readonly Regex UrlSuffixRegex = new Regex(@"[\-A-Za-z0-9+&@#\/%=~_()|'$*\[\]?!:,.;]*[\-A-Za-z0-9+&@#\/%=~_()|'$*\[\]]");

        private static Regex _matcherRegex;

        private static Regex MatcherRegex {
            get {
                if (_matcherRegex != null) {
                    return _matcherRegex;
                }

                return _matcherRegex = new Regex(String.Concat(
                    "(", // *** Capturing group $1, which is used to match a URL
                        "(?:", // Parens to cover match for protocol (optional), and domain
                            "(", // *** Capturing group $2, for a protocol-prefixed url (ex: http://google.com)
                                ProtocolRegex,
                                DomainNameRegex,
                            ")",

                            "|",

                            "(?:",
                                "(.?//)?",
                                WwwRegex, // non-capturing paren for a 'www.' prefixed url (ex: www.google.com)
                                DomainNameRegex, // *** Capturing group $3 for an optional protocol-relative URL. Must be at the beginning of the string or start with a non-word character
                            ")",

                            "|",

                            "(?:", // Non-capturing paren for known a TLD url (ex: google.com)
                                "(.?//)?", // *** Capturing group $4 for an optional protocol-relative URL. Must be at the beginning of the string or start with a non-word character
                                DomainNameRegex,
                                TldRegex,
                            ")",
                        ")",

                        "(?:" + UrlSuffixRegex + ")?", // match for path, query string, and/or hash anchor - optional
                    ")"
                ), RegexOptions.IgnoreCase);
            }
        }

        /// <summary>
        ///      The regular expression used to retrieve the character before a protocol-relative URL match. 
        /// 
        ///     This is used in conjunction with the {@link #matcherRegex}, which needs to grab the character before a protocol-relative
        ///     '//' due to the lack of a negative look-behind in JavaScript regular expressions. The character before the match is stripped
        ///     from the URL.
        /// </summary>
        private static readonly Regex CharBeforeProtocolRelMatchRegex = new Regex(@"^(.)?\/\/");

        public static IEnumerable<Match> GetMatches(string text) {
            return MatcherRegex.Matches(text).Cast<System.Text.RegularExpressions.Match>().Select(ProcessCandidateMatch).Where(x => x != null);
        }

        public static string Link(string text) {
            return MatcherRegex.Replace(text, m => {
                var match = ProcessCandidateMatch(m);

                return match != null ? String.Concat("<a href=\"", match.Uri, "\">", match.Text, "</a>") : m.Value;
            });
        }

        private static Match ProcessCandidateMatch(System.Text.RegularExpressions.Match match) {
            return ProcessCandidateMatch(
                match.Value,
                match.Groups[1],
                match.Groups[2],
                match.Groups[3],
                match.Groups[4]
            );
        }

        private static Match ProcessCandidateMatch(string match, Group urlMatch, Group protocolUrlMatch, Group wwwProtocolRelativeMatch, Group tldProtocolRelativeMatch) {

            var protocolRelativeMatch = wwwProtocolRelativeMatch.Success ? wwwProtocolRelativeMatch : tldProtocolRelativeMatch;

            // Return out with `null` for match types that are disabled (url, email, twitter), or for matches that are 
            // invalid (false positives from the matcherRegex, which can't use look-behinds since they are unavailable in JS).
            if (!urlMatch.Success || !MatchValidator.IsValidMatch(urlMatch, protocolUrlMatch, protocolRelativeMatch)) {
                return null;
            }

            if (MatchHasUnbalancedClosingParen(match)) {
                match = match.Substring(0, match.Length - 1); // remove the trailing ")"
            }

            // TODO: Email, twitter

            // If it's a protocol-relative '//' match, remove the character before the '//' (which the matcherRegex needed
            // to match due to the lack of a negative look-behind in JavaScript regular expressions)
            // TODO: investigate negative look behind in C#?
            if (protocolRelativeMatch.Success) {
                var charBeforeMatch = CharBeforeProtocolRelMatchRegex.Matches(protocolRelativeMatch.Value);

                // Fix up the `match` if there was a preceding char before a protocol-relative match, which was needed to
                // determine the match itself (since there are no look-behinds in JS regexes)
                if (charBeforeMatch.Count >= 2) {
                    match = match.Substring(1); // remove the prefixed char from the match
                }
            }

            return new UrlMatch(
                urlMatch.Index,
                match,
                protocolUrlMatch.Success,
                protocolRelativeMatch.Success,
                stripPrefix: true // TODO: Make configurable
            );
        }

        /// <summary>
        ///     Determines if a match found has an unmatched closing parenthesis. If so, this parenthesis will be removed
        ///     from the match itself, and appended after the generated anchor tag in {@link #processTextNode}.
        /// 
        ///     A match may have an extra closing parenthesis at the end of the match because the regular expression must include parenthesis
        ///     for URLs such as "wikipedia.com/something_(disambiguation)", which should be auto-linked. 
        /// 
        ///     However, an extra parenthesis *will* be included when the URL itself is wrapped in parenthesis, such as in the case of
        ///     "(wikipedia.com/something_(disambiguation))". In this case, the last closing parenthesis should *not* be part of the URL 
        ///     itself, and this method will return `true`.
        /// </summary>
        /// <param name="match">
        ///     The full match string from the <see cref="MatcherRegex"/>
        /// </param>
        /// <returns>
        ///     True if there is an unbalanced closing parenthesis at the end of the <paramref name="match"/>, false otherwise.
        /// </returns>
        private static bool MatchHasUnbalancedClosingParen(string match) {
            var lastChar = match[match.Length - 1];

            if (lastChar == ')') {
                var openParensMatch = new Regex(@"\(").Match(match);
                var closeParensMatch = new Regex(@"\)").Match(match);

                return openParensMatch.Length < closeParensMatch.Length;
            }

            return false;
        }

        static class MatchValidator {
            /// <summary>
            ///     The regular expression used to check a potential protocol-relative URL match, coming from the 
            ///    <see cref="AutoLinker.MatcherRegex"/> A protocol-relative URL is, for example, "//yahoo.com"
            /// 
            ///    This regular expression checks to see if there is a word character before the '//' match in order to determine if 
            ///    we should actually autolink a protocol-relative URL. This is needed because there is no negative look-behind in 
            ///    JavaScript regular expressions. 
            /// 
            ///    For instance, we want to autolink something like "Go to: //google.com", but we don't want to autolink something 
            ///    like "abc//google.com"
            /// </summary>
            static readonly Regex InvalidProtocolRelMatchRegex = new Regex(@"^[\w]\/\/");

            /// <summary>
            ///     Regex to test for a full protocol, with the two trailing slashes. Ex: 'http://'
            /// </summary>
            static readonly Regex HasFullProtocolRegex = new Regex(@"^[A-Za-z][-.+A-Za-z0-9]+:\/\/");

            /// <summary>
            ///     Regex to find the URI scheme, such as 'mailto:'.
            /// 
            ///     This is used to filter out 'javascript:' and 'vbscript:' schemes.
            /// </summary>
            static readonly Regex UriSchemeRegex = new Regex(@"^[A-Za-z][-.+A-Za-z0-9]+:");

            /// <summary>
            ///     Regex to determine if at least one word char exists after the protocol (i.e. after the ':')
            /// </summary>
            static readonly Regex HasWordCharAfterProtocolRegex = new Regex(@":[^\s]*?[A-Za-z]");

            /// <summary>
            /// 
            /// </summary>
            /// <param name="urlMatch"></param>
            /// <param name="protocolUrlMatch"></param>
            /// <param name="protocolRelativeMatch"></param>
            /// <returns></returns>
            public static bool IsValidMatch(Group urlMatch, Group protocolUrlMatch, Group protocolRelativeMatch) {
                if (
                    (protocolUrlMatch.Success && !IsValidUriScheme(protocolUrlMatch)) ||
                    // At least one period ('.') must exist in the URL match for us to consider it
                    // an actual URL, *unless* it was a full protocol match (like 'http://localhost')
                    UrlMatchDoesNotHaveProtocolOrDot(urlMatch, protocolUrlMatch) ||
                    // At least one letter character must exist in the domain name after a protocol
                    // match. Ex: skip over something like "git:1.0"
                    UrlMatchDoesNotHaveAtLeastOneWordChar(urlMatch, protocolUrlMatch) ||
                    // A protocol-relative match which has a word character in front of it (so we can
                    // skip something like "abc//google.com")
                    IsInvalidProtocolRelativeMatch(protocolRelativeMatch)
                ) {
                    return false;
                }

                return true;
            }

            /// <summary>
            ///     Determines if the URI scheme is a valid scheme to be autolinked. Returns false if the scheme is 
            ///     <c>"javascript:"</c> or <c>"vbscript:"</c>.
            /// </summary>
            /// <param name="uriSchemeMatch">
            ///     The match URL string for a full URI scheme match. Ex: 'http://yahoo.com' or 'mailto:a@a.com'.
            /// </param>
            /// <returns>
            ///     True if the scheme is a valid one, false otherwise.
            /// </returns>
            private static bool IsValidUriScheme(Group uriSchemeMatch) {
                var uriScheme = UriSchemeRegex.Match(uriSchemeMatch.Value);

                return (
                    uriScheme.Success &&
                    !"javascript:".Equals(uriScheme.Value, StringComparison.OrdinalIgnoreCase) &&
                    !"vbscript:".Equals(uriScheme.Value, StringComparison.OrdinalIgnoreCase)
                );
            }

            /// <summary>
            ///     Determines if a URL match does not have either:
            /// 
            ///     a) a full protocol (i.e. 'http://'), or
            ///     b) at least one dot ('.') in the domain name (for a non-full-protocol match).
            /// 
            ///     Either situation is considered an invalid URL (ex: 'git:d' does not have either the '://' part, or at least one dot
            ///     in the domain name. If the match was 'git:abc.com', we would consider this valid.)
            /// </summary>
            /// <param name="urlMatch">
            ///     The matched URL, if there was one. Will be an empty string if the match is not a URL match.
            /// </param>
            /// <param name="protocolUrlMatch">
            ///     The match URL string for a protocol match. Ex: 'http://yahoo.com'. This is used to match
            ///     something like 'http://localhost', where we won't double check that the domain name has at least one '.' in it.
            /// </param>
            /// <returns>
            ///     True if the URL match does not have a full protocol, or at least one dot ('.') in a non-full-protocol match.
            /// </returns>
            private static bool UrlMatchDoesNotHaveProtocolOrDot(Group urlMatch, Group protocolUrlMatch) {
                return urlMatch.Success && (!protocolUrlMatch.Success || !HasFullProtocolRegex.IsMatch(protocolUrlMatch.Value)) && !urlMatch.Value.Contains(".");
            }

            /// <summary>
            ///     Determines if a URL match does not have at least one word character after the protocol (i.e. in the domain name).
            /// 
            ///     At least one letter character must exist in the domain name after a protocol match. Ex: skip over something 
            ///     like "git:1.0"
            /// </summary>
            /// <param name="urlMatch">
            ///     The matched URL, if there was one. Will be an empty string if the match is not a URL match.
            /// </param>
            /// <param name="protocolUrlMatch">
            ///     The match URL string for a protocol match. Ex: 'http://yahoo.com'. This is used to
            ///     know whether or not we have a protocol in the URL string, in order to check for a word character after the protocol
            ///     separator (':').
            /// </param>
            /// <returns>
            ///     True if the URL match does not have at least one word character in it after the protocol, false otherwise.
            /// </returns>
            private static bool UrlMatchDoesNotHaveAtLeastOneWordChar(Group urlMatch, Group protocolUrlMatch) {
                return (
                    urlMatch.Success && protocolUrlMatch.Success && !HasWordCharAfterProtocolRegex.IsMatch(urlMatch.Value)
                );
            }

            /// <summary>
            ///     Determines if a protocol-relative match is an invalid one. This method returns true if there is a <paramref name="protocolRelativeMatch"/>,
            ///     and that match contains a word character before the '//' (i.e. it must contain whitespace or nothing before the '//' in
            ///     order to be considered valid).
            /// </summary>
            /// <param name="protocolRelativeMatch">
            ///     The protocol-relative string for a URL match (i.e. '//'), possibly with a preceding
            ///     character (ex, a space, such as: ' //', or a letter, such as: 'a//'). The match is invalid if there is a word character
            ///     preceding the '//'.
            /// </param>
            /// <returns>
            ///     True if it is an invalid protocol-relative match, false otherwise.
            /// </returns>
            private static bool IsInvalidProtocolRelativeMatch(Group protocolRelativeMatch) {
                return protocolRelativeMatch.Success && InvalidProtocolRelMatchRegex.IsMatch(protocolRelativeMatch.Value);
            }
        }

        public abstract class Match {
            private readonly string _matchedText;
            private readonly int _startIndex;

            public string MatchedText {
                get { return _matchedText; }
            }

            public int StartIndex {
                get { return _startIndex; }
            }

            public int EndIndex {
                get { return _startIndex + _matchedText.Length; }
            }

            public abstract Uri Uri { get; }

            public abstract string Text { get; }

            protected Match(string matchedText, int startIndex) {
                this._matchedText = matchedText;
                this._startIndex = startIndex;
            }
        }

        public class UrlMatch : Match {
            /// <summary>
            ///     A regular expression used to remove the 'http://' or 'https://' and/or the 'www.' from URLs.
            /// </summary>
            static readonly Regex UrlPrefixRegex = new Regex(@"^(https?:\/\/)?(www\.)?", RegexOptions.IgnoreCase);

            /// <summary>
            ///     The regular expression used to remove the protocol-relative '//' from the {@link #url} string, for purposes
            ///     of {@link #getAnchorText}. A protocol-relative URL is, for example, "//yahoo.com"
            /// </summary>
            static readonly Regex ProtocolRelativeRegex = new Regex(@"^\/\/");

            private readonly bool _protocolPrepended;
            private readonly Uri _uri;
            private readonly string _text;

            public bool ProtocolPrepended {
                get { return _protocolPrepended; }
            }

            public override Uri Uri {
                get { return _uri; }
            }

            public override string Text {
                get { return _text; }
            }

            /// <param name="url">The url that was matched.</param>
            /// <param name="protocolUrlMatch">
            ///     True if the URL is a match which already has a protocol (i.e. 'http://'), false if the match was from a 'www' or
            ///     known TLD match.
            /// </param>
            /// <param name="protocolRelativeMatch">
            ///     True if the URL is a protocol-relative match. A protocol-relative match is a URL that starts with '//',
            ///     and will be either http:// or https:// based on the protocol that the site is loaded under.
            /// </param>
            /// <param name="stripPrefix"></param>
            public UrlMatch(int startIndex, string url, bool protocolUrlMatch, bool protocolRelativeMatch, bool stripPrefix)
                : base(url, startIndex) {

                // If the url string doesn't begin with a protocol, assume 'http://'
                if (!protocolUrlMatch && !protocolRelativeMatch) {
                    url = "http://" + url;

                    this._protocolPrepended = true;
                }

                // any &amp;'s in the URL should be converted back to '&' if they were displayed as &amp; in the source html 
                _uri = new Uri(url.Replace("&amp;", "&"));

                _text = url;

                if (protocolRelativeMatch) {
                    // Strip off any protocol-relative '//' from the anchor text
                    _text = StripProtocolRelativePrefix(_text);
                }

                if (stripPrefix) {
                    _text = StripUrlPrefix(_text);
                }

                // Remove trailing slash, if there is one
                _text = RemoveTrailingSlash(_text);
            }

            #region Utils

            /// <summary>
            ///     Strips the URL prefix (such as "http://" or "https://") from the given text.
            /// </summary>
            /// <param name="text">
            ///     The text of the anchor that is being generated, for which to strip off the 
            ///     url prefix (such as stripping off "http://").
            /// </param>
            /// <returns>
            ///     The given <paramref name="text"/>, with the prefix stripped.
            /// </returns>
            private static string StripUrlPrefix(string text) {
                return UrlPrefixRegex.Replace(text, String.Empty);
            }

            /// <summary>
            ///      Strips any protocol-relative '//' from the given text.
            /// </summary>
            /// <param name="text">
            ///     The text of the anchor that is being generated, for which to strip off the
            ///     protocol-relative prefix (such as stripping off "//")
            /// </param>
            /// <returns>
            ///     The given <paramref name="text"/>, with the protocol-relative prefix stripped.
            /// </returns>
            private static string StripProtocolRelativePrefix(string text) {
                return ProtocolRelativeRegex.Replace(text, String.Empty);
            }

            /// <summary>
            ///     Removes any trailing slash from the given `anchorText`, in preparation for the text to be displayed.
            /// </summary>
            /// <param name="text">
            ///     The text of the anchor that is being generated, for which to remove any trailing slash ('/') that may exist.
            /// </param>
            /// <returns>
            ///     The given <paramref name="text"/>, with the trailing slash removed.
            /// </returns>
            private static string RemoveTrailingSlash(string text) {
                return text[text.Length - 1] == '/' ? text.Substring(0, text.Length - 1) : text;
            }

            #endregion
        }
    }
}
