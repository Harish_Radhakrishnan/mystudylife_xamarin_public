using System;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Plugin.Messenger;
using MvvmCross.Presenters;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Data.Caching;
using MyStudyLife.Data.Serialization;
using MyStudyLife.Data.Services;
using MyStudyLife.Data.Store;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.Net;
using MyStudyLife.Reminders;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Caching;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Presentation.DeepLinking;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Services;
using MyStudyLife.Utility;
using MyStudyLife.Versioning;
using Newtonsoft.Json;
using Task = System.Threading.Tasks.Task;
using UserTask = MyStudyLife.Data.Task;
using MyStudyLife.UI.ViewModels;
using MvvmCross.ViewModels;
using MvvmCross.Views;

namespace MyStudyLife.UI {
    public class MslApp : MvxApplication {
        public static MslApp Current { get; private set; }

        public MslApp() {
            if (Current != null) {
                throw new Exception("Cannot create more than one instance of MslApp");
            }

            Current = this;
        }

        public override void Initialize() {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings {
                ContractResolver = MslJsonContractResolver.Default
            };
            
#if DEBUG
            Mvx.IoCProvider.TryResolve(out IDateTimeProvider dateTimeProvider);
            Mvx.IoCProvider.RegisterSingleton<IDateTimeProvider>(new MockDateTimeProvider(dateTimeProvider));
#endif

            Mvx.IoCProvider.RegisterSingleton<ICache>(() => Cache.Current);

            Mvx.IoCProvider.RegisterType<IHttpApiClient, HttpApiClient>();
            
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ISyncService, SyncService>();

            Mvx.IoCProvider.RegisterType<IAuthorizationService, AuthorizationService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IDataStore, DataStore>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ISingletonDataStore, DataStore>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IUserStore, UserStore>();

            Mvx.IoCProvider.RegisterType<IAcademicYearRepository, AcademicYearRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<AcademicYear>, AcademicYearRepository>();
            Mvx.IoCProvider.RegisterType<ISubjectRepository, SubjectRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<Subject>, SubjectRepository>();
            Mvx.IoCProvider.RegisterType<IClassRepository, ClassRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<Class>, ClassRepository>();
            Mvx.IoCProvider.RegisterType<ITaskRepository, TaskRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<UserTask>, TaskRepository>();
            Mvx.IoCProvider.RegisterType<IExamRepository, ExamRepository>();
            Mvx.IoCProvider.RegisterType<IRepository<Exam>, ExamRepository>();

            Mvx.IoCProvider.RegisterType<ISubjectDependentRepository<Class>, ClassRepository>();
            Mvx.IoCProvider.RegisterType<ISubjectDependentRepository<UserTask>, TaskRepository>();
            Mvx.IoCProvider.RegisterType<ISubjectDependentRepository<Exam>, ExamRepository>();

            Mvx.IoCProvider.RegisterType<IUserService, UserService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAgendaScheduler, AgendaScheduler>();

            Mvx.IoCProvider.RegisterType<ICatalog, Catalog>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IInputCacheService<AcademicYear>, InputCacheService<AcademicYear>>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IInputCacheService<Class>, InputCacheService<Class>>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IReminderService, ReminderService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ITimerService, TimerService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IAppStateManager, AppStateManager>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IUpgradeService, UpgradeService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IRemoteAssetCache, RemoteAssetCache>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IFeatureService, FeatureService>();

            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<ITimetableSettingsStore, TimetableSettingsStore>();

            Mvx.IoCProvider.RegisterType<IFeedbackService, FeedbackService>();

            Mvx.IoCProvider.RegisterType<IDeepLinkDispatcher, DeepLinkDispatcher>();

            Mvx.IoCProvider.CallbackWhenRegistered<IMvxViewPresenter>(RegisterPresentationHintHandlers);

            RegisterCustomAppStart<MslAppStart>();

            Task.Run(DeferredInitialize).ConfigureAwait(false);
        }

        private void RegisterPresentationHintHandlers(IMvxViewPresenter presenterDelegate) {
            presenterDelegate.AddPresentationHintHandler<UpgradeCompletePresentationHint>(HandleUpgradeCompletePresentationHint);
            presenterDelegate.AddPresentationHintHandler<AuthorizedPresentationHint>(HandleAuthorizedPresentationHint);
        }

        private async Task<bool> HandleUpgradeCompletePresentationHint(UpgradeCompletePresentationHint hint) {
            var authService = Mvx.IoCProvider.Resolve<IAuthorizationService>();

            Type viewModelType;

            if (!authService.IsAuthorized) {
                viewModelType = typeof(SignInViewModel);
            }
            else {
                var appState = await Mvx.IoCProvider.Resolve<IAppStateManager>().GetStateAsync();
                var user = appState.User;
                if (user == null) {
                    throw new InvalidOperationException("IAppState.SetUser should be called before calling ChangePresentation from UpgradeViewModel");
                }

                viewModelType = Mvx.IoCProvider.Resolve<INavigationService>().GetFirstViewAuthorized(user);
            }

            return await Mvx.IoCProvider.Resolve<INavigationService>().Navigate(viewModelType);
        }

        private async Task<bool> HandleAuthorizedPresentationHint(AuthorizedPresentationHint hint) {
            var appState = await Mvx.IoCProvider.Resolve<IAppStateManager>().GetStateAsync();
            var user = appState.User;
            if (user == null) {
                throw new InvalidOperationException("IAppState.SetUser should be called before calling ChangePresentation from SignInViewModel");
            }

            return await Mvx.IoCProvider.Resolve<INavigationService>().NavigateFirstViewAuthorized(user);
        }

        /// <summary>
        ///     Initialization that the app is not dependent on to create view models,
        ///     so can be waited for by the view model.
        /// </summary>
        /// <remarks>
        ///     MslApp will eventually be initialized from the background, so we can't
        ///     shouldn't do anything here other than "setup" - no triggering sync or
        ///     showing dialogs.
        /// </remarks>
        private void DeferredInitialize() {
            var appStateManager = Mvx.IoCProvider.Resolve<IAppStateManager>();
            var analyticsService = Mvx.IoCProvider.Resolve<IAnalyticsService>();
            var messenger = Mvx.IoCProvider.Resolve<IMvxMessenger>();
            var syncService = Mvx.IoCProvider.Resolve<ISyncService>();
            var networkProvider = Mvx.IoCProvider.Resolve<INetworkProvider>();

            analyticsService.EnsureInitialized();

            messenger.ProxyEvent<SyncStatusChangedEventArgs>(syncService, nameof(syncService.StatusChanged));
            messenger.ProxyEvent<SyncCompletedEventArgs>(syncService, nameof(syncService.Completed));
            syncService.Error += SyncServiceOnError;

            networkProvider.ConnectivityChanged += NetworkProviderOnConnectivityChanged;

            appStateManager.EnsureInitialized();
        }

        private void SyncServiceOnError(object sender, SyncErrorEventArgs e) {
            Mvx.IoCProvider.GetSingleton<IMvxMessenger>().Publish(ProxyEventMessage.For(sender, e));

            if (e.Exception?.GetType() == typeof(HttpApiUnauthorizedException)) {
                var sp = Mvx.IoCProvider.Resolve<IStorageProvider>();

                var at = AccessToken.GetCurrent(sp);

                at?.Remove(sp);

                Mvx.IoCProvider.Resolve<IMvxViewDispatcher>().ChangePresentation(new AuthorizationChangedPresentationHint(AuthStatusChangeCause.UnauthorizedHttpResponse));
            }
        }

        private void NetworkProviderOnConnectivityChanged(object sender, NetworkConnectivityChangedEventArgs e) {
            Mvx.IoCProvider.GetSingleton<IMvxMessenger>().Publish(ProxyEventMessage.For(sender, e));

            if (!e.WasConnectionMetered && e.IsConnectionMetered) {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowNotification(
                    R.Network.MeteredConnectionTitle,
                    String.Format(R.Network.MeteredConnectionMessage, e.IsConnectionDisabled ? R.Common.Disabled : R.Common.Enabled)
                );
            }

            if (!e.CouldUseConnection && e.CanUseConnection && Mvx.IoCProvider.Resolve<IAuthorizationService>().IsAuthorized) {
                Mvx.IoCProvider.GetSingleton<ISyncService>().TrySync(SyncTriggerKind.Programmatic);
            }
        }
    }
}