﻿using MvvmCross;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Messaging;
using MyStudyLife.Sync;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Utilities;
using PropertyChanged;
using System;
using System.Windows.Input;
using System.ComponentModel;
using MyStudyLife.UI.Caching;
using System.Collections.Generic;
using FFImageLoading.Transformations;
using FFImageLoading.Work;
using MvvmCross.Base;
using MvvmCross.Commands;
using MyStudyLife.Authorization;
using MyStudyLife.UI.Analytics;

namespace MyStudyLife.UI.ViewModels.Components {
    public class NavigationDrawerViewModel : MvxViewModel {
        private readonly INavigationService _navigationService;
        private readonly IUserStore _userStore;

        // ReSharper disable once NotAccessedField.Local
        private readonly IDisposable[] _subscriptions;

        public bool IsOpen { get; set; }

        #region User

        public User User { get; private set; }

        public Uri UserPicture { get; private set; }
        
        public bool HasSchool { get; private set; }

        [DependsOn(nameof(SchoolLogo))]
        public bool HasSchoolLogo => SchoolLogo != null;

        [DependsOn(nameof(HasSchool), nameof(SchoolLogo))]
        public bool HasSchoolWithoutLogo => HasSchool && SchoolLogo == null;

        public SchoolLogo SchoolLogo { get; private set; }

        public bool SchoolLogoLoaded { get; set; }

        public bool HasStandardSchoolLogo { get; private set; }
        public bool HasCoverSchoolLogo { get; private set; }
        public bool HasWideSchoolLogo { get; private set; }
        public bool HasEmblemSchoolLogo { get; private set; }

        public CachedImage SchoolStandardLogo { get; private set; }
        public CachedImage SchoolWideLogo { get; private set; }
        public CachedImage SchoolCoverLogo { get; private set; }
        public CachedImage SchoolEmblemLogo { get; private set; }

        #endregion

        #region Sync

        // TODO: Throttled (waiting), offline?

        private readonly IAnalyticsService _analyticsService;
        private readonly ISyncService _syncService;
        private bool _isSyncing;
        private MvxCommand _triggerSyncCommand;

        public bool IsSyncing {
            get => _isSyncing;
            private set {
                _isSyncing = value;
                _triggerSyncCommand?.RaiseCanExecuteChanged();
            }
        }

        public string SyncStatusMessage { get; private set; }

        public ICommand TriggerSyncCommand => Command.Create(
            ref _triggerSyncCommand,
            () => {
                _analyticsService.TrackEvent("App", EventActions.Click, "Sync");
                _syncService.TrySync(SyncTriggerKind.Manual);
             },
            () => !IsSyncing
        );

        #endregion

        public NavigationDrawerViewModel(
            INavigationService navigationService,
            IMvxMessenger messenger,
            IUserStore userStore,
            ISyncService syncService,
            IAnalyticsService analyticsService
        ) {
            _subscriptions = new IDisposable[] {
                messenger.SubscribeOnMainThread<AuthStatusChangedEventMessage>(OnAuthStatusChanged),
                messenger.SubscribeOnMainThread<ProxyEventMessage<SyncStatusChangedEventArgs>>(SyncServiceOnStatusChanged),
                messenger.SubscribeOnMainThread<ProxyEventMessage<SyncCompletedEventArgs>>(SyncServiceOnCompleted)
            };

            _navigationService = navigationService;
            _userStore = userStore;
            _syncService = syncService;
            _analyticsService = analyticsService;
        }

        public override void Start() {
            base.Start();

            Invalidate();
        }

        private void Invalidate() {
            var user = _userStore.GetCurrentUser();

            User = user;
            UserPicture = null; // user?.Picture.ToString();
            HasSchool = user != null && user.HasSchool;
            SchoolLogo = HasSchool && user != null && user.School.HasLogo ? user.School.Logo : null;

            if (SchoolLogo != null) {
                // TODO: These should be set based on testing the URL
                SchoolStandardLogo = new CachedImage("school-logo", SchoolLogo.Standard);
                SchoolWideLogo = new CachedImage(
                    new KeyValuePair<string, string>("school-logo-wide", SchoolLogo.Wide),
                    new KeyValuePair<string, string>("school-logo", SchoolLogo.Standard)
                );
                SchoolCoverLogo = new CachedImage(
                    new KeyValuePair<string, string>("school-logo-cover", SchoolLogo.Cover),
                    new KeyValuePair<string, string>("school-logo", SchoolLogo.Standard)
                );
                SchoolEmblemLogo = new CachedImage("school-logo-emblem", SchoolLogo.Emblem);

                SchoolStandardLogo.PropertyChanged += LogoImageOnPropertyChanged;
                SchoolWideLogo.PropertyChanged += LogoImageOnPropertyChanged;
                SchoolCoverLogo.PropertyChanged += LogoImageOnPropertyChanged;
                SchoolEmblemLogo.PropertyChanged += LogoImageOnPropertyChanged;

                HasStandardSchoolLogo = !String.IsNullOrEmpty(SchoolLogo.Standard);
                HasCoverSchoolLogo = !String.IsNullOrEmpty(SchoolLogo.Cover);
                HasWideSchoolLogo = !String.IsNullOrEmpty(SchoolLogo.Wide);
                HasEmblemSchoolLogo = !String.IsNullOrEmpty(SchoolLogo.Emblem);
            }
            else {
                SchoolLogoLoaded = false;

                SchoolStandardLogo = null;
                SchoolWideLogo = null;
                SchoolCoverLogo = null;
                SchoolEmblemLogo = null;

                HasStandardSchoolLogo = false;
                HasCoverSchoolLogo = false;
                HasWideSchoolLogo = false;
                HasEmblemSchoolLogo = false;
            }

            UpdateSyncStatus();
        }

        private void LogoImageOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(CachedImage.IsLoaded)) {
                SchoolLogoLoaded = true;
            }
        }

        private void UpdateSyncStatus() {
            bool isSyncing = _syncService.Status == SyncStatus.Syncing;

            IsSyncing = isSyncing;

            if (isSyncing) {
                SyncStatusMessage = R.SyncingWithEllipsis;
            }
            else {
                DateTime? lastUpdated = _syncService.LastUpdated;

                SyncStatusMessage = String.Format(
                    R.LastSyncedFormat,
                    lastUpdated.HasValue
                        ? lastUpdated.Value.ToLocalTime().ToRelativeTimeString()
                        : R.never
                );
            }
        }

        [SuppressPropertyChangedWarnings]
        private void OnAuthStatusChanged(AuthStatusChangedEventMessage msg) => Invalidate();

        private void SyncServiceOnStatusChanged(ProxyEventMessage<SyncStatusChangedEventArgs> msg) => UpdateSyncStatus();

        private void SyncServiceOnCompleted(ProxyEventMessage<SyncCompletedEventArgs> msg) {
            if (msg.Args.UserModified || msg.Args.UserSettingsModified) {
                Invalidate();
            }
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsOpen) && IsOpen) {
                UpdateSyncStatus();
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        public class CachedImage : MvxNotifyPropertyChanged {
            private static Lazy<IRemoteAssetCache> _remoteAssetCache = new Lazy<IRemoteAssetCache>(() => Mvx.IoCProvider.Resolve<IRemoteAssetCache>());

            private readonly KeyValuePair<string, string>[] _images;

            private string _path;

            public bool IsLoaded { get; private set; }

            public bool IsLoading { get; private set; }

            public bool LoadFailed { get; private set; }

            public bool IsFallback { get; private set; }

            public KeyValuePair<string, string> LoadedImage { get; private set; }

            [DoNotNotify]
            public string Path {
                get {
                    if (_path == null && !IsLoaded && !IsLoading) {
                        LoadAsync();
                    }

                    return _path;
                }
            }

            public CachedImage(string key, string imageUrl) {
                _images = new[] {
                    new KeyValuePair<string, string>(key, imageUrl)
                };
            }

            public CachedImage(params KeyValuePair<string, string>[] images) {
                _images = images;
            }

            private void LoadAsync() {
                IsLoading = true;

                LoadImage(0);
            }

            private void LoadImage(int index) {
                var image = _images[index];

                Action onError = () => {
                    int nextIndex = index + 1;

                    if (nextIndex <= (_images.Length - 1)) {
                        LoadImage(nextIndex);
                    }
                    else {
                        IsLoaded = IsLoading = false;
                        LoadFailed = true;
                    }
                };

                if (String.IsNullOrEmpty(image.Value)) {
                    onError();
                    return;
                }

                _remoteAssetCache.Value.RequestFile(image.Key, image.Value,
                    localPath => Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                        _path = localPath;

                        IsLoading = false;
                        IsLoaded = true;
                        LoadFailed = false;

                        if (index > 0) {
                            IsFallback = true;
                        }

                        LoadedImage = _images[index];

                        RaisePropertyChanged(nameof(Path));
                    }),
                    onError
                );
            }
        }
    }
}
