﻿using MvvmCross;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Versioning;
using MyStudyLife.Extensions;

namespace MyStudyLife.UI.ViewModels.Settings {
    public class AboutViewModel : BaseViewModel {
        public string AppVersion { get; set; }

        public AboutViewModel() {
            this.AppVersion = Mvx.IoCProvider.Resolve<IUpgradeService>().CurrentAppVersion.ToHumanString();
        }
    }
}
