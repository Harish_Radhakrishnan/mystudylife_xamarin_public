﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Extensions;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.Tasks;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Settings {
    public sealed class TimetableSettingsViewModel : BaseSettingsViewModel {
        private ChangeTracker _settingsChangeTracker;

        public TimetableSettings Settings { get; private set; }

        [UsedImplicitly]
        private IDisposable _subscription;

        private readonly int[] _rotationWeekCountOptions = { 2, 3, 4 };
        private readonly ObservableCollection<KVP<int, string>> _rotationWeeks = new ObservableCollection<KVP<int, string>>();
        private readonly int[] _dayCountOptions = { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        private readonly ObservableCollection<KVP<int, string>> _rotationDays = new ObservableCollection<KVP<int, string>>();

        public IEnumerable<int> RotationWeekCountOptions {
            get { return _rotationWeekCountOptions; }
        }
        public ObservableCollection<KVP<int, string>> RotationWeeks {
            get { return _rotationWeeks; }
        }

        public IEnumerable<int> RotationDayCountOptions {
            get { return _dayCountOptions; }
        }
        public ObservableCollection<KVP<int, string>> RotationDays {
            get { return _rotationDays; }
        }

        public ICollection<Tuple<int, string, int>> Days { get; private set; }

        public DaysOfWeek TimetableDays {
            get { return this.Settings != null ? this.Settings.Days : DaysOfWeek.None; }
            set {
                var days = (int)value;

                int i = 0;
                while (EnumEx.GetSetFlagCount(days) < 4) {
                    int bit = 1 << i;

                    if ((days & bit) == 0) {
                        days = (days | bit);
                    }

                    i++;

                    if (i >= 7) break;
                }

                this.Settings.Days = (DaysOfWeek)days;

                RaisePropertyChanged(() => this.TimetableDaysText);
            }
        }
        public string TimetableDaysText {
            get {
                var days = new List<string>();

                for (int i = 0; i < 7; i++) {
                    if ((this.TimetableDays & (DaysOfWeek)(1 << i)) != 0) {
                        days.Add(DateTimeFormat.ShortDayNames[i]);
                    }
                }

                return String.Join(", ", days);
            }
        }

        #region Android Specifics (lack of SelectedValuePath)

        public KVP<int, string> SetWeekValue {
            get { return this.RotationWeeks.FirstOrDefault(x => x.Key == this.Settings.SetWeek); }
            set {
                this.Settings.SetWeek = value == null ? 0 : value.Key;
            }
        }

        public KVP<int, string> SetDayValue {
            get { return this.RotationDays.FirstOrDefault(x => x.Key == this.Settings.SetDay); }
            set {
                this.Settings.SetDay = value == null ? 0 : value.Key;
            }
        }

        #endregion

        public Uri UpgradeGuideUri {
            get { return new Uri("http://feedback.mystudylife.com/knowledgebase/articles/498459"); }
        }

        private ICommand _upgradeGuideCommand;

        public ICommand UpgradeGuideCommand {
            get { return Command.Create(ref _upgradeGuideCommand, ViewUpgradeGuide); }
        }

        private void ViewUpgradeGuide() {
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(this.UpgradeGuideUri);   
        }

        public override async void Start() {
            base.Start();

            this.Days = DateTimeFormat.ShortDays.Select((x, i) => new Tuple<int, string, int>(1 << (int)x.DayOfWeek, x.Name, i)).ToList();

            var tSettings = await TimetableSettings.Get(Mvx.IoCProvider.Resolve<ISingletonDataStore>());

            SetAvailableWeeksAndDays(tSettings);

            this.Settings = tSettings.Clone();

            if (this.Settings.IsWeekRotation) {
                this.Settings.SetWeek = this.Settings.GetCurrentWeek();
            }

            if (this.Settings.IsDayRotation) {
                this.Settings.SetDay = this.Settings.GetDayForCurrentDate().GetValueOrDefault();
            }

            _settingsChangeTracker = this.Settings.TrackChanges();

            _subscription = this.Settings.WeakSubscribe(TimetableSettingsOnPropertyChanged);
        }

        public void TimetableSettingsOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            bool raiseSetWeekValue = false, raiseSetDayValue = false;

            if (e.PropertyName == "Mode") {
                this.Settings.SetDefaults();

                raiseSetWeekValue = this.Settings.IsWeekRotation;
                raiseSetDayValue = this.Settings.IsDayRotation;
            }
            else if (e.PropertyName.In("DayCount", "WeekCount")) {
                SetAvailableWeeksAndDays();
            }
            else if (e.PropertyName == "UseLetters") {
                foreach (var week in this.RotationWeeks) {
                    week.Value = User.Current.Settings.IsRotationScheduleLettered ? week.Key.ToLetter() : week.Key.ToString();
                }

                foreach (var day in this.RotationDays) {
                    day.Value = User.Current.Settings.IsRotationScheduleLettered ? day.Key.ToLetter() : day.Key.ToString();
                }
            }
            else if (e.PropertyName.In("SetWeek", "SetDay")) {
                this.Settings.Set = DateTime.UtcNow;
            }

            if (raiseSetWeekValue || (e.PropertyName == "SetWeek" && (this.SetWeekValue == null || this.SetWeekValue.Key != this.Settings.SetWeek))) {
                RaisePropertyChanged(() => this.SetWeekValue);
            }

            if (raiseSetDayValue || (e.PropertyName == "SetDay" && (this.SetDayValue == null || this.SetDayValue.Key != this.Settings.SetDay))) {
                RaisePropertyChanged(() => this.SetDayValue);
            }
        }

        private void SetAvailableWeeksAndDays(TimetableSettings tSettings = null) {
            tSettings = tSettings ?? this.Settings;

            if (tSettings == null) {
                return;
            }

            if (tSettings.IsWeekRotation) {
                SetWeeksOrDaysFromCount(this.RotationWeeks, tSettings.WeekCount.GetValueOrDefault(TimetableSettings.DefaultRotationWeekCount));

                if (tSettings.SetWeek > tSettings.WeekCount) {
                    tSettings.SetWeek = tSettings.WeekCount;
                }
            }

            if (tSettings.IsDayRotation) {
                SetWeeksOrDaysFromCount(this.RotationDays, tSettings.DayCount.GetValueOrDefault(TimetableSettings.DefaultRotationDayCount));

                if (tSettings.SetDay > tSettings.DayCount) {
                    tSettings.SetDay = tSettings.DayCount;
                }
            }
        }

        private void SetWeeksOrDaysFromCount(ObservableCollection<KVP<int, string>> items, int count) {
            if (items.Count < count) {
                for (var i = (short)(items.Count + 1); i <= count; i++) {
                    items.Add(new KVP<int, string>(i,
                        User.Current.Settings.IsRotationScheduleLettered ? Humanize.NumberToLetter(i) : i.ToString()));
                }
            }
            else if (count < items.Count) {
                for (var i = items.Count; i > count; i--) {
                    items.RemoveAt(i - 1);
                }
            }
        }

        protected override async Task SaveChangesAsync() {
            if (_settingsChangeTracker.HasChanges() && (User.School == null || User.IsTeacher)) {
                Settings.Purge();

                Settings.UpdatedAt = DateTime.UtcNow;
                Settings.Syncify();

                await Settings.Save(Mvx.IoCProvider.Resolve<ISingletonDataStore>());

                this.Messenger.Publish(new TimetableSettingsChangedEventMessage(this, this.Settings));

                Mvx.IoCProvider.Resolve<ISyncService>().TrySync(SyncTriggerKind.DataChange);
            }
        }
    }
}