﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Settings {
    public sealed class GeneralSettingsViewModel : BaseSettingsViewModel {
        private readonly IUserStore _userStore;

        private ChangeTracker _settingsChangeTracker;

        public UserSettings Settings { get; private set; }

        public ICollection<DayMapping> Days { get; private set; }

        private DayMapping _firstDayOfWeekValue;
        public DayMapping FirstDayOfWeekValue {
            get { return _firstDayOfWeekValue; }
            set {
                _firstDayOfWeekValue = value;

                if (this.Settings != null && value != null) {
                    this.Settings.FirstDayOfWeek = _firstDayOfWeekValue.DayOfWeek;
                }
            }
        }

        public GeneralSettingsViewModel(IUserStore userStore) {
            this._userStore = userStore;
        }

        protected override Task<bool> StartAsync() => base.StartAsync().ContinueWith(_ => {
            this.Days = new List<DayMapping> {
                new DayMapping(DayOfWeek.Sunday, DateTimeFormat.GetDayName(DayOfWeek.Sunday)),
                new DayMapping(DayOfWeek.Monday, DateTimeFormat.GetDayName(DayOfWeek.Monday)),
                new DayMapping(DayOfWeek.Saturday, DateTimeFormat.GetDayName(DayOfWeek.Saturday)),
            };

            this.Settings = User.Settings.Clone();

            this.FirstDayOfWeekValue = this.Days.SingleOrDefault(x => x.DayOfWeek == this.Settings.FirstDayOfWeek);

            _settingsChangeTracker = this.Settings.TrackChanges();

            return true;
        });

        protected override async Task SaveChangesAsync() {
            if (_settingsChangeTracker.HasChanges()) {
                this.Settings.UpdatedAt = DateTime.UtcNow;
                this.Settings.Syncify();

                var tcs = new TaskCompletionSource<object>();

                InvokeOnMainThread(() => {
                    this.Settings.CopyPropertyValuesTo(this.User.Settings);

                    tcs.TrySetResult(null);
                });

                await tcs.Task;

                await _userStore.PersistUserAsync(this.User);

                this.Messenger.Publish(new UserSettingsChangedEventMessage(this, this.Settings));

                Mvx.IoCProvider.Resolve<ISyncService>().TrySync(SyncTriggerKind.DataChange);
            }
        }
    }
}
