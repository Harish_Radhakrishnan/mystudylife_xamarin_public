﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Settings;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Settings {
    public sealed class DashboardSettingsViewModel : BaseSettingsViewModel {
        private readonly IUserStore _userStore;
        private readonly ISyncService _syncService;

        private ChangeTracker _settingsChangeTracker;
        private IDisposable _subscription;

            public DashboardTasksTimescale TasksTimescale { get; set; }
            public DashboardExamsTimescale ExamsTimescale { get; set; }
            public bool ShowExams { get; set; }
            public bool ExamsHideWhenNone { get; set; }

        public IDictionary<DashboardTasksTimescale, string> TasksTimescaleOptions { get; }
        public IDictionary<DashboardExamsTimescale, string> ExamsTimescaleOptions { get; }
        public string ExamsHideWhenEmptyCaption => Catalog.GetString("Hide when no exams within {0}").WithArgs(
            this.ExamsTimescaleOptions[this.ExamsTimescale]
        );

        public DashboardSettingsViewModel(
            IUserStore userStore,
            ISyncService syncService
        ) {
            this._userStore = userStore;
            this._syncService = syncService;

            this.TasksTimescaleOptions = Enum.GetValues(typeof(DashboardTasksTimescale))
                .Cast<DashboardTasksTimescale>()
                .ToDictionary(k => k, v => v.GetTitle());
            this.ExamsTimescaleOptions = Enum.GetValues(typeof(DashboardExamsTimescale))
                .Cast<DashboardExamsTimescale>()
                .ToDictionary(k => k, v => v.GetTitle());
        }

        public override void Start() {
            base.Start();

            var ds = UserSettings.Default;
            var settings = this.User.Settings;

            this.TasksTimescale = settings.DashboardTasksTimescale ?? ds.DashboardTasksTimescale.Value;
            this.ExamsTimescale = settings.DashboardExamsTimescale ?? ds.DashboardExamsTimescale.Value;
            this.ShowExams = !(settings.DashboardExamsHidden ?? ds.DashboardExamsHidden.Value);
            this.ExamsHideWhenNone = settings.DashboardExamsHiddenWhenNone ?? ds.DashboardExamsHiddenWhenNone.Value;

            _settingsChangeTracker = this.TrackChanges();
            _subscription = new MvxPropertyChangedListener(this)
                .Listen(nameof(this.ExamsTimescale), () => RaisePropertyChanged(nameof(ExamsHideWhenEmptyCaption)));
        }

        protected override async Task SaveChangesAsync() {
            if (_settingsChangeTracker.HasChanges()) {
                this.User.Settings.UpdatedAt = DateTime.UtcNow;

                await InvokeOnMainThreadAsync(() => {
                    this.User.Settings.DashboardTasksTimescale = this.TasksTimescale;
                    this.User.Settings.DashboardExamsTimescale = this.ExamsTimescale;
                    this.User.Settings.DashboardExamsHidden = !this.ShowExams;
                    this.User.Settings.DashboardExamsHiddenWhenNone = this.ExamsHideWhenNone;
                });

                await _userStore.PersistUserAsync(this.User);

                this.Messenger.Publish(new UserSettingsChangedEventMessage(this, this.User.Settings));

                _syncService.TrySync(SyncTriggerKind.DataChange);
            }
        }
    }
}
