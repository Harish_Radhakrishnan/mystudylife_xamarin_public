﻿using System;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.ViewModels;
using MyStudyLife.Diagnostics;
using MyStudyLife.UI.ViewModels.Base;

namespace MyStudyLife.UI.ViewModels.Settings {
    public interface ISettingsViewModel : IMvxViewModel {
        void SaveChanges();
    }

    public abstract class BaseSettingsViewModel : BaseViewModel, ISettingsViewModel {
        public async void SaveChanges() {
            // It's an async void, we need the try-catch for bug tracking
            try {
                await SaveChangesAsync();
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);

                throw;
            }
        }

        protected abstract Task SaveChangesAsync();
    }
}
