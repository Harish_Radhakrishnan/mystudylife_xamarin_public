﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyStudyLife.Configuration;
using MyStudyLife.Data.Services;
using MyStudyLife.Sync;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Utility;
using MyStudyLife.Versioning;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.Settings {
    public class SettingsViewModel : BaseViewModel {
        private readonly IUserService _userService;
        private readonly IFeatureService _featureService;
        private readonly ITaskProvider _taskProvider;
        private readonly ISyncService _syncService;

        #region Sign Out

        public bool IsSigningOut { get; private set; }

        private MvxAsyncCommand<bool?> _signOutCommand;

        public ICommand SignOutCommand => _signOutCommand ??= new MvxAsyncCommand<bool?>(SignOut);

        private async Task SignOut(bool? force = false) {
            try {
                this.IsSigningOut = true;

                await _userService.SignOutAsync(force.GetValueOrDefault());
            }
            catch (SignOutException) {
                var result = await DialogService.ShowCustomAsync(
                    "Cannot verify your data is synced.",
                    "Your data may not be up to date and there is no guarantee that unsynced items will be present upon sign in. Would you like to continue to sign out?",
                    "Sign Out",
                    R.Common.Cancel
                );

                if (result.ChosePositiveAction) {
                    await SignOut(true);
                }

                return;
            }
            finally {
                this.IsSigningOut = false;
            }

            await this.NavigationService.ChangePresentation(new AuthorizationChangedPresentationHint(Authorization.AuthStatusChangeCause.SignOut));
        }

        #endregion

        #region About

        private MvxCommand _showFeedbackAndSupportCommand;
        private MvxCommand _showPrivacyPolicyCommand;
        private MvxCommand _showTermsOfUseCommand;
        private MvxCommand _facebookCommand;
        private MvxCommand _twitterCommand;

        public ICommand ShowFeedbackAndSupportCommand => Command.Create(ref _showFeedbackAndSupportCommand,
            () => ShowUrl(MslConfig.Current.FeedbackUri));

        public ICommand ShowPrivacyPolicyCommand => Command.Create(ref _showPrivacyPolicyCommand,
            () => ShowUrl("https://www.mystudylife.com/privacy-policy"));

        public ICommand ShowTermsOfUseCommand => Command.Create(ref _showTermsOfUseCommand,
            () => ShowUrl("https://www.mystudylife.com/terms-of-service"));

        public ICommand FacebookCommand => Command.Create(ref _facebookCommand, _taskProvider.ShowFacebookPage);

        public ICommand TwitterCommand => Command.Create(ref _twitterCommand,
            () => ShowNativeUrl(MslConfig.Current.TwitterProfileDeeplink, MslConfig.Current.TwitterProfileUri));

        private void ShowUrl(string url) => _taskProvider.ShowWebUri(new Uri(url));

        private void ShowNativeUrl(string nativeUri, string fallbackUrl) {
            if (!_taskProvider.TryShowNativeUri(new Uri(nativeUri))) {
                _taskProvider.ShowWebUri(new Uri(fallbackUrl));
            }
        }

        #endregion

        public string AppVersion { get; private set; }

        public bool CanCustomizeDashboard { get; private set; }
        public bool NeedsTimetableSettings { get; private set; }

        public ObservableCollection<SettingsItem> Settings { get; }

        #region "Force" Sync

		private MvxCommand _forceSyncCommand;

        public ICommand ForceSyncCommand =>
            Command.Create(ref _forceSyncCommand, ForceSync, () => _syncService.Status == SyncStatus.Idle);

        private void ForceSync() {
            _syncService.Reset();
            _syncService.TrySync(SyncTriggerKind.Manual);
        }

        #endregion

        public SettingsViewModel(
            IUserService userService,
            IFeatureService featureService,
            ITaskProvider taskProvider,
            ISyncService syncService
        ) {
            this._userService = userService;
            this._featureService = featureService;
            this._taskProvider = taskProvider;
            this._syncService = syncService;

            this.Settings = new[] {
                SettingsItem.For<GeneralSettingsViewModel>(),
                SettingsItem.For<ReminderSettingsViewModel>("Reminders")
            }.ToObservableCollection();
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            this.AppVersion = MslConfig.Current.AppVersionName;

            this.CanCustomizeDashboard = await _featureService.CanUseFeatureAsync(Feature.CustomizeDashboard);
            this.NeedsTimetableSettings = await LegacyHelper.NeedsTimetableSettings();

            if (this.CanCustomizeDashboard) {
                this.Settings.Insert(1, SettingsItem.For<DashboardSettingsViewModel>());
            }

            if (this.NeedsTimetableSettings) {
                this.Settings.Add(SettingsItem.For<TimetableSettingsViewModel>());
            }

            return true;
        }

        [SuppressPropertyChangedWarnings]
	    protected override void OnSyncStatusChanged(SyncStatusChangedEventArgs e) {
		    base.OnSyncStatusChanged(e);

	        this._forceSyncCommand?.RaiseCanExecuteChanged();
	    }

        public class SettingsItem {
            public ICommand Command { get; }

            public string Name { get; }

            public SettingsItem(Type viewModelType) : this(viewModelType, ViewModelHelpers.GetFriendlyName(viewModelType).Replace(" Settings", String.Empty)) { }

            public SettingsItem(Type viewModelType, string name) {
                Check.NotNull(viewModelType, nameof(viewModelType));
                Check.NotNullOrEmpty(ref name, nameof(name));

                this.Name = name;
                this.Command = new MvxAsyncCommand(() =>  Mvx.IoCProvider.Resolve<IMvxNavigationService>().Navigate(viewModelType));
            }

            public static SettingsItem For<TViewModel>() where TViewModel : IMvxViewModel
                => new SettingsItem(typeof(TViewModel));

            public static SettingsItem For<TViewModel>(string name) where TViewModel : IMvxViewModel
                => new SettingsItem(typeof(TViewModel), name);
        }
    }
}
