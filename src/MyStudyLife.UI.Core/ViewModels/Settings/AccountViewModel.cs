﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using MyStudyLife.Data.Services;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Base;

namespace MyStudyLife.UI.ViewModels.Settings {
    public class AccountViewModel : BaseViewModel {
        private readonly IUserService _userService;

        public string UserName { get; set; }
        
        private MvxAsyncCommand<bool?> _signOutCommand;
        public ICommand SignOutCommand => _signOutCommand ??= new MvxAsyncCommand<bool?>(SignOut);

        public AccountViewModel(IUserService userService) {
            _userService = userService;
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(User)) {
                var user = User;

                UserName = user == null ? "No User" : String.Concat(user.FirstName ?? "--", " ", user.LastName ?? "--");
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        private async Task SignOut(bool? force = false) {
            try {
                IsBusy = true;

                await _userService.SignOutAsync(force.GetValueOrDefault());
            }
            catch (SignOutException) {
                var result = await DialogService.ShowCustomAsync(
                    "Cannot verify your data is synced.",
                    "Your data may not be up to date and there is no guarantee that unsynced items will be present upon sign in. Would you like to continue to sign out?",
                    "Sign Out",
                    R.Common.Cancel
                );

                if (result.ChosePositiveAction) {
                    await SignOut(true);
                }
                
                return;
            }
            finally {
                IsBusy = false;
            }

            await NavigationService.ChangePresentation(new AuthorizationChangedPresentationHint(Authorization.AuthStatusChangeCause.SignOut));
        }
    }
}
