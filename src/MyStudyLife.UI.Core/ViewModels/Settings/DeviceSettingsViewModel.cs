﻿using System;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MyStudyLife.Data.Settings;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Reminders;
using MyStudyLife.Sync;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Settings {
    public abstract class DeviceSettingsViewModel : BaseSettingsViewModel {
        private ChangeTracker _settingsChangeTracker;

        public DeviceSettings Settings { get; private set; }

        public override async void Start() {
            base.Start();

            using (var settingsRepo = new SettingsRepository()) {
                this.Settings = (await settingsRepo.GetDeviceSettingsAsync()).Clone();
            }

            _settingsChangeTracker = this.Settings.TrackChanges();
        }

        protected override async Task SaveChangesAsync() {
            if (_settingsChangeTracker.HasChanges()) {
                this.Settings.UpdatedAt = DateTime.UtcNow;
                this.Settings.Syncify();

                using (var settingsRepo = new SettingsRepository()) {
                    await settingsRepo.PersistDeviceSettingsAsync(this.Settings);
                }

                this.Messenger.Publish(new DeviceSettingsChangedEventMessage(this, this.Settings));

                Mvx.IoCProvider.Resolve<ISyncService>().TrySync(SyncTriggerKind.DataChange);
            }
        }
    }

    public sealed class ReminderSettingsViewModel : DeviceSettingsViewModel {
        private readonly ReminderTime[] _reminderTimeOptions = { ReminderTime.Minutes5, ReminderTime.Minutes15, ReminderTime.Minutes30 };

        public ReminderTime[] ReminderTimeOptions {
            get { return _reminderTimeOptions; }
        }

        protected override async Task SaveChangesAsync() {
            await base.SaveChangesAsync();

            await Mvx.IoCProvider.Resolve<IReminderService>().RunAsync();
        }
    }

    // ReSharper disable UnusedMember.Global
    public sealed class SyncSettingsViewModel : DeviceSettingsViewModel {
        private ISyncService SyncService {
            get { return Mvx.IoCProvider.GetSingleton<ISyncService>(); }
        }

        private MvxCommand _forceSyncCommand;

        public ICommand ForceSyncCommand {
            get { return _forceSyncCommand ?? (_forceSyncCommand = new MvxCommand(ForceSync, () => SyncService.Status == SyncStatus.Idle)); }
        }

        private void ForceSync() {
            SyncService.Reset();
            SyncService.TrySync(SyncTriggerKind.Manual);
        }
    }
    // ReSharper restore UnusedMember.Global
}
