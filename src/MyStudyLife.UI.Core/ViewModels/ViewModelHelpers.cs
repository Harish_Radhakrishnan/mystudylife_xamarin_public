﻿using System;
using System.Text.RegularExpressions;
using MyStudyLife.UI.ViewModels.Input;

namespace MyStudyLife.UI.ViewModels {
    public static class ViewModelHelpers {
        private static readonly Lazy<Regex> CamelCaseRegex = new Lazy<Regex>(() => new Regex("([a-z0-9])([A-Z])"));

        /// <summary>
        ///     Turns something like "ClassInputViewModel" into "Class Input"
        /// </summary>
        public static string GetFriendlyName(Type viewModelType) {
            if (!viewModelType.Name.EndsWith("ViewModel")) {
                throw new ArgumentException("Expected view model type, got '" + viewModelType.Name + "'");
            }

            return CamelCaseRegex.Value.Replace(
                viewModelType.Name.Substring(0, viewModelType.Name.IndexOf("ViewModel", StringComparison.Ordinal)),
                "$1 $2"
            );
        }

        public static string GetTitle(this IInputViewModel inputViewModel) {
            var viewModelType = inputViewModel.GetType();

            var friendlyName = CamelCaseRegex.Value.Replace(
                viewModelType.Name.Substring(0, viewModelType.Name.IndexOf("InputViewModel", StringComparison.Ordinal)),
                "$1 $2"
            );

            if (inputViewModel.IsUpdate) {
                return "Edit " + friendlyName;
            }

            return "New " + friendlyName;
        }
    }
}
