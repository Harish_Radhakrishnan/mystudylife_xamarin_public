﻿using System.Threading.Tasks;

namespace MyStudyLife.UI.ViewModels {
    public interface ISearchable {
        string QueryText { get; set; }

        Task ProcessQueryText(string queryText);

        void ClearQuery();
    }
}
