﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Services;
using MyStudyLife.Diagnostics;
using MyStudyLife.Sync;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Versioning;

namespace MyStudyLife.UI.ViewModels {
    [Screen(BackStackPushMode = NavigationBackStackPushMode.ReplaceOnPush)]
    public class UpgradeViewModel : BaseViewModel {
        private readonly IUpgradeService _upgradeService;
        private readonly IUserStore _userStore;
        private readonly IUserService _userService;
        private readonly ITaskProvider _taskProvider;
        private readonly ISyncService _syncService;

        private Exception _exception;

        public bool IsUpgrading { get; private set; }

        public bool HasFailed { get; private set; }

        #region Commands

        private MvxCommand _continueCommand;
        private MvxCommand _emailErrorCommand;
        private MvxCommand _openWebAppCommand;

        public ICommand ContinueCommand => Command.Create(ref _continueCommand, Continue);

        public ICommand EmailErrorCommand => Command.Create(ref _emailErrorCommand, EmailError);

        public ICommand OpenWebAppCommand => Command.Create(ref _openWebAppCommand, OpenWebApp);

        private void Continue() {
            DialogService.RequestConfirmation(
                R.ConfirmationTitle,
                "Any unsynced data will be lost and you will be signed out.",
                ContinueImpl
            );
        }

        private async void ContinueImpl() {
            try {
                IsUpgrading = true;

                await _upgradeService.ForceCurrentVersionAsync();

                await _userService.SignOutAsync(true);

                await NavigationService.ChangePresentation(
                    new AuthorizationChangedPresentationHint(AuthStatusChangeCause.SignOut));
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, new List<string> {"UpgradeService"});

                throw;
            }
            finally {
                IsUpgrading = false;
            }
        }

        private void EmailError() {
            var bodyBuilder = new StringBuilder("Hi Support,");

            bodyBuilder.AppendLine();
            bodyBuilder.AppendLine();

            bodyBuilder.AppendLine("I've encountered an error whilst upgrading my data:");

            if (_exception != null) {
                bodyBuilder.AppendLine();
                bodyBuilder.AppendLine("-- BEGIN ERROR --");
                bodyBuilder.AppendLine(
                    Convert.ToBase64String(Encoding.UTF8.GetBytes(_exception.ToLongString()))
                );
                bodyBuilder.AppendLine("-- END ERROR --");
            }

            _taskProvider.ShowEmailComposer(
                $"{_upgradeService.CurrentAppVersion} Data Upgrade Error",
                bodyBuilder.ToString(),
                MslConfig.Current.SupportEmailAddress
            );
        }

        private void OpenWebApp() {
            _taskProvider.ShowWebUri(new Uri(MslConfig.Current.WebAppUri));
        }

        #endregion

        public UpgradeViewModel(
            IUpgradeService upgradeService,
            IUserStore userStore,
            IUserService userService,
            ITaskProvider taskProvider,
            ISyncService syncService
        ) {
            _upgradeService = upgradeService;
            _userStore = userStore;
            _userService = userService;
            _taskProvider = taskProvider;
            _syncService = syncService;
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            IsUpgrading = true;

            await _upgradeService.DoUpgradeAsync().ContinueWith(async task => {
                IsUpgrading = false;
                HasFailed = true;

                if (task.IsFaulted) {
                    // Upgrade service handles exception logging
                    _exception = task.Exception?.GetBaseException();

                    HasFailed = true;
                }
                else if (!task.IsCanceled) {
                    // This is skipped when a startup is required, so we must setup
                    // the app state here
                    var user = _userStore.GetCurrentUser();
                    var appState = await Mvx.IoCProvider.Resolve<IAppStateManager>().GetStateAsync();
                    appState.SetUser(user);

                    // TODO: Refactor IBootstrapAction out
                    if (Mvx.IoCProvider.TryResolve(out IBootstrapAction action)) await action.RunAsync();

                    _syncService.TrySync(SyncTriggerKind.Programmatic);

                    await NavigationService.ChangePresentation(new UpgradeCompletePresentationHint());
                }
            });

            return true;
        }
    }
}