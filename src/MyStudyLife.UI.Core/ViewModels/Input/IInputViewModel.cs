﻿using System.Collections.Generic;
using System.Windows.Input;

namespace MyStudyLife.UI.ViewModels.Input {
    public interface IInputViewModel : ILoaded {
        bool IsNew { get; }

        bool IsUpdate { get; }

        bool CanDelete { get; }

        ICommand CancelInputCommand { get; }

        ICommand SaveCommand { get; }

        ICommand DeleteCommand { get; }

        ICommand DeleteWithConfirmationCommand { get; }

        IDictionary<string, string> Errors { get; }

        bool HasErrors { get; }
    }
}
