﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Data.Validation;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Services;
using MyStudyLife.Utility;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    public class ClassInputViewModel : SubjectDependentInputViewModel<Class> {
        const int MIN_DURATION_IN_DAYS = 1;

        private IDisposable _timeRangeEnforcer;
        private DateRangeEnforcer<Class> _dateRangeEnforcer;
        private IFeatureService _featureService;

        public override string ItemTypeName => R.Class;

        private bool _canUseExtendedColors;

        public IEnumerable<UserColor> Colors => UserColors.GetColors(_canUseExtendedColors);

        #region Android Specific

        private UserColor _selectedColor;

        [DoNotNotify] // There's a SetProperty in there!
        public UserColor SelectedColor {
            get { return _selectedColor; }
            set {
                if (SetProperty(ref _selectedColor, value) && InputItem != null && (value == null || value.Identifier != InputItem.Color)) {
                    InputItem.Color = value != null ? value.Identifier : null;
                }
            }
        }

        /// <summary>
        ///		Sets the selected color object from
        ///		the current subject and colors collection.
        /// </summary>
        private void SetSelectedColor() {
            string color = !String.IsNullOrEmpty(InputItem?.Color) ? UserColors.NormalizeColorIdentifier(InputItem.Color) : null;

            SelectedColor = color != null
                ? Colors.SingleOrDefault(x => x.Identifier == color) ?? Colors.FirstOrDefault()
                : null;
        }

        #endregion

		#region Class Input Types

	    private ClassType? _inputClassType;

        [DoNotNotify] // There's a SetProperty in there!
	    public ClassType InputClassType {
		    get { return _inputClassType.GetValueOrDefault(ClassType.Recurring); }
			set {
				if (InputItem != null && (IsNew || !_inputClassType.HasValue)) {
					SetProperty(ref _inputClassType, value);

                    RaisePropertyChanged(() => IsRecurring);
				}
			}
	    }

        public bool IsRecurring {
            get { return InputClassType == ClassType.Recurring; }
        }

		#endregion

        #region Help Text

        public IAcademicSchedule EffectiveSchedule {
            get {
                if (SelectedSchedule != null) {
                    // If the subject that is selected has a schedule that is a term of
                    // of the selected schedule then we're obviously going to constrain
                    // the class to that.
                    return (
                        InputItem.Subject != null &&
                        InputItem.Subject.Schedule != null &&
                        InputItem.Subject.Schedule is AcademicTerm
                    ) ? InputItem.Subject.Schedule : SelectedSchedule;
                }

                if (InputItem.Subject != null) {
                    return InputItem.Subject.Schedule;
                }

                return null;
            }
        }

        public string HelpText {
            get {
                if (InputItem == null || InputClassType == ClassType.OneOff) {
                    return String.Empty;
                }

                if (InputItem.Subject == null) {
                    return R.ClassInputHelpNoScheduleOrDates;
                }

                var schedule = EffectiveSchedule;

                if (InputItem.HasStartEndDates) {
                    return String.Format(R.ClassInputHelpWithStartEndDatesFormat, InputItem.StartDate, InputItem.EndDate);
                }

                if (schedule != null) {
                    return String.Format(R.ClassInputHelpWithScheduleFormat, schedule.StartDate, schedule.EndDate);
                }

                return R.ClassInputHelpNoScheduleOrDates;
            }
        }

        #endregion

		#region Commands

        private MvxCommand<bool> _overrideColorCommand;
        private MvxAsyncCommand _newTimeCommand;
        private MvxAsyncCommand<ClassTime> _editTimeCommand;
        private MvxCommand<ClassTime> _deleteTimeCommand;
        private MvxCommand<bool> _hasStartEndDatesCommand;

        public ICommand OverrideColorCommand => Command.Create(ref _overrideColorCommand, OverrideColor);
        public ICommand NewTimeCommand => _newTimeCommand ??= new MvxAsyncCommand(async () => await ShowTimeInput(null));
        public ICommand EditTimeCommand => _editTimeCommand ??= new MvxAsyncCommand<ClassTime>(ShowTimeInput);
        public ICommand DeleteTimeCommand => Command.Create(ref _deleteTimeCommand, DeleteTime);
        public ICommand HasStartEndDatesCommand => Command.Create(ref _hasStartEndDatesCommand, HasStartEndDatesChanged);

        private void OverrideColor(bool shouldOverride) {
            InputItem.Color = shouldOverride ? Colors?.First().Identifier : null;
        }

        private async Task ShowTimeInput(ClassTime time) {
            Mvx.IoCProvider.Resolve<IInputCacheService<Class>>().SetItem(InputItem);

            var navObject = new ClassTimeInputViewModel.NavObject();

            if (SelectedSchedule != null) {
                navObject.YearGuid = SelectedSchedule is AcademicYear
                    ? SelectedSchedule.Guid
                    : ((AcademicTerm) SelectedSchedule).YearGuid;
            }

            if (time != null) {
                navObject.TimeGuid = time.Guid;
            }

            await ShowViewModel<ClassTimeInputViewModel>(navObject);
        }

        private void DeleteTime(ClassTime time) {
            InputItem.Times.Remove(time);
        }

        private void HasStartEndDatesChanged(bool hasStartEndDates) {
            if (InputItem == null) return;

            if (hasStartEndDates) {
                // Prevents reset when editing
                if (InputItem.HasStartEndDates) return;

                var startDate = DateTimeEx.Today.GetClosestInDays(DaysOfWeek.Weekdays);

                InputItem.StartDate = startDate;
                InputItem.EndDate = startDate.AddMonths(1).GetClosestInDays(DaysOfWeek.Weekdays);
            }
            else {
                InputItem.StartDate = null;
                InputItem.EndDate = null;
            }

            SetupDateRangeEnforcer();
            RaisePropertyChanged(nameof(HelpText)); // TODO: Not sure if this is required here as handled in InterceptRaisePropertyChanged(...)?
        }

        private void SetupDateRangeEnforcer() {
            _dateRangeEnforcer?.Dispose();
            _dateRangeEnforcer = InputItem != null
                ? new DateRangeEnforcer<Class>(InputItem, x => x.StartDate, x => x.EndDate) {
                    AllowedDays = DaysOfWeek.Weekdays,
                    MinDate = MinStartDate,
                    MaxDate = MaxEndDate,
                    MinRangeDays = MIN_DURATION_IN_DAYS
                }
                : null;
        }

        #endregion

        #region Validation

        public DateTime? MinStartDate { get; private set; }
        public DateTime? MaxStartDate { get; private set; }
        public DateTime? MinEndDate { get; private set; }
        public DateTime? MaxEndDate { get; private set; }

        private void SetStartEndDateRestrictions() {
            var effectiveSchedule = EffectiveSchedule;

            MinStartDate = effectiveSchedule?.StartDate;
            MaxStartDate = effectiveSchedule?.EndDate.AddDays(-MIN_DURATION_IN_DAYS);

            MinEndDate = effectiveSchedule?.StartDate.AddDays(MIN_DURATION_IN_DAYS);
            MaxEndDate = effectiveSchedule?.EndDate;

            var dre = _dateRangeEnforcer;
            if (dre != null) {
                dre.MinDate = MinStartDate;
                dre.MaxDate = MaxEndDate;
            }
        }

        #endregion

        public ClassInputViewModel(
            IAcademicYearRepository academicYearRepo,
            IFeatureService featureService,
            ISubjectRepository subjectRepository)
        : base(academicYearRepo, featureService, subjectRepository) {
            _featureService = featureService;
        }

        protected override async Task<bool> StartAsync() {
            _canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);
            await base.StartAsync();
            SetSelectedColor();

            // Currently handled by Init which actually takes longer than Start
            return false;
        }

        #region Overrides of InputViewModel<Class>

        protected override async Task<IAcademicSchedule> GetDefaultAcademicSchedule() {
            return IsNew ? await base.GetDefaultAcademicSchedule() : null;
        }

        private bool _holidayWarningIgnored;

        protected override async Task<SavingInterceptionResult> OnInputItemSaving(Class item) {
            if (item.Subject.Schedule == null || (item.Subject.Schedule is AcademicYear && SelectedSchedule is AcademicTerm)) {
                var term = SelectedSchedule as AcademicTerm;

                if (term != null) {
                    item.YearGuid = term.YearGuid;
                    item.Year = term.Year;
                    item.TermGuid = term.Guid;
                    item.Term = term;
                }
                else {
                    if (SelectedSchedule != null) {
                        item.YearGuid = SelectedSchedule.Guid;
                        item.Year = (AcademicYear)SelectedSchedule;
                    }

                    item.TermGuid = null;
                    item.Term = null;
                }
            }
            else {
                item.YearGuid = null;
                item.Year = null;
                item.TermGuid = null;
                item.Term = null;
            }

            if (item.Type == ClassType.OneOff) {
                if (IsUpdate || _holidayWarningIgnored) {
                    return SavingInterceptionResult.Save;
                }

// ReSharper disable once PossibleInvalidOperationException
                var holiday = await AcademicYearRepo.GetHolidayForDate(item.Date.Value);

                if (holiday != null) {
                    var result = await DialogService.ShowCustomAsync(
                        Catalog.GetString("Date is a holiday"),
                        Catalog.GetString("The date of the class ({0}) occurs {1} {2} ({3})").WithArgs(
                            item.Date.Value.ToString("D"),
                            Catalog.GetString(holiday.Duration > 1 ? "in" : "on"),
                            holiday.Name,
                            holiday.Dates
                        ),
                        R.Common.Save,
                        R.Common.Cancel
                    );

                    if (!result.ChosePositiveAction) {
                        return SavingInterceptionResult.Cancel;
                    }

                    _holidayWarningIgnored = true;
                }
            }
            else if (SelectedSchedule == null && !AppState.NoSchedulesPromptIgnored) {
                bool anyYears = AcademicYears.Any();

                string message;

                if (!anyYears) {
                    message =
                        "You have not added any academic years yet. " +
                        "An academic year neatly groups all of your classes into your school year and also enables week and day rotation scheduling.";
                }
                else {
                    message = "This class does not have an academic year or term assigned.";
                }

                var result = await DialogService.ShowCustomAsync(
                    "No academic year/term",
                    message,
                    anyYears ? "Assign One" : "Add One",
                    "Ignore"
                );

                if (result.ChosePositiveAction) {
                    if (!anyYears) {
                        await ShowViewModel<AcademicYearInputViewModel>();
                    }

                    return SavingInterceptionResult.Cancel;
                }

                AppState.NoSchedulesPromptIgnored = true;
            }

            return SavingInterceptionResult.Save;
        }

        protected override bool ValidateInputItem(Class item, out IDictionary<string, string> errors) {
            if (item.Guid.IsEmpty()) {
                item.Type = InputClassType;
            }

            item.Purge();

            var context = new ValidationContext(
                item,
                IsRecurring
                    ? (User.IsTeacher ? Class.RecurringTeacherValidationGroup : Class.RecurringStudentValidationGroup)
                    : (User.IsTeacher ? Class.OneOffTeacherValidationGroup : Class.OneOffStudentValidationGroup)
            );

            context.AddCustomValidationCheck(
                "Time",
                "End time must be 5 or more minutes after the start time",
                ignored => {
                    if (InputItem.IsOneOff && InputItem.StartTime.HasValue && InputItem.EndTime.HasValue) {
                        return (InputItem.EndTime.Value - InputItem.StartTime.Value).TotalMinutes >= 5;
                    }

                    return true;
                }
            );

            if (!context.IsValid()) {
                errors = context.Errors;

                return false;
            }

            errors = null;

            if (item.HasStartEndDates) {
// ReSharper disable PossibleInvalidOperationException
                if (item.EndDate.Value.IsBefore(item.StartDate.Value)) {
                    ErrorMessage = R.ValidationStartEndDateMismatch;

                    return false;
                }

                if (item.EndDate.Value.IsSame(item.StartDate.Value)) {
                    ErrorMessage = R.ValidationClassStartEndDatesIdentical;

                    return false;
                }

                if (EffectiveSchedule != null) {
                    // Fixes #200 - we're only showing an after and before error only 1 at once
                    // error for clarity as users don't seem to understand "outside range" error.
                    // If both dates are either both before or after, only one error will show as
                    // start/end dates cannot be the wrong way around anyway :-)

                    if (item.StartDate.Value < EffectiveSchedule.StartDate) {
                        ErrorMessage = String.Format(
                            R.ClassInput.StartDateBeforeScheduleFormat,
                            item.StartDate.Value,
                            (EffectiveSchedule is AcademicYear ? R.Year : R.Term).ToLower(),
                            EffectiveSchedule.StartDate
                        );
                    }

                    if (EffectiveSchedule.EndDate < item.EndDate.Value) {
                        ErrorMessage = String.Format(
                            R.ClassInput.EndDateAfterScheduleFormat,
                            item.EndDate.Value,
                            (EffectiveSchedule is AcademicYear ? R.Year : R.Term).ToLower(),
                            EffectiveSchedule.EndDate
                        );
                        return false;
                    }
                }
// ReSharper restore PossibleInvalidOperationException
            }

            return true;
        }

        #endregion

        #region Overrides

		public override async Task<Class> GetDefaultItemAsync() {
            if (await AcademicYearRepo.Count() == 0 && !AppState.NoSchedulesPromptIgnored) {
                var result = await DialogService.ShowCustomAsync(
                    "No academic years",
                    "You have not added any academic years yet. " +
                    "An academic year neatly groups all of your classes into your school year and also enables week and day rotation scheduling.",
                    "Add One",
                    "Ignore"
                );

                if (result.ChosePositiveAction) {
                    await ShowViewModel<AcademicYearInputViewModel>();
                }
                else {
                    AppState.NoSchedulesPromptIgnored = true;
                }
            }

            return new Class {
			    Type = ClassType.Recurring
			};
		}

        public override string GetConflictErrorMessage(IEnumerable<Class> conflictingItems) {
            return R.ValidationClassConflict;
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(Class oldItem, Class newItem) {
            SetInputItemDefaults();

            InputClassType = InputItem != null ? InputItem.Type : ClassType.Recurring;

            SetSelectedColor();

            base.OnInputItemChanged(oldItem, newItem);
        }

        private void SetInputItemDefaults() {
            if (InputItem == null || User == null) {
                return;
            }

            bool isNew = IsNew;

            var settings = User.Settings;

            if (InputClassType == ClassType.OneOff) {
                _dateRangeEnforcer?.Dispose();
                _dateRangeEnforcer = null;

                if (isNew) {
                    InputItem.Date = DateTimeEx.Today;

                    var dst = settings.DefaultStartTime;

                    InputItem.StartTime = dst;
                    InputItem.EndTime = dst.Add(TimeSpan.FromMinutes(settings.DefaultDuration));
                }

                if (_timeRangeEnforcer == null) {
                    _timeRangeEnforcer = new TimeRangeEnforcer<Class>(InputItem, x => x.StartTime, x => x.EndTime);
                }
            }
            else {
                _timeRangeEnforcer?.Dispose();
                _timeRangeEnforcer = null;

                if (isNew) {
                    InputItem.Times = new ObservableCollection<ClassTime>();
                }

                SetupDateRangeEnforcer();
            }
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnInputItemPropertyChanged(sender, e);

            if (e.PropertyName == nameof(Class.Subject)) {
                RaisePropertyChanged(nameof(EffectiveSchedule));
            }
            else if (e.PropertyName.In(nameof(Class.StartDate), nameof(Class.EndDate))) {
                RaisePropertyChanged(nameof(HelpText));
            }
            else if (e.PropertyName == nameof(Class.Color) && InputItem != null) {
                SetSelectedColor();
                SetColor();
            }
        }

        protected override Task<System.Drawing.Color?> GetColor() {
            var user = User.Current;
            var inputItem = InputItem;

            System.Drawing.Color? color = null;

            if (inputItem?.Subject != null) {
                color = UserColors.GetColor(user.IsTeacher ? inputItem.ActualColor : inputItem.Subject.Color, _canUseExtendedColors).Color;
            }

            return Task.FromResult(color);
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(InputClassType) && InputItem != null) {
                InputItem.Purge();
                SetInputItemDefaults();

                RaisePropertyChanged(nameof(HelpText));
            }
            else if (e.PropertyName == nameof(SelectedSchedule)) {
                RaisePropertyChanged(nameof(EffectiveSchedule));
            }
            else if (e.PropertyName == nameof(EffectiveSchedule)) {
                SetStartEndDateRestrictions();

                RaisePropertyChanged(nameof(HelpText));
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        #endregion
    }
}
