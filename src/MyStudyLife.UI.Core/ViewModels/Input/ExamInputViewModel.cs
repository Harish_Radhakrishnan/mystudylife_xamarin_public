﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Utility;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    public class ExamInputViewModel : SubjectDependentInputViewModel<Exam> {
        public override string ItemTypeName {
            get { return R.Exam; }
        }

        public TimeSpan InputExamEndTime {
            get {
                return this.InputItem != null ? this.InputItem.EndTime : new TimeSpan(9, 0, 0);
            }
        }

        public ExamInputViewModel(
            IAcademicYearRepository academicYearRepo,
            IFeatureService featureService,
            ISubjectRepository subjectRepository) : base(academicYearRepo, featureService, subjectRepository) { }

        public override Task<Exam> GetDefaultItemAsync() {
            UserSettings settings = User.Current.Settings;

            var date = DateTimeEx.Now;

            return AsyncTask.FromResult(new Exam {
                Date = new DateTime(date.Year, date.Month, date.Day, settings.DefaultStartTime.Hours, settings.DefaultStartTime.Minutes, 0),
                Duration = settings.DefaultDuration
            });
        }

        public override string GetConflictErrorMessage(IEnumerable<Exam> conflictingItems) {
            return Catalog.GetString(
                "The dates and duration of the {0} {1} conflict with the one you're trying to enter."
            ).WithArgs(
                Catalog.GetPluralString("exam", "exams", conflictingItems.Count()),
                String.Join(", ", conflictingItems.Select(x =>
                    String.Concat("'", x.Title, " - ", x.Date.ToString("f"), " ",  x.Duration, " minutes'")
                ))
            );
        }

        private bool _holidayWarningIgnored;

        protected override async Task<SavingInterceptionResult> OnInputItemSaving(Exam item) {
            if (this.IsUpdate || _holidayWarningIgnored) {
                return SavingInterceptionResult.Save;
            }

            var holiday = await AcademicYearRepo.GetHolidayForDate(item.Date);

            if (holiday != null) {
                var result = await DialogService.ShowCustomAsync(
                    Catalog.GetString("Date is a holiday"),
                    Catalog.GetString("The date of the exam ({0}) occurs {1} {2} ({3})").WithArgs(
                        item.Date.ToString("D"),
                        Catalog.GetString(holiday.Duration > 1 ? "in" : "on"),
                        holiday.Name,
                        holiday.Dates
                    ),
                    R.Common.Save,
                    R.Common.Cancel
                );

                if (!result.ChosePositiveAction) {
                    return SavingInterceptionResult.Cancel;
                }

                _holidayWarningIgnored = true;
            }

            return SavingInterceptionResult.Save;
        }

        protected override async Task<IAcademicSchedule> GetDefaultAcademicSchedule() {
            IAcademicSchedule schedule = null;

            if (this.IsUpdate) {
                schedule = await AcademicYearRepo.GetClosestScheduleToDateAsync(this.InputItem.Date, TenseBias.None);
            }

            return schedule ?? await AcademicYearRepo.GetClosestScheduleToCurrentAsync();
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(Exam oldItem, Exam newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            RaisePropertyChanged(() => this.InputExamEndTime);
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnInputItemPropertyChanged(sender, e);

            if (e.PropertyName == "Duration" || e.PropertyName == "Date") {
                RaisePropertyChanged(() => this.InputExamEndTime);
            }
        }
    }
}
