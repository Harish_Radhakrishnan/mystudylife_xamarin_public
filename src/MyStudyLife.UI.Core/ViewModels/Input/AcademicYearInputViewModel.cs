﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Data.Validation;
using MyStudyLife.Extensions;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.Services;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    public sealed class AcademicYearInputViewModel : InputViewModel<AcademicYear> {
        public override string ItemTypeName=> R.AcademicYear;

        #region Scheduling

        public short[] WeekCountOptions { get; } = { 2, 3, 4 };

        public ObservableCollection<KVP<short, string>> AvailableWeeks { get; } = new ObservableCollection<KVP<short, string>>();

        public int[] DayCountOptions { get; } = { 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

        public ObservableCollection<KVP<int, string>> AvailableDays { get; } = new ObservableCollection<KVP<int, string>>();

        public ICollection<Tuple<int, string, int>> Days { get; private set; }

        public DaysOfWeek SelectedDays {
            get { return this.InputItem != null ? this.InputItem.Scheduling.Days.GetValueOrDefault(DaysOfWeek.Weekdays) : DaysOfWeek.Weekdays; }
            set {
                var days = (int)value;

                int i = 0;
                while (EnumEx.GetSetFlagCount(days) < 4) {
                    int bit = 1 << i;

                    if ((days & bit) == 0) {
                        days = (days | bit);
                    }

                    i++;

                    if (i >= 7) break;
                }

                this.InputItem.Scheduling.Days = (DaysOfWeek)days;

                RaisePropertyChanged(() => this.SelectedDaysText);
            }
        }

        public string SelectedDaysText {
            get {
                var days = new List<string>();

                for (int i = 0; i < 7; i++) {
                    if ((this.SelectedDays & (DaysOfWeek)(1 << i)) != 0) {
                        days.Add(DateTimeFormat.ShortDayNames[i]);
                    }
                }

                return String.Join(", ", days);
            }
        }

        private void SetAvailableWeeksAndDays(SchedulingOptions options = null) {
            options = options ?? this.InputItem.Scheduling;

            if (options == null) {
                return;
            }

            if (options.IsWeekRotation) {
                SetWeeksOrDaysFromCount(
                    this.AvailableWeeks,
                    options.WeekCount.GetValueOrDefault(SchedulingOptions.DefaultWeekCount),
                    (week, name) => new KVP<short, string>((short)week, name)
                );

                if (options.StartWeek > options.WeekCount) {
                    options.StartWeek = options.WeekCount;
                }
                else {
                    RaisePropertyChanged(nameof(this.StartWeekValue));
                    // Prevents list resetting on Windows 8
                    // Without this on iOS selected item doesn't render first time around either
                    // ReSharper disable once ExplicitCallerInfoArgument
                    ((IRaisePropertyChanged)options).RaisePropertyChanged(nameof(options.StartWeek));
                }
            }

            if (options.IsDayRotation) {
                SetWeeksOrDaysFromCount(
                    this.AvailableDays,
                    options.DayCount.GetValueOrDefault(SchedulingOptions.DefaultDayCount),
                    (day, name) => new KVP<int, string>(day, name)
                );

                if (options.StartDay > options.DayCount) {
                    options.StartDay = options.DayCount;
                }
                else {
                    RaisePropertyChanged(nameof(this.StartDayValue));
                    // Prevents list resetting on Windows 8
                    // Without this on iOS selected item doesn't render first time around either
                    // ReSharper disable once ExplicitCallerInfoArgument
                    ((IRaisePropertyChanged)options).RaisePropertyChanged(nameof(options.StartDay));
                }
            }
        }

        private void SetWeeksOrDaysFromCount<T>(ObservableCollection<T> items, int count, Func<int, string, T> createElement) {
            if (items.Count < count) {
                var useLetters = User.Current.Settings.IsRotationScheduleLettered;

                for (var i = items.Count + 1; i <= count; i++) {
                    items.Add(createElement(i, useLetters ? Humanize.NumberToLetter(i) : i.ToString()));
                }
            }
            else if (count < items.Count) {
                for (var i = items.Count; i > count; i--) {
                    items.RemoveAt(i - 1);
                }
            }
        }

        #region Android Specifics (lack of SelectedValuePath)

        public KVP<short, string> StartWeekValue {
            get { return this.AvailableWeeks.FirstOrDefault(x => x.Key == this.InputItem.Scheduling.StartWeek); }
            set {
                this.InputItem.Scheduling.StartWeek = value == null ? (short)0 : value.Key;
            }
        }

        public KVP<int, string> StartDayValue {
            get { return this.AvailableDays.FirstOrDefault(x => x.Key == this.InputItem.Scheduling.StartDay); }
            set {
                this.InputItem.Scheduling.StartDay = value == null ? 0 : value.Key;
            }
        }

        #endregion

        private MvxCommand<bool> _setFixedCommand;
        private MvxCommand<bool> _setWeekRotationCommand;
        private MvxCommand<bool> _setDayRotationCommand;

        public ICommand SetFixedCommand {
            get { return _setFixedCommand ?? (_setFixedCommand = GetCommandForSchedulingMode(SchedulingMode.Fixed)); }
        }
        public ICommand SetWeekRotationCommand {
            get { return _setWeekRotationCommand ?? (_setWeekRotationCommand = GetCommandForSchedulingMode(SchedulingMode.WeekRotation)); }
        }
        public ICommand SetDayRotationCommand {
            get { return _setDayRotationCommand ?? (_setDayRotationCommand = GetCommandForSchedulingMode(SchedulingMode.DayRotation)); }
        }

        private MvxCommand<bool> GetCommandForSchedulingMode(SchedulingMode mode) {
            return new MvxCommand<bool>((isMode) => {
                if (isMode && this.InputItem != null) {
                    this.InputItem.Scheduling.Mode = mode;
                }
            });
        }

        public string DayRotationWarning { get; private set; }

        #endregion

        #region Commands

        protected override async Task Delete() {
	        try {
                await base.Delete();
            }
            catch (ItemConstraintException<Subject> ex) {
                DialogService.ShowDismissible(
                    R.AcademicScheduleDeletionConstraintTitle,
                    String.Format(
                        this.InputItem.Terms != null && this.InputItem.Terms.Count > 0
                            ? R.AcademicScheduleYearWithTermsDeletionSubjectConstraint
                            : R.AcademicScheduleYearDeletionSubjectConstraint,
                        String.Join(",", ex.AffectedItems.Select(x => String.Concat("'", x.Name, "'")))
                    )
                );
            }
	        catch (ItemConstraintException<Class> ex) {
				DialogService.ShowDismissible(
					R.AcademicScheduleDeletionConstraintTitle,
					String.Format(
                        new PluralFormatProvider(true),
						this.InputItem.Terms != null && this.InputItem.Terms.Count > 0
							? R.AcademicScheduleYearWithTermsDeletionClassConstraint
							: R.AcademicScheduleYearDeletionClassConstraint,
                        ex.AffectedItems.Count()
					)
				);
	        }
        }

        #endregion

        #region Scheduling Commands

        private MvxCommand _showSchedulingCommand;

        public ICommand ShowSchedulingCommand {
            get { return Command.Create(ref _showSchedulingCommand, () => this.ShowScheduling = true); }
        }

        public bool ShowScheduling { get; private set; }

        #endregion

        #region Term Commands

        private ICommand _newTermCommand;
        private ICommand _editTermCommand;
        private ICommand _deleteTermCommand;

        public ICommand NewTermCommand => _newTermCommand ??= new MvxAsyncCommand(NewTerm);
        public ICommand EditTermCommand => _editTermCommand ??= new MvxAsyncCommand<AcademicTerm>(EditTerm);
        public ICommand DeleteTermCommand => _deleteTermCommand ??= new MvxCommand<AcademicTerm>(DeleteTerm);

        private async Task NewTerm() {
            Mvx.IoCProvider.Resolve<IInputCacheService<AcademicYear>>().SetItem(this.InputItem);

            await this.ShowViewModel<AcademicTermInputViewModel>();
        }
		private async Task EditTerm(AcademicTerm term) {
            Mvx.IoCProvider.Resolve<IInputCacheService<AcademicYear>>().SetItem(this.InputItem);

            await this.ShowViewModel<AcademicTermInputViewModel>(new AcademicTermInputViewModel.NavObject {
                Guid = term.Guid
            });
        }

        private void DeleteTerm(AcademicTerm term) => this.InputItem.RemoveTerm(term.Guid);

        #endregion

        #region Holiday Commands

        private ICommand _newHolidayCommand;
        private ICommand _editHolidayCommand;
        private ICommand _deleteHolidayCommand;

        public ICommand NewHolidayCommand => _newHolidayCommand ??= new MvxAsyncCommand(NewHoliday);
        public ICommand EditHolidayCommand => _editHolidayCommand ??= new MvxAsyncCommand<Holiday>(EditHoliday);
        public ICommand DeleteHolidayCommand => _deleteHolidayCommand ??= new MvxCommand<Holiday>(DeleteHoliday);

        private async Task NewHoliday() {
            Mvx.IoCProvider.Resolve<IInputCacheService<AcademicYear>>().SetItem(this.InputItem);

            await this.ShowViewModel<HolidayInputViewModel>();
        }
        private async Task EditHoliday(Holiday holiday) {
            Mvx.IoCProvider.Resolve<IInputCacheService<AcademicYear>>().SetItem(this.InputItem);

            await this.ShowViewModel<HolidayInputViewModel>(new HolidayInputViewModel.NavObject {
                Guid = holiday.Guid
            });
        }

        private void DeleteHoliday(Holiday holiday) {
            this.InputItem.Holidays.Remove(holiday);
        }

        #endregion

        public override void Start() {
            base.Start();

            this.Days = DateTimeFormat.ShortDays.Select((x, i) => new Tuple<int, string, int>(1 << (int)x.DayOfWeek, x.Name, i)).ToList();
        }

        protected override bool ValidateInputItem(AcademicYear item, out IDictionary<string, string> errors) {
            ValidationContext context = new ValidationContext(item);

            if (!context.IsValid()) {
                errors = context.Errors;

                return false;
            }

            if (item.EndDate.IsBefore(item.StartDate, DateTimeComparison.Day)) {
                this.ErrorMessage = R.ValidationStartEndDateMismatch;

                errors = null;

                return false;
            }

	        if (item.EndDate.IsSame(item.StartDate, DateTimeComparison.Day)) {
	            this.ErrorMessage = R.ValidationYearStartEndDatesIdentical;

	            errors = null;

	            return false;
	        }

            errors = null;

            return true;
	    }

		public override async Task<AcademicYear> GetDefaultItemAsync() {
		    var years = await Mvx.IoCProvider.Resolve<IAcademicYearRepository>().GetAllNonDeletedAsync();

		    var previousYear = years.OrderByDescending(x => x.EndDate).FirstOrDefault();

			return new AcademicYear {
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(9),
                Scheduling = previousYear != null ? previousYear.Scheduling.Clone() : SchedulingOptions.Default
            };
        }

        public override string GetConflictErrorMessage(IEnumerable<AcademicYear> conflictingItems) {
            return Catalog.GetString(
                "The entered dates overlap with {0}. Academic years cannot overlap."
            ).WithArgs(String.Join(", ", conflictingItems.Select(x => String.Concat("'", x, " (", x.Dates, ")'"))));
        }

        protected override Task<bool> GetCanDeleteAsync(AcademicYear inputItem) => Task.FromResult(
            !inputItem.SchoolId.HasValue && this.User != null && !this.User.IsTeacher
        );

        [UsedImplicitly] private DateRangeEnforcer<AcademicYear> _dateRangeEnforcer;
        [UsedImplicitly] private IDisposable _schedulingOptionsSubscription;

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(AcademicYear oldItem, AcademicYear newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            if (newItem != null) {
                _dateRangeEnforcer = new DateRangeEnforcer<AcademicYear>(newItem, x => x.StartDate, x => x.EndDate) {
                    AllowedDays = newItem.Scheduling.IsDayRotation
                        ? newItem.Scheduling.Days.GetValueOrDefault(DaysOfWeek.Weekdays)
                        : DaysOfWeek.Weekdays
                 };
                _schedulingOptionsSubscription = newItem.Scheduling.WeakSubscribe(SchedulingOptionsOnPropertyChanged);

                newItem.Scheduling.SetDefaults(true);
                SetAvailableWeeksAndDays(newItem.Scheduling);

                this.ShowScheduling = this.IsNew;
            }
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnInputItemPropertyChanged(sender, e);

            if (e.PropertyName == nameof(AcademicYear.StartDate)) {
                SetDayRotationWarning();
            }
        }

        public void SchedulingOptionsOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var options = (SchedulingOptions) sender;

            switch (e.PropertyName) {
                case nameof(SchedulingOptions.Mode):
                    options.SetDefaults();
                    SetDayRotationWarning();
                    break;
                case nameof(SchedulingOptions.StartWeek):
                    if (this.StartWeekValue == null || this.StartWeekValue.Key != options.StartWeek) {
                        RaisePropertyChanged(() => this.StartWeekValue);
                    }
                    break;
                case nameof(SchedulingOptions.StartDay):
                    if (this.StartDayValue == null || this.StartDayValue.Key != options.StartDay) {
                        RaisePropertyChanged(() => this.StartDayValue);
                    }
                    break;
                case nameof(SchedulingOptions.Days):
                    SetDayRotationWarning();
                    break;
            }

            if (e.PropertyName.In(nameof(SchedulingOptions.Mode), nameof(SchedulingOptions.WeekCount), nameof(SchedulingOptions.DayCount))) {
                SetAvailableWeeksAndDays(options);
            }

            if (e.PropertyName.In(nameof(SchedulingOptions.Mode), nameof(SchedulingOptions.Days))) {
                if (this._dateRangeEnforcer != null) {
                    this._dateRangeEnforcer.AllowedDays = this.InputItem.Scheduling.IsDayRotation
                        ? this.InputItem.Scheduling.Days.GetValueOrDefault(DaysOfWeek.Weekdays)
                        : DaysOfWeek.Weekdays;
                }
            }
        }

        private void SetDayRotationWarning() {
            var schedule = this.InputItem;

            if (schedule != null && schedule.Scheduling.IsDayRotation) {
                var startDateDoWFlag = schedule.StartDate.DayOfWeek.ToFlag();

                if (schedule.Scheduling.Days.HasValue && schedule.Scheduling.Days.Value > 0 && !schedule.Scheduling.Days.Value.HasFlag(startDateDoWFlag)) {
                    var firstRotationDayDate = schedule.StartDate;

                    do {
                        firstRotationDayDate = firstRotationDayDate.AddDays(1);
                    } while (!schedule.Scheduling.Days.Value.HasFlag(firstRotationDayDate.DayOfWeek.ToFlag()));

                    this.DayRotationWarning = Catalog.GetString(
                        "Start date is a {0} which is not selected as a rotation day. First rotation day will be {1}."
                    ).WithArgs(
                        schedule.StartDate.ToString("dddd"),
                        firstRotationDayDate.ToString("D")
                    );

                    return;
                }
            }

            this.DayRotationWarning = null;
        }
    }
}
