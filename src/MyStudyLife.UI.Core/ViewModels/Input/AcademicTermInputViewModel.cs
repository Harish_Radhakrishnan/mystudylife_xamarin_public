﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Services;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    [Screen(DoNotTrack = true)]
    public sealed class AcademicTermInputViewModel : BaseInputViewModel<AcademicTerm> {
        const int MIN_DURATION_IN_DAYS = 1;

        public override string ItemTypeName => R.Term;

        private AcademicYear _inputAcademicYear;
        public AcademicYear InputAcademicYear => _inputAcademicYear;

        public DateTime MinStartDate { get; private set; }
        public DateTime MaxStartDate { get; private set; }
        public DateTime MinEndDate { get; private set; }
        public DateTime MaxEndDate { get; private set; }

        protected override Task InitImpl(NavObject navObject) {
            this._inputAcademicYear = Mvx.IoCProvider.GetSingleton<IInputCacheService<AcademicYear>>().GetItem();

            this.MinStartDate = this._inputAcademicYear.StartDate;
            this.MaxStartDate = this._inputAcademicYear.EndDate.AddDays(-MIN_DURATION_IN_DAYS);

            this.MinEndDate = this._inputAcademicYear.StartDate.AddDays(MIN_DURATION_IN_DAYS);
            this.MaxEndDate = this._inputAcademicYear.EndDate;

            return base.InitImpl(navObject);
        }

        protected override async Task SaveImpl(AcademicTerm term) {
            #region Validation

            var year = this.InputAcademicYear;

            // This shouldn't happen, but there might be some legacy terms that this applies to
            // due to a prior bug where validation was missing.
            if (term.EndDate.IsBefore(term.StartDate)) {
                this.ErrorMessage = R.ValidationStartEndDateMismatch;

                return;
            }

            if (term.StartDate.IsBefore(year.StartDate)) {
                this.ErrorMessage = String.Format(
                    R.AcademicYearInput.TermStartDateBeforeScheduleFormat,
                    term.StartDate,
                    year.StartDate
                );

                return;
            }

            if (term.EndDate.IsAfter(year.EndDate)) {
                this.ErrorMessage = String.Format(
                    R.AcademicYearInput.TermEndDateAfterScheduleFormat,
                    term.EndDate,
                    year.EndDate
                );

                return;
            }

            var conflictingTerms = year.Terms.Where(t =>
                !t.Guid.Equals(term.Guid) && (
                    term.StartDate.IsBetween(t.StartDate, t.EndDate) ||
                    term.EndDate.IsBetween(t.StartDate, t.EndDate)
                )
            ).ToList();

            if (conflictingTerms.Any()) {
                this.ErrorMessage = Catalog.GetString(
                    "The entered dates overlap with {0}. Terms cannot overlap."
                ).WithArgs(String.Join(", ", conflictingTerms.Select(x => String.Concat("'", x.Name, " (", x.Dates, ")'"))));

                return;
            }

            #endregion

            if (this.InputItem.Guid.IsEmpty()) {
                this.InputItem.Guid = Guid.NewGuid();
            }

            var existingTerm = _inputAcademicYear.Terms.SingleOrDefault(x => x == term || x.Guid == this.InputItem.Guid);

            if (existingTerm != null) {
                this.InputItem.CopyPropertyValuesTo(existingTerm);
            }
            else {
                _inputAcademicYear.AddTerm(this.InputItem);
            }

            await this.NavigationService.Close(this);
        }

        #region Deletion

        protected override Task<bool> GetCanDeleteAsync(AcademicTerm inputItem) => Task.FromResult(
            (this.InputAcademicYear != null && this.InputAcademicYear.Guid.IsEmpty()) ||
            (this.InputAcademicYear != null && !this.InputAcademicYear.IsFromSchool && this.User != null && !this.User.IsTeacher)
        );

        protected override Task Delete() {
            if (this.InputAcademicYear == null || this.InputItem == null) {
                return TaskEx.NoOp;
            }

            // TODO: Term deletion warning (defaults to academic year), don't prevent it

            this.InputAcademicYear.RemoveTerm(this.InputItem.Guid);

            this.NavigationService.ChangePresentation(EntityDeletedPresentationHint.ForEntity(this.InputItem, this));

            return TaskEx.NoOp;
        }

		protected override async Task DeleteWithConfirmation() {
            this.Analytics.TrackEvent(this.ItemTypeName.ToTitleCase(), EventActions.Click, "Delete");

            if (this.InputAcademicYear == null || this.InputItem == null) return;

			if (this.InputAcademicYear.Guid.IsEmpty()) {
                this.InputAcademicYear.RemoveTerm(this.InputItem.Guid);

				await this.NavigationService.Close(this);
			}
			else {
				var result = await DialogService.RequestConfirmationAsync(
                    R.DeletionPromptFormat.WithArgs(ItemTypeName.ToLower()),
                    R.AcademicScheduleDeletion,
                    ConfirmationType.Delete
				);

                if (result.DidConfirm) {
                    await Delete();
                }
			}
	    }

        #endregion

        protected override Task<AcademicTerm> GetDefaultItemAsync() {
            var year = this.InputAcademicYear;

            DateTime startDate = default(DateTime), endDate = default(DateTime);
            bool setDates = false;

            DaysOfWeek allowedDays = DaysOfWeek.Weekdays;

            if (year.Scheduling.IsDayRotation) {
                allowedDays = year.Scheduling.Days.GetValueOrDefault(DaysOfWeek.Weekdays);
            }

            if (year.Terms.Any()) {
                var lastEndDate = year.Terms.OrderByDescending(x => x.EndDate).First().EndDate;

                if ((year.EndDate - lastEndDate).TotalDays > 7) {
                    double duration = year.Terms.Sum(t => (t.EndDate - t.StartDate).TotalDays + 1);

                    startDate = lastEndDate
                        .AddDays(1d)
                        .AddWeeks(2d)
                        .GetClosestInDays(allowedDays);

                    endDate = startDate
                        .AddDays(duration/year.Terms.Count)
                        .GetClosestInDays(allowedDays);

                    if (endDate.Date > year.EndDate.Date) {
                        endDate = year.EndDate;
                    }

                    setDates = true;
                }
            }

            if (!setDates) {
                // Assumes 3 terms, 2 weeks of holiday between terms
                double yearDuration = (this._inputAcademicYear.EndDate - this._inputAcademicYear.StartDate).TotalDays + 1;

                double duration = yearDuration/3;

                if (duration > 90) {
                    duration = (yearDuration - 28)/3;
                }

                startDate = this._inputAcademicYear.StartDate;
                endDate = startDate.AddDays(Math.Max(duration, 30)).GetClosestInDays(allowedDays);

                if (endDate.Date > year.EndDate.Date) {
                    endDate = year.EndDate;
                }
            }

            return Task.FromResult(
                new AcademicTerm {
                    StartDate = startDate,
                    EndDate = endDate
                }
            );
        }

        protected override Task<AcademicTerm> GetItemAsync(Guid guid) {
            return Task.FromResult(this._inputAcademicYear.Terms.SingleOrDefault(x => x.Guid == guid));
        }

// ReSharper disable once NotAccessedField.Local
        private IDisposable _dateRangeEnforcer;

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(AcademicTerm oldItem, AcademicTerm newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            if (newItem != null) {
                var enforcer = new DateRangeEnforcer<AcademicTerm>(newItem, x => x.StartDate, x => x.EndDate) {
                    AllowedDays = (this._inputAcademicYear?.Scheduling.Days).GetValueOrDefault(DaysOfWeek.Weekdays)
                };

                if (this._inputAcademicYear != null) {
                    enforcer.MinDate = this._inputAcademicYear.StartDate;
                    enforcer.MaxDate = this._inputAcademicYear.EndDate;
                }

                _dateRangeEnforcer = enforcer;
            }
        }
    }
}
