﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MvvmCross.ViewModels;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Data.Validation;
using MyStudyLife.Diagnostics;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Base;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input
{
    [Screen(NameSetByViewModel = true)]
    public abstract class BaseInputViewModel<T> : BaseViewModel, IInputViewModel
        where T : class, IDbRow, INotifyPropertyChanged {

        private readonly TaskCompletionSource<T> _initTaskCompletionSource = new TaskCompletionSource<T>();

        public abstract string ItemTypeName { get; }

        private IDisposable _inputItemSubscription;

        #region Error Messages

        public IDictionary<string, string> Errors { get; protected set; }

        public string ErrorMessage {
            set {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(R.ValidationErrorTitle, value);
            }
        }

        [DependsOn("Errors")]
        public bool HasErrors {
            get { return Errors != null && Errors.Any(); }
        }

        #endregion

        private T _inputItem;
        private bool _canDelete;

        [DoNotNotify]
        public T InputItem {
            get { return _inputItem; }
            set {
                var oldItem = _inputItem;

                _inputItemSubscription?.Dispose();

                SetProperty(ref _inputItem, value);

                _inputItemSubscription = value?.WeakSubscribe(OnInputItemPropertyChanged);

                RaisePropertyChanged(nameof(InputType));
                RaisePropertyChanged(nameof(IsNew));
                RaisePropertyChanged(nameof(IsUpdate));

                OnInputItemChanged(oldItem, value);
            }
        }

        /// <summary>
        ///     Used for displaying the input title (eg. New Academic Term)
        /// </summary>
        public string InputType => IsNew ? R.New : R.Edit;

        public bool IsNew => InputItem == null || InputItem.Guid.IsEmpty();

        public bool IsUpdate => !IsNew;

        public bool CanDelete {
            get { return _canDelete; }
            set {
                if (_canDelete != value) {
                    _canDelete = value;

                    _deleteCommand?.RaiseCanExecuteChanged();
                    _deleteWithConfirmationCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        #region Commands

        private MvxAsyncCommand _saveCommand;
        private MvxAsyncCommand _cancelInputCommand;
        private MvxAsyncCommand _deleteCommand;
        private MvxAsyncCommand _deleteWithConfirmationCommand;

        public ICommand SaveCommand
            => _saveCommand ??= new MvxAsyncCommand(Save);
        public ICommand CancelInputCommand
            => _cancelInputCommand ??= new MvxAsyncCommand(CancelInput);
        public ICommand DeleteCommand
            => _deleteCommand ??= new MvxAsyncCommand(Delete, () => CanDelete);
        public ICommand DeleteWithConfirmationCommand
            => _deleteWithConfirmationCommand ??= new MvxAsyncCommand(DeleteWithConfirmation, () => CanDelete);

        protected virtual async AsyncTask Save() {
            T item = InputItem;

            // If the item is null then it's likely they've
            // just pressed save quite quickly...
            if (item == null) {
                await NavigationService.Close(this);

                return;
            }

            IDictionary<string, string> errors;

            if (!ValidateInputItem(item, out errors)) {
                Errors = errors;

                return;
            }

            Errors = null;

            await SaveImpl(item);
        }

        protected abstract AsyncTask SaveImpl(T item);

        protected virtual async AsyncTask CancelInput() => await NavigationService.Close(this);

        protected abstract AsyncTask Delete();

        protected virtual async AsyncTask DeleteWithConfirmation() {
            Analytics.TrackEvent(ItemTypeName.ToTitleCase(), EventActions.Click, "Delete");
            var result = await DialogService.RequestConfirmationAsync(
                R.DeletionPromptFormat.WithArgs(ItemTypeName.ToLower()),
                null,
                ConfirmationType.Delete
            );

            if (result.DidConfirm) {
                await Delete();
            }
        }

        #endregion

        protected virtual ValidationContext GetValidationContext(T item) => new ValidationContext(item);

        protected virtual bool ValidateInputItem(T item, out IDictionary<string, string> errors) {
            var context = GetValidationContext(item);

            if (!context.IsValid()) {
                errors = context.Errors;

                return false;
            }

            errors = null;

            return true;
        }

        public async void Init(NavObject navObject) {
            await InitImpl(navObject);
        }

        protected virtual async AsyncTask InitImpl(NavObject navObject) {
            if (!navObject.Guid.IsEmpty()) {
                InputItem = (await GetItemAsync(navObject.Guid)).Clone();
            }

            // If item is still null, the guid may be invalid, so we switch to "new" mode
            if (InputItem == null) {
                InputItem = await GetDefaultItemAsync();
            }

            _initTaskCompletionSource.SetResult(InputItem);
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            // Ensure both start and init are completed
            await _initTaskCompletionSource.Task;

            CanDelete = !IsNew && await GetCanDeleteAsync(InputItem);

            return true;
        }

        protected abstract Task<T> GetItemAsync(Guid guid);

        protected abstract Task<T> GetDefaultItemAsync();

        /// <returns>
        ///     True if the current user is able to delete the given input item.
        /// </returns>
        protected virtual Task<bool> GetCanDeleteAsync(T inputItem) => AsyncTask.FromResult(true);

        #region Overridable Eventing

        [SuppressPropertyChangedWarnings]
        protected virtual void OnInputItemChanged(T oldItem, T newItem) { }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) { }

        #endregion

        #region NavObject

        public class NavObject {

            public Guid Guid { get; set; }

            public Guid YearGuid { get; set; }
        }

        #endregion

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsLoaded) && IsLoaded) {
                // The app shouldn't crash due to analytics should it?
                try {
                    if (NavigationEntry == null) {
                        var attr = ScreenAttribute.Get(GetType());

                        if (attr != null && attr.DoNotTrack) {
                            goto cont; // Skip out of the exception handler
                        }
                    }

                    // We're purposely allowing this to be null so we can get the exception :-)
                    NavigationEntry.SetName(this.GetTitle());
                }
                catch (Exception ex) {
                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, true);
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger("BaseInputViewModel").LogError("Caught exception whilst setting navigation entry's name:\n" + ex.ToLongString());
                }
            }

        cont:
            return base.InterceptRaisePropertyChanged(e);
        }
    }

    [Screen(NameSetByViewModel = true)]
    public abstract class InputViewModel<T> : BaseViewModel, IInputViewModel
        where T : BaseUserEntity {

        public abstract string ItemTypeName { get; }

        public ChangeTracker ChangeTracker { get; protected set; }

        private IDisposable _inputItemSubscription;

        #region Error Messages

        public IDictionary<string, string> Errors { get; protected set; }

        public string ErrorMessage {
            set {
                Mvx.IoCProvider.Resolve<IDialogService>().ShowDismissible(R.ValidationErrorTitle, value);
            }
        }

        [DependsOn("Errors")]
        public bool HasErrors {
            get { return Errors != null && Errors.Any(); }
        }

        #endregion

        private T _inputItem;
        private bool _canDelete;

        protected IRepository<T> Repo { get; }

        [DoNotNotify]
        public T InputItem {
            get { return _inputItem; }
            set {
                var oldItem = _inputItem;

                _inputItemSubscription?.Dispose();

                SetProperty(ref _inputItem, value);

                _inputItemSubscription = value?.WeakSubscribe(OnInputItemPropertyChanged);

                RaisePropertyChanged(nameof(InputType));
                RaisePropertyChanged(nameof(IsNew));
                RaisePropertyChanged(nameof(IsUpdate));

                OnInputItemChanged(oldItem, value);
            }
        }

        /// <summary>
        ///     Used for displaying the input title (eg. New Subject)
        /// </summary>
        public string InputType =>  IsNew ? R.New : R.Edit;

	    public bool IsNew => InputItem == null || InputItem.Guid.IsEmpty();

	    public bool IsUpdate => !IsNew;

        public bool CanDelete {
            get { return _canDelete; }
            set {
                if (_canDelete != value) {
                    _canDelete = value;

                    _deleteCommand?.RaiseCanExecuteChanged();
                    _deleteWithConfirmationCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        #region Commands

        private MvxAsyncCommand _saveCommand;
        private MvxAsyncCommand _cancelInputCommand;
        private MvxAsyncCommand _deleteCommand;
        private MvxAsyncCommand _deleteWithConfirmationCommand;

        public ICommand SaveCommand
            => _saveCommand ??= new MvxAsyncCommand(Save);
        public ICommand CancelInputCommand
            => _cancelInputCommand ??= new MvxAsyncCommand(CancelInput);
        public ICommand DeleteCommand
            => _deleteCommand ??= new MvxAsyncCommand(Delete, () => CanDelete);
        public ICommand DeleteWithConfirmationCommand
            => _deleteWithConfirmationCommand ??= new MvxAsyncCommand(DeleteWithConfirmation, () => CanDelete);
        
        public virtual async System.Threading.Tasks.Task Save() {
            T item = InputItem;

            // If the item is null then it's likely they've
            // just pressed save quite quickly...
            if (item == null) {
                await NavigationService.Close(this);

                return;
            }

            IDictionary<string, string> errors;

            if (!ValidateInputItem(item, out errors)) {
                Errors = errors;

                return;
            }

            Errors = null;

            ItemAction action = IsNew ? ItemAction.Add : ItemAction.Update;

            try {
                // Saves spawning unnecessary threads
                var savingTask = OnInputItemSaving(item);

                if (savingTask != null && await savingTask == SavingInterceptionResult.Cancel) {
                    return;
                }

                await Repo.AddOrUpdateAsync(item);

                var savedTask = OnInputItemSaved(item);

                if (savedTask != null) {
                    await savedTask;
                }

                await NavigationService.Close(this);

                Messenger.Publish(new EntityEventMessage<T>(this, item, action));
            }
            catch (ItemConflictException<T> ex) {
                ErrorMessage = GetConflictErrorMessage(ex.ConflictingItems);
            }
            catch (Exception ex) {
                ErrorMessage = R.UnhandledErrorTitle;

	            Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);
            }
        }

        public async System.Threading.Tasks.Task CancelInput() {
            // MSL-232 - some users experienced null references
            // on this.ChangeTracker, likley due to CancelInput()
            // being called before Init() had completed
            if (ChangeTracker?.HasChanges() ?? false) {
                var result = await DialogService.ShowCustomAsync(
                    R.InputCancelTitle, R.InputCancelMessage,
                    R.Discard,
                    R.Common.Cancel
                );

                if (!result.ChosePositiveAction) {
                    return;
                }
            }
            
            await NavigationService.Close(this);
        }

        protected virtual async AsyncTask Delete() {
            if (InputItem != null && IsUpdate) {
                await Repo.DeleteAsync(InputItem);

                await NavigationService.ChangePresentation(EntityDeletedPresentationHint.ForEntity(InputItem, this));

                Messenger.Publish(new EntityEventMessage<T>(this, InputItem, ItemAction.Delete));
            }
            else {
                await NavigationService.Close(this);
            }
        }

        protected virtual async AsyncTask DeleteWithConfirmation() {
            Analytics.TrackEvent(ItemTypeName.ToTitleCase(), EventActions.Click, "Delete");
            var result = await DialogService.RequestConfirmationAsync(
                R.DeletionPromptFormat.WithArgs(ItemTypeName.ToLower()),
                null,
                ConfirmationType.Delete
            );

            if (result.DidConfirm) {
                await Delete();
            }
        }

        #endregion

        protected virtual bool ValidateInputItem(T item, out IDictionary<string, string> errors) {
            ValidationContext context = new ValidationContext(item);

            if (!context.IsValid()) {
                errors = context.Errors;

                return false;
            }

            errors = null;

            return true;
        }

        /// <summary>
        ///     Method called before the <see cref="InputItem"/>
        ///     is saved to the database.
        /// </summary>
        protected virtual Task<SavingInterceptionResult> OnInputItemSaving(T item) {
            return AsyncTask.FromResult(SavingInterceptionResult.Save);
        }

        protected enum SavingInterceptionResult {
            Save,
            Cancel
        }

        /// <summary>
        ///     Method called after the <see cref="InputItem"/>
        ///     has been saved to the database.
        /// </summary>
        protected virtual AsyncTask OnInputItemSaved(T item) => null;

        protected InputViewModel() {
            Repo = Mvx.IoCProvider.Resolve<IRepository<T>>();
        }

        public virtual async void Init(NavObject navObject) {
            if (navObject.Guid.IsEmpty() || (InputItem = (await Repo.GetAsync(navObject.Guid)).Clone()) == null) {
                InputItem = await GetDefaultItemAsync();
            }

            ChangeTracker = new ChangeTracker(InputItem);

            if (IsUpdate) {
                CanDelete = await GetCanDeleteAsync(InputItem);
            }

            IsLoaded = true;
        }

        /// <returns>
        ///     True if the current user is able to delete the given input item.
        /// </returns>
        protected virtual Task<bool> GetCanDeleteAsync(T inputItem) => AsyncTask.FromResult(true);

        #region Abstracts

        public abstract Task<T> GetDefaultItemAsync();

        public abstract string GetConflictErrorMessage(IEnumerable<T> conflictingItems);

        #endregion

        #region Overridable Eventing

        protected virtual void OnInputItemChanged(T oldItem, T newItem) { }

        protected virtual void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) { }

        #endregion

        #region NavObject

        public class NavObject {

            public Guid Guid { get; set; }
        }

        #endregion

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(IsLoaded) && IsLoaded) {
                // The app shouldn't crash due to analytics should it?
                try {
                    if (NavigationEntry == null) {
                        var attr = ScreenAttribute.Get(GetType());

                        if (attr != null && attr.DoNotTrack) {
                            goto cont; // Skip out of the exception handler
                        }
                    }

                    // We're purposely allowing this to be null so we can get the exception :-)
                    NavigationEntry.SetName(this.GetTitle());
                }
                catch (Exception ex) {
                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, true);
                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger("InputViewModel").LogError("Caught exception whilst setting navigation entry's name:\n" + ex.ToLongString());
                }
            }

        cont:
            return base.InterceptRaisePropertyChanged(e);
        }
    }
}
