﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.Helpers;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;
using MyStudyLife.Utility;
using MyStudyLife.UI.Analytics;

namespace MyStudyLife.UI.ViewModels.Input {
    public class SubjectInputViewModel : SchedulableInputViewModel<Subject> {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IFeatureService _featureService;
        private bool _canUseExtendedColors;

        public override string ItemTypeName => R.Subject;

        public IEnumerable<UserColor> Colors { get; private set; }

		#region Android Specific

	    private UserColor _selectedColor;

        [DoNotNotify] // There's a SetProperty in there!
	    public UserColor SelectedColor {
		    get { return _selectedColor; }
		    set {
			    if (SetProperty(ref _selectedColor, value) && value != null && this.InputItem != null && value.Identifier != this.InputItem.Color) {
				    this.InputItem.Color = value.Identifier;
			    }
		    }
	    }

		/// <summary>
		///		Sets the selected color object from
		///		the current subject and colors collection.
		/// </summary>
        private void SetSelectedColor() {
            string color = !String.IsNullOrEmpty(this.InputItem?.Color) ? UserColors.NormalizeColorIdentifier(this.InputItem.Color) : null;

            this.SelectedColor = (color != null ?
                this.Colors.FirstOrDefault(x => x.Identifier == color)
                : null
            ) ?? this.Colors.FirstOrDefault();
		}

		#endregion

        public string SelectedScheduleHelpText {
            get {
                return this.SelectedSchedule == null
                    ? R.SubjectScheduleHelpIndefinite
                    : String.Format(R.SubjectScheduleHelpPeriodFormat, this.SelectedSchedule.StartDate, this.SelectedSchedule.EndDate);
            }
        }

        public bool ShowAdvanced { get; private set; }

        private ICommand _showCommandAdvanced;

        public ICommand ShowAdvancedCommand {
            get { return Command.Create(ref _showCommandAdvanced, () => this.ShowAdvanced = true); }
        }

        public SubjectInputViewModel(IFeatureService featureService, ISubjectRepository subjectRepository) {
            this._featureService = featureService;
            this._subjectRepository = subjectRepository;
        }

	    protected override async System.Threading.Tasks.Task DeleteWithConfirmation() {
            this.Analytics.TrackEvent(this.ItemTypeName.ToTitleCase(), EventActions.Click, "Delete");

            var result = await DialogService.RequestConfirmationAsync(
                R.DeletionPromptFormat.WithArgs(ItemTypeName.ToLower()),
                R.SubjectDeletion,
                ConfirmationType.Delete
            );

            if (result.DidConfirm) {
                await Delete();
            }
	    }

        #region Overrides of InputViewModel<Subject>

        // Override to remove schedule setting
        public override async void Init(NavObject navObject) {
            _canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);

            this.Colors = UserColors.GetColors(_canUseExtendedColors);

            if (!navObject.Guid.IsEmpty() && (this.InputItem = (await this.Repo.GetAsync(navObject.Guid)).Clone()) != null) {
                this.CanDelete = await GetCanDeleteAsync(this.InputItem);
            }
            else {
                this.InputItem = await GetDefaultItemAsync();
            }

            this.SetSelectedColor();

            this.IsLoaded = true;
        }

        public override async Task<Subject> GetDefaultItemAsync() {
            var subjects = await this._subjectRepository.GetByScheduleAsync(this.SelectedSchedule);

            var colors = UserColors.GetColors(_canUseExtendedColors).ToList();
            var unusedColors = colors.Where(x => subjects.All(y => !Equals(UserColors.NormalizeColorIdentifier(y.Color), x.Identifier))).ToList();

            UserColor color;

            if (unusedColors.Any()) {
                color = unusedColors[new Random().Next(0, unusedColors.Count - 1)];
            }
            else {
                color = colors[new Random().Next(0, colors.Count - 1)];
            }

            return new Subject {
                Color = color.Identifier
            };
        }

        protected override Task<bool> GetCanDeleteAsync(Subject inputItem) => AsyncTask.FromResult(
            !inputItem.IsFromSchool && this.User != null && !this.User.IsTeacher
        );

        public override string GetConflictErrorMessage(IEnumerable<Subject> conflictingItems) {
            if (this.InputItem == null || this.InputItem.Schedule == null) {
                return R.ValidationSubjectNoScheduleConflict;
            }

            if (this.InputItem.Schedule is AcademicTerm) {
                return R.ValidationSubjectWithTermConflict;
            }

            return R.ValidationSubjectWithYearConflict;
        }

	    protected override async void OnInputItemChanged(Subject oldItem, Subject newItem) {
		    base.OnInputItemChanged(oldItem, newItem);

            if (newItem != null) {
                IAcademicSchedule schedule = null;

                // Fixes #202 by retrieving a schedule with all children and year's children set.
                if (newItem.ScheduleGuid.HasValue) {
                    schedule = await Mvx.IoCProvider.Resolve<IAcademicYearRepository>().GetSchedule(newItem.ScheduleGuid.Value);
                }

                this.SelectedSchedule = schedule;

				SetSelectedColor();
			}

	        this.ShowAdvanced = newItem?.Schedule != null;
	    }

        [SuppressPropertyChangedWarnings]
	    protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
			base.OnInputItemPropertyChanged(sender, e);

			if (e.PropertyName == nameof(Subject.Color) && this.InputItem != null) {
				SetSelectedColor();
			}
	    }

	    #region Overrides of MvxNotifyPropertyChanged

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            var result = base.InterceptRaisePropertyChanged(e);

            if (e.PropertyName == nameof(SelectedSchedule)) {
                RaisePropertyChanged(() => this.SelectedScheduleHelpText);

                if (this.InputItem != null) {
                    var term = this.SelectedSchedule as AcademicTerm;

                    if (term != null) {
                        this.InputItem.YearGuid = term.YearGuid;
                        this.InputItem.Year = term.Year;
                        this.InputItem.TermGuid = term.Guid;
                        this.InputItem.Term = term;
                    }
                    else if (this.SelectedSchedule != null) {
                        this.InputItem.YearGuid = this.SelectedSchedule.Guid;
                        this.InputItem.Year = (AcademicYear) this.SelectedSchedule;
                        this.InputItem.TermGuid = null;
                        this.InputItem.Term = null;
                    }
                    else {
                        this.InputItem.YearGuid = null;
                        this.InputItem.Year = null;
                        this.InputItem.TermGuid = null;
                        this.InputItem.Term = null;
                    }
                }

                return MvxInpcInterceptionResult.RaisePropertyChanged;
            }

            return result;
        }

        #endregion

        #endregion
    }
}
