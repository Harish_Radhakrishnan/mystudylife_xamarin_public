﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using MyStudyLife.Data;
using MyStudyLife.Utility;
using PropertyChanged;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    public class TaskInputViewModel : SubjectDependentInputViewModel<Task> {
        private readonly IExamRepository _examRepository;

        public override string ItemTypeName {
            get { return R.Task; }
        }

        private ObservableCollection<Exam> _exams;
        private ObservableCollection<Exam> _examsWithNull;
        private Exam _selectedExam;

        [DoNotNotify]
        public ObservableCollection<Exam> Exams {
            get { return _exams ?? (_exams = new ObservableCollection<Exam>()); }
            private set { SetProperty(ref _exams, value); }
        }

        // TODO: Remove ExamsWithNull
        [DoNotNotify]
        public ObservableCollection<Exam> ExamsWithNull {
            get { return _examsWithNull ?? (_examsWithNull = new ObservableCollection<Exam>()); }
            private set { SetProperty(ref _examsWithNull, value); }
        }

        [DoNotNotify]
        public Exam SelectedExam {
            get { return _selectedExam; }
            set {
                SetProperty(ref _selectedExam, value);

                // No need to set the guid as this happens on save
                if (value == null || value.Guid.IsEmpty()) {
                    this.InputItem.Exam = null;
                }
                else {
                    this.InputItem.Exam = value;
                }
            }
        }

        public TaskInputViewModel(IAcademicYearRepository academicYearRepo,
                IFeatureService featureService,
                ISubjectRepository subjectRepository,
                IExamRepository examRepository)
                : base(academicYearRepo, featureService, subjectRepository) {
            this._examRepository = examRepository;
        }

        public override string GetConflictErrorMessage(IEnumerable<Task> conflictingItems) {
            return R.ValidationTaskConflict;
        }

        public override Task<Task> GetDefaultItemAsync() {
            return System.Threading.Tasks.Task.FromResult(new Task {
                DueDate = DateTimeEx.Today
            });
        }

        private bool _holidayWarningIgnored;

        protected override async Task<SavingInterceptionResult> OnInputItemSaving(Task item) {
            if (this.IsUpdate || _holidayWarningIgnored) {
                return SavingInterceptionResult.Save;
            }

            var holiday = await AcademicYearRepo.GetHolidayForDate(item.DueDate);

            if (holiday != null) {
                var result = await DialogService.ShowCustomAsync(
                    Catalog.GetString("Due date is a holiday"),
                    Catalog.GetString("The due date of the task ({0}) occurs {1} {2} ({3})").WithArgs(
                        item.DueDate.ToString("D"),
                        Catalog.GetString(holiday.Duration > 1 ? "in" : "on"),
                        holiday.Name,
                        holiday.Dates
                    ),
                    R.Common.Save,
                    R.Common.Cancel
                );

                if (!result.ChosePositiveAction) {
                    return SavingInterceptionResult.Cancel;
                }

                _holidayWarningIgnored = true;
            }

            return SavingInterceptionResult.Save;
        }

        protected override async Task<IAcademicSchedule> GetDefaultAcademicSchedule() {
            IAcademicSchedule schedule = null;

            if (this.IsUpdate) {
                schedule = await AcademicYearRepo.GetClosestScheduleToDateAsync(this.InputItem.DueDate, TenseBias.None);
            }

            return schedule ?? await AcademicYearRepo.GetClosestScheduleToCurrentAsync();
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(Task oldItem, Task newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            SetExams(newItem);
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName.In(nameof(Task.Subject), nameof(Task.Type), nameof(Task.DueDate))) {
                SetExams((Task)sender);
            }

            base.OnInputItemPropertyChanged(sender, e);
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
            base.OnSelectedScheduleChanged(oldSchedule, newSchedule);

            SetExams(this.InputItem);
        }

        /// <summary>
        ///     Retrieves the appropriate exam for the tasks input popup.
        ///     Selects an exam if needed.
        /// </summary>
        /// <param name="task">The task bound to the input controls.</param>
        private async void SetExams(Task task) {
            var subject = task?.Subject;
            if (subject == null || !task.IsRevision) {
                this.Exams = null;
                this.ExamsWithNull = null;
                return;
            }

            var currentExam = task.Exam;
            if (currentExam != null && !currentExam.SubjectGuid.Equals(task.Subject.Guid)) {
                task.Exam = null;
            }

            var dueDate = task.DueDate;
            var schedule = this.SelectedSchedule;

            DateTime startDate;
            DateTime? endDate = null;

            if (schedule != null) {
                startDate = DateTimeEx.Max(schedule.StartDate.StartOfDay(), dueDate);

                endDate = schedule.EndDate.EndOfDay();

                if (endDate < startDate) {
                    endDate = null;
                }
            }
            else {
                startDate = dueDate;
            }

            var exams = (await _examRepository.GetBySubjectGuidAndDateRangeAsync(task.Subject.Guid, startDate, endDate)).ToList();

            // If the selected exam doesn't exist in the list, add it.
            //
            // We're checking task.Exam not ExamGuid - Exam will be null if it's deleted, ExamGuid won't
            Guid? examGuid = task.Exam?.Guid;
            if (examGuid != null && exams.All(x => !x.Guid.Equals(examGuid.Value))) {
                // #MSL-95 Re-retrieve so we populate all the child properties (such as subject)
                // Selection dealt with below
                exams.Add(await _examRepository.GetAsync(examGuid.Value));
            }

            this.Exams = exams.OrderBy(x => x.Date).ToObservableCollection();

            var examsWithNull = this.Exams.ToObservableCollection();

            var noExam = new Exam { Guid = Guid.Empty, Module = "No Exam" };
            examsWithNull.Insert(0, noExam);

            this.ExamsWithNull = examsWithNull;

            this.SelectedExam = task.IsRevision && task.ExamGuid.HasValue
                ? exams.FirstOrDefault(exam => exam.Guid.Equals(task.ExamGuid.Value))
                : noExam;
        }
    }
}
