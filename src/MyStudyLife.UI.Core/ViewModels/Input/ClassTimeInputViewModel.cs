﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Data.UI;
using MyStudyLife.Data.Validation;
using MyStudyLife.Globalization;
using MyStudyLife.ObjectModel;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Utility;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.WeakSubscription;

namespace MyStudyLife.UI.ViewModels.Input {
    [Screen(DoNotTrack = true)]
    public class ClassTimeInputViewModel : BaseViewModel, IInputViewModel {
        #region Error Messages

        public IDictionary<string, string> Errors { get; protected set; }

        [DependsOn(nameof(Errors))]
        public bool HasErrors {
            get { return this.Errors != null && this.Errors.Any(); }
        }

        #endregion

        public System.Drawing.Color Color { get; private set; }

        public event EventHandler Saved;

        private IFeatureService _featureService;
        private AcademicYear _inputAcademicYear;
        private Class _inputClass;
        private ClassDayType _dayType;
        private int _defaultDuration;

        private bool _canDelete;

// ReSharper disable NotAccessedField.Local
        private IDisposable _timeRangeEnforcer;
        private IDisposable _inputItemSubscription;
// ReSharper restore NotAccessedField.Local

        public bool IsWeekRotationEnabled { get; private set; }

        public bool IsDayRotationEnabled { get; private set; }

        public ClassTime InputItem { get; private set; }

        public bool IsNew { get; private set; }

        [DependsOn(nameof(IsNew))]
        public bool IsUpdate => !IsNew;

        public bool CanDelete {
            get { return _canDelete; }
            set {
                _canDelete = value;

                _deleteCommand?.RaiseCanExecuteChanged();
                _deleteWithConfirmationCommand?.RaiseCanExecuteChanged();
            }
        }

        [DependsOn(nameof(IsNew))]
        public string InputTypeText=> this.IsNew ? R.New : R.Edit;

        private IEnumerable<ClassDayType> _dayTypes;

        public IEnumerable<ClassDayType> DayTypes {
            get {
                return _dayTypes ?? (_dayTypes = new[] {
                    ClassDayType.DayOfWeek,
                    ClassDayType.Rotation
                });
            }
        }

        public ClassDayType DayType {
            get { return _dayType; }
            set {
                if (_dayType != value) {
                    _dayType = value;

                    OnDayTypeChanged();
                }
            }
        }

        public ClassTimeInputViewModel(IFeatureService featureService) {
            _featureService = featureService;
        }

        /// <summary>
        ///     Sets <see cref="DayType"/> without
        ///     setting any properties on the class.
        /// </summary>
        private void SetDayTypeWithoutChange(ClassDayType dayType) {
            _dayType = dayType;
            RaisePropertyChanged(nameof(this.DayType));
            RaisePropertyChanged(nameof(this.IsDayRotation));
        }

        [DependsOn(nameof(DayType))]
        public bool IsDayRotation => this.DayType == ClassDayType.Rotation;

        #region Weeks/Days/RotationDays

        public ObservableCollection<Tuple<int, string>> RotationWeeks { get; private set; }

        public ObservableCollection<Tuple<int, string, int>> RotationDays { get; private set; }

        public ICollection<Tuple<int, string, int>> Days { get; private set; }

        public string RotationWeekText { get; private set; }

        #endregion

        #region Commands

        private MvxCommand _saveCommand;
        private MvxCommand _cancelInputCommand;
        private MvxCommand _deleteCommand;
        private MvxCommand _deleteWithConfirmationCommand;
        private MvxCommand<bool> _isDayRotationCommand;

        public ICommand SaveCommand
            => Command.Create(ref _saveCommand, Save);
        public ICommand CancelInputCommand
            => Command.Create(ref _cancelInputCommand, CancelInput);
        public ICommand DeleteCommand
            => Command.Create(ref _deleteCommand, Delete, () => CanDelete);
        public ICommand DeleteWithConfirmationCommand
            => Command.Create(ref _deleteWithConfirmationCommand, DeleteWithConfirmation, () => CanDelete);
        public ICommand IsDayRotationCommand
            => _isDayRotationCommand ??= new MvxCommand<bool>(IsDayRotationChanged);

        private void Save() {
            if (this.InputItem == null) {
                return;
            }

            var validationContext = new ValidationContext(
                this.InputItem,
                this.IsDayRotation ? ClassTime.RotationDayValidationGroup : ClassTime.DayOfWeekValidationGroup
            );

            validationContext.AddCustomValidationCheck(
                "Time",
                "End time must be 5 or more minutes after the start time",
                ignored => (this.InputItem.EndTime - this.InputItem.StartTime).TotalMinutes >= 5
            );

            if (!validationContext.IsValid()) {
                this.Errors = validationContext.Errors;

                return;
            }

            this.Errors = null;

            if (this.InputItem.Guid.IsEmpty()) {
                this.InputItem.Guid = Guid.NewGuid();
            }

            var existingTime = _inputClass.Times.SingleOrDefault(x => x == InputItem || x.Guid == this.InputItem.Guid);

            if (existingTime != null) {
                this.InputItem.CopyPropertyValuesTo(existingTime);
            }
            else {
                _inputClass.Times.Add(this.InputItem);
            }

            this.NavigationService.Close(this);

            Saved?.Invoke(this, EventArgs.Empty);
        }

        private void CancelInput() => this.NavigationService.Close(this);

        private void Delete() {
            if (this._inputClass != null && this.InputItem != null) {
                this._inputClass.Times.RemoveById(this.InputItem);
            }

            this.NavigationService.Close(this);
        }

        private void DeleteWithConfirmation() {
            this.Analytics.TrackEvent("Class Time", EventActions.Click, "Delete");

            if (this._inputClass == null || this.InputItem == null) return;

            if (this._inputClass.Guid.IsEmpty()) {
                Delete();
            }
            else {
                DialogService.RequestConfirmation(
                    R.DeletionPromptFormat.WithArgs(R.Time.ToLower()),
                    null,
                    ConfirmationType.Delete,
                    Delete
                );
            }
        }

        private void IsDayRotationChanged(bool isDayRotation) {
            this.DayType = isDayRotation ? ClassDayType.Rotation : ClassDayType.DayOfWeek;
        }

        #endregion

        #region Init / Start

        private NavObject _navObject;
        public void Init(NavObject navObject) {
            this._navObject = navObject;
            
            this.IsNew = this._navObject.TimeGuid.IsEmpty();
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            if (_navObject == null) {
                throw new Exception($"{nameof(ClassTimeInputViewModel)} should not be created without params. Init() must be called before Start()");
            }

            this._inputClass = Mvx.IoCProvider.GetSingleton<IInputCacheService<Class>>().GetItem();
            var color = this._inputClass.ActualColor;

            if (color != null) {
                var extendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);
                this.Color = UserColors.GetColor(this._inputClass.ActualColor, extendedColors).Color;
            }

            if (!this._navObject.YearGuid.IsEmpty()) {
                this._inputAcademicYear = await Mvx.IoCProvider.Resolve<IAcademicYearRepository>().GetAsync(this._navObject.YearGuid);
            }

            var userSettings = this.User.Settings;
            this._defaultDuration = userSettings.DefaultDuration;

            this.Days = DateTimeFormat.ShortDays.Select((x, i) => new Tuple<int, string, int>(1 << (int)x.DayOfWeek, x.Name, i)).ToObservableCollection();

            await this.SetSchedulingOptionsAsync();

            if (!this._navObject.TimeGuid.IsEmpty()) {
                var inputItem = this._inputClass.Times.SingleOrDefault(x => x.Guid == this._navObject.TimeGuid);

                if (inputItem == null) {
                    await this.NavigationService.Close(this);
                    return false;
                }

                this.InputItem = inputItem.Clone();
            }
            else {
                this.InputItem = new ClassTime {
                    StartTime = userSettings.DefaultStartTime,
                    EndTime = userSettings.DefaultStartTime.Add(TimeSpan.FromMinutes(this._defaultDuration)),
                    RotationWeek = this.IsWeekRotationEnabled ? new int?(0) : null,
                    RotationDays = this.IsDayRotationEnabled ? new RotationDays?(MyStudyLife.Data.RotationDays.None) : null,
                    Days = this.IsDayRotationEnabled ? null : new DaysOfWeek?(DaysOfWeek.None),
                };
            }

            if (this.IsWeekRotationEnabled && !this.InputItem.RotationWeek.HasValue) {
                this.InputItem.RotationWeek = 0;
            }

            this.SetDayTypeWithoutChange(this.InputItem.RotationDays.HasValue ? ClassDayType.Rotation : ClassDayType.DayOfWeek);
            this._timeRangeEnforcer = new TimeRangeEnforcer<ClassTime>(this.InputItem, x => x.StartTime, x => x.EndTime);

            this._inputItemSubscription = this.InputItem.WeakSubscribe(InputItemOnPropertyChanged);

            this.SetRotationWeekText();

            this.CanDelete = !this.IsNew;

            return true;
        }

        private void InputItemOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(this.InputItem.RotationWeek)) {
                SetRotationWeekText();
            }
        }

        private void SetAvailableWeeksAndDays(int? weekCount, int? dayCount) {
            bool isRotationScheduleLettered = this.User.Settings.IsRotationScheduleLettered;

            if (weekCount.HasValue) {
                this.RotationWeeks = (
                    from w in Enumerable.Range(0, weekCount.Value + 1)
                    select new Tuple<int, string>(
                        w,
                        w == 0 ? "Every Week" : "Week " + (
                            isRotationScheduleLettered
                                ? Humanize.NumberToLetter(w)
                                : w.ToString()
                        )
                    )
                ).ToObservableCollection();
            }

            if (dayCount.HasValue) {
                this.RotationDays = (
                    from d in Enumerable.Range(0, dayCount.Value)
                    select new Tuple<int, string, int>(
                        1 << d,
                        isRotationScheduleLettered
                            ? Humanize.NumberToLetter(d + 1)
                            : (d + 1).ToString(),
                        d
                    )
                ).ToObservableCollection();
            }
        }

        private void SetRotationWeekText() {
            if (this.InputItem == null || !this.IsWeekRotationEnabled) {
                this.RotationWeekText = null;
            }
            else if (!this.InputItem.RotationWeek.HasValue || this.InputItem.RotationWeek.Value <= 0) {
                this.RotationWeekText = Catalog.GetString("Every Week");
            }
            else {
                this.RotationWeekText = Catalog.GetString("Week {0}").WithArgs(
                    User.Settings.IsRotationScheduleLettered
                        ? Humanize.NumberToLetter(this.InputItem.RotationWeek.Value)
                        : this.InputItem.RotationWeek.Value.ToString()
                );
            }
        }

        private async AsyncTask SetSchedulingOptionsAsync() {

            if (this._inputAcademicYear != null) {
                this.IsWeekRotationEnabled = this._inputAcademicYear.Scheduling.IsWeekRotation;
                this.IsDayRotationEnabled = this._inputAcademicYear.Scheduling.IsDayRotation;

                SetAvailableWeeksAndDays(this._inputAcademicYear.Scheduling.WeekCount, this._inputAcademicYear.Scheduling.DayCount);

                return;
            }

            var tSettings = await TimetableSettings.Get(Mvx.IoCProvider.Resolve<ISingletonDataStore>(), false);

            if (tSettings != null) {
                this.IsWeekRotationEnabled = tSettings.IsWeekRotation;
                this.IsDayRotationEnabled = tSettings.IsDayRotation;

                SetAvailableWeeksAndDays(tSettings.WeekCount, tSettings.DayCount);
            }
            else {
                this.IsWeekRotationEnabled = false;
                this.IsDayRotationEnabled = false;
            }
        }

        public class NavObject {
            public Guid YearGuid { get; set; }

            /// <summary>
            ///		Only valid if editing.
            /// </summary>
            public Guid TimeGuid { get; set; }
        }

        #endregion

        private void OnDayTypeChanged() {
            if (this.DayType == ClassDayType.DayOfWeek) {
                this.InputItem.Days = DaysOfWeek.None;
                this.InputItem.RotationDays = null;
            }
            else {
                this.InputItem.Days = null;
                this.InputItem.RotationDays = MyStudyLife.Data.RotationDays.None;
            }
        }
    }
}
