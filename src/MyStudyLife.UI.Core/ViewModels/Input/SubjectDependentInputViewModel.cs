﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Utility;
using AsyncTask = System.Threading.Tasks.Task;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.Input {
    public abstract class SubjectDependentInputViewModel<T> : SchedulableInputViewModel<T>
        where T : SubjectDependentEntity {

        #region Repo

        protected IAcademicYearRepository AcademicYearRepo { get; set; }

        private readonly IFeatureService _featureService;
        private readonly ISubjectRepository _subjectRepository;
        private bool _canUseExtendedColors;

        #endregion

        private ObservableCollection<Subject> _subjects = new ObservableCollection<Subject>();

        public ObservableCollection<Subject> Subjects {
            get => _subjects;
            private set => _subjects = value;
        }

        private readonly System.Drawing.Color _fallbackColor = System.Drawing.Color.FromArgb(28, 141, 118);
        private System.Drawing.Color? _color;

        public virtual System.Drawing.Color Color {
            get => _color ?? _fallbackColor;
            set => _color = value;
        }

        /// <summary>
        ///     Like <see cref="Color"/> but returns null
        ///     rather than a fallback color.
        /// </summary>
        [DependsOn(nameof(Color))]
        public virtual System.Drawing.Color? ColorOrNull => _color;

        #region Commands

        private bool _canAddSubjects;
        private MvxAsyncCommand _newSubjectCommand;

        [DoNotNotify]
        public bool CanAddSubjects {
            get => _canAddSubjects;
            set {
                if (SetProperty(ref _canAddSubjects, value)) {
                    _newSubjectCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand NewSubjectCommand => _newSubjectCommand ??= new MvxAsyncCommand(NewSubject, () => CanAddSubjects);

        private async AsyncTask NewSubject() {
            await this.ShowViewModel<SubjectInputViewModel>(new SubjectInputViewModel.NavObject {
                ScheduleGuid = this.SelectedSchedule?.Guid ?? Guid.Empty
            });
        }

        #endregion

        public SubjectDependentInputViewModel(IAcademicYearRepository academicScheduleRepo, IFeatureService featureService, ISubjectRepository subjectRepository) {
            AcademicYearRepo = academicScheduleRepo;
            _featureService = featureService;
            _subjectRepository = subjectRepository;
        }

        public virtual async void Init(NavObject navObject) {
            T inputItem;
	        IAcademicSchedule schedule = null;

	        Subject subject = null;

            _canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);

            if (!navObject.Guid.IsEmpty() && (inputItem = (await this.Repo.GetAsync(navObject.Guid))) != null) {
                subject = inputItem.Subject;

                // Fixes #202 by retrieving a schedule with all children and year's children set.
                var scheduleRestrictableItem = inputItem as IScheduleRestrictable;

                if (scheduleRestrictableItem?.ScheduleGuid != null) {
                    schedule = await AcademicYearRepo.GetSchedule(scheduleRestrictableItem.ScheduleGuid.Value);
                }

                inputItem.Subject = null;
            }
            else {
                inputItem = await GetDefaultItemAsync();

	            if (!navObject.SubjectGuid.IsEmpty()) {
                    subject = (await _subjectRepository.GetAsync(navObject.SubjectGuid));
	            }
                if (subject == null) {
                    subject = this.Subjects.FirstOrDefault();
                }

                if (!navObject.ScheduleGuid.IsEmpty()) {
                    schedule = await AcademicYearRepo.GetSchedule(navObject.ScheduleGuid);

                    // If we have a subject that doesn't belong to that schedule, null it!
                    if (schedule != null && !(subject?.Schedule?.EqualsRecursive(schedule) ?? true)) {
                        schedule = null;
                    }
                }
            }

	        this.InputItem = inputItem;

            if (schedule == null) {
                if (subject?.ScheduleGuid != null) {
                    // Fixes #202 by retrieving a schedule with all children and year's children set.
                    schedule = await AcademicYearRepo.GetSchedule(subject.ScheduleGuid.Value);
                }
                else {
                    schedule = await GetDefaultAcademicSchedule();
                }
            }

            // For some reason the compiler on MonoDroid optimises away setting this.SelectedSchedule
            // to null so OnSelectedScheduleChanged can't be used there if the value is null...
	        await SetSubjectsByScheduleAsync(schedule, subject);

	        if (schedule != null) {
	            _suppressScheduleChangedHandler = true;

	            this.SelectedSchedule = schedule;

	            _suppressScheduleChangedHandler = false;
            }

            this.ChangeTracker = new ChangeTracker(this.InputItem);

            this.CanDelete = this.IsUpdate && await GetCanDeleteAsync(this.InputItem);
            this.CanAddSubjects = await Mvx.IoCProvider.Resolve<IFeatureService>().CanUseFeatureAsync(Feature.WriteSubjects);

            this.IsLoaded = true;
        }
		// For some reason this is called as well as the one above, overriding
		// it ensures it does no harm...
		public override void Init(InputViewModel<T>.NavObject navObject) { }
        public override void Init(SchedulableInputViewModel<T>.NavObject navObject) { }

        protected virtual async Task<IAcademicSchedule> GetDefaultAcademicSchedule() {
            return await AcademicYearRepo.GetClosestToCurrentAsync();
        }

// ReSharper disable once InconsistentNaming
        protected bool _suppressScheduleChangedHandler = false;

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(T oldItem, T newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            SetColor();
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnInputItemPropertyChanged(sender, e);

            if (e.PropertyName == nameof(Subject)) {
                SetColor();
            }
        }

        protected virtual async void SetColor() {
            this.Color = await this.GetColor() ?? _fallbackColor;
        }

        protected virtual Task<Color?> GetColor() {
            var inputItem = this.InputItem;
            return System.Threading.Tasks.Task.FromResult(inputItem?.Subject != null ? UserColors.GetColor(inputItem.Subject.Color, _canUseExtendedColors).Color : (Color?)null);
        }

        protected override async void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
            if (_suppressScheduleChangedHandler) return;

            await SetSubjectsByScheduleAsync(newSchedule);
        }

        protected async AsyncTask SetSubjectsByScheduleAsync(IAcademicSchedule schedule, Subject subject = null) {
            // Taking the subject before prevents the list resetting it when we change the subjects collection
            var itemSubject = subject ?? (this.InputItem != null ? this.InputItem.Subject : null);

            this.Subjects = (await this._subjectRepository.GetByScheduleAsync(schedule)).OrderBy(x => x.Name).ToObservableCollection();

            if (this.InputItem != null) {
                this.InputItem.Subject = (itemSubject != null
                    ? this.Subjects.SingleOrDefault(x => x.Guid == itemSubject.Guid)
                    : null) ?? this.Subjects.FirstOrDefault();
            }
        }

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<EntityEventMessage<Subject>>(OnMessage);
        }

        private async void OnMessage(EntityEventMessage<Subject> message) {
            // This is a local only event so if the user has managed to add another
            // subject the chances are they want it for the current InputItem.
            if (message.Action == ItemAction.Add && message.Entity != null) {
                await SetSubjectsByScheduleAsync(message.Entity.Schedule, message.Entity);
            }
        }

        #region NavObject

        public new class NavObject {
            public Guid Guid { get; set; }

            public Guid SubjectGuid { get; set; }

            public Guid ScheduleGuid { get; set; }
        }

        #endregion
    }
}
