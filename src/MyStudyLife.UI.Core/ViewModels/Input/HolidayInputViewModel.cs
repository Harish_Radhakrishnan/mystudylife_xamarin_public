﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Diagnostics;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.UI.Annotations;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Services;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Input {
    public sealed class HolidayInputViewModel : BaseInputViewModel<Holiday> {
        public override string ItemTypeName => R.Holiday;

        private readonly IAcademicYearRepository _repository;
        private ChangeTracker _changeTracker;

        public AcademicYear InputAcademicYear { get; private set; }

        public bool ShowPushesSchedule { get; private set; }

        public string HelpText { get; private set; }

        public string SchedulingHelpText { get; private set; }

        public string PushesScheduleHelpText { get; private set; }

        public HolidayInputViewModel(IAcademicYearRepository academicYearRepository) {
            this._repository = academicYearRepository;

            this.HelpText = Catalog.GetString("Holidays (also known as vacations or days off) enable you to specify dates on which you do not have classes.");
        }

        protected override async AsyncTask InitImpl(NavObject navObject) {
            this.InputAcademicYear = await _repository.GetAsync(navObject.YearGuid);

            if (this.InputAcademicYear == null) {
                throw new ArgumentException("HolidayInputViewModel: Could not find year with guid in nav object.");
            }

            if (this.InputAcademicYear.Scheduling.IsWeekRotation) {
                this.SchedulingHelpText = Catalog.GetString(
                    "The pushes schedule option allows you to push the rotation week until after your holiday " +
                    "as opposed to skipping it. Holidays must be at least 5 days out of a week to push rotation weeks."
                );
            }
            else if (this.InputAcademicYear.Scheduling.IsDayRotation) {
                this.SchedulingHelpText = Catalog.GetString(
                    "The pushes schedule option allows you to push the rotation day until after your holiday as opposed to skipping it."
                );
            }

            if (this.SchedulingHelpText != null) {
                this.HelpText += Environment.NewLine + Environment.NewLine + this.SchedulingHelpText;
            }

            await base.InitImpl(navObject);

            _changeTracker = new ChangeTracker(this.InputItem);
        }

        protected override async AsyncTask CancelInput() {
            if (this._changeTracker.HasChanges()) {
                var result = await DialogService.ShowCustomAsync(
                    R.InputCancelTitle, R.InputCancelMessage,
                    R.Discard,
                    R.Common.Cancel
                );

                if (result.ChosePositiveAction) {
                    await this.NavigationService.Close(this);
                }
            }
            else {
                await this.NavigationService.Close(this);
            }
        }

        protected override bool ValidateInputItem(Holiday item, out IDictionary<string, string> errors) {
            if (!base.ValidateInputItem(item, out errors)) {
                return false;
            }

            var year = this.InputAcademicYear;

            // This shouldn't happen, but there might be some legacy holidays that this applies to
            // due to a prior bug where validation was missing.
            if (item.EndDate.IsBefore(item.StartDate)) {
                this.ErrorMessage = R.ValidationStartEndDateMismatch;

                return false;
            }

            if (item.StartDate.IsBefore(year.StartDate)) {
                this.ErrorMessage = Catalog.GetString("The start of the holiday ({0}) cannot be before the start of the academic year ({1}).").WithArgs(
                    item.StartDate.ToString("D"), year.StartDate.ToString("D")
                );

                return false;
            }

            if (item.EndDate.IsAfter(year.EndDate)) {
                this.ErrorMessage = Catalog.GetString("The end of the holiday ({0}) cannot be after the end of the academic year ({1}).").WithArgs(
                    item.EndDate.ToString("D"), year.EndDate.ToString("D")
                );

                return false;
            }

            return true;
        }

        protected override async AsyncTask SaveImpl(Holiday item) {
            var year = this.InputAcademicYear;

            ItemAction action = IsNew ? ItemAction.Add : ItemAction.Update;

            try {
                await _repository.AddOrUpdateHolidayAsync(year, item);

                this.Messenger.Publish(new EntityEventMessage<Holiday>(this, item, action));

                await this.NavigationService.Close(this);
            }
            catch (ItemConflictException<Holiday> ex) {
                this.ErrorMessage = Catalog.GetString(
                    "The entered dates overlap with {0}. Holidays cannot overlap."
                ).WithArgs(String.Join(", ", ex.ConflictingItems.Select(x => x.Name)));
            }
            catch (Exception ex) {
                this.ErrorMessage = R.UnhandledErrorMessage;

                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);
            }
        }

        #region Deletion

        protected override Task<bool> GetCanDeleteAsync(Holiday inputItem) => AsyncTask.FromResult(
            this.InputAcademicYear.CanBeEditedBy(this.User)
        );

        protected override async AsyncTask Delete() {
            if (this.InputAcademicYear == null || this.InputItem == null) {
                return;
            }

            await _repository.DeleteHolidayAsync(this.InputItem);

            await this.NavigationService.ChangePresentation(EntityDeletedPresentationHint.ForEntity(this.InputItem, this));

            this.Messenger.Publish(new EntityEventMessage<Holiday>(this, this.InputItem, ItemAction.Delete));
        }

        #endregion

        protected override Task<Holiday> GetItemAsync(Guid guid) {
            return AsyncTask.FromResult(this.InputAcademicYear.Holidays.SingleOrDefault(x => x.Guid == guid && x.DeletedAt == null));
        }

        protected override Task<Holiday> GetDefaultItemAsync() {
            DateTime defaultDate = DateTime.Today;

            if (!defaultDate.IsBetween(this.InputAcademicYear.StartDate, this.InputAcademicYear.EndDate)) {
                defaultDate = this.InputAcademicYear.StartDate;
            }

            return AsyncTask.FromResult(
                new Holiday {
                    StartDate = defaultDate,
                    EndDate = defaultDate,
                    PushesSchedule = !InputAcademicYear.Scheduling.IsFixed
                }
            );
        }

        [UsedImplicitly] private IDisposable _dateRangeEnforcer;

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemChanged(Holiday oldItem, Holiday newItem) {
            base.OnInputItemChanged(oldItem, newItem);

            if (newItem != null) {
                var inputAcademicYear = this.InputAcademicYear;

                var enforcer = new DateRangeEnforcer<Holiday>(newItem, x => x.StartDate, x => x.EndDate) {
                    MinRangeDays = 0,
                    AllowedDays = (inputAcademicYear?.Scheduling.Days).GetValueOrDefault(DaysOfWeek.Weekdays)
                };

                if (inputAcademicYear != null) {
                    enforcer.MinDate = inputAcademicYear.StartDate;
                    enforcer.MaxDate = inputAcademicYear.EndDate;
                }

                _dateRangeEnforcer = enforcer;

                this.SetShowPushesSchedule();
                this.SetPushesScheduleHelpText();
            }
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnInputItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            base.OnInputItemPropertyChanged(sender, e);

            if (e.PropertyName.In("StartDate", "EndDate") && !InputAcademicYear.Scheduling.IsFixed) {
                this.SetShowPushesSchedule();
                this.SetPushesScheduleHelpText();
            }
            else if (e.PropertyName == "PushesSchedule") {
                this.SetPushesScheduleHelpText();
            }
        }

        private void SetShowPushesSchedule() {
            this.ShowPushesSchedule = (
                InputAcademicYear.Scheduling.IsDayRotation ||
                (InputAcademicYear.Scheduling.IsWeekRotation && this.InputItem.Duration >= 5)
            );
        }

        private void SetPushesScheduleHelpText() {
            var year = this.InputAcademicYear;

            if (!this.ShowPushesSchedule || (
                !this.InputItem.StartDate.IsBetween(year.StartDate, year.EndDate) &&
                !this.InputItem.EndDate.IsBetween(year.StartDate, year.EndDate)
            ) || this.InputItem.EndDate >= year.EndDate) {
                this.PushesScheduleHelpText = null;
                return;
            }

            var fictitiousYear = new AcademicYear {
                StartDate = year.StartDate,
                EndDate = year.EndDate,
                Scheduling = year.Scheduling.Clone(),
                // By removing all holidays after the current holiday, we can
                // always show the day/week after the holiday regardless of any
                // other holidays.
                Holidays = year.Holidays.Where(x =>
                    x.StartDate.Date <= this.InputItem.EndDate.Date &&
                    x.Guid != this.InputItem.Guid
                ).ToObservableCollection()
            };

            fictitiousYear.Holidays.Add(this.InputItem);

            string text;
            int? rotation;

            if (fictitiousYear.Scheduling.IsWeekRotation) {
                var date = this.InputItem.EndDate.StartOfWeek();

                if (date < this.InputItem.StartDate || this.InputItem.EndDate.Range(date).CalendarDays >= AgendaScheduler.WeekRotationHolidayThreshold) {
                    date = date.AddWeeks(1);
                }
                else {
                    date = this.InputItem.EndDate.AddDays(1d);
                }

                rotation = Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetRotationWeekForDate(fictitiousYear, date);

                text = "Week after holiday will be week {0}";
            }
            else {
                var dayRotationDays = fictitiousYear.Scheduling.Days.GetValueOrDefault(DaysOfWeek.Weekdays);
                var followingDay = this.InputItem.EndDate;

                do {
                    followingDay = followingDay.AddDays(1);
                } while ((dayRotationDays & followingDay.DayOfWeek.ToFlag()) == 0);

                rotation = Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetRotationDayForDate(fictitiousYear, followingDay);

                text = "First day after holiday will be day {0}";
            }

            string rotationStr = "unknown";

            if (rotation.HasValue) {
                rotationStr = User.Current.Settings.IsRotationScheduleLettered
                    ? Humanize.NumberToLetter(rotation.Value)
                    : rotation.ToString();
            }

            this.PushesScheduleHelpText = this.Catalog.GetString(text).WithArgs(rotationStr);
        }
    }
}
