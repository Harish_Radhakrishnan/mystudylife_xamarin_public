﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Utility;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.Input {
    /// <summary>
    ///     View model for entities which are directly or indirectly dependent on academic years or terms.
    /// </summary>
    public abstract class SchedulableInputViewModel<T> : InputViewModel<T> where T : BaseUserEntity {

        private ObservableCollection<AcademicYear> _academicYears = new ObservableCollection<AcademicYear>();
        private IAcademicSchedule _selectedSchedule;

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return _academicYears; }
            protected set { _academicYears = value; }
        }

        [DoNotNotify]
        public IAcademicSchedule SelectedSchedule {
            get { return _selectedSchedule; }
            set {
                var oldItem = _selectedSchedule;

                if (SetProperty(ref _selectedSchedule, value) || (_selectedSchedule == null && value == null)) {
                    OnSelectedScheduleChanged(oldItem, value);

                    RaisePropertyChanged(() => SelectedScheduleText);
                }
            }
        }

        public string SelectedScheduleText {
            get {
                if (this.SelectedSchedule == null) {
                    return this.AcademicYears.Count == 0 ? "No years/terms" : "No year/term selected";
                }

                var term = this.SelectedSchedule as AcademicTerm;

                if (term != null) {
                    return String.Format("{0} | {1}",
                        term.Year,
                        term.Name
                    );
                }

                return this.SelectedSchedule.ToString();
            }
        }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) { }

        private bool _canAddYears;
        private MvxAsyncCommand _newAcademicYearCommand;

        [DoNotNotify]
        public bool CanAddYears {
            get => _canAddYears;
            set {
                if (SetProperty(ref _canAddYears, value)) {
                    _newAcademicYearCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand NewAcademicYearCommand => _newAcademicYearCommand ??= new MvxAsyncCommand(
            () => ShowViewModel<AcademicYearInputViewModel>(),
            () => CanAddYears
        );

        protected SchedulableInputViewModel() {
// ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Invalidate();
        }

        protected virtual async void Invalidate() {
            using (var acYearsRepo = new AcademicYearRepository()) {
                this.AcademicYears = (await acYearsRepo.GetAllNonDeletedAsync()).ToObservableCollection();
            }
        }

        public virtual async void Init(NavObject navObject) {
            if (!navObject.Guid.IsEmpty() && (this.InputItem = (await this.Repo.GetAsync(navObject.Guid)).Clone()) != null) {
                this.CanDelete = await GetCanDeleteAsync(this.InputItem);

                this.IsLoaded = true;

                return;
            }

            this.InputItem = await GetDefaultItemAsync();

            using (var acYearsRepo = new AcademicYearRepository()) {
                if (navObject.ScheduleGuid.IsEmpty()) {
                    this.SelectedSchedule = await acYearsRepo.GetCurrentScheduleAsync();
                }
                else {
                    this.SelectedSchedule = await acYearsRepo.GetAsync(navObject.ScheduleGuid);
                }
            }

            this.ChangeTracker = new ChangeTracker(this.InputItem);

            this.CanAddYears = await Mvx.IoCProvider.Resolve<IFeatureService>().CanUseFeatureAsync(Feature.WriteAcademicYears);

            this.IsLoaded = true;
        }

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<EntityEventMessage<AcademicYear>>(OnMessage);
        }

        private void OnMessage(EntityEventMessage<AcademicYear> message) {
            // This is a local only event so if the user has managed to add another
            // subject the chances are they want it for the current InputItem.
            if (message.Action == ItemAction.Add && message.Entity != null) {
                this.AcademicYears.Add(message.Entity);
                this.SelectedSchedule = message.Entity;
            }
        }

        #region NavObject

        public new class NavObject {

            public Guid Guid { get; set; }

            public Guid ScheduleGuid { get; set; }
        }

        #endregion
    }
}
