﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.Net;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Models;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.UI.ViewModels.View;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.UI.ViewModels.Base {
    public abstract class BaseViewModel : MvxViewModel, ILoaded {
        private IAnalyticsService _analytics;
        private ICatalog _catalog;
        private IMvxMessenger _messenger;
        private IBugReporter _bugReporter;
        private IDialogService _dialogService;
        private INavigationService _navigationService;

        // ReSharper disable once CollectionNeverQueried.Local
        private readonly List<MvxSubscriptionToken> _messengerTokens = new List<MvxSubscriptionToken>();

        protected IAnalyticsService Analytics => _analytics ??= Mvx.IoCProvider.GetSingleton<IAnalyticsService>();
        protected ICatalog Catalog => _catalog ??= Mvx.IoCProvider.Resolve<ICatalog>();
        protected IMvxMessenger Messenger => _messenger ??= Mvx.IoCProvider.GetSingleton<IMvxMessenger>();
        protected IBugReporter BugReporter => _bugReporter ??= Mvx.IoCProvider.GetSingleton<IBugReporter>();
        protected IDialogService DialogService => _dialogService ??= Mvx.IoCProvider.Resolve<IDialogService>();
        protected INavigationService NavigationService => _navigationService ??= Mvx.IoCProvider.Resolve<INavigationService>();

        protected virtual void SubscribeToMessages(IMvxMessenger messenger) { }

        protected void On<TMessage>(Action<TMessage> deliveryAction, Thread thread = Thread.CurrentThread) where TMessage : MvxMessage {
            switch (thread) {
                case Thread.MainThread:
                    _messengerTokens.Add(this.Messenger.SubscribeOnMainThread(deliveryAction));
                    break;
                case Thread.ThreadPoolThread:
                    _messengerTokens.Add(this.Messenger.SubscribeOnThreadPoolThread(deliveryAction));
                    break;
                default:
                    _messengerTokens.Add(this.Messenger.Subscribe(deliveryAction));
                    break;
            }
        }

        protected void On<TMessage>(Func<TMessage, System.Threading.Tasks.Task> deliveryAction) where TMessage : MvxMessage {
            _messengerTokens.Add(this.Messenger.SubscribeOnThreadPoolThread((TMessage message) => {
                deliveryAction(message).ContinueWith(task => {
                    if (task.IsFaulted) {
                        var ex = task.Exception?.InnerException;

                        Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogError($"Caught exception during handling {typeof(TMessage).Name}: " + ex.ToLongString());
                        _bugReporter.Send(ex, true, new List<string> { GetType().Name, typeof(TMessage).Name });
                    }
                });
            }));
        }

        protected enum Thread {
            CurrentThread, MainThread, ThreadPoolThread
        }

        #region Busy / Network Connectivity

        private bool _isBusy;
        private bool _isOnline;

        public bool IsBusy {
            get => _isBusy;
            protected set => SetProperty(ref _isBusy, value);
        }

        public bool IsOnline {
            get => _isOnline;
            private set => SetProperty(ref _isOnline, value);
        }

        #endregion

        #region User

        public User User { get; private set; }

        #endregion

        #region App State

        protected IAppState AppState { get; private set; }

        #endregion

        #region Commands

        private MvxAsyncCommand<object> _viewEntityCommand;
        private MvxAsyncCommand<ScheduledClass> _viewScheduledClassCommand;
        private MvxAsyncCommand<BaseUserEntity> _editEntityCommand;
        private MvxAsyncCommand<string> _navigateCommand;
        private MvxAsyncCommand _closeCommand;

        public ICommand ViewEntityCommand {
            get { return _viewEntityCommand ??= new MvxAsyncCommand<object>(ViewEntity); }
        }

        public ICommand ViewScheduledClassCommand {
            get {
                return _viewScheduledClassCommand ??= new MvxAsyncCommand<ScheduledClass>(ViewScheduledClass);
            }
        }

        public ICommand EditEntityCommand {
            get { return _editEntityCommand ??= new MvxAsyncCommand<BaseUserEntity>(EditEntity); }
        }

        public ICommand NavigateCommand {
            get { return _navigateCommand ??= new MvxAsyncCommand<string>(Navigate); }
        }

        public ICommand CloseCommand {
            get { return _closeCommand ??= new MvxAsyncCommand(() => this.NavigationService.Close(this)); }
        }

        protected async System.Threading.Tasks.Task ViewEntity(object e) {
            Type viewModel;

            if (e is IModel model) {
                e = model.Base;
            }

            if (e is Class) {
                viewModel = typeof(ClassViewModel);
            }
            else if (e is Task) {
                viewModel = typeof(TaskViewModel);
            }
            else if (e is Exam) {
                viewModel = typeof(ExamViewModel);
            }
            else {
                throw new NotSupportedException("Given type is not yet supported, you have some work to do!");
            }

            await NavigationService.Navigate(viewModel, new EntityViewModel<SubjectDependentEntity>.NavObject {
                Guid = ((SubjectDependentEntity)e).Guid
            });
        }

        protected async System.Threading.Tasks.Task ViewScheduledClass(ScheduledClass sc) {
            var navObject = new ClassViewModel.ScheduledNavObject {
                Guid = sc.ClassGuid,
                Date = sc.Date
            };

            if (sc.ClassTime != null) {
                navObject.TimeGuid = sc.ClassTime.Guid;
            }

            await ShowViewModel<ClassViewModel>(navObject);
        }

        protected async System.Threading.Tasks.Task EditEntity<T>(T e)
            where T : BaseUserEntity {

            Type viewModel;

            if (e is Subject) {
                viewModel = typeof(SubjectInputViewModel);
            }
            else if (e is Class) {
                viewModel = typeof(ClassInputViewModel);
            }
            else if (e is Task) {
                viewModel = typeof(TaskInputViewModel);
            }
            else if (e is Exam) {
                viewModel = typeof(ExamInputViewModel);
            }
            else {
                throw new NotSupportedException(
                    $"Given type '{typeof(T)}' is not yet supported, you have some work to do!");
            }

            await NavigationService.Navigate(viewModel, new SchedulableInputViewModel<Class>.NavObject {
                Guid = e.Guid
            });
        }

        private async System.Threading.Tasks.Task Navigate(string viewName) {
            Type tViewModel = Type.GetType($"MyStudyLife.UI.ViewModels.{viewName}ViewModel");

            if (tViewModel == null)
                throw new Exception($"Could not find view with name {viewName}ViewModel");

            await this.NavigationService.Navigate(tViewModel);
        }

        protected async System.Threading.Tasks.Task ShowViewModel<TViewModel>() where TViewModel : IMvxViewModel {
            await this.NavigationService.Navigate<TViewModel>();
        }

        protected async System.Threading.Tasks.Task ShowViewModel<TViewModel>(object parameterBundle) where TViewModel : IMvxViewModel {
            await this.NavigationService.Navigate(typeof(TViewModel), parameterBundle);
        }

        #endregion

        #region ILoaded

        public virtual bool IsLoaded { get; protected set; }

        #endregion

        /// <summary>
        ///		Closes the view model, navigating
        ///		back through the navigation stack.
        /// </summary>
        public void GoBack() {
            this.NavigationService.Close(this);
        }

        private NavigationBackStack.Entry _navigationEntry;

        protected NavigationBackStack.Entry NavigationEntry =>
            _navigationEntry ??= Mvx.IoCProvider.Resolve<INavigationService>().BackStack.Peek(this.GetType());

        #region Ctor(...)

        protected BaseViewModel() {
            this.IsOnline = Mvx.IoCProvider.GetSingleton<INetworkProvider>().IsOnline;

            this.On<ProxyEventMessage<NetworkConnectivityChangedEventArgs>>(NetworkProviderOnConnectivityChanged, Thread.MainThread);
            this.On<ProxyEventMessage<SyncStatusChangedEventArgs>>(SyncServiceOnStatusChanged, Thread.MainThread);
            this.On<ProxyEventMessage<SyncCompletedEventArgs>>(SyncServiceOnCompleted, Thread.MainThread);
            this.On<ProxyEventMessage<SyncErrorEventArgs>>(SyncServiceOnError, Thread.MainThread);

            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            this.SubscribeToMessages(this.Messenger);

            // This is still required, despite the coalescing property as there's a higher
            // chance that the navigation entry here will be the right one.
            this._navigationEntry = Mvx.IoCProvider.Resolve<INavigationService>().BackStack.Peek(this.GetType());
        }

        #endregion

        #region Start

        public override void Start() {
            base.Start();

            StartAsyncCore().ContinueWith(t => {
                if (t.IsFaulted) {
                    var ex = t.Exception?.InnerException;

                    // TODO: Show error
                    BugReporter.Send(ex, true, new List<string> { "ViewModelStart", this.GetType().Name });

                    Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(GetType().Name).LogError($"Caught exception during {this.GetType().Name}.Start: " + ex.ToLongString());

                    throw ex;
                }

                if (t.Result) {
                    this.IsLoaded = true;
                }
            });
        }

        private async Task<bool> StartAsyncCore() {
            this.AppState = await Mvx.IoCProvider.Resolve<IAppStateManager>().GetStateAsync();

            this.User = this.AppState.User;

            return await this.StartAsync();
        }

        /// <summary>
        ///     Called after Init, safe to override.
        /// </summary>
        /// <returns>
        ///     True if whatever was happening was successful. Used to enable
        ///     overriding in scenarios where exceptions wouldn't be thrown,
        ///     instead the VM would be closed etc.
        ///
        ///     Default false as it would also prevent <see cref="IsLoaded"/> being
        ///     set to <c>true</c> in VMs which don't use this model yet.
        /// </returns>
        protected virtual Task<bool> StartAsync() => System.Threading.Tasks.Task.FromResult(false);

        #endregion

        #region SetProperty(...)

        /// <summary>
        ///     Checks if a property already matches a desired value.  Sets the property and
        ///     notifies listeners only when necessary.
        /// </summary>
        /// <typeparam name="T">Type of the property.</typeparam>
        /// <param name="storage">Reference to a property with both getter and setter.</param>
        /// <param name="value">Desired value for the property.</param>
        /// <param name="propertyName">
        ///     Name of the property used to notify listeners.  This
        ///     value is optional and can be provided automatically when invoked from compilers that
        ///     support CallerMemberName.
        /// </param>
        /// <returns>True if the value was changed, false if the existing value matched the
        /// desired value.</returns>
        protected new bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null) {
            // We use Ref Equals, MvvmCross uses Equals
            if (ReferenceEquals(storage, value))
                return false;

            storage = value;
            // ReSharper disable once ExplicitCallerInfoArgument
            this.RaisePropertyChanged(propertyName);

            return true;
        }

        #endregion

        #region InvokeOnMainThread(...)

        protected System.Threading.Tasks.Task InvokeOnMainThreadAsync(Action action)
            => InvokeOnMainThreadAsync(() => { action(); return true; });

        protected System.Threading.Tasks.Task InvokeOnMainThreadAsync<T>(Func<T> action) {
            var tcs = new TaskCompletionSource<T>();

            InvokeOnMainThread(() => {
                try {
                    tcs.SetResult(action());
                }
                catch (Exception ex) {
                    tcs.SetException(ex);
                }
            });

            return tcs.Task;
        }

        #endregion

        #region Virtual Eventing Methods

        #region Helper Eventing Methods

        private void NetworkProviderOnConnectivityChanged(ProxyEventMessage<NetworkConnectivityChangedEventArgs> msg) {
            this.IsOnline = msg.Args.IsOnline;

            OnNetworkConnectivityChanged(msg.Args.IsOnline, msg.Args.CanUseConnection);
        }

        private void SyncServiceOnStatusChanged(ProxyEventMessage<SyncStatusChangedEventArgs> msg) {
            this.IsBusy = msg.Args.NewStatus == SyncStatus.Syncing;

            OnSyncStatusChanged(msg.Args);
        }

        private void SyncServiceOnCompleted(ProxyEventMessage<SyncCompletedEventArgs> msg) {
            var e = msg.Args;

            if (e.UserModified) {
                this.User = e.User;
                AppState.SetUser(user: e.User);
            }
            OnSyncCompleted(e);
        }

        private void SyncServiceOnError(ProxyEventMessage<SyncErrorEventArgs> msg) => OnSyncError(msg.Args);

        #endregion

        /// <summary>
        /// Triggered when the network connectivity changes.
        /// </summary>
        /// <param name="isOnline">Whether the device is connected to the internet or not.</param>
        /// <param name="canUseConnection">Whether we can use the connection or not..</param>
        protected virtual void OnNetworkConnectivityChanged(bool isOnline, bool canUseConnection) { }

        /// <summary>
        /// Triggered when a single enti
        /// </summary>
        /// <param name="e">Event Args containing details about the status change.</param>
        protected virtual void OnSyncStatusChanged(SyncStatusChangedEventArgs e) { }

        /// <summary>
        /// Triggered when a sync of any type completes successfully.
        /// </summary>
        /// <param name="e">Event Args containing details about the synchronization.</param>
        protected virtual void OnSyncCompleted(SyncCompletedEventArgs e) { }

        /// <summary>
        /// Trigger when an error occurs during sync.
        /// </summary>
        /// <param name="e">Event Args containg details about the error.</param>
        protected virtual void OnSyncError(SyncErrorEventArgs e) { }

        #endregion
    }
}