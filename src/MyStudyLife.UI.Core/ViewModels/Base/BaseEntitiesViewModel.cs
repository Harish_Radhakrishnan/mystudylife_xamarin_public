﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Utility;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Base {
    public abstract class BaseEntitiesViewModel<T> : BaseViewModel, ISearchable
        where T : SubjectDependentEntity, new() {

        private readonly string _pluralType;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IFeatureService _featureService;
        private readonly IAcademicYearRepository _academicYearRepository;
        private readonly ISubjectDependentRepository<T> _repository;
        private bool _canUseExtendedColors;

        [DoNotNotify]
        public override bool IsLoaded {
            get { return base.IsLoaded; }
            protected set {
                base.IsLoaded = value;

                if (value) {
                    RaisePropertyChanged(() => this.FilterText);
                }
            }
        }

        private ObservableCollection<T> _items;
        private Subject _selectedSubject;
        private readonly ObservableCollection<object> _selectedItems = new ObservableCollection<object>();

		#region Academic Schedule

		private ObservableCollection<AcademicYear> _academicYears;
        private IAcademicSchedule _selectedSchedule;
	    private bool _isCurrentSchedule;

	    public ObservableCollection<AcademicYear> AcademicYears {
			get { return _academicYears ?? (_academicYears = new ObservableCollection<AcademicYear>()); }
		    private set { _academicYears = value; }
	    }

        [DoNotNotify]
        public IAcademicSchedule SelectedSchedule {
		    get { return _selectedSchedule; }
		    set {
			    var oldSchedule = _selectedSchedule;

			    if (SetProperty(ref _selectedSchedule, value)) {
					OnSelectedScheduleChanged(oldSchedule, value);
				}
		    }
	    }

		public string SelectedScheduleText {
            get {
                if (this.SelectedSchedule == null) {
                    return this.AcademicYears.Any() ? R.NoAcademicScheduleText : null;
                }

				return this.SelectedSchedule.ToString();
			}
		}

        public string ScheduleSelectionHelpText {
            get { return String.Format(R.AcademicScheduleTaskExamListHelp, _pluralType); }
        }

        [DoNotNotify]
	    public bool IsCurrentSchedule {
		    get { return _isCurrentSchedule; }
			set {
				if (SetProperty(ref _isCurrentSchedule, value)) {
					RaisePropertyChanged(() => this.FilterText);
				}
			}
	    }

	    public async AsyncTask SetIsCurrentScheduleAsync() {
		    if (this.SelectedSchedule == null) {
			    this.IsCurrentSchedule = false;
		    }
		    else {
			    var currSchedule = await this._academicYearRepository.GetCurrentScheduleAsync();

			    this.IsCurrentSchedule = currSchedule != null && currSchedule.Guid == this.SelectedSchedule.Guid;
		    }
	    }

        protected virtual async void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
			await RaisePropertyChanged(() => SelectedScheduleText);

			await this.SetIsCurrentScheduleAsync();
			await this.SetSubjectsByScheduleAsync();
		}

        #endregion

        public System.Drawing.Color? Color { get; private set; }

        public ObservableCollection<Subject> Subjects { get; private set; }

        /// <summary>
        ///     The collection of subjects.
        /// </summary>
        public SortedObservableCollection<Subject> SubjectsWithNull { get; private set; }

        /// <summary>
        ///     Returns true if subjects are present (excluding null "-- All Classes --" subject).
        /// </summary>
        public bool HasSubjects => this.SubjectsWithNull.Count > 1;

        /// <summary>
        ///     The collection of items filtered by the user's current preferences.
        /// </summary>
        [DoNotNotify]
        public ObservableCollection<T> Items {
            get { return _items ?? (_items = new ObservableCollection<T>()); }
            private set {
                OnItemsChanging(_items, value);

                if (SetProperty(ref _items, value)) {
                    OnItemsChanged();
                }
            }
        }

        public abstract CollectionViewSource<T> ViewSource { get; }

        /// <summary>
        /// The subject selected to filter the list.
        /// </summary>
        [DoNotNotify]
        public Subject SelectedSubject {
            get { return _selectedSubject; }
            set {

                bool ignore = _selectedSubject == null && value != null && value.Color == "NULL";

                if (SetProperty(ref _selectedSubject, value)) {
                    this.Color = value != null ? UserColors.GetColor(value.Color, _canUseExtendedColors)?.Color : null;

                    this.RaisePropertyChanged(nameof(this.Title));

                    if (!ignore) {
#pragma warning disable 4014
                        SetItemsBySubjectAsync();
#pragma warning restore 4014
                    }
                }
            }
        }

	    public virtual string Title
            // Non-user data, ToTitleCase required.
            => $"{this.SelectedSubject?.Name} {this._pluralType.ToTitleCase()}".Trim();

        /// <summary>
        /// The selected tasks.
        /// </summary>
        public ObservableCollection<object> SelectedItems => _selectedItems;

        /// <summary>
        /// The text displayed when no tasks are shown.
        /// </summary>
        public string LonelyText { get; set; }

	    public string FilterText {
		    get {
		        if (!this.IsLoaded) return null;

				// TODO: Localize
			    string s = null;

			    if (this.SelectedSchedule != null) {
				    if (!this.IsCurrentSchedule) {
					    s = String.Format(
						    "Showing {0} for non-current {1}: {2}",
						    _pluralType,
                            this.SelectedSchedule is AcademicTerm ? "term" : "year",
						    this.SelectedSchedule
						);
				    }
			    }
				else if (this.AcademicYears.Count > 0) {
					s = String.Format("Showing all {0} regardless of year/term", _pluralType);
				}

				if (!String.IsNullOrWhiteSpace(QueryText)) {
					if (this.IsCurrentSchedule) {
						s = String.Format("Search results for \"{0}\"", this.QueryText);
					}

					s = (s ?? String.Empty) + String.Format(" filtered by the query \"{0}\"", this.QueryText);
				}

			    return s;
		    }
	    }

        #region Selected Item

        private IDisposable _selectedItemSubscription;

        private T _selectedItem;

        /// <summary>
        ///     The selected TEntity.
        /// </summary>
        [Obsolete("Not sure this is actually used")]
        public T SelectedItem {
            get { return _selectedItem; }
            set {
                if (_selectedItemSubscription != null) {
                    _selectedItemSubscription.Dispose();
                    _selectedItemSubscription = null;
                }

                var oldItem = this._selectedItem;

                SetProperty(ref _selectedItem, value);

                OnSelectedItemChanged(oldItem, value);

                if (value != null) {
                    _selectedItemSubscription = value.WeakSubscribe(OnSelectedItemPropertyChanged);
                }
            }
        }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnSelectedItemPropertyChanged(object sender, PropertyChangedEventArgs e) { }
        
        [SuppressPropertyChangedWarnings]
        protected virtual void OnSelectedItemChanged(T oldValue, T newValue) { }

        #endregion

        #region Commands

        private ICommand _newCommand;
        private ICommand _deleteSelectedCommand;
        private ICommand _clearQueryCommand;

        public ICommand NewCommand => _newCommand ?? (_newCommand = new MvxAsyncCommand(New));
        public ICommand DeleteSelectedCommand => _deleteSelectedCommand ??= new MvxAsyncCommand(DeleteSelected);
        public ICommand ClearQueryCommand => _clearQueryCommand ??= new MvxCommand(ClearQuery);

        private async System.Threading.Tasks.Task New() {
            var navObject = new SubjectDependentInputViewModel<T>.NavObject {
                SubjectGuid = this.SelectedSubject?.Guid ?? Guid.Empty,
                ScheduleGuid = this.SelectedSchedule?.Guid ?? Guid.Empty
            };

            if (typeof (T) == typeof (Class)) {
                await ShowViewModel<ClassInputViewModel>(navObject);
            }
            else if (typeof(T) == typeof(MyStudyLife.Data.Task)) {
                await ShowViewModel<TaskInputViewModel>(navObject);
            }
            else if (typeof(T) == typeof(Exam)) {
                await ShowViewModel<ExamInputViewModel>(navObject);
            }
        }

        #endregion

        protected BaseEntitiesViewModel(
            string pluralType,
            IFeatureService featureService,
            ISubjectRepository subjectRepository,
            IAcademicYearRepository academicYearRepository,
            ISubjectDependentRepository<T> repository
        ) {
            this._featureService = featureService;
            this._subjectRepository = subjectRepository;
            this._academicYearRepository = academicYearRepository;
            this._repository = repository;

            if (String.IsNullOrWhiteSpace(pluralType)) {
                throw new ArgumentNullException(nameof(pluralType));
            }

            this._pluralType = pluralType;
        }

        #region Overrides of MvxViewModel

        public override async void Start() {
            await this.Invalidate();

            this.IsLoaded = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Invalidates the current view model causing a refresh of all data.
        /// </summary>
        public async AsyncTask Invalidate() {
            _canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);

            // Main
            this.SelectedItems.Clear();

            this.AcademicYears = (await this._academicYearRepository.GetAllNonDeletedAsync()).ToObservableCollection();

            if (this.SelectedSchedule == null) {
                this.SelectedSchedule = await this._academicYearRepository.GetClosestScheduleToCurrentAsync();
            }

            // If it's null RaisePropertyChanged isn't called because it's
            // the same as the existing value.
            if (this.SelectedSchedule != null) {
                await this.SetIsCurrentScheduleAsync();
            }

            await SetSubjectsByScheduleAsync();

            if (this.SelectedSubject == null) {
                this.SelectedSubject = this.SubjectsWithNull.Single(x => x.Color == "NULL");
            }

            await SetItemsBySubjectAsync();
        }

        public async AsyncTask SetSubjectsByScheduleAsync() {
            this.Subjects = (await this._subjectRepository.GetByScheduleAsync(this.SelectedSchedule)).OrderBy(x => x.Name).ToObservableCollection();
            var subjectsWithNull = this.Subjects.ToList();

            // This is shit
			var nullSubject = new Subject {
				Color = "NULL"
			};
			subjectsWithNull.Insert(0, nullSubject);

			this.SubjectsWithNull = new SortedObservableCollection<Subject>(x => x.Name, subjectsWithNull);
		}

		public virtual async AsyncTask SetItemsBySubjectAsync() {
			Task<IEnumerable<T>> items;

			if (!String.IsNullOrEmpty(this.QueryText)) {
				this.SelectedSubject = null;

				items = this._repository.GetByQueryTextAsync(this.QueryText);
			}
			else if (this.SelectedSubject == null || this.SelectedSubject.Guid.IsEmpty()) {
				items = this._repository.GetByScheduleAsync(this.SelectedSchedule);
			}
			else {
				items = this._repository.GetBySubjectAsync(this.SelectedSubject);
			}

		    this.Items = (await items).ToObservableCollection();

			SetLonelyText();
		}

        /// <summary>
        ///     Sets the lonely text.
        /// </summary>
        public void SetLonelyText() {
            string filterText = String.Empty; //this.Filter != null ? String.Concat(" ", this.Filter.GetFilterTitle()) : String.Empty;

            if (this.IsFilteredByQuery) {
                this.LonelyText = Catalog.GetString("No {0} match your search.").WithArgs(this._pluralType);
            }
            else {
                string subjectText = this.SelectedSubject == null || this.SelectedSubject.Name == null
                                         ? String.Empty
                                         : String.Concat(" for ", this.SelectedSubject.Name);

                this.LonelyText = String.Format("No{0} {1}{2}.", filterText, this._pluralType, subjectText);
            }
        }

        public async AsyncTask DeleteSelected() {
            var selectedItems = this.SelectedItems.Cast<T>().ToList(); // ToList essential.

            foreach (var x in selectedItems.Where(t => !t.DeletedAt.HasValue))
                x.DeletedAt = DateTime.UtcNow;

            await Repository<T>.GetInstance(Mvx.IoCProvider.Resolve<IStorageProvider>()).DeleteAsync(selectedItems);

            this.Items.RemoveRange(selectedItems);

            foreach (T x in selectedItems) {
                this.Messenger.Publish(new EntityEventMessage<T>(this, x, ItemAction.Delete));
            }
        }

        #endregion

        #region ISearchable

        private string _queryText;

        public string QueryText {
            get { return _queryText; }
            set {
	            if (SetProperty(ref _queryText, value)) {
		            RaisePropertyChanged(() => this.FilterText);
	            }
            }
        }

        public bool IsFilteredByQuery {
            get { return !String.IsNullOrWhiteSpace(_queryText); }
        }

        public async AsyncTask ProcessQueryText(string queryText) {
            if (String.IsNullOrWhiteSpace(queryText) && this.QueryText == null) {
                return;
            }

            if (String.Equals(this.QueryText, queryText, StringComparison.CurrentCultureIgnoreCase)) {
                return;
            }
            else {
                this.QueryText = queryText;

                this.SelectedSubject = null;
            }

            await SetItemsBySubjectAsync();
        }

        public async void ClearQuery() {
            bool setItems = !String.IsNullOrWhiteSpace(this.QueryText);

            this.QueryText = null;

            if (setItems) {
                await SetItemsBySubjectAsync();
            }
        }

        #endregion

        #region Virtual Eventing

        protected virtual void OnItemsChanging(ObservableCollection<T> oldItems, ObservableCollection<T> newItems) {}
        protected virtual void OnItemsChanged() {}

        #endregion

        #region OnSyncCompleted(...)

        protected override async void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.HasAnyUserDataChanged) {
                // Update everything.
                await this.Invalidate();
            }
        }

        /// <summary>
        ///     Merges the given collection of <paramref name="modifiedItems"/>
        ///     with the current collection of <see cref="Items"/>.
        /// </summary>
        /// <param name="modifiedItems">The items to merge.</param>
        protected void MergeModified(IEnumerable<T> modifiedItems) {
            InvokeOnMainThread(() => {
                // Remove entries which have been deleted or updated.
                this.Items.RemoveAll(a => modifiedItems.Any(b => a.Guid == b.Guid));
                // Add new entries or the new updated entries which have just been removed.
                this.Items.AddRange(modifiedItems.Where(t => !t.DeletedAt.HasValue));
            });
        }

        #endregion

		#region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            this.On<EntityEventMessage<AcademicYear>>(OnMessage);
            this.On<EntityEventMessage<Subject>>(OnMessage);
            this.On<EntityEventMessage<T>>(OnMessage);
		}

        protected virtual async void OnMessage(EntityEventMessage<AcademicYear> message) {
			await this.Invalidate();
		}

        protected virtual async void OnMessage(EntityEventMessage<Subject> message) {
			await this.Invalidate();
		}

		private async void OnMessage(EntityEventMessage<T> message) {
		    if (message.Entity == null) {
                await this.SetItemsBySubjectAsync();
		    }
		    else switch (message.Action) {
		        case ItemAction.Delete:
		            this.Items.RemoveAll(x => x.Guid == message.Entity.Guid);

		            OnItemDeletedLocal(message.Entity);
		            break;
		        case ItemAction.Update: {
                    var itemInCollection = this.Items.SingleOrDefault(x => x.Guid == message.Entity.Guid);

                    // TODO(eventually) check items are identical rather than the reference
                    // TODO: Replace item, not copy and use colleciton change event...
                    if (itemInCollection != null && !ReferenceEquals(message.Entity, itemInCollection)) {
                        OnItemUpdatingLocal(message.Entity);

                        message.Entity.CopyPropertyValuesTo(itemInCollection);

                        OnItemUpdatedLocal(itemInCollection);
                    }
		        }
		            break;
		        case ItemAction.Add: {

		            OnItemAddingLocal(message.Entity);

                    if (!String.IsNullOrEmpty(this.QueryText)) {
                        await this.SetItemsBySubjectAsync();
		            }
                    // Guid will be empty when using SubjectsWithNull... should probably change that.
                    else if (this.SelectedSubject != null && !this.SelectedSubject.Guid.IsEmpty()) {
                        if (message.Entity.Subject.Guid == this.SelectedSubject.Guid) {
                            this.Items.Add(message.Entity);
                        }
                    }
                    else {
                        var entitySchedule = message.Entity.Subject.Schedule;

                        if (entitySchedule == null || this.SelectedSchedule == null) {
                            this.Items.Add(message.Entity);
                        }
                        else if (entitySchedule.EqualsRecursive(this.SelectedSchedule)) {
                            this.Items.Add(message.Entity);
                        }
                    }

                    OnItemAddedLocal(message.Entity);
		        }
		            break;
		    }
		}

        protected virtual void OnItemAddedLocal(T item) { }
        protected virtual void OnItemAddingLocal(T item) { }
        protected virtual void OnItemUpdatedLocal(T item) { }
        protected virtual void OnItemUpdatingLocal(T item) { }
        protected virtual void OnItemDeletedLocal(T item) { }

        #endregion
    }
}
