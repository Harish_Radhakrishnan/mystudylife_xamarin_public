﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Messaging;
using MyStudyLife.Sync;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Base {
    // TODO: This may as well be merged with SubjectsViewModel unless we can
    // share some of the schedule stuff with Tasks, Exams and Schedule
    public abstract class ScheduleDependentEntitiesViewModel<TEntity> : BaseViewModel
        where TEntity : BaseEntity, IScheduleRestrictable {
        
        private readonly IAcademicYearRepository _academicYearRepository;
        
        #region Academic Schedule

        private ObservableCollection<AcademicYear> _academicYears;
        private IAcademicSchedule _selectedSchedule;
        private bool _isCurrentSchedule;

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return _academicYears ?? (_academicYears = new ObservableCollection<AcademicYear>()); }
            private set { _academicYears = value; }
        }

        public IAcademicSchedule SelectedSchedule {
            get { return _selectedSchedule; }
            set {
                var oldSchedule = _selectedSchedule;

                if (SetProperty(ref _selectedSchedule, value)) {
                    OnSelectedScheduleChanged(oldSchedule, value);
                }
            }
        }

        public string SelectedScheduleText {
            get {
                if (this.SelectedSchedule == null) {
                    return this.AcademicYears.Any() ? R.NoAcademicScheduleText : null;
                }

                return this.SelectedSchedule.ToString();
            }
        }

        public bool IsCurrentSchedule {
            get { return _isCurrentSchedule; }
            set {
                if (SetProperty(ref _isCurrentSchedule, value)) {
                    RaisePropertyChanged(() => this.FilterText);
                }
            }
        }

        public async Task SetIsCurrentScheduleAsync() {
            if (this.SelectedSchedule == null) {
                this.IsCurrentSchedule = false;
            }
            else {
                var currentSchedule = await this._academicYearRepository.GetCurrentScheduleAsync();

                this.IsCurrentSchedule = currentSchedule != null && this.SelectedSchedule.Guid == currentSchedule.Guid;
            }
        }

        private async void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
            await this.SetIsCurrentScheduleAsync();
            await this.SetItemsByScheduleAsync(newSchedule);

            await RaisePropertyChanged(nameof(SelectedScheduleText));
        }

        #endregion

        private ObservableCollection<TEntity> _items;

        /// <summary>
        ///     The collection of items.
        /// </summary>
        [DoNotNotify]
        public ObservableCollection<TEntity> Items {
            get { return _items ?? (_items = new ObservableCollection<TEntity>()); }
            protected set {
                if (SetProperty(ref _items, value)) {
                    OnItemsChanged();
                }
            }
        }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnItemsChanged() { }

        public string FilterText {
            get {
                string str = null;

                if (this.SelectedSchedule != null && !this.IsCurrentSchedule) {
                    str = String.Format(
                        "Showing non-current {0}: {1}",
                        (this.SelectedSchedule is AcademicYear ? "year" : "term"),
                        this.SelectedSchedule
                    );
                }
                else if (this.AcademicYears.Any() && this.SelectedSchedule == null) {
                    str = "Showing all subjects which do not have a year / term assigned";
                }

                return str;
            }
        }

        protected ScheduleDependentEntitiesViewModel(IAcademicYearRepository academicYearRepository) {
            this._academicYearRepository = academicYearRepository;
        }

        private Guid? _navScheduleGuid;

        public void Init(NavObject navObject) {
            if (navObject == null || navObject.ScheduleGuid.IsEmpty()) {
                return;
            }

            _navScheduleGuid = navObject.ScheduleGuid;
        }

        public class NavObject {
            public Guid ScheduleGuid { get; set; }
        }

        protected override async Task<bool> StartAsync() {
            await this.Invalidate();

            return true;
        }

        protected async Task Invalidate() {
            this.AcademicYears = (await this._academicYearRepository.GetAllNonDeletedAsync()).ToObservableCollection();

            if (_navScheduleGuid.HasValue) {
                this.SelectedSchedule = await this._academicYearRepository.GetSchedule(_navScheduleGuid.Value);
            }

            if (this.SelectedSchedule == null) {
                this.SelectedSchedule = await this._academicYearRepository.GetClosestScheduleToCurrentAsync();
            }

            // If it's null RaisePropertyChanged isn't called because it's
            // the same as the existing value.
            if (this.SelectedSchedule == null) {
                await SetItemsByScheduleAsync(null);
                await this.SetIsCurrentScheduleAsync();
            }
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName.In(nameof(SelectedSchedule), nameof(AcademicYears))) {
                RaisePropertyChanged(nameof(this.FilterText));
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        protected abstract Task SetItemsByScheduleAsync(IAcademicSchedule schedule);

        #region Overrides of BaseViewModel

        /// <summary>
        /// Triggered when a sync of any type completes successfully.
        /// </summary>
        /// <param name="e">Event Args containing details about the synchronization.</param>
        protected async override void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.AcademicYearsModified) {
                await Invalidate();
            }

            base.OnSyncCompleted(e);
        }

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<EntityEventMessage<AcademicYear>>(OnMessage);
            this.On<EntityEventMessage<TEntity>>(OnMessage);
        }

        private async void OnMessage(EntityEventMessage<AcademicYear> message) {
            await Invalidate();
        }

        private async void OnMessage(EntityEventMessage<TEntity> message) {
            await Invalidate();
        }

        #endregion
    }
}
