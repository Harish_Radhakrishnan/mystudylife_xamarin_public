﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MyStudyLife.Data.Services;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;

namespace MyStudyLife.UI.ViewModels.Schools {
    public class SchoolWelcomeViewModel : Base.BaseViewModel {
        private readonly IUserService _userService;

        private MvxCommand _getStartedCommand;

        public ICommand GetStartedCommand => Command.Create(ref _getStartedCommand, GetStartedAsync);

        public bool IsSaving { get; private set; }

        public SchoolWelcomeViewModel(IUserService userService) {
            this._userService = userService;
        }

        private async void GetStartedAsync() {
            try {
                this.IsSaving = true;
                
                await _userService.SetupUser();

                this.IsSaving = false;
            }
            catch (Exception ex) {
                // Is busy MUST be false before we attempt to display a dialog...
                this.IsSaving = false;

                if (ex is HttpApiUnauthorizedException) {
                    await this.NavigationService.ChangePresentation(new AuthorizationChangedPresentationHint(Authorization.AuthStatusChangeCause.UnauthorizedHttpResponse));

                    return;
                }

                if (ex.IsTransientWebOrNaughtyHotspotException()) {
                    if (!this.IsOnline) {
                        DialogService.ShowDismissible(R.OfflineError, R.WelcomeWizardOfflineMessage);

                        return;
                    }
                }
                else {
                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, true, new List<string> { "SchoolWelcome" });
                }

                var apiEx = ex as HttpApiException;

                DialogService.ShowDismissible(R.UnhandledErrorTitle, apiEx?.ErrorMessage?.Message ?? String.Empty);

                return;
            }

            await this.NavigationService.ChangePresentation(new WelcomeWizardCompletedPresentationHint());
        }
    }
}