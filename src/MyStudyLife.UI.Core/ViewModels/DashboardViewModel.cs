using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.UI;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Messaging;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.Models;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.Reactive;
using MyStudyLife.UI.Utilities;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Utility;
using AsyncTask = System.Threading.Tasks.Task;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.UI.ViewModels
{
    using Extensions;
    using Microsoft.Extensions.Logging;
    using ExamGroupedCollectionViewSource = GroupedCollectionViewSource<DateTime, ExamModel>;
    using TaskGroupedCollectionViewSource = GroupedCollectionViewSource<int, Task>;

    public class DashboardViewModel : BaseViewModel {
        private static readonly Dictionary<NextDayType, string> NextDayReasons = new Dictionary<NextDayType, string> {
            [NextDayType.Tomorrow] = "Tomorrow",
            [NextDayType.AfterWeekend] = "After the weekend",
            [NextDayType.DayAfterHoliday] = "Next day back",
            [NextDayType.NextTerm] = "Next term",
            [NextDayType.NextAcademicYear] = "Next academic year",
            [NextDayType.NextDay] = "Next school day",
            [NextDayType.None] = String.Empty
        };
        private static readonly Dictionary<TaskState, System.Drawing.Color> TaskStateColors = new Dictionary<TaskState, System.Drawing.Color> {
            [TaskState.Complete] = UserColors.Okay,
            [TaskState.IncompleteAndDueSoon] = UserColors.Warning,
            [TaskState.Overdue] = UserColors.Attention,
            [TaskState.Todo] = UserColors.Todo
        };

        private DashboardSettings _dashboardSettings;
        private readonly ITimerService _timerService;
        private readonly IAgendaScheduler _agendaScheduler;
        private readonly IStorageProvider _storageProvider;
        private readonly IFeatureService _featureService;
        private readonly ITaskRepository _taskRepository;
        private readonly IExamRepository _examRepository;
        private readonly ISyncService _syncService;
        private readonly IUserStore _userStore;
        private readonly IBugReporter _bugReporter;

        private bool _canUseExtendedColors;
        private bool _canCustomizeDashboard;

        #region Tabs

        private DialData _todayDial;
        private DialData _tasksDial;
        private DialData _examsDial;

        public DialData TodayDial => _todayDial;
        public DialData TasksDial => _tasksDial;
        public DialData ExamsDial => _examsDial;

        /// <summary>
        ///     Whether or not the view should show the exams tab.
        /// </summary>
        /// <remarks>
        ///     Calculated value, not the same as <see cref="UserSettings.DashboardExamsHidden"/>.
        /// </remarks>
        public bool ShowExamsTab { get; private set; } = !(UserSettings.Default.DashboardExamsHidden ?? false);

        #endregion

        #region Today

        private ObservableCollection<AgendaEntry> _entries;
        private CollectionViewSource<AgendaEntry> _entriesView;

        /// <summary>
        ///		The current date.
        /// </summary>
        public DateTime CurrentDate { get; private set; }

        /// <summary>
        ///     The holiday for the current date (if any).
        /// </summary>
        public Holiday Holiday { get; private set; }

        public bool HasCurrentEntry { get; private set; }

        public string TimelineText { get; private set; }
        public System.Drawing.Color TimelineColor { get; private set; }
        public bool ShowTimeline { get; private set; }
        /// <summary>
        ///     Specifies how far the timeline should be offset
        ///     in terms of "current" and "past" entries.
        /// </summary>
        public TimelineOffset TimelineOffset { get; private set; }

        public string CurrentDayTitle { get; private set; }

        public ObservableCollection<AgendaEntry> Entries {
            get { return _entries ??= new ObservableCollection<AgendaEntry>(); }
            private set {
                _entries = value;

                if (value != null) {
                    HasEntries = 0 < Entries.Count;
                }

                if (_entriesView != null) {
                    _entriesView.Source = value;
                }
            }
        }

        public CollectionViewSource<AgendaEntry> EntriesView =>_entriesView ??= new CollectionViewSource<AgendaEntry>(
            Entries,
            // Start time then end time, so if we have two events which start at the
            // same time, the one with the shortest length is displayed first
            new SortDescription<AgendaEntry>(ae => ae.StartTime),
            new SortDescription<AgendaEntry>(ae => ae.EndTime)
        );

        public bool HasEntries { get; private set; }

        /// <summary>
        ///     True if the today lonely should be visible. We use
        ///     this so we can hide the today lonely before we know
        ///     if we have any entities or not, creating a smoother
        ///     transition.
        /// </summary>
        public bool ShowTodayLonely { get; private set; }
        public string TodayLonely { get; private set; }
        public string TodayLonelySubTitle { get; private set; }
        public Glyph? TodayLonelyGlyph { get; private set; }

        // Used to track when we need to update the dashboard
        private IEnumerable<AgendaEntry> _nextDayEntries;
        private IEnumerable<Guid> _nextDayTasks;

        private DialData _nextDayClassesDial;
        private DialData _nextDayTasksDial;
        private DialData _nextDayExamsDial;

        public NextDay NextDay { get; private set; }
        public MultiLengthText NextDayTitle { get; private set; }
        public DialData NextDayClassesDial => _nextDayClassesDial;
        public DialData NextDayTasksDial => _nextDayTasksDial;
        public DialData NextDayExamsDial => _nextDayExamsDial;

        #endregion

        #region Tasks

        private ObservableCollection<Task> _tasks;
        private CollectionViewSource<Task> _tasksView;
        private TaskGroupedCollectionViewSource _tasksGroupedViewSource;

        public ObservableCollection<Task> Tasks {
            get { return _tasks ?? (_tasks = new ObservableCollection<Task>()); }
            private set {
                _tasks = value;

                if (_tasksView != null) {
                    _tasksView.Source = value;
                }
                if (_tasksGroupedViewSource != null) {
                    _tasksGroupedViewSource.Source = value;
                }
            }
        }

        public CollectionViewSource<Task> TasksView {
            get {
                if (_tasksView == null) {
                    _tasksView = new CollectionViewSource<Task>(
                        Tasks,
                        new SortDescription<Task>(t => t.DueDate)
                    );
                    _tasksView.ViewChanged += (s, e) => InvalidateTaskGroupings();
                }
                return _tasksView;
            }
        }

        public TaskGroupedCollectionViewSource TasksGroupedViewSource {
            get {
                if (_tasksGroupedViewSource == null) {
                    _tasksGroupedViewSource = new TaskGroupedCollectionViewSource(
                        Tasks,
                        TaskGrouping.For
                    ) {
                        EqualityComparer = new EntityEqualityComparer()
                    };
                }

                return _tasksGroupedViewSource;
            }
        }


        public List<TaskGrouping> TaskGroupings { get; } = new List<TaskGrouping>();
        public TaskGrouping GetTaskGrouping(int position) {
            if (TasksView.Count <= position) return null;

            var groupings = TaskGroupings;

            if (groupings.Count <= position) {
                int i = groupings.Count;

                for (; i <= position; i++) {
                    groupings.Insert(i, TaskGrouping.For(TasksView.View[i]));
                }
            }

            return groupings[position];
        }
        private void InvalidateTaskGroupings() => TaskGroupings?.Clear();

        public string TasksLonely { get; private set; }

        #endregion

        #region Exams

        private ObservableCollection<ExamModel> _exams;
        private CollectionViewSource<ExamModel> _examsView;
        private ExamGroupedCollectionViewSource _examsGroupedViewSource;

        public ObservableCollection<ExamModel> Exams {
            get { return _exams ?? (_exams = new ObservableCollection<ExamModel>()); }
            private set {
                _exams = value;

                if (_examsView != null) {
                    _examsView.Source = value;
                }
                if (_examsGroupedViewSource != null) {
                    _examsGroupedViewSource.Source = value;
                }
            }
        }

        public CollectionViewSource<ExamModel> ExamsView {
            get {
                if (_examsView == null) {
                    _examsView = new CollectionViewSource<ExamModel>(
                        Exams,
                        new SortDescription<ExamModel>(e => e.Date)
                    );
                    _examsView.ViewChanged += (s, e) => InvalidateExamGroupings();
                }

                return _examsView;
            }
        }

        // TODO: We can't use ExamGrouping.For here :(
        public ExamGroupedCollectionViewSource ExamsGroupedViewSource {
            get {
                if (_examsGroupedViewSource == null) {
                    _examsGroupedViewSource = new ExamGroupedCollectionViewSource(
                        Exams,
                        ExamModelGrouping.For
                    ) {
                        EqualityComparer = new ModelBaseEqualityComparer<Exam>()
                    };
                }
                return _examsGroupedViewSource;
            }
        }

        public List<ExamGrouping> ExamGroupings { get; } = new List<ExamGrouping>();
        public ExamGrouping GetExamGrouping(int position) {
            if (ExamsView.Count <= position) return null;

            var groupings = ExamGroupings;

            if (groupings.Count <= position) {
                int i = groupings.Count;

                for (; i <= position; i++) {
                    groupings.Insert(i, ExamGrouping.For(ExamsView.View[i].Base));
                }
            }

            return groupings[position];
        }
        private void InvalidateExamGroupings() => ExamGroupings?.Clear();

        public string ExamsLonely { get; private set; }

        #endregion

        #region Commands

        private MvxAsyncCommand<AgendaEntry> _viewEntryCommand;
        private MvxAsyncCommand _newTaskCommand;

        public ICommand ViewEntryCommand => _viewEntryCommand ??= new MvxAsyncCommand<AgendaEntry>(ViewEntry);
        public ICommand NewTaskCommand => _newTaskCommand ??= new MvxAsyncCommand(NewTask);

        private async System.Threading.Tasks.Task ViewEntry(AgendaEntry entry) {
            if (entry.Type == AgendaEntryType.Class) {
                await ViewScheduledClass((ScheduledClass)entry.Base);
            }
            else {
                await ViewEntity((Exam)entry.Base);
            }
        }

        /// <summary>
        ///     Shows the new task screen using either the subject
        ///     from the current or event within the past 30 minutes.
        /// </summary>
        private async System.Threading.Tasks.Task NewTask() {
            var now = DateTimeEx.Now;
            var halfAnHourAgo = now.Subtract(TimeSpan.FromMinutes(30));
            var lastSubject = _entries?.Where(e =>
                    e.StartTime <= now &&
                    e.IsCurrent || (e.IsPast && halfAnHourAgo <= e.EndTime)
                )
                .OrderByDescending(e => e.StartTime)
                .FirstOrDefault()?.Base.Subject;

            await ShowViewModel<TaskInputViewModel>(new TaskInputViewModel.NavObject {
                SubjectGuid = (lastSubject?.Guid).GetValueOrDefault()
            });
        }

        #endregion

        public DashboardViewModel(
            ITimerService timerService,
            IAgendaScheduler agendaScheduler,
            IStorageProvider storageProvider,
            IFeatureService featureService,
            ITaskRepository taskRepository,
            IExamRepository examRepository,
            ISyncService syncService,
            IUserStore userStore,
            IBugReporter bugReporter
        ) {
            _timerService = timerService;
            _agendaScheduler = agendaScheduler;
            _storageProvider = storageProvider;
            _featureService = featureService;
            _taskRepository = taskRepository;
            _examRepository = examRepository;
            _syncService = syncService;
            _userStore = userStore;
            _bugReporter = bugReporter;

            timerService.RequireMessageEvery(Reactive.TimeUnit.Minute);
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            await Invalidate();

            return true;
        }

        public async AsyncTask Invalidate() {
            CurrentDate = DateTimeEx.Today;
            CurrentDayTitle = $"{R.RelativeTime.Today.ToTitleCase()} - {CurrentDate.ToContextualDateString(ContextualDateOptions.OmitYearWhenCurrent)}";

            _canUseExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);
            _canCustomizeDashboard = await _featureService.CanUseFeatureAsync(Feature.CustomizeDashboard);

            var user = _userStore.GetCurrentUser();
            _dashboardSettings = new DashboardSettings(user.Settings, _canCustomizeDashboard);

            await AsyncTask.WhenAll(
                RetrieveEntries(false),
                RetrieveNextDay(false),
                RetrieveTasks(false),
                RetrieveExams(false)
            );

            // We raise the these properties chaned at the same time so the dials
            // all animate in parrallel
            InvokeOnMainThread(() => {
                RaisePropertyChanged(nameof(TodayDial));
                RaisePropertyChanged(nameof(TasksDial));
                RaisePropertyChanged(nameof(ExamsDial));
                RaisePropertyChanged(nameof(NextDayClassesDial));
                RaisePropertyChanged(nameof(NextDayTasksDial));
                RaisePropertyChanged(nameof(NextDayExamsDial));
            });

            BeginUIUpdate();
        }

        private volatile bool _retrievingExams = false;
        private volatile bool _retrievingTasks = false;
        private volatile bool _retrievingEntries = false;
        private volatile bool _retrievingNextDay = false;

        #region Today

        private async AsyncTask RetrieveEntries(bool raiseDialChanged = true) {
            var today = await _agendaScheduler.GetDayForDateAsync(CurrentDate, AgendaSchedulerOptions.SetTaskCounts);
            var entries = today.Entries.OrderBy(e => e.StartTime);

            _todayDial = new DialData(
                entries.Select(e => new DialDataset((int)e.Duration.TotalMinutes, UserColors.GetColor(e.Color, _canUseExtendedColors).Color))
            );

            string todayLonely = null;
            string todayLonelySubTitle = null;
            Glyph? todayLonelyGlyph = null;

            if (!entries.Any()) {
                if (today.IsHoliday) {
                    todayLonely = Catalog.GetString("You're on holiday ({0})").WithArgs(today.Holiday.Name);
                    todayLonelySubTitle = R.NoClassesToday;
                    todayLonelyGlyph = Glyph.Holiday;
                }
                else {
                    todayLonely = R.NoClassesToday;
                    todayLonelyGlyph = Glyph.Today;
                }
            }

            await InvokeOnMainThreadAsync(() => {
                ShowTodayLonely = todayLonely != null;
                TodayLonely = todayLonely;
                TodayLonelySubTitle = todayLonelySubTitle;
                TodayLonelyGlyph = todayLonelyGlyph;
                Holiday = today.Holiday;
                Entries = entries.ToObservableCollection();

                if (raiseDialChanged) {
                    RaisePropertyChanged(nameof(TodayDial));
                }
            });

#if DEBUG
            BeginMockTime();
#endif

            BeginUIUpdate();

        }
        private async AsyncTask TryRetrieveEntries(bool raiseDialChanged = true) {
            if (!_retrievingEntries) {
                try {
                    _retrievingEntries = true;
                    await RetrieveEntries(raiseDialChanged);
                }
                finally {
                    _retrievingEntries = false;
                }
            }
        }

        private async AsyncTask RetrieveNextDay(bool raiseDialChanged = true) {
            IEnumerable<AgendaEntry> nextDayEntries = null;
            IEnumerable<Guid> nextDayTaskGuids = null;
            MultiLengthText nextDayTitle = null;

            var nextDay = await _agendaScheduler.GetNextScheduledDayAsync(DateTimeEx.Now);

            if (nextDay != null) {
                var nextDayReason = NextDayReasons[nextDay.Type] + (nextDay.Type != NextDayType.None ? " - " : String.Empty);

                nextDayTitle = new MultiLengthText(
                    nextDayReason + nextDay.Date.ToContextualDateString(ContextualDateOptions.OmitYearWhenCurrent),
                    () => nextDayReason + nextDay.Date.ToContextualDateString(ContextualDateOptions.OmitYearWhenCurrent, ContextualDateFormat.Abbreviated)
                );

                var nextDayAgenda = await _agendaScheduler.GetDayForDateAsync(nextDay.Date);
                nextDayEntries = nextDayAgenda.Entries.OrderBy(e => e.StartTime);

                _nextDayClassesDial = new DialData(nextDayEntries.Where(e => e.IsClass)
                    .Select(e => new DialDataset((int)e.Duration.TotalMinutes, UserColors.GetColor(e.Base.Subject.Color, _canUseExtendedColors).Color))
                );

                var nextDayTasks = (await _taskRepository.GetByDueDateAsync(nextDay.Date));
                _nextDayTasksDial = new DialData(
                    from task in nextDayTasks
                    group task by task.GetState() into grouping
                    let count = grouping.Count()
                    select new DialDataset(count, count, grouping.Key == TaskState.Complete ? UserColors.Okay : UserColors.Warning)
                );
                nextDayTaskGuids = nextDayTasks.Select(t => t.Guid);

                _nextDayExamsDial = new DialData(nextDayEntries.Where(e => e.IsExam)
                    .OrderBy(e => e.StartTime)
                    .Select(e => new DialDataset((int)e.Duration.TotalMinutes, UserColors.GetColor(e.Color, _canUseExtendedColors).Color))
                );
            }
            else {
                _nextDayClassesDial = null;
                _nextDayTasksDial = null;
                _nextDayExamsDial = null;
            }

            _nextDayEntries = nextDayEntries;
            _nextDayTasks = nextDayTaskGuids;

            InvokeOnMainThread(() => {
                NextDay = nextDay;
                NextDayTitle = nextDayTitle;

                if (raiseDialChanged) {
                    RaisePropertyChanged(nameof(NextDayClassesDial));
                    RaisePropertyChanged(nameof(NextDayTasksDial));
                    RaisePropertyChanged(nameof(NextDayExamsDial));
                }
            });
        }
        private async AsyncTask TryRetrieveNextDay(bool raiseDialChanged = true) {
            if (!_retrievingNextDay) {
                try {
                    _retrievingNextDay = true;
                    await RetrieveNextDay(raiseDialChanged);
                }
                finally {
                    _retrievingNextDay = false;
                }
            }
        }

        #endregion

        #region Mock Time
#if DEBUG

        private static System.Threading.CancellationTokenSource _mockTimeCancellationToken;

        /// <summary>
        ///     Mocks a whole day's worth of class states in a matter of minutes
        /// </summary>
        private void BeginMockTime() {
            return;

#pragma warning disable CS0162 // Unreachable code detected
            _mockTimeCancellationToken?.Cancel();

            var entries = Entries;

            if (!entries.Any()) {
                return;
            }

            _mockTimeCancellationToken = new System.Threading.CancellationTokenSource();
            var cancellationToken = _mockTimeCancellationToken.Token;

            var startTime = entries[0].StartTime.AddMinutes(-5);
            var endTime = Entries.Last().EndTime.AddMinutes(5);
            var diff = (int)(endTime - startTime).TotalMinutes + 1;

            new AsyncTask(async (state) => {
                int i = -1;

                while (true) {
                    i = (i + 1) % diff;

                    try {
                        await AsyncTask.Delay(200, cancellationToken);
                    }
                    catch (TaskCanceledException) { }

                    if (cancellationToken.IsCancellationRequested) {
                        break;
                    }

                    MockDateTimeProvider.SetNow(startTime.AddMinutes(i));

                    BeginUIUpdate();
                }
            }, cancellationToken).Start();
#pragma warning restore CS0162 // Unreachable code detected
        }

#endif
        #endregion

        #region Tasks

        private async AsyncTask RetrieveTasks(bool raiseDialChanged = true) {
            var tasks = (await _taskRepository.GetIncompleteByDueDateAsync(
                null, CurrentDate.AddDays((int)_dashboardSettings.TasksTimescale)
            )).ToObservableCollection();

            _tasksDial = new DialData(
                from task in tasks
                let g = task.GetState()
                group g by g into grouping
                let count = grouping.Count()
                select new DialDataset(count, count, TaskStateColors[grouping.Key])
            );

            string tasksLonely = null;
            if (!tasks.Any()) {
                // TODO: Localize
                tasksLonely = $"No tasks due in the next {_dashboardSettings.TasksTimescale.GetTitle().ToLower()}";
            }

            InvokeOnMainThread(() => {
                Tasks = tasks;
                TasksLonely = tasksLonely;

                if (raiseDialChanged) {
                    RaisePropertyChanged(nameof(TasksDial));
                }
            });
        }
        private async AsyncTask TryRetrieveTasks(bool raiseDialChanged = true) {
            if (!_retrievingTasks) {
                try {
                    _retrievingTasks = true;
                    await RetrieveTasks(raiseDialChanged);
                }
                finally {
                    _retrievingTasks = false;
                }
            }
        }

        #endregion

        #region Exams

        private async AsyncTask RetrieveExams(bool raiseDialChanged = true) {
            bool showExamsTab;
            ObservableCollection<ExamModel> examModels = null;
            string lonelyText = null;

            // Don't do unnecesary work if we're not showing exams
            if (!_dashboardSettings.ExamsHidden) {
                // Set this on the UI thread before we do any work, although it in most cases
                // this will be a no-op since ShowExamsTab defaults to true as it's the user default
                if (!_dashboardSettings.ExamsHideWhenNone) {
                    await InvokeOnMainThreadAsync(() => ShowExamsTab = showExamsTab = true);
                }

                // This will be affected if mock time is enabled
                var exams = (
                    await _examRepository.GetForDateRangeAsync(
                        CurrentDate, CurrentDate.AddDays((int)_dashboardSettings.ExamsTimescale)
                    ))
                    .Where(e => e.Date.AddMinutes(e.Duration) >= DateTimeEx.Now)
                    .ToList();

                if (exams.Any()) {
                    showExamsTab = true;

                    examModels = exams.Select(e =>
                        new ExamModel(e, UserColors.GetColor(e.Subject.Color, _canUseExtendedColors).Color)
                    ).ToObservableCollection();

                    _examsDial = new DialData(
                        from exam in exams
                        let g = ExamGrouping.For(exam)
                        orderby g.Kind
                        group g by g.Color.ToArgb() into grouping
                        let count = grouping.Count()
                        select new DialDataset(count, count, System.Drawing.Color.FromArgb(grouping.Key))
                    );

                    // TODO: We don't need to wait for this to complete, as long as we can cancel execution, post to main, catch errors etc
                    await AsyncTask.WhenAll(examModels.Select(e =>
                        _taskRepository.GetForExamAsync(e.Base).ContinueWith(task => {
                            e.PopulateTasks(task.Result);
                        })
                    ));
                }
                else {
                    showExamsTab = !_dashboardSettings.ExamsHideWhenNone;

                    if (showExamsTab) {
                        _examsDial = new DialData();
                    }

                    // TODO: Localize
                    lonelyText = $"No exams in the next {_dashboardSettings.ExamsTimescale.GetTitle().ToLower()}";
                }
            }
            else {
                showExamsTab = false;
            }

            InvokeOnMainThread(() => {
                ShowExamsTab = showExamsTab;
                Exams = examModels;
                ExamsLonely = lonelyText;

                if (raiseDialChanged) {
                    RaisePropertyChanged(nameof(ExamsDial));
                }
            });
        }
        private async AsyncTask TryRetrieveExams(bool raiseDialChanged = true) {
            if (!_retrievingExams) {
                try {
                    _retrievingExams = true;
                    await RetrieveExams(raiseDialChanged);
                }
                finally {
                    _retrievingExams = false;
                }
            }
        }

        #endregion

        #region Timeline / Minutes Remaining

        // Bit hacky
        private bool _ignoreTimerCallbacks = false;

        public void StartUIUpdateTimer() {
            BeginUIUpdate();

            _ignoreTimerCallbacks = false;
        }

        public void StopUIUpdateTimer() {
            _ignoreTimerCallbacks = true;
        }

        private async void BeginUIUpdate() {
            try {
                bool invalidate = !CurrentDate.Date.IsToday();

                if (invalidate) {
                    await Invalidate();
                    // Invalidate will call this method again, so no point running the below
                    return;
                }

                string timelineText = String.Empty;
                System.Drawing.Color? timelineColor = null;
                bool hasCurrentEntry = false;
                bool hasFutureEntries = false;
                // Past meaning entries in the list which occur before
                // a current entry.
                int pastEntryCount = 0;
                int futureEntryCount = 0;
                float currentEntryPosition = 0;

                var currentEntries = new List<AgendaEntry>();
                bool foundCurrent = false;

                // We're using the sorted collection here
                foreach (var entry in EntriesView.View) {
                    // Checks in order of likeliness...
                    if (entry.IsPast) {
                        // Handles scenario with conflicting long and short events.
                        //
                        // EXAMPLE:
                        //     (A) 09:00 - 12:00 CLASS
                        //     (B) 10:00 - 11:00 EXAM
                        //
                        // This check ensures that the timeline will be displayed
                        // over event A after event B has finished, even though
                        // it appears after event A in the list.
                        //
                        // TODO: Change sort order dependent on state of event
                        // (eg. B would appear before A when B has concluded)
                        if (!foundCurrent) {
                            pastEntryCount++;
                        }
                    }
                    else if (entry.IsCurrent) {
                        currentEntries.Add(entry);
                        foundCurrent = true;
                    }
                    else if (entry.IsFuture) {
                        futureEntryCount++;
                    }
                }

                // If there are multiple current entries, display the timeline over the exam
                // or the entry with the most recent start time
                var currentEntry = currentEntries
                    .OrderByDescending(x => x.IsExam)
                    .ThenByDescending(x => x.StartTime)
                    .FirstOrDefault();

                var hasExtendedColors = await _featureService.CanUseFeatureAsync(Feature.ExtendedColors);

                if (currentEntry != null) {
                    // TODO: Localize
                    timelineText = $"{currentEntry.EndTime.Subtract(DateTimeEx.Now).ToShortString(Utilities.TimeUnit.Minutes, TimeUnits.All, false)}";
                    timelineColor = UserColors.GetColor(currentEntry.Color, hasExtendedColors).Color;
                    hasCurrentEntry = true;

                    currentEntryPosition += currentEntries.IndexOf(currentEntry);
                    currentEntryPosition += (1f - (currentEntry.MinutesRemaining / (float)currentEntry.Duration.TotalMinutes));
                }
                else {
                    var nextEntry = EntriesView.View.FirstOrDefault(x => x.IsFuture);

                    if (nextEntry != null) {
                        timelineText = $"In {nextEntry.StartTime.Subtract(DateTimeEx.Now).ToShortString(Utilities.TimeUnit.Minutes, TimeUnits.All, false)}";
                        timelineColor = UserColors.GetColor(nextEntry.Color, hasExtendedColors).Color;
                        hasFutureEntries = true;
                    }
                }

                InvokeOnMainThread(() => {
                    Entries.Apply(e => e.SetTimeSensitiveProperties());
                    TimelineText = timelineText;
                    TimelineColor = timelineColor ?? System.Drawing.Color.Transparent;
                    HasCurrentEntry = hasCurrentEntry;
                    ShowTimeline = hasCurrentEntry || hasFutureEntries;
                    TimelineOffset = new TimelineOffset(pastEntryCount, currentEntries.Count, currentEntryPosition, futureEntryCount);
                });
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<DashboardViewModel>>().LogWarning($"Caught exception during {nameof(BeginUIUpdate)}: " + ex.ToLongString());
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, handled: true, tags: new List<string> { "Dashboard", nameof(BeginUIUpdate) });
            }
        }
        #endregion

        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            On<TimetableSettingsChangedEventMessage>(OnMessage);
            On<EntityEventMessage<AcademicYear>>(OnMessage);
            On<EntityEventMessage<Holiday>>(OnMessage);
            On<EntityEventMessage<Subject>>(OnMessage);
            On<EntityEventMessage<Class>>(OnMessage);
            On<EntityEventMessage<Task>>(OnMessage);
            On<EntityEventMessage<Exam>>(OnMessage);
            On<UserSettingsChangedEventMessage>(OnMessage);

            On<TimerCallbackMessage>(OnMessage);
        }

        public AsyncTask OnMessage(TimetableSettingsChangedEventMessage message) => Invalidate();

        private AsyncTask OnAcademicYearsModified(IEnumerable<AcademicYear> academicYear) => Invalidate();
        
        public AsyncTask OnMessage(EntityEventMessage<AcademicYear> message) => OnAcademicYearsModified(new[] { message.Entity });

        private ChangeResult OnHolidaysModified(IEnumerable<Holiday> holidays) {
            var res = new ChangeResult();

            if (holidays.Any(h => h.Guid == Holiday?.Guid || CurrentDate.IsBetween(h.StartDate, h.EndDate, DateTimeComparison.Day))) {
                res.ShouldUpdateAgenda = true;
            }
            // TODO: This wont handle scenarios where the next day was caused by multiple holidays
            if (NextDay != null && holidays.Any(h =>
                h.Guid == NextDay.PreviousHoliday?.Guid ||
                NextDay.Date.IsBetween(h.StartDate, h.EndDate, DateTimeComparison.Day)
            )) {
                res.ShouldUpdateNextDay = true;
            }

            return res;
        }
        public AsyncTask OnMessage(EntityEventMessage<Holiday> message) => UpdateFromChangeResult(OnHolidaysModified(new[] { message.Entity }));

        private ChangeResult OnSubjectChange(IEnumerable<Subject> subjects) {
            var res = new ChangeResult();

            var guids = subjects.Select(s => s.Guid).ToList();
            if (guids.Any()) {
                if (Entries.Select(e => e.Base.Subject.Guid).Intersect(guids).Any()) {
                    res.ShouldUpdateExams = true;
                }
                if (_nextDayEntries != null && _nextDayEntries.Select(e => e.Base.Subject.Guid).Intersect(guids).Any()) {
                    res.ShouldUpdateNextDay = true;
                }
                if (Tasks.Select(t => t.Subject.Guid).Intersect(guids).Any()) {
                    res.ShouldUpdateTasks = true;
                }
                if (Exams.Select(e => e.Subject.Guid).Intersect(guids).Any()) {
                    res.ShouldUpdateExams = true;
                }
            }

            return res;
        }
        public AsyncTask OnMessage(EntityEventMessage<Subject> message) => UpdateFromChangeResult(OnSubjectChange(new[] { message.Entity }));

        private ChangeResult OnClassesModified(IEnumerable<Class> classes) {
            var res = new ChangeResult();

            var guids = classes.Select(c => c.Guid);

            // Null when bulk
            if (classes.Count() > 0 && classes.All(c => c == null)) {
                res.ShouldUpdateAgenda = true;
                res.ShouldUpdateNextDay = true;
            }
            else {
                if (Entries.Select(e => e.BaseGuid).Intersect(guids).Any() || classes.ExcludeDeleted().Any(c => c.CanScheduleFor(CurrentDate))) {
                    res.ShouldUpdateAgenda = true;
                }
                if (
                    (_nextDayEntries?.Select(e => e.BaseGuid).Intersect(guids).Any() ?? false) ||
                    (NextDay != null && classes.ExcludeDeleted().Any(c => c.CanScheduleFor(NextDay.Date)))
                ) {
                    res.ShouldUpdateNextDay = true;
                }
            }

            return res;
        }
        public AsyncTask OnMessage(EntityEventMessage<Class> message) => UpdateFromChangeResult(OnClassesModified(new[] { message.Entity }));

        private ChangeResult OnTasksModified(IEnumerable<Task> tasks, ItemAction action = ItemAction.Update) {
            var res = new ChangeResult();

            var guids = tasks.Select(t => t.Guid);
            var examGuids = tasks.SelectNonNull(t => t.ExamGuid);
            var tasksTimelineEnd = CurrentDate.AddDays((int)_dashboardSettings.TasksTimescale);

            // The only real way to do this check would be to store the task guids or know the old entity's values,
            // so in the meantime, we'll just check the subject as it's unlikely the user would change the subject
            // TODO: Extend EntityEventMessage and sync service to give us the old values
            if (action == ItemAction.Update) {
                if (Entries.Any(e => tasks.Any(t => e.Base.Subject.Guid == t.SubjectGuid))) {
                    res.ShouldUpdateAgenda = true;
                }
            }
            else if (tasks.Any(t =>
                (t.DueDate.IsSame(CurrentDate, DateTimeComparison.Day) || t.IsOverdue) &&
                Entries.Any(e => e.Base.Subject.Guid == t.SubjectGuid)
            )) {
                res.ShouldUpdateAgenda = true;
            }

            if (
                Tasks.Select(t => t.Guid).Intersect(guids).Any() ||
                tasks.ExcludeDeleted().Any(t => t.IsOverdue || t.DueDate.IsBetween(CurrentDate, tasksTimelineEnd, DateTimeComparison.Day))
            ) {
                res.ShouldUpdateTasks = true;
            }

            if (Exams.Select(e => e.Guid).Intersect(examGuids).Any()) {
                res.ShouldUpdateExams = true;
            }

            if (
                (_nextDayTasks?.Intersect(guids).Any() ?? false) ||
                tasks.ExcludeDeleted().Any(t => t.DueDate.IsBetween(CurrentDate, NextDay?.Date ?? CurrentDate.AddDays(1), DateTimeComparison.Day))
            ) {
                res.ShouldUpdateNextDay = true;
            }

            return res;
        }
        public AsyncTask OnMessage(EntityEventMessage<Task> message) => UpdateFromChangeResult(OnTasksModified(new[] { message.Entity }, message.Action));

        private ChangeResult OnExamsModified(IEnumerable<Exam> exams) {
            var res = new ChangeResult();

            var guids = exams.Select(e => e.Guid);
            var examsTimelineEnd = CurrentDate.AddDays((int)_dashboardSettings.ExamsTimescale);
            var now = DateTimeEx.Now;

            if (Exams.Select(e => e.Guid).Intersect(guids).Any() || exams.ExcludeDeleted().Any(e => e.Date.IsBetween(now, examsTimelineEnd))) {
                res.ShouldUpdateExams = true;
            }

            if (Entries.Select(e => e.BaseGuid).Intersect(guids).Any() || exams.ExcludeDeleted().Any(e => e.Date.IsToday())) {
                res.ShouldUpdateAgenda = true;
            }

            if (
                (_nextDayEntries?.Select(e => e.BaseGuid).Intersect(guids).Any() ?? false) ||
                exams.ExcludeDeleted().Any(e => e.Date.IsBetween(CurrentDate, NextDay?.Date.AddDays(1) ?? CurrentDate.AddDays(1), DateTimeComparison.Day))
            ) {
                res.ShouldUpdateNextDay = true;
            }

            return res;
        }
        public AsyncTask OnMessage(EntityEventMessage<Exam> message) => UpdateFromChangeResult(OnExamsModified(new[] { message.Entity }));

        public void OnMessage(TimerCallbackMessage message) {
            if (_ignoreTimerCallbacks) return;

            if (message.Unit == Reactive.TimeUnit.Minute) {
                BeginUIUpdate();
            }
        }

        public async AsyncTask OnMessage(UserSettingsChangedEventMessage message) {
            var newSettings = message.Settings;
            var tasks = new List<AsyncTask>(2);
            var tasksChanged = false;
            var examsChanged = false;

            if (newSettings.DashboardTasksTimescale != _dashboardSettings.TasksTimescale) {
                tasksChanged = true;
            }

            if (newSettings.DashboardExamsHidden != _dashboardSettings.ExamsHidden ||
                newSettings.DashboardExamsTimescale != _dashboardSettings.ExamsTimescale ||
                newSettings.DashboardExamsHiddenWhenNone != _dashboardSettings.ExamsHideWhenNone
            ) {
                examsChanged = true;
            }

            if (tasksChanged || examsChanged) {
                var canCustomiseDashboard = await _featureService.CanUseFeatureAsync(Feature.CustomizeDashboard);
                _dashboardSettings.FromSettings(newSettings, canCustomiseDashboard);

                if (tasksChanged) {
                    tasks.Add(TryRetrieveTasks(true));
                }
                if (examsChanged) {
                    tasks.Add(TryRetrieveExams(true));
                }
            }

            await AsyncTask.WhenAll(tasks);
        }

        #endregion

        #region Override Event Methods

        protected override async void OnSyncCompleted(SyncCompletedEventArgs ev) {
            try {
                if (ev.AcademicYearsModified) {
                    await OnAcademicYearsModified(null);
                }
                else {
                    var res = new ChangeResult();

                    if (ev.SubjectsModified) {
                        res.Combine(OnSubjectChange(ev.ModifiedSubjects));
                    }
                    if (ev.HolidaysModified) {
                        res.Combine(OnHolidaysModified(ev.ModifiedHolidays));
                    }
                    // TODO: Add the moment, we could be invalidating
                    // all or parts of the viewmodel multiple times
                    if (ev.ClassesModified) {
                        res.Combine(OnClassesModified(ev.ModifiedClasses));
                    }
                    if (ev.ExamsModified) {
                        res.Combine(OnExamsModified(ev.ModifiedExams));
                    }
                    if (ev.TasksModified) {
                        res.Combine(OnTasksModified(ev.ModifiedTasks));
                    }

                    await UpdateFromChangeResult(res);
                }
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILogger<DashboardViewModel>>().LogError($"Caught exception during {nameof(OnSyncCompleted)}: " + ex.ToLongString());
                _bugReporter.Send(ex, handled: true, tags: new List<string> { nameof(DashboardViewModel), nameof(OnSyncCompleted) });
            }
        }

        private async AsyncTask UpdateFromChangeResult(ChangeResult res) {
            if (res.ShouldUpdateAgenda) {
                await TryRetrieveEntries();
            }
            if (res.ShouldUpdateNextDay) {
                await TryRetrieveNextDay();
            }
            if (res.ShouldUpdateTasks) {
                await TryRetrieveTasks();
            }
            if (res.ShouldUpdateExams) {
                await TryRetrieveExams();
            }
        }

        #endregion

        private sealed class ChangeResult {
            public bool ShouldUpdateAgenda { get; set; }
            public bool ShouldUpdateNextDay { get; set; }
            public bool ShouldUpdateTasks { get; set; }
            public bool ShouldUpdateExams { get; set; }

            /// <summary>
            ///     Combines the current change result with the given one.
            /// </summary>
            public ChangeResult Combine(ChangeResult b) {
                ShouldUpdateAgenda |= b.ShouldUpdateAgenda;
                ShouldUpdateNextDay |= b.ShouldUpdateNextDay;
                ShouldUpdateTasks |= b.ShouldUpdateTasks;
                ShouldUpdateExams |= b.ShouldUpdateExams;

                return this;
            }
        }

        private class DashboardSettings {
            public DashboardTasksTimescale TasksTimescale { get; private set; }
            public DashboardExamsTimescale ExamsTimescale { get; private set; }
            public bool ExamsHidden { get; private set; }
            public bool ExamsHideWhenNone { get; private set; }

            public DashboardSettings(UserSettings settings, bool canCustomizeDashboard) {
                FromSettings(settings, canCustomizeDashboard);
            }

            public void FromSettings(UserSettings settings, bool canCustomizeDashboard) {
                var ds = UserSettings.Default;
                var s = canCustomizeDashboard ? settings : ds;
                TasksTimescale = s.DashboardTasksTimescale ?? ds.DashboardTasksTimescale.Value;
                ExamsTimescale = s.DashboardExamsTimescale ?? ds.DashboardExamsTimescale.Value;
                ExamsHidden = s.DashboardExamsHidden ?? ds.DashboardExamsHidden.Value;
                ExamsHideWhenNone = s.DashboardExamsHiddenWhenNone ?? ds.DashboardExamsHiddenWhenNone.Value;
            }
        }
    }

    /// <summary>
    ///     Encapsulates timeline offset, so the subscriber does
    ///     not have to listen to changes on multiple properties.
    /// </summary>
    public struct TimelineOffset {
        /// <summary>
        ///     Number of past entries.
        /// </summary>
        public int PastEntries { get; }

        /// <summary>
        ///     Number of current entries.
        /// </summary>
        public int CurrentEntries { get; }

        /// <summary>
        ///     Number of current entries to offset by (in the case
        ///     where there may be multiple), including a fraction
        ///     offset for the chosen current entry.
        /// </summary>
        public float CurrentEntriesPosition { get; }

        /// <summary>
        ///     Number of entries in the future.
        /// </summary>
        public int FutureEntries { get; }

        public TimelineOffset(int pastEntries, int currentEntriesCount, float currentEntriesPosition, int futureEntries) {
            PastEntries = pastEntries;
            CurrentEntries = currentEntriesCount;
            CurrentEntriesPosition = currentEntriesPosition;
            FutureEntries = futureEntries;
        }
    }
}
