﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Plugin.Messenger;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using MyStudyLife.Threading;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Calendar {
    // Maybe at some point in the future the "CalendarViewModel" is a parent for both the week/month view rather
    // than inheriting
    [Screen(BackStackEntryKey = "Calendar", BackStackPushMode = NavigationBackStackPushMode.ReplaceOnSamePush)]
    public sealed class CalendarWeekViewModel : CalendarViewModel {

		#region Android and iOS Specific

	    public string PageTitleWithoutWeek { get; private set; }
        public string RotationWeekText { get; private set; }

        #endregion

        private ObservableCollection<CalendarWeekModel> _weeks = new ObservableCollection<CalendarWeekModel>(new[] {
            new CalendarWeekModel(), new CalendarWeekModel(), new CalendarWeekModel()
        });
        private CalendarWeekModel _selectedWeek;

        [DoNotNotify]
        public ObservableCollection<CalendarWeekModel> Weeks {
            get { return _weeks; }
            private set { SetProperty(ref _weeks, value); }
        }

        [DoNotNotify]
        public CalendarWeekModel SelectedWeek {
            get { return _selectedWeek; }
            set {
                var oldWeek = _selectedWeek;

                if (SetProperty(ref _selectedWeek, value)) {
                    OnSelectedWeekChanged(oldWeek, value);

                    RaisePropertyChanged(() => this.IsSelectedPeriodCurrent);
                }
            }
        }

        public override bool IsSelectedPeriodCurrent {
            get { return this.SelectedWeek != null &&  this.SelectedWeek.IsCurrentWeek; }
        }

        public override void Start() {
            base.Start();

            var selectedWeekStart = this.Weeks[1].WeekStart = NavDate.GetValueOrDefault(DateTimeEx.Today).StartOfWeek();

            this.Weeks[0].WeekStart = selectedWeekStart.AddWeeks(-1);
            this.Weeks[2].WeekStart = selectedWeekStart.AddWeeks(+1);

            this.SelectedWeek = this.Weeks[1];

            Task.Delay(750).ContinueWith(ignored => {
                this.Weeks[2].PopulateAsync();
                this.Weeks[0].PopulateAsync();
            });
        }

        protected override async Task SwitchView() {
            await this.ShowViewModel<CalendarMonthViewModel>(new NavObject {
                Date = this.IsSelectedPeriodCurrent ? DateTime.Today : this.PeriodStart
            });
        }

        #region OnSelectedWeekChanged(...)

        private IDisposable _selectedWeekSubscription;

        [SuppressPropertyChangedWarnings]
        private void OnSelectedWeekChanged(CalendarWeekModel oldWeek, CalendarWeekModel newWeek) {
            if (_selectedWeekSubscription != null) {
                _selectedWeekSubscription.Dispose();
                _selectedWeekSubscription = null;
            }

            if (newWeek != null) {
                SetPageTitle();

                _selectedWeekSubscription = newWeek.WeakSubscribe(SelectedWeekOnPropertyChanged);

                newWeek.EnsurePopulated();

                // Unfortunately removing weeks from the start of the collection
                // causes Windows 8's flip view to jump back one :(
                //
                // I don't think it's this that causes memory leaks anyway, it's the UI.

                //const int threshold = 5;

                int newIndex = this.Weeks.IndexOf(newWeek);

                if (newIndex == (this.Weeks.Count - 1)) {
                    var week = new CalendarWeekModel {
                        WeekStart = newWeek.WeekStart.AddWeeks(1)
                    };

                    this.Weeks.Add(week);

                    week.PopulateAsync();

                    //if (this.Weeks.Count > threshold) {
                    //    this.Weeks.RemoveAt(0);
                    //}
                }
                else if (newIndex == 0) {
                    var week = new CalendarWeekModel {
                        WeekStart = newWeek.WeekStart.AddWeeks(-1)
                    };

                    this.Weeks.Insert(0, week);

                    week.PopulateAsync();

                    //if (this.Weeks.Count > threshold) {
                    //    this.Weeks.RemoveAt(this.Weeks.Count - 1);
                    //}
	            }
            }
        }

        private void SelectedWeekOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var week = ((CalendarWeekModel)sender);

            if (this.SelectedWeek == week && (e.PropertyName == "WeekStart" || e.PropertyName == "CurrentRotationWeek")) {
                SetPageTitle();
            }
        }

        #endregion

        #region Overrides of CalendarViewModel

        public override DateTime PeriodStart {
            get {
                return this.SelectedWeek.WeekStart;
            }
        }

        protected override void SetPageTitle() {
            DateTime weekStart = this.SelectedWeek != null ? this.SelectedWeek.WeekStart : DateTimeEx.Today.StartOfWeek();
            DateTime weekEnd = weekStart.AddDays(6d);

            string pageTitle;

            if (weekStart.Month == weekEnd.Month) {
                pageTitle = String.Format("{0} - {1}", weekStart.Day, weekEnd.ToString("d MMM, yyyy"));
            }
            else if (weekStart.Year == weekEnd.Year) {
                pageTitle = String.Format("{0} - {1}", weekStart.ToString("d MMM"), weekEnd.ToString("d MMM, yyyy"));
            }
            else {
                pageTitle = String.Format("{0} - {1}", weekStart.ToString("d MMM, yyyy"), weekEnd.ToString("d MMM, yyyy"));
            }

            if (this.SelectedWeek != null && this.SelectedWeek.CurrentRotationWeek.HasValue) {
                RotationWeekText = String.Concat(
                    "Week ",
                    this.User.Settings.IsRotationScheduleLettered
                        ? Humanize.NumberToLetter(this.SelectedWeek.CurrentRotationWeek.Value)
                        : this.SelectedWeek.CurrentRotationWeek.Value.ToString()
                );
		        PageTitleWithoutWeek = pageTitle;

		        pageTitle = String.Format("{0} ({1})", pageTitle, RotationWeekText);
	        }
	        else {
		        RotationWeekText = null;
		        PageTitleWithoutWeek = pageTitle;
	        }

            this.PageTitle = pageTitle;
        }

        protected override void ViewCurrentPeriod() {
            this.ViewPeriod(DateTimeEx.Today);
        }

        protected override void ViewNextPeriod() {
            this.SelectedWeek = this.Weeks[this.Weeks.IndexOf(this.SelectedWeek) + 1];
        }

        protected override void ViewPreviousPeriod() {
            this.SelectedWeek = this.Weeks[this.Weeks.IndexOf(this.SelectedWeek) - 1];
        }

        private void ViewPeriod(DateTime date) {
            var weekStart = date.StartOfWeek();
            var week = this.Weeks.FirstOrDefault(w => w.WeekStart == weekStart);

            if (week != null) {
                this.SelectedWeek = week;
            }
            else {
                // Resetting the monthstart will clear the entries too.
                //
                // We're not just resetting the Weeks collection as that
                // causes havoc with Android's pager
                var selectedWeek = this.SelectedWeek;
                var selectedIndex = this.Weeks.IndexOf(selectedWeek);

                selectedWeek.WeekStart = weekStart;

                for (int i = (selectedIndex - 1); 0 <= i; i--) {
                    this.Weeks[i].WeekStart = selectedWeek.WeekStart.AddWeeks(-(i + 1));
                }

                for (int i = (selectedIndex + 1); i < this.Weeks.Count; i++) {
                    this.Weeks[i].WeekStart = selectedWeek.WeekStart.AddWeeks(i - selectedIndex);
                }

                OnSelectedWeekChanged(null, selectedWeek);
            }
        }

        #endregion

        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<TimetableSettingsChangedEventMessage>(OnMessage);
            this.On<EntityEventMessage<Subject>>(OnMessage);
            this.On<EntityEventMessage<Class>>(OnMessage);
            this.On<EntityEventMessage<Exam>>(OnMessage);
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnUserSettingsChanged(UserSettingsChangedEventMessage message) {
            base.OnUserSettingsChanged(message);

            this.SetPageTitle();

            if (message.FirstDayChanged) {
                this.SelectedWeek.EnsureWeekStartIsWeekStart();
                this.SelectedWeek.SetNeedsPopulating();
                this.SelectedWeek.EnsurePopulated();

                int selectedIndex = this.Weeks.IndexOf(this.SelectedWeek);

                if (selectedIndex == -1) {
                    return;
                }

                if (selectedIndex > 0) {
                    for (int i = (selectedIndex - 1); i >= 0; i--) {
                        var week = this.Weeks[i];

                        week.WeekStart = this.Weeks[i + 1].WeekStart.AddWeeks(-1d);
                        week.SetNeedsPopulating();
                    }
                }

                if (selectedIndex < (this.Weeks.Count - 1)) {
                    for (int i = (selectedIndex + 1); i < this.Weeks.Count; i++) {
                        var week = this.Weeks[i];

                        week.WeekStart = this.Weeks[i - 1].WeekStart.AddWeeks(1d);
                        week.SetNeedsPopulating();
                    }
                }
            }
        }

        private void OnMessage(TimetableSettingsChangedEventMessage message) {
            this.Weeks.Apply(w => w.SetNeedsPopulating());

            if (this.SelectedWeek != null) {
                this.SelectedWeek.EnsurePopulated();
            }
        }

        private void OnMessage(EntityEventMessage<Subject> message) {
            this.Weeks.Apply(w => w.SetNeedsPopulating());

            if (this.SelectedWeek != null) {
                this.SelectedWeek.EnsurePopulated();
            }
        }

        private void OnMessage(EntityEventMessage<Class> message) {
            if (this.SelectedWeek != null) {
                bool shouldPopulate = message.Entity == null;

                if (!shouldPopulate) {
                    shouldPopulate = message.Entity.CanScheduleForRange(this.SelectedWeek.WeekStart, this.SelectedWeek.WeekEnd);

                    if (shouldPopulate && this.SelectedWeek.CurrentRotationWeek.HasValue) {
                        shouldPopulate = message.Entity.OccursInRotationWeek(this.SelectedWeek.CurrentRotationWeek.Value);
                    }
                }

                if (shouldPopulate) {
                    this.SelectedWeek.SetNeedsPopulating();
                    this.SelectedWeek.EnsurePopulated();
                }
            }

            this.Weeks.Where(w => w != this.SelectedWeek).Apply(w => w.SetNeedsPopulating());
        }

        private void OnMessage(EntityEventMessage<Exam> message) {
            if (this.SelectedWeek != null) {

                // If the new exam falls in this week
                if (message.Entity.Date.Date.IsBetween(this.SelectedWeek.WeekStart.Date, this.SelectedWeek.WeekEnd.Date)) {
                    this.SelectedWeek.SetNeedsPopulating();
                    this.SelectedWeek.EnsurePopulated();
                }
            }

            this.Weeks.Where(w => w != this.SelectedWeek).Apply(w => w.SetNeedsPopulating());
        }

        #endregion

        #region Override Event Methods

        /// <summary>
        /// Triggered when a sync of any type completes successfully.
        /// </summary>
        /// <param name="e">Event Args containing details about the synchronization.</param>
        protected override void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (!e.HasAnyUserDataChanged) {
                return;
            }

            foreach (var week in this.Weeks) {
                if (e.UserSettingsModified) {
                    week.EnsureWeekStartIsWeekStart();
                }

                week.SetNeedsPopulating();
            }

            if (this.SelectedWeek != null) {
                this.SelectedWeek.EnsurePopulated();
            }
        }

        #endregion
    }

    #region Models for Binding

    public class CalendarWeekModel : CalendarPeriodModel {
        // ReSharper disable once InconsistentNaming
        private static readonly AsyncLock _lock = new AsyncLock(false);
        protected override AsyncLock Lock => _lock;

        #region Utils

        private static IAgendaScheduler _agendaScheduler;
        public static IAgendaScheduler AgendaScheduler => _agendaScheduler ?? (_agendaScheduler = Mvx.IoCProvider.Resolve<IAgendaScheduler>());

        #endregion

        #region Main

        private DateTime? _weekStart;

        /// <summary>
        /// The date of the first day of the week for the current displayed timetable.
        /// </summary>
        [DoNotNotify]
        public DateTime WeekStart {
            get { return _weekStart.GetValueOrDefault(DateTimeEx.Today.StartOfWeek()); }
            set {
                if (SetProperty(ref _weekStart, value)) {
                    SetNeedsPopulating();
                }

                WeekEnd = _weekStart.Value.EndOfWeek();

                IsCurrentWeek = DateTimeEx.Today.IsBetween(value, WeekEnd);

                CurrentDayIndex = IsCurrentWeek ? (int)(DateTimeEx.Today - value.Date).TotalDays : -1;
            }
        }

        public DateTime WeekEnd { get; private set; }

        /// <summary>
        ///     A boolean value indicating whether the current week is being displayed.
        ///     Used to enable "Today" AppBar button.
        /// </summary>
        public bool IsCurrentWeek { get; private set; }

        public int CurrentDayIndex { get; private set; }

        public short? CurrentRotationWeek { get; private set; }

        public bool HasHolidays { get; private set; }

        /// <summary>
        ///     The time that the scrollview should show first.
        /// </summary>
        public TimeSpan? InitialTime { get; private set; }

        #endregion

        #region Collections

        private List<DayInfo> _days;
        private ObservableCollection<AgendaEntry> _entries;

        public IList<DayInfo> Days {
            get {
                if (_days == null) {
                    var weekStart = this.WeekStart;

                    _days = new List<DayInfo>(new [] {
                        new DayInfo(weekStart),
                        new DayInfo(weekStart.AddDays(1)),
                        new DayInfo(weekStart.AddDays(2)),
                        new DayInfo(weekStart.AddDays(3)),
                        new DayInfo(weekStart.AddDays(4)),
                        new DayInfo(weekStart.AddDays(5)),
                        new DayInfo(weekStart.AddDays(6))
                    });
                }

                return _days;
            }
        }

        [DoNotNotify]
        public ObservableCollection<AgendaEntry> Entries {
            get { return _entries ?? (_entries = new ObservableCollection<AgendaEntry>()); }
            set { SetProperty(ref _entries, value); }
        }

        #endregion

        /// <summary>
        ///     Ensures the <see cref="WeekStart"/> is the
        ///     start of the week according to the user's settings.
        /// </summary>
        public void EnsureWeekStartIsWeekStart() {
            if (this.WeekStart.DayOfWeek != User.Current.Settings.FirstDayOfWeek) {
                // Tries to be as least disruptive as possible...
                var middleOfWeek = this.WeekStart.AddDays(4d);

                this.WeekStart = middleOfWeek.StartOfWeek();

                this.SetNeedsPopulating();
            }
        }

        protected override async Task PopulateAsyncImpl() {
            var user = User.Current;
            var days = (await AgendaScheduler.GetByDayForWeekAsync(this.WeekStart)).ToList();
            var allEntries = days.SelectMany(x => x.Entries).ToObservableCollection();

            short? rotationWeek = null;
            int daysWithRotationWeek = 0;
            bool hasHols = false;

            try {
                for (int d = 0; d < 7; d++) {
                    var day = days[d];

                    this.Days[d].SetValues(day.Date, day.RotationDay, day.Holiday);

                    if (day.RotationWeek.HasValue) {
                        daysWithRotationWeek++;

                        if (!rotationWeek.HasValue) {
                            rotationWeek = day.RotationWeek;
                        }
                    }

                    if (day.IsHoliday) {
                        hasHols = true;
                    }
                }
            }
            catch (ArgumentOutOfRangeException ex) when (ex.ParamName == "index") {
                ex.Data["ActualValue"] = ex.ActualValue;
                ex.Data["days.Count"] = days.Count;
                ex.Data["this.Days"] = this.Days.Count;
            }

            if (daysWithRotationWeek < Scheduling.AgendaScheduler.WeekRotationHolidayThreshold) {
                rotationWeek = null;
            }

           await Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                for (int d = 0; d < 7; d++) {
                    this.Days[d].RaisePropertiesChanged();
                }

                this.CurrentRotationWeek = rotationWeek;
                this.HasHolidays = hasHols;
            });

            // Do a "reset" rebind if more than 5 entries need to be added/removed
            if (_entries == null || _entries.Count == 0 || Math.Abs(_entries.Count - allEntries.Count) > 5) {

                var initialTime = allEntries.Count > 0
                    ? allEntries.Min(x => x.StartTime.TimeOfDay)
                    : user.Settings.DefaultStartTime;

                if (initialTime.TotalMinutes >= 15) {
                    initialTime -= TimeSpan.FromMinutes(15d);
                }

               await Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                    this.Entries = allEntries;
                    this.InitialTime = initialTime;
                });
            }
            else {
                var currEntries = this.Entries;

                var toRemove = currEntries.Where(x => allEntries.All(y => !y.IsForSameBase(x))).ToList();
                var toAdd = allEntries.Where(x => currEntries.All(y => !y.IsForSameBase(x))).ToList();

                // old entries we've already seen.
                var oldEntries = new List<AgendaEntry>();
                var toReplace = allEntries.Select(x => new {
                    NewEntry = x,
                    OldEntry = currEntries.FirstOrDefault(y => {
                        if (!x.IsForSameBase(y) || oldEntries.Contains(y)) {
                            return false;
                        }

                        // check core properties of the entries
                        var entryPropChanged = !String.Equals(x.Title, y.Title) || !String.Equals(x.Location, y.Location) || !String.Equals(x.Color, y.Color);

                        // check start/end time
                        var entryTimeChanged = entryPropChanged || !x.StartTime.Equals(y.StartTime) || !x.EndTime.Equals(y.EndTime);

                        var classTimeChanged = false;
                        var examTimeChanged = false;
                        // check ClassTime
                        if (entryTimeChanged || x.IsClass && y.IsClass) {
                            var clsXTime = (x.Base as ScheduledClass)?.ClassTime;
                            var clsYTime = (y.Base as ScheduledClass)?.ClassTime;

                            if (clsXTime != null && clsYTime != null) {
                                classTimeChanged = !clsXTime.StartTime.Equals(clsYTime.StartTime) || !clsXTime.EndTime.Equals(clsYTime.EndTime) ||
                                                   !clsXTime.Days.Equals(clsYTime.Days) || !clsXTime.RotationDays.Equals(clsYTime.RotationDays) ||
                                                   !clsXTime.RotationWeek.Equals(clsYTime.RotationWeek);
                            }
                            else {
                                classTimeChanged = clsXTime != clsYTime;
                            }
                        }
                        else {
                            examTimeChanged = !x.StartTime.Equals(y.StartTime) || !x.EndTime.Equals(y.EndTime);
                        }
                        if (entryPropChanged || entryTimeChanged || classTimeChanged || examTimeChanged) {
                            oldEntries.Add(y);
                            return true;
                        }
                        return false;
                    })
                }).Where(x => x.OldEntry != null).ToList();

               await Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                    if (toRemove.Any()) {
                        this.Entries.RemoveRange(toRemove);
                    }

                    if (toAdd.Any()) {
                        foreach (var entry in toAdd) {
                            this.Entries.Add(entry);
                        }
                    }

                    if (toReplace.Any()) {
                        foreach (var entry in toReplace) {
                            this.Entries.Remove(entry.OldEntry);
                            this.Entries.Add(entry.NewEntry);
                        }
                    }
                });
            }
        }

        #region DayInfo

        [DoNotNotify]
        public class DayInfo : BindableBase {
            public DateTime Date { get; set; }

            public bool IsCurrentDay {
                get { return this.Date.IsToday(); }
            }

            public string Heading { get; set; }

            public string AbbrHeading { get; set; }

            public int? RotationDay { get; set; }

            public string RotationDayText { get; set; }

            public bool IsHoliday { get; set; }

            public string HolidayName { get; set; }

            public DayInfo(DateTime date) {
                this.SetValues(date, null, null);
            }

            public void SetValues(DateTime date, int? rotationDay, Holiday holiday) {
                var user = User.Current;

                this.Date = date;

                var heading = date.ToDayAndMonthString();
                string abbrHeading;

                if (rotationDay.HasValue) {
                    string rDay = user.Settings.IsRotationScheduleLettered ? Humanize.NumberToLetter(rotationDay.Value) : rotationDay.Value.ToString();

                    heading += String.Concat(" - Day ", rDay);
                    abbrHeading = String.Concat(DateTimeFormat.GetShortestDayName(date.DayOfWeek), " - ", rDay);
                }
                else {
                    abbrHeading = DateTimeFormat.GetShortDayName(date.DayOfWeek);
                }

                this.Heading = heading;
                this.AbbrHeading = abbrHeading;

                if (holiday != null) {
                    this.IsHoliday = true;
                    this.HolidayName = holiday.Name;
                }
                else {
                    this.IsHoliday = false;
                    this.HolidayName = null;
                }
            }

            public void RaisePropertiesChanged() {
// ReSharper disable ExplicitCallerInfoArgument
                RaisePropertyChanged("Date");
                RaisePropertyChanged("Heading");
                RaisePropertyChanged("AbbrHeading");
                RaisePropertyChanged("IsHoliday");
                RaisePropertyChanged("HolidayName");
// ReSharper restore ExplicitCallerInfoArgument
            }
        }

        #endregion
    }

    #endregion
}
