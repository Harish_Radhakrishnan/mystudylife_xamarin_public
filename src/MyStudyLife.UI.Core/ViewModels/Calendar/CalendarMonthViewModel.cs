﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MvvmCross.WeakSubscription;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.Sync;
using MyStudyLife.Threading;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;
using Task = MyStudyLife.Data.Task;

namespace MyStudyLife.UI.ViewModels.Calendar
{
    [Screen(BackStackEntryKey = "Calendar", BackStackPushMode = NavigationBackStackPushMode.ReplaceOnSamePush)]
    public sealed class CalendarMonthViewModel : CalendarViewModel {
        private readonly ITaskRepository _taskRepository;

        private ObservableCollection<CalendarMonthModel> _months = new ObservableCollection<CalendarMonthModel>();
        private CalendarMonthModel _selectedMonth;
        private CalendarMonthModel.DayAndEntries _selectedDay;

        [DoNotNotify]
        public ObservableCollection<CalendarMonthModel> Months {
            get { return _months; }
            private set { SetProperty(ref _months, value); }
        }

        [DoNotNotify]
        public CalendarMonthModel SelectedMonth {
            get { return _selectedMonth; }
            set {
                var oldMonth = _selectedMonth;

                if (SetProperty(ref _selectedMonth, value)) {
                    OnSelectedMonthChanged(oldMonth, value);

                    RaisePropertyChanged(() => this.IsSelectedPeriodCurrent);
                }
            }
        }

        public override bool IsSelectedPeriodCurrent {
            get {
                return this.SelectedMonth != null && this.SelectedMonth.IsCurrent && this.SelectedDay != null &&
                       this.SelectedDay.Day.Date == DateTime.Today;
            }
        }

        /// <remarks>
        ///     Private setter, use <see cref="ShowDayCommand"/>
        ///     for binding.
        /// </remarks>
        [DoNotNotify]
        public CalendarMonthModel.DayAndEntries SelectedDay {
            get { return _selectedDay; }
            set {
                var oldDay = _selectedDay;

                if (SetProperty(ref _selectedDay, value)) {
                    OnSelectedDayChanged(oldDay, value);

                    RaisePropertyChanged(() => this.SelectedDayLonelyText);
                    RaisePropertyChanged(() => this.IsSelectedPeriodCurrent);
                }
            }
        }

        public ObservableCollection<Task> SelectedDayTasks { get; set; }

        private void SelectedDayOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            RaisePropertyChanged(() => this.SelectedDayLonelyText);
        }

        public string SelectedDayLonelyText {
            get {
                var selectedDay = this.SelectedDay;

                if (selectedDay == null || selectedDay.Entries == null) {
                    return null;
                }

                if (selectedDay.Entries.Count > 0) {
                    return null;
                }

                if (selectedDay.IsHoliday) {
                    return Catalog.GetString("No classes or exams, you're on holiday ({0}).")
                        .WithArgs(selectedDay.HolidayName);
                }

                return Catalog.GetString("There are no classes or exams on this day.");
            }
        }

        #region Commands

        private ICommand _showDayCommand;

        public ICommand ShowDayCommand => _showDayCommand ??= new MvxCommand<CalendarMonthModel.DayAndEntries>(ShowDay);

        private void ShowDay(CalendarMonthModel.DayAndEntries day) {
            this.SelectedDay = day;
        }

        #endregion

        public CalendarMonthViewModel(ITaskRepository taskRepository) {
            this._taskRepository = taskRepository;

            if (this._months.Count != 3) {
                var today = DateTimeEx.Today;

                this._months = new ObservableCollection<CalendarMonthModel>(new[] {
                    new CalendarMonthModel(today.AddMonths(-1)),
                    new CalendarMonthModel(today),
                    new CalendarMonthModel(today.AddMonths(1)),
                });
            }

            this._selectedMonth = this._months[1];
        }

        public override void Start() {
            base.Start();

            var navDate = NavDate.GetValueOrDefault(DateTime.Today);

            this.Months[1].MonthStart = navDate.StartOfMonth();
            this.Months[0].MonthStart = this.Months[1].MonthStart.AddMonths(-1);
            this.Months[2].MonthStart = this.Months[1].MonthStart.AddMonths(1);

            if (this.SelectedMonth != null) {
                OnSelectedMonthChanged(null, this.SelectedMonth, navDate);
            }
            else {
                this.SelectedMonth = this.Months[1];
            }

            RaisePropertyChanged(() => this.SelectedDayLonelyText);

            AsyncTask.Delay(750).ContinueWith(ignored => {
                this.Months[2].PopulateAsync();
                this.Months[0].PopulateAsync();
            });
        }

        #region Overrides of CalendarViewModel

        public override DateTime PeriodStart {
            get { return this.SelectedMonth != null ? this.SelectedMonth.MonthStart : DateTimeEx.Today; }
        }

        protected override void SetPageTitle() {
            this.PageTitle = this.SelectedMonth == null ? "Calendar" : this.PeriodStart.ToString("MMM yyyy");
        }

        protected override async AsyncTask SwitchView() {
            await this.ShowViewModel<CalendarWeekViewModel>(
                this.SelectedMonth != null
                    ? new NavObject {Date = this.SelectedMonth.SelectedDay.Day}
                    : null
            );
        }

        protected override void ViewCurrentPeriod() {
            this.ViewPeriod(DateTime.Today);
        }

        protected override void ViewNextPeriod() {
            this.SelectedMonth = this.Months[this.Months.IndexOf(this.SelectedMonth) + 1];
        }

        protected override void ViewPreviousPeriod() {
            this.SelectedMonth = this.Months[this.Months.IndexOf(this.SelectedMonth) - 1];
        }

        private void ViewPeriod(DateTime date) {
            var monthStart = date.StartOfMonth();
            var month = this.Months.FirstOrDefault(w => w.MonthStart == monthStart);

            if (month != null) {
                if (this.SelectedMonth.MonthStart != month.MonthStart) {
                    this.SelectedMonth = month;
                }
                else {
                    SetSelectedDay(date.Date);
                }
            }
            else {
                // Resetting the monthstart will clear the entries too.
                //
                // We're not just resetting the Months collection as that
                // causes havoc with Android's pager
                var selectedMonth = this.SelectedMonth;
                var selectedIndex = this.Months.IndexOf(selectedMonth);

                selectedMonth.MonthStart = monthStart;

                for (int i = (selectedIndex - 1); 0 <= i; i--) {
                    this.Months[i].MonthStart = selectedMonth.MonthStart.AddMonths(-(i + 1));
                }

                for (int i = (selectedIndex + 1); i < this.Months.Count; i++) {
                    this.Months[i].MonthStart = selectedMonth.MonthStart.AddMonths(i - selectedIndex);
                }

                OnSelectedMonthChanged(null, selectedMonth);
            }
        }

        #endregion

        #region OnSelectedMonthChanged(...)

        private IDisposable _selectedMonthSubscription;

        [SuppressPropertyChangedWarnings]
        private void OnSelectedMonthChanged(CalendarMonthModel oldMonth, CalendarMonthModel newMonth,
            DateTime? selectedDayDate = null) {
            if (_selectedMonthSubscription != null) {
                _selectedMonthSubscription.Dispose();
                _selectedMonthSubscription = null;
            }

            if (newMonth != null) {
                SetPageTitle();

                this.SetSelectedDay(selectedDayDate.GetValueOrDefault(DateTime.Today));

                _selectedMonthSubscription = newMonth.WeakSubscribe(SelectedMonthOnPropertyChanged);

                newMonth.EnsurePopulated();

                int newIndex = this.Months.IndexOf(newMonth);

                if (newIndex == (this.Months.Count - 1)) {
                    var month = new CalendarMonthModel(newMonth.MonthStart.AddMonths(1));

                    this.Months.Add(month);

                    month.PopulateAsync();
                }
                else if (newIndex == 0) {
                    var month = new CalendarMonthModel(newMonth.MonthStart.AddMonths(-1));

                    this.Months.Insert(0, month);

                    month.PopulateAsync();
                }
            }
        }

        private void SelectedMonthOnPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var month = ((CalendarMonthModel) sender);

            if (e.PropertyName == "SelectedDay" && month == this.SelectedMonth) {
                this.SelectedDay = month.SelectedDay;
            }
        }

        #endregion

        #region OnSelectedDayChanged(...)

        private IDisposable _selectedDaySubscription;

        [SuppressPropertyChangedWarnings]
        private void OnSelectedDayChanged(CalendarMonthModel.DayAndEntries oldDay,
            CalendarMonthModel.DayAndEntries newDay) {
            if (_selectedDaySubscription != null) {
                _selectedDaySubscription.Dispose();
                _selectedDaySubscription = null;
            }

            if (newDay != null) {
                _selectedDaySubscription = newDay.WeakSubscribe(SelectedDayOnPropertyChanged);
            }

            this.SetSelectedDayTasks();
        }

        private async void SetSelectedDayTasks() {
            this.SelectedDayTasks = this.SelectedDay == null
                ? null
                : (await _taskRepository.GetByDueDateAsync(this.SelectedDay.Day)).ToObservableCollection();
        }

        #endregion

        #region SetSelectedDay(...)

        private void SetSelectedDay(DateTime targetDate) {
            if (this.SelectedMonth == null) {
                this.SelectedDay = null;
                return;
            }

            // Using .First as more efficient than Single
            this.SelectedDay = this.SelectedMonth.MonthStart.IsSame(targetDate, DateTimeComparison.Month)
                ? this.SelectedMonth.Days.First(x => x.Day.Date == targetDate)
                : this.SelectedMonth.Days.First(x => x.Day.Date == this.SelectedMonth.MonthStart.Date);

            this.SelectedMonth.SelectedDay = this.SelectedDay;
        }

        #endregion

        #region Override Event Methods

        /// <summary>
        /// Triggered when a sync of any type completes successfully.
        /// </summary>
        /// <param name="e">Event Args containing details about the synchronization.</param>
        protected override void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (!e.HasAnyUserDataChanged) {
                return;
            }

            foreach (var month in this.Months) {
                if (e.UserSettingsModified) {
                    month.Invalidate();
                }

                month.SetNeedsPopulating();
            }

            if (this.SelectedMonth != null) {
                this.SelectedMonth.EnsurePopulated();
            }

            if (this.SelectedDayTasks == null) {
                return;
            }

            if (e.TasksModified && e.ModifiedTasks.Any(x =>
                this.SelectedDayTasks.Any(y => y.Guid == x.Guid) || x.DueDate.Date == this.SelectedDay.Day
            )) {
                this.SetSelectedDayTasks();
            }
        }

        #endregion

        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<TimetableSettingsChangedEventMessage>(OnMessage);
            this.On<EntityEventMessage<Subject>>(OnMessage);
            this.On<EntityEventMessage<Class>>(OnMessage);
            this.On<EntityEventMessage<Task>>(OnMessage);
            this.On<EntityEventMessage<Exam>>(OnMessage);
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnUserSettingsChanged(UserSettingsChangedEventMessage message) {
            base.OnUserSettingsChanged(message);

            if (message.FirstDayChanged) {
                foreach (var month in this.Months) {
                    month.Invalidate();
                    month.SetNeedsPopulating();
                }

                if (this.SelectedMonth != null) {
                    this.SelectedMonth.EnsurePopulated();
                }
            }
        }

        private void OnMessage(TimetableSettingsChangedEventMessage message) {
            this.Months.Apply(m => m.SetNeedsPopulating());

            if (this.SelectedMonth != null) {
                this.SelectedMonth.EnsurePopulated();
            }
        }

        private void OnMessage(EntityEventMessage<Subject> message) {
            this.Months.Apply(m => m.SetNeedsPopulating());

            if (this.SelectedMonth != null) {
                this.SelectedMonth.EnsurePopulated();
            }
        }

        private void OnMessage(EntityEventMessage<Class> message) {
            if (this.SelectedMonth != null) {
                bool shouldPopulate = message.Entity == null;

                if (!shouldPopulate) {
                    shouldPopulate = message.Entity.CanScheduleForRange(this.SelectedMonth.CalendarStart,
                        this.SelectedMonth.CalendarEnd);
                }

                if (shouldPopulate) {
                    this.SelectedMonth.SetNeedsPopulating();
                    this.SelectedMonth.EnsurePopulated();
                }
            }

            this.Months.Where(m => m != this.SelectedMonth).Apply(m => m.SetNeedsPopulating());
        }

        private void OnMessage(EntityEventMessage<Task> message) {
            if (this.SelectedDayTasks == null || this.SelectedDay == null) {
                return;
            }

            if (message.Entity == null || message.Entity.DueDate == this.SelectedDay.Day ||
                this.SelectedDayTasks.Any(x => x.Guid == message.Entity.Guid)) {
                this.SetSelectedDayTasks();
            }
        }

        private void OnMessage(EntityEventMessage<Exam> message) {
            if (this.SelectedMonth != null) {
                // If the new exam falls in this week
                if (message.Entity.Date.Date.IsBetween(this.SelectedMonth.CalendarStart,
                    this.SelectedMonth.CalendarEnd)) {
                    this.SelectedMonth.SetNeedsPopulating();
                    this.SelectedMonth.EnsurePopulated();
                }
            }

            this.Months.Where(m => m != this.SelectedMonth).Apply(m => m.SetNeedsPopulating());
        }

        #endregion
    }

    #region Models for Binding

    public class CalendarMonthModel : CalendarPeriodModel {
        private static readonly AsyncLock _lock = new AsyncLock(false);
        private readonly ILogger _log = Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(CalendarMonthModel));

        protected override AsyncLock Lock {
            get { return _lock; }
        }

        private DateTime _monthStart;
        private DayAndEntries _selectedDay;

        [DoNotNotify]
        public DateTime MonthStart {
            get { return _monthStart; }
            set {
                value = value.StartOfMonth();

                if (SetProperty(ref _monthStart, value)) {
                    Invalidate();
                }
            }
        }

        public DateTime CalendarStart { get; private set; }

        public DateTime CalendarEnd { get; private set; }

        public IList<DayAndEntries> Days { get; private set; }

        public DayAndEntries SelectedDay {
            get { return _selectedDay; }
            set {
                _selectedDay = value;

                if (Days != null) {
                    Days.Apply(x => x.IsSelected = (x == value));
                }
            }
        }

        public bool IsCurrent {
            get { return this.MonthStart.IsSame(DateTimeEx.Today, DateTimeComparison.Month); }
        }

        public CalendarMonthModel(DateTime monthStart) {
            this.MonthStart = monthStart;
        }

        public void Invalidate() {
            _log.LogTrace("CalendarMonth: Invalidate Called");

            var days = new List<DayAndEntries>(42);

            if (this.MonthStart != default(DateTime)) {
                DateTime calStart = this.MonthStart.StartOfWeek();
                DateTime endOfMonth = this.MonthStart.EndOfMonth();
                DateTime calEnd = endOfMonth.EndOfWeek();

                int totalWeeks = (int) Math.Ceiling((calEnd - calStart).TotalDays / 7d);

                // This occurs in February occasionally...
                // such as 2015
                if (totalWeeks < 5) {
                    calStart = calStart.AddWeeks(-1d);
                }

                if (totalWeeks < 6) {
                    calEnd = calEnd.AddWeeks(1d);
                }

                DateTime iCal = calStart;
                int iCalWeek = -1;

                while (iCal <= calEnd) {
                    if (iCal.DayOfWeek == DateTimeFormat.FirstDayOfWeek) {
                        iCalWeek++;
                    }

                    days.Add(new DayAndEntries(iCal, iCalWeek) {
                        IsOtherMonth = iCal.Month != MonthStart.Month
                    });

                    iCal = iCal.AddDays(1d);
                }

                this.CalendarStart = calStart;
                this.CalendarEnd = calEnd;
            }

            this.Days = days;

            this.SetNeedsPopulating();

            _log.LogTrace("CalendarMonth: Invalidate Complete");
        }

        protected override async AsyncTask PopulateAsyncImpl() {
            _log.LogTrace("CalendarMonth: Populate Called");

            var scheduler = Mvx.IoCProvider.Resolve<IAgendaScheduler>();

            var days = (await scheduler.GetByDayForDateRangeAsync(this.CalendarStart, this.CalendarEnd)).Select(x =>
                new {
                    Entries = x.Entries.OrderBy(y => y.StartTime).ToList(),
                    x.IsHoliday,
                    x.Holiday
                }).ToList();
            
            await Mvx.IoCProvider.Resolve<IMvxMainThreadAsyncDispatcher>().ExecuteOnMainThreadAsync(() => {
                for (int i = 0; i < this.Days.Count; i++) {
                    var day = days[i];
                    var dayModel = this.Days[i];

                    dayModel.Entries = day.Entries;
                    dayModel.IsHoliday = day.IsHoliday;
                    dayModel.HolidayName = day.Holiday?.Name;
                }

                _log.LogTrace("CalendarMonth: CalendarMonth UI Populated");
            });

            _log.LogTrace("CalendarMonth: CalendarMonth Complete");
        }

        public class DayAndEntries : BindableBase {
            public DateTime Day { get; private set; }

            public int DayOfWeek { get; private set; }

            public int CalendarWeek { get; private set; }

            public bool IsOtherMonth { get; set; }

            public bool IsCurrent {
                get { return this.Day.Date == DateTimeEx.Today; }
            }

            // Used for iOS
            public bool IsSelected { get; internal set; }

            public IList<AgendaEntry> Entries { get; set; }

            public bool IsHoliday { get; set; }

            public string HolidayName { get; set; }

            public DayAndEntries(DateTime date, int calendarWeek) {
                // Shouldn't need to check this as it's an internal API,
                // ...wasting CPU cycles.
                //if (calendarWeek < 0 || calendarWeek > 5) {
                //    throw new ArgumentOutOfRangeException("calendarWeek", "Calendar week must be no less than 0 and no greater than 5");
                //}

                this.Day = date;

                this.DayOfWeek = (int) date.DayOfWeek;

                this.CalendarWeek = calendarWeek;
            }
        }
    }

    #endregion
}