﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.Threading;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ViewModels.Base;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Calendar {
    public abstract class CalendarViewModel : BaseViewModel {
        #region l10n

        private string[] _dayNames = DateTimeFormat.ShortDayNames;
        private string[] _abbrDayNames = DateTimeFormat.ShortestDayNames;
        private readonly string[] _localizedHours = DateTimeFormat.Hours;

        /// <summary>
        ///     Gets a collection of "short" day
        ///     names, localized for the current
        ///     locale. eg. Sat, Mon
        /// </summary>
        public string[] DayNames {
            get { return _dayNames; }
            private set { _dayNames = value; }
        }

        /// <summary>
        ///     Gets a collection of "abbreviated" (2 character) day
        ///     names, localized for the current locale.
        ///     eg. Sa, Mo, Th
        /// </summary>
        public string[] AbbrDayNames {
            get { return _abbrDayNames; }
            private set { _abbrDayNames = value; }
        }

        /// <summary>
        ///     Gets a localized collection of the
        ///     hours in a day.
        /// </summary>
        public string[] LocalizedHours {
            get { return _localizedHours; }
        }

        #endregion

        #region Commands

        private MvxCommand _currentPeriodCommand;
        private MvxCommand _previousPeriodCommand;
        private MvxCommand _nextPeriodCommand;
        private MvxAsyncCommand<AgendaEntry> _viewEntryCommand;

        public ICommand CurrentPeriodCommand {
            get { return Command.Create(ref _currentPeriodCommand, ViewCurrentPeriod); }
        }

        public ICommand PreviousPeriodCommand {
            get { return Command.Create(ref _previousPeriodCommand, ViewPreviousPeriod); }
        }

        public ICommand NextPeriodCommand {
            get { return Command.Create(ref _nextPeriodCommand, ViewNextPeriod); }
        }

        public ICommand ViewEntryCommand {
            get { return _viewEntryCommand ??= new MvxAsyncCommand<AgendaEntry>(ViewEntry); }
        }

        private async System.Threading.Tasks.Task ViewEntry(AgendaEntry entry) {
            if (entry.Type == AgendaEntryType.Class) {
                await this.ViewScheduledClass((ScheduledClass)entry.Base);
            }
            else {
                await this.ViewEntity((Exam)entry.Base);
            }
        }

        #endregion

        #region Page Title

        private string _pageTitle = String.Empty;

        [DoNotNotify]
        public string PageTitle {
            get { return this._pageTitle; }
            protected set { SetProperty(ref _pageTitle, value); }
        }

        public abstract DateTime PeriodStart { get; }

        protected abstract void SetPageTitle();

        #endregion

        #region Period Navigation

        protected abstract void ViewCurrentPeriod();

        protected abstract void ViewNextPeriod();

        protected abstract void ViewPreviousPeriod();

        #endregion

        #region View Switcher

        private ICommand _switchView;
        public ICommand SwitchViewCommand => _switchView ??= new MvxAsyncCommand(SwitchView);

        protected abstract Task SwitchView();

        #endregion

        public abstract bool IsSelectedPeriodCurrent { get; }

        protected abstract override void OnSyncCompleted(Sync.SyncCompletedEventArgs e);
        
        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<UserSettingsChangedEventMessage>(OnUserSettingsChanged);
	    }

        [SuppressPropertyChangedWarnings]
	    protected virtual void OnUserSettingsChanged(UserSettingsChangedEventMessage message) {
	        if (message.FirstDayChanged) {
	            this.DayNames = DateTimeFormat.ShortDayNames;
	            this.AbbrDayNames = DateTimeFormat.ShortestDayNames;
	        }
	    }

        #endregion

        protected DateTime? NavDate { get; set; }

        public void Init(NavObject navObject) {
            if (navObject == null || navObject.Date == default(DateTime)) {
                return;
            }

            NavDate = navObject.Date;
        }

        public class NavObject {
            public DateTime Date { get; set; }
        }

        #region State Handling

        // Quick fix for #292, hopefully

        public void ReloadState(SavedState state) {
            if (state != null && state.PeriodStart != default(DateTime)) {
                this.NavDate = state.PeriodStart;
            }
        }

        public SavedState SaveState() => new SavedState {
            PeriodStart = this.PeriodStart
        };

        public class SavedState {
            public DateTime PeriodStart { get; set; }
        }

        #endregion
    }

    interface ICalendarPeriodModel : INotifyPropertyChanged {
        bool IsPopulated { get; }

        void EnsurePopulated();

        void PopulateAsync();

        void SetNeedsPopulating();
    }

    public abstract class CalendarPeriodModel : BindableBase, ICalendarPeriodModel {
        private bool _isPopulating, _isPopulated;

        protected abstract AsyncLock Lock { get; }

        public bool IsPopulated {
            get { return _isPopulated; }
        }

        public void EnsurePopulated() {
            if (this._isPopulating || this._isPopulated) return;

            PopulateAsync();
        }
        
        public void PopulateAsync() {
            if (this._isPopulating) {
                return;
            }

            System.Threading.Tasks.Task.Factory.StartNew(async () => {
                using (await Lock.LockAsync().ConfigureAwait(false)) {
                    try {
                        this._isPopulating = true;
                        this._isPopulated = false;

                        await PopulateAsyncImpl();

                        this._isPopulated = true;
                    }
                    finally {
                        this._isPopulating = false;
                    }
                }
            }, TaskCreationOptions.LongRunning).ConfigureAwait(false);
        }

        public void SetNeedsPopulating() {
            this._isPopulated = false;
        }

        protected abstract System.Threading.Tasks.Task PopulateAsyncImpl();
    }
}
