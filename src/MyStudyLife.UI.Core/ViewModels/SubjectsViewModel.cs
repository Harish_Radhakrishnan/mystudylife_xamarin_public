﻿using System;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Input;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels {
    [Screen("Manage Subjects")]
    public class SubjectsViewModel : ScheduleDependentEntitiesViewModel<Subject> {
        private readonly ISubjectRepository _subjectRepository;

	    private CollectionViewSource<Subject> _viewSource; 

	    public CollectionViewSource<Subject> ViewSource {
		    get {
			    return _viewSource ??= new CollectionViewSource<Subject>(Items, new SortDescription<Subject>(x => x.Name));
		    }
        }

        protected override void OnItemsChanged() {
            if (_viewSource != null) {
                _viewSource.Source = this.Items;
            }
        }

        public string LonelyText { get; private set; }

        #region Commands

        private MvxAsyncCommand _newCommand;
        private MvxAsyncCommand<Subject> _editCommand;
        private MvxInteraction<string> _editErrorInteraction = new MvxInteraction<string>();

        public ICommand NewCommand => _newCommand ??= new MvxAsyncCommand(New);
        public ICommand EditCommand => _editCommand ??= new MvxAsyncCommand<Subject>(Edit);

        /// <summary>
        ///     Should raise an error when <see cref="EditCommand"/> is invoked
        ///     but editting the subject is not possible.
        /// </summary>
        public IMvxInteraction<string> EditErrorInteraction => _editErrorInteraction;


        private async AsyncTask New() {
            await this.ShowViewModel<SubjectInputViewModel>(new SubjectInputViewModel.NavObject {
                ScheduleGuid = this.SelectedSchedule != null ? this.SelectedSchedule.Guid : Guid.Empty
            });
        }

        private async AsyncTask Edit(Subject s) {
            if (s.CanBeEditedBy(this.User)) {
                await this.ShowViewModel<SubjectInputViewModel>(new SubjectInputViewModel.NavObject {
                    Guid = s.Guid
                });
            }
            else {
                this._editErrorInteraction.Raise(R.SubjectFromSchoolError);
            }
        }

        #endregion

        public SubjectsViewModel(
            IAcademicYearRepository academicYearRepository,
            ISubjectRepository subjectRepository
        ) : base(academicYearRepository) {
            this._subjectRepository = subjectRepository;
        }

        protected override async AsyncTask SetItemsByScheduleAsync(IAcademicSchedule schedule) {
            var items = (await this._subjectRepository.GetByScheduleAsync(schedule)).ToObservableCollection();

            this.InvokeOnMainThread(() => {
                this.Items = items;

                this.SetLonelyText();
            });
        }

        private void SetLonelyText() {
            string filterText = this.SelectedSchedule == null
                                     ? String.Empty
                                     : String.Concat(" for ", this.SelectedScheduleText);

            this.LonelyText = Catalog.GetString("You haven't added any subjects{0} yet.").WithArgs(filterText);
        }

        #region Overrides of BaseViewModel

        /// <summary>
        /// Triggered when a sync of any type completes successfully.
        /// </summary>
        /// <param name="e">Event Args containing details about the synchronization.</param>
        protected async override void OnSyncCompleted(SyncCompletedEventArgs e) {
		    if (e.AcademicYearsModified || e.SubjectsModified) {
			    await Invalidate();
		    }
			
			base.OnSyncCompleted(e);
	    }

	    #endregion
    }
}
