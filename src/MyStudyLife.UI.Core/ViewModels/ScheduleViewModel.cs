﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Messaging;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.UI.ViewModels.Wizards;
using MyStudyLife.Utility;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels
{
    public sealed class ScheduleViewModel : BaseViewModel {
        private readonly IFeatureService _featureService;
        private readonly IClassRepository _classRepository;
        private readonly ISubjectRepository _subjectRepository;
        private readonly IAcademicYearRepository _academicYearRepository;

        public bool ShowLonely { get; private set; }
        public bool ShowNewYearButton { get; private set; }

        public string LonelyText { get; private set; }
        public string LonelySubText { get; private set; }

        #region Academic Years

        private ObservableCollection<AcademicYear> _academicYears;
        private IAcademicSchedule _selectedSchedule;
        private bool _isCurrentSchedule;

        public ObservableCollection<AcademicYear> AcademicYears {
            get { return _academicYears ?? (_academicYears = new ObservableCollection<AcademicYear>()); }
            private set { _academicYears = value; }
        }

        public bool CanModifyYear => (this.SelectedYear?.CanBeEditedBy(this.User) ?? false);

        public AcademicYear SelectedYear {
            get {
                return _selectedSchedule != null
                    ? _selectedSchedule as AcademicYear ?? ((AcademicTerm)_selectedSchedule).Year
                    : null;
            }
            set { this.SelectedSchedule = value; }
        }

        public AcademicTerm SelectedTerm {
            get { return this.SelectedSchedule as AcademicTerm; }
            set {
                if (value == null && this._selectedSchedule is AcademicYear) {
                    return;
                }

                this.SelectedSchedule = value;
            }
        }

        [DoNotNotify]
        public IAcademicSchedule SelectedSchedule {
            get { return _selectedSchedule; }
            set {
                var oldSchedule = _selectedSchedule;

                if (SetProperty(ref _selectedSchedule, value)) {
                    OnSelectedScheduleChanged(oldSchedule, value);
                }
            }
        }

        public string SelectedScheduleText {
            get {
                if (this.SelectedSchedule == null) {
                    return this.AcademicYears.Any() ? R.NoAcademicScheduleText : null;
                }

                return this.SelectedSchedule.ToString();
            }
        }

        [DoNotNotify]
        public bool IsCurrentSchedule {
            get { return _isCurrentSchedule; }
            set {
                if (SetProperty(ref _isCurrentSchedule, value)) {
                    // RaisePropertyChanged(() => this.FilterText);
                }
            }
        }

        public async AsyncTask SetIsCurrentScheduleAsync() {
            var selectedSchedule = this.SelectedSchedule;

            if (selectedSchedule == null) {
                this.IsCurrentSchedule = false;
            }
            else {
                var currSchedule = await this._academicYearRepository.GetCurrentScheduleAsync();

                this.IsCurrentSchedule = currSchedule != null && currSchedule.Guid == selectedSchedule.Guid;
            }
        }

        private MvxCommand<IAcademicSchedule> _selectedScheduleCommand;

        public ICommand SelectedScheduleCommand => Command.Create(ref _selectedScheduleCommand, SetSelectedSchedule);

        private void SetSelectedSchedule(IAcademicSchedule academicSchedule) {
            this.SelectedSchedule = academicSchedule;
        }

        #endregion

        #region Subjects

        public ObservableCollection<Subject> Subjects { get; private set; }
        public ObservableCollection<Subject> SubjectsWithNull { get; private set; }
        public Subject SelectedSubject { get; set; }
        public bool HasSelectedSubject => this.SelectedSubject != null && this.SelectedSubject.Color != "NULL";

        #endregion

        #region Classes

        private ObservableCollection<Class> _classes;
        private CollectionViewSource<Class> _classesViewSource;

        [DoNotNotify]
        public ObservableCollection<Class> Classes {
            get { return _classes ?? (_classes = new ObservableCollection<Class>()); }
            set {
                if (value != null) {
                    value.Apply(x => x.SortTimes());
                }

                if (SetProperty(ref _classes, value) && this._classesViewSource != null) {
                    this._classesViewSource.Source = value;
                }
            }
        }
        public CollectionViewSource<Class> ClassesViewSource {
            get {
                return _classesViewSource ?? (_classesViewSource = new CollectionViewSource<Class>(
                    this.Classes,
                    new SortDescription<Class>(x => x)
                ));
            }
        }

        public bool HasClassesWithoutSchedule { get; private set; }

        public bool ShowClassesLonely { get; private set; }
        public string ClassesLonelyText { get; private set; }
        public string ClassesLonelySubText { get; private set; }

        #endregion

        #region Holiday

        private ObservableCollection<Holiday> _holidays;
        private CollectionViewSource<Holiday> _holidaysViewSource;

        [DoNotNotify]
        public ObservableCollection<Holiday> Holidays {
            get { return _holidays ?? (_holidays = new ObservableCollection<Holiday>()); }
            set {
                if (SetProperty(ref _holidays, value) && this._holidaysViewSource != null) {
                    this._holidaysViewSource.Source = value;
                }
            }
        }
        public CollectionViewSource<Holiday> HolidaysViewSource {
            get {
                return _holidaysViewSource ?? (_holidaysViewSource = new CollectionViewSource<Holiday>(
                    this.Holidays,
                    new SortDescription<Holiday>(x => x.StartDate)
                ));
            }
        }

        public string HolidaysLonelyText { get; private set; }

        public bool CanAddHoliday => this.CanModifyYear;

        #endregion

        #region Commands

        private bool _canAddYears, _canManageSubjects;

        private MvxAsyncCommand _newAcademicYearCommand;
        private MvxAsyncCommand _editAcademicYearCommand;
        private MvxAsyncCommand _showManageSubjectsCommand;

        [DoNotNotify]
        public bool CanAddYears {
            get { return _canAddYears; }
            set {
                if (SetProperty(ref _canAddYears, value)) {
                    _newAcademicYearCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        [DoNotNotify]
        public bool CanManageSubjects {
            get { return _canManageSubjects; }
            set {
                if (SetProperty(ref _canManageSubjects, value)) {
                    _showManageSubjectsCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        public ICommand NewAcademicYearCommand => _newAcademicYearCommand ??= new MvxAsyncCommand(NewAcademicYear, () => CanAddYears);

        public ICommand EditAcademicYearCommand => _editAcademicYearCommand ??= new MvxAsyncCommand(EditAcademicYear, CanEditAcademicYear);

        public ICommand ShowManageSubjectsCommand => _showManageSubjectsCommand ??= new MvxAsyncCommand(ShowManageSubjects, () => CanManageSubjects);

        private async AsyncTask NewAcademicYear() {
            await this.ShowViewModel<AcademicYearInputViewModel>();
        }
        private async AsyncTask EditAcademicYear() {
            await this.ShowViewModel<AcademicYearInputViewModel>(new AcademicYearInputViewModel.NavObject {
                Guid = this.SelectedYear.Guid
            });
        }
        private bool CanEditAcademicYear() {
            return this.CanAddYears && this.SelectedYear != null && this.User != null && this.SelectedYear.CanBeEditedBy(this.User);
        }
        private async AsyncTask ShowManageSubjects() {
            await this.ShowViewModel<SubjectsViewModel>(new SubjectsViewModel.NavObject {
                ScheduleGuid = (this.SelectedSchedule?.Guid).GetValueOrDefault(Guid.Empty)
            });
        }

        #endregion

        #region Class Commands

        private MvxAsyncCommand _newClassCommand;
        private MvxAsyncCommand _showJoinClassesCommand;
        private bool _canAddClasses, _canJoinClasses;

        public ICommand NewClassCommand => _newClassCommand ??= new MvxAsyncCommand(NewClass, () => CanAddClasses);
        public ICommand ShowJoinClassesCommand => _showJoinClassesCommand ??= new MvxAsyncCommand(ShowJoinClasses, () => CanJoinClasses);

        [DoNotNotify]
        public bool CanAddClasses {
            get { return _canAddClasses; }
            set {
                if (SetProperty(ref _canAddClasses, value)) {
                    _newClassCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        [DoNotNotify]
        public bool CanJoinClasses {
            get { return _canJoinClasses; }
            set {
                if (SetProperty(ref _canJoinClasses, value)) {
                    _showJoinClassesCommand?.RaiseCanExecuteChanged();
                }
            }
        }

        private async AsyncTask NewClass() {
            bool hasSelectedSubject = this.SelectedSubject != null && this.SelectedSubject.Color != "NULL";

            var navObject = new ClassInputViewModel.NavObject {
                SubjectGuid = hasSelectedSubject ? this.SelectedSubject.Guid : Guid.Empty,
                ScheduleGuid = !hasSelectedSubject && this.SelectedSchedule != null ? this.SelectedSchedule.Guid : Guid.Empty
            };

            await this.ShowViewModel<ClassInputViewModel>(navObject);
        }
        
        private async AsyncTask ShowJoinClasses() => await this.ShowViewModel<JoinClassesViewModel>();

        #endregion

        #region Holiday Commands

        private MvxAsyncCommand _newHolidayCommand;
        private MvxAsyncCommand<Holiday> _editHolidayCommand;

        public ICommand NewHolidayCommand => _newHolidayCommand ??= new MvxAsyncCommand(NewHoliday, () => CanAddHoliday);
        public ICommand EditHolidayCommand => _editHolidayCommand ??= new MvxAsyncCommand<Holiday>(EditHoliday, (h) => CanAddHoliday);

        private async AsyncTask NewHoliday() {
            await this.ShowViewModel<HolidayInputViewModel>(new HolidayInputViewModel.NavObject {
                YearGuid = this.SelectedYear.Guid
            });
        }
        private async AsyncTask EditHoliday(Holiday holiday) {
            if (this.SelectedYear.CanBeEditedBy(this.User)) {
                await this.ShowViewModel<HolidayInputViewModel>(new HolidayInputViewModel.NavObject {
                    Guid = holiday.Guid,
                    YearGuid = holiday.YearGuid
                });
            }
            else {
                DialogService.ShowNotification(String.Empty, R.HolidayFromSchoolError);
            }
        }

        #endregion

        public ScheduleViewModel(
            IFeatureService featureService,
            IClassRepository classRepository,
            ISubjectRepository subjectRepository,
            IAcademicYearRepository academicYearRepository
        ) {
            this._featureService = featureService;
            this._classRepository = classRepository;
            this._subjectRepository = subjectRepository;
            this._academicYearRepository = academicYearRepository;
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            this.CanAddYears = await this._featureService.CanUseFeatureAsync(Feature.WriteAcademicYears);
            this.CanManageSubjects = await this._featureService.CanUseFeatureAsync(Feature.WriteSubjects);
            this.CanAddClasses = await this._featureService.CanUseFeatureAsync(Feature.WriteClasses);
            this.CanJoinClasses = await this._featureService.CanUseFeatureAsync(Feature.JoinClasses);

            await this.Invalidate();

            return true;
        }

        /// <summary>
        ///     Invalidates the current view model causing a refresh of all data.
        /// </summary>
        public async AsyncTask Invalidate() {
            await this.SetAcademicYearsAsync();

            // Ensures selected schedule is the object in the list
            var selectedSchedule = this.SelectedSchedule ?? await this._academicYearRepository.GetClosestScheduleToCurrentAsync();

            if (selectedSchedule == null) {
                // Setting this.SelectedSchedule wouldn't call this as it's
                // the same as the existing value.
                OnSelectedScheduleChanged(null, null);
                return;
            }

            var term = selectedSchedule as AcademicTerm;

            // Only do this onload
            if (!this.IsLoaded && term != null) {
                // If there are no classes for ANY term (not just the current one),
                // then select the year (better to see holidays).
                var anyClassesForYearHaveTerm = await this._classRepository.AnyClassesForYearHaveTerm(term.Year);
                if (!anyClassesForYearHaveTerm) {
                    selectedSchedule = term.Year;
                    term = null;
                }
            }

            if (term != null) {
                foreach (var year in this.AcademicYears) {
                    if (year.Guid == term.YearGuid) {
                        this.SelectedSchedule = year.Terms.FirstOrDefault(x => x.Guid == term.Guid);
                        return;
                    }
                }
            }
            else {
                this.SelectedSchedule = this.AcademicYears.FirstOrDefault(x => x.Guid == selectedSchedule.Guid);
            }
        }

        private async AsyncTask SetAcademicYearsAsync() {
            this.AcademicYears = (
                await this._academicYearRepository.GetAllNonDeletedAsync()
            ).OrderBy(x => x.StartDate).ToObservableCollection();

            this.SetShowLonely();
        }

        private async AsyncTask SetSubjectsByScheduleAsync() {
            this.Subjects = (await this._subjectRepository.GetByScheduleAsync(this.SelectedSchedule)).OrderBy(x => x.Name).ToObservableCollection();
            var subjectsWithNull = this.Subjects.ToList();

            // This is shit
            var nullSubject = new Subject {
                Color = "NULL"
            };
            subjectsWithNull.Insert(0, nullSubject);

            this.SubjectsWithNull = subjectsWithNull.ToObservableCollection();
        }

        private async AsyncTask SetClassesBySubjectAsync() {
            if (_classes != null) {
                _classes.CollectionChanged -= ClassesOnCollectionChanged;
            }

            this.HasClassesWithoutSchedule = await this._classRepository.HasClassesWithoutSchedule();

            Task<IEnumerable<Class>> classes;

            if (this.SelectedSubject == null || this.SelectedSubject.Guid.IsEmpty()) {
                classes = this._classRepository.GetByScheduleAsync(this.SelectedSchedule);
            }
            else {
                classes = this._classRepository.GetBySubjectAsync(this.SelectedSubject);
            }

            this.Classes = (await classes).ToObservableCollection();

            this.Classes.CollectionChanged += ClassesOnCollectionChanged;

            this.SetShowLonely();

            this.SetClassesLonelyText();
        }
        private void ClassesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) => this.SetClassesLonelyText();

        private async AsyncTask SetHolidaysByScheduleAsync() {
            var selectedSchedule = this.SelectedSchedule;

            if (selectedSchedule == null) {
                this.Holidays = new ObservableCollection<Holiday>();
            }
            else {
                var holidays = await this._academicYearRepository.GetHolidaysAsync(this.SelectedYear.Guid);

                if (selectedSchedule is AcademicYear) {
                    this.Holidays = holidays.ToObservableCollection();
                }
                else {
                    var term = (AcademicTerm) selectedSchedule;
                    // exception with null selectedschedule in W8?
                    this.Holidays = holidays.Where(x =>
                        x.StartDate.IsBetween(term.StartDate, term.EndDate) ||
                        x.EndDate.IsBetween(term.StartDate, term.EndDate) ||
                        term.StartDate.IsBetween(x.StartDate, x.EndDate) ||
                        term.EndDate.IsBetween(x.StartDate, x.EndDate)
                    ).ToObservableCollection();
                }
            }

            await this.RaisePropertyChanged(nameof(this.CanAddHoliday));

            this.SetHolidaysLonelyText();
        }

        private void OnSelectedScheduleChanged(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
            // Extra checks to diganose MSL-448 (we think the cause is SelectedSchedule with a null year, but
            // don't have a stack trace above OnSelectedScheduleChanged as it is an async void
            var newTerm = newSchedule as AcademicTerm;
            if (newTerm != null && newTerm.Year == null) {
                throw new InvalidOperationException($"{nameof(AcademicTerm)} with inconsistent state (Year=null) cannot be set as {nameof(SelectedSchedule)}");
            }

            OnSelectedScheduleChangedCore(oldSchedule, newSchedule);
        }

        private async void OnSelectedScheduleChangedCore(IAcademicSchedule oldSchedule, IAcademicSchedule newSchedule) {
            try {
                await RaisePropertyChanged(nameof(SelectedScheduleText));
                await RaisePropertyChanged(nameof(SelectedYear));
                await RaisePropertyChanged(nameof(SelectedTerm));

                await this.SetIsCurrentScheduleAsync();
                await this.SetSubjectsByScheduleAsync();
                await this.SetClassesBySubjectAsync();
                await this.SetHolidaysByScheduleAsync();

                _newHolidayCommand?.RaiseCanExecuteChanged();
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(nameof(ScheduleViewModel)).LogTrace("Caught exception during selected schedule change handling: " + ex.ToLongString());

                throw;
            }
        }

        private void SetShowLonely() {
            var anyYears = this.AcademicYears.Any();

            this.ShowNewYearButton = !anyYears && this.CanAddYears;

            if (this.User.School != null && this.User.School.IsManaged && !anyYears) {
                ShowLonely = true;
                LonelyText = R.MissingSchedule;
                LonelySubText = $"It doesn't look like your school has added your schedule yet. {R.ContactITDepartment}";
            }
            else if (!anyYears && !this.Classes.Any()) {
                ShowLonely = true;
                LonelyText = "Schedule";
                LonelySubText = "This is where your years, terms, classes and holidays will live. Add an academic year to get started.";
            }
            else {
                ShowLonely = false;
                LonelyText = String.Empty;
                LonelySubText = String.Empty;
            }
        }

        private void SetClassesLonelyText() {
            if (!this.Classes.Any()) {
                this.ShowClassesLonely = true;

                if (this.User.School != null && this.User.School.IsManaged) {
                    this.ClassesLonelyText = R.NotEnrolled;
                    this.ClassesLonelySubText = R.ContactITDepartment;
                }
                else {
                    if (this.SelectedSchedule == null) {
                        this.ClassesLonelyText = Catalog.GetString("There are no {0}classes without a year/term assigned.").WithArgs(
                            this.HasSelectedSubject ? String.Concat(this.SelectedSubject.Name, " ") : String.Empty
                        );
                    }
                    else {
                        this.ClassesLonelyText = Catalog.GetString("You haven't added any {0}classes for {1} yet.").WithArgs(
                            this.HasSelectedSubject ? String.Concat(this.SelectedSubject.Name, " ") : String.Empty,
                            String.Concat(this.SelectedTerm != null ? String.Concat(this.SelectedTerm.Name, " ") : String.Empty, this.SelectedYear)
                        );
                    }
                }
            }
            else {
                this.ShowClassesLonely = false;
                this.ClassesLonelyText = String.Empty;
                this.ClassesLonelySubText = String.Empty;
            }
        }

        private void SetHolidaysLonelyText() {
            if (this.SelectedYear == null) {
                this.HolidaysLonelyText = Catalog.GetString(
                    this.AcademicYears.Any()
                        ? "Select an academic year (or term) to add holidays."
                        : "You can't add holidays yet - you must first add an academic year."
                );

                return;
            }

            var term = this.SelectedSchedule as AcademicTerm;

            this.HolidaysLonelyText = Catalog.GetString("There are no holidays for {0}{1} yet.").WithArgs(
                this.SelectedYear,
                term != null ? Catalog.GetString(" which occur in {0}").WithArgs(term.Name) : String.Empty
            );
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == nameof(SelectedYear)) {
                _newHolidayCommand?.RaiseCanExecuteChanged();
                _editAcademicYearCommand?.RaiseCanExecuteChanged();

                RaisePropertyChanged(nameof(CanModifyYear));
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        #region OnSyncCompleted(...)

        protected override async void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.HasAnyUserDataChanged) {
                // Update everything.
                await this.Invalidate();
            }
        }

        #endregion

        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            this.On<EntityEventMessage<AcademicYear>>(OnMessage);
            this.On<EntityEventMessage<Subject>>(OnMessage);
            this.On<EntityEventMessage<Class>>(OnMessage);
            this.On<EntityEventMessage<Holiday>>(OnMessage);
        }

        private async void OnMessage(EntityEventMessage<AcademicYear> message) {
            var selectedSchedule = this.SelectedSchedule;
            var year = message.Entity;

            if (year == null) {
                await this.Invalidate();
            }
            else switch (message.Action) {
                case ItemAction.Delete:
                    if (selectedSchedule != null && (selectedSchedule.Guid == year.Guid || year.Terms.Any(x => x.Guid == selectedSchedule.Guid))) {
                        await Invalidate();
                    }
                    else {
                        this.AcademicYears.RemoveAll(x => x.Guid == message.Entity.Guid);
                    }
                    break;
                case ItemAction.Update: {
                        var itemInCollection = this.AcademicYears.SingleOrDefault(x => x.Guid == year.Guid);

                        if (itemInCollection == null) {
                            return;
                        }

                        var term = selectedSchedule as AcademicTerm;

                        // If the term was selected but has been deleted, select the year
                        // or select the term in the new year as it's copied to the existing one.
                        if (term != null && term.YearGuid == year.Guid) {
                            selectedSchedule = (IAcademicSchedule) year.Terms.SingleOrDefault(x => x.Guid == term.Guid) ?? itemInCollection;
                        }

                        if (!ReferenceEquals(year, itemInCollection)) {
                            year.CopyPropertyValuesTo(itemInCollection);
                        }

                        this.SelectedSchedule = selectedSchedule;
                    }
                    break;
                case ItemAction.Add: {
                        if (!this.AcademicYears.Any(x => x.Guid == year.Guid)) {
                            if (this.AcademicYears.Count == 0) {
                                this.AcademicYears.Add(year);
                            }
                            else {
                                var existingYears = this.AcademicYears.ToList();
                                existingYears.Add(year);

                                this.AcademicYears.Insert(
                                    existingYears.OrderBy(x => x.StartDate).FirstIndexOf(x => x.Guid == year.Guid),
                                    year
                                );
                            }
                        }

                        this.SelectedSchedule = year;
                    }
                    break;
            }
        }

        private async void OnMessage(EntityEventMessage<Subject> message) {
            await this.Invalidate();
        }

        private async void OnMessage(EntityEventMessage<Class> message) {
            if (message.Entity == null) {
                await this.SetClassesBySubjectAsync();
            }
            else switch (message.Action) {
                case ItemAction.Delete:
                    this.Classes.RemoveAll(x => x.Guid == message.Entity.Guid);
                    break;
                case ItemAction.Update: {
                        var itemInCollection = this.Classes.SingleOrDefault(x => x.Guid == message.Entity.Guid);

                        // TODO(eventually) check items are identical rather than the reference
                        if (itemInCollection != null && !ReferenceEquals(message.Entity, itemInCollection)) {
                            message.Entity.CopyPropertyValuesTo(itemInCollection);
                        }
                    }
                    break;
                case ItemAction.Add: {
                        // Guid will be empty when using SubjectsWithNull... should probably change that.
                        if (this.SelectedSubject != null && !this.SelectedSubject.Guid.IsEmpty()) {
                            if (message.Entity.Subject.Guid == this.SelectedSubject.Guid) {
                                this.Classes.Add(message.Entity);
                            }
                        }
                        else {
                            var entitySchedule = message.Entity.ActualSchedule;

                            if (entitySchedule == null || this.SelectedSchedule == null) {
                                this.Classes.Add(message.Entity);
                            }
                            else if (entitySchedule.Guid == this.SelectedSchedule.Guid) {
                                this.Classes.Add(message.Entity);
                            }
                            // If entitySchedule is a term and the selected is a year
                            else if (entitySchedule is AcademicTerm && this.SelectedSchedule is AcademicYear) {
                                if (((AcademicYear)this.SelectedSchedule).Terms.Any(x => x.Guid == entitySchedule.Guid)) {
                                    this.Classes.Add(message.Entity);
                                }
                            }
                        }
                    }
                    break;
            }
        }


        private async void OnMessage(EntityEventMessage<Holiday> message) {
            var holiday = message.Entity;

            // If you add a holiday and it's outside the current term, change the selected schedule to the year.
            if (message.Action == ItemAction.Add && holiday != null && this.SelectedSchedule is AcademicTerm) {
                var term = (AcademicTerm) this.SelectedSchedule;

                if (!(holiday.StartDate.IsBetween(term.StartDate, term.EndDate) ||
                    holiday.EndDate.IsBetween(term.StartDate, term.EndDate) ||
                    term.StartDate.IsBetween(holiday.StartDate, holiday.EndDate) ||
                    term.EndDate.IsBetween(holiday.StartDate, holiday.EndDate))) {

                    this.SelectedSchedule = this.SelectedYear;

                    return;
                }
            }

            await this.SetHolidaysByScheduleAsync();
        }

        #endregion
    }
}
