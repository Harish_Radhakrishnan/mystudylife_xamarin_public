﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Exceptions;
using MvvmCross.ViewModels;
using MyStudyLife.Authorization;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Annotations;
using MyStudyLife.Data.Store;
using MyStudyLife.Data.Validation;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.Services;
using MyStudyLife.Sync;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Tasks;
using MyStudyLife.UI.ViewModels.Base;
using PropertyChanged;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels
{
    // TODO: Add a ClearOnPush? - (clear the existing stack)
    [Screen(BackStackPushMode = NavigationBackStackPushMode.ReplaceOnPush)]
    public sealed class SignInViewModel : BaseViewModel {
        private const string AnalyticsCategory = "Sign In";

        private readonly IUserStore _userStore;
        private readonly IAnalyticsService _analyticsService;
        private readonly INetworkProvider _networkProvider;
        private readonly IAuthorizationService _authService;
        private readonly ITaskProvider _taskProvider;

        private User? _previousUser;
        private OAuthWebFlowResult? _oAuthWebFlowResult;

        #region Constants

        /// <summary>
        /// The error text that is shown when an
        /// internet connection is not present.
        /// </summary>
        public const string NoInternetErrorText =
            "You don’t appear to be connected to the internet at the moment, you’ll need to connect before you can sign in.";

        public const string ServiceUnavailableText =
            "We couldn't reach My Study Life's servers. This may be caused by being connected to a WiFi hotspot that requires T&Cs to be accepted.";

        /// <summary>
        /// Occurs when an unexpected exception occurs
        /// or we can't connect to the API server.
        /// </summary>
        public const string UnexpectedErrorText =
            "An unexpected error occurred, please check your internet connection and try again.";

        #endregion

        public string? ErrorMessage { get; set; }

        public bool SigningIn { get; set; }

        public bool ShowEmailSignIn { get; set; }

        #region My Study Life

        private string? _mslEmail;
        private string? _mslPassword;
        private bool _credentialsSigningIn;

        [Required]
        [Regex(@"(\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)")]
        public string? MslEmail {
            get => _mslEmail;
            set => SetProperty(ref _mslEmail, value);
        }

        [Required]
        public string? MslPassword {
            get => _mslPassword;
            set => SetProperty(ref _mslPassword, value);
        }

        /// <summary>
        ///     True if we an email/password sign in request
        ///     is active.
        /// </summary>
        [DoNotNotify]
        public bool CredentialsSigningIn {
            get => _credentialsSigningIn;
            private set {
                SetProperty(ref _credentialsSigningIn, value);

                SigningIn = value;
            }
        }

        #endregion

        #region Commands

        private MvxCommand? _googleSignInCommand;
        private MvxCommand? _facebookSignInCommand;
        private MvxCommand? _office365SignInCommand;
        private MvxCommand? _toggleEmailSignInCommand;
        private MvxCommand? _mslSignInCommand;
        private MvxCommand? _signUpCommand;
        private MvxCommand? _troubleCommand;
        private MvxCommand? _findOutMoreCommand;
        private MvxCommand? _privacyPolicyCommand;
        private MvxCommand? _termsAndConditionsCommand;

        private readonly Func<bool> _signInCommandCanExecute;

        public ICommand GoogleSignInCommand => Command.Create(ref _googleSignInCommand,
            () => PerformOAuthLogin(AuthenticationProvider.Google), _signInCommandCanExecute);

        public ICommand FacebookSignInCommand => Command.Create(ref _facebookSignInCommand,
            () => PerformOAuthLogin(AuthenticationProvider.Facebook), _signInCommandCanExecute);

        public ICommand Office365SignInCommand => Command.Create(ref _office365SignInCommand,
            () => PerformOAuthLogin(AuthenticationProvider.Azure), _signInCommandCanExecute);

        public ICommand ToggleEmailSignInCommand => Command.Create(ref _toggleEmailSignInCommand, ToggleIsEmailSignIn);

        public ICommand MslSignInCommand =>
            Command.Create(ref _mslSignInCommand, PerformMyStudyLifeSignIn, _signInCommandCanExecute);

        public ICommand SignUpCommand => Command.Create(ref _signUpCommand, SignUp);

        public ICommand ForgottenPasswordCommand => Command.Create(ref _troubleCommand, ForgottenPassword);

        public ICommand FindOutMoreCommand => Command.Create(ref _findOutMoreCommand, FindOutMore);

        public ICommand PrivacyPolicyCommand => Command.Create(ref _privacyPolicyCommand, ShowPrivacyPolicy);

        public ICommand TermsOfServiceCommand => Command.Create(ref _termsAndConditionsCommand, ShowTermsOfService);

        public string FacebookOAuthText => String.Format(R.SignInOAuthText, "Facebook");
        public string GoogleOAuthText => String.Format(R.SignInOAuthText, "Google");
        public string Office365OAuthText => String.Format(R.SignInOAuthText, "Office 365");

        // ANDROID / iOS only
        public bool SignInCommandCanExecute => _signInCommandCanExecute();

        private void ToggleIsEmailSignIn() {
            ShowEmailSignIn = !ShowEmailSignIn;
        }

        private void SignUp() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Sign Up");
            _taskProvider.ShowWebUri(new Uri(MslConfig.Current.SignUpUri));
        }

        private void ForgottenPassword() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Forgotten Password");
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new Uri(MslConfig.Current.PasswordResetUri));
        }

        private void FindOutMore() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Find Out More");
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new Uri(MslConfig.Current.FindOutMoreUri));
        }

        private void ShowPrivacyPolicy() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Privacy Policy");
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new Uri(MslConfig.Current.PrivacyPolicyUri));
        }

        private void ShowTermsOfService() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Terms of Service");
            Mvx.IoCProvider.Resolve<ITaskProvider>().ShowWebUri(new Uri(MslConfig.Current.TermsOfServiceUri));
        }

        #endregion

        #region Versioning

        /// <summary>
        /// Indicates whether a full sync is required upon sign in
        /// due to an application upgrade.
        /// </summary>
        public bool FullSyncRequired { get; set; }

        #endregion

        public SignInViewModel(
            IUserStore userStore,
            IAnalyticsService analyticsService,
            INetworkProvider networkProvider,
            IAuthorizationService authService,
            ITaskProvider taskProvider
        ) {
            _userStore = userStore;
            _analyticsService = analyticsService;
            _networkProvider = networkProvider;
            _authService = authService;
            _taskProvider = taskProvider;

            _signInCommandCanExecute = () => IsOnline && !SigningIn;
        }

        public void Init(NavObject navObject) {
            _oAuthWebFlowResult = navObject.GetOAuthResult();

            if (navObject.Cause == AuthStatusChangeCause.UnauthorizedHttpResponse) {
                ErrorMessage =
                    "For security a session only lasts a short period of time, please sign in to reconfirm your identity.";
            }
        }

        protected override async Task<bool> StartAsync() {
            if (_oAuthWebFlowResult != null) {
                HandleOAuthWebFlowResult(_oAuthWebFlowResult);
            }

            var result = await base.StartAsync();

            InvokeOnMainThread(() => {
                if (!IsOnline) {
                    ErrorMessage = NoInternetErrorText;
                }
            });

            return result;
        }

        #region Sign In

        public async void PerformMyStudyLifeSignIn() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Sign In");

            ErrorMessage = null;

            if (!IsOnline) {
                ErrorMessage = NoInternetErrorText;

                return;
            }

            try {
                // Get the current incase it is destroyed!
                _previousUser = _userStore.GetCurrentUser();

                ValidationContext context = new ValidationContext(this);

                if (!context.IsValid()) {
                    ErrorMessage = "The email / password you have entered is invalid.";

                    return;
                }

                CredentialsSigningIn = true;

                var response = await _authService.AuthorizeCredentials(MslEmail!, MslPassword!);

                if (response.IsAuthorized) {
                    await OnAuthorized(response.User);
                }
                else {
                    ErrorMessage = response.ErrorMessage.Message;
                }
            }
            catch (Exception ex) {
                HandleAuthException(ex, "Credentials");
            }
            finally {
                CredentialsSigningIn = false;
            }
        }

        private void PerformOAuthLogin(AuthenticationProvider provider) {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Sign In With " + provider.GetTitle());

            ErrorMessage = null;

            if (!IsOnline) {
                ErrorMessage = NoInternetErrorText;

                return;
            }

            _taskProvider.ShowWebUri(new Uri(MslConfig.Current.GetOAuthStartUri(provider)));
        }

        public async void HandleOAuthWebFlowResult(OAuthWebFlowResult webFlowResult) {
            if (webFlowResult.Code != null) {
                try {
                    SigningIn = true;

                    // Get the current incase it is destroyed!
                    _previousUser = _userStore.GetCurrentUser();

                    var authResult = await _authService.AuthorizeOAuthResult(webFlowResult.Code);

                    if (authResult.IsAuthorized) {
                        await OnAuthorized(authResult.User);
                    }
                    else {
                        ErrorMessage = authResult.ErrorMessage.Message;
                    }
                }
                catch (Exception ex) {
                    HandleAuthException(ex, "OAuth");
                }
                finally {
                    SigningIn = false;
                }
            }
            else if (webFlowResult.Error != OAuthWebFlowResultError.UserCancel && !(webFlowResult.Error?.StartsWith("oauth_code") ?? false)) {
                ErrorMessage = String.Concat("Error: ", webFlowResult.ErrorMessage);
            }
            else {
                ErrorMessage = null;
            }
        }

        public async void HandleAppleIdResult(string? idToken, string? firstName, string? lastName) {
            if (idToken != null) {
                try {
                    SigningIn = true;

                    // Get the current incase it is destroyed!
                    _previousUser = _userStore.GetCurrentUser();

                    var authResult = await _authService.AuthorizeAppleIdResult(idToken, firstName, lastName);

                    if (authResult.IsAuthorized) {
                        await OnAuthorized(authResult.User);
                    }
                    else {
                        ErrorMessage = authResult.ErrorMessage.Message;
                    }
                }
                catch (Exception ex) {
                    HandleAuthException(ex, "AppleId");
                }
                finally {
                    SigningIn = false;
                }
            }
            else {
                ErrorMessage = null;
            }
        }

        private void HandleAuthException(Exception ex, string signInType) {
            if (ex.IsTransientWebOrNaughtyHotspotException()) {
                ErrorMessage = _networkProvider.IsOnline ? ServiceUnavailableText : NoInternetErrorText;
            }
            else {
                Mvx.IoCProvider.Resolve<ILogger<SignInViewModel>>().LogTrace("Handled error during sign in:\n" + ex.ToLongString());
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(
                    ex,
                    new List<string> {"SignIn"},
                    new Dictionary<string, object> {
                        {"SignIn_Type", signInType}
                    }
                );

                ErrorMessage = UnexpectedErrorText;
            }
        }

        /// <summary>
        ///     Triggers events that should occur after the user
        ///     has signed in successfully.
        /// </summary>
        private async Task OnAuthorized(User user) {
            // TODO: This shouldn't really be handled in the view model
            bool deleteData = false;

            if (_previousUser == null) {
                deleteData = true;

                Debug.WriteLine("SignInViewModel: Previous user was null, deleting all data.");
            }
            else if (_previousUser.Id > 0 && _previousUser.Id != user.Id) {
                deleteData = true;

                Debug.WriteLine(
                    "SignInViewModel: Previous user is not the same as the authorized user, deleting all data.");
            }

            if (deleteData) {
                await Mvx.IoCProvider.Resolve<IDataStore>().TruncateAllAsync();

                var storageProvider = Mvx.IoCProvider.Resolve<IStorageProvider>();

                await storageProvider.RemoveFilesAsync();

                // QUICK HACK to stop User.json being deleted
                await _userStore.PersistUserAsync(user);
            }

            var appState = await Mvx.IoCProvider.Resolve<IAppStateManager>().GetStateAsync();
            appState.SetUser(user);

            // This also sets SigningIn to false
            CredentialsSigningIn = false;

            await NavigationService.ChangePresentation(new AuthorizedPresentationHint());

            var syncService = Mvx.IoCProvider.GetSingleton<ISyncService>();

            if (deleteData || FullSyncRequired) {
                syncService.Reset();
            }

            syncService.TrySync(SyncTriggerKind.Programmatic);

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            Task.Run(async () => {
                // Temporary things until we refactor this
                await Mvx.IoCProvider.GetSingleton<IPushNotificationService>().EnsureNotificationChannelAsync();

                IBootstrapAction action;
                if (Mvx.IoCProvider.TryResolve(out action)) {
                    await action.RunAsync();
                }
            }).AnyContext();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }

        #endregion

        #region Override Events

        protected override void OnNetworkConnectivityChanged(bool isOnline, bool canUseConnection) {
            // We don't care about syncing here so we CAN use the connection.
            ErrorMessage = !isOnline ? NoInternetErrorText : null;
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName.In(nameof(SigningIn), nameof(IsOnline))) {
                // Any of these will be null if the view model has not yet bound (ie. Android/iOS)
                _mslSignInCommand?.RaiseCanExecuteChanged();
                _office365SignInCommand?.RaiseCanExecuteChanged();
                _facebookSignInCommand?.RaiseCanExecuteChanged();
                _googleSignInCommand?.RaiseCanExecuteChanged();

                RaisePropertyChanged(nameof(SignInCommandCanExecute));
            }

            return base.InterceptRaisePropertyChanged(e);
        }

        #endregion

        public class NavObject {
            // -1 as MvvmCross doesn't support nullables
            public AuthStatusChangeCause Cause { get; set; } = (AuthStatusChangeCause) (-1);

            // MvvmCross only supports "simple" property serialization, so no complex types,
            // which is why the below is required
            public string OAuthResultCode { get; set; }
            public string OAuthResultError { get; set; }
            public string OAuthResultErrorMessage { get; set; }
            public string OAuthResultErrorDetail { get; set; }
            public string OAuthResultCorrelationId { get; set; }

            public static NavObject ForOAuthResult(OAuthWebFlowResult result) => new NavObject {
                OAuthResultCode = result.Code,
                OAuthResultError = result.Error,
                OAuthResultErrorMessage = result.ErrorMessage,
                OAuthResultErrorDetail = result.ErrorDetail,
                OAuthResultCorrelationId = result.CorrelationId
            };

            /// <summary>
            ///     Returns the <see cref="OAuthWebFlowResult"/> if it
            ///     is part of the navigation request, null otherwise.
            /// </summary>
            public OAuthWebFlowResult GetOAuthResult() {
                if (OAuthResultCode != null) {
                    return new OAuthWebFlowResult(OAuthResultCode);
                }

                if (OAuthResultError != null) {
                    return new OAuthWebFlowResult(OAuthResultError, OAuthResultErrorMessage,
                        OAuthResultErrorDetail, OAuthResultCorrelationId);
                }

                return null;
            }
        }
    }
}