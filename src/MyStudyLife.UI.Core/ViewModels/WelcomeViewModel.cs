﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MyStudyLife.Data;
using MyStudyLife.Data.Services;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Validation;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;
using Task = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels {
    public class WelcomeViewModel : Base.BaseViewModel {
        private readonly IUserService _userService;
        private readonly IUserStore _userStore;

        public UserSettings Settings { get; private set; }

        public bool IsSaving { get; private set; }

        private MvxAsyncCommand _doneCommand;

        public ICommand DoneCommand => _doneCommand ??= new MvxAsyncCommand(Done);

        public WelcomeViewModel(IUserService userService, IUserStore userStore) {
            this._userService = userService;
            this._userStore = userStore;
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            var user = _userStore.GetCurrentUser();

            var settings = user.Settings;
            settings.SetLocaleSettingsFromCurrentCulture();

            this.Settings = settings;

            return true;
        }

        private async Task Done() {
            var context = new ValidationContext(this.Settings);

            if (!context.IsValid()) {
                var errors = context.Errors.Where(x => x.Key.In(nameof(Settings.DefaultDuration), nameof(Settings.DefaultStartTime))).ToList();

                if (errors.Any()) {
                    DialogService.ShowDismissible(
                        R.ValidationErrorTitle,
                        String.Join(Environment.NewLine, errors.Select(x => "- " + x.Value))
                    );

                    return;
                }
            }

            try {
                this.IsSaving = true;

                await _userService.SetupUser(this.Settings);

                this.IsSaving = false;
            }
            catch (Exception ex) {
                // Is busy MUST be false before we attempt to display a dialog...
                this.IsSaving = false;

                if (ex is HttpApiUnauthorizedException) {
                    await Mvx.IoCProvider.Resolve<IMvxNavigationService>().ChangePresentation(new AuthorizationChangedPresentationHint(Authorization.AuthStatusChangeCause.UnauthorizedHttpResponse));

                    return;
                }

                if (ex.IsTransientWebOrNaughtyHotspotException()) {
                    if (!Mvx.IoCProvider.Resolve<INetworkProvider>().IsOnline) {
                        DialogService.ShowDismissible(R.OfflineError, R.WelcomeWizardOfflineMessage);

                        return;
                    }
                }
                else {
                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, new List<string> { "WelcomeWizard" });
                }

                var apiEx = ex as HttpApiException;

                DialogService.ShowDismissible(R.UnhandledErrorTitle, apiEx != null ? apiEx.ErrorMessage.Message : String.Empty);

                return;
            }

            var result = await DialogService.ShowCustomAsync(
                null,
                Catalog.GetString("Great, what would you like to do next?"),
                "Add Schedule",
                "View Dashboard"
            );

            await Mvx.IoCProvider.Resolve<IMvxNavigationService>().ChangePresentation(
                new WelcomeWizardCompletedPresentationHint {
                    AddClasses = result.ChosePositiveAction
                });
        }
    }
}
