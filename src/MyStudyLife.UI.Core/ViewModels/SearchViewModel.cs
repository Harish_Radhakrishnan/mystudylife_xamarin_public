﻿using System;
using System.Collections.ObjectModel;
using MyStudyLife.Data;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.ViewModels.Base;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels {
    public class SearchViewModel : BaseViewModel, ISearchable {

        public string PageTitle {
            get {
                return String.Format("Results for \"{0}\"", this.QueryText);
            }
        }

        private readonly IClassRepository _classRepository;
        private readonly IExamRepository _examRepository;
        private readonly ITaskRepository _taskRepository;

        private ObservableCollection<BaseUserEntity> _items;
		private ObservableCollection<Class> _classes;
		private ObservableCollection<Task> _tasks;
		private ObservableCollection<Exam> _exams;

        public ObservableCollection<BaseUserEntity> Items {
			get { return _items ?? (_items = new ObservableCollection<BaseUserEntity>()); }
			private set { SetProperty(ref _items, value); }
        }
        public ObservableCollection<Class> Classes {
			get { return _classes ?? (_classes = new ObservableCollection<Class>()); }
			set {
				_classes = value;

				this.ClassesViewSource.Source = value;
			}
		}
        public ObservableCollection<Task> Tasks {
			get { return _tasks ?? (_tasks = new ObservableCollection<Task>()); }
			set {
				_tasks = value;

				this.TasksViewSource.Source = value;
			}
		}
		public ObservableCollection<Exam> Exams {
			get { return _exams ?? (_exams = new ObservableCollection<Exam>()); }
			set {
				_exams = value;

				this.ExamsViewSource.Source = value;
			}
		}

        #region View Source

        private CollectionViewSource<Class> _classesViewSource;
        private CollectionViewSource<Task> _tasksViewSource;
        private CollectionViewSource<Exam> _examsViewSource;
        private CollectionViewSource<BaseUserEntity> _itemsViewSource;

        public CollectionViewSource<Class> ClassesViewSource {
            get {
                return
                    _classesViewSource ??
                    (_classesViewSource =
                     new CollectionViewSource<Class>(Classes, new SortDescription<Class>(x => x.CreatedAt, SortOrder.Descending)));
            }
        }

        public CollectionViewSource<Task> TasksViewSource {
            get {
                return
                    _tasksViewSource ??
                    (_tasksViewSource =
                     new CollectionViewSource<Task>(Tasks, new SortDescription<Task>(x => x.CreatedAt, SortOrder.Descending)));
            }
        }

        public CollectionViewSource<Exam> ExamsViewSource {
            get {
                return
                    _examsViewSource ??
                    (_examsViewSource =
                     new CollectionViewSource<Exam>(Exams, new SortDescription<Exam>(x => x.CreatedAt, SortOrder.Descending)));
            }
        }

        public CollectionViewSource<BaseUserEntity> ItemsViewSource {
            get {
                return
                    _itemsViewSource ??
                    (_itemsViewSource =
                     new CollectionViewSource<BaseUserEntity>(_items, new SortDescription<BaseUserEntity>(x => x.CreatedAt, SortOrder.Descending)));
            }
        }

        #endregion

        #region ctor

        public SearchViewModel(IClassRepository classRepository, IExamRepository examRepository, ITaskRepository taskRepository) {
            this._classRepository = classRepository;
            this._examRepository = examRepository;
            this._taskRepository = taskRepository;
        }

        #endregion

        #region Overrides of MvxViewModel

        public async void Init(string queryText) {
            await this.ProcessQueryText(queryText);
        }

        #endregion

        #region ISearchable

        private string _queryText;

        [DoNotNotify]
        public string QueryText {
            get { return _queryText; }
            set {
                if (SetProperty(ref _queryText, value)) {
                    RaisePropertyChanged(() => this.PageTitle);
                }
            }
        }

        public async AsyncTask ProcessQueryText(string queryText) {
            if (String.IsNullOrWhiteSpace(queryText)) {
                this.QueryText = null; // Ensure null!
                return;
            }

            this.QueryText = queryText;

            var classes = (await _classRepository.GetByQueryTextAsync(queryText)).ToObservableCollection();

            classes.Apply(x => x.SortTimes());

            this.Classes = classes;
            this.Tasks = (await _taskRepository.GetByQueryTextAsync(queryText)).ToObservableCollection();
            this.Exams = (await _examRepository.GetByQueryTextAsync(queryText)).ToObservableCollection();

	        var items = new ObservableCollection<BaseUserEntity>();

			items.AddRange(this._classes);
			items.AddRange(this._tasks);
			items.AddRange(this._exams);

	        this.Items = items;
        }

        public void ClearQuery() { }

        #endregion

        #region Override Event Methods

        protected override async void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.SubjectsModified || e.ClassesModified || e.TasksModified || e.ExamsModified) {
                await this.ProcessQueryText(this.QueryText);
            }
        }

        #endregion
    }
}
