﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using MvvmCross.Commands;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Utility;
using PropertyChanged;
using ExamGroupedCollectionViewSource = MyStudyLife.UI.Data.GroupedCollectionViewSource<System.DateTime, MyStudyLife.Data.Exam>;

namespace MyStudyLife.UI.ViewModels {
    public class ExamsViewModel : BaseEntitiesViewModel<Exam> {
        public ExamsViewModel(
            IFeatureService featureService,
            ISubjectRepository subjectRepository,
            IAcademicYearRepository academicYearRepository,
            IExamRepository examRepository
        ) : base("exams", featureService, subjectRepository, academicYearRepository, examRepository) {
            this.Filter = ExamFilterOption.Current;
            this.Groupings = new List<ExamGrouping>();
        }

        #region Android / Windows 8 / iOS

        public List<ExamGrouping> Groupings { get; set; }

        // On Windows 8 we change the filter instead
        // of switching between views.

        private CollectionViewSource<Exam> _viewSource;
        public override CollectionViewSource<Exam> ViewSource {
            get {
                if (_viewSource == null) {
                    _viewSource = new CollectionViewSource<Exam>(
                        this.Items,
                        GetFilterPredicate(this.Filter),
                        GetSortDescription(this.Filter)
                    ) {
                        EqualityComparer = new EntityEqualityComparer()
                    };

                    _viewSource.ViewChanged += (s, e) => this.InvalidateGroupings();
                }

                return _viewSource;
            }
        }

        private ExamGroupedCollectionViewSource _groupedViewSource;
        public ExamGroupedCollectionViewSource GroupedViewSource {
            get {
                if (_groupedViewSource == null) {
                    _groupedViewSource = new ExamGroupedCollectionViewSource(
                        this.Items,
                        ExamGrouping.For,
                        GetFilterPredicate(this.Filter),
                        GetSortDescription(this.Filter).Order
                    ) {
                        EqualityComparer = new EntityEqualityComparer()
                    };
                }

                return _groupedViewSource;
            }
        }

        public ExamFilterOption Filter { get; set; }

        [DependsOn(nameof(Filter))]
        public bool IsPastFilter => Filter == ExamFilterOption.Past;

        #region Commands

        private MvxCommand<ExamFilterOption> _setFilterCommand;
        private MvxCommand _toggleFilterCommand;

        public ICommand SetFilterCommand => Command.Create(ref _setFilterCommand, SetFilter);

        public ICommand ToggleFilterCommand => Command.Create(ref _toggleFilterCommand, ToggleFilter);

        private void SetFilter(ExamFilterOption filter) {
            if (this.Filter != filter) {
                this.Filter = filter;

                this._viewSource?.SetFilterAndSort(
                    GetFilterPredicate(filter),
                    GetSortDescription(filter)
                );


                this._groupedViewSource?.SetFilterAndSort(
                    GetFilterPredicate(filter),
                    GetSortDescription(filter).Order
                );
            }
        }

        private void ToggleFilter() {
            SetFilter(this.Filter == ExamFilterOption.Past ? ExamFilterOption.Current : ExamFilterOption.Past);
        }

        #endregion

        private static Predicate<Exam> GetFilterPredicate(ExamFilterOption filter) {
            return filter == ExamFilterOption.Past
                ? ExamFilter.FilterPastPredicate
                : ExamFilter.FilterCurrentPredicate;
        }

        private static SortDescription<Exam> GetSortDescription(ExamFilterOption filter) {
            return new SortDescription<Exam>(
                x => x.Date,
                filter == ExamFilterOption.Past
                    ? SortOrder.Descending
                    : SortOrder.Ascending
            );
        }

        #endregion

        #region Windows Phone

        private CollectionViewSource<Exam> _currentViewSource;
        public CollectionViewSource<Exam> CurrentViewSource {
            get {
                return _currentViewSource ??
                       (_currentViewSource = new CollectionViewSource<Exam>(
                        this.Items,
                        GetFilterPredicate(ExamFilterOption.Current),
                        GetSortDescription(ExamFilterOption.Current)
                    )
                );
            }
        }

        private CollectionViewSource<Exam> _pastViewSource;

        public CollectionViewSource<Exam> PastViewSource {
            get {
                return _pastViewSource ??
                       (_pastViewSource = new CollectionViewSource<Exam>(
                        this.Items,
                        GetFilterPredicate(ExamFilterOption.Past),
                        GetSortDescription(ExamFilterOption.Past)
                    )
                );
            }
        }

        #endregion

        #region Override Event Methods

        protected override void OnItemsChanged() {
            if (_viewSource != null) {
                _viewSource.Source = this.Items;
            }

            if (_groupedViewSource != null) {
                _groupedViewSource.Source = this.Items;
            }

            if (_currentViewSource != null) {
                _currentViewSource.Source = this.Items;
            }

            if (_pastViewSource != null) {
                _pastViewSource.Source = this.Items;
            }
        }

        private void InvalidateGroupings() {
            this.Groupings.Clear();
        }

        public ExamGrouping GetGrouping(int position) {
            if (_viewSource.Count <= position) return null;

            var groupings = this.Groupings;

            if (groupings.Count <= position) {
                int i = groupings.Count;

                for (; i <= position; i++) {
                    groupings.Insert(i, ExamGrouping.For(_viewSource.View[i]));
                }
            }

            return groupings[position];
        }

        protected override void OnItemAddedLocal(Exam item) {
            this.SetFilter(ExamFilter.FilterPastPredicate(item) ? ExamFilterOption.Past : ExamFilterOption.Current);
        }

        protected override void OnItemUpdatedLocal(Exam item) {
            // TODO(eventually) compare to the values before updating

            this.InvalidateGroupings();

            if (this.SelectedItems.Count == 0 || (this.SelectedItems.Count == 1 && ((Exam)this.SelectedItems.First()).Guid == item.Guid)) {
                this.SetFilter(ExamFilter.FilterPastPredicate(item) ? ExamFilterOption.Past : ExamFilterOption.Current);

                if (_viewSource == null) {
                    _groupedViewSource?.NotifyItemChanged(item);

                    _currentViewSource?.NotifyItemsSourceChanged();
                    _pastViewSource?.NotifyItemsSourceChanged();
                }
            }
        }

        protected override async void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.SubjectsModified) {
                // Update everything.
                await this.Invalidate();
            }
            else {

                if (e.ExamsModified) {
                    this.MergeModified(e.ModifiedExams);
                }
            }
        }

        #endregion


    }
}
