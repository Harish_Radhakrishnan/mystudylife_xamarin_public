﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MyStudyLife.UI.ViewModels.Base;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.Wizards {
    public abstract class WizardViewModel : BaseViewModel {

        public event EventHandler<StepChangingEventArgs> StepChanging;
        public event EventHandler<StepChangedEventArgs> StepChanged;
         

        private bool _isBusy;

        /// <remarks>
        ///     Overriding the base means that this isn't affected
        ///     by the sync service starting. This should be improved
        ///     further in the future.
        /// </remarks>
        public new bool IsBusy {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }

        public class StepChangingEventArgs : CancelEventArgs {
            public int CurrentStep { get; set; }

            public int NewStep { get; set; }

            public bool IsForward {
                get { return this.NewStep > this.CurrentStep; }
            }

            public bool IsBack {
                get { return !IsForward; }
            }
        }

        public class StepChangedEventArgs : EventArgs {
            public int OldStep { get; set; }

            public int NewStep { get; set; }

            public bool IsForward {
                get { return this.NewStep > this.OldStep; }
            }

            public bool IsBack {
                get { return !IsForward; }
            }
        }

        public virtual int StepCount {
            get { return 4; }
        }

        private int _step;

        [DoNotNotify]
        public int Step {
            get { return _step; }
            set {
                SetProperty(ref _step, value);

                if (_previousStepCommand != null) {
                    _previousStepCommand.RaiseCanExecuteChanged();
                }
                else {
                    RaisePropertyChanged(() => this.CanGoBack);
                }

                if (_nextStepCommand != null) {
                    _nextStepCommand.RaiseCanExecuteChanged();
                }
                else {
                    RaisePropertyChanged(() => this.CanGoNext);
                }
            }
        }

        #region Commands

        private MvxCommand _previousStepCommand;
        private MvxCommand _nextStepCommand;
        private MvxAsyncCommand _doneCommand;

        public MvxCommand PreviousStepCommand {
            get {
                if (_previousStepCommand == null) {
                    _previousStepCommand = new MvxCommand(PreviousStep, GetCanGoBack);

                    _previousStepCommand.CanExecuteChanged += (s, e) => RaisePropertyChanged(() => this.CanGoBack);
                }

                return _previousStepCommand;
            }
        }

        public MvxCommand NextStepCommand {
            get {
                if (_nextStepCommand == null) {
                    _nextStepCommand = new MvxCommand(NextStep, GetCanGoNext);

                    _nextStepCommand.CanExecuteChanged += (s, e) => RaisePropertyChanged(() => this.CanGoNext);
                }

                return _nextStepCommand;
            }
        }

        public ICommand DoneCommand => _doneCommand ??= new MvxAsyncCommand(Done);

        #region Windows Phone Stuff

        public bool CanGoBack {
            get { return GetCanGoBack(); }
        }

        public bool CanGoNext {
            get { return GetCanGoNext(); }
        }

        #endregion

        protected virtual bool GetCanGoBack() {
            return Step > 0 && Step < (StepCount - 1);
        }

        protected virtual bool GetCanGoNext() {
            return true;
        }

        protected virtual void PreviousStep() {
            ChangeStep(-1);
        }

        protected virtual void NextStep() {
            ChangeStep(+1);
        }

        protected virtual Task Done() => this.NavigationService.Close(this);

        protected void ChangeStep(int change, bool noInternalEvents = false) {
            int currentStep = this.Step;

            var eChanging = new StepChangingEventArgs {
                CurrentStep = currentStep,
                NewStep = currentStep + change
            };

            if (!noInternalEvents) {
                OnStepChanging(eChanging);
            }

            if (eChanging.Cancel) {
                return;
            }

            if (StepChanging != null) {
                StepChanging(this, eChanging);

                if (eChanging.Cancel) {
                    return;
                }
            }
            
            Step += change;

            var eChanged = new StepChangedEventArgs {
                OldStep = currentStep,
                NewStep = this.Step
            };

            if (!noInternalEvents) {
                OnStepChanged(eChanged);
            }

            if (StepChanged != null) {
                StepChanged(this, eChanged);
            }
        }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnStepChanging(StepChangingEventArgs e) {
            
        }

        [SuppressPropertyChangedWarnings]
        protected virtual void OnStepChanged(StepChangedEventArgs e) {
            
        }

        #endregion
    }
}
