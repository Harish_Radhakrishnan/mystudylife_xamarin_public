﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.ObjectModel;
using MyStudyLife.Resources;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.Wizards {
    [Screen(BackStackPushMode = NavigationBackStackPushMode.ReplaceOnPush)]
    public class JoinClassesViewModel : WizardViewModel {
        private readonly ISchoolResource _schoolResource;

        public new bool IsBusy { get; private set; }

        public override int StepCount => 5;

        public string NextText {
            get { return this.Step == 3 ? "Confirm" : "Next"; }
        }

        #region Step 1

        public ObservableCollection<AcademicYear> AcademicYears { get; private set; }

        public IAcademicSchedule SelectedSchedule { get; set; }

        public string SelectedScheduleText {
            get { return this.SelectedSchedule != null ? this.SelectedSchedule.ToString() : R.NoAcademicScheduleText; }
        }

        public JoinClassesFindMode FindMode { get; private set; }

        public bool IsFindModeTeacher {
            get { return this.FindMode == JoinClassesFindMode.Teacher; }
        }

        public bool IsFindModeSubject {
            get { return this.FindMode == JoinClassesFindMode.Subject; }
        }

        public string FindModeText {
            get { return IsFindModeTeacher ? "teachers" : "subjects"; }
        }

        public string FindModeAsString {
            get { return IsFindModeTeacher ? "teacher" : "subject"; }
        }

        private MvxCommand<JoinClassesFindMode> _findModeCommand;

        public ICommand FindModeCommand => _findModeCommand ??= new MvxCommand<JoinClassesFindMode>(SetFindMode);

        private void SetFindMode(JoinClassesFindMode mode) {
            this.FindMode = mode;

            this.NextStep();
        }

        private async void InitStep1() {
            using (var acSceduleRepo = new AcademicYearRepository()) {
                this.AcademicYears = (
                    await acSceduleRepo.GetAllNonDeletedAsync(x => x.SchoolId != null)
                ).ToObservableCollection();

                SetCurrentSchedule();
            }
        }

        private void SetCurrentSchedule() {
            if (this.AcademicYears == null) {
                return;
            }

            // We're doing this like this because otherwise
            // it may include the student's schedules, perhaps refactor
            // in the future.
            foreach (var year in this.AcademicYears) {
                if (year.IsCurrent) {
                    if (year.Terms != null && year.Terms.Any()) {
                        foreach (var term in year.Terms) {
                            if (term.IsCurrent) {
                                this.SelectedSchedule = term;

                                return;
                            }
                        }
                    }

                    this.SelectedSchedule = year;

                    return;
                }
            }

            this.SelectedSchedule = null;
        }

        #endregion

        #region Step 2

        private readonly ObservableCollection<object> _selectedTeachers = new ObservableCollection<object>();
        private readonly ObservableCollection<object> _selectedSubjects = new ObservableCollection<object>(); 

        public bool Step2HasErrored { get; private set; }

        public ObservableCollection<Teacher> Teachers { get; private set; }

        public ObservableCollection<Subject> Subjects { get; private set; }

        public ObservableCollection<object> SelectedTeachers {
            get { return _selectedTeachers; }
        }

        public ObservableCollection<object> SelectedSubjects {
            get { return _selectedSubjects; }
        }

        public bool HasTeachersOrSubjects {
            get {
                return this.IsBusy || this.Step2HasErrored || // Hide lonely message when busy or error.
                       (IsFindModeTeacher && this.Teachers != null && this.Teachers.Count > 0) ||
                       (IsFindModeSubject && this.Subjects != null && this.Subjects.Count > 0);
            }
        }

        private async void InitStep2() {
            this.IsBusy = true;
            this.Step2HasErrored = false;

            this.SelectedTeachers.Clear();
            this.SelectedSubjects.Clear();

            try {
                if (IsFindModeTeacher) {
                    this.Teachers = (await _schoolResource.GetTeachersAsync()).OrderBy(x => x.FullName).ToObservableCollection();
                }
                else {
                    this.Subjects = (await _schoolResource.GetSubjectsAsync()).OrderBy(x => x.Name).ToObservableCollection();
                }
            }
            catch (Exception) {
                this.Step2HasErrored = true;
            }
            finally {
                this.IsBusy = false;
            }
        }

        private MvxCommand _step2RetryCommand;

        public ICommand Step2RetryCommand {
            get { return _step2RetryCommand ?? (_step2RetryCommand = new MvxCommand(InitStep2)); }
        }

        #endregion

        #region Step 3

        private readonly ObservableCollection<object> _selectedClasses = new ObservableCollection<object>(); 

        public bool Step3HasErrored { get; private set; }

        public IEnumerable<IGrouping<string, Class>> GroupedClasses { get; private set; }

        public IEnumerable<Class> Classes { get; private set; }

        public ObservableCollection<object> SelectedClasses {
            get { return _selectedClasses; }
        }

        private async void InitStep3() {
            this.IsBusy = true;
            this.Step3HasErrored = false;

            this.Classes = null;

            this.SelectedClasses.Clear();

            try {
                if (IsFindModeTeacher) {
                    var teachers = this.SelectedTeachers.Cast<Teacher>().ToList();

                    // OrderBy is very important so the index in `this.Classes` matches the effective
                    // index of `this.GroupedClasses` for Android selection.
                    this.Classes = (await _schoolResource.GetClassesForTeachersAsync(teachers, this.SelectedSchedule)).OrderBy(x => x.Teacher.FullName);

                    this.GroupedClasses = teachers.OrderBy(x => x.FullName).Select(x =>
                        new Grouping<string, Class>(
                            x.FullName,
                            this.Classes.Where(y => y.Teacher.Id == x.Id)
                        )
                    );
                }
                else {
                    var subjects = this.SelectedSubjects.Cast<Subject>().ToList();

                    this.Classes = (await _schoolResource.GetClassesForSubjectsAsync(subjects, this.SelectedSchedule)).OrderBy(x => x.Subject.Name);

                    this.GroupedClasses = subjects.OrderBy(x => x.Name).Select(x =>
                        new Grouping<string, Class>(
                            x.Name,
                            this.Classes.Where(y => y.SubjectGuid == x.Guid)
                        )
                    );
                }
            }
            catch (Exception) {
                this.Step3HasErrored = true;
            }
            finally {
                this.IsBusy = false;
            }
        }

        private MvxCommand _step3RetryCommand;

        public ICommand Step3RetryCommand {
            get { return _step3RetryCommand ?? (_step3RetryCommand = new MvxCommand(InitStep3)); }
        }

        #endregion

        #region Step 4
        public IEnumerable<IGrouping<string, ClassTime>> GroupedClassTimes { get; private set; }

        public bool Step4HasErrored { get; private set; }

        private MvxCommand _step4RetryCommand;

        public ICommand Step4RetryCommand {
            get { return _step4RetryCommand ?? (_step4RetryCommand = new MvxCommand(JoinClasses)); }
        }

        private void InitStep4() {
            this.GroupedClassTimes = this.SelectedClasses.Cast<Class>().Select(x =>
                new Grouping<string, ClassTime>(
                    x.Title,
                    x.Times
                )
            );
        }

        private async void JoinClasses() {
            this.IsBusy = true;
            this.Step4HasErrored = false;

            try {
                using (var classRepo = new ClassRepository()) {
                    var classes = this.SelectedClasses.Cast<Class>().OrderBy(x => x.Subject.Name).ToList();

                    await classRepo.Join(classes);
                }

                this.ChangeStep(1, true);
            }
            catch (Exception) {
                this.Step4HasErrored = true;
            }
            finally {
                this.IsBusy = false;
            }
        }

        #endregion

        protected override void OnStepChanging(StepChangingEventArgs e) {
            if (e.IsForward) {
                switch (e.CurrentStep) {
                    case 1:
                        e.Cancel = (
                            this.IsFindModeTeacher && this.SelectedTeachers.Count == 0 ||
                            this.IsFindModeSubject && this.SelectedSubjects.Count == 0
                        );
                        break;
                    case 2:
                        e.Cancel = this.SelectedClasses.Count == 0;
                        break;
                    case 3:
                        JoinClasses();
                        e.Cancel = true;
                        break;
                }
            }
        }
        
        [SuppressPropertyChangedWarnings]
        protected override void OnStepChanged(StepChangedEventArgs e) {
            // Steps are 0 index based
            if (e.IsForward) {
                switch (e.NewStep) {
                    case 1:
                        InitStep2();
                        break;
                    case 2:
                        InitStep3();
                        break;
                    case 3:
                        InitStep4();
                        break;
                }
            }
        }

        public JoinClassesViewModel(ISchoolResource schoolResource) {
            this._schoolResource = schoolResource;

            // Strong for Windows Phone....
            // https://github.com/MvvmCross/MvvmCross/commit/f1ebeb5188a16c21d2179f02b2efb840d597f0bc
            this.SelectedTeachers.CollectionChanged += RaiseCanExecuteChanged;
            this.SelectedSubjects.CollectionChanged += RaiseCanExecuteChanged;
            this.SelectedClasses.CollectionChanged += RaiseCanExecuteChanged;
        }

        private void RaiseCanExecuteChanged(object sender, NotifyCollectionChangedEventArgs e) {
            this.NextStepCommand.RaiseCanExecuteChanged();
            this.RaisePropertyChanged(() => this.CanGoNext); // This shouldn't be necessary but it is (WP)
        }

        public override void Start() {
            base.Start();

            this.InitStep1();
        }

        protected override bool GetCanGoNext() {
            switch (this.Step) {
                case 1:
                    return (
                        this.IsFindModeTeacher && this.SelectedTeachers != null && this.SelectedTeachers.Any() ||
                        this.IsFindModeSubject && this.SelectedSubjects != null && this.SelectedSubjects.Any()
                    );
                case 2:
                    return this.SelectedClasses != null && this.SelectedClasses.Any();
                default:
                    return base.GetCanGoNext();
            }
        }

        protected override MvxInpcInterceptionResult InterceptRaisePropertyChanged(PropertyChangedEventArgs e) {
            if (e.PropertyName == "Step") {
                RaisePropertyChanged(() => this.NextText);
            }
            else if (e.PropertyName.In("Teachers", "Subjects", "SelectedTeachers", "SelectedSubjects", "Classes", "SelectedClasses")) {
                this.NextStepCommand.RaiseCanExecuteChanged();
            }

            return base.InterceptRaisePropertyChanged(e);
        }
    }

    public enum JoinClassesFindMode {
        Teacher,
        Subject
    }
}
