﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Services;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Validation;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Net;
using MyStudyLife.UI.Analytics;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Tasks;
using MvvmCross.Commands;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.Wizards {
    [Obsolete("Replaced with WelcomeViewModel")]
    [Screen(BackStackPushMode = NavigationBackStackPushMode.ReplaceOnPush)]
    public class WelcomeWizardViewModel : WizardViewModel {
        private readonly ITaskProvider _taskProvider;
        private readonly IMslConfig _mslConfig;
        private readonly IUserStore _userStore;
        private readonly IUserService _userService;
        private readonly INetworkProvider _networkProvider;
        private readonly IBugReporter _bugReporter;

        private const string AnalyticsCategory = EventCategories.WelcomeWizard;

        #region Input

        private User _user;
        private UserSettings _settings;

        public new User User {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public UserSettings Settings {
            get => _settings;
            set => SetProperty(ref _settings, value);
        }

        #region Validation

        private IDictionary<string, string> _errors = new Dictionary<string, string>();

        public IDictionary<string, string> Errors {
            get => _errors;
            protected set {
				// For some reason android thinks we're casting a string
				// to a textview... when we find the cause this can be
				// removed
		        try {
					SetProperty(ref _errors, value);
		        }
				catch (InvalidCastException) {}
	        }
        }

        #endregion

        #endregion

        private MvxCommand _skipCommand;
        private MvxCommand _addClassesCommand;
        private MvxCommand _showFeedbackPortalCommand;

        public ICommand SkipCommand => Command.Create(ref _skipCommand, Skip);

        public ICommand AddClassesCommand => Command.Create(ref _addClassesCommand, AddClasses);

        public ICommand ShowFeedbackPortalCommand => Command.Create(ref _showFeedbackPortalCommand, ShowFeedbackPortal);

        private void Skip() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Skip");
            SetupUser(true);
        }

        private void AddClasses() {
            this.NavigationService.ChangePresentation(new WelcomeWizardCompletedPresentationHint {
                AddClasses = true
            });
        }

        protected override void PreviousStep() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Back"); // Yes, back not previous
            base.PreviousStep();
        }

        protected override void NextStep() {
            Analytics.TrackEvent(AnalyticsCategory, EventActions.Click, "Next");
            base.NextStep();
        }

        protected override AsyncTask Done() {
            return this.NavigationService.ChangePresentation(new WelcomeWizardCompletedPresentationHint());
        }

        private void ShowFeedbackPortal() {
            this._taskProvider.ShowWebUri(new Uri(this._mslConfig.FeedbackUri));
        }

        public WelcomeWizardViewModel(
            ITaskProvider taskProvider,
            IMslConfig mslConfig,
            IUserStore userStore,
            IUserService userService,
            INetworkProvider networkProvider,
            IBugReporter bugReporter
        ) {
            this._taskProvider = taskProvider;
            this._mslConfig = mslConfig;
            this._userStore = userStore;
            this._networkProvider = networkProvider;
            this._bugReporter = bugReporter;
            this._userService = userService;
        }

        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            // No point cloning as they can't go any further without saving the changes
            this.User = _userStore.GetCurrentUser();

            var settings = this.User.Settings;
            settings.SetLocaleSettingsFromCurrentCulture();

            this.Settings = settings;

            return true;
        }

        protected override void OnStepChanging(StepChangingEventArgs e) {
            this.Errors = null;

            if (e.IsBack) {
                return;
            }

            if (e.CurrentStep == 2) {
                var context = new ValidationContext(this.Settings, "WelcomeWizard");

                e.Cancel = true;

                if (!context.IsValid()) {
                    this.Errors = context.Errors;
                    return;
                }

                SetupUser(false);
            }
        }

        private async void SetupUser(bool skip) {
            try {
                this.IsBusy = true;

                await this._userService.SetupUser(skip ? null : this.Settings);

                this.IsBusy = false;

                if (skip) {
                    await this.NavigationService.ChangePresentation(new WelcomeWizardCompletedPresentationHint {
                        Skipped = true
                    });
                }
                else {
                    // Change manually doesn't trigger events.
                    this.Step++;
                }
            }
            catch (Exception ex) {
                // Is busy MUST be false before we attempt to display a dialog...
                this.IsBusy = false;

                if (ex is HttpApiUnauthorizedException) {
                    await this.NavigationService.ChangePresentation(new AuthorizationChangedPresentationHint(Authorization.AuthStatusChangeCause.UnauthorizedHttpResponse));

                    return;
                }

                if (ex.IsTransientWebException()) {
                    if (!this._networkProvider.IsOnline) {
                        DialogService.ShowDismissible(R.OfflineError, R.WelcomeWizardOfflineMessage);

                        return;
                    }
                }
                else {
                    this._bugReporter.Send(ex, new List<string> { "WelcomeWizard" });
                }

                var apiEx = ex as HttpApiException;

                DialogService.ShowDismissible(R.UnhandledErrorTitle, apiEx != null ? apiEx.ErrorMessage.Message : String.Empty);
            }
        }
    }
}
