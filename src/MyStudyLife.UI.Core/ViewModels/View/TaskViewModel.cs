﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using MvvmCross.Commands;
using MyStudyLife.Data;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using Task = MyStudyLife.Data.Task;
using AsyncTask = System.Threading.Tasks.Task;
using MyStudyLife.UI.Utilities;
using MyStudyLife.Data.UI;
using MyStudyLife.UI.Helpers;
using MyStudyLife.Utility;
using MyStudyLife.UI.Extensions;
using PropertyChanged;

namespace MyStudyLife.UI.ViewModels.View {
    public class TaskViewModel : EntityViewModel<Task> {
        public const string SetCompleteEventAction = "Set Complete";
        public const string SetIncompleteEventAction = "Set Incomplete";
        public const string ChangeProgressEventAction = "Change Progress";

        private readonly IClassRepository _classRepository;
        private readonly IExamRepository _examRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly SortedObservableCollection<ScheduledClass> _classForTask = new SortedObservableCollection<ScheduledClass>(c => c.Date);
        private readonly SortedObservableCollection<Exam> _examForTask = new SortedObservableCollection<Exam>(e => e.Date);

        public override string Title { get; protected set; }
        public override string SubTitle { get; protected set; }

        public TaskState State { get; private set; }
        public System.Drawing.Color? StateColor { get; private set; }

        public string DueText { get; private set; }

        public string DueRelativeText { get; private set; }
        public bool ShowDueRelativeText { get; private set; }

        /// <summary>
        ///     Text showing when the task was marked as complete
        ///     relative to the current date.
        /// </summary>
        /// <example>Completed 2 days ago</example>
        public string CompletedRelativeText { get; private set; }

        /// <summary>
        ///     The class that the task is due for.
        /// </summary>
        public SortedObservableCollection<ScheduledClass> ClassForTask => _classForTask;

        /// <summary>
        ///     If the task is revision, the exam the task is due for.
        /// </summary>
        public SortedObservableCollection<Exam> ExamForTask => _examForTask;

        #region Commands

        private MvxCommand _setIncompleteCommand;
        private MvxCommand _markAsCompleteCommand;

        public ICommand SetIncompleteCommand => Command.Create(ref _setIncompleteCommand, () => SetProgress(0));

        public ICommand MarkAsCompleteCommand => Command.Create(ref _markAsCompleteCommand, () => SetProgress(100));

        private async void SetProgress(int progress) {
            var progressType = progress == 0 ? SetIncompleteEventAction : (progress == 100 ? SetCompleteEventAction : ChangeProgressEventAction);

            this.Analytics.TrackEvent(R.Task, progressType);

            var task = this.Item;

            if (task == null) return;

            task.Progress = progress;

            await this._taskRepository.UpdateProgressAsync(task);
        }

        #endregion

        public TaskViewModel(
            IFeatureService featureService,
            IClassRepository classReposity,
            ITaskRepository taskRepository,
            IExamRepository examRepository
        ) : base(taskRepository, featureService) {
            this._classRepository = classReposity;
            this._taskRepository = taskRepository;
            this._examRepository = examRepository;
        }

        private void SetCompletedRelativeText() {
            var task = this.Item;

            if (task == null || task.IsIncomplete) {
                this.CompletedRelativeText = null;
            }
            else {
                this.CompletedRelativeText = $"{R.Completed} {task.CompletedAt.GetValueOrDefault(DateTime.Now).ToRelativeDateString(Tense.Past)}";
            }
        }

        #region Overrides of ViewEntityViewModel<Task>

        private CancellationTokenSource _updateTaskProgressCancellationTokenSource;

        [SuppressPropertyChangedWarnings]
        protected override async void OnItemChanged(Task oldItem, Task newItem) {
            this.ClassForTask.Clear();
            this.ExamForTask.Clear();

            if (newItem == null) {
                this.Title = null;
                this.SubTitle = null;
                return;
            }

            this.Title = this.Item.Title;
            this.SubTitle = $"{newItem.Subject.Name} {Catalog.GetString(newItem.Type.ToString())}";
            // TODO: Localize
            this.DueText = $"Due {newItem.DueDate.ToContextualDateString()}";

            var classForTask = (await _classRepository.GetForTaskAsync(newItem)).OrderBy(x => x.StartTime).FirstOrDefault();

            if (classForTask != null) {
                this.ClassForTask.Add(classForTask);
            }

            if (newItem.IsRevision && newItem.ExamGuid.HasValue) {
                var exam = await _examRepository.GetAsync(newItem.ExamGuid.Value);

                if (exam != null) {
                    this.ExamForTask.Add(exam);
                }
            }

            var state = newItem.GetState();
            var daysDue = newItem.DueDate.ToRelativeDateString(Tense.Any, false, false);

            this.State = state;

            if (state != TaskState.Complete) {
                if (state == TaskState.Overdue) {
                    this.DueRelativeText = String.Format(R.OverdueText, daysDue);
                    this.StateColor = UserColors.Attention;
                }
                else {
                    this.DueRelativeText = String.Format(R.DueText, daysDue);
                    this.StateColor = state == TaskState.IncompleteAndDueSoon ? UserColors.Warning : (Color?)null;
                }
            }
            else {
                this.DueRelativeText = null;
                this.StateColor = null;
            }

            this.ShowDueRelativeText = !newItem.IsComplete;

            this.SetCompletedRelativeText();
        }

        [SuppressPropertyChangedWarnings]
        protected override void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e) {
            var item = this.Item;

            if (item == null) {
                return;
            }

            if (e.PropertyName == nameof(this.Item.Progress)) {

                var ts = this._updateTaskProgressCancellationTokenSource;

                ts?.Cancel();

                ts = this._updateTaskProgressCancellationTokenSource = new CancellationTokenSource();

                AsyncTask.Delay(500, ts.Token).ContinueWith((t) => {

                    if (!ts.IsCancellationRequested) {
                        // Has to be main thread because changing the progress / updated
                        // properties raises the PropertyChanged event.
                        InvokeOnMainThread(async () => {
                            Task task = this.Item;

                            if (task == null || ts.IsCancellationRequested) {
                                return;
                            }

                            await this._taskRepository.UpdateProgressAsync(task);

                            this.Analytics.TrackEvent(R.Task, task.Progress == 100 ? SetCompleteEventAction : ChangeProgressEventAction);

                            this.Messenger.Publish(new EntityEventMessage<Task>(this, task, ItemAction.Update));
                        });
                    }

                    this._updateTaskProgressCancellationTokenSource?.Dispose();
                    this._updateTaskProgressCancellationTokenSource = null;

                }, ts.Token);
            }
            else if (e.PropertyName.In(nameof(this.Item.IsComplete), nameof(this.Item.CompletedAt))) {
                this.SetCompletedRelativeText();
            }
        }

        #endregion
    }
}
