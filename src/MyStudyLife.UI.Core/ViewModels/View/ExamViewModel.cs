﻿using System;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.Data.UI;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.Utility;
using MyStudyLife.UI.Extensions;
using MyStudyLife.UI.Utilities;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Messaging;

namespace MyStudyLife.UI.ViewModels.View {
    public class ExamViewModel : EntityViewModel<Exam> {
        private readonly IClassRepository _classRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IAcademicYearRepository _academicYearRepository;


        public override string Title { get; protected set; }
        public override string SubTitle { get; protected set; }
        public string Instance { get; private set; }
        public string Duration { get; private set; }
        public bool IsAfterToday { get; private set; }
        public ExamState State { get; private set; }
        public System.Drawing.Color? StateColor { get; private set; }

        public string DateRelativeText { get; private set; }

        private IAcademicSchedule _effectiveSchedule;
        public override IAcademicSchedule EffectiveSchedule => _effectiveSchedule ?? base.EffectiveSchedule;

        private SortedObservableCollection<Task> _tasksForExam;
        private SortedObservableCollection<ScheduledClass> _conflictingClasses;

        public SortedObservableCollection<Task> TasksForExam {
            get { return _tasksForExam ?? (_tasksForExam = new SortedObservableCollection<Task>(x => x.DueDate)); }
            private set { SetProperty(ref _tasksForExam, value); }
        }

        public SortedObservableCollection<ScheduledClass> ConflictingClasses {
            get { return _conflictingClasses ?? (_conflictingClasses = new SortedObservableCollection<ScheduledClass>(x => x.StartTime)); }
            private set { SetProperty(ref _conflictingClasses, value); }
        }

        public ExamViewModel(
            IAcademicYearRepository academicYearRepository,
            IClassRepository classRepository,
            IExamRepository examRepository,
            ITaskRepository taskRepository,
            IFeatureService featureService)
        : base(examRepository, featureService) {
            this._classRepository = classRepository;
            this._taskRepository = taskRepository;
            this._academicYearRepository = academicYearRepository;
        }

        #region Override Item / Item Property Changed Methods

        protected override async void OnItemChanged(Exam oldItem, Exam newItem) {
            // Detach observable collections so CollectionChanged is not listened to.
            var tasksForExam = this.TasksForExam;
            var conflictingClasses = this.ConflictingClasses;

            this.TasksForExam = null;
            this.ConflictingClasses = null;

            tasksForExam.Clear();
            conflictingClasses.Clear();

            if (newItem != null) {
                if (newItem.Subject.Schedule == null) {
                    this.SetProperty(
                        ref _effectiveSchedule,
                        await _academicYearRepository.GetClosestScheduleToDateAsync(newItem.Date, TenseBias.None),
                        // ReSharper disable once ExplicitCallerInfoArgument
                        nameof(EffectiveSchedule)
                    );
                }

                this.Title = newItem.Title;
                this.SubTitle = this._effectiveSchedule != null ? this.ScheduleText : String.Empty;
                this.Instance = $"{newItem.Date.ToShortTimeStringEx()} {newItem.Date.ToContextualDateString()}";
                this.Duration = $"{newItem.Duration} {R.minutes}";

                tasksForExam.AddRange(await this._taskRepository.GetForExamAsync(newItem));
                conflictingClasses.AddRange(await this._classRepository.GetConflictsForExamAsync(newItem));

                this.IsAfterToday = newItem.Date.IsAfter(DateTimeEx.Now.EndOfDay());
                this.State = newItem.State;

                if (this.IsAfterToday) {
                    var state = newItem.State;

                    if (state == ExamState.InTheNext3Days) {
                        this.StateColor = UserColors.Attention;
                    }
                    else if (state == ExamState.InTheNext7Days) {
                        this.StateColor = UserColors.Warning;
                    }
                    else {
                        this.StateColor = null;
                    }

                    this.DateRelativeText = this.Item.Date.ToRelativeDateString(Tense.Future, false).ToSentenceCase();
                }
                else {
                    this.StateColor = null;
                    this.DateRelativeText = null;
                }
            }
            else {
                this.Title = null;
                this.SubTitle = null;
            }

            this.TasksForExam = tasksForExam;
            this.ConflictingClasses = conflictingClasses;
        }

        #endregion

        #region Messaging

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<EntityEventMessage<Task>>(OnMessage, Thread.MainThread);
        }

        private async void OnMessage(EntityEventMessage<Task> message) {
            var task = message.Entity;
            var item = this.Item;

            if (task != null && item != null && this._tasksForExam != null) {
                if ((task.ExamGuid != null && task.ExamGuid == item.Guid) || this._tasksForExam.Any(t => t.Guid == task.Guid)) {
                    var tasksForExam = await this._taskRepository.GetForExamAsync(item);

                    this.InvokeOnMainThread(() => {
                        this._tasksForExam.Clear();
                        this._tasksForExam.AddRange(tasksForExam);
                    });
                }
            }
        }

        #endregion
    }
}
