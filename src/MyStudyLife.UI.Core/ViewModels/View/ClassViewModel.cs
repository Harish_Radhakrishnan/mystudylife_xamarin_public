﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Data.Schools;
using MyStudyLife.Data.UI;
using MyStudyLife.Diagnostics;
using MyStudyLife.Extensions;
using MyStudyLife.Messaging;
using MyStudyLife.ObjectModel;
using MyStudyLife.Scheduling;
using MyStudyLife.Utility;
using PropertyChanged;
using Task = MyStudyLife.Data.Task;
using AsyncTask = System.Threading.Tasks.Task;
using System.Collections.Generic;
using MvvmCross.Commands;
using MyStudyLife.Data.Settings;
using MyStudyLife.Data.Store;
using MyStudyLife.Globalization;
using MyStudyLife.UI.Utilities;
using MyStudyLife.UI.Extensions;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.UI.Helpers;

namespace MyStudyLife.UI.ViewModels.View {
    [Screen(NameSetByViewModel = true)]
    public class ClassViewModel : EntityViewModel<Class> {
        private readonly IClassRepository _classRepository;
        private readonly ITaskRepository _taskRepository;

        public override string Title { get; protected set; }
        public override string SubTitle { get; protected set; }

        public bool IsReadOnly { get; private set; }

        public override IAcademicSchedule EffectiveSchedule => this.Item?.ActualSchedule;

        #region Scheduled Class

        public string Instance { get; private set; }

        public string Location { get; private set; }

        public string TeacherName { get; private set; }

        private ScheduledClass _scheduledItem;

        [DoNotNotify]
        public ScheduledClass ScheduledItem {
            get { return _scheduledItem; }
            private set {
                var oldItem = _scheduledItem;

                if (SetProperty(ref _scheduledItem, value)) {
                    OnScheduledItemChanged(oldItem, value);
                }
            }
        }

        public SortedObservableCollection<Task> TasksForClass { get; private set; } = new SortedObservableCollection<Task>(x => x.DueDate);

        public SortedObservableCollection<Task> OverdueTasksForSubject { get; private set; } = new SortedObservableCollection<Task>(x => x.DueDate);

        [Obsolete("Use StartEndDateText")]
        public string ClassScheduleText { get; private set; }

        public bool IsScheduled { get; private set; }

        [DependsOn(nameof(IsScheduled))]
        public bool ShowOverdueTasks => this.IsScheduled && this.OverdueTasksForSubject.Any();

        #endregion

        #region Schedule Tab

        public ClassScheduleViewModel ClassSchedule { get; private set; }

        #endregion

        #region Student:Leave

        public bool CanLeave { get; private set; }

        public bool IsLeaving { get; private set; }

        private MvxCommand _leaveCommand;
        private MvxCommand _leaveWithConfirmationCommand;

        public ICommand LeaveCommand => Command.Create(ref _leaveCommand, Leave);

        public ICommand LeaveWithConfirmationCommand => Command.Create(ref _leaveWithConfirmationCommand, LeaveWithConfirmation);

        private async void Leave() {
            var cls = this.Item;

            if (cls == null) {
                return;
            }

            this.IsLeaving = true;

            try {
                using (var classRepo = new ClassRepository()) {
                    await classRepo.Leave(cls);
                }

                await this.NavigationService.Close(this);

                this.Messenger.Publish(new EntityEventMessage<Class>(this, cls, ItemAction.Delete));
            }
            catch (Exception ex) {
                if (ex.IsTransientWebException() || !IsOnline) {
                    DialogService.ShowDismissible(R.OfflineError, R.ClassSubscriptionOfflineMessage);
                }
                else {
                    DialogService.ShowDismissible(R.UnhandledErrorTitle, R.UnhandledErrorMessage);

                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);
                }
            }
            finally {
                this.IsLeaving = false;
            }
        }

        private void LeaveWithConfirmation() {
            DialogService.ShowCustom(
                R.ClassInput.LeaveClassQuestion,
                R.ClassInput.LeaveConfirmation,
                new Services.CustomDialogAction(R.Leave, Leave),
                new Services.CustomDialogAction(R.Common.Cancel)
            );
        }

        #endregion

        #region Teacher:Students

        public bool StudentsCanJoinClass { get; private set; }
        public bool StudentsHasErrored { get; private set; }
        public bool StudentsIsBusy { get; private set; }

        private ObservableCollection<Student> _students;
        private string _studentsText;
        private MvxCommand _setStudentsCommand;
        private CancellationTokenSource _studentsCancellationTokenSource;

        public ObservableCollection<Student> Students {
            get { return _students; }
            private set { SetProperty(ref _students, value); }
        }

        public string StudentsText {
            get { return _studentsText; }
            private set { SetProperty(ref _studentsText, value); }
        }

        public bool HasStudents {
            get { return _students != null && _students.Count > 0; }
        }

        public ICommand SetStudentsCommand {
            get { return _setStudentsCommand ?? (_setStudentsCommand = new MvxCommand(SetStudents)); }
        }

        private async void SetStudents() {
            this.StudentsHasErrored = false;

            this.StudentsCanJoinClass = User.IsTeacher && User.School.ClassSharingEnabled;

            if (!this.StudentsCanJoinClass) {
                return;
            }

            if (this.Item == null) {
                this.Students = new ObservableCollection<Student>();
            }
            else if (this.IsOnline) {
                this.StudentsIsBusy = true;
                this.StudentsText = R.Common.LoadingWithEllipsis;

                try {
                    if (_studentsCancellationTokenSource != null) {
                        _studentsCancellationTokenSource.Cancel();

                        _studentsCancellationTokenSource = null;
                    }

                    _studentsCancellationTokenSource = new CancellationTokenSource();

                    await _classRepository.GetStudentsAsync(this.Item, _studentsCancellationTokenSource.Token).ContinueWith(
                        async (task) => {
                            if (task.IsFaulted) {
                                var ex = task.Exception.InnerExceptions.FirstOrDefault();

                                this.StudentsHasErrored = true;

                                if (ex != null && (ex.IsTransientWebException() || !IsOnline)) {
                                    this.StudentsText = R.Common.OfflineTapToRetry;
                                }
                                else {
                                    this.StudentsText = R.Common.ErrorTapToRetry;

                                    Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex);
                                }
                            }
                            else if (task.IsCompleted) {
                                this.Students = (await task).ToObservableCollection();

                                this.StudentsText = String.Format(new PluralFormatProvider(true), "{0:student;students} joined", this.Students.Count);
                            }
                        });
                }
                finally {
                    this.StudentsIsBusy = false;
                }
            }
            else {
                this.StudentsText = R.Common.OfflineTapToRetry;
            }
        }

        public void CancelPendingRequests() {
            if (_studentsCancellationTokenSource != null) {
                _studentsCancellationTokenSource.Cancel();
            }
        }

        #endregion

        #region Commands

        protected override async System.Threading.Tasks.Task Edit(Class item) {
            if (this.ScheduledItem == null || this.Item.Type == ClassType.OneOff) {
                await base.Edit(item);
            }
            else {
                var result = await DialogService.RequestConfirmationAsync(R.ConfirmationTitle, R.ScheduledClassEdit);

                if (result.DidConfirm) {
                    await base.Edit(item);
                }
            }
        }

        protected override async  System.Threading.Tasks.Task DeleteWithConfirmation() {
            if (this.ScheduledItem == null) {
                await base.DeleteWithConfirmation();
            }
            else {
                var result = await DialogService.RequestConfirmationAsync(R.ConfirmationTitle, R.ScheduledClassDeletion);

                if (result.DidConfirm) {
                    await Delete();
                }
            }
        }

        #endregion
        
        public ClassViewModel(IClassRepository classRepository, ITaskRepository taskRepository, IFeatureService featureService)
            : base(classRepository, featureService) {
            this._classRepository = classRepository;
            this._taskRepository = taskRepository;
        }
        
        private ScheduledNavObject _navObject;
        public void Init(ScheduledNavObject navObject) {
            _navObject = navObject;
        }

        protected override async Task<bool> StartAsync() {
            if (!await base.StartAsync()) {
                return false;
            }

            var c = this.Item;

            if (c.IsOneOff) {
                this.ScheduledItem = ScheduledClass.FromClass(c);
            }
            else if (_navObject.Date != default(DateTime) && !_navObject.TimeGuid.IsEmpty()) {
                c.SortTimes();

                var time = c.Times.SingleOrDefault(x => x.Guid == _navObject.TimeGuid);

                // Fail gracefully
                if (time != null) {
                    this.ScheduledItem = new ScheduledClass(c, time, _navObject.Date);
                }
            }

            try {
                this.NavigationEntry?.SetName(this.IsScheduled ? "Scheduled Class" : "Class");
            }
            catch (Exception ex) {
                Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, handled: true);
            }

            return true;
        }

        #region Item Changed
        protected override void OnItemChanged(Class oldItem, Class newItem) {
            base.OnItemChanged(oldItem, newItem);

            // Extra check to diganose MSL-108 (we think the cause is a term being provided
            // with a null year and we don't have a good stack trace because OnItemChanged
            // was an async void)
            var term = newItem.ActualSchedule as AcademicTerm;
            if (term != null && term.Year == null) {
                throw new InvalidOperationException(
                    $"{nameof(Class)} ({newItem.Guid}) with {nameof(AcademicTerm)} as ActualSchedule has a null year."
                );
            }

            OnItemChangedCore(oldItem, newItem);
        }

        protected async void OnItemChangedCore(Class oldItem, Class newItem) {
            if (newItem == null) {
                this.Title = null;
                this.SubTitle = null;
                return;
            }

            this.Title = String.Concat(
                newItem.Subject.Name,
                !String.IsNullOrWhiteSpace(newItem.Module) ? ": " + newItem.Module : String.Empty,
                " ",
                Catalog.GetString("Class")
            );
            this.SubTitle = this.ScheduleText;

            this.Location = newItem.Location;
            this.TeacherName = newItem.TeacherName;

            var user = User.Current;

            this.IsReadOnly = this.Item.IsFromSchool && !user.IsTeacher;
            this.CanLeave = this.Item.IsFromSchool && await FeatureService.CanUseFeatureAsync(Feature.JoinClasses);

            if (!this.Item.IsOneOff) {
                var schedule = this.Item.ActualSchedule;

                // TODO(V5.1): Remove this
                if (!this.Item.HasStartEndDates && schedule == null) {
#pragma warning disable CS0618 // Type or member is obsolete
                    this.ClassScheduleText = R.Scheduling.InTimetableIndefinitely;
#pragma warning restore CS0618 // Type or member is obsolete
                }
                else {
                    DateTime startDate, endDate;

                    // ReSharper disable PossibleInvalidOperationException
                    if (this.Item.HasStartEndDates) {
                        startDate = this.Item.StartDate.Value;
                        endDate = this.Item.EndDate.Value;
                    }
                    else if (schedule != null) {
                        startDate = schedule.StartDate;
                        endDate = schedule.EndDate;
                    }
                    else {
                        return;
                    }

                    // Normalize
                    if (this.Item.HasStartEndDates && schedule != null) {
                        startDate = this.Item.StartDate > schedule.StartDate ? schedule.StartDate : this.Item.StartDate.Value;
                        endDate = this.Item.EndDate > schedule.EndDate ? schedule.EndDate : this.Item.EndDate.Value;
                    }
                    // ReSharper restore PossibleInvalidOperationException

#pragma warning disable CS0618 // Type or member is obsolete
                    this.ClassScheduleText = String.Format(R.Scheduling.InTimetableDateRangeFormat, startDate, endDate);
#pragma warning restore CS0618 // Type or member is obsolete
                }

                this.ClassSchedule = new ClassScheduleViewModel(this.Item, CanUseExtendedColors, await TimetableSettings.Get(Mvx.IoCProvider.Resolve<ISingletonDataStore>()));
            }

            SetStudents();
        }

        protected async void OnScheduledItemChanged(ScheduledClass oldItem, ScheduledClass newItem) {
            this.TasksForClass.Clear();
            this.OverdueTasksForSubject.Clear();

            this.IsScheduled = newItem != null;

            if (newItem == null) return;

            this.Instance = $"{newItem.Time} {newItem.Date.ToContextualDateString()}";
            this.Location = newItem.Location;
            this.TeacherName = newItem.TeacherName;

            await this.UpdateTasksAsync();
        }

        #endregion

        protected override Task<System.Drawing.Color?> GetColor() {
            var user = User.Current;

            System.Drawing.Color? color = null;

            if (this.Item?.Subject != null) {
                color = UserColors.GetColor(user.IsTeacher ? this.Item.ActualColor : this.Item.Subject.Color, CanUseExtendedColors).Color;
            }

            return AsyncTask.FromResult(color);
        }

        public class ScheduledNavObject : NavObject {
            public Guid TimeGuid { get; set; }

            public DateTime Date { get; set; }
        }

        private async AsyncTask UpdateTasksAsync() {
            if (this.ScheduledItem != null) {
                var tasks = await _taskRepository.GetForScheduledClassAsync(this.ScheduledItem);
                var tasksForClass = tasks.Where(x => !x.IsOverdue).ToSortedObservableCollection(x => x.DueDate);
                var overdueTasksForSubject = tasks.Where(x => x.IsOverdue).ToSortedObservableCollection(x => x.DueDate);

                InvokeOnMainThread(() => {
                    this.TasksForClass = tasksForClass;
                    this.OverdueTasksForSubject = overdueTasksForSubject;

                    RaisePropertyChanged(nameof(this.ShowOverdueTasks));
                });
            }
        }

        #region Messenger

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            this.On<EntityEventMessage<Task>>(OnMessage, Thread.MainThread);
        }

        private async void OnMessage(EntityEventMessage<Task> message) {
            var task = message.Entity;
            var item = this.Item;
            var tasksForClass = this.TasksForClass;
            var overdueTasksForSubject = this.OverdueTasksForSubject;

            if (task != null && item != null && (
                task.SubjectGuid == item.SubjectGuid ||
                (tasksForClass?.Any(x => x.Guid == task.Guid) ?? false) ||
                (overdueTasksForSubject?.Any(x => x.Guid == task.Guid) ?? false)
            )) {
                await this.UpdateTasksAsync();
            }
        }

        protected override async AsyncTask OnMessageAsync(EntityEventMessage<Class> message) {
            await base.OnMessageAsync(message);

            if (message.Action != ItemAction.Update) {
                return;
            }

            var cls = message.Entity;

            if (cls != null && this.Item != null && message.Entity.Guid == this.Item.Guid) {
                // Checks if the user has not changed the date so it no longer occurs
                // on the previous scheduled item's date. If it doesn't then we show
                // the class as if it were not scheduled.
                var scheduledItem = this.ScheduledItem;
                if (scheduledItem != null) {
                    if (cls.IsOneOff) {
                        this.ScheduledItem = ScheduledClass.FromClass(cls);
                        return;
                    }

                    // Check the time still exists
                    var classTimeGuid = scheduledItem.ClassTime.Guid;
                    var newTime = cls.Times.FirstOrDefault(x => x.Guid == classTimeGuid);

                    if (newTime == null) {
                        this.ScheduledItem = null;
                        return;
                    }

                    var entries = await Mvx.IoCProvider.Resolve<IAgendaScheduler>().GetForDateAsync(scheduledItem.Date, AgendaSchedulerOptions.None);

                    this.ScheduledItem = entries.FirstOrDefault(x =>
                        x.IsClass && x.BaseGuid == cls.Guid &&
                        classTimeGuid == ((ScheduledClass)x.Base).ClassTime.Guid
                    )?.Base as ScheduledClass;
                }
            }
        }

        #endregion

        public class ClassScheduleViewModel {
            public string Location { get; }
            public bool HasMultipleLocations { get; }
            public bool HasMultipleTeachers { get; }

            /// <summary>
            ///     The primary teacher if multiple.
            /// </summary>
            public string TeacherName { get; }

            public List<ClassTimeGroup> GroupedTimes { get; }

            public List<ClassTimeViewModel> Times { get; }

            /// <summary>
            ///     If the class has start end dates, the formatted date range
            ///     for the class, otherwise null.
            /// </summary>
            public string StartEndDateText { get; }

            public ClassScheduleViewModel(Class @cls, bool extendedColors, TimetableSettings tSettings = null) {
                Check.NotNull(@cls, nameof(@cls));

                if (@cls.IsOneOff) {
                    throw new NotSupportedException($"One-off classes do not have a schedule and therefore cannot be used by {nameof(ClassScheduleViewModel)}");
                }
                
                var year = (@cls.ActualSchedule as AcademicTerm)?.Year ?? (AcademicYear)@cls.ActualSchedule;

                bool isWeekRotation = year?.Scheduling.IsWeekRotation ?? false;

                string location = @cls.Location;
                string teacherName = @cls.TeacherName;
                var times = @cls.Times.ToList();
                var timeVms = new List<ClassTimeViewModel>();
                var timeGroups = new Dictionary<int, ClassTimeGroup>();

                @times.Sort((a, b) => ClassTime.Compare(a, b, true));

                foreach (var time in times) {
                    if (location == null) {
                        location = time.Location;
                    }
                    else if (!String.IsNullOrEmpty(time.Location) && !location.Equals(time.Location)) {
                        this.HasMultipleLocations = true;
                    }

                    if (String.IsNullOrEmpty(teacherName)) {
                        teacherName = time.TeacherName;
                    }
                    else if (!String.IsNullOrEmpty(time.TeacherName) && !time.TeacherName.Equals(teacherName)) {
                        this.HasMultipleTeachers = true;
                    }

                    var timeVm = new ClassTimeViewModel(time, @cls.Location, @cls.TeacherName, tSettings);

                    var rotationWeek = isWeekRotation ? (int?)time.RotationWeek.GetValueOrDefault(0) : null;
                    var groupKey = rotationWeek.GetValueOrDefault(0);

                    ClassTimeGroup classTimeGroup;
                    if (!timeGroups.TryGetValue(groupKey, out classTimeGroup)) {
                        timeGroups[groupKey] = new ClassTimeGroup(
                            groupKey,
                            rotationWeek != null
                                // TODO(pri:low): Translate
                                ? rotationWeek == 0
                                    ? "Weekly"
                                    : $"Week {(User.Current.Settings.IsRotationScheduleLettered ? Humanize.NumberToLetter(rotationWeek.Value) : rotationWeek.ToString())}"
                                : null,
                            UserColors.GetColor(@cls.GetColor(User.Current), extendedColors).Color,
                            timeVm
                        );
                    }
                    else {
                        classTimeGroup.Times.Add(timeVm);
                    }

                    timeVms.Add(timeVm);
                }

                if (!this.HasMultipleLocations) {
                    this.Location = location;
                }

                timeVms.Apply(t => t.ShowLocation = this.HasMultipleLocations);
                timeVms.Apply(t => t.ShowTeacher = this.HasMultipleTeachers);

                this.GroupedTimes = timeGroups.Values.ToList();
                this.TeacherName = @cls.Teacher?.FullName ?? @cls.TeacherName;

                if (@cls.HasStartEndDates) {
                    this.StartEndDateText = new DateTimeRange(@cls.EffectiveStartDate.Value, @cls.EffectiveEndDate.Value).ToString();
                }
            }

            public class ClassTimeGroup {
                public object Key { get; }
                public string Name { get; }
                public System.Drawing.Color Color { get; }
                public List<ClassTimeViewModel> Times { get; } = new List<ClassTimeViewModel>();

                public ClassTimeGroup(object key, string name, System.Drawing.Color color, params ClassTimeViewModel[] classTimeVms) {
                    this.Key = key;
                    this.Name = name;
                    this.Color = color;
                    this.Times.AddRange(classTimeVms);
                }
            }

            public class ClassTimeViewModel {
                public MultiLengthText OccurrenceText { get; set; }
                public string Time { get; set; }
                public string Location { get; set; }
                public bool ShowLocation { get; set; }
                public string TeacherName { get; set; }
                public bool ShowTeacher { get; set; }

                public ClassTimeViewModel(ClassTime time, string location, string teacherName, TimetableSettings tSettings = null) {
                    this.OccurrenceText = new MultiLengthText(
                        time.GetOccurrenceText(tSettings, prependRotationWeek: false),
                        () => time.GetOccurrenceText(tSettings, Globalization.DateComponentNaming.Abbreviated, false),
                        () => time.GetOccurrenceText(tSettings, Globalization.DateComponentNaming.Shortest, false)
                    );
                    this.Time = time.Time;
                    this.Location = time.Location ?? location;
                    this.TeacherName = time.TeacherName ?? teacherName;
                }
            }
        }
    }
}
