﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Plugin.Messenger;
using MyStudyLife.Data;
using MyStudyLife.Data.UI;
using MyStudyLife.Messaging;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.UI.ViewModels.Input;
using MyStudyLife.Utility;
using PropertyChanged;
using Task = MyStudyLife.Data.Task;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.ViewModels.View {
    [SuppressPropertyChangedWarnings]
    public abstract class EntityViewModel<T> : BaseViewModel
        where T : SubjectDependentEntity {

        private readonly IRepository<T> _repo;
        protected IFeatureService FeatureService { get; }

        private readonly Color _fallbackColor = System.Drawing.Color.FromArgb(28, 141, 118);
        private Color? _color;

        public virtual Color? Color {
            get => _color ?? _fallbackColor;
            set => _color = value;
        }

        [DependsOn(nameof(Color))]
        public Color? ColorOrNull => _color;

        private T _item;

        [DoNotNotify]
        public T Item {
            get { return _item; }
            set {
                var oldItem = _item;

                if (_item != null) {
                    _item.PropertyChanged -= OnItemPropertyChanged;
                }

                OnItemChanging(oldItem, value);

                SetProperty(ref _item, value);

                if (value != null) {
                    value.PropertyChanged += OnItemPropertyChanged;
                }

                OnItemChanged(oldItem, value);

                RaisePropertyChanged(() => Color);
                RaisePropertyChanged(() => ScheduleText);
                RaisePropertyChanged(() => EffectiveSchedule);
            }
        }

        public virtual IAcademicSchedule EffectiveSchedule => Item?.Subject.Schedule;

        public virtual string ScheduleText {
            get {
                var schedule = EffectiveSchedule;

                if (schedule == null) {
                    return R.NoAcademicScheduleText;
                }

                return schedule is AcademicTerm term ? $"{term.Year} | {term.Name}" : schedule.ToString();
            }
        }

        public abstract string Title { get; protected set; }
        public abstract string SubTitle { get; protected set; }
        
        public bool CanUseExtendedColors { get; private set; }

        #region Commands

        private MvxAsyncCommand _newCommand;
        private MvxAsyncCommand _editCommand;
        private MvxAsyncCommand _deleteCommand;
        private MvxAsyncCommand _deleteWithConfirmationCommand;

        public ICommand NewCommand {
            get { return _newCommand ??= new MvxAsyncCommand(New); }
        }

        public ICommand EditCommand {
            get { return _editCommand ??= new MvxAsyncCommand(Edit); }
        }

        public ICommand DeleteCommand {
            get { return _deleteCommand ??= new MvxAsyncCommand(Delete); }
        }

        public ICommand DeleteWithConfirmationCommand {
            get {
                return _deleteWithConfirmationCommand ??= new MvxAsyncCommand(DeleteWithConfirmation);
            }
        }

        #endregion

        protected EntityViewModel(IRepository<T> repo, IFeatureService featureService) {
            _repo = repo;
            FeatureService = featureService;
        }

        #region Overrides of MvxViewModel

        private NavObject _navObject;
        public void Init(NavObject navObject) {
            _navObject = navObject;
        }
        protected override async Task<bool> StartAsync() {
            await base.StartAsync();

            CanUseExtendedColors = await FeatureService.CanUseFeatureAsync(Feature.ExtendedColors);

            Item = await _repo.GetAsync(_navObject.Guid);

            if (Item == null) {
                // if the Item is null, push a this.Close to the end of the callstack
                // so it executes after Mvx is finish initialising the ViewModel.
                await AsyncTask.Delay(1).ContinueWith(_ => InvokeOnMainThread(async () => {
                    await this.NavigationService.Close(this);
                }));
                return false;
            }

            Color = await GetColor() ?? _fallbackColor;

            return true;
        }

        #endregion

        protected async System.Threading.Tasks.Task New() {
            await New(null);
        }

        protected async System.Threading.Tasks.Task New(Subject subject) {
            // Prevents the "NULL" subject from the filter list
            // being used...
            var navObject = new SubjectDependentInputViewModel<T>.NavObject();

            if (subject?.Name != null) {
                navObject.SubjectGuid = subject.Guid;
            }

            await NewOrEdit(navObject);
        }

        protected async System.Threading.Tasks.Task Edit() {
            if (Item != null) {
                await Edit(Item);
            }
        }

        protected virtual async System.Threading.Tasks.Task Edit(T item) {
            if (item == null) {
                throw new ArgumentNullException(nameof(item));
            }

            await NewOrEdit(new SubjectDependentInputViewModel<T>.NavObject {
                Guid = item.Guid
            });
        }

        protected virtual async System.Threading.Tasks.Task DeleteWithConfirmation() {
            var result = await DialogService.RequestConfirmationAsync(R.ConfirmationTitle, R.DeletionMessage);

            if (result.DidConfirm) {
                await Delete();
            }
        }

        protected virtual async System.Threading.Tasks.Task Delete() {
            var item = Item;

            await _repo.DeleteAsync(item);

            Item = null;

            await NavigationService.ChangePresentation(EntityDeletedPresentationHint.ForEntity(item, this));

            Messenger.Publish(new EntityEventMessage<T>(this, item, ItemAction.Delete));
        }

        protected virtual Task<Color?> GetColor() {
            return System.Threading.Tasks.Task.FromResult(Item != null ? UserColors.GetColor(Item.Subject.Color, CanUseExtendedColors).Color : (Color?)null);
        }

        #region Overridable Eventing

        /// <summary>
        ///     Called just before the item is set.
        /// </summary>
        protected virtual void OnItemChanging(T oldItem, T newItem) { }

        /// <summary>
        ///     Called just after the item is set.
        /// </summary>
        protected virtual void OnItemChanged(T oldItem, T newItem) { }

        protected virtual void OnItemPropertyChanged(object sender, PropertyChangedEventArgs e) { }

        #endregion

        private async System.Threading.Tasks.Task NewOrEdit(SubjectDependentInputViewModel<T>.NavObject navObject) {
            Type viewModel;

            if (typeof(T) == typeof(Class)) {
                viewModel = typeof(ClassInputViewModel);
            }
            else if (typeof(T) == typeof(Task)) {
                viewModel = typeof(TaskInputViewModel);
            }
            else if (typeof(T) == typeof(Exam)) {
                viewModel = typeof(ExamInputViewModel);
            }
            else {
                throw new NotSupportedException("Given type is not yet supported, you have some work to do!");
            }

            await NavigationService.Navigate(viewModel, navObject);
        }

        public class NavObject {
            public Guid Guid { get; set; }
        }

        #region Messenger

        protected override void SubscribeToMessages(IMvxMessenger messenger) {
            base.SubscribeToMessages(messenger);

            On<EntityEventMessage<T>>(OnMessage, Thread.MainThread);
        }

        private async void OnMessage(EntityEventMessage<T> message) {
            await OnMessageAsync(message);
        }

        protected virtual async AsyncTask OnMessageAsync(EntityEventMessage<T> message) {
            if (message.Entity == null || Item == null) {
                return;
            }

            if (message.Entity.Guid == Item.Guid) {
                if (message.Action == ItemAction.Delete) {
                    await NavigationService.Close(this);
                }
                else if (message.Action == ItemAction.Update) {
                    if (!ReferenceEquals(message.Entity, Item)) {
                        // Without this the item setter doesn't do anything, strange!
                        _item = null;

                        Item = await _repo.GetAsync(message.Entity.Guid);

                        Color = await GetColor();
                    }
                }
            }
        }

        #endregion
    }
}
