﻿namespace MyStudyLife.UI.ViewModels {
    public interface ILoaded {
        bool IsLoaded { get; }
    }
}