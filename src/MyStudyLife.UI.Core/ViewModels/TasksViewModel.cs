﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Input;
using MvvmCross.Commands;
using MyStudyLife.Data;
using MyStudyLife.Data.Filters;
using MyStudyLife.Sync;
using MyStudyLife.UI.Data;
using MyStudyLife.UI.Helpers;
using MyStudyLife.UI.ObjectModel;
using MyStudyLife.UI.ViewModels.Base;
using MyStudyLife.Utility;
using PropertyChanged;
using AsyncTask = System.Threading.Tasks.Task;
using Task = MyStudyLife.Data.Task;
using TaskGroupedCollectionViewSource = MyStudyLife.UI.Data.GroupedCollectionViewSource<int, MyStudyLife.Data.Task>;

namespace MyStudyLife.UI.ViewModels {
    public class TasksViewModel : BaseEntitiesViewModel<Task> {
        private readonly ITaskRepository _taskRepository;

        public TasksViewModel(
            IFeatureService featureService,
            ITaskRepository taskRepository,
            ISubjectRepository subjectRepository,
            IAcademicYearRepository academicYearRepository
        ) : base("tasks", featureService, subjectRepository, academicYearRepository, taskRepository) {
            this._taskRepository = taskRepository;
            this.Filter = TaskFilterOption.Current;
            this.Groupings = new List<TaskGrouping>();

            this.SelectedItems.CollectionChanged += SelectedItemsOnCollectionChanged;
        }

        public async AsyncTask MarkSelectedAsComplete() {
            var selectedTasks = this.SelectedItems.Cast<Task>()
                                    .Where(t => t.Progress < 100)
                                    .ToList();

            await this._taskRepository.UpdateProgressAsync(selectedTasks, 100);

            this.SelectedItems.RemoveRange(selectedTasks);
        }

        #region Android / Windows 8 / iOS

        public List<TaskGrouping> Groupings { get; set; }

        // On Windows 8 we change the filter instead
        // of switching between views.

        private CollectionViewSource<Task> _viewSource;
        public override CollectionViewSource<Task> ViewSource {
            get {
                if (_viewSource == null) {
                    _viewSource = new CollectionViewSource<Task>(
                        this.Items,
                        GetFilterPredicate(this.Filter),
                        GetSortDescription(this.Filter)
                    ) {
                        EqualityComparer = new EntityEqualityComparer()
                    };

                    // We use a custom event (ViewChanged) as it is (hopefully)
                    // always the first callback in the queue that is triggered so
                    // we invalidate the groupings before the UI gets the event.
                    _viewSource.ViewChanged += (s, e) => this.InvalidateGroupings();
                }

                return _viewSource;
            }
        }

        private TaskGroupedCollectionViewSource _groupedViewSource;
        public TaskGroupedCollectionViewSource GroupedViewSource {
            get {
                if (_groupedViewSource == null) {
                    _groupedViewSource = new TaskGroupedCollectionViewSource(
                        this.Items,
                        TaskGrouping.For,
                        GetFilterPredicate(this.Filter),
                        GetSortDescription(this.Filter).Order
                    ) {
                        EqualityComparer = new EntityEqualityComparer()
                    };
                }

                return _groupedViewSource;
            }
        }

        public TaskFilterOption Filter { get; set; }

        [DependsOn(nameof(Filter))]
        public bool IsPastFilter => Filter == TaskFilterOption.Past;

        #region Commands

        private MvxCommand<TaskFilterOption> _setFilterCommand;
        private MvxCommand _toggleFilterCommand;

        public ICommand SetFilterCommand => Command.Create(ref _setFilterCommand, SetFilter);
        public ICommand ToggleFilterCommand => Command.Create(ref _toggleFilterCommand, ToggleFilter);

        private void SetFilter(TaskFilterOption filter) {
            if (this.Filter != filter) {
                this.Filter = filter;

                this._viewSource?.SetFilterAndSort(
                    GetFilterPredicate(filter),
                    GetSortDescription(filter)
                );
                this._groupedViewSource?.SetFilterAndSort(
                    GetFilterPredicate(filter),
                    GetSortDescription(filter).Order
                );
            }
        }

        private void ToggleFilter() {
            SetFilter(this.Filter == TaskFilterOption.Past ? TaskFilterOption.Current : TaskFilterOption.Past);
        }

        #endregion

        private static Predicate<Task> GetFilterPredicate(TaskFilterOption filter) {
            return filter == TaskFilterOption.Past
                ? TaskFilter.FilterPastPredicate
                : TaskFilter.FilterCurrentPredicate;
        }

        private static SortDescription<Task> GetSortDescription(TaskFilterOption filter) {
            return new SortDescription<Task>(
                x => x.DueDate,
                filter == TaskFilterOption.Past
                    ? SortOrder.Descending
                    : SortOrder.Ascending
            );
        }

        #endregion

        #region Windows Phone

        private CollectionViewSource<Task> _currentViewSource;
        public CollectionViewSource<Task> CurrentViewSource {
            get {
                return _currentViewSource ??
                       (_currentViewSource = new CollectionViewSource<Task>(
                        this.Items,
                        GetFilterPredicate(TaskFilterOption.Current),
                        GetSortDescription(TaskFilterOption.Current)
                    )
                );
            }
        }

        private CollectionViewSource<Task> _pastViewSource;

        [Obsolete("Renamed to PastViewSource")]
        public CollectionViewSource<Task> CompletedViewSource {
            get {
                return PastViewSource;
            }
        }

        public CollectionViewSource<Task> PastViewSource {
            get {
                return _pastViewSource ??
                       (_pastViewSource = new CollectionViewSource<Task>(
                        this.Items,
                        GetFilterPredicate(TaskFilterOption.Past),
                        GetSortDescription(TaskFilterOption.Past)
                    )
                );
            }
        }

        #endregion

        #region Override Event Methods

        protected override void OnItemsChanged() {
            if (_viewSource != null) {
                _viewSource.Source = this.Items;
            }

            if (_groupedViewSource != null) {
                _groupedViewSource.Source = this.Items;
            }

            if (_currentViewSource != null) {
                _currentViewSource.Source = this.Items;
            }

            if (_pastViewSource != null) {
                _pastViewSource.Source = this.Items;
            }
        }

        private void InvalidateGroupings() {
            this.Groupings.Clear();
        }

        public TaskGrouping GetGrouping(int position) {
            if (_viewSource.Count <= position) return null;

            var groupings = this.Groupings;

            if (groupings.Count <= position) {
                int i = groupings.Count;

                for (; i <= position; i++) {
                    groupings.Insert(i, TaskGrouping.For(_viewSource.View[i]));
                }
            }

            return groupings[position];
        }

        private void SelectedItemsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            if (e.OldItems != null && e.Action.In(
                NotifyCollectionChangedAction.Remove,
                NotifyCollectionChangedAction.Replace,
                NotifyCollectionChangedAction.Reset)
            ) {
                bool setLonely = false;

                foreach (var item in e.OldItems.Cast<Task>()) {
                    if (TaskFilter.FilterPastPredicate(item) && this.Filter != TaskFilterOption.Past) {
                        setLonely = true;

                        this._viewSource?.View?.Remove(item);
                        this._currentViewSource?.View?.Remove(item);
                    }
                    else if (TaskFilter.FilterCurrentPredicate(item) && this.Filter != TaskFilterOption.Current) {
                        setLonely = true;

                        this._viewSource?.View?.Remove(item);
                        this._pastViewSource?.View?.Remove(item);
                    }
                }

                if (setLonely) {
                    this.SetLonelyText();
                }
            }
        }

        protected override void OnItemAddedLocal(Task item) {
            this.Filter = TaskFilter.FilterPastPredicate(item) ? TaskFilterOption.Past : TaskFilterOption.Current;
        }

        protected override void OnItemUpdatedLocal(Task item) {
            // TODO(eventually) we could make this even better by comparing it to the task's old values
            this.InvalidateGroupings();

            // Can't change the view when there is selected items
            if (this.SelectedItems.Count == 0 || (this.SelectedItems.Count == 1 && ((Task)this.SelectedItems.First()).Guid == item.Guid)) {
                // When the task becomes a past task, it's extremely likely
                // that the user has just updated the progress - in that
                // case they don't want to the list filter to change.
                if (this.Filter == TaskFilterOption.Current && TaskFilter.FilterPastPredicate(item)) {

                    this._viewSource?.View.Remove(item);
                    this._currentViewSource?.View.Remove(item);
                    this._pastViewSource?.NotifyItemsSourceChanged();
                }
                else if (this.Filter != TaskFilterOption.Current && TaskFilter.FilterCurrentPredicate(item)) {
                    this.SetFilter(TaskFilterOption.Current);

                    this._currentViewSource?.NotifyItemsSourceChanged();
                    this._pastViewSource?.NotifyItemsSourceChanged();
                }
                else {
                    this._viewSource?.ReSort();
                    this._currentViewSource?.ReSort();
                    this._pastViewSource?.ReSort();
                }

                this.SetLonelyText();
                this._groupedViewSource?.NotifyItemChanged(item);
            }
        }

        protected override async void OnSyncCompleted(SyncCompletedEventArgs e) {
            if (e.SubjectsModified) {
                // Update everything.
                await this.Invalidate();
            }
            else if (e.TasksModified) {
                this.MergeModified(e.ModifiedTasks);
            }
        }

        #endregion
    }
}
