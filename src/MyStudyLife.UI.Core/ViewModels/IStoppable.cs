﻿namespace MyStudyLife.UI.ViewModels {
    public interface IStoppable {
        void Stop();
    }
}
