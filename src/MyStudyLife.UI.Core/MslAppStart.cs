﻿using System;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.ViewModels;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Services;
using MyStudyLife.UI.Bootstrap;
using MyStudyLife.UI.Services;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.Versioning;
using Task = System.Threading.Tasks.Task;
using MyStudyLife.Sync;
using Microsoft.Extensions.Logging;

namespace MyStudyLife.UI
{
    public class MslAppStart : MvxAppStart
    {
        // We are not using constructor injection here 
        private readonly INavigationService _navigationService;
        private readonly IMvxMainThreadAsyncDispatcher _dispatcher;
        private readonly IUserStore _userStore;
        private readonly IAuthorizationService _authService;
        private readonly IUpgradeService _upgradeService;
        private readonly IPushNotificationService _pushNotificationService;
        private readonly IFeedbackService _feedbackService;
        private readonly ISyncService _syncService;
        private readonly ILogger<MslAppStart> _logger;

        /// <summary>
        ///     Creates a new instance of <see cref="MslAppStart"/> which lazy
        ///     resolves dependencies using <see cref="Mvx.IoCProvider.Resolve{TService}"/>.
        ///     
        ///     This is useful for platforms when we know <see cref="IMvxAppStart.Start(object)"/>
        ///     is not always called, such as on a "cold resume" on Android.
        /// </summary>
        public MslAppStart(
            IMvxApplication application,
            INavigationService navigationService,
            IMvxMainThreadAsyncDispatcher dispatcher,
            IUserStore userStore,
            IAuthorizationService authorizationService,
            IUpgradeService upgradeService,
            IPushNotificationService pushNotificationService,
            IFeedbackService feedbackService,
            ISyncService syncService,
            ILogger<MslAppStart> logger
        ) : base(application, navigationService)
        {
            _navigationService = navigationService ?? throw new ArgumentNullException(nameof(navigationService));
            _dispatcher = dispatcher ?? throw new ArgumentNullException(nameof(dispatcher));
            _userStore = userStore ?? throw new ArgumentNullException(nameof(userStore));
            _authService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
            _upgradeService = upgradeService ?? throw new ArgumentNullException(nameof(upgradeService));
            _pushNotificationService = pushNotificationService ?? throw new ArgumentNullException(nameof(pushNotificationService));
            _feedbackService = feedbackService ?? throw new ArgumentNullException(nameof(feedbackService));
            _syncService = syncService ?? throw new ArgumentNullException(nameof(syncService));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override async Task NavigateToFirstViewModel(object hintObj = null)
        {
            _logger.LogDebug("hintObj shouldn't be null, otherwise the feedback service will presume AppStartMode=Resume");

            Type viewModelType;

            if (_upgradeService.RequiresUpgrade)
            {
                viewModelType = typeof(UpgradeViewModel);
            }
            else if (!_authService.IsAuthorized)
            {
                viewModelType = typeof(SignInViewModel);
            }
            else
            {
                var user = _userStore.GetCurrentUser();

                viewModelType = _navigationService.GetFirstViewAuthorized(user);
            }

            await NavigationService.Navigate(viewModelType);
        }

        protected override async Task<object> ApplicationStartup(object hintObj = null)
        {
            var hint = await base.ApplicationStartup(hintObj) as AppStartHint;

            RunApplicationStartupTasks(hint);

            return hint;
        }

        private async void RunApplicationStartupTasks(AppStartHint hint)
        {
            if (!_authService.IsAuthorized)
            {
                return;
            }

            // TODO: Refactor and split into whether required to run on each sign in etc
            if (Mvx.IoCProvider.TryResolve(out IBootstrapAction action))
            {
                await action.RunAsync();
            }

            await _pushNotificationService.EnsureNotificationChannelAsync();

            _syncService.TrySync(SyncTriggerKind.Programmatic);

            await Task.Delay(3000);

            // TODO: Do this when the first view is loaded
            var user = _userStore.GetCurrentUser();

            if (user != null && !user.HasSchool)
            {
                await _dispatcher.ExecuteOnMainThreadAsync(() =>
                {
                    var appStartMode = hint?.Mode ?? AppStartMode.Resume;

                    // We're only incrementing the number of app starts if
                    // the user is authorized!
                    _feedbackService.CheckAndShowReviewView(appStartMode);
                });
            }
        }
    }

    public enum AppStartMode
    {
        New,
        Resume
    }

    public class AppStartHint
    {
        public AppStartMode Mode { get; }

        public AppStartHint(AppStartMode mode)
        {
            Mode = mode;
        }
    }
}