﻿namespace MyStudyLife.UI.Analytics {
    public static class EventCategories {
        public const string SignIn = "Sign In";

        public const string WelcomeWizard = "Welcome Wizard";
    }

    public static class EventActions {
        public const string Click = "Click";
    }
}
