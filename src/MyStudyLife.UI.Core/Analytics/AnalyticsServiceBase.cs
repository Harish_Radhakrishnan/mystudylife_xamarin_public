﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MvvmCross.Exceptions;
using MyStudyLife.Authorization;
using MyStudyLife.Data;
using MyStudyLife.Diagnostics;

namespace MyStudyLife.UI.Analytics
{
    // We could split this into AnalyticsService and AnalyticsProvider (per platform)?

    public abstract class AnalyticsServiceBase : IAnalyticsService {
        private const string TraceTag = "msl:Analytics";

#if DEBUG
        protected const string PropertyId = "UA-60628705-1";
        private readonly TimeSpan _dispatchInterval = TimeSpan.FromSeconds(1);
#else
        protected const string PropertyId = "UA-22456359-3";
        private readonly TimeSpan _dispatchInterval = TimeSpan.FromSeconds(5);
#endif

        private string _currentUserId;

        protected TimeSpan DispatchInterval => _dispatchInterval;

        protected INavigationService NavigationService { get; private set; }

        protected AnalyticsServiceBase(INavigationService navigationService) {
            this.NavigationService = navigationService;
        }

        private readonly object initLock = new object();
        private bool _initialized;

        public void EnsureInitialized() {
            if (_initialized) {
                return;
            }

            lock (initLock) {
                if (_initialized) {
                    return;
                }

                InitializeImpl();

                _initialized = true;
            }
        }

        protected virtual void InitializeImpl() {
            this.NavigationService.BackStack.Changed += BackStackOnChanged;
        }

        private void BackStackOnChanged(object sender, NavigationBackStackChangedEventArgs e) {
            try {
                var currentScreen = ((NavigationBackStack) sender).Peek();

                if (currentScreen == null) {
                    Trace("Back stack is empty, nothing to track");
                    return;
                }

                var screenAttribute = ScreenAttribute.Get(currentScreen.ViewModelType);

                if ((screenAttribute == null || !screenAttribute.DoNotTrack) && currentScreen.HasName) {
                    TrackScreen(currentScreen.Name);
                }
            }
            catch (Exception ex) {
                TraceError(ex, "tracking screen from back stack change");
            }
        }

        private void OnMessage(AuthStatusChangedEventMessage message) {
            
        }

        protected void Trace(string message, params object[] args) {
            Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(TraceTag).LogDebug(message, args);
        }

        protected void TraceError(Exception ex, string whilstDoingWhat) {
            Mvx.IoCProvider.Resolve<ILoggerProvider>().CreateLogger(TraceTag).LogError("An error occurred whilst " + whilstDoingWhat + ":\n" + ex.ToLongString());

            Mvx.IoCProvider.Resolve<IBugReporter>().Send(ex, new List<string> {
                "Handled", TraceTag
            });
        }

        public void SetUserInfo(User user) {
            if (user != null) {
                var userId = user.Id.ToString();
                var userType = user.IsTeacher ? "Teacher" : "Student";

                string schoolId = null;
                bool isManagedSchool = false;

                if (user.HasSchool) {
                    schoolId = user.School.Id.ToString();
                    isManagedSchool = user.School.IsManaged;
                }
                this.SetUserInfo(userId, userType, schoolId, isManagedSchool);
            }
            else {
                this.SetUserInfoCore(null, null, null, null);
            }
        }

        public void SetUserInfo(string userId, string userType) {
            this.SetUserInfoCore(userId, userType, null, null);
        }

        public void SetUserInfo(string userId, string userType, string schoolId, bool isManagedSchool) {
            var schoolType = schoolId == null ? null : (isManagedSchool ? "Managed" : "Unmanaged");
            this.SetUserInfoCore(userId, userType, schoolId, schoolType);
        }

        private void SetUserInfoCore(string userId, string userType, string schoolId, string schoolType) {
            if (userId == null && this._currentUserId != userId) {
                this.NotifySessionEnd();
            }

            this._currentUserId = userId;

            this.SetUserInfoImpl(userId, userType, schoolId, schoolType);
        }

        public void TrackScreen(string screenName) {
            Trace("Tracking screen view '{0}'", screenName);

            TrackScreenImpl(screenName);
        }

        public void TrackEvent(string category, string action) {
            Trace("Tracking event '{0}' / '{1}'", category, action);

            TrackEventImpl(category, action);
        }

        public void TrackEvent(string category, string action, string label) {
            Trace("Tracking event '{0}' / '{1}' / '{2}'", category, action, label);

            TrackEventImpl(category, action, label);
        }

        public void NotifySessionEnd() {
            NotifySessionEndImpl();
        }

        protected abstract void SetUserInfoImpl(string userId, string userType, string schoolId, string schoolType);
        protected abstract void TrackScreenImpl(string screenName);
        protected abstract void TrackEventImpl(string category, string action);
        protected abstract void TrackEventImpl(string category, string action, string label);
        protected abstract void NotifySessionEndImpl();
    }
}
