﻿using MyStudyLife.Data;

namespace MyStudyLife.UI.Analytics {
    public interface IAnalyticsService {
        void EnsureInitialized();

        void SetUserInfo(User user);
        void SetUserInfo(string userId, string userType);
        void SetUserInfo(string userId, string userType, string schoolId, bool isManagedSchool);

        void TrackScreen(string screenName);

        void TrackEvent(string category, string action);
        void TrackEvent(string category, string action, string label);

        /// <summary>
        ///     Tells the tracker the next screens and/or events tracked
        ///     will form part of a new session.
        /// </summary>
        void NotifySessionEnd();
    }
}
