﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MyStudyLife.Data.Filters;
using MyStudyLife.ObjectModel;

// ReSharper disable once CheckNamespace
namespace MyStudyLife.UI {
    public static class CollectionExtensions {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection) {
            return new ObservableCollection<T>(collection);
        }

	    public static SortedObservableCollection<T> ToSortedObservableCollection<T>(this IEnumerable<T> collection,
			Func<T, object> orderByKeySelector, SortDirection sortOrder = SortDirection.Ascending) {
		    return new SortedObservableCollection<T>(orderByKeySelector, collection, sortOrder);
	    } 
    }
}
