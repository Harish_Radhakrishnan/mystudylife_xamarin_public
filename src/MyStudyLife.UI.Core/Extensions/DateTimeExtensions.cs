﻿using System;
using System.Globalization;
using MyStudyLife.Globalization;

namespace MyStudyLife.UI.Extensions {
    public static class DateTimeExtensions {
        public static string ToContextualDateString(this DateTime dt)
            => ToContextualDateString(dt, ContextualDateOptions.Default);

        public static string ToContextualDateString(this DateTime dt, ContextualDateOptions options)
            => ToContextualDateString(dt, options, ContextualDateFormat.Long);

        public static string ToContextualDateString(this DateTime dt, ContextualDateFormat format)
            => ToContextualDateString(dt,
                format == ContextualDateFormat.Short ? ContextualDateOptions.DefaultShortFormat : ContextualDateOptions.Default,
                format
            );

        public static string ToContextualDateString(this DateTime dt, ContextualDateOptions options, ContextualDateFormat format) {
            var now = DateTimeEx.Now;

            if (options.HasFlag(ContextualDateOptions.RelativeDayNames)) {
                var days = (int)(dt.StartOfDay() - now.StartOfDay()).TotalDays;

                // ToTitleCase on non-user data
                if (days == -1 && !options.HasFlag(ContextualDateOptions.OmitYesterday)) {
                    return R.RelativeTime.Yesterday.ToTitleCase();
                }
                else if (days == 0) {
                    return R.RelativeTime.Today.ToTitleCase();
                }
                else if (days == 1) {
                    return R.RelativeTime.Tomorrow.ToTitleCase();
                }
            }

            string pattern = GetFormatPattern(format);

            if (options.HasFlag(ContextualDateOptions.OmitYearWhenCurrent) && dt.Year == now.Year) {
                pattern = DateTimeFormat.RemoveYearRegex.Replace(pattern, String.Empty);
            }

            return dt.ToString(pattern);
        }

        private static string GetFormatPattern(ContextualDateFormat format) {
            switch (format) {
                case ContextualDateFormat.Long:
                    return DateTimeFormatInfo.CurrentInfo.LongDatePattern;
                case ContextualDateFormat.Abbreviated:
                    return DateTimeFormat.ShortLongDatePattern;
                case ContextualDateFormat.Short:
                    return DateTimeFormat.AbbrLongDatePattern;
                default:
                    throw new ArgumentOutOfRangeException(nameof(format), format, $"Must be a member of {nameof(ContextualDateFormat)}");
            }
        }
    }

    public enum ContextualDateFormat {
        /// <summary>
        ///     Equivalent to 'D'
        /// </summary>
        Long,
        /// <summary>
        ///     Like long, but using the abbreviated
        ///     month and day names. eg. "Mon, 19 Sept, 2016"
        /// </summary>
        Abbreviated,
        /// <summary>
        ///     The shortest date string with the day, month and year.
        ///     eg. 19 Sep 16
        /// </summary>
        Short
    }

    [Flags]
    public enum ContextualDateOptions {
        /// <summary>
        ///     Specifies the strings "Yesterday", "Today" or "Tomorrow" or their
        ///     translated equivalents should be used for the respective days.
        /// </summary>
        RelativeDayNames = 1 << 0,

        /// <summary>
        ///     When used in conjunction with <see cref="RelativeDayNames"/>, "Yesterday"
        ///     will not be returned, instead the default format will be used.
        /// </summary>
        OmitYesterday = 1 << 1,

        /// <summary>
        ///     Specifies the year should be ommitted when the date is for the
        ///     current year.
        /// </summary>
        OmitYearWhenCurrent = 1 << 2,

        /// <summary>
        ///     The default options used when converting.
        /// </summary>
        Default = RelativeDayNames | OmitYesterday | OmitYearWhenCurrent,

        /// <summary>
        ///     The default options used when converting using <see cref="ContextualDateFormat.Short"/>.
        /// </summary>
        DefaultShortFormat = OmitYearWhenCurrent
    }
}
