﻿using System.Collections.Generic;
using MyStudyLife.Data;

namespace MyStudyLife.UI.ObjectModel {
    public class EntityEqualityComparer : EqualityComparer<BaseEntity> {
        public override bool Equals(BaseEntity x, BaseEntity y) => x.Guid == y.Guid;
        public override int GetHashCode(BaseEntity obj) => obj.Guid.GetHashCode();
    }
}
