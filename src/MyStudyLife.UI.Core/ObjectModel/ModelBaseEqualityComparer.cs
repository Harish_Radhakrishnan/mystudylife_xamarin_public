﻿using System.Collections.Generic;
using MyStudyLife.Data;
using MyStudyLife.UI.Models;

namespace MyStudyLife.UI.ObjectModel {
    public class ModelBaseEqualityComparer<T> : EqualityComparer<IModel<T>>
        where T : BaseEntity {

        public override bool Equals(IModel<T> x, IModel<T> y) => x.Base.Guid == y.Base.Guid;
        public override int GetHashCode(IModel<T> obj) => obj.Base.Guid.GetHashCode();
    }
}
