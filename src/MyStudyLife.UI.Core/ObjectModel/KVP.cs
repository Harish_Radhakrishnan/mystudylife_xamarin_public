﻿using System.Collections.Generic;

namespace MyStudyLife.UI.ObjectModel {
    /// <summary>
    ///     An observable <see cref="KeyValuePair{TKey,TValue}"/>
    ///     equivalent. Also useful where W8 doesn't like to bind
    ///     to <see cref="KeyValuePair{TKey,TValue}"/>s.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public sealed class KVP<TKey, TValue> {
        public TKey Key { get; set; }

        public TValue Value { get; set; }

        public KVP() { }

        public KVP(TKey key, TValue value) {
            this.Key = key;
            this.Value = value;
        }

        public override string ToString() {
            return this.Value.ToString();
        }
    }
}
