﻿namespace MyStudyLife.UI.Models {
    public interface IModel {
        object Base { get; }
    }

    public interface IModel<T> : IModel {
        new T Base { get; }
    }
}
