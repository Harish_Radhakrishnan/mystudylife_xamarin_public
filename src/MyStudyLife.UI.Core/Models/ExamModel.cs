﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyStudyLife.Data;
using MyStudyLife.Globalization;
using MyStudyLife.UI.Extensions;

namespace MyStudyLife.UI.Models {
    public class ExamModel : IModel<Exam> {
        public System.Drawing.Color Color { get; }

        public string Title => Base.Title;
        public string SubTitle => $"{Date.ToShortTimeStringEx()} {Date.ToContextualDateString()}";
        // TODO: Localize
        public string DurationText => $"{Duration} min";

        public string TasksText => $"{TasksComplete}/{Tasks}";

        public int Tasks { get; set; }
        public int TasksComplete { get; set; }

        public DateTime Date => Base.Date;
        public Subject Subject => Base.Subject;
        public int Duration => Base.Duration;
        public TimeSpan StartTime => Base.StartTime;
        public Guid Guid => Base.Guid;

        public Exam Base { get; }

        object IModel.Base => Base;

        public ExamModel(Exam exam, System.Drawing.Color color) {
            Base = exam;
            Color = color;
        }

        public void PopulateTasks(IEnumerable<Task> tasks) {
            Tasks = tasks.Count();
            TasksComplete = tasks.Count(t => t.IsComplete);
        }
    }
}
