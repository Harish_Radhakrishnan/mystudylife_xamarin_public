﻿namespace MyStudyLife.UI {
    public enum Glyph {
        Dashboard = 0xEA06,
        Calendar = 0xEA02,
        Schedule = 0xEA19,
        Subject = 0xEA20,
        Class = 0xEA03,
        Task = 0xEA22,
        Exam = 0xEA0A,
        Search = 0xEA1B,
        Settings = 0xEA04,
        Today = 0xEA27,

        Delete = 0xEA07,
        Time = 0xEA26,
        Location = 0xEA0F,
        Seat = 0xEA1C,
        Teacher = 0xEA23,
        Attention = 0xEA01,
        Tick = 0xEA24,
        Tickbox = 0xEA25,
        Progress = 0xEA15,
        Student = 0xEA1F,
        School = 0xEA1A,
        Holiday = 0xEA0C,
        Stopwatch = 0xEA1E,

        Pushes = 0xEA18,
        Notification = 0xEA12,
        Sync = 0xEA21,
        Left = 0xEA0E
    }
}
