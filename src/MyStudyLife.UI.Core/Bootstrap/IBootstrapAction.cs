﻿namespace MyStudyLife.UI.Bootstrap {
    /// <summary>
    ///     An action which is run just after
    ///     the first view model has been shown.
    /// 
    ///     This action will only run once.
    /// </summary>
    public interface IBootstrapAction {
        System.Threading.Tasks.Task RunAsync();
    }
}
