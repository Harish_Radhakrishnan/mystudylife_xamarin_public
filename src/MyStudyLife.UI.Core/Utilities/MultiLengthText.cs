﻿using System;

namespace MyStudyLife.UI.Utilities {
    public class MultiLengthText {
        private readonly string _abbreviated, _shortest;
        private readonly Lazy<string> _abbreviatedAccessor, _shortestAccessor;

        public string Full { get; }
        public string Abbreviated => _abbreviated ?? _abbreviatedAccessor?.Value;
        public string Shortest => _shortest ?? _shortestAccessor?.Value;

        public MultiLengthText(string full, string abbreviated, string shortest) {
            this.Full = full;
            this._abbreviated = abbreviated;
            this._shortest = shortest;
        }
        public MultiLengthText(string full, Lazy<string> abbreviatedAccessor, Lazy<string> shortestAccessor = null) {
            Check.NotNull(full, nameof(full));
            Check.NotNull(abbreviatedAccessor, nameof(abbreviatedAccessor));

            this.Full = full;
            this._abbreviatedAccessor = abbreviatedAccessor;
            this._shortestAccessor = shortestAccessor;
        }
        public MultiLengthText(string full, Func<string> abbreviatedAccessor, Func<string> shortestAccessor = null) {
            Check.NotNull(full, nameof(full));
            Check.NotNull(abbreviatedAccessor, nameof(abbreviatedAccessor));

            this.Full = full;
            this._abbreviatedAccessor = new Lazy<string>(abbreviatedAccessor);
            this._shortestAccessor = shortestAccessor != null ? new Lazy<string>(shortestAccessor) : null;
        }

        public override string ToString() => this.Full;
    }
}
