﻿using System;
using System.Text;

namespace MyStudyLife.UI.Utilities {
    public static class DateTimeFormatEx {
        /// <summary>
        ///     Returns a human readable string relative to the current time,
        ///     such as "45 seconds ago" or "in 1 hour".
        /// </summary>
        public static string ToRelativeTimeString(this DateTime date) {
            double milliseconds = (date - DateTimeEx.Now).TotalMilliseconds;
            bool isFuture = milliseconds > 0;

            double seconds = Math.Round(Math.Abs(milliseconds) / 1000);
            if (seconds < 45) return Format(isFuture, R.RelativeTime.Seconds);

            double minutes = Math.Round(seconds / 60);
            if (minutes == 1d) return Format(isFuture, R.RelativeTime.Minute);
            if (minutes < 45) return Format(isFuture, R.RelativeTime.Minutes, minutes);

            double hours = Math.Round(minutes / 60);
            if (hours == 1d) return Format(isFuture, R.RelativeTime.Hour);
            if (hours < 22) return Format(isFuture, R.RelativeTime.Hours, hours);

            double days = Math.Round(hours / 24);
            if (days == 1d) return Format(isFuture, R.RelativeTime.Day);
            if (days <= 25) return Format(isFuture, R.RelativeTime.Days, days);
            if (days <= 45) return Format(isFuture, R.RelativeTime.Month);
            if (days < 345) return Format(isFuture, R.RelativeTime.Months, Math.Round(days / 30));

            double years = Math.Round(days / 365);
            if (years == 1) return Format(isFuture, R.RelativeTime.Year);

            return Format(isFuture, R.RelativeTime.Years, years);
        }

        /// <summary>
        ///     Returns a human readable string relative to the current date,
        ///     such as "today" or "yesterday".
        /// </summary>
        public static string ToRelativeDateString(this DateTime date) => ToRelativeDateString(date, Tense.Any);

        public static string ToRelativeDateString(this DateTime date, Tense allowedTense) => ToRelativeDateString(date, allowedTense, true);

        public static string ToRelativeDateString(this DateTime date, Tense allowedTense, bool namedDays) => ToRelativeDateString(date, allowedTense, namedDays, true);

        /// <summary>
        ///     Returns a human readable string relative to the current date,
        ///     such as "today" or "yesterday".
        /// </summary>
        /// <param name="allowedTenses">
        ///     If it makes no sense for the date to be in the past or future,
        ///     this can be specified to avoid time offset / rounding issues.
        /// </param>
        /// <param name="namedDays">
        ///     Whether or not to return "yesterday" "today" and "tomorrow"
        ///     rather than numbers
        /// </param>
        public static string ToRelativeDateString(this DateTime date, Tense allowedTenses, bool namedDays, bool includeTense) {
            double days = Math.Round((date.Date - DateTimeEx.Today).TotalDays);

            switch (allowedTenses) {
                case Tense.Past:
                    days = Math.Min(0, days);
                    break;
                case Tense.Future:
                    days = Math.Max(0, days);
                    break;
            }

            if (namedDays) {
                if (days == 0d) return R.RelativeTime.Today;
                if (days == 1d) return R.RelativeTime.Tomorrow;
            }

            bool isFuture = days > 0;

            days = Math.Abs(days);

            if (days == 1) return Format(isFuture, includeTense, R.RelativeTime.OneDay);
            if (days <= 25) return Format(isFuture, includeTense, R.RelativeTime.Days, days);
            if (days <= 45) return Format(isFuture, includeTense, R.RelativeTime.Month);
            if (days < 345) return Format(isFuture, includeTense, R.RelativeTime.Months, Math.Round(days / 30));

            double years = Math.Round(days / 365);
            if (years == 1) return Format(isFuture, includeTense, R.RelativeTime.Year);

            return Format(isFuture, includeTense, R.RelativeTime.Years, years);
        }

        private static string Format(bool isFuture, string format, params object[] args) => Format(isFuture, true, format, args);

        private static string Format(bool isFuture, bool includeTense, string format, params object[] args) => String.Format(
            includeTense ? (isFuture ? R.RelativeTime.Future : R.RelativeTime.Past) : "{0}",
            String.Format(format, args)
        );

        /// <summary>
        ///     Formats the given <see cref="TimeSpan"/> as a human
        ///     readable string. eg. "1d 1h 1m".
        /// </summary>
        /// <param name="timeSpan">
        ///     The <see cref="TimeSpan"/> to format.
        /// </param>
        /// <param name="minimumUnit">
        ///     The minimum unit to format. This also will be the unit
        ///     which is output when the given timespan is empty.
        /// </param>
        /// <param name="expandUnits">
        ///     Units to use a long label with when displayed on
        ///     their own. eg. "1m" would become "1 min"
        /// </param>
        /// <param name="roundUpMininmumUnit">
        ///     True to enable round up or "ceil" the minimum
        ///     unit. For example, when minimum unit is minutes
        ///     and the given TimeSpan has 1m 1s, this would be
        ///     rounded up to 2mins.
        /// </param>
        public static string ToShortString(
            this TimeSpan timeSpan,
            TimeUnit minimumUnit = TimeUnit.Seconds,
            TimeUnits expandUnits = TimeUnits.All,
            bool roundUpMininmumUnit = true
        ) {
            TimeUnit? minUnitWithValue = null;
            
            if (TimeUnit.Seconds <= minimumUnit && timeSpan.Seconds > 0) {
                minUnitWithValue = TimeUnit.Seconds;
            }
            else if (TimeUnit.Minutes <= minimumUnit && timeSpan.Minutes > 0) {
                minUnitWithValue = TimeUnit.Minutes;
            }
            else if (TimeUnit.Hours <= minimumUnit && timeSpan.Hours > 0) {
                minUnitWithValue = TimeUnit.Hours;
            }
            else if (timeSpan.Days > 0) {
                minUnitWithValue = TimeUnit.Days;
            }
            else {
                minUnitWithValue = minimumUnit;
            }

            var sb = new StringBuilder();

            Action<TimeUnit, string, string, int> formatAndAppendUnit = (unit, pluralFormat, identifier, value) => {
                if (sb.Length == 0 && expandUnits.HasFlag((TimeUnits)(1 << (int)unit)) && minUnitWithValue == unit) {
                    sb.AppendFormat(new PluralFormatProvider(true), $"{{0:{pluralFormat}}}", value);
                }
                else {
                    if (sb.Length > 0) {
                        sb.Append(" ");
                    }

                    sb.Append($"{value}{identifier}");
                }
            };

            // TODO: Localize
            if (timeSpan.Days > 0) {
                int days = timeSpan.Days;

                if (roundUpMininmumUnit && minimumUnit == TimeUnit.Days) {
                    days = timeSpan.Days + (int)Math.Ceiling(timeSpan.Hours / 24f);
                }

                formatAndAppendUnit(TimeUnit.Days, "day;days", "d", days);
            }

            // Adds unit if greater than minimum unit and has a value, or appends 0 and the unit's label if said unit is the minimum

            if ((timeSpan.Hours > 0 && TimeUnit.Hours <= minimumUnit) || minUnitWithValue == TimeUnit.Hours) {
                int hours = timeSpan.Hours;

                if (roundUpMininmumUnit && minimumUnit == TimeUnit.Hours) {
                    hours = timeSpan.Hours + (int)Math.Ceiling(timeSpan.Minutes / 60f);
                }

                formatAndAppendUnit(TimeUnit.Hours, "hour;hours", "h", hours);
            }

            if ((timeSpan.Minutes > 0 && TimeUnit.Minutes <= minimumUnit) || minUnitWithValue == TimeUnit.Minutes) {
                int minutes = timeSpan.Minutes;

                if (roundUpMininmumUnit && minimumUnit == TimeUnit.Minutes) {
                    minutes = timeSpan.Minutes + (int)Math.Ceiling(timeSpan.Seconds / 60f); 
                }
                
                formatAndAppendUnit(TimeUnit.Minutes, "min;mins", "m", minutes);
            }

            if ((timeSpan.Seconds > 0 && TimeUnit.Seconds <= minimumUnit) || minUnitWithValue == TimeUnit.Seconds) {
                int seconds = timeSpan.Seconds;

                if (roundUpMininmumUnit) {
                    seconds = timeSpan.Seconds + (int)Math.Ceiling(timeSpan.Milliseconds / 1000f);
                }

                formatAndAppendUnit(TimeUnit.Seconds, "sec;secs", "s", seconds);
            }
            
            return sb.ToString();
        }
    }

    public enum TimeUnit {
        Days = 0,
        Hours = 1,
        Minutes = 2,
        Seconds = 3
    }

    [Flags]
    public enum TimeUnits {
        Days = 1 << TimeUnit.Days,
        Hours = 1 << TimeUnit.Hours,
        Minutes = 1 << TimeUnit.Minutes,
        Seconds = 1 << TimeUnit.Seconds,

        All = Days | Hours | Minutes | Seconds
    }
}
