﻿using System;
using System.Reflection;

namespace MyStudyLife.UI {
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ScreenAttribute : Attribute {
        public string Name { get; set; }

        /// <summary>
        ///     Indicates the name of the screen will be set by
        ///     the view model.
        /// </summary>
        public bool NameSetByViewModel { get; set; }

        /// <summary>
        ///     Tells the back stack not to track this view.
        /// </summary>
        public bool DoNotTrack { get; set; }

        public string BackStackEntryKey { get; set; }

        public NavigationBackStackPushMode BackStackPushMode { get; set; }

        public ScreenAttribute() {
            this.BackStackPushMode = NavigationBackStackPushMode.Add;
        }

        public ScreenAttribute(string name) : this() {
            this.Name = name;
        }

        public static ScreenAttribute Get(Type viewModelType) => Get(viewModelType, true);
        public static ScreenAttribute Get(Type viewModelType, bool inherit) => viewModelType.GetTypeInfo().GetCustomAttribute<ScreenAttribute>(inherit);
    }
}
