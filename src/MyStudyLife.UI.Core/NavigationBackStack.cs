﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;
using MvvmCross;
using MyStudyLife.UI.ViewModels;

namespace MyStudyLife.UI
{
    public sealed class NavigationBackStack {
        private const string TraceTag = "msl:Navigation";

        private static readonly Dictionary<Type, string> _immutableScreenNameCache = new Dictionary<Type, string>();

        public event EventHandler<NavigationBackStackChangedEventArgs> Changed;

        private readonly List<Entry> _stack = new List<Entry>();

        private bool _preventRaiseChangedEvent;

        public int Depth => _stack.Count;

        public Transaction CurrentTransaction { get; private set; }

        public Transaction BeginTransaction() {
            if (CurrentTransaction != null) {
                throw new InvalidOperationException("Cannot start multiple transactions. Commit the existing before starting the new.");
            }

            return CurrentTransaction = new Transaction(this);
        }

        public void Push(Type viewModelType) {
            if (DoNotTrack(viewModelType)) return;

            this.Push(viewModelType, GetEntryName(viewModelType));
        }

        public void Push(Type viewModelType, string name) {
            if (DoNotTrack(viewModelType)) return;

            var pushMode = NavigationBackStackPushMode.Add;

            var attribute = viewModelType.GetTypeInfo().GetCustomAttribute<ScreenAttribute>();

            if (attribute != null) {
                pushMode = attribute.BackStackPushMode;
            }

            this.Push(viewModelType, name, pushMode);
        }

        public void Push(Type viewModelType, NavigationBackStackPushMode pushMode) {
            if (DoNotTrack(viewModelType)) return;

            this.Push(viewModelType, GetEntryName(viewModelType), pushMode);
        }

        public void Push(Type viewModelType, string name, NavigationBackStackPushMode pushMode) {
            if (DoNotTrack(viewModelType)) return;

            if (_stack.Count > 0) {
                int lastIndex = _stack.Count - 1;

                var previousItem = _stack[lastIndex];

                if (
                    previousItem.PushMode == NavigationBackStackPushMode.ReplaceOnPush ||
                    (
                        previousItem.PushMode == NavigationBackStackPushMode.ReplaceOnSamePush &&
                        previousItem.Key == GetEntryKey(viewModelType)
                    )
                )
                {
                    _stack.RemoveAt(lastIndex);
                }
            }

            _stack.Add(new Entry(this, viewModelType, name, pushMode));
            RaiseChanged(NavigationBackStackChangeAction.Push, viewModelType);
        }

        public void Pop() {
            if (_stack.Count > 0) {
                int index = _stack.Count - 1;
                var entry = _stack[index];
                _stack.RemoveAt(index);
                RaiseChanged(NavigationBackStackChangeAction.Pop, entry.ViewModelType);
            }
        }

        public void Pop(int count) {
            if (count < 1) {
                throw new ArgumentOutOfRangeException(nameof(count), count, "Number of back stack entries to pop must be greater than 0.");
            }

            if (_stack.Count == 0) {
                return;
            }

            if (_stack.Count > count) {
                int startIndex = _stack.Count - count;

                var entries = _stack.GetRange(startIndex, count);
                _stack.RemoveRange(startIndex, count);
                RaiseChanged(NavigationBackStackChangeAction.Pop, entries.Select(x => x.ViewModelType));
            }
            else {
                var entries = _stack.ToList();
                _stack.Clear();
                RaiseChanged(NavigationBackStackChangeAction.Pop, entries.Select(x => x.ViewModelType));
            }
        }

        /// <summary>
        ///     Pops the top entry, but only if it matches the
        ///     given <paramref name="viewModelType" />.
        /// </summary>
        /// <returns>
        ///     True if the top entry is removed.
        /// </returns>
        public bool Pop(Type viewModelType) {
            if (_stack.Count > 0) {
                int index = _stack.Count - 1;
                var entry = _stack[index];

                if (entry.ViewModelType == viewModelType) {
                    _stack.RemoveAt(index);
                    RaiseChanged(NavigationBackStackChangeAction.Pop, entry.ViewModelType);
                    return true;
                }
            }

            return false;
        }

        public Entry Peek() {
            return _stack.LastOrDefault();
        }

        /// <summary>
        ///     Returns the most recently pushed entry,
        ///     but only if the type matches
        /// </summary>
        /// <param name="viewModelType"></param>
        /// <returns></returns>
        public Entry Peek(Type viewModelType) {
            var entry = Peek();

            if (entry == null) {
                return null;
            }

            if (entry.ViewModelType != viewModelType) {
                Trace($"Call to peek expected view model type '{viewModelType.Name}' but actual type is '{entry.ViewModelType.Name}'");
                return null;
            }

            return entry;
        }

        /// <summary>
        ///     Returns the entry in the stack specified
        ///     by how far back in the stack it is.
        /// </summary>
        /// <param name="depth">The number of entries back from the top entry (where the top would be depth=0).</param>
        public Entry Peek(int depth) {
            if (depth >= _stack.Count) {
                throw new ArgumentOutOfRangeException(nameof(depth), depth, "Depth must be a number greater than 0 and less than the total depth of $Depth$.");
            }

            return _stack.ElementAt(_stack.Count - depth);
        }

        /// <remarks>
        ///     Does not call change event.
        /// </remarks>
        public void Clear() {
            _stack.Clear();
        }

        private void RaiseChanged(NavigationBackStackChangeAction action, Type item) {
            Trace($"Back stack changed - {action} '{item.Name}'");

            var handler = Changed;
            if (handler != null && !_preventRaiseChangedEvent) {
                handler(this, new NavigationBackStackChangedEventArgs(action, Enumerable.Repeat(item, 1)));
            }
        }

        private void RaiseChanged(NavigationBackStackChangeAction action, IEnumerable<Type> items) {
            Trace($"Back stack changed - {action} " + String.Join(", ", items.Select(x => String.Concat("'", x.Name, "'"))));

            var handler = Changed;
            if (handler != null && !_preventRaiseChangedEvent) {
                handler(this, new NavigationBackStackChangedEventArgs(action, items));
            }
        }

        private static bool DoNotTrack(Type viewModelType) {
            var attribute = GetScreenAttribute(viewModelType);

            return attribute != null && attribute.DoNotTrack;
        }

        private static string GetEntryKey(Type viewModelType) {
            var attribute = GetScreenAttribute(viewModelType);

            if (attribute != null && !String.IsNullOrEmpty(attribute.BackStackEntryKey)) {
                return attribute.BackStackEntryKey;
            }

            return viewModelType.Name.Substring(0, viewModelType.Name.IndexOf("ViewModel", StringComparison.Ordinal));
        }

        private static string GetEntryName(Type viewModelType) {
            string screenName;

            if (_immutableScreenNameCache.TryGetValue(viewModelType, out screenName)) {
                return screenName;
            }

            var attribute = GetScreenAttribute(viewModelType);
            bool hasAttribute = attribute != null;

            if (hasAttribute && attribute.NameSetByViewModel) {
                return null;
            }

            if (hasAttribute && !String.IsNullOrEmpty(attribute.Name)) {
                screenName = attribute.Name;
            }
            else {
                screenName = ViewModelHelpers.GetFriendlyName(viewModelType);
            }

            return _immutableScreenNameCache[viewModelType] = screenName;
        }

        private static ScreenAttribute GetScreenAttribute(Type viewModelType) => ScreenAttribute.Get(viewModelType);

        private static void Trace(string message, params object[] args) {
            Mvx.IoCProvider.Resolve<ILogger<NavigationBackStack>>().LogDebug(TraceTag + ": " + message, args);
        }

        [DebuggerDisplay("ViewModelType = {ViewModelType}")]
        public sealed class Entry {
            private readonly NavigationBackStack _backStack;

            private readonly NavigationBackStackPushMode _pushMode;
            private string _name;

            public string Key { get; }

            public Type ViewModelType { get; }

            public NavigationBackStackPushMode PushMode => _pushMode;

            /// <summary>
            ///     The name of the screen. If this is not set,
            ///     the view has opted to set the name dynamically
            ///     after it loads. Listen for the Change event on the
            ///     back stack.
            /// </summary>
            public string Name => _name;

            public bool HasName => _name != null;

            public Entry(NavigationBackStack backStack, Type viewModelType, string name,  NavigationBackStackPushMode pushMode) {
                _backStack = backStack;
                Key = GetEntryKey(viewModelType);
                _name = name;
                ViewModelType = viewModelType;
                _pushMode = pushMode;
            }

            public void SetName(string name) {
                if (this.HasName) {
                    if (this._name == name) {
                        return;
                    }

                    throw new InvalidOperationException("Name for this back stack entry has already been set. Once set it cannot be changed.") {
                        Data = {
                            ["Entry.ViewModelType"] = this.ViewModelType.Name,
                            ["Entry.Name"] = this.Name,
                            ["GivenName"] = name
                        }
                    };
                }

                this._name = name;

                _backStack.RaiseChanged(NavigationBackStackChangeAction.EntryModified, this.ViewModelType);
            }
        }

        // This is kind of a hack, transactions weren't originally designed to be a part of this.
        public sealed class Transaction {
            private readonly NavigationBackStack _backStack;

            private readonly Queue<Action> _queuedActions = new Queue<Action>();

            private bool _committing, _committed;

            internal Transaction(NavigationBackStack backStack) {
                _backStack = backStack;
            }

            public void Push(Type viewModelType) {
                CheckState();

                _queuedActions.Enqueue(() => _backStack.Push(viewModelType));
            }

            public void Push(Type viewModelType, string name) {
                CheckState();

                _queuedActions.Enqueue(() => _backStack.Push(viewModelType, name));
            }

            public void Push(Type viewModelType, NavigationBackStackPushMode pushMode) {
                CheckState();

                _queuedActions.Enqueue(() => _backStack.Push(viewModelType, pushMode));
            }

            public void Pop() {
                CheckState();

                _queuedActions.Enqueue(_backStack.Pop);
            }

            public void Pop(int count) {
                CheckState();

                _queuedActions.Enqueue(() => _backStack.Pop(count));
            }

            public void Commit() {
                if (_committed) {
                    throw new InvalidOperationException("Transaction has already been committed.");
                }

                if (_queuedActions.Any()) {
                    _committing = true;

                    _backStack._preventRaiseChangedEvent = true;

                    while (_queuedActions.Count > 0) {
                        _queuedActions.Dequeue()();
                    }

                    _backStack.CurrentTransaction = null;

                    _backStack._preventRaiseChangedEvent = false;

                    _backStack.Changed?.Invoke(_backStack, NavigationBackStackChangedEventArgs.Empty);

                    _committing = false;
                }

                _committed = true;
            }

            private void CheckState() {
                if (_committing) {
                    throw new InvalidOperationException("Cannot make changes to the transaction whilst it is being committed.");
                }

                if (_committed) {
                    throw new InvalidOperationException("Transaction has already been committed. To make changes to the back stack, start a new transaction.");
                }
            }
        }
    }

    public enum NavigationBackStackPushMode {
        Add,
        /// <summary>
        ///     If the top back stack item has the same key as the
        ///     view model being pushed, it is replaced.
        /// </summary>
        ReplaceOnSamePush,
        /// <summary>
        ///     If another view is pushed after this one, it is replaced.
        ///     This item can only ever be top of the back stack.
        /// </summary>
        ReplaceOnPush
    }

    public class NavigationBackStackChangedEventArgs : EventArgs {
        public static new NavigationBackStackChangedEventArgs Empty => new NavigationBackStackChangedEventArgs();

        public NavigationBackStackChangeAction Action { get; }

        public IEnumerable<Type> Items { get; }

        private NavigationBackStackChangedEventArgs() { }

        public NavigationBackStackChangedEventArgs(NavigationBackStackChangeAction action, IEnumerable<Type> items) {
            this.Action = action;
            this.Items = items;
        }
    }

    public enum NavigationBackStackChangeAction {
        Push, Pop, EntryModified
    }
}
