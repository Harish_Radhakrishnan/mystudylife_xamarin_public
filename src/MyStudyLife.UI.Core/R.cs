﻿using System;
// ReSharper disable InconsistentNaming

namespace MyStudyLife.UI {
    public static class R {
        public const string Subjects = "Subjects";
        public const string Classes = "Classes";
        public const string Tasks = "Tasks";
        public const string Exams = "Exams";

        public const string AcademicYear = "Academic Year";
        public const string Year = "Year";
        public const string Term = "Term";
        public const string Holiday = "Holiday";
        public const string Subject = "Subject";
        public const string Class = "Class";
        public const string Time = "Time";
        public const string Task = "Task";
        public const string Exam = "Exam";

        public const string Information = "Information";

        public const string ValidationErrorTitle = "Something you've entered is not quite right";

        public const string ValidationOccurrenceConflict = "One or more existing occurrences conflict with the one you're trying to enter.";
        public const string ValidationOccurrenceDuration = "That's a very short occurrence you're trying to enter! The duration of an occurrence must be at least 5 minutes.";

        public const string ValidationClassDuration = "That's a very short class you're trying to enter! The duration of a class must be at least 5 minutes.";
        public const string ValidationClassOccurrences = "Looks like you've forgotten to add the class' occurrences.";

        public const string ValidationTermYearDateOverlap = "Term start/end dates must be between (or equal to) academic year start/end dates.";
        public const string ValidationTermConflict = "The entered dates overlap with one or more terms. Terms cannot overlap.";

        public const string ValidationStartEndTimeMismatch = "Looks like you've got the start and end times the wrong way round. The start time cannot be after the end time.";
        public const string ValidationStartEndDateMismatch = "Looks like you've got the start and end dates the wrong way round. The start date cannot be after the end date.";

	    public const string ConfirmationTitle = "Are you sure?";
	    public const string DeletionMessage = "This action cannot be undone.";
        public const string DeletionPromptFormat = "Delete this {0}?";
        public const string Delete = "Delete";

        public const string InputCancelTitle = "Discard changes?";
        public const string InputCancelMessage = "The changes you've made will be lost.";
        public const string Discard = "Discard";

        public const string ScheduledClassDeletion = "You are about to delete all occurrences for this class.";
        public const string ScheduledClassEdit = "Editing this class will modify all occurrences.";

        public const string AcademicScheduleDeletionConstraintTitle = "Unable to delete";
        public const string AcademicScheduleYearDeletionSubjectConstraint = "This year has subjects assigned to it, to delete this year please delete or assign {0} to a different academic year/term.";
        public const string AcademicScheduleYearWithTermsDeletionSubjectConstraint = "This year and/or it's terms have subjects assigned to them, to delete this year please delete or assign {0} to a different academic year/term.";
        public const string AcademicScheduleYearDeletionClassConstraint = "This year has {0:class;classes} assigned to it, to delete this year please delete or assign the classes to a different academic year/term.";
        public const string AcademicScheduleYearWithTermsDeletionClassConstraint = "This year and/or it's terms have {0:class;classes} assigned to them, to delete this year please delete or assign the classes to a different academic year/term.";

        public const string AcademicScheduleTermDeletionConstraint =
            "This year has subjects assigned to it, to delete this year please delete or assign {0} to a different academic year/term.";
        public const string AcademicScheduleDeletion = DeletionMessage;
        public const string AcademicScheduleFromSchoolError = "This academic year and its holidays are from your school and cannot be edited.";

        public const string HolidayDeletion = DeletionMessage;

        public const string SubjectsScheduleFilterHelp =
            "By default your list of subjects is filtered to the current year and/or term. Subjects without a year/term assigned will be visible in all years and terms.";

        public const string SubjectScheduleHelpIndefinite = Scheduling.InTimetableIndefinitely;
        public const string SubjectScheduleHelpPeriodFormat = Scheduling.InTimetableDateRangeFormat;
	    public const string SubjectDeletion = "This subject and all associated classes, tasks and exams will be deleted.";
        public const string SubjectFromSchoolError = "This subject is from your school and cannot be edited.";
        public const string HolidayFromSchoolError = "This holiday is from your school and cannot be edited.";

        public const string ClassInputDateRangeFormat = "from {0:D} - {1:D}";
        public const string ClassInputHelpNoScheduleOrDates = "This class will exist in your timetable indefinitely unless a year/term is set or start/end dates are specified";
        public const string ClassInputHelpWithScheduleFormat = ClassInputHelpWithStartEndDatesFormat + " unless start/end dates are specified";
        public const string ClassInputHelpWithStartEndDatesFormat = "This class will exist in your timetable " + ClassInputDateRangeFormat;

        public const string Indefinitely = "indefinitely";

        public const string ValidationClassStartEndDatesIdentical = "That's a short class! The start and end dates cannot be the same, did you mean to create a one-off class instead?";
        public const string ValidationYearStartEndDatesIdentical = "That's a short academic year! The start and end dates cannot be the same.";
        public const string ValidationTermStartEndDatesIdentical = "That's a short academic term! The start and end dates cannot be the same.";

        public const string ValidationSubjectNoScheduleConflict = "A subject with that name already exists.";
        public const string ValidationSubjectWithTermConflict = "A subject with that name already exists for this term or year.";
        public const string ValidationSubjectWithYearConflict = "A subject with that name already exists for this year or one of it's terms.";
        public const string ValidationClassConflict = "One or more existing classes conflict with the one you're trying to enter.";
        public const string ValidationTaskConflict = "A task already exists for that subject with the entered title and due date.";
        public const string ValidationExamConflict = "One or more existing exams conflict with the one you're trying to enter.";

        public const string UnhandledErrorTitle = "Sorry, we couldn't complete that request";
        public const string UnhandledErrorMessage = "An unhandled error occured, sorry about that. Please try again in a few moments.";
        public const string OfflineError = "Looks like you're offline";
        public const string WelcomeWizardOfflineMessage = "Sorry, you must be online to setup your account. Please check your internet connection and try again.";
        public const string ClassSubscriptionOfflineMessage = "Sorry, you must be online to join or leave classes. Please check your internet connection and try again.";

        public const string ChangeAcademicSchedule = "change year / term";
        public const string NoAcademicScheduleText = "No year/term";

        public const string ReadOnly = "Read Only";

        public const string Done = "Done";
        public const string None = "None";
        public const string New = "New";
        public const string Edit = "Edit";
        public const string Ok = "Ok";
        public const string Leave = "Leave";

        public const string NewAcademicYear = "New Academic Year";
        public const string EditAcademicYear = "Edit Academic Year";

        public const string NewAcademicTerm = "New Academic Term";
        public const string EditAcademicTerm = "Edit Academic Term";

        public const string NewHoliday = "New Holiday";
        public const string EditHoliday = "Edit Holiday";

        public const string NewSubject = "New Subject";
        public const string EditSubject = "Edit Subject";

        public const string NewClass = "New Class";
        public const string EditClass = "Edit Class";

        public const string NewClassTime = "New Time";
        public const string EditClassTime = "Edit Time";

        public const string NewTask = "New Task";
        public const string EditTask = "Edit Task";

        public const string NewExam = "New Exam";
        public const string EditExam = "Edit Exam";

        public const string AcademicScheduleTaskExamInputHelp = "The selected year or term will determine which subjects appear in the dropdown.";
        public const string AcademicScheduleTaskExamListHelp = "By default your list of {0} is filtered to the current year and/or term. You can view a past or future year/term by changing the selection below.";

        public static string GetScheduleViewFilterHelp(bool hasClassesWithoutSchedule, bool prependFilter = true) {
            return (
                (prependFilter ? "Filter your classes and holidays by year and/or term. " : String.Empty) +
                "Classes assigned to a term will also appear when the term's year is selected. " +
                "Holidays are filtered to terms based on their dates." +
                (hasClassesWithoutSchedule ? " Classes without a year/term assigned will appear regardless of the selected year/term." : String.Empty)
            );
        }

        public const string Completed = "Completed";

        public const string Overdue = "Overdue";
        public const string Due = "Due";
        public const string DueToday = "Due Today";
        public const string DueTomorrow = "Due Tomorrow";
        public const string DueThisWeek = "Due This Week";
        public const string DueNextWeek = "Due Next Week";
        public const string DueLaterThisMonth = "Due Later This Month";

        public const string InTheNext3Days = "In The Next 3 Days";
        public const string InTheNext7Days = "In The Next 7 Days";
        public const string InTheNext14Days = "In The Next 14 Days";
        public const string LaterThisMonth = "Later This Month";

        public const string Filter_tasks = "Filter tasks...";

        public const string minutes = "minutes";

        public const string SyncingWithEllipsis = "Syncing...";
        public const string LastSyncedFormat = "Last synced: {0}";
        public const string never = "never";

        public const string OverdueText = "Overdue by {0}";
        public const string DueText = "{0} to complete";

        public const string NoClassesToday = "There are no classes or exams today.";

        public const string MissingSchedule = "Missing Schedule";
        public const string ContactITDepartment = "Contact your school's IT department for more information";
        public const string NotEnrolled = "It doesn't look like you are enrolled in any classes.";

        public const string SignInOAuthText = "Continue with {0}";

        public static class Scheduling {
            public const string InTimetableIndefinitely = "In timetable indefinitely";
            public const string InTimetableDateRangeFormat = "In timetable from {0:D} - {1:D}";
        }

        public static class AcademicYearInput {
            public const string TermStartDateBeforeScheduleFormat = "The start date of the term ({0:D}) cannot be before the start of the academic year ({1:D}).";
            public const string TermEndDateAfterScheduleFormat = "The end date of the term ({0:D}) cannot be after the end of the academic year ({1:D}).";
        }

        public static class SubjectInput {
            public const string AcademicScheduleInputHelp = "The selected year or term will determine how long this subject's classes will exist in your timetable for.";
            public const string AcademicScheduleListHelp =
                "If you have assigned subjects to academic years or terms, the selected year/term will filter the list of subjects to those with the selected year/term or without one assigned. " +
                "If no year/term is selected, all subjects are displayed.";
        }

        public static class ClassInput {
            public const string InputModuleStudent = "Module";
            public const string InputModuleTeacher = "Name / Module";

            public const string AcademicScheduleHelp = "The selected year or term will determine how long this class will exist in your timetable for and the subjects that appear in the dropdown.";

            public const string LeaveClassQuestion = "Leave class?";
            public const string LeaveConfirmation = "You will no longer see this class in your timetable.";

            public const string LeavingWithEllipsis = "Leaving...";

            public const string JoinWizardAcademicScheduleHelp = "The selected year or term will determine the classes which you can join.";

            public const string StartDateBeforeScheduleFormat = "The start date of the class ({0:D}) cannot be before the start of the class' academic {1} ({2:D}).";
            public const string EndDateAfterScheduleFormat = "The end date of the class ({0:D}) cannot be after the end of the class' academic {1} ({2:D}).";
        }

        public static class TimetableSettings {
            public const string StudentWithSchoolMessage =
                "Your timetable settings are managed by your school. If you think they may incorrect please ask one of your teachers for more information.";

            public const string TeacherWithSchoolMessage =
                "These settings are applicable to all teachers and students in {0}. Changing them may affect the classes created by other teachers and students.";
        }

        public static class Common {
            public const string Unknown = "unknown";

            public const string Okay = "Okay";
            public const string Save = "Save";
            public const string Cancel = "Cancel";
            public const string Yes = "Yes";
            public const string No = "No";
            public const string Disabled = "disabled";
            public const string Enabled = "enabled";
            public const string Days = "days";

            /// <value>Loading...</value>
            public const string LoadingWithEllipsis = "Loading...";
            public const string UpdatingWithEllipsis = "Updating...";
            public const string SigningInWithEllipsis = "Signing In...";
            public const string OfflineTapToRetry = "Offline. Tap to retry.";
            public const string ErrorTapToRetry = "An error occurred. Tap to retry.";
        }

        public static class RelativeTime {
            public const string Future = "in {0}";
            public const string Past = "{0} ago";
            public const string Seconds = "a few seconds";
            public const string Minute = "a minute";
            public const string Minutes = "{0} minutes";
            public const string Hour = "an hour";
            public const string Hours = "{0} hours";
            public const string OneDay = "1 day";
            public const string Day = "a day";
            public const string Days = "{0} days";
            public const string Month = "a month";
            public const string Months = "{0} months";
            public const string Year = "a year";
            public const string Years = "{0} years";

            public const string Tomorrow = "tomorrow";
            public const string Today = "today";
            public const string Yesterday = "yesterday";
        }

        public static class Network {
            public const string MeteredConnectionTitle = "Metered Connection";
            public const string MeteredConnectionMessage = "Your current network connection indicates increased costs. Synchronization is {0}. You can change this in preferences.";
        }
    }
}
