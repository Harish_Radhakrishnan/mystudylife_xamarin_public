﻿using System;
using System.Threading.Tasks;

namespace MyStudyLife.UI.Services {
    public interface IDialogService {
        void RequestConfirmation(string title, string message, Action onConfirm);
        void RequestConfirmation(string title, string message, ConfirmationType confirmationType, Action onConfirm);

        Task<ConfirmationDialogResult> RequestConfirmationAsync(string title, string message);
        Task<ConfirmationDialogResult> RequestConfirmationAsync(string title, string message, ConfirmationType confirmationType);

        /// <summary>
        ///     Shows a blocking dismissible message box.
        /// </summary>
        void ShowDismissible(string title, string message);

        /// <summary>
        ///     Shows a non-blocking notification - for example
        ///     a toast notification in Windows 8 / Phone.
        /// </summary>
        void ShowNotification(string title, string message);

	    void ShowCustom(string title, string message, CustomDialogAction positiveAction, CustomDialogAction negativeAction);

        Task<CustomDialogResult> ShowCustomAsync(string title, string message, string positiveAction, string negativeAction);
    }

    public enum ConfirmationType {
        YesNo,
        Discard,
        Delete
    }

    public class ConfirmationDialogResult {
        public bool DidConfirm { get; private set; }

        public ConfirmationDialogResult(bool didConfirm) {
            this.DidConfirm = didConfirm;
        }
    }

    public class CustomDialogResult {
        public bool ChosePositiveAction { get; private set; }

        public CustomDialogResult(bool positiveAction) {
            this.ChosePositiveAction = positiveAction;
        }
    }
}