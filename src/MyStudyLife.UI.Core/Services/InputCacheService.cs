﻿using System;

namespace MyStudyLife.UI.Services {
    public class InputCacheService<TInputItem> : IInputCacheService<TInputItem> where TInputItem : class {

		// TODO: Add storage to file for tombstoning.
        private static TInputItem CachedItem { get; set; }

		#region Implementation of IInputCacheService<Class>

        public TInputItem GetItem() {
			if (CachedItem == null)
				throw new Exception("Cached input item not found.");

			return CachedItem;
		}

        public bool TryGetItem(out TInputItem item) {
			item = CachedItem;

			return item != null;
		}

        public void SetItem(TInputItem item) {
			if (item == null)
				throw new ArgumentNullException(nameof(item), "Item cannot be null, to remove the cached item use InputCacheService<T>.RemoveItem()");

			CachedItem = item;
		}

		public void RemoveItem() {
			CachedItem = null;
		}

		#endregion
	}
}
