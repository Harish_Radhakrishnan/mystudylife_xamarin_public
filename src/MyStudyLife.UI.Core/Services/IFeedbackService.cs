﻿namespace MyStudyLife.UI.Services {
	public interface IFeedbackService {
		void CheckAndShowReviewView(AppStartMode mode);
	}
}
