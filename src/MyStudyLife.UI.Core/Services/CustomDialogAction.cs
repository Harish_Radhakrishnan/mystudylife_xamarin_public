﻿using System;

namespace MyStudyLife.UI.Services {
	public sealed class CustomDialogAction {
		/// <summary>
		///		The action's content. eg. "no thanks"
		/// </summary>
		public string Content { get; set; }

		/// <summary>
		///		An action which is called when the 
		///		action is chosen/tapped/clicked.
		/// </summary>
		public Action OnChosen { get; set; }

		public bool IsDefault { get; set; }

		public CustomDialogAction(string content) {
			this.Content = content;
		}

		public CustomDialogAction(string content, Action onChosen) : this(content) {
			this.OnChosen = onChosen;
		}
        
        public CustomDialogAction(string content, Func<System.Threading.Tasks.Task> onChosen) : this(content) {
            this.OnChosen = () => onChosen().Start();
        }

		public void TryTriggerOnChosen() {
            OnChosen?.Invoke();
        }
	}
}
