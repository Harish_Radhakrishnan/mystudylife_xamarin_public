﻿using System;
using System.Threading.Tasks;

namespace MyStudyLife.UI.Services {
    public abstract class DialogServiceBase {
        public void RequestConfirmation(string title, string message, Action onConfirm) {
            RequestConfirmation(title, message, ConfirmationType.YesNo, onConfirm);
        }
        public void RequestConfirmation(string title, string message, ConfirmationType confirmationType, Action onConfirm) {
            RequestConfirmationImpl(title, message, confirmationType, confirmed => (confirmed ? onConfirm : null)?.Invoke());
        }

        public Task<ConfirmationDialogResult> RequestConfirmationAsync(string title, string message) {
            return RequestConfirmationAsync(title, message, ConfirmationType.YesNo);
        }

        public Task<ConfirmationDialogResult> RequestConfirmationAsync(string title, string message, ConfirmationType confirmationType) {
            var tcs = new TaskCompletionSource<ConfirmationDialogResult>();

            RequestConfirmationImpl(
                title, message, confirmationType,
                confirmed => tcs.TrySetResult(new ConfirmationDialogResult(confirmed))
            );

            return tcs.Task;
        }

        protected virtual void RequestConfirmationImpl(string title, string message, ConfirmationType confirmationType, Action<bool> onActioned) {
            var buttonText = GetConfirmationDialogButtonText(confirmationType);

            RequestConfirmationImpl(title, message, buttonText.Cancel, buttonText.Confirm, onActioned);
        }

        protected abstract void RequestConfirmationImpl(string title, string message, string cancelText, string confirmText, Action<bool> onActioned);

        protected virtual ConfirmationDialogButtonText GetConfirmationDialogButtonText(ConfirmationType confirmationType) {
            string confirmText = R.Common.Yes;
            string cancelText = R.Common.No;

            if (confirmationType != ConfirmationType.YesNo) {
                cancelText = R.Common.Cancel;

                switch (confirmationType) {
                    case ConfirmationType.Discard:
                        confirmText = R.Discard;
                        break;
                    case ConfirmationType.Delete:
                        confirmText = R.Delete;
                        break;
                }
            }

            return new ConfirmationDialogButtonText(confirmText, cancelText);
        }

        public struct ConfirmationDialogButtonText {
            public string Confirm { get; }
            public string Cancel { get; }

            public ConfirmationDialogButtonText(string confirm, string cancel) {
                this.Confirm = confirm;
                this.Cancel = cancel;
            } 
        }
    }
}
