﻿using System;
using System.Text;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.UI.Tasks;
using MyStudyLife.Utility;

namespace MyStudyLife.UI.Services {
    // TODO: Refactor dialog showing. This class should just handle whether to show
    // the dialog and which type, not the showing itself.
	public class FeedbackService : RequireStorageProviderBase, IFeedbackService {
		#region Config

		private const int FirstChancePromptStartCount = 5;
		private const int LastChancePromptStartCount = 10;

		#endregion

		private readonly IDialogService _dialogService;
		private readonly IDeviceInfoProvider _deviceInfoProvider;
		private readonly ITaskProvider _taskProvider;

        #region Tracking

        private const string StartCountKey = "MSL_APP_START_COUNT";
		private const string LastStartDateKey = "MSL_APP_LAST_START_DATE";
		private const string ReviewedKey = "MSL_APP_REVIEW_OR_FEEDBACK";

		private int? _startCount;
		private DateTime? _lastStartDate;
		private bool? _hasReviewOrFeedback;

		private int AppStartCount {
			get { return GetCachedSetting(StartCountKey, ref _startCount); }
			set { SetCachedSetting(StartCountKey, ref _startCount, value); }
		}

		private DateTime AppLastStartDate {
			get { return GetCachedSetting(LastStartDateKey, ref _lastStartDate); }
			set { SetCachedSetting(LastStartDateKey, ref _lastStartDate, value); }
		}

		private bool HasReviewOrFeedback {
			get { return GetCachedSetting(ReviewedKey, ref _hasReviewOrFeedback); }
			set { SetCachedSetting(ReviewedKey, ref _hasReviewOrFeedback, value); }
		}

		private T GetCachedSetting<T>(string key, ref T? store) 
			where T : struct {

			if (!store.HasValue) {
				store = this.StorageProvider.GetSetting<T>(key);
			}

			return store.Value;
		}

		private void SetCachedSetting<T>(string key, ref T? store, T value)
			where T : struct {

			if (!store.HasValue || !store.Value.Equals(value)) {
				this.StorageProvider.AddOrUpdateSetting(key, value);

				store = value;
			}
		}

		#endregion

		public FeedbackService(
            IStorageProvider storageProvider,
            IDialogService dialogService,
            IDeviceInfoProvider deviceInfoProvider,
            ITaskProvider taskProvider
        ) : base(storageProvider) {
            this._dialogService = dialogService;
            this._deviceInfoProvider = deviceInfoProvider;
            this._taskProvider = taskProvider;
        }
		
		public void CheckAndShowReviewView(AppStartMode mode) {
			if (mode == AppStartMode.Resume) return;
            
#if DEBUG
			//HasReviewOrFeedback = false;

			//AppStartCount = LastChancePromptStartCount - 1;
#endif

		    var today = DateTimeEx.Today;

			// Don't even bother incrementing after the LastChancePromptStartCount.
			if (AppStartCount >= LastChancePromptStartCount || HasReviewOrFeedback) return;
            
            // Otherwise someone will say no and get prompted every time they open
            // the app in the same day!
            if (AppLastStartDate.Date == today) return;

			// We don't want to piss off our users so only increment
			// once per day.
            if (AppLastStartDate.Date < today) {
                AppStartCount++;
			}

            AppLastStartDate = today;

			if (AppStartCount == FirstChancePromptStartCount) {
				PromptForReview(PromptType.FirstChance);
			}
			else if (AppStartCount == LastChancePromptStartCount) {
				PromptForReview(PromptType.LastChance);
			}
		}

		private void PromptForReview(PromptType type) {
			string message;

			if (type == PromptType.FirstChance) {
				message = "We'd love to have some feedback on how you're finding My Study Life.";
			}
			else {
				message = String.Concat(
					"You've been using My Study Life for a few days now, we'd love to have some feedback on how you're finding it.",
					Environment.NewLine,
					Environment.NewLine,
					"This is the last time we'll ask :)"
				);
			}

			this._dialogService.ShowCustom(
				"How are you getting on?",
				message,
				new CustomDialogAction("Rate 5 Stars", () => {
					HasReviewOrFeedback = true;

                    this._taskProvider.ReviewApp();
				}),
				new CustomDialogAction("No Thanks", () => PromptForFeedback(type))
			);
		}

		private void PromptForFeedback(PromptType type) {
            this._dialogService.ShowCustom(
				"Would you like to send some feedback instead?",
				String.Concat(
					"Sorry to hear you didn't want to rate My Study Life.",
					Environment.NewLine,
					Environment.NewLine,
					"Tell us about your experience or suggest how we can make My Study Life better."
				),
				new CustomDialogAction("Send Feedback", SendFeedback),
				new CustomDialogAction(type == PromptType.FirstChance ? "Not Yet" : "No Thanks")
			);
		}

		private async void SendFeedback() {
			var device = await this._deviceInfoProvider.GetDeviceInformationAsync();
			
			var bodyBuilder = new StringBuilder();

			bodyBuilder.AppendLine("Hi My Study Life Team,");
			bodyBuilder.AppendLine();
			bodyBuilder.AppendLine("[[Add your feedback here]]");
			bodyBuilder.AppendLine();
			bodyBuilder.AppendLine("-------------------------");
			bodyBuilder.AppendLine("Device: " + (device.Manufacturer ?? String.Empty) + " " + (device.Model ?? String.Empty));
			bodyBuilder.AppendLine("OS Version: " + (device.OSVersion ?? "Unknown"));
			bodyBuilder.AppendLine("-------------------------");

			this._taskProvider.ShowEmailComposer(
				"My Study Life Feedback",
				bodyBuilder.ToString(),
				MslConfig.Current.SupportEmailAddress
			);
		}

		private enum PromptType {
			FirstChance,
			LastChance
		}
	}
}
