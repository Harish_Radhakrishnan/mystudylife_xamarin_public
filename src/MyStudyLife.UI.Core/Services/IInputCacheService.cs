﻿namespace MyStudyLife.UI.Services {
	/// <summary>
	///		Service to cache input items
	///		whilst navigating between screens
	///		(or when the app is tombstoned).
	/// </summary>
	public interface IInputCacheService<TInputItem>
		where TInputItem : class {

		TInputItem GetItem();

		bool TryGetItem(out TInputItem item);

		void SetItem(TInputItem item);

		void RemoveItem();
	}
}
