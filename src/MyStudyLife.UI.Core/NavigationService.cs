﻿using System;
using System.Collections.Generic;
using System.Threading;
using MvvmCross.Core;
using MvvmCross.IoC;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MyStudyLife.Data;
using MyStudyLife.Scheduling;
using MyStudyLife.UI.ViewModels;
using MyStudyLife.UI.ViewModels.Schools;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.UI.ViewModels.Wizards;

namespace MyStudyLife.UI {
    public class NavigationService : MvxNavigationService, INavigationService {
        public NavigationBackStack BackStack { get; } = new NavigationBackStack();

        public NavigationService(IMvxViewModelLoader viewModelLoader,IMvxViewDispatcher mvxViewDispatcher, IMvxIoCProvider mvxIoCProvider )
            : base(viewModelLoader , mvxViewDispatcher, mvxIoCProvider) { }

        public async System.Threading.Tasks.Task<bool> Navigate<TViewModel>(object parameterValuesObject) where TViewModel : IMvxViewModel {
            return await Navigate(typeof(TViewModel), parameterValuesObject);
        }

        /// <summary>
        ///     Backwards compat for view model pattern prior to MvvmCross 6 - Init(object) methods.
        /// </summary>
        public System.Threading.Tasks.Task<bool> Navigate(Type viewModelType, object parameterValuesObject) {
            return Navigate(viewModelType, parameterValuesObject, null);
        }

        /// <summary>
        ///     Backwards compat for view model pattern prior to MvvmCross 6 - Init(object) methods.
        /// </summary>
        public async System.Threading.Tasks.Task<bool> Navigate(Type viewModelType, object parameterValuesObject, IDictionary<string, string> presentationValues) {
            var request = new MvxViewModelInstanceRequest(viewModelType) {
                ParameterValues = parameterValuesObject?.ToSimplePropertyDictionary()
            };
            request.ViewModelInstance = ViewModelLoader.LoadViewModel(request, null);
            return await Navigate(
                request,
                request.ViewModelInstance,
                presentationValues != null ? new MvxBundle(presentationValues) : null,
                CancellationToken.None
            ).ConfigureAwait(false);
        }

        public void ViewEntry(AgendaEntry entry) {
            var request = GetRequest(entry);

            Navigate(request.ViewModelType, request.ParameterValues)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
        }

        public void ViewEntity<T>(T entity) where T : SubjectDependentEntity {
            var request = GetRequest(entity);

            Navigate(request.ViewModelType, request.ParameterValues)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
        }

        public MvxViewModelRequest GetRequest<T>(T entity)
            where T : SubjectDependentEntity {
            Type viewModel;

            if (entity is Class) {
                viewModel = typeof(ClassViewModel);
            }
            else if (entity is Task) {
                viewModel = typeof(TaskViewModel);
            }
            else if (entity is Exam) {
                viewModel = typeof(ExamViewModel);
            }
            else {
                throw new NotSupportedException("Given type is not yet supported, you have some work to do!");
            }

            return new MvxViewModelRequest {
                ViewModelType = viewModel,
                ParameterValues = new EntityViewModel<T>.NavObject {
                    Guid = entity.Guid
                }.ToSimplePropertyDictionary()
            };
        }

        public MvxViewModelRequest GetRequest(AgendaEntry entry) {
            if (entry.Type == AgendaEntryType.Class) {
                var sc = (ScheduledClass) entry.Base;

                var navObject = new ClassViewModel.ScheduledNavObject {
                    Guid = sc.ClassGuid,
                    Date = sc.Date
                };

                if (sc.ClassTime != null) {
                    navObject.TimeGuid = sc.ClassTime.Guid;
                }

                return new MvxViewModelRequest {
                    ViewModelType = typeof(ClassViewModel),
                    ParameterValues = navObject.ToSimplePropertyDictionary()
                };
            }
            else if (entry.Type == AgendaEntryType.Exam) {
                return GetRequest((Exam) entry.Base);
            }

            throw new NotSupportedException("Given type is not yet supported, you have some work to do!");
        }


        /// <summary>
        ///     Returns view model type for the first view when the user has
        ///     been authenticated.
        /// </summary>
        /// <param name="user">
        ///     The user to determine the first view for, if null we assume
        ///     not authenticated.
        /// </param>
        public virtual Type GetFirstViewAuthorized(User user) {
            Type viewModelType;

            if (user == null) {
                viewModelType = typeof(SignInViewModel);
            }
            else if (user.SetupAt.HasValue) {
                viewModelType = typeof(DashboardViewModel);
            }
            else if (user.IsStudentWithManagedSchool) {
                viewModelType = typeof(SchoolWelcomeViewModel);
            }
            else {
#pragma warning disable CS0618 // Type or member is obsolete
                viewModelType = typeof(WelcomeWizardViewModel);
#pragma warning restore CS0618 // Type or member is obsolete
            }

            return viewModelType;
        }

        public async System.Threading.Tasks.Task<bool> NavigateFirstViewAuthorized(User user) {
            return await Navigate(GetFirstViewAuthorized(user));
        }
    }
}