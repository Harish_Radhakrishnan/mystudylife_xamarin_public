﻿using MvvmCross;
using MyStudyLife.Data;
using MyStudyLife.Net;
using System;
using System.Collections.Generic;
using System.Net.Http;
using MvvmCross.Plugin.File;
using MyStudyLife.Utility;

namespace MyStudyLife.UI.Caching {
    /// <summary>
    ///     A local cache designed for files which are always required 
    ///     for the app (such as profile picture) and don't expire
    ///     based on date (so they don't disappear when the user is offline),
    ///     but are updated when online.
    /// </summary>
    public class RemoteAssetCache : MvxLockableObject, IRemoteAssetCache {
        // TODO: Move http requests to a queue?

        private const string Folder = @"cache\remote-assets";

        private readonly IMvxFileStore _fileStore;
        private readonly HttpClient _httpClient;

        // Entries which have been loaded during this app session.
        private readonly Dictionary<string, EntryInfo> _sessionEntries = new Dictionary<string, EntryInfo>();

        public RemoteAssetCache(IMvxFileStore fileStore, IHttpClientFactory httpClientFactory) {
            _fileStore = fileStore;
            _httpClient = httpClientFactory.Get();
        }
        
        // TODO: If the remoteUrl has changed but the key has not this may cause issues with the lastModified http check.
        public void RequestFile(string key, string remoteUrl, Action<string> onFile, Action onError) {
            Check.NotNull(key, nameof(key));
            Check.NotNull(remoteUrl, nameof(remoteUrl));

            RunAsyncWithLock(() => {
                _fileStore.EnsureFolderExists(Folder);

                var path = System.IO.Path.Combine(Folder, key);
                bool fileExists = _fileStore.Exists(path);

                if (fileExists) {
                    onFile(_fileStore.NativePath(path));
                }

                EntryInfo entry;

                if (!_sessionEntries.TryGetValue(key, out entry)) {
                    _sessionEntries[key] = entry = new EntryInfo();
                }

                if (!entry.CheckedForUpdate) {
                    DateTimeOffset? lastModified = fileExists ? (DateTimeOffset?)Mvx.IoCProvider.Resolve<IStorageProvider>().GetLastModified(path) : null;

                    var request = new HttpRequestMessage(HttpMethod.Get, remoteUrl) {
                        Headers = {
                            IfModifiedSince = lastModified
                        }
                    };

                    _httpClient.SendAsync(request).ContinueWith(async task => {
                        try {
                            var response = task.Result;

                            if (response.StatusCode == System.Net.HttpStatusCode.NotModified) {
                                entry.CheckedForUpdate = true;
                            }
                            else if (response.IsSuccessStatusCode) {
                                string downloadPath = path + ".tmp";

                                using (var rs = await response.Content.ReadAsStreamAsync())
                                using (var ws = _fileStore.OpenWrite(downloadPath)) {
                                    var buffer = new byte[4 * 1024];
                                    int count;

                                    while ((count = rs.Read(buffer, 0, buffer.Length)) > 0) {
                                        ws.Write(buffer, 0, count);
                                    }
                                }

                                // We would use TryMove(,,true) but there is a bug in the Windows
                                // implementation which doesn't delete the file! 
                                try {
                                    _fileStore.DeleteFile(path);
                                }
                                catch { }

                                if (_fileStore.TryMove(downloadPath, path, false)) {
                                    onFile(_fileStore.NativePath(path));
                                }
                            }
                            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound) {
                                try {
                                    _fileStore.DeleteFile(path);
                                }
                                catch { }

                                onError();
                            }
                            else if (!fileExists) {
                                onError();
                            }
                        }
                        catch (Exception) {
                            onError();
                        }
                    });
                }
            });
        }

        class EntryInfo {
            public bool CheckedForUpdate { get; set; }
        }
    }
}