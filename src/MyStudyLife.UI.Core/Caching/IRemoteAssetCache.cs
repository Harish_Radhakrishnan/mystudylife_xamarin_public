﻿using System;

namespace MyStudyLife.UI.Caching {
    public interface IRemoteAssetCache {
        void RequestFile(string key, string remoteUrl, Action<string> onFile, Action onError);
    }
}
