﻿using System;
using System.Collections.Generic;
using MvvmCross;
using MvvmCross.Plugin.Messenger;

namespace MyStudyLife.UI.Reactive {
    public class TimerService : ITimerService {
        private readonly IMvxMessenger _messenger;
        private readonly Dictionary<TimeUnit, ITimer> _timers = new Dictionary<TimeUnit, ITimer>();

        public TimerService(IMvxMessenger messenger) {
            this._messenger = messenger;
        }

        public void RequireMessageEvery(TimeUnit unit) {
            if (!_timers.ContainsKey(unit)) {
                var t = _timers[unit] = Mvx.IoCProvider.Resolve<ITimer>();

                TimeSpan timeUntilRoundUnit;
                TimeSpan timeBetweenCallbacks;

                var now = DateTimeEx.Now;

                switch (unit) {
                    case TimeUnit.Second:
                        timeUntilRoundUnit = TimeSpan.FromMilliseconds(1000 - now.Millisecond);
                        timeBetweenCallbacks = TimeSpan.FromSeconds(1);
                        break;
                    case TimeUnit.Minute:
                        timeUntilRoundUnit = TimeSpan.FromSeconds(60 - now.Second);
                        timeBetweenCallbacks = TimeSpan.FromMinutes(1);
                        break;
                    case TimeUnit.Hour:
                        timeUntilRoundUnit = TimeSpan.FromMinutes(60 - now.Minute);
                        timeBetweenCallbacks = TimeSpan.FromHours(1);
                        break;
                    case TimeUnit.Day:
                        timeUntilRoundUnit = TimeSpan.FromHours(24 - now.Hour);
                        timeBetweenCallbacks = TimeSpan.FromDays(1);
                        break;
                    default:
                        throw new NotSupportedException("Unknown unit '" + unit + "'");
                }

                t.Start(() => TimerCallback(unit), timeUntilRoundUnit, timeBetweenCallbacks);
            }
        }

        private void TimerCallback(TimeUnit unit) {
            _messenger.Publish(new TimerCallbackMessage(this, unit));
        }
    }
}
