﻿using MvvmCross.Plugin.Messenger;

namespace MyStudyLife.UI.Reactive {
    public class TimerCallbackMessage : MvxMessage {
        public TimeUnit Unit { get; private set; }

        public TimerCallbackMessage(object sender, TimeUnit unit) : base(sender) {
            this.Unit = unit;
        }
    }
}
