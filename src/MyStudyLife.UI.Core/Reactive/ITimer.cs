﻿using System;

namespace MyStudyLife.UI.Reactive {
    public interface ITimer : IDisposable {
        ITimer Start(Action callback, TimeSpan startIn, TimeSpan repeatEvery);

        void Change(TimeSpan startIn, TimeSpan repeatEvery);
    }
}
