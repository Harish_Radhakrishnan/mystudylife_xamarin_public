﻿namespace MyStudyLife.UI.Reactive {
    public interface ITimerService {
        void RequireMessageEvery(TimeUnit unit);
    }

    public enum TimeUnit {
        Second,
        Minute,
        Hour,
        Day
    }
}
