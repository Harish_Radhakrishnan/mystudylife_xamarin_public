My Study Life for Mobile Devices
==============
Android, iPhone and core libraries. Windows 8, Windows Phone also present in the repository, but no longer maintained (excluded from the `.sln`, do not compile).

## Getting Started ##
### Prerequisities ###
- Xamarin.Android 12
- Xamarin.iOS 15
- MacOS with Visual Studio 2019 for Mac or Rider
- Xcode 13.2.1
- Xcode command line tools
- Android SDK 32

### Configuration ###
The applications share a single base configuration, `MslConfig`, which is inherited and overridden / extended on each platform as appropriate (`iOSConfig`, `DroidConfig`).

During development, configuration changes can be made to any of these files. Care should be taken not to commit changes unless intended.

### Compiled Dependencies
The application contains a number of compiled dependencies, stored in the `lib` directory. These include forks with additional functionality, and bindings for native Android and iOS libraries.

| Library     | Type        |
| ----------- | ----------- |
| [SQLite-Net](https://dev.azure.com/mystudylife/mystudylife/_git/sqlite-net) | [Fork](https://github.com/praeclarum/sqlite-net) |
| [SQLite-Net-Extensions](https://dev.azure.com/mystudylife/_git/sqlite-net-extensions) | [Fork](https://bitbucket.org/twincoders/sqlite-net-extensions/src) |
| [Android - AnderWeb.DiscreteSeekBar](https://github.com/mystudylife/xamarin-discreteseekbar) | [Android Binding](https://github.com/mystudylife/discreteSeekBar) |
| [Android - Timehop.StickyHeadersRecyclerView](https://github.com/mystudylife/xamarin-stickyheadersrecyclerview) | [Android Binding](https://github.com/timehop/sticky-headers-recyclerview) |
| [iOS - BTProgressHud](https://github.com/mystudylife/BTProgressHUD) | [iOS Binding](https://github.com/OSVProgressHUD/SVProgressHUD) |
| [iOS - CrossUI](https://github.com/MvvmCross/MvvmCross/tree/4.4.0/CrossUI) | Deprecated Library* |
| [iOS - MaterialControls](https://github.com/mystudylife/xamarin-iosmaterialcontrols) | [iOS Binding](https://github.com/mystudylife/Material-Controls-For-iOS) |
| [iOS - WYPopover](https://github.com/mystudylife/xamarin-wypopovercontroller) | [iOS Binding](https://github.com/nicolaschengdev/WYPopoverController) |

#### *iOS - CrossUI
This library, originally part of MvvmCross, allows for easy creation of table cell based UI. It was removed from the MvvmCross repository after V4.4 and support dropped but forms a large part of the My Study Life iOS UI, including the settings and input screens. The library can be found under the V4.4 tag on the MvvmCross repository. CrossUI.Core.dll and CrossUI.iOS.dll are compiled from this. MvvmCross specific functionality such as binding is included in the project under `src/MyStudyLife.iOS/Dialog`.

## Notes ##
Any changes to `Resource.Designer.cs` in `MyStudyLife.Droid` are [ignored](http://blog.pagebakers.nl/2009/01/29/git-ignoring-changes-in-tracked-files/) but the file still remains in the repo so it doesn't have to be recreated manually.

As of version 3.0, the iOS app depends on "native" libraries that are written using Swift requiring the library `Xamarin.Swift`. The `ipa` must be built with a root `SwiftSupport` directory, which is handled using `auto_package_ipa.sh`.

### Testing ###
We use XUnit and Moq for testing. **All new code should have associated unit tests**, and areas of code which are not currently tested should have tests added when code is modified.

Test classes should be named in the format `{ClassToTest}Test`. For example `AgendaScheduler` has a test class named `AgendaSchedulerTest`.

Tests should be named in the format `{Method}_Should{DoSomething}_When{SomethingHappens}`. Both the `{Method}` and `_When` parts can be ommited when appropriate. For example:

- Validators do not have method names, so they appear in the format `ShouldBeInvalid_WhenNameIsEmpty`.
- When testing for a successful result, `_When` might not be required unless there are multiple tests for success. In this case something such as `Delete_ShouldDeleteExam` is fine. 

Tests should follow the arrange, act and assert pattern. There should be a single test per condition (tests can contain multiple asserts). For example, `GetRotationWeekForDate_ShouldPushSchedule_WhenHolidayWithPushesSchedule` is does not cover conditions where the holiday may not push a schedule, so these should be a separate test.

### Commit Messages ###
- Use the present tense ("Fix bug" not "Fixed bug")
- Use the imperative mood ("Disable shadow on Android......" not "Disables shadow on Android ...")
- Limit the first line to 72 characters or less (treat emojis as a single character)
- Reference JIRA issues

## Publishing ##
Publishing is handled by the pipelines, `azure-pipelines-android.yml` and `azure-pipelines-ios.yml`.

### Versioning ###
The applications are versioned using the major, minor and patch versions specified in the `.yml` pipeline files. They must be manually incremented for the display version to change.

The applications also have a binary version, which differs from the display version:
- iOS - the revision is automatically incremented (major.minor.patch.revision).
- Android - the version code is automatically incremented (integer).

## Troubleshooting ##
### Android ###

#### No resource found that matches the given name 'RESOURCE' ####
Go to your Android SDK installed directory then:

> extras > android > support > v7 > appcompat

Once you are in appcompat folder, open the `project.properties` file then change the value from default 19 to the latest SDK version (eg target=android-24). Save the file, clean and rebuild.

### "java.exe" exited with code 1 when compiling for release ###
Usually indicates Proguard needs updating. Can confirm by setting the verbosity of the build to **Detailed**. Latest versions of Proguard can be found on [sourceforge](https://sourceforge.net/projects/proguard/files/) and should be copied over the files in `%ANDROIDSDK%\tools\proguard`.
