﻿using Moq;
using MyStudyLife.Data;
using System.IO;
using Xunit;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Core.Tests.Data {
    public class UserStoreTest : TestBase {
        const string USER_FILE_PATH = "User.json";

        public UserStoreTest(ITestOutputHelper output) : base(output) { }

        /// <remarks>
        ///     This is something we've spotted in Raygun (MSL-499) via a
        ///     JsonReaderException, which correlates in the source of Json.Net
        ///     to an empty file. This should never really be the case, but it
        ///     does occur so we need to handle it.
        /// </remarks>
        [Fact]
        public void GetUserAsync_ShouldReturnNull_WhenEmptyStream() {
            var storageProviderMock = new Mock<IStorageProvider>();
            storageProviderMock
                .Setup(x => x.FileOpenReadAsync(USER_FILE_PATH))
                .ReturnsAsync(new MemoryStream());

            var userStore = new UserStore(storageProviderMock.Object);

            Assert.Null(userStore.GetCurrentUser());
        }
    }
}
