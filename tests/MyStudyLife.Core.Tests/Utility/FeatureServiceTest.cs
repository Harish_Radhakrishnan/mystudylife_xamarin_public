﻿using MyStudyLife.Data;
using MyStudyLife.Utility;
using System;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Core.Tests.Utility {
    public class FeatureServiceTest : TestBase {
        public FeatureServiceTest(ITestOutputHelper output) : base(output) { }

        private async Task<bool> UserCanUseFeatureAsync(Feature feature, Action<User> user = null) {
            await EnsureInitializedAsync();

            var userStore = await SetupMockUserAsync(user);

            var featureService = new FeatureService(userStore);
            return await featureService.CanUseFeatureAsync(feature);
        }

        private Task<bool> IndividualCanUseFeatureAsync(Feature feature)
            => UserCanUseFeatureAsync(feature);

        private Task<bool> UserWithUnmanagedSchoolCanUseFeatureAsync(Feature feature)
            => UserCanUseFeatureAsync(feature, u => {
                u.School = new MyStudyLife.Data.Schools.School {
                    Id = 1,
                    Name = "School of Test",
                    IsManaged = false,
                    ClassSharingEnabled = true
                };
            });

        private Task<bool> UserWithManagedSchoolCanUseFeatureAsync(Feature feature)
            => UserCanUseFeatureAsync(feature, u => {
                u.School = new MyStudyLife.Data.Schools.School {
                    Id = 1,
                    Name = "School of Test",
                    IsManaged = true
                };
            });

        #region WriteUserProfile

        [Fact]
        public async AsyncTask WriteUserProfile_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canWriteUserProfile = await UserWithManagedSchoolCanUseFeatureAsync(Feature.WriteUserProfile);

            Assert.False(canWriteUserProfile);
        }


        [Fact]
        public async AsyncTask WriteUserProfile_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canWriteUserProfile = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.WriteUserProfile);

            Assert.True(canWriteUserProfile);
        }

        [Fact]
        public async AsyncTask WriteUserProfile_ShouldBeTrue_WhenUserDoesNotHaveSchool() {
            var canWriteUserProfile = await IndividualCanUseFeatureAsync(Feature.WriteUserProfile);

            Assert.True(canWriteUserProfile);
        }

        #endregion

        #region WriteAcademicYears

        [Fact]
        public async AsyncTask WriteAcademicYears_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canWriteAcademicYears = await UserWithManagedSchoolCanUseFeatureAsync(Feature.WriteAcademicYears);

            Assert.False(canWriteAcademicYears);
        }


        [Fact]
        public async AsyncTask WriteAcademicYears_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canWriteAcademicYears = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.WriteAcademicYears);

            Assert.True(canWriteAcademicYears);
        }

        [Fact]
        public async AsyncTask WriteAcademicYears_ShouldBeTrue_WhenUserDoesNotHaveSchool() {
            var canWriteAcademicYears = await IndividualCanUseFeatureAsync(Feature.WriteAcademicYears);

            Assert.True(canWriteAcademicYears);
        }

        #endregion

        #region WriteAcademicYears

        [Fact]
        public async AsyncTask WriteHolidays_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canWriteHolidays = await UserWithManagedSchoolCanUseFeatureAsync(Feature.WriteHolidays);

            Assert.False(canWriteHolidays);
        }


        [Fact]
        public async AsyncTask WriteHolidays_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canWriteHolidays = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.WriteHolidays);

            Assert.True(canWriteHolidays);
        }

        [Fact]
        public async AsyncTask WriteHolidays_ShouldBeTrue_WhenUserDoesNotHaveSchool() {
            var canWriteHolidays = await IndividualCanUseFeatureAsync(Feature.WriteHolidays);

            Assert.True(canWriteHolidays);
        }

        #endregion

        #region WriteSubjects

        [Fact]
        public async AsyncTask WriteSubjects_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canWriteSubjects = await UserWithManagedSchoolCanUseFeatureAsync(Feature.WriteSubjects);

            Assert.False(canWriteSubjects);
        }


        [Fact]
        public async AsyncTask WriteSubjects_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canWriteSubjects = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.WriteSubjects);

            Assert.True(canWriteSubjects);
        }

        [Fact]
        public async AsyncTask WriteSubjects_ShouldBeTrue_WhenUserDoesNotHaveSchool() {
            var canWriteSubjects = await IndividualCanUseFeatureAsync(Feature.WriteSubjects);

            Assert.True(canWriteSubjects);
        }

        #endregion

        #region WriteClasses

        [Fact]
        public async AsyncTask WriteClasses_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canWriteClasses = await UserWithManagedSchoolCanUseFeatureAsync(Feature.WriteClasses);

            Assert.False(canWriteClasses);
        }


        [Fact]
        public async AsyncTask WriteClasses_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canWriteClasses = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.WriteClasses);

            Assert.True(canWriteClasses);
        }

        [Fact]
        public async AsyncTask WriteClasses_ShouldBeTrue_WhenUserDoesNotHaveSchool() {
            var canWriteClasses = await IndividualCanUseFeatureAsync(Feature.WriteClasses);

            Assert.True(canWriteClasses);
        }

        #endregion

        #region JoinClasses

        [Fact]
        public async AsyncTask JoinClasses_ShouldBeFalse_WhenUserHasManagedSchool() {
            var canJoinClasses = await UserWithManagedSchoolCanUseFeatureAsync(Feature.JoinClasses);

            Assert.False(canJoinClasses);
        }


        [Fact]
        public async AsyncTask JoinClasses_ShouldBeTrue_WhenUserHasUnmanagedSchool() {
            var canJoinClasses = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.JoinClasses);

            Assert.True(canJoinClasses);
        }

        [Fact]
        public async AsyncTask JoinClasses_ShouldBeFalse_WhenUserDoesNotHaveSchool() {
            var canJoinClasses = await IndividualCanUseFeatureAsync(Feature.JoinClasses);

            Assert.False(canJoinClasses);
        }

        #endregion

        #region CustomizeDashboard

        [Fact]
        public async AsyncTask CustomizeDashboard_ShouldBeTrue_WhenUserHasManagedSchool() {
            var canCustomizeDashboard = await UserWithManagedSchoolCanUseFeatureAsync(Feature.CustomizeDashboard);

            Assert.True(canCustomizeDashboard);
        }


        [Fact]
        public async AsyncTask CustomizeDashboard_ShouldBeFalse_WhenUserHasUnmanagedSchool() {
            var canCustomizeDashboard = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.CustomizeDashboard);

            Assert.False(canCustomizeDashboard);
        }

        [Fact]
        public async AsyncTask CustomizeDashboard_ShouldBeFalse_WhenUserDoesNotHaveSchool() {
            var canCustomizeDashboard = await IndividualCanUseFeatureAsync(Feature.CustomizeDashboard);

            Assert.False(canCustomizeDashboard);
        }

        #endregion

        #region CustomizeDashboard

        [Fact]
        public async AsyncTask ExtendedColors_ShouldBeTrue_WhenUserHasManagedSchool() {
            var canUseExtendedColors = await UserWithManagedSchoolCanUseFeatureAsync(Feature.ExtendedColors);

            Assert.True(canUseExtendedColors);
        }


        [Fact]
        public async AsyncTask ExtendedColors_ShouldBeFalse_WhenUserHasUnmanagedSchool() {
            var canUseExtendedColors = await UserWithUnmanagedSchoolCanUseFeatureAsync(Feature.ExtendedColors);

            Assert.False(canUseExtendedColors);
        }

        [Fact]
        public async AsyncTask ExtendedColors_ShouldBeFalse_WhenUserDoesNotHaveSchool() {
            var canUseExtendedColors = await IndividualCanUseFeatureAsync(Feature.ExtendedColors);

            Assert.False(canUseExtendedColors);
        }

        #endregion
    }
}
