﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using MvvmCross;
using MvvmCross.IoC;
using MyStudyLife.Configuration;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Threading;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.Core.Tests
{
    public abstract class TestBase : IDisposable {
        private static volatile bool _mvxInitialized;
        private static readonly object _mvxLocker = new object();

        private volatile bool _initialized;
        private readonly AsyncLock _initializationLock = new AsyncLock(false);

        public TestBase(ITestOutputHelper output) {
            // TODO: logging
        }

        /// <summary>
        ///     Initializes MvvmCross components required for testing, such
        ///     as the IoC and trace. This is done once per test run.
        /// </summary>
        private static void EnsureMvxInitialized() {
            if (_mvxInitialized) return;

            lock (_mvxLocker) {
                if (_mvxInitialized) return;

                var logger = Mock.Of<ILogger>();
                var loggerProvider = new Mock<ILoggerProvider>();
                loggerProvider.Setup(x => x.CreateLogger(It.IsAny<string>())).Returns(logger);

                MvxIoCProvider.Initialize();
                Mvx.IoCProvider.RegisterSingleton(logger);
                Mvx.IoCProvider.RegisterSingleton(loggerProvider.Object);
                
                _mvxInitialized = true;
            }
        }

        protected virtual AsyncTask InitializeAsync() {
            EnsureMvxInitialized();

            var config = new Mock<MslConfig>();
            config.CallBase = true;

            config.SetupGet(x => x.MockUserPremiumStatus).Returns(false);

            Mvx.IoCProvider.RegisterSingleton<IMslConfig>(config.Object);
            
            return AsyncTask.CompletedTask;
        }

        protected async AsyncTask EnsureInitializedAsync() {
            if (_initialized) return;

            using (await _initializationLock.LockAsync()) {
                if (_initialized) return;

                await InitializeAsync();

                _initialized = true;
            }
        }

        /// <summary>
        ///     Creates a mock of <see cref="IUserStore"/> with a
        ///     default fake user.
        /// </summary>
        protected Task<IUserStore> SetupMockUserAsync(Action<User> configureUser = null) {
            var userStore = new Mock<IUserStore>();

            var user = new User {
                Id = 1,
                FirstName = "Testy",
                LastName = "McTestFace",
                Email = "testy.mctestface@mystudylife.com",
                SetupAt = DateTime.UtcNow,
                Settings = UserSettings.Default
            };
    
            configureUser?.Invoke(user);

            userStore.Setup(x => x.GetCurrentUser()).Returns(user);

            User.Current = user;

            return AsyncTask.FromResult(userStore.Object);
        }

        public void Dispose() { }
    }
}
