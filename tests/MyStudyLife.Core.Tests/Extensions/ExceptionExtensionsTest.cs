﻿using MyStudyLife.Extensions;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using Xunit;
using Xunit.Abstractions;

namespace MyStudyLife.Core.Tests.Extensions {
    public class ExceptionExtensionsTest : TestBase {
        public ExceptionExtensionsTest(ITestOutputHelper output) : base(output) { }

        /// <remarks>
        ///     MSL-512 - SocketException introduced by Mono 4.8
        /// </remarks>
        [Fact]
        public void IsTransientWebException_ShouldReturnTrue_WhenHttpRequestNestedSocketException() {
            var ex = new HttpRequestException(
                "An error occurred while sending the request",
                new WebException(
                    "Error: SecureChannelFailure (The authentication or decryption has failed.)",
                    new IOException(
                        "The authentication or decryption has failed.",
                        new IOException(
                            "Unable to write data to the transport connection: The socket is not connected.",
                            // 	The read operation did not complete. Your program must attempt to read again to obtain the complete data packet.
                            new SocketException(-3)
                        )
                    )
                )
            );

            Assert.True(ExceptionExtensions.IsTransientWebException(ex));
        }
    }
}
