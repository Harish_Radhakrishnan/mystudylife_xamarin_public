﻿using MyStudyLife.Authorization;
using System;
using Xunit;
using Xunit.Abstractions;

namespace MyStudyLife.Core.Tests.Authorization {
    public class OAuthWebFlowResultTest : TestBase {
        public OAuthWebFlowResultTest(ITestOutputHelper output) : base(output) { }

        [Fact]
        public void FromQueryString_ShouldParseCode() {
            var result = OAuthWebFlowResult.FromQueryString("?code=test");

            Assert.Equal("test", result.Code);
            Assert.Null(result.Error);
            Assert.Null(result.ErrorMessage);
            Assert.Null(result.ErrorDetail);
            Assert.Null(result.CorrelationId);
        }

        [Fact]
        public void FromQueryString_ShouldParseError() {
            var correlationId = Guid.NewGuid().ToString("n");

            var result = OAuthWebFlowResult.FromQueryString(
                $"?error=invalid&error_message=Something%20was%20invalid&error_detail=ValidationException&correlation_id={correlationId}"
            );

            Assert.Null(result.Code);
            Assert.Equal("invalid", result.Error);
            Assert.Equal("Something was invalid", result.ErrorMessage);
            Assert.Equal("ValidationException", result.ErrorDetail);
            Assert.Equal(correlationId, result.CorrelationId);
        }

        [Theory]
        // No code param
        [InlineData("?access_token=asd")]
        // No error_message param
        [InlineData("?error=an_error")]
        // No error param
        [InlineData("?error_message=an_error")]
        public void FromQueryString_ShouldFailForUnexpectedQueryString(string queryString) {
            var ex = Assert.Throws<ArgumentException>("queryString", () => OAuthWebFlowResult.FromQueryString(queryString));
            Assert.StartsWith("Query string is not valid for an OAuth redirect URI", ex.Message);
        }

        [Theory]
        [InlineData("?error=user_cancel")]
        [InlineData("?error=user_cancel&error_message=")]
        public void FromQueryString_ShouldNotRequireAnErrorMessageWhenErrorIsUserCancel(string queryString) {
            var result = OAuthWebFlowResult.FromQueryString(queryString);
            Assert.Equal("user_cancel", result.Error);
        }
    }
}
