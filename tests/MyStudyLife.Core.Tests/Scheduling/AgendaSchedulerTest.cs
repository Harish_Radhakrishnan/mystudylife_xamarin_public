﻿using Moq;
using MyStudyLife.Data;
using MyStudyLife.Data.Caching;
using MyStudyLife.Scheduling;
using MyStudyLife.Utility;
using System;
using System.Linq;
using Xunit;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;
using System.Collections.ObjectModel;
using System.Globalization;
using MvvmCross;
using MvvmCross.Plugin.Messenger;

namespace MyStudyLife.Core.Tests.Scheduling {
    public class AgendaSchedulerTest : TestBase {
        private readonly AgendaScheduler _defaultAgendaScheduler;

        public AgendaSchedulerTest(ITestOutputHelper output) : base(output) {
            _defaultAgendaScheduler = new AgendaScheduler(
                // Mocks are empty because they are not used for GetRotationWeekForDate
                new Mock<IMvxMessenger>().Object,
                new Mock<IAcademicYearRepository>().Object,
                new Mock<IClassRepository>().Object,
                new Mock<ITaskRepository>().Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                new Mock<IUserStore>().Object
            );
        }

        protected override async AsyncTask InitializeAsync() {
            await base.InitializeAsync();
            await SetupMockUserAsync();

            // All tests are based on the same day being "today"
            Mvx.IoCProvider.RegisterSingleton<IDateTimeProvider>(new MockDateTimeProvider(new DateTime(2016, 12, 08)));
        }

        #region GetEntriesByDayForRange

        [Fact]
        public async AsyncTask GetEntriesByDayForRange_ShouldReturnClassesWhichOccurBetweenTheGivenDates() {
            await EnsureInitializedAsync();

            var classRepo = new Mock<IClassRepository>();
            classRepo.Setup(x => x.GetAllNonDeletedAsync()).ReturnsAsync(
                new [] {
                    new Class() {
                        Date = new DateTime(2016, 12, 06)
                    }, new Class() {
                        Date = new DateTime(2016, 12, 07)
                    }, new Class() {
                        Date = new DateTime(2016, 12, 08)
                    }, new Class() {
                        Date = new DateTime(2016, 12, 09)
                    }, new Class() {
                        Date = new DateTime(2016, 12, 10)
                    }
                }
            );

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                new Mock<IAcademicYearRepository>().Object,
                classRepo.Object,
                new Mock<ITaskRepository>().Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                new Mock<IUserStore>().Object
            );

            var agenda = await agendaScheduler.GetByDayForDateRangeAsync(new DateTime(2016, 12, 07), new DateTime(2016, 12, 09));
            foreach (var agendaDay in agenda) {
                Assert.True(agendaDay.Date.IsBetween(new DateTime(2016, 12, 07), new DateTime(2016, 12, 09), DateTimeComparison.Day));
                agendaDay.Entries.Apply(x => Assert.Equal(agendaDay.Date, x.StartTime.Date));
            }
        }

        #endregion

        #region GetRotationWeekForDate

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldReturnNull_WhenScheduleIsNotWeekRotation() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear() {
                Guid = Guid.NewGuid(),
                Scheduling = {
                    Mode = SchedulingMode.Fixed
                },
                StartDate = new DateTime(2016, 12, 07),
                EndDate = new DateTime(2016, 12, 09)
            };

            Assert.Null(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, new DateTime(2016, 12, 08)));
        }

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldReturnNull_WhenDateIsOutsideRange() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear() {
                Guid = Guid.NewGuid(),
                Scheduling = {
                    Mode = SchedulingMode.WeekRotation
                },
                StartDate = new DateTime(2016, 12, 09),
                EndDate = new DateTime(2016, 12, 10)
            };

            Assert.Null(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, new DateTime(2016, 12, 11)));
        }

        [Theory]
        [InlineData(2, "2016-12-05", 1)]
        [InlineData(2, "2016-12-12", 2)]
        [InlineData(2, "2016-12-19", 1)]

        [InlineData(3, "2016-12-05", 1)]
        [InlineData(3, "2016-12-12", 2)]
        [InlineData(3, "2016-12-19", 3)]
        [InlineData(3, "2016-12-26", 1)]

        [InlineData(4, "2016-12-05", 1)]
        [InlineData(4, "2016-12-12", 2)]
        [InlineData(4, "2016-12-19", 3)]
        [InlineData(4, "2016-12-26", 4)]
        [InlineData(4, "2017-01-02", 1)]
        public async AsyncTask GetRotationWeekForDate_ShouldReturnWeek(int weekCount, string date, int expectedWeek) {
            await EnsureInitializedAsync();

            var cultureInfo = CultureInfo.CurrentCulture;
            if (CultureInfo.CurrentCulture.IsReadOnly) {
                cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            }
            cultureInfo.DateTimeFormat.FirstDayOfWeek = DateTime.Parse("2016-12-05").DayOfWeek;
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = DateTime.Parse("2016-12-05"),
                EndDate = DateTime.Parse("2017-01-02"),
                Scheduling = {
                    WeekCount = (short)weekCount,
                    StartWeek = 1,
                    Mode = SchedulingMode.WeekRotation
                }
            };

            Assert.Equal(
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, DateTime.Parse(date)),
                (short)expectedWeek
            );
        }

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldAdaptToFirstDayOfWeek() {
            // Test that when first day of week:
            //  = Sunday -> Week != Saturday
            //  = Monday -> Week != Sunday
            //  = Saturday -> Week != Friday
            await EnsureInitializedAsync();

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 01),
                EndDate = new DateTime(2016, 12, 30),
                Scheduling = {
                    Mode = SchedulingMode.WeekRotation,
                    WeekCount = 2,
                    StartWeek = 1
                }
            };

            var cultureInfo = CultureInfo.CurrentCulture;
            if (CultureInfo.CurrentCulture.IsReadOnly) {
                cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            }
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;

            var sunday = new DateTime(2016, 12, 11);
            var saturday = new DateTime(2016, 12, 10);
            var monday = new DateTime(2016, 12, 12);
            var friday = new DateTime(2016, 12, 09);

            cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Sunday;
            Assert.NotEqual(
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, sunday),
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, saturday)
            );

            cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Monday;
            Assert.NotEqual(
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday),
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, sunday)
            );

            cultureInfo.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Saturday;
            Assert.NotEqual(
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, saturday),
                _defaultAgendaScheduler.GetRotationWeekForDate(schedule, friday)
            );
        }

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldNotPushSchedule_WhenHolidayWithDurationLessThan5Days() {
            await EnsureInitializedAsync();

            var monday = new DateTime(2016, 12, 12);

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 08),
                EndDate = new DateTime(2016, 12, 28),
                Scheduling = {
                    Mode = SchedulingMode.WeekRotation,
                    WeekCount = 2
                },
                Holidays = {
                    new Holiday {
                        Guid = Guid.NewGuid(),
                        PushesSchedule = true,
                        StartDate = monday,
                        EndDate = monday.AddDays(2)
                    }
                }
            };

            Assert.Equal((short)1, _defaultAgendaScheduler.GetRotationWeekForDate(schedule, new DateTime(2016, 12, 08)));
            Assert.Null(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday));
            Assert.Equal((short)2, _defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday.AddDays(3)));
            Assert.Equal((short)1, _defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday.AddDays(7)));
        }

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldPushSchedule_WhenGreaterThan5DaysHolidayWithPushesSchedule() {
            await EnsureInitializedAsync();

            var monday = new DateTime(2016, 12, 12);

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 08),
                EndDate = new DateTime(2016, 12, 28),
                Scheduling = {
                    Mode = SchedulingMode.WeekRotation,
                    WeekCount = 2
                },
                Holidays = {
                    new Holiday {
                        Guid = Guid.NewGuid(),
                        PushesSchedule = true,
                        StartDate = monday,
                        EndDate = monday.AddDays(6)
                    }
                }
            };

            Assert.Equal(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, new DateTime(2016, 12, 08)), (short)1);
            Assert.Null(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday));
            Assert.Equal(_defaultAgendaScheduler.GetRotationWeekForDate(schedule, monday.AddDays(7)), (short)2);
        }

        [Fact]
        public async AsyncTask GetRotationWeekForDate_ShouldHandleAdjustments() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 01),
                EndDate = new DateTime(2016, 12, 28),
                Scheduling = {
                    Mode = SchedulingMode.WeekRotation,
                    Adjustments = new SchedulingAdjustments {
                        new SchedulingAdjustment(new DateTime(2016, 12, 08), SchedulingAdjustmentType.RotationWeek, 1)
                    }
                }
            };

            Assert.Equal((short)1, _defaultAgendaScheduler.GetRotationWeekForDate(schedule, new DateTime(2016, 12, 08)));
        }

        #endregion

        #region GetRotationDayForDate

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldReturnNull_WhenDayIsNotDefined() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
                Scheduling = {
                    Mode = SchedulingMode.DayRotation,
                    Days = DaysOfWeek.Weekdays
                }
            };

            var saturday = new DateTime(2016, 12, 10);

            Assert.Null(_defaultAgendaScheduler.GetRotationDayForDate(schedule, saturday));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldReturnNull_WhenScheduleIsNotDayRotation() {
            await EnsureInitializedAsync();

            Assert.Null(this._defaultAgendaScheduler.GetRotationDayForDate(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
                Scheduling = {
                    Mode = SchedulingMode.Fixed,
                }

            }, new DateTime(2016, 12, 10)));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldReturnNull_WhenDateIsOutsideRange() {
            await EnsureInitializedAsync();

            Assert.Null(this._defaultAgendaScheduler.GetRotationDayForDate(new AcademicYear {
                Guid = Guid.NewGuid(),
                Scheduling = {
                    Mode = SchedulingMode.DayRotation,
                    StartDay = 1,
                    DayCount = 10,
                    Days = DaysOfWeek.Weekdays
                },
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 09)
            }, new DateTime(2016, 12, 10)));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldReturnNull_WhenDateIsNotARotationDay() {
            await EnsureInitializedAsync();

            Assert.Null(this._defaultAgendaScheduler.GetRotationDayForDate(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
                Scheduling = {
                    Mode = SchedulingMode.DayRotation,
                    Days = DaysOfWeek.Weekdays,
                    StartDay = 1
                }

            }, new DateTime(2016, 12, 10)));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldReturnDays() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                Scheduling = {
                    Mode = SchedulingMode.DayRotation,
                    StartDay = 1,
                    DayCount = 3,
                    Days = DaysOfWeek.Weekdays
                },
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 09)
            };

            Assert.Equal(1,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 05)));
            Assert.Equal(2,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 06)));
            Assert.Equal(3,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 07)));
            Assert.Equal(1,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 08)));
            Assert.Equal(2,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 09)));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldSkipUndefinedDays() {
            await EnsureInitializedAsync();

            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 16),
                Scheduling = {
                    Mode = SchedulingMode.DayRotation,
                    StartDay = 1,
                    DayCount = 10,
                    Days = DaysOfWeek.Weekdays
                }
            };

            Assert.Equal(5,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 09)));
            Assert.Equal(6,
                this._defaultAgendaScheduler.GetRotationDayForDate(schedule, new DateTime(2016, 12, 12)));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldPushSchedule_WhenHolidayWithPushesSchedule() {
            await EnsureInitializedAsync();

            Assert.Equal(4, this._defaultAgendaScheduler.GetRotationDayForDate(
                new AcademicYear {
                    Guid = Guid.NewGuid(),
                    StartDate = new DateTime(2016, 12, 05),
                    EndDate = new DateTime(2016, 12, 16),
                    Scheduling = {
                        Mode = SchedulingMode.DayRotation,
                        StartDay = 1,
                        DayCount = 10,
                        Days = DaysOfWeek.Weekdays
                    },
                    Holidays = {
                        new Holiday {
                            Guid = Guid.NewGuid(),
                            StartDate = new DateTime(2016, 12, 08),
                            EndDate = new DateTime(2016, 12, 08),
                            PushesSchedule = true
                        }
                    }
                }, new DateTime(2016, 12, 09)
            ));
        }

        [Fact]
        public async AsyncTask GetRotationDayForDate_ShouldHandleAdjustments() {
            await EnsureInitializedAsync();

            Assert.Equal(1, this._defaultAgendaScheduler.GetRotationDayForDate(
                new AcademicYear {
                    Guid = Guid.NewGuid(),
                    StartDate = new DateTime(2016, 12, 05),
                    EndDate = new DateTime(2016, 12, 09),
                    Scheduling = {
                        Mode = SchedulingMode.DayRotation,
                        StartDay = 1,
                        DayCount = 5,
                        Days = DaysOfWeek.Weekdays,
                        Adjustments = {
                            new SchedulingAdjustment(
                                new DateTime(2016, 12, 08),
                                SchedulingAdjustmentType.RotationDay,
                                1
                            )
                        }
                    }
                }, new DateTime(2016, 12, 08)
            ));
        }

        #endregion

        #region GetByDayForDateRangeAsync

        [Fact]
        public async AsyncTask GetByDayForDateRangeAsync_ShouldNotReturnClassesInBetweenTerms_WhenSchedulingScopeIsAuto() {
            await EnsureInitializedAsync();

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var classRepo = new Mock<IClassRepository>();

            var academicYear = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 09, 05),
                EndDate = new DateTime(2017, 07, 21),
                Scheduling = new SchedulingOptions {
                    Mode = SchedulingMode.Fixed,
                    Scope = SchedulingScope.Auto
                },
                Terms = {
                    new AcademicTerm {
                        Guid = Guid.NewGuid(),
                        StartDate = new DateTime(2016, 09, 05),
                        EndDate = new DateTime(2016, 12, 16)
                    },
                    new AcademicTerm {
                        Guid = Guid.NewGuid(),
                        StartDate = new DateTime(2017, 01, 03),
                        EndDate = new DateTime(2017, 04, 14)
                    }
                }
            };

            academicYearRepo
                .Setup(x => x.GetForDateRange(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                .ReturnsAsync(new[] { academicYear });
            classRepo
                .Setup(x => x.GetByDateRangeAndScheduleGuidsAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<Guid[]>()))
                .ReturnsAsync(new [] {
                    new Class {
                        Guid = Guid.NewGuid(),
                        Year = academicYear,
                        YearGuid = academicYear.Guid,
                        Subject = new Subject(),
                        Type = ClassType.Recurring,
                        Times = new System.Collections.ObjectModel.ObservableCollection<ClassTime> {
                            new ClassTime {
                                StartTime = new TimeSpan(9,0,0),
                                EndTime = new TimeSpan(10,0,0),
                                Days = DaysOfWeek.Weekdays
                            }
                        }
                    }
                });

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                classRepo.Object,
                new Mock<ITaskRepository>().Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var days = await agendaScheduler.GetByDayForDateRangeAsync(new DateTime(2016, 12, 16), new DateTime(2017, 01, 03));

            Assert.Empty(days.Where(x => new DateTime(2016, 12, 16) < x.Date && x.Date < new DateTime(2017, 01, 03)).SelectMany(x => x.Entries));
            Assert.NotEmpty(days.First(x => x.Date == new DateTime(2016, 12, 16)).Entries);
            Assert.NotEmpty(days.First(x => x.Date == new DateTime(2017, 01, 03)).Entries);
        }

        #endregion

        #region GetNextScheduledDay

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnTomorrow_WhenThereIsAnExamTomorrow() {
            await EnsureInitializedAsync();

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var examRepo = new Mock<IExamRepository>();

            academicYearRepo.Setup(x => x.GetForDateAsync(new DateTime(2016, 12, 08))).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
            });
            var tomorrow = new DateTime(2016, 12, 09);
            examRepo.Setup(x => x.GetForDateRangeAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>())).ReturnsAsync(
                new[]{
                    new Exam {
                        Guid = Guid.NewGuid(),
                        Date = tomorrow,
                        Subject = new Subject()
                    }
            });

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                new Mock<IClassRepository>().Object,
                new Mock<ITaskRepository>().Object,
                examRepo.Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.Tomorrow, nextDay.Type);
            Assert.Equal(tomorrow, nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnTomorrow_WhenThereIsATaskDueTomorrow() {
            await EnsureInitializedAsync();
            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var taskRepo = new Mock<ITaskRepository>();

            var tomorrow = new DateTime(2016, 12, 09);
            academicYearRepo.Setup(x => x.GetForDateAsync(new DateTime(2016, 12, 08))).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
            });
            taskRepo.Setup(x => x.GetIncompleteByDueDateAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>())).ReturnsAsync(
                new[]{
                    new Task {
                        Guid = Guid.NewGuid(),
                        DueDate = new DateTime(2016, 12, 09),
                        Subject = new Subject()
                    }
            });

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                new Mock<IClassRepository>().Object,
                taskRepo.Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                new Mock<IUserStore>().Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.Tomorrow, nextDay.Type);
            Assert.Equal(tomorrow, nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnTomorrow_WhenThereIsAOneOffClassTomorrow() {
            await EnsureInitializedAsync();

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var classRepo = new Mock<IClassRepository>();

            var tomorrow = new DateTime(2016, 12, 09);
            academicYearRepo.Setup(x => x.GetForDateAsync(new DateTime(2016, 12, 08))).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11),
            });
            classRepo.Setup(x => x.GetByDateRangeAndScheduleGuidsAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>())).ReturnsAsync(
                new[] {
                    new Class {
                        Type = ClassType.OneOff,
                        Guid = Guid.NewGuid(),
                        Subject = new Subject(),
                        Date = tomorrow,
                        StartTime = new TimeSpan(10, 10, 10),
                        EndTime = new TimeSpan(11, 11, 11)
                    }    
            });

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                classRepo.Object,
                new Mock<ITaskRepository>().Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.Tomorrow, nextDay.Type);
            Assert.Equal(tomorrow, nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnTomorrow_WhenThereIsARecurringClassTomorrow() {
            await EnsureInitializedAsync();

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var classRepo = new Mock<IClassRepository>();

            var tomorrow = new DateTime(2016, 12, 09);
            var schedule = new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 05),
                EndDate = new DateTime(2016, 12, 11)
            };

            academicYearRepo.Setup(x => x.GetForDateAsync(It.IsAny<DateTime>())).ReturnsAsync(schedule);
            academicYearRepo.Setup(x => x.GetForDateRange(It.IsAny<DateTime>(), It.IsAny<DateTime>())).ReturnsAsync(new[] { schedule });
            var cls = new Class {
                Type = ClassType.Recurring,
                Guid = Guid.NewGuid(),
                Subject = new Subject(),
                Year = schedule,
                YearGuid = schedule.Guid,
                Times = new ObservableCollection<ClassTime> {
                    new ClassTime() {
                        StartTime = new TimeSpan(10, 10, 10),
                        EndTime = new TimeSpan(11, 11, 11),
                        Days = DaysOfWeek.All
                    }
                },
            };
            classRepo.Setup(x => x.GetByDateRangeAndScheduleGuidsAsync(
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<Guid[]>()
            )).ReturnsAsync(
                new[] { cls }
            );

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                classRepo.Object,
                new Mock<ITaskRepository>().Object,
                new Mock<IExamRepository>().Object,
                new Mock<ICache>().Object,
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.Tomorrow, nextDay.Type);
            Assert.Equal(tomorrow, nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnFirstDayOfNextAcademicYear_WhenNoCurrentAcademicYear() {
            await EnsureInitializedAsync();

            var startDate = new DateTime(2016, 11, 15);
            var endDate = new DateTime(2016, 11, 15);

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            var classRepo = new Mock<IClassRepository>();
            var taskRepo = new Mock<ITaskRepository>();
            var examRepo = new Mock<IExamRepository>();
            var featureService = new Mock<IFeatureService>();

            var academicYears = new[] {
                new AcademicYear {
                    Guid = Guid.NewGuid(),
                    StartDate = new DateTime(2016, 09, 03),
                    EndDate = new DateTime(2017, 07, 20),
                    Scheduling = new SchedulingOptions {
                        Mode = SchedulingMode.Fixed
                    }
                }
            };

            academicYearRepo.Setup(x => x.GetForDateRange(startDate, endDate)).ReturnsAsync(academicYears);
            academicYearRepo.Setup(x => x.GetYearAfterDateAsync(It.IsAny<DateTime>())).ReturnsAsync(academicYears.First());

            var agendaScheduler = new AgendaScheduler(
                null,
                academicYearRepo.Object,
                classRepo.Object,
                taskRepo.Object,
                examRepo.Object,
                new Cache(),
                featureService.Object,
                new Mock<ITimetableSettingsStore>().Object,
                new Mock<IUserStore>().Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 07, 26));

            Assert.NotNull(nextDay);
            Assert.Equal(nextDay.Date, new DateTime(2016, 09, 03));
            Assert.Equal(NextDayType.NextAcademicYear, nextDay.Type);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnFirstDayOfNextTerm_WhenFirstDayWithEventsIsNextTerm() {
            await EnsureInitializedAsync();

            var startDate = new DateTime(2016, 12, 08);
            var endDate = new DateTime(2016, 12, 28);
            var termStartDate = new DateTime(2016, 12, 18);
            var termEndDate = new DateTime(2016, 12, 20);

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            academicYearRepo.Setup(x => x.GetForDateAsync(startDate)).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = startDate,
                EndDate = endDate,
                Terms = new ObservableCollection<AcademicTerm> {
                    new AcademicTerm {
                        Guid = Guid.NewGuid(),
                        StartDate = termStartDate,
                        EndDate = termEndDate
                    }
                }
            });

            var examRepo = new Mock<IExamRepository>();
            examRepo.Setup(x => x.GetForDateRangeAsync(
                new DateTime(2016, 12, 16), new DateTime(2016, 12, 23).EndOfDay()
            )).ReturnsAsync(new[] {
                new Exam {
                    Guid = Guid.NewGuid(),
                    Date = termStartDate,
                    StartTime = new TimeSpan(10, 10, 10),
                    Duration = 60,
                    Subject = new Subject()
                }
            });

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                new Mock<IClassRepository>().Object,
                new Mock<ITaskRepository>().Object,
                examRepo.Object,
                new Cache(),
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(startDate);
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.NextTerm, nextDay.Type);
            Assert.Equal(termStartDate, nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnFirstDayAfterHoliday_WhenThereIsACurrentHoliday() {
            await EnsureInitializedAsync();

            var holidayStart = new DateTime(2016, 12, 18);
            var holidayEnd = new DateTime(2016, 12, 21);

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            academicYearRepo.Setup(x => x.GetForDateAsync(new DateTime(2016, 12, 08))).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 08),
                EndDate = new DateTime(2016, 12, 28),
                Holidays = new ObservableCollection<Holiday> {
                    new Holiday {
                        Guid = Guid.NewGuid(),
                        StartDate = holidayStart,
                        EndDate = holidayEnd
                    }
                }
            });

            var examRepo = new Mock<IExamRepository>();
            examRepo.Setup(x => x.GetForDateRangeAsync(
                new DateTime(2016, 12, 16), new DateTime(2016, 12, 23).EndOfDay()
            )).ReturnsAsync(new[] {
                new Exam {
                    Guid = Guid.NewGuid(),
                    Date = new DateTime(2016, 12, 22),
                    StartTime = new TimeSpan(10, 10, 10),
                    Duration = 60,
                    Subject = new Subject()
                }
            });


            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                new Mock<IClassRepository>().Object,
                new Mock<ITaskRepository>().Object,
                examRepo.Object,
                new Cache(),
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.DayAfterHoliday, nextDay.Type);
            Assert.Equal(holidayEnd.AddDays(1), nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnNextDay_WhenThereAreNoApplicableDayDescriptions() {
            await EnsureInitializedAsync();

            var academicYearRepo = new Mock<IAcademicYearRepository>();
            academicYearRepo.Setup(x => x.GetForDateAsync(new DateTime(2016, 12, 08))).ReturnsAsync(new AcademicYear {
                Guid = Guid.NewGuid(),
                StartDate = new DateTime(2016, 12, 08),
                EndDate = new DateTime(2016, 12, 28)
            });

            var examRepo = new Mock<IExamRepository>();
            examRepo.Setup(x => x.GetForDateRangeAsync(
                new DateTime(2016, 12, 16), new DateTime(2016, 12, 23).EndOfDay()
            )).ReturnsAsync(new[] {
                new Exam {
                    Guid = Guid.NewGuid(),
                    Date = new DateTime(2016, 12, 22),
                    StartTime = new TimeSpan(10, 10, 10),
                    Duration = 60,
                    Subject = new Subject()
                }
            });

            var userStore = new Mock<IUserStore>();
            userStore.Setup(x => x.GetCurrentUser()).Returns(new User());

            var agendaScheduler = new AgendaScheduler(
                new Mock<IMvxMessenger>().Object,
                academicYearRepo.Object,
                new Mock<IClassRepository>().Object,
                new Mock<ITaskRepository>().Object,
                examRepo.Object,
                new Cache(),
                new Mock<IFeatureService>().Object,
                new Mock<ITimetableSettingsStore>().Object,
                userStore.Object
            );

            var nextDay = await agendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.NotNull(nextDay);
            Assert.Equal(NextDayType.NextDay, nextDay.Type);
            Assert.Equal(new DateTime(2016, 12, 22), nextDay.Date);
        }

        [Fact]
        public async AsyncTask GetNextScheduledDay_ShouldReturnNone_WhenNoNextScheduledDay() {
            await EnsureInitializedAsync();

            var nextDay = await _defaultAgendaScheduler.GetNextScheduledDayAsync(new DateTime(2016, 12, 08));
            Assert.Null(nextDay);
        }

        #endregion
    }
}
