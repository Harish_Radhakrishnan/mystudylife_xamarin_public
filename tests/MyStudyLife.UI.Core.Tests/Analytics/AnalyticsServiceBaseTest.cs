﻿using Moq;
using MyStudyLife.Core.Tests;
using MyStudyLife.UI.Analytics;
using Xunit;
using Xunit.Abstractions;

namespace MyStudyLife.UI.Core.Tests.Analytics {
    public class AnalyticsServiceBaseTest : TestBase {
        public AnalyticsServiceBaseTest(ITestOutputHelper output) : base(output) { }

        [Fact]
        public void SetUserInfo_ShouldNotifySessionEnd_WhenSetToNullAndLastKnownWasNotNull() {
            var analyticsService = new MockAnalyticsServiceBase(
                new Mock<INavigationService>().Object
            );

            analyticsService.SetUserInfo("1", "Student", null, false);

            Assert.False(analyticsService.NotifiySessionEndImplCalled);

            analyticsService.SetUserInfo(null, null, null, false);

            Assert.True(analyticsService.NotifiySessionEndImplCalled);
                
        }

        class MockAnalyticsServiceBase : AnalyticsServiceBase {
            public bool NotifiySessionEndImplCalled { get; private set; }

            public MockAnalyticsServiceBase(INavigationService navigationService) : base(navigationService) { }

            protected override void NotifySessionEndImpl() {
                this.NotifiySessionEndImplCalled = true;
            }

            protected override void SetUserInfoImpl(string userId, string userType, string schoolId, string schoolType) { }
            protected override void TrackEventImpl(string category, string action) { }
            protected override void TrackEventImpl(string category, string action, string label) { }
            protected override void TrackScreenImpl(string screenName) { }
        }
    }
}
