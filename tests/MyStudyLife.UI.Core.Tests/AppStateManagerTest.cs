﻿using Moq;
using MyStudyLife.Authorization;
using MyStudyLife.Core.Tests;
using MyStudyLife.Data;
using MyStudyLife.Data.Settings;
using MyStudyLife.Diagnostics;
using MyStudyLife.UI.Analytics;
using MyStudyLife.Versioning;
using System;
using MvvmCross.Plugin.Messenger;
using Xunit;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.Core.Tests {
    public class AppStateManagerTest : TestBase {
        public AppStateManagerTest(ITestOutputHelper output) : base(output) { }

        [Fact]
        public async AsyncTask GetStateAsync_ShouldNotReturnUser_WhenUpgradeRequired() {
            await EnsureInitializedAsync();

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(true);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = new Mock<IUserStore>();
            userStoreMock.Setup(x => x.GetCurrentUser()).Callback(() => {
                throw new Exception("Should not be called");
            });

            var appStateManager = new AppStateManager(
                new Mock<IMvxMessenger>().Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock.Object,
                new Mock<IAnalyticsService>().Object,
                new Mock<IBugReporter>().Object
            );

            var appState = await appStateManager.GetStateAsync();
            Assert.Null(appState.User);
        }

        [Fact]
        public async AsyncTask GetStateAsync_ShouldReturnUser_WhenAuthorized() {
            await EnsureInitializedAsync();

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(false);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = await SetupMockUserAsync();

            var appStateManager = new AppStateManager(
                new Mock<IMvxMessenger>().Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock,
                new Mock<IAnalyticsService>().Object,
                new Mock<IBugReporter>().Object
            );

            var appState = await appStateManager.GetStateAsync();
            Assert.NotNull(appState.User);
        }

        [Fact]
        public async AsyncTask GetStateAsync_SetUser_ShouldUpdateAnalytics() {
            await EnsureInitializedAsync();

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(false);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = new Mock<IUserStore>();
            userStoreMock.Setup(x => x.GetCurrentUser()).Returns((User)null);

            var analyticsServiceMock = new Mock<IAnalyticsService>();
            analyticsServiceMock.Setup(x => x.SetUserInfo(It.IsAny<User>())).Verifiable();

            var appStateManager = new AppStateManager(
                new Mock<IMvxMessenger>().Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock.Object,
                analyticsServiceMock.Object,
                new Mock<IBugReporter>().Object
            );

            var appState = await appStateManager.GetStateAsync();
            appState.SetUser(new User {
                Id = 1,
                Type = UserType.Student,
                Settings = UserSettings.Default
            });

            analyticsServiceMock.Verify();
        }

        [Fact]
        public async AsyncTask GetStateAsync_SetUser_ShouldUpdateBugReporter() {
            await EnsureInitializedAsync();

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(false);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = new Mock<IUserStore>();
            userStoreMock.Setup(x => x.GetCurrentUser()).Returns((User)null);

            var bugReporterMock = new Mock<IBugReporter>();
            bugReporterMock.Setup(x => x.SetUser(It.IsAny<User>())).Verifiable();

            var appStateManager = new AppStateManager(
                new Mock<IMvxMessenger>().Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock.Object,
                new Mock<IAnalyticsService>().Object,
                bugReporterMock.Object
            );

            var appState = await appStateManager.GetStateAsync();
            appState.SetUser(new User {
                Id = 1,
                Settings = UserSettings.Default
            });

            bugReporterMock.Verify();
        }

        [Fact]
        public async AsyncTask GetStateAsync_SetUser_ShouldApplyLocaleSettings() {
            await EnsureInitializedAsync();

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(false);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = new Mock<IUserStore>();
            userStoreMock.Setup(x => x.GetCurrentUser()).Returns((User)null);

            var appStateManager = new AppStateManager(
                new Mock<IMvxMessenger>().Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock.Object,
                new Mock<IAnalyticsService>().Object,
                new Mock<IBugReporter>().Object
            );

            var appState = await appStateManager.GetStateAsync();

            var userMock = new Mock<User>();
            userMock.Setup(x => x.ApplyLocaleSettings()).Verifiable();

            var user = userMock.Object;
            user.Id = 1;
            user.Settings = UserSettings.Default;

            appState.SetUser(user);

            userMock.Verify();
        }

        [Fact]
        public async AsyncTask ShouldSetUserToNull_OnAuthStatusChangedMessage() {
            await EnsureInitializedAsync();

            Action<AuthStatusChangedEventMessage> messengerCallback = null;

            var messengerMock = new Mock<IMvxMessenger>();
            messengerMock
                .Setup(x => x.SubscribeOnThreadPoolThread(It.IsAny<Action<AuthStatusChangedEventMessage>>(), It.IsAny<MvxReference>(), It.IsAny<string>()))
                .Callback<Action<AuthStatusChangedEventMessage>, MvxReference, string>((callback, reference, tag) => {
                    messengerCallback = callback;
                });

            var upgradeServiceMock = new Mock<IUpgradeService>();
            upgradeServiceMock.Setup(x => x.RequiresUpgrade).Returns(false);

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.Setup(x => x.IsAuthorized).Returns(true);

            var userStoreMock = new Mock<IUserStore>();
            userStoreMock.Setup(x => x.GetCurrentUser()).Returns(new User {
                Id = 1,
                Type = UserType.Student,
                Settings = UserSettings.Default
            });

            var appStateManager = new AppStateManager(
                messengerMock.Object,
                upgradeServiceMock.Object,
                authServiceMock.Object,
                new Mock<IStorageProvider>().Object,
                userStoreMock.Object,
                new Mock<IAnalyticsService>().Object,
                new Mock<IBugReporter>().Object
            );
            appStateManager.EnsureInitialized();

            Assert.NotNull(messengerCallback);

            // Ensure we've loaded the state with a user, so we can check it gets set to null
            await appStateManager.GetStateAsync();

            messengerCallback(new AuthStatusChangedEventMessage(this, false, AuthStatusChangeCause.SignOut));

            var appState = await appStateManager.GetStateAsync();
            Assert.Null(appState.User);
        }
    }
}
