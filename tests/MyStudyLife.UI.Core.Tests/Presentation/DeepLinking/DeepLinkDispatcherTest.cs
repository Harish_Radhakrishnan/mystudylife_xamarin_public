﻿using System;
using System.Threading;
using Moq;
using MyStudyLife.Authorization;
using MyStudyLife.Core.Tests;
using MyStudyLife.UI.Presentation;
using MyStudyLife.UI.Presentation.DeepLinking;
using Xunit;
using Xunit.Abstractions;

namespace MyStudyLife.UI.Core.Tests.Presentation.DeepLinking
{
    public class DeepLinkDispatcherTest : TestBase {
        public DeepLinkDispatcherTest(ITestOutputHelper output) : base(output) { }

        [Fact]
        public void Dispatch_ShouldChangePresentationWithDeepLinkPresentationHint() {
            var navigationServiceMock = new Mock<INavigationService>();
            navigationServiceMock
                .Setup(x => x.ChangePresentation(It.IsAny<DeepLinkPresentationHint>(), CancellationToken.None))
                .ReturnsAsync(true)
                .Verifiable();

            var dispatcher = new DeepLinkDispatcher(navigationServiceMock.Object);

            dispatcher.Dispatch(new Uri("msl://launch"));

            navigationServiceMock.Verify();
        }

        [Theory]
        [InlineData("msl://oauth/callback?code=test")]
        [InlineData("msl://oauth/callback?error=user_cancel")]
        [InlineData("msl://oauth/callback?error=server_error&error_message=Bad+things+happened")]
        public void Dispatch_ShouldChangePresentationWithOAuthWebFlowResultHint_WhenOAuthCallbackUri(string uri) {
            var navigationServiceMock = new Mock<INavigationService>();
            navigationServiceMock
                .Setup(x => x.ChangePresentation(It.IsAny<OAuthWebFlowResultPresentationHint>(), CancellationToken.None))
                .ReturnsAsync(true)
                .Verifiable();

            var authServiceMock = new Mock<IAuthorizationService>();
            authServiceMock.SetupGet(x => x.IsAuthorized).Returns(false);

            var dispatcher = new DeepLinkDispatcher(navigationServiceMock.Object);

            dispatcher.Dispatch(new Uri(uri));

            navigationServiceMock.Verify();
        }
    }
}
