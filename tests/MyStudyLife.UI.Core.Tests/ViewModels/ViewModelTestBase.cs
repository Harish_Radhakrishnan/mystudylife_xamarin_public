﻿using Moq;
using MyStudyLife.Core.Tests;
using MyStudyLife.Diagnostics;
using MyStudyLife.Net;
using System.Threading.Tasks;
using MvvmCross.Core;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using MvvmCross;
using MvvmCross.Base;
using MvvmCross.Plugin.Messenger;
using Xunit.Abstractions;

namespace MyStudyLife.UI.Core.Tests.ViewModels {
    public abstract class ViewModelTestBase : TestBase {
        protected IMvxViewModelLoader ViewModelLoader { get; private set; }

        public ViewModelTestBase(ITestOutputHelper output) : base(output) { }

        protected override async Task InitializeAsync() {
            await base.InitializeAsync();

            MvxSingletonCache.Initialize();

            Mvx.IoCProvider.RegisterSingleton<IMvxSettings>(new MvxSettings());

            Mvx.IoCProvider.RegisterSingleton(Mock.Of<IMvxViewDispatcher>());
            Mvx.IoCProvider.RegisterSingleton(Mock.Of<IMvxMainThreadDispatcher>());

            this.ViewModelLoader = new MvxViewModelLoader(new MslApp());
            Mvx.IoCProvider.RegisterSingleton(this.ViewModelLoader);

            Mvx.IoCProvider.RegisterType<IMvxStringToTypeParser, MvxStringToTypeParser>();

            var networkProvider = new Mock<INetworkProvider>();
            networkProvider.SetupGet(x => x.IsOnline).Returns(true);
            Mvx.IoCProvider.RegisterSingleton(networkProvider.Object);

            var navigationService = new Mock<INavigationService>();
            navigationService.Setup(x => x.BackStack).Returns(new NavigationBackStack());
            Mvx.IoCProvider.RegisterSingleton<INavigationService>(navigationService.Object);

            var messenger = new MvxMessengerHub();
            Mvx.IoCProvider.RegisterSingleton<IMvxMessenger>(messenger);

            var bugReporter = new Mock<IBugReporter>();
            Mvx.IoCProvider.RegisterSingleton(bugReporter.Object);
        }

        protected TViewModel LoadViewModel<TViewModel>(TViewModel viewModel) where TViewModel : IMvxViewModel
            => LoadViewModel(viewModel, null);

        protected TViewModel LoadViewModel<TViewModel>(TViewModel viewModel, object navObject) where TViewModel : IMvxViewModel {
            // This mirrors what MvvmCross does in MvxDefaultViewModelLocator, but
            // if we were to use that, we'd need to register all our dependencies as
            // it uses IoC construct to create the VM

            viewModel.CallBundleMethods("Init", new MvxBundle(navObject?.ToSimplePropertyDictionary()));

            //if (savedState != null) {
            //    viewModel.CallBundleMethods("ReloadState", savedState);
            //}

            viewModel.Start();

            return viewModel;
        }
    }
}
