﻿using Moq;
using MyStudyLife.Data;
using MyStudyLife.UI.ViewModels.View;
using MyStudyLife.Utility;
using System;
using MvvmCross;
using Xunit;
using Xunit.Abstractions;
using AsyncTask = System.Threading.Tasks.Task;

namespace MyStudyLife.UI.Core.Tests.ViewModels.View {
    public class TaskViewModelTest : ViewModelTestBase {
        public TaskViewModelTest(ITestOutputHelper output) : base(output) { }

        protected override async AsyncTask InitializeAsync() {
            await base.InitializeAsync();

            var userStore = await SetupMockUserAsync();

            Mvx.IoCProvider.RegisterSingleton(userStore);

            var appStateMock = new Mock<IAppState>();
            appStateMock.Setup(x => x.User).Returns(userStore.GetCurrentUser());

            var appStateManagerMock = new Mock<IAppStateManager>();
            appStateManagerMock.Setup(x => x.GetStateAsync()).ReturnsAsync(appStateMock.Object);
            Mvx.IoCProvider.RegisterSingleton(appStateManagerMock.Object);
        }

        [Fact]
        public async AsyncTask ShouldLoadItem_WhenNavObjectHasGuid() {
            await EnsureInitializedAsync();

            var task = new Task {
                Guid = Guid.NewGuid(),
                Subject = new Subject {
                    Guid = Guid.NewGuid(),
                    Name = "Test",
                    Color = "0"
                },
                DueDate = new DateTime(2016, 11, 16),
                Type = TaskType.Assignment,
                Title = "Test"
            };

            var classRepo = new Mock<IClassRepository>();
            var examRepo = new Mock<IExamRepository>();
            var taskRepo = new Mock<ITaskRepository>();
            taskRepo.Setup(x => x.GetAsync(task.Guid)).ReturnsAsync(task);

            var featureService = new Mock<IFeatureService>();

            var vm  = LoadViewModel(
                new TaskViewModel(featureService.Object, classRepo.Object, taskRepo.Object, examRepo.Object),
                new TaskViewModel.NavObject {
                    Guid = task.Guid
                }
            );
            
            Assert.NotNull(vm.Item);
            Assert.Equal(vm.Item.Guid, task.Guid);
        }
    }
}
